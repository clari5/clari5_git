package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxRest;
import clari5.platform.util.Hocon;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import clari5.rdbms.Rdbms;
import clari5.platform.dbcon.QueryMapper;
import cxps.apex.utils.CxpsLogger;

import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.*;
import java.util.Date;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import cxps.apex.utils.StringUtils;

public class NFT_DedupEvent extends Event {
	private String ucicId;
	private String custId;
	private String custName;
	private String mobNumber;
	private String landlineNumber;
	private String homeLandlineNumber;
	private String officeLandlineNumber;
	private String emailId;
	private String permanentAddr1;
	private String permanentAddr2;
	private String permanentAddr3;
	private String mailingAddr1;
	private String mailingAddr2;
	private String mailingAddr3;
	private String pan;
	private String passportNumber;
	private Date doi;
	private Date dob;
	private String handPhone;
	private String completePermanentAddr;
	private String completeMailingAddr;
	private String nameChanged;
	private String completePerAddrChanged;
	private String completeMailingAddrChanged;
	private int mobMatchedCount=0;
	private int landlineMatchedCount=0;
	private int officephoneMatchedCount=0;
	private int emailMatchedCount=0;
	private int panMatchedCount=0;
	private int passportMatchedCount=0;
	private int mobNonStaffMatchedCount=0;
	private int completePerAddrCount=0;
	private int completeMailingAddrCount=0;
	private int phoneCount=0;
	private int mobCount=0;
	private int intermediateLandlineCount=0;
	private int intermediateofficelandCount=0;
	private int intermediatehandphoneCount=0;
	private Double score;
	private Double scorePercentage = 100.0;
	private Double nameMatchScore = 0.0;
	private Double permanantAddrScore= 0.0 ;
	private Double mailingAddrScore =0.0;
	private int dobMatchedCount=0;
	private int doiMatchedCount=0;
	private int accountcount=0;
	private String accountId;
	private String product_code;
	private Date sys_time;
	private String host_id;
	private String entityType;


	@JsonIgnore
	protected SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-dd-MM");
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setUcicId((String) msgMap.get("ucicId"));
		setCustId((String) msgMap.get("custId"));
		setCustName((String) msgMap.get("custName"));
		setMobNumber((String) msgMap.get("mobNumber"));
		setLandlineNumber((String) msgMap.get("landlineNumber"));
		setHomeLandlineNumber((String) msgMap.get("homeLandlineNumber"));
		setOfficeLandlineNumber((String) msgMap.get("officeLandlineNumber"));
		setEmailId((String) msgMap.get("emailId"));
		setPermanentAddr1((String) msgMap.get("permanentAddr1"));
		setPermanentAddr2((String) msgMap.get("permanentAddr2"));
		setPermanentAddr3((String) msgMap.get("permanentAddr3"));
		setMailingAddr1((String) msgMap.get("mailingAddr1"));
		setMailingAddr2((String) msgMap.get("mailingAddr2"));
		setMailingAddr3((String) msgMap.get("mailingAddr3"));
		setPan((String) msgMap.get("pan"));
		setPassportNumber((String) msgMap.get("passportNumber"));
		setDoi((String) msgMap.get("doi"));
		setDob((String) msgMap.get("dob"));
		setHandPhone((String) msgMap.get("handPhone"));
		setCompletePermanentAddr((String) msgMap.get("completePermanentAddr"));
		setCompleteMailingAddr((String) msgMap.get("completeMailingAddr"));
		setNameChanged((String) msgMap.get("nameChanged"));
		setCompletePerAddrChanged((String) msgMap.get("completePerAddrChanged"));
		setCompleteMailingAddrChanged((String) msgMap.get("completeMailingAddrChanged"));
		setAccountId((String) msgMap.get("accountId"));
		setProduct_code((String) msgMap.get("product_code"));
		setSys_time1((String) msgMap.get("sys_time"));
		setEntityType((String) msgMap.get("entity_type"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_DedupEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		/*
		String hostAcctKey = getAccountId ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		*/
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/

		Connection con = null;
        try {
            con = Rdbms.getAppConnection();
            try {
                Hocon hocon = new Hocon();
                hocon.loadFromContext("custom-wl-rule.conf");
                Hocon factHocon = hocon.get("clari5.custom.wl-rule");
                List<String> rulesList = factHocon.getStringList("rules");
                WlRules wlRules = new WlRules(hocon);
                for ( String rule : rulesList) {
                    JSONObject data = wlRules.createRequestJson(rule, setDedupData());
                    String response = wlRules.sendWlRequest(data);
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getJSONObject("status").get("status").toString();
                    if ("success".equals(status.trim())) {
                        if (jsonObject.has("matchedResults")) {
                            String maxScore = jsonObject.getJSONObject("matchedResults").getString("maxScore");
                            score = Double.parseDouble(maxScore) * scorePercentage;
                            if (rule.equalsIgnoreCase("NAME_MATCH")) {
                                setNameMatchScore(score);
                            } else if (rule.equalsIgnoreCase("MAILING_ADDRESS_PARTIAL_MATCH")) {
                                setMailingAddrScore(score);
                            } else if (rule.equalsIgnoreCase("PERMANENT_ADDRESS_PARTIAL_MATCH")) {
                                setPermanantAddrScore(score);
                            } else {
                                cxLog.debug("rule name [" + rule + "] not mapped in custom-wl-rule.conf ");
                            }
                        }
                    }

                }
            }catch (Exception e){
                cxLog.error("Exception in Watchlist " +e.getMessage());
            }

            String []data= {getMobNumber(),getHomeLandlineNumber(),getOfficeLandlineNumber(),getEmailId(),getPan(),getPassportNumber(),getDob().toString(),getDoi().toString(),getHandPhone()};
            int dataIndex =0;
            DbQueries dbQueries = new DbQueries();
            String ucicId = getUcicId();
            Date dob =getDob();
             for (String value : data ) {

                 if (!StringUtils.isNullOrEmpty(value)){
                     switch (dataIndex){
                         case 0: int [] mobiledata = dbQueries.getMobiledata(con,value,ucicId);
                             setMobMatchedCount(mobiledata[0]);
                             setMobNonStaffMatchedCount(mobiledata[1]);
                             setMobCount(mobiledata[2]);
                             break;
                         case 1: int[]  homelandline =dbQueries.gethomeLandline(con,value,ucicId);
                             setLandlineMatchedCount(homelandline[0]);
                             setIntermediateLandlineCount(homelandline[1]);
                             break;
                         case 2 : int[] officelandline= dbQueries.getofficeLandline(con,value,ucicId);
                             setOfficephoneMatchedCount(officelandline[0]);
                             setIntermediateofficelandCount(officelandline[1]);
                             break;
                         case 3:int email= dbQueries.getemail(con,value,ucicId);
                             setEmailMatchedCount(email);
                             break;
                         case 4:int pan= dbQueries.getpan(con,value,ucicId);
                             setPanMatchedCount(pan);
                             break;
                         case 5:int passport= dbQueries.getpassport(con,value,ucicId);
                             setPassportMatchedCount(passport);
                             break;
                         case 6:int dateOfBirth= dbQueries.getdob(con,dob,ucicId);
                             setDobMatchedCount(dateOfBirth);
                             break;
                         case 7:int dateOfIncorporation= dbQueries.getdoi(con,doi,ucicId);
                             setDoiMatchedCount(dateOfIncorporation);
                             break;
                         case 8:int handphone= dbQueries.gethandphone(con,value,ucicId);
                             setIntermediatehandphoneCount(handphone);
                             break;
                     }
                 }
                 dataIndex++;
             }
             //The logic for phonecount need to be revisited
             int phonecount = getIntermediatehandphoneCount() +getIntermediateLandlineCount()+getIntermediateofficelandCount();
             setPhoneCount(phonecount);

        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (con != null) con.close();
            } catch (Exception e) {
            }
        }


		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setUcicId(String ucicId) throws InvalidDataException { 
		this.ucicId = ucicId;
	}
	public String getUcicId() {
		return this.ucicId;
	}

	public void setCustId(String custId) throws InvalidDataException { 
		this.custId = custId;
	}
	public String getCustId() {
		return this.custId;
	}

	public void setCustName(String custName) throws InvalidDataException { 
		this.custName = custName;
	}
	public String getCustName() {
		return this.custName;
	}

	public void setMobNumber(String mobNumber) {
		this.mobNumber = mobNumber;
	}
	public String getMobNumber() {
		return this.mobNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}
	public String getLandlineNumber() {
		return this.landlineNumber;
	}

	public void setHomeLandlineNumber(String homeLandlineNumber) {
		this.homeLandlineNumber = homeLandlineNumber;
	}
	public String getHomeLandlineNumber() {
		return this.homeLandlineNumber;
	}

	public void setOfficeLandlineNumber(String officeLandlineNumber) {
		this.officeLandlineNumber = officeLandlineNumber;
	}
	public String getOfficeLandlineNumber() {
		return this.officeLandlineNumber;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEmailId() {
		return this.emailId;
	}

	public void setPermanentAddr1(String permanentAddr1) {
		this.permanentAddr1 = permanentAddr1;
	}
	public String getPermanentAddr1() {
		return this.permanentAddr1;
	}

	public void setPermanentAddr2(String permanentAddr2) {
		this.permanentAddr2 = permanentAddr2;
	}
	public String getPermanentAddr2() {
		return this.permanentAddr2;
	}

	public void setPermanentAddr3(String permanentAddr3) {
		this.permanentAddr3 = permanentAddr3;
	}
	public String getPermanentAddr3() {
		return this.permanentAddr3;
	}

	public void setMailingAddr1(String mailingAddr1) {
		this.mailingAddr1 = mailingAddr1;
	}
	public String getMailingAddr1() {
		return this.mailingAddr1;
	}

	public void setMailingAddr2(String mailingAddr2) {
		this.mailingAddr2 = mailingAddr2;
	}
	public String getMailingAddr2() {
		return this.mailingAddr2;
	}

	public void setMailingAddr3(String mailingAddr3) {
		this.mailingAddr3 = mailingAddr3;
	}
	public String getMailingAddr3() {
		return this.mailingAddr3;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getPan() {
		return this.pan;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPassportNumber() {
		return this.passportNumber;
	}

	public void setDoi(String doi) throws InvalidDataException { 
		try { 
			this.doi = dateFormat3.parse(doi);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("doi is blank or contains incorrect value.");
		}
	}
	public Date getDoi() {
		return this.doi;
	}

	public void setDob(String dob) throws InvalidDataException { 
		try { 
			this.dob = dateFormat3.parse(dob);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("dob is blank or contains incorrect value.");
		}
	}
	public Date getDob() {
		return this.dob;
	}

	public void setHandPhone(String handPhone) {
		this.handPhone = handPhone;
	}
	public String getHandPhone() {
		return this.handPhone;
	}

	public void setCompletePermanentAddr(String completePermanentAddr) {
		this.completePermanentAddr = completePermanentAddr;
	}
	public String getCompletePermanentAddr() {
		return this.completePermanentAddr;
	}

	public void setCompleteMailingAddr(String completeMailingAddr) {
		this.completeMailingAddr = completeMailingAddr;
	}
	public String getCompleteMailingAddr() {
		return this.completeMailingAddr;
	}

	public void setNameChanged(String nameChanged) {
		this.nameChanged = nameChanged;
	}
	public String getNameChanged() {
		return this.nameChanged;
	}

	public void setCompletePerAddrChanged(String completePerAddrChanged) {
		this.completePerAddrChanged = completePerAddrChanged;
	}
	public String getCompletePerAddrChanged() {
		return this.completePerAddrChanged;
	}

	public void setCompleteMailingAddrChanged(String completeMailingAddrChanged) {
		this.completeMailingAddrChanged = completeMailingAddrChanged;
	}
	public String getCompleteMailingAddrChanged() {
		return this.completeMailingAddrChanged;
	}

	public void setMobMatchedCount (int mobMatchedCount ) {
			this.mobMatchedCount  = mobMatchedCount;
	}
	public int getMobMatchedCount () {
		return this.mobMatchedCount ;
	}

	public void setLandlineMatchedCount(int landlineMatchedCount) {
			this.landlineMatchedCount = landlineMatchedCount;
	}
	public int getLandlineMatchedCount() {
		return this.landlineMatchedCount;
	}

	public void setOfficephoneMatchedCount(int officephoneMatchedCount) {
			this.officephoneMatchedCount = officephoneMatchedCount;
	}
	public int getOfficephoneMatchedCount() {
		return this.officephoneMatchedCount;
	}

	public void setEmailMatchedCount(int emailMatchedCount) {
			this.emailMatchedCount = emailMatchedCount;
	}
	public int getEmailMatchedCount() {
		return this.emailMatchedCount;
	}

	public void setPanMatchedCount(int panMatchedCount) {

			this.panMatchedCount = panMatchedCount;

	}
	public int getPanMatchedCount() {
		return this.panMatchedCount;
	}

	public void setPassportMatchedCount(int passportMatchedCount) {
			this.passportMatchedCount = passportMatchedCount;
	}
	public int getPassportMatchedCount() {
		return this.passportMatchedCount;
	}

	public void setMobNonStaffMatchedCount(int mobNonStaffMatchedCount) {
			this.mobNonStaffMatchedCount = mobNonStaffMatchedCount;
	}
	public int getMobNonStaffMatchedCount() {
		return this.mobNonStaffMatchedCount;
	}

	public void setCompletePerAddrCount(int completePerAddrCount) {
			this.completePerAddrCount = completePerAddrCount;
	}
	public int getCompletePerAddrCount() {
		return this.completePerAddrCount;
	}

	public void setCompleteMailingAddrCount(int completeMailingAddrCount) {
			this.completeMailingAddrCount = completeMailingAddrCount;
	}
	public int getCompleteMailingAddrCount() {
		return this.completeMailingAddrCount;
	}

	public void setPhoneCount(int phoneCount) {
			this.phoneCount = phoneCount;
	}

	public int getPhoneCount() {
		return this.phoneCount;
	}

	public void setMobCount(int mobCount) {
			this.mobCount = mobCount;
	}
	public int getMobCount() {
		return this.mobCount;
	}

	public void setIntermediateLandlineCount(int intermediateLandlineCount) {
			this.intermediateLandlineCount = intermediateLandlineCount;
	}
	public int getIntermediateLandlineCount() {
		return this.intermediateLandlineCount;
	}

	public void setIntermediateofficelandCount(int intermediateofficelandCount) {
			this.intermediateofficelandCount = intermediateofficelandCount;
	}
	public int getIntermediateofficelandCount() {
		return this.intermediateofficelandCount;
	}

	public void setIntermediatehandphoneCount(int intermediatehandphoneCount) {
			this.intermediatehandphoneCount = intermediatehandphoneCount;
	}
	public int getIntermediatehandphoneCount() {
		return this.intermediatehandphoneCount;
	}

	public void setScore(Double score) {
			this.score = score;
	}
	public Double getScore() {
		return this.score;
	}

	public void setScorePercentage (String scorePercentage ) {
		try { 
			this.scorePercentage  = Double.parseDouble(scorePercentage );
		} catch (Exception ex) {
			this.scorePercentage  = 0.0;
		}
	}
	public Double getScorePercentage () {
		return this.scorePercentage ;
	}

	public void setNameMatchScore (Double nameMatchScore ) {
			this.nameMatchScore  = nameMatchScore;
	}

	public Double getNameMatchScore () {
		return this.nameMatchScore ;
	}

	public void setPermanantAddrScore (Double permanantAddrScore ) {
			this.permanantAddrScore  = permanantAddrScore;
	}
	public Double getPermanantAddrScore () {
		return this.permanantAddrScore ;
	}

	public void setMailingAddrScore (Double mailingAddrScore ) {
			this.mailingAddrScore  = mailingAddrScore;
	}
	public Double getMailingAddrScore () {
		return this.mailingAddrScore ;
	}

	public void setDobMatchedCount(int dobMatchedCount) {
			this.dobMatchedCount = dobMatchedCount;
	}

	public int getDobMatchedCount() {
		return this.dobMatchedCount;
	}

	public void setDoiMatchedCount(int doiMatchedCount) {
			this.doiMatchedCount = doiMatchedCount;
	}
	
	public int getDoiMatchedCount() {
		return this.doiMatchedCount;
	}

	public void setAccountcount(String accountcount) {
		try { 
			this.accountcount = Integer.parseInt(accountcount);
		} catch (Exception ex) {
			this.accountcount = 0;
		}
	}
	public int getAccountcount() {
		return this.accountcount;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountId() {
		return this.accountId;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getProduct_code() {
		return this.product_code;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
		try { 
			this.sys_time = dateFormat1.parse(sys_time1);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("sys_time is blank or contains incorrect value.");
		}
	}
	public void setSys_time(Date sys_time) throws InvalidDataException {
		this.sys_time = sys_time;
	}
	public Date getSys_time() {
		return this.sys_time;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public  NFT_DedupEvent setDedupData() throws InvalidDataException {
		NFT_DedupEvent nft_dedupEvent  =  new NFT_DedupEvent();
		try {
			nft_dedupEvent.setCustName(this.getCustName());
		} catch (InvalidDataException e) {
			e.printStackTrace();
		}
		nft_dedupEvent.setEntityType(this.getEntityType());
		nft_dedupEvent.setCompletePermanentAddr(this.getCompletePermanentAddr());
		nft_dedupEvent.setCompleteMailingAddr(this.getCompleteMailingAddr());
		nft_dedupEvent.setCompleteMailingAddrChanged(this.getCompleteMailingAddrChanged());
		nft_dedupEvent.setCompletePerAddrChanged(this.getCompletePerAddrChanged());
		nft_dedupEvent.setNameChanged(this.getNameChanged());
		return  nft_dedupEvent;
	}
}
