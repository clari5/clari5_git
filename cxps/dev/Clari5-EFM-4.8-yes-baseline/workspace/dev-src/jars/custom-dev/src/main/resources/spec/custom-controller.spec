cxps.noesis.glossary.entity.custom-controller{
        db-name = CUSTOM_CONTROLLER
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
                
	       { name = server-id, column = server_id, type = "number:10", key=true }
               { name = last-ping-time, column = last_ping_time, type = timestamp }
 ]

}
