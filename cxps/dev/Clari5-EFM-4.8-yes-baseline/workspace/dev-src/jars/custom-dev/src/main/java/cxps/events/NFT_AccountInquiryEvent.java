package cxps.events;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;

import clari5.custom.Queries;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.dbcon.QueryMapper;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_AccountInquiryEvent extends Event {
	private String menu_id;
	private String acct_id;
	private String user_id;
	private String RM_Code_flag;
	private Double avl_bal;
	private String cust_id;
	private String branchid;
	private String branchiddesc;
	private String acct_status;
	private Date sys_time;
	private String host_id;
	private Date eventts;
	private String cust_type;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setMenu_id((String) msgMap.get("menu_id"));
		setAcct_id((String) msgMap.get("acct_id"));
		setUser_id((String) msgMap.get("user_id"));
		setRM_Code_flag((String) msgMap.get("RM_Code_flag"));
		setAvl_bal((String) msgMap.get("avl_bal"));
		setCust_id((String) msgMap.get("cust_id"));
		setBranchid((String) msgMap.get("branchid"));
		setBranchiddesc((String) msgMap.get("branchiddesc"));
		setAcct_status((String) msgMap.get("acct_status"));
		setSys_time1((String) msgMap.get("sys_time"));
		setHost_id((String) msgMap.get("host_id"));
		setEventts1((String) msgMap.get("eventts"));
		setCust_type((String) msgMap.get("cust_type"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_AccountInquiryEvent");
	//	Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
	//	CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAcct_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}

		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
                CxKeyHelper h = Hfdb.getCxKeyHelper();
                Connection con = null;
                PreparedStatement ps = null, ps1 = null, ps2 = null;
                ResultSet result = null, result1 = null, result2 = null;
                int count=0;
                try {
                        con = Rdbms.getAppConnection();
                        QueryMapper mapper = QueryMapper.getInstance(Rdbms.getAppDbType(), "clari5.custom");
                        //String userId = getUser_id();
                        String custId = getCust_id();
			if (custId != null && !"".equals(custId)) {
                                String loadcustType = mapper.get(Queries.SELECT_CUST_TYPE);
                                ps1 = con.prepareStatement(loadcustType);
                                ps1.setString(1, custId);
                                result1 = ps1.executeQuery();
                                String custType = "";
                                while (result1.next()) {
                                        custType = result1.getString("CUSTOMER_TYPE");
                                        String loadcustTyp = mapper.get(Queries.SELECT_CUST_FLG);
                                        ps2 = con.prepareStatement(loadcustTyp);
                                        ps2.setString(1, custType);
                                        result2 = ps2.executeQuery();
                                        if (result2.next()) {
                                                String flag = result2.getString("FLAG");
                                                setCust_type(flag);
                                        }
                                }
                        }
		} catch (Exception e) {
                       cxLog.error(e.getMessage());

                } finally {
                        try {
                                if (result != null) result.close();
                                if (ps != null) ps.close();
                                if (con != null) con.close();
                        } catch (Exception e) {
                        }
                }


		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		
		String cxUserKey = "";
                cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUser_id());
                WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
                wsInfoSet.add(wbr);	
                
        String cxNoncustKey = "";
        String hostNoncustkey=getUser_id()+getAcct_id();
		cxNoncustKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(),hostNoncustkey);
		WorkspaceInfo wbn = new WorkspaceInfo(Constants.KEY_REF_TYP_NONCUST,cxNoncustKey);
		wsInfoSet.add(wbn);


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		/*
		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		*/
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setMenu_id(String menu_id) throws InvalidDataException { 
		this.menu_id = menu_id;
	}
	public String getMenu_id() {
		return this.menu_id;
	}

	public void setAcct_id(String acct_id) throws InvalidDataException { 
		this.acct_id = acct_id;
	}
	public String getAcct_id() {
		return this.acct_id;
	}

	public void setUser_id(String user_id) throws InvalidDataException { 
		this.user_id = user_id;
	}
	public String getUser_id() {
		return this.user_id;
	}

	public void setRM_Code_flag(String RM_Code_flag) {
		this.RM_Code_flag = RM_Code_flag;
	}
	public String getRM_Code_flag() {
		return this.RM_Code_flag;
	}

	public void setAvl_bal(String avl_bal) {
		try { 
			this.avl_bal = Double.parseDouble(avl_bal);
		} catch (Exception ex) {
			this.avl_bal = 0.0;
		}
	}
	public Double getAvl_bal() {
		return this.avl_bal;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getCust_id() {
		return this.cust_id;
	}

	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
	public String getBranchid() {
		return this.branchid;
	}

	public void setBranchiddesc(String branchiddesc) {
		this.branchiddesc = branchiddesc;
	}
	public String getBranchiddesc() {
		return this.branchiddesc;
	}

	public void setAcct_status(String acct_status) {
		this.acct_status = acct_status;
	}
	public String getAcct_status() {
		return this.acct_status;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }
	public String getCust_type() {
                return cust_type;
        }

        public void setCust_type(String cust_type) {
                this.cust_type = cust_type;
        }

}
