package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_ChequeBookRequestEvent extends Event {
	private String Account_Id;
	private String Customer_ID;
	private String BeginCheqNumber;
	private String EndCheqNumber;
	private int No_of_Leav_isud;
	private Double avl_bal;
	private String System;
	private Date Issue_Date;
	private Date sys_time;
	private String host_id;
	private Date eventts;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setAccount_Id((String) msgMap.get("Account_Id"));
		setCustomer_ID((String) msgMap.get("Customer_ID"));
		setBeginCheqNumber((String) msgMap.get("BeginCheqNumber"));
		setEndCheqNumber((String) msgMap.get("EndCheqNumber"));
		setNo_of_Leav_isud((String) msgMap.get("No_of_Leav_isud"));
		setAvl_bal((String) msgMap.get("avl_bal"));
		setSystem((String) msgMap.get("System"));
		setIssue_Date((String) msgMap.get("Issue_Date"));
		setSys_time1((String) msgMap.get("sys_time"));
		setHost_id((String) msgMap.get("host_id"));
		setEventts1((String) msgMap.get("eventts"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_ChequeBookRequestEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAccount_Id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		/*
		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		*/
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setAccount_Id(String Account_Id) throws InvalidDataException { 
		this.Account_Id = Account_Id;
	}
	public String getAccount_Id() {
		return this.Account_Id;
	}

	public void setCustomer_ID(String Customer_ID) throws InvalidDataException { 
		this.Customer_ID = Customer_ID;
	}
	public String getCustomer_ID() {
		return this.Customer_ID;
	}

	public void setBeginCheqNumber(String BeginCheqNumber) throws InvalidDataException { 
		this.BeginCheqNumber = BeginCheqNumber;
	}
	public String getBeginCheqNumber() {
		return this.BeginCheqNumber;
	}

	public void setEndCheqNumber(String EndCheqNumber) throws InvalidDataException { 
		this.EndCheqNumber = EndCheqNumber;
	}
	public String getEndCheqNumber() {
		return this.EndCheqNumber;
	}

	public void setNo_of_Leav_isud(String No_of_Leav_isud) throws InvalidDataException { 
		try { 
			this.No_of_Leav_isud = Integer.parseInt(No_of_Leav_isud);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("No_of_Leav_isud is blank or contains incorrect value.");
		}
	}
	public int getNo_of_Leav_isud() {
		return this.No_of_Leav_isud;
	}

	public void setAvl_bal(String avl_bal) throws InvalidDataException { 
		try { 
			this.avl_bal = Double.parseDouble(avl_bal);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("avl_bal is blank or contains incorrect value.");
		}
	}
	public Double getAvl_bal() {
		return this.avl_bal;
	}

	public void setSystem(String System) throws InvalidDataException { 
		this.System = System;
	}
	public String getSystem() {
		return this.System;
	}

	public void setIssue_Date(String Issue_Date) throws InvalidDataException { 
		try { 
			this.Issue_Date = dateFormat1.parse(Issue_Date);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("Issue_Date is blank or contains incorrect value.");
		}
	}
	public Date getIssue_Date() {
		return this.Issue_Date;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }

}
