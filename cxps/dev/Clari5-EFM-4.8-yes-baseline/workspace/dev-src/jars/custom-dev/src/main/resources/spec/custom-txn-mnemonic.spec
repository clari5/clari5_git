cxps.noesis.glossary.entity.custom-txn-mnemonic{
        db-name = CUSTOM_TXN_MNEMONIC
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = cod-txn-mnemonic, column = cod_txn_mnemonic, type = "number:10"}
		{ name = cod-txn-literal, column = cod_txn_literal, type = "string:50"}
		{ name = txt-txn-desc, column = txt_txn_desc, type = "string:50"}
		{ name = transaction-mode, column = transaction_mode, type = "string:50"}
		{ name = sub-tran-mode, column = sub_tran_mode, type = "string:50"}
		{ name = transaction-sub-type, column = transaction_sub_type, type = "string:50"}
		{ name = transaction-channel, column = transaction_channel, type = "string:50"}
		{ name = cod-dr-cr, column = cod_dr_cr, type = "string:50"}
		{ name = user-id, column = user_id, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
