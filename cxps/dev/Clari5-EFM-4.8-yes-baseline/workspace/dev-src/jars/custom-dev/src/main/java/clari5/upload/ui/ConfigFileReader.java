package clari5.upload.ui;


import java.io.IOException;
import java.util.Properties;

import static cxps.apex.utils.FileUtils.loadFromProperties;

/**
 * Created by didhin
 Date :  8/4/16.
 */
class ConfigFileReader {
    private String fileBackUpPath = null;
    Properties props = null;

    /*
     * Following code reads the configuration files 
     * upload-ui-clari5.conf (contains backup path) and 
     * upload-rdbms.conf(contains db configuration)
     */
    public ConfigFileReader(String fileName) {
    	try{
        if(fileName.equalsIgnoreCase("upload-ui-clari5.conf"));
        {
            props = loadFromProperties(fileName);   
            fileBackUpPath = props.getProperty("file-backup-path");
        }
    	}
        catch(IOException e){

        	}
    	
    }
    public String getFileBackUpPath() {
        return fileBackUpPath;
    }
}
