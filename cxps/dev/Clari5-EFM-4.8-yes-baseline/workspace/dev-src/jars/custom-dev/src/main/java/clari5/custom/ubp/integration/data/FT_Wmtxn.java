package clari5.custom.ubp.integration.data;

public class FT_Wmtxn extends ITableData {
    private String tableName = "FT_WMTXN";
    private String event_type = "FT_Wmtxn";
    private String   EVENT_ID;
    private String   tran_type;
    private String   trade_date;
    private String   acctid;
    private String   wms_ref_no;
    private String   amount;
    private String   scheme_type;
    private String   tran_code;
    private String   sub_tran_type;
    private String   eventts;
    private String   host_id;
    private String   cust_Id;
    private String   txn_ref_no;
    private String   scheme_name;
    private String   sys_time;
    private String rm_code;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTrade_date() {
        return trade_date;
    }

    public void setTrade_date(String trade_date) {
        this.trade_date = trade_date;
    }

    public String getAcctid() {
        return acctid;
    }

    public void setAcctid(String acctid) {
        this.acctid = acctid;
    }

    public String getWms_ref_no() {
        return wms_ref_no;
    }

    public void setWms_ref_no(String wms_ref_no) {
        this.wms_ref_no = wms_ref_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getScheme_type() {
        return scheme_type;
    }

    public void setScheme_type(String scheme_type) {
        this.scheme_type = scheme_type;
    }

    public String getTran_code() {
        return tran_code;
    }

    public void setTran_code(String tran_code) {
        this.tran_code = tran_code;
    }

    public String getSub_tran_type() {
        return sub_tran_type;
    }

    public void setSub_tran_type(String sub_tran_type) {
        this.sub_tran_type = sub_tran_type;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getCust_Id() {
        return cust_Id;
    }

    public void setCust_Id(String cust_Id) {
        this.cust_Id = cust_Id;
    }

    public String getTxn_ref_no() {
        return txn_ref_no;
    }

    public void setTxn_ref_no(String txn_ref_no) {
        this.txn_ref_no = txn_ref_no;
    }

    public String getScheme_name() {
        return scheme_name;
    }

    public void setScheme_name(String scheme_name) {
        this.scheme_name = scheme_name;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getRm_code() {
        return rm_code;
    }

    public void setRm_code(String rm_code) {
        this.rm_code = rm_code;
    }
}

