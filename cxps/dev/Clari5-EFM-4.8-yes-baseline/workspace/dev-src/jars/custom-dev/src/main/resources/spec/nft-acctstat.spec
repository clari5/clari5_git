cxps.noesis.glossary.entity.nft-acctstat{
        db-name = NFT_ACCTSTAT
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [

               { name = host-id, column = host_id, type = "string:50"}
	       { name = account-id, column = account_id, type = "string:50"}
	       { name = avl-bal, column = avl_bal, type = "number:30"}
	       { name = init-acct-status, column = init_acct_status, type = "string:50"}
	       { name = user-id, column = user_id, type = "string:50"}
	       { name = branch-id, column = branch_id, type = "string:50"}
	       { name = final-acct-status, column = final_acct_status, type = "string:50"}
	       { name = branch-id-desc, column = branch_id_desc, type = "string:50"}
	       { name = cust-id, column = cust_id, type = "string:50"}
	       { name = sys-time, column = sys_time, type = timestamp}
	       { name = eventts, column = eventts, type = timestamp}
	       { name = acct-open-dt, column = acct_open_dt, type = timestamp}
	       { name = sync-status, column = sync_status, type = "string:50"}
	       { name = insertion-time, column = insertion_time, type = timestamp}
	       { name = server-id, column = server_id, type = "number:30"}
	      
 ]

}
