cxps.noesis.glossary.entity.prof-code-desc{
        db-name = PROF_CODE_DESC
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = prof-code, column = prof_code, type = "string:50", key=true }
		{ name = prof-desc, column = prof_desc, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
