package cxps.events;


import clari5.custom.Queries;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import clari5.platform.dbcon.QueryMapper;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.rdbms.Rdbms;
import org.omg.CORBA.DATA_CONVERSION;

public class DbQueries {

    PreparedStatement ps, ps1, ps2;
    ResultSet result, result1, result2;
    int dataFetched = 0;
    int data[] = new int[3];
    QueryMapper mapper = QueryMapper.getInstance(Rdbms.getAppDbType(), "clari5.custom");
    ICXLog cxLog = CXLog.fenter("derive.NFT_DedupEvent");
/*
     getMobiledata method fetches the count of ucic id where the mobile from the json is  matched with the
     CUSTOMER_MASTER based on the following three criteria.
     1.SELECT_MOB_STAFF -matches the last 10 digits of the mobile number with
         last 10 digits of either mobile or phone residence or office phone of staff
     2.SELECT_MOB_NONSTAFF-matches the last 10 digits of the mobile number with
         last 10 digits of either mobile or phone residence or office phone of non-staff
     3.SELECT_MOB - matches the last 10 digits of the mobile number with
         last 10 digits of  mobile
 */

    public int[] getMobiledata(Connection con, String mobile, String ucic) {

        try {

            String mobileNo = mobile.trim().substring((mobile.length() - 10), mobile.length());
            String mobStaff = mapper.get(Queries.SELECT_MOB_STAFF);
            String mobNonStaff = mapper.get(Queries.SELECT_MOB_NONSTAFF);
            String mobile1 = mapper.get(Queries.SELECT_MOB);
            ps = con.prepareStatement(mobStaff);
            ps1 = con.prepareStatement(mobNonStaff);
            ps2 = con.prepareStatement(mobile1);
            ps.setString(1, ucic);
            ps.setString(2, mobileNo);
            ps.setString(3, mobileNo);
            ps.setString(4, mobileNo);
            ps1.setString(1, ucic);
            ps1.setString(2, mobileNo);
            ps1.setString(3, mobileNo);
            ps1.setString(4, mobileNo);
            ps2.setString(2, mobileNo);
            ps2.setString(1, ucic);
            result = ps.executeQuery();
            result1 = ps1.executeQuery();
            result2 = ps2.executeQuery();
            while (result.next()) {
                data[0] = result.getInt(1);
            }
            while (result1.next()) {
                data[1] = result1.getInt(1);
            }
            while (result2.next()) {
                data[2] = result2.getInt(1);
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null || result1 != null || result2 != null) {
                    result.close();
                    result1.close();
                    result2.close();
                }
                if (ps != null || ps1 != null || ps2 != null) {
                    ps.close();
                    ps1.close();
                    ps2.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
        }
        return data;
    }
    /*
    gethomeLandline method fetches the count of ucic id where the homelandline from the json is
     matched with the CUSTOMER_MASTER based on the following  criteria.
     1.SELECT_LANDLINE- matches the last 8 digit of homelandline with phone residence or
     phone office

     2.SELECT_MATCH_PHONE- matches the last 8 digit of homelandline with phone office ,phone residence or
     hand phone
     */

    public int[] gethomeLandline(Connection con, String homeland, String ucic) {
        String homelandline = homeland.trim().substring((homeland.length() - 8), homeland.length());
        String homelandLine = mapper.get(Queries.SELECT_LANDLINE);
        String matchPhone =mapper.get(Queries.SELECT_MATCH_PHONE);
        try {
            ps = con.prepareStatement(homelandLine);
            ps1 = con.prepareStatement(matchPhone);
            ps.setString(1, ucic);
            ps.setString(2, homelandline);
            ps.setString(3, homelandline);
            ps1.setString(1,homelandline);
            ps1.setString(2,homelandline);
            ps1.setString(3,homelandline);
            ps1.setString(4,ucic);
            result = ps.executeQuery();
            result1=ps1.executeQuery();
            while (result.next()) {
                data[0] = result.getInt(1);
            }
            while (result1.next()) {
                data[1] = result1.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                    result1.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();
                    ps1.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return data;
        }
    }
    /*
    getofficeLandline method fetches the count of ucic id where the officeLandline from the json is
     matched with the CUSTOMER_MASTER based on the following  criteria.

    1.SELECT_LANDLINE - matches the last 8 digit of officeLand with phone residence or
     phone office
    2.SELECT_MATCH_PHONE- matches the last 8 digit of homelandline with phone office ,phone residence or
     hand phone

     */

    public int[] getofficeLandline(Connection con, String officeLandline, String ucic) {
        String officeLand = officeLandline.trim().substring((officeLandline.length() - 8), officeLandline.length());
        String officelandLine = mapper.get(Queries.SELECT_LANDLINE);
        String matchPhone =mapper.get(Queries.SELECT_MATCH_PHONE);
        try {
            ps = con.prepareStatement(officelandLine);
            ps1 = con.prepareStatement(matchPhone);
            ps.setString(1, ucic);
            ps.setString(2, officeLand);
            ps.setString(3, officeLand);
            ps1.setString(1,officeLand);
            ps1.setString(2,officeLand);
            ps1.setString(3,officeLand);
            ps1.setString(4,ucic);
            result = ps.executeQuery();
            while (result.next()) {
                data[0] = result.getInt(1);
            }
            while (result1.next()) {
                data[1] = result1.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                    result1.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();
                    ps1.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return data;
        }
    }
    /*
  getemail method fetches the count of ucic id where the email from the json is
     matched with the email of the CUSTOMER_MASTER table

     */

    public int getemail(Connection con, String email, String ucic) {
        String emailId = mapper.get(Queries.SELECT_EMAIL);
        try {
            ps = con.prepareStatement(emailId);
            ps.setString(1, ucic);
            ps.setString(2, email);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return dataFetched;
        }
    }

    /*
    getpan method fetches the count of ucic id where the pan from the json is
     matched with the pan of the CUSTOMER_MASTER table
     */
    public int getpan(Connection con, String pan, String ucic) {
        String panNum = mapper.get(Queries.SELECT_PAN);
        try {
            ps = con.prepareStatement(panNum);
            ps.setString(1, ucic);
            ps.setString(2, pan);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return dataFetched;
        }
    }
    /*
     getpassport method fetches the count of ucic id where the passport from the json is
     matched with the passport of the CUSTOMER_MASTER table
     */

    public int getpassport(Connection con, String passport, String ucic) {
        String passtortNum = mapper.get(Queries.SELECT_PASSPORT);
        try {
            ps = con.prepareStatement(passtortNum);
            ps.setString(1, ucic);
            ps.setString(2, passport);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return dataFetched;
        }
    }
    /*
    getdob method fetches the count of ucic id where the dob from the json is
     matched with the dob of the CUSTOMER_MASTER table
     */

    public int getdob(Connection con, Date dob, String ucic) {
        String dob1 = mapper.get(Queries.SELECT_DOB);
        java.sql.Date dob2 = new java.sql.Date(dob.getTime());
        try {
            ps = con.prepareStatement(dob1);
            ps.setDate(1, dob2);
            ps.setString(2,ucic);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return dataFetched;
        }
    }

    /*
   getdoi method fetches the count of ucic id where the doi from the json is
    matched with the doi of the CUSTOMER_MASTER table
    */
    public int getdoi(Connection con, Date doi, String ucic) {
        String doi1 =mapper.get(Queries.SELECT_DOI);
        java.sql.Date doi2 = new java.sql.Date(doi.getTime());
        try {
            ps=con.prepareStatement(doi1);
            ps.setDate(1,doi2);
            ps.setString(2,ucic);
            result=ps.executeQuery();
            while (result.next()){
                dataFetched=result.getInt(1);
            }
        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return dataFetched;
        }
    }

    /*
    gethandphone  method fetches the count of ucic id where the handphone from the json is
     matched with the CUSTOMER_MASTER based on the following  criteria.
     1.SELECT_MATCH_PHONE- matches the last 8 digit of handphone with phone office ,phone residence or
     hand phone


     */
    public int gethandphone(Connection con, String handphone, String ucic) {
        String handPhone = handphone.trim().substring((handphone.length() - 8), handphone.length());

        String matchPhone =mapper.get(Queries.SELECT_MATCH_PHONE);
        try {
            ps = con.prepareStatement(matchPhone);
            ps.setString(1, handPhone);
            ps.setString(2, handPhone);
            ps.setString(3, handPhone);
            ps.setString(4,ucic);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }

        } catch (Exception e) {
            cxLog.error(e.getMessage());
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();

                }

            } catch (Exception e) {
                cxLog.error(e.getMessage());
            }
            return dataFetched;
        }
    }
}