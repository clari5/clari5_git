package clari5.custom.ubp.integration.data;

public class NFT_Accountinquiry extends ITableData {
    private String tableName = "NFT_ACCTINQUIRY";
    private String event_type = "NFT_Accountinquiry";
    private String EVENT_ID;
    private String  branchid;
    private String  acct_id;
    private String  avl_bal;
    private String  branchiddesc;
    private String  user_id;
    private String  acct_status;
    private String  sys_time;
    private String   rm_code_flag;


    private String   cust_id;
    private String  eventts;
    private String  menu_id;
    private String  host_id;

    public String getRm_code_flag() {
        return rm_code_flag;
    }

    public void setRm_code_flag(String rm_code_flag) {
        this.rm_code_flag = rm_code_flag;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getAcct_id() {
        return acct_id;
    }

    public void setAcct_id(String acct_id) {
        this.acct_id = acct_id;
    }

    public String getAvl_bal() {
        return avl_bal;
    }

    public void setAvl_bal(String avl_bal) {
        this.avl_bal = avl_bal;
    }

    public String getBranchiddesc() {
        return branchiddesc;
    }

    public void setBranchiddesc(String branchiddesc) {
        this.branchiddesc = branchiddesc;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAcct_status() {
        return acct_status;
    }

    public void setAcct_status(String acct_status) {
        this.acct_status = acct_status;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }





    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }
}
