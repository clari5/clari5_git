cxps.noesis.glossary.entity.ft-wmtxn{
        db-name = FT_WMTXN
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
                
	       { name = tran-type, column = tran_type, type = "string:50"}
	       { name = trade-date, column = trade_date, type = "string:50"}
	       { name = acctid, column = acctid, type = "string:50"}
	       { name = wms-ref-no, column = wms_ref_no, type = "string:50"}
               { name = amount, column = amount, type = "number:30"}
	       { name = scheme-type, column = scheme_type, type = "string:50"}
	       { name = tran-code, column = tran_code, type = "string:50"}
	       { name = sub-tran-type, column = sub_tran_type, type = "string:50"}
	       { name = host-id, column = host_id, type = "string:50"}
	       { name = cust-id, column = cust_id, type = "string:50"}
	       { name = txn-ref-no, column = txn_ref_no, type = "string:50"}
	       { name = scheme-name, column = scheme_name, type = "string:50"}
	       { name = rm-code, column = rm_code, type = "string:50"}
	       { name = sys-time, column = sys_time, type = timestamp}
	       { name = eventts, column = eventts, type = timestamp}
	       { name = sync-status, column = sync_status, type = "string:50"}
	       { name = server-id, column = server_id, type = "number:30"}
	      
 ]

}
