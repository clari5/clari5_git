package cxps.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import clari5.rdbms.Rdbms;

import cxps.events.CustomException;

/**
 * Created by vijay on 25/9/17.
 */
public class UploadService {

    private static final String COMMA_DELIMITER = ",";
/*
    Properties props = new Properties();
    FileInputStream in = new FileInputStream("/home/vikash/cxps/dev/Clari5-EFM-4.8-yes-baseline/workspace/bin/releases/48.C8P1b20182901/data/dev-src/jars/custom-dev/src/main/resources/custom-db.conf");

    String driver = props.getProperty("jdbc.driver");
    String url = props.getProperty("jdbc.url");
    String username = props.getProperty("jdbc.username");
    String password = props.getProperty("jdbc.password");

    Connection con = DriverManager.getConnection(url, username, password);*/

   /* static String DRIVER = "oracle.jdbc.driver.OracleDriver";
    static String CONNECTION_URL = "jdbc:oracle:thin:@192.168.5.37:1521:cxps12c";//AMLoginConfigManager.getInstance().getCONNECTION_URL();
    static String USERNAME = "cxpsadm_VIKASH1_48dev";//AMLoginConfigManager.getInstance().getUSERNAME();
    static String PASSWORD = "c_xps123";//AMLoginConfigManager.getInstance().getPASSWORD();
*/
    void readCSVFile(String fileName, HttpServletResponse response, HttpServletRequest request) throws CustomException, IOException {
        BufferedReader fileReader = null;
        int finalCount = 0;
        int totalCount = 0;
        //long seqId = getSequenceId();
        Date lastUpdated1 = new Date();
        Timestamp timestamp1 = new Timestamp(lastUpdated1.getTime());

        try {
            String line = "";
            int j = 0;
            fileReader = new BufferedReader(new FileReader(fileName));
            String headers = fileReader.readLine();
            System.out.println(headers);
            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(COMMA_DELIMITER);
                int inputLength = tokens.length;
                totalCount++;
                System.out.println("Length of the records : " + inputLength);
                if (inputLength >= 18) {
                    Date lastUpdated = new Date();
                    Timestamp timestamp = new Timestamp(lastUpdated.getTime());
                    doInsert(tokens);
                    boolean b = true;//insertIntoDb(jiraEvent);
                    if (b) {
                        finalCount++;
                    }
                } else {
                    System.out.println("not 22");
                    try {
                        throw new CustomException("Uploaded file is not in format. Please upload formated file.");
                    } catch (Exception e) {
                        System.out.println("Error in Customer Exception!!!");
                        e.printStackTrace();
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } finally {
            Date lastUpdated2 = new Date();
            Timestamp timestamp2 = new Timestamp(lastUpdated2.getTime());
            //doAudit(seqId, timestamp1, timestamp2, totalCount, finalCount, request);
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
                e.printStackTrace();
            }
        }
    }



    public long getSequenceId() {
        long myId = 0L;
        Connection con = null;
        PreparedStatement pst = null;
        try {
            try {

               // Class.forName(DRIVER);
              //  con = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
                 con = Rdbms.getAppConnection();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error in Connection Provider");
            }
            String sqlIdentifier = "select EVENT_UPLOAD_SEQUENCE.nextval from dual";
            pst = con.prepareStatement(sqlIdentifier);
            ResultSet rs1 = pst.executeQuery();
            if (rs1.next()) {
                myId = rs1.getLong(1);
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                }
            }
        }
        return myId;
    }


    public long getJiraSequenceId() {
        long myId = 0L;
        Connection con = null;
        PreparedStatement pst = null;
        try {
            try {
               /* Class.forName(DRIVER);
                con = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);*/
                con = Rdbms.getAppConnection();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error in Connection Provider");
            }
            String sqlIdentifier = "select JIRA_EVENT_SEQUENCE.nextval from dual";
            pst = con.prepareStatement(sqlIdentifier);
            ResultSet rs1 = pst.executeQuery();
            if (rs1.next()) {
                myId = rs1.getLong(1);
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                }
            }
        }
        return myId;
    }


    private void doInsert(String[] token) {
        Connection con = null;
        PreparedStatement pst = null;
        try {
            try {
            /*    Class.forName(DRIVER);
                con = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);*/
                con = Rdbms.getAppConnection();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error in Connection Provider");
            }
            String sql = "INSERT INTO BRANCH_MASTER1 (\"BRCODE\",\"BRNAME\",\"CATEGORY\",\"CLUSTER_NAME\",\"REGION\",\"STATE\",\"CITY\",\"BRANCH_CLASSIFICATION\",\"DBBL_NAME\",\"BBL_NAME\",\"CSDL_NAME\",\"CBL_NAME\",\"RSDL_NAME\",\"SR_RBL_NAME\",\"BBH_NAME\",\"RBL_NAME\",\"RCRE_TIME\",\"RCRE_USER\")  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            pst = con.prepareStatement(sql);
            Date lastUpdated2 = new Date();
            Timestamp timestamp2 = new Timestamp(lastUpdated2.getTime());
            pst.setString(1, token[0]);
            pst.setString(2, token[1]);
            pst.setString(3, token[2]);
            pst.setString(4, token[3]);
            pst.setString(5, token[4]);
            pst.setString(6, token[5]);
            pst.setString(7, token[6]);
            pst.setString(8, token[7]);
            pst.setString(9, token[8]);
            pst.setString(10, token[9]);
            pst.setString(11, token[10]);
            pst.setString(12, token[11]);
            pst.setString(13, token[12]);
            pst.setString(14, token[13]);
            pst.setString(15, token[14]);
            pst.setString(16, token[15]);
           // pst.setString(17, token[16]);
            pst.setTimestamp(17, timestamp2);
            pst.setString(18, token[17]);


            int updateValue = pst.executeUpdate();
            if (updateValue > 0) {
                System.out.println("Succesfully Inserted to Upload Event.");
            } else {
                System.out.println("No Result found.");
            }
            con.commit();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                }
            }
        }
    }



    private void doAudit(long seqId, Timestamp initUpdated, Timestamp lastUpdate, int totalCount, int finalCount, HttpServletRequest request) {
        Connection con = null;
        PreparedStatement pst = null;
        try {
            try {
         /*       Class.forName(DRIVER);
                con = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);*/
                con = Rdbms.getAppConnection();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error in Connection Provider");
            }
            String sql = "INSERT INTO UPLOAD_EVENT (\"eventId\",\"CREATED_ON\", \"COMPLETED_ON\" ,\"rowFound\", \"rowInserted\",\"userId\",\"alertCreated\")  VALUES (?,?,?,?,?,?,?)";
            pst = con.prepareStatement(sql);
            pst.setLong(1, seqId);
            pst.setTimestamp(2, initUpdated);
            pst.setTimestamp(3, lastUpdate);
            pst.setInt(4, totalCount);
            pst.setInt(5, finalCount);
            String user = (String) request.getSession().getAttribute("LAST_USERNAME");
            System.out.println("User : "+ user);
            pst.setString(6, user);
            pst.setInt(7, 0);
            int updateValue = pst.executeUpdate();
            if (updateValue > 0) {
                System.out.println("Succesfully Inserted to Upload Event.");
            }
            con.commit();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    private long doStringToLong(String s) {
        try {
            double value = Double.parseDouble(s);
            long l = (new Double(value)).longValue();
            return l;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


}
