package clari5.custom.ubp.integration.config;

import clari5.custom.ubp.integration.audit.LogLevel;
import clari5.custom.ubp.integration.exceptions.ConfigurationException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.HashMap;
import java.util.Set;

public class BepCon extends Properties {
    private static final long serialVersionUID = -8967877852117047914L;
    private static BepCon config;
    private String[] orderOfTable;
    private String logPath;
    private LogLevel logLevel;
    private int noOfConsumers = 1;
    private String dbURL;
    private String usr;
    private String pwd;
    private int simultaneousRowCount;
    private String clari5URL;
    private String qname;
    private HashMap<String,String> tableClassMap=new HashMap<String,String>();
    public static Map<String,String> tableSelectCols = new HashMap<String,String>();
    // Debug
    private String filedebugoutputpath;

    private BepCon() throws Exception {
        try {
            load(Thread.currentThread().getContextClassLoader().getResourceAsStream("BatchEventProcessorConfig.properties"));
            this.orderOfTable = this.getProperty("Table_Processing_Order").split(",");
            String tableMap=this.getProperty("table_to_class");
            String []tableArr=tableMap.split(",");

            for(String key:tableArr){
                String KeyValue[]=key.split(":");
                   this.tableClassMap.put(KeyValue[0],KeyValue[1]);
            }
           // this.logPath = this.getProperty("logPath");
            //this.logLevel = LogLevel.getLogLevel(this.getProperty("logLevel", "3").trim());
           // this.noOfConsumers = Integer.parseInt(this.getProperty("NumberOfConsumers").trim());
           /* if (this.noOfConsumers > 5) {
                this.noOfConsumers = 5;
            } else if (this.noOfConsumers < 1) {
                throw new Exception("Exception: Number of threads should be between 1 and 5");
            }*/
           // this.dbURL = this.getProperty("url");
            //this.usr = this.getProperty("usr");
           // this.pwd = this.getProperty("pwd");
           // this.simultaneousRowCount = Integer.parseInt(this.getProperty("simultaneousrowcount"));
            //this.setClari5URL(this.getProperty("clari5URL"));
           // this.setQname(this.getProperty("qname"));
            this.filedebugoutputpath = this.getProperty("filedebugoutputpath");
            Set<Object> keys = this.keySet();
            
            for(Object key : keys) {
                String tableName,fields;
                if(key != null && key.toString().startsWith("FIELDLIST.")){
                    tableName = key.toString().split("\\.")[1];
                    fields = getProperty(key.toString());
                    tableSelectCols.put(tableName,fields);
                }
            }
            
        } catch (FileNotFoundException fnfe) {
            throw new ConfigurationException("The configuration file BatchEventProcessorConfig is not found --> " + fnfe.getMessage());
        } catch (IOException ioe) {
            throw new ConfigurationException("The configuration file BatchEventProcessorConfig could not be accessed --> " + ioe.getMessage());
        } catch (Exception e) {
            throw new ConfigurationException("Exception caught --> " + e.getMessage());
        }
    }

    public HashMap<String, String> getTableClassMap() {
        return tableClassMap;
    }

    public void setTableClassMap(HashMap<String, String> tableClassMap) {
        this.tableClassMap = tableClassMap;
    }

    public String[] getOrderOfTableProc() {
        return this.orderOfTable;
    }

    public String getLogPath() {
        return this.logPath;
    }

    public LogLevel getLogLevel() {
        return this.logLevel;
    }

    public int getnoOfConsumers() {
        return this.noOfConsumers;
    }

    public String getDbURL() {
        return dbURL;
    }

    public String getUsr() {
        return usr;
    }

    public String getPwd() {
        return pwd;
    }

    public int getSimultaneousRowCount() {
        return simultaneousRowCount;
    }

    public String getFiledebugoutputpath() {
        return filedebugoutputpath;
    }

    public String getClari5URL() {
        return clari5URL;
    }

    public String getQname() {
        return qname;
    }

    public static BepCon getConfig() throws Exception {
        if (config == null) {
            config = new BepCon();
        }

        return config;
    }

    public static void main(String[] args) {
        try {
            System.out.println(BepCon.getConfig().noOfConsumers);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setClari5URL(String clari5URL) {
        this.clari5URL = clari5URL;
    }

    public void setQname(String qname) {
        this.qname = qname;
    }
}