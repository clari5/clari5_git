package cxps.events;

import cxps.apex.utils.CxpsLogger;

import java.io.IOException;
import java.util.Properties;

import static cxps.apex.utils.FileUtils.loadFromProperties;

public class ConfigFileReader {

    private static final CxpsLogger logger = CxpsLogger.getLogger(ConfigFileReader.class);

    private String fileBackUpPath = null;
    Properties props = null;
    private String loggerPath = null;

    /*
     * Following code reads the configuration files 
     * bulkupload-clari5.conf (contains backup path)
     */
    public ConfigFileReader(String fileName) {
        logger.info("Start reading config file called bulkupload-clari5.conf");
        try {
            if (fileName.equalsIgnoreCase("bulkupload-clari5.conf")) {
                props = loadFromProperties(fileName);
                fileBackUpPath = props.getProperty("file-backup-path");
            }
        } catch (IOException e) {
            logger.error("Error while reading config file called bulkupload-clari5.conf");
            e.printStackTrace();
        }
    }

    public String getFileBackUpPath() {
        return fileBackUpPath;
    }

    public String getLoggerPath() {
        return loggerPath;
    }
}
