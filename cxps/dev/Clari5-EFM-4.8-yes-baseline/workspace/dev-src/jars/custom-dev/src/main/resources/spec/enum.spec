clari5.custom.enum {
    queries {
        values = [
            { code : SELECT_CUST }
            { code : SELECT_MOB_STAFF }
            { code : SELECT_TYPE }
            { code : SELECT_BRANCH}
            { code : SELECT_MOB_NONSTAFF}
            {code :SELECT_LANDLINE}
            {code :SELECT_EMAIL}
            {code :SELECT_PAN}
            {code :SELECT_PASSPORT}
            {code :SELECT_CUST_TYPE}
            {code :SELECT_CUST_FLG}
	        {code :SELECT_DOB}
	        {code :SELECT_DOI}
            {code :SELECT_MATCH_PHONE}
            {code :SELECT_MOB}
	        {code :SELECT_ACCOUNTCOUNT}
        ]
    }
}




