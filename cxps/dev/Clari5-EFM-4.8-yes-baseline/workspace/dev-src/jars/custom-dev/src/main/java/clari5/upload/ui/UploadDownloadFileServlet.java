package clari5.upload.ui;

import cxps.apex.utils.CxpsLogger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import clari5.rdbms.Rdbms;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.text.SimpleDateFormat;

@WebServlet("/UploadDownloadFileServlet")
public class UploadDownloadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String status;
    private String timeStamp = null;
    private int numberOfColumnsDB = 0;
    private String insertSql = null;
    private int rowNum = 0;
    private int columnNum = 0;
    static int count=0;
    private String requestFileName = null;
    private  String fileBackupPath=null;
    private String requestFileNameSplit=null;
    Map<Integer, String> hmap= new HashMap<Integer, String>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request,response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String requestTableName;
        HttpSession session;
        ResultSetMetaData rsData;
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
        
        
        
        session = request.getSession();
        String userId = (String)session.getAttribute("userId");
        requestTableName = (String) session.getAttribute("myTable");

        fileBackupPath=(String) session.getAttribute("FilePath");
        requestFileName=(String) session.getAttribute("FileName");

          try{
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement("Delete from "+requestTableName);
            ps.executeQuery();
            con.commit();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
            try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}
        }

         
        //getting the count of columns of table
        try{
           // con =DBConnection.getDBConnection();
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement("Select * from "+requestTableName);
            rs = ps.executeQuery();
            rsData = rs.getMetaData();
            numberOfColumnsDB = rsData.getColumnCount();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
            try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}
            try { if (rs != null) rs.close();}catch (SQLException ex){ ex.printStackTrace();}
        }
     
/*        if (session == null || session.getAttribute("USERID")== null){
    		try {
    			request.getRequestDispatcher("/expiry.jsp").forward(request,
    				    response);
    		} catch (ServletException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}*/
        
        	// preparing insert query as per column size
        switch (numberOfColumnsDB)  {
            case 20 : insertSql = "INSERT  INTO "+requestTableName+" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,systimestamp,'"+userId+"',systimestamp,'"+userId+"')";
            break;
            default:
                System.out.println( "insertion failed  into "+ requestTableName+ " \n Query is ["+insertSql +"]"); break;
        }
        
        //reading the type of file (xls,xlsx)
        int s = fileBackupPath.lastIndexOf('.');
        if (s>= 0) 
        	requestFileNameSplit = fileBackupPath.substring(s+1);
        
      
        
        //calling the respective insert function based on file type
         if (requestFileNameSplit.equalsIgnoreCase("xlsx")) {
             count = 0;
             xlsxFileInsert(requestTableName, response);
         }
        else if (requestFileNameSplit.equalsIgnoreCase("xls")) {
             count =0;
        	xlsFileInsert(requestTableName,response);
            }
         
        
        //update the audit table 
        UploadUiAudit.updateAudit(requestTableName,userId,status,"",requestFileName,"Insert");
        request.setAttribute("TOTAL",count);   
        request.getRequestDispatcher("/DisplayInsert").include(request, response);
        }
    
    
    
	public void xlsFileInsert(String requestTableName,HttpServletResponse response)   {
		
        HSSFWorkbook wb;
        HSSFSheet  ws;
        BigInteger temp=null;
        int error = 0;
        try {
		PrintWriter out=response.getWriter();
        	 
          //  con = DBConnection.getDBConnection();
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement(insertSql);
            wb = new HSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum();
            columnNum = ws.getRow(0).getLastCellNum();
	          Iterator<Row> rowIterator = ws.iterator(); 
              while(rowIterator.hasNext()) {
            	  int cellPos=0;
                      Row row1 = rowIterator.next(); 
                      Iterator<Cell> cellIterator = row1.cellIterator();
                              while(cellIterator.hasNext()) {
                            	 
                                      Cell cell1 = cellIterator.next();
                                      cellPos++;
                                      
                                      if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING)  // insertion of String data
                                      {
                                          ps.setString(cellPos, cell1.getStringCellValue().trim());
                                      }
                                      else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {

                                          if (HSSFDateUtil.isCellDateFormatted(cell1)) {      // Insertion of Date cell
                                              timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                              ps.setString(cellPos, timeStamp.trim());

                                          } else {
                                        	 temp = BigDecimal.valueOf(cell1.getNumericCellValue()).toBigInteger();//Insertion of number field
                                              ps.setString(cellPos, String.valueOf(temp).trim());


                                          }

                                      }
                                     
                              }
               
                try
                {
                ps.executeUpdate();
                count++;
                }
                catch(SQLException e){
                	error++;
                  status = "Failure";
                  e.printStackTrace();
                }
                
            }  if (error >0){
              	out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Error in Inserting "+error+" records  Kindly check the logs for more information \" "+");");
                out.println("</script>");
                
            }
              
        con.commit();
		con.close();
		      if (error==0){
                status ="Success";
		      }
            }
        

    catch (SQLException | IOException e) {
		
        status = "Failure";
        e.printStackTrace();
    } finally {
        try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
        try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}
    }
}
     
    private void xlsxFileInsert(String requestTableName,HttpServletResponse response)   {
        XSSFWorkbook wb;
        XSSFSheet ws;
        int error = 0;
       

        try {
		PrintWriter out=response.getWriter();
          //  con = DBConnection.getDBConnection();
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement(insertSql);
            wb = new XSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum()+1;
            columnNum = ws.getRow(0).getLastCellNum();
		Iterator<Row> rowIterator = ws.iterator(); 
        while(rowIterator.hasNext()) {
      	  int cellPos=0;
                Row row1 = rowIterator.next(); 
                Iterator<Cell> cellIterator = row1.cellIterator();
                        while(cellIterator.hasNext()) {
                                Cell cell1 = cellIterator.next();
                                cellPos++;
                                
                                if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING)  // insertion of String data
                                {
                                    ps.setString(cellPos, cell1.getStringCellValue().trim());
                                }
                                else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                    if (HSSFDateUtil.isCellDateFormatted(cell1)) {      // Insertion of Date cell
                                        timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                        ps.setString(cellPos, timeStamp.trim());
                                    } else {
                                    	BigInteger temp = BigDecimal.valueOf(cell1.getNumericCellValue()).toBigInteger();//Insertion of number field
                                        ps.setString(cellPos, String.valueOf(temp).trim());
                                    }
                                }
                        }
               try
               {
                ps.executeUpdate();
                count++;
               }
               catch(SQLException e){
            	   error++;
                   status = "Failure";
                   e.getMessage();
               }
               

            }
        if (error >0){
          	out.println("<script type=\"text/javascript\">");
              out.println("alert(\"Error in Inserting "+error+" records  Kindly check the logs for more information \");");
              out.println("</script>");
              
          }
           
		con.commit();
		con.close();
            if (error == 0) status ="Success";
           
        }catch (SQLException | IOException e) {
		
            status ="Failure";
            e.printStackTrace();
            hmap.clear();
        } finally {
            try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
            try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}

        }

    }

}
