package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_DebitCardRequestEvent extends Event {
	private String Account_Id;
	private String Customer_ID;
	private String Card_No;
	private Date sys_time;
	private String host_id;
	private Date eventts;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setAccount_Id((String) msgMap.get("Account_Id"));
		setCustomer_ID((String) msgMap.get("Customer_ID"));
		setCard_No((String) msgMap.get("Card_No"));
		setSys_time((String) msgMap.get("sys_time"));
		setHost_id((String) msgMap.get("host_id"));
		setEventts((String) msgMap.get("eventts"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_DebitCardRequestEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		/*
		String hostAcctKey = getAccountId ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		*/
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		String cxCifId = "";
		String hostCustKey = getCustomer_ID();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setAccount_Id(String Account_Id) throws InvalidDataException { 
		this.Account_Id = Account_Id;
	}
	public String getAccount_Id() {
		return this.Account_Id;
	}

	public void setCustomer_ID(String Customer_ID) throws InvalidDataException { 
		this.Customer_ID = Customer_ID;
	}
	public String getCustomer_ID() {
		return this.Customer_ID;
	}

	public void setCard_No(String Card_No) throws InvalidDataException { 
		this.Card_No = Card_No;
	}
	public String getCard_No() {
		return this.Card_No;
	}

	public void setSys_time(String sys_time) throws InvalidDataException { 
		try { 
			this.sys_time = dateFormat1.parse(sys_time);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("sys_time is blank or contains incorrect value.");
		}
	}
	public Date getSys_time() {
		return this.sys_time;
	}

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setEventts(String eventts) {
		try { 
			this.eventts = dateFormat1.parse(eventts);
		} catch (Exception ex) {
			Calendar c = Calendar.getInstance();
			c.set(1900, 1, 1, 1, 1, 1);
			this.eventts = c.getTime();
		}
	}
	public Date getEventts() {
		return this.eventts;
	}

}
