cxps.noesis.glossary.entity.nft-cheqbookreq{
        db-name = NFT_CHEQBOOKREQ
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [

	       	{ name = final-acct-status, column = ENDCHEQNUMBER, type = "string:50"}
		{ name = account-id, column = account_id, type = "string:50"}
		{ name = avl-bal, column = avl_bal, type = "number:30"}
		{ name = init-acct-status, column = CUSTOMER_ID, type = "string:50"}
		{ name = branch-id-desc, column = BEGINCHEQNUMBER, type = "string:50"}
		{ name = acct-open-dt, column = NO_OF_LEAV_ISUD, type = timestamp}
		{ name = insertion-time, column = ISSUE_DATE, type = timestamp}
		{ name = sys-time, column = sys_time, type = timestamp}
		{ name = eventts, column = eventts, type = timestamp}
		{ name = user-id, column = SYSTEM, type = "string:50"}
               	{ name = host-id, column = host_id, type = "string:50"}
	       	{ name = sync-status, column = sync_status, type = "string:50"}
                { name = server-id, column = server_id, type = "number:30"}
	       	       
	      
 ]

}
