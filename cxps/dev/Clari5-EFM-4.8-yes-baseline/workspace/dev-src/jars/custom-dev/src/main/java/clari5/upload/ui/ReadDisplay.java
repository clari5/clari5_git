package clari5.upload.ui;

import java.sql.ResultSetMetaData;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import clari5.rdbms.Rdbms;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Servlet implementation class ReadDisplay
 */
@WebServlet("/ReadDisplay")
public class ReadDisplay extends HttpServlet {
    private static final long serialVersionUID = 102831973239L;
    private Connection con = null;
    private PreparedStatement stat = null;
    private String timeStamp = null;
    private int rowNum = 0;
    private int columnNum = 0;
    int count = 0;
    private String requestFileName = null;
    private String fileBackupPath = null;
    private String requestFileNameSplit = null;

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session;
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        ConfigFileReader reader = new ConfigFileReader("upload-ui-clari5.conf");
        String uploadDirectory = reader.getFileBackUpPath();

        if (ServletFileUpload.isMultipartContent(request)) {
            try {

                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);

                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        requestFileName = new File(item.getName()).getName();
                        fileBackupPath = uploadDirectory + File.separator + timeStamp + requestFileName;
                        item.write(new File(uploadDirectory + File.separator + timeStamp + requestFileName));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' type='text/css' href='css/main.css'>");
        out.println("</head>");
        out.println("</html>");
        session = request.getSession();
        String table_name = (String) session.getAttribute("myTable");
        int[] modifyRow = new int[1000];


        /// check the data type of table and size of table
        int[] dbColType = new int[30];
        //int[] dbColSize = new int[30];
        /*	try
			{   
				if (session == null || session.getAttribute("USERID")== null){
					request.getRequestDispatcher("/expiry.jsp").forward(request,response);
				}*/

        //con = DBConnection.getDBConnection();
        try {
            con = Rdbms.getAppConnection();
            stat = con.prepareStatement("select * from " + table_name);
            ResultSet rs = stat.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            columnNum = rsmd.getColumnCount();
            int[] dbColSize = new int[columnNum];
            for (int i = 1; i <= columnNum; i++) {
                String s = rsmd.getColumnTypeName(i);
                if (s.equalsIgnoreCase("varchar2")) {
                    dbColType[i - 1] = 1;
                } else {
                    dbColType[i - 1] = 2;
                }
                dbColSize[i - 1] = rsmd.getColumnDisplaySize(i) + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		/*	catch (ServletException e) {
				e.printStackTrace();
			 }*/ finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        session = request.getSession();
        session.setAttribute("FilePath", fileBackupPath);
        session.setAttribute("FileName", requestFileName);
        int s = fileBackupPath.lastIndexOf('.');
        if (s >= 0) {
            requestFileNameSplit = fileBackupPath.substring(s + 1);
        }

        String ename = new String(requestFileNameSplit);
        try {


            if (ename.equals("xls")) {
				     /*try
				       {*/

                File excel = new File(fileBackupPath);
                FileInputStream fis = new FileInputStream(excel);//accessing the file from particular location
                HSSFWorkbook wb = new HSSFWorkbook(fis);
                HSSFSheet ws = wb.getSheetAt(0);
                int maxNumOfCells = ws.getRow(0).getLastCellNum();
                Iterator<Row> rows = ws.rowIterator();
                int column = 0;
                int num = 0;
                if (columnNum != maxNumOfCells + 4) {

                    column++;

                }
                while (rows.hasNext()) {
                    HSSFRow row = (HSSFRow) rows.next();
                    if (row.getRowNum() >= 0) {
                        for (int cellCounter = 0
                             ; cellCounter < maxNumOfCells
                                ; cellCounter++) {// Loop through cells

                            if (row.getCell(cellCounter) == null) {


                                num++;
                            }
                        }
                    }
                }

                if (column >= 1 && num >= 1) {

                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-3'></div>");
                    out.println("<div class='col-xs-6'>");
                    out.println("<h3>Row is null and column is missing </h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-3'></div></div>");
                    out.println("<div class='row'><div class='col-xs-5'></div>");
                    out.println("<div class='col-xs-2'>");
                    out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("</body>");
                    out.println("</html>");


                } else if (column >= 1) {

                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-3'></div>");
                    out.println("<div class='col-xs-6'>");
                    out.println("<h3>Some Columns are missing!! Check the file and upload again</h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-3'></div></div>");
                    out.println("<div class='row'><div class='col-xs-5'></div>");
                    out.println("<div class='col-xs-2'>");
                    out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("</body>");
                    out.println("</html>");
                    num++;
                } else if (num >= 1) {
                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-3'></div>");
                    out.println("<div class='col-xs-6'>");
                    out.println("<h3>Row is null, Kindly check and Insert again </h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-3'></div></div>");
                    out.println("<div class='row'><div class='col-xs-5'></div>");
                    out.println("<div class='col-xs-2'>");
                    out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("</body>");
                    out.println("</html>");

                } else {

                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-4'></div>");
                    out.println("<div class='col-xs-4'>");
                    out.println("<h3>All Records are right</h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("<div class='row'><div class='col-xs-4'></div>");
                    out.println("<div class='col-xs-1'>");
                    out.println("<a href='UploadDownloadFileServlet'><input type='button' value='Refresh & Insert' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-6'></div></div>");
                    out.println("</body>");
                    out.println("</html>");


                }


            }


            /////////////////////xlsx////////////////////////////////////////////////////////

            else if (ename.equals("xlsx")) {
                File excel = new File(fileBackupPath);
                FileInputStream fis = new FileInputStream(excel);//accessing the file from particular location
                XSSFWorkbook wb = new XSSFWorkbook(fis);
                XSSFSheet ws = wb.getSheetAt(0);
                int maxNumOfCells = ws.getRow(0).getLastCellNum();
                Iterator<Row> rows = ws.rowIterator();
                int num = 0;
                int column = 0;
                if (columnNum != maxNumOfCells + 4) {

                    column++;
                }
                while (rows.hasNext()) {
                    XSSFRow row = (XSSFRow) rows.next();
                    for (int cellCounter = 0
                         ; cellCounter < maxNumOfCells
                            ; cellCounter++) { // Loop through cells
                        if (row.getCell(cellCounter) == null) {

                            num++;

                        }
                    }
                }
                if (num >= 1 && column >= 1) {
                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-3'></div>");
                    out.println("<div class='col-xs-6'>");
                    out.println("<h3>Row is null and column is missing </h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-3'></div></div>");
                    out.println("<div class='row'><div class='col-xs-5'></div>");
                    out.println("<div class='col-xs-2'>");
                    out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("</body>");
                    out.println("</html>");


                } else if (column >= 1) {

                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-3'></div>");
                    out.println("<div class='col-xs-6'>");
                    out.println("<h3>Some Columns are missing!! Check the file and upload again</h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-3'></div></div>");
                    out.println("<div class='row'><div class='col-xs-5'></div>");
                    out.println("<div class='col-xs-2'>");
                    out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("</body>");
                    out.println("</html>");


                } else if (num >= 1) {
                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-3'></div>");
                    out.println("<div class='col-xs-6'>");
                    out.println("<h3>Row is null,Kindly check and Insert again </h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-3'></div></div>");
                    out.println("<div class='row'><div class='col-xs-5'></div>");
                    out.println("<div class='col-xs-2'>");
                    out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("</body>");
                    out.println("</html>");

                } else {

                    out.println("<html>");
                    out.println("<head>");
                    out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                    out.println("<script src='js/bootstrap.min.js'></script>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-4'></div>");
                    out.println("<div class='col-xs-4'>");
                    out.println("<h3>All Records are right</h3><br>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("<div class='row'><div class='col-xs-4'></div>");
                    out.println("<div class='col-xs-1'>");
                    out.println("<a href='UploadDownloadFileServlet'><input type='button' value='Refresh & Insert' class='btn btn-primary  btn-info'></a>");
                    out.println("</div>");
                    out.println("<div class='col-xs-6'></div></div>");
                    out.println("</body>");
                    out.println("</html>");


                }


            } else {

                out.println("<html>");
                out.println("<head>");
                out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
                out.println("<script src='js/bootstrap.min.js'></script>");
                out.println("</head>");
                out.println("<body>");
                out.println("<div class='row'><div class='col-xs-3'></div>");
                out.println("<div class='col-xs-6'>");
                out.println("<h3>The Uploaded file should be either xls or xlsx </h3><br>");
                out.println("</div>");
                out.println("<div class='col-xs-3'></div></div>");
                out.println("<div class='row'><div class='col-xs-5'></div>");
                out.println("<div class='col-xs-2'>");
                out.println("<a href='InsertPage.html'><input type='button' value='Back' class='btn btn-info'></a>");
                out.println("</div>");
                out.println("<div class='col-xs-4'></div></div>");
                out.println("</body>");
                out.println("</html>");


            }

        }catch (Exception e){e.printStackTrace();}
    }

    }
