package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class FT_CoreDeclineTxnEvent extends Event {
	private String acct_id;
	private String cust_id;
	private String ucic_id;
	private String product_code;
	private String DRCR;
	private Double Tran_amount;
	private String Branch_Id;
	private Date TD_Maturity_Date;
	private Double Eff_Available_Balance;
	private String Channel_Id;
	private String Product_Code;
	private String Tran_numonic_code;
	private String Host_Type;
	private String Numeric_Code;
	private String Instrument_Type;
	private String Instrument_Number;
	private String ATM_ID;
	private String ISIN_Number;
	private Date tran_date;
	private Date pstd_date;
	private String tran_id;
	private String part_tran_srl_num;
	private Date value_date;
	private String tran_crncy_code;
	private Double ref_tran_amt;
	private String ref_tran_crncy;
	private String rate;
	private String tran_particular;
	private Date sys_time;
	private String bank_code;
	private String pstd_flg;
	private String online_batch;
	private String User_id;
	private String status;
	private String host_id;
	private String acct_name;
	private String TD_Liquidation_Type;
	private String Account_Type;
	private Double TOD_Grant_Amount;
	private String CA_Scheme_Code;
	private String SYSTEM;
	private String REM_TYPE;
	private String BRANCH;
	private String TRAN_CURR;
	private Double TRAN_AMT;
	private Double USD_EQV_AMT;
	private Double INR_AMOUNT;
	private String PURPOSE_CODE;
	private String PURPOSE_DESC;
	private String REM_CUST_ID;
	private String REM_NAME;
	private String REM_ADD1;
	private String REM_ADD2;
	private String REM_ADD3;
	private String REM_CITY;
	private String REM_CNTRY_CODE;
	private String BEN_CUST_ID;
	private String BEN_NAME;
	private String BEN_ADD1;
	private String BEN_ADD2;
	private String BEN_ADD3;
	private String BEN_CITY;
	private String BEN_CNTRY_CODE;
	private String CLIENT_ACC_NO;
	private String CPTY_AC_NO;
	private String BEN_ACCT_NO;
	private String BEN_BIC;
	private String REM_ACCT_NO;
	private String REM_BIC;
	private Date TRN_DATE;
	private String Product_desc;
	private Date eventts;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setAcct_id((String) msgMap.get("acct_id"));
		setCust_id((String) msgMap.get("cust_id"));
		setUcic_id((String) msgMap.get("ucic_id"));
		setProduct_code((String) msgMap.get("product_code"));
		setDRCR((String) msgMap.get("DRCR"));
		setTran_amount((String) msgMap.get("Tran_amount"));
		setBranch_Id((String) msgMap.get("Branch_Id"));
		setTD_Maturity_Date((String) msgMap.get("TD_Maturity_Date"));
		setEff_Available_Balance((String) msgMap.get("Eff_Available_Balance"));
		setChannel_Id((String) msgMap.get("Channel_Id"));
		setProduct_Code((String) msgMap.get("Product_Code"));
		setTran_numonic_code((String) msgMap.get("Tran_numonic_code"));
		setHost_Type((String) msgMap.get("Host_Type"));
		setNumeric_Code((String) msgMap.get("Numeric_Code"));
		setInstrument_Type((String) msgMap.get("Instrument_Type"));
		setInstrument_Number((String) msgMap.get("Instrument_Number"));
		setATM_ID((String) msgMap.get("ATM_ID"));
		setISIN_Number((String) msgMap.get("ISIN_Number"));
		setTran_date((String) msgMap.get("tran_date"));
		setPstd_date((String) msgMap.get("pstd_date"));
		setTran_id((String) msgMap.get("tran_id"));
		setPart_tran_srl_num((String) msgMap.get("part_tran_srl_num"));
		setValue_date((String) msgMap.get("value_date"));
		setTran_crncy_code((String) msgMap.get("tran_crncy_code"));
		setRef_tran_amt((String) msgMap.get("ref_tran_amt"));
		setRef_tran_crncy((String) msgMap.get("ref_tran_crncy"));
		setRate((String) msgMap.get("rate"));
		setTran_particular((String) msgMap.get("tran_particular"));
		setSys_time1((String) msgMap.get("sys_time"));
		setBank_code((String) msgMap.get("bank_code"));
		setPstd_flg((String) msgMap.get("pstd_flg"));
		setOnline_batch((String) msgMap.get("online_batch"));
		setUser_id((String) msgMap.get("User_id"));
		setStatus((String) msgMap.get("status"));
		setHost_id((String) msgMap.get("host_id"));
		setAcct_name((String) msgMap.get("acct_name"));
		setTD_Liquidation_Type((String) msgMap.get("TD_Liquidation_Type"));
		setAccount_Type((String) msgMap.get("Account_Type"));
		setTOD_Grant_Amount((String) msgMap.get("TOD_Grant_Amount"));
		setCA_Scheme_Code((String) msgMap.get("CA_Scheme_Code"));
		setSYSTEM((String) msgMap.get("SYSTEM"));
		setREM_TYPE((String) msgMap.get("REM_TYPE"));
		setBRANCH((String) msgMap.get("BRANCH"));
		setTRAN_CURR((String) msgMap.get("TRAN_CURR"));
		setTRAN_AMT((String) msgMap.get("TRAN_AMT"));
		setUSD_EQV_AMT((String) msgMap.get("USD_EQV_AMT"));
		setINR_AMOUNT((String) msgMap.get("INR_AMOUNT"));
		setPURPOSE_CODE((String) msgMap.get("PURPOSE_CODE"));
		setPURPOSE_DESC((String) msgMap.get("PURPOSE_DESC"));
		setREM_CUST_ID((String) msgMap.get("REM_CUST_ID"));
		setREM_NAME((String) msgMap.get("REM_NAME"));
		setREM_ADD1((String) msgMap.get("REM_ADD1"));
		setREM_ADD2((String) msgMap.get("REM_ADD2"));
		setREM_ADD3((String) msgMap.get("REM_ADD3"));
		setREM_CITY((String) msgMap.get("REM_CITY"));
		setREM_CNTRY_CODE((String) msgMap.get("REM_CNTRY_CODE"));
		setBEN_CUST_ID((String) msgMap.get("BEN_CUST_ID"));
		setBEN_NAME((String) msgMap.get("BEN_NAME"));
		setBEN_ADD1((String) msgMap.get("BEN_ADD1"));
		setBEN_ADD2((String) msgMap.get("BEN_ADD2"));
		setBEN_ADD3((String) msgMap.get("BEN_ADD3"));
		setBEN_CITY((String) msgMap.get("BEN_CITY"));
		setBEN_CNTRY_CODE((String) msgMap.get("BEN_CNTRY_CODE"));
		setCLIENT_ACC_NO((String) msgMap.get("CLIENT_ACC_NO"));
		setCPTY_AC_NO((String) msgMap.get("CPTY_AC_NO"));
		setBEN_ACCT_NO((String) msgMap.get("BEN_ACCT_NO"));
		setBEN_BIC((String) msgMap.get("BEN_BIC"));
		setREM_ACCT_NO((String) msgMap.get("REM_ACCT_NO"));
		setREM_BIC((String) msgMap.get("REM_BIC"));
		setTRN_DATE((String) msgMap.get("TRN_DATE"));
		setProduct_desc((String) msgMap.get("Product_desc"));
		setEventts1((String) msgMap.get("eventts"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.FT_CoreDeclineTxnEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAcct_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);

		String cxUserKey = "";
                cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUser_id());
                WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
                wsInfoSet.add(wbr);


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		/*
		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		*/
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setAcct_id(String acct_id) throws InvalidDataException { 
		this.acct_id = acct_id;
	}
	public String getAcct_id() {
		return this.acct_id;
	}

	public void setCust_id(String cust_id) throws InvalidDataException { 
		this.cust_id = cust_id;
	}
	public String getCust_id() {
		return this.cust_id;
	}

	public void setUcic_id(String ucic_id) throws InvalidDataException { 
		this.ucic_id = ucic_id;
	}
	public String getUcic_id() {
		return this.ucic_id;
	}

	public void setProduct_code(String product_code) throws InvalidDataException { 
		this.product_code = product_code;
	}
	public String getProduct_code() {
		return this.product_code;
	}

	public void setDRCR(String DRCR) throws InvalidDataException { 
		this.DRCR = DRCR;
	}
	public String getDRCR() {
		return this.DRCR;
	}

	public void setTran_amount(String Tran_amount) throws InvalidDataException { 
		try { 
			this.Tran_amount = Double.parseDouble(Tran_amount);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("Tran_amount is blank or contains incorrect value.");
		}
	}
	public Double getTran_amount() {
		return this.Tran_amount;
	}

	public void setBranch_Id(String Branch_Id) throws InvalidDataException { 
		this.Branch_Id = Branch_Id;
	}
	public String getBranch_Id() {
		return this.Branch_Id;
	}

	public void setTD_Maturity_Date(String TD_Maturity_Date) throws InvalidDataException { 
		try { 
			this.TD_Maturity_Date = dateFormat1.parse(TD_Maturity_Date);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("TD_Maturity_Date is blank or contains incorrect value.");
		}
	}
	public Date getTD_Maturity_Date() {
		return this.TD_Maturity_Date;
	}

	public void setEff_Available_Balance(String Eff_Available_Balance) throws InvalidDataException { 
		try { 
			this.Eff_Available_Balance = Double.parseDouble(Eff_Available_Balance);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("Eff_Available_Balance is blank or contains incorrect value.");
		}
	}
	public Double getEff_Available_Balance() {
		return this.Eff_Available_Balance;
	}

	public void setChannel_Id(String Channel_Id) throws InvalidDataException { 
		this.Channel_Id = Channel_Id;
	}
	public String getChannel_Id() {
		return this.Channel_Id;
	}

	public void setProduct_Code(String Product_Code) throws InvalidDataException { 
		this.Product_Code = Product_Code;
	}
	public String getProduct_Code() {
		return this.Product_Code;
	}

	public void setTran_numonic_code(String Tran_numonic_code) {
		this.Tran_numonic_code = Tran_numonic_code;
	}
	public String getTran_numonic_code() {
		return this.Tran_numonic_code;
	}

	public void setHost_Type(String Host_Type) throws InvalidDataException { 
		this.Host_Type = Host_Type;
	}
	public String getHost_Type() {
		return this.Host_Type;
	}

	public void setNumeric_Code(String Numeric_Code) throws InvalidDataException { 
		this.Numeric_Code = Numeric_Code;
	}
	public String getNumeric_Code() {
		return this.Numeric_Code;
	}

	public void setInstrument_Type(String Instrument_Type) throws InvalidDataException { 
		this.Instrument_Type = Instrument_Type;
	}
	public String getInstrument_Type() {
		return this.Instrument_Type;
	}

	public void setInstrument_Number(String Instrument_Number) throws InvalidDataException { 
		this.Instrument_Number = Instrument_Number;
	}
	public String getInstrument_Number() {
		return this.Instrument_Number;
	}

	public void setATM_ID(String ATM_ID) throws InvalidDataException { 
		this.ATM_ID = ATM_ID;
	}
	public String getATM_ID() {
		return this.ATM_ID;
	}

	public void setISIN_Number(String ISIN_Number) throws InvalidDataException { 
		this.ISIN_Number = ISIN_Number;
	}
	public String getISIN_Number() {
		return this.ISIN_Number;
	}

	public void setTran_date(String tran_date) throws InvalidDataException { 
		try { 
			this.tran_date = dateFormat1.parse(tran_date);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("tran_date is blank or contains incorrect value.");
		}
	}
	public Date getTran_date() {
		return this.tran_date;
	}

	public void setPstd_date(String pstd_date) throws InvalidDataException { 
		try { 
			this.pstd_date = dateFormat1.parse(pstd_date);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("pstd_date is blank or contains incorrect value.");
		}
	}
	public Date getPstd_date() {
		return this.pstd_date;
	}

	public void setTran_id(String tran_id) throws InvalidDataException { 
		this.tran_id = tran_id;
	}
	public String getTran_id() {
		return this.tran_id;
	}

	public void setPart_tran_srl_num(String part_tran_srl_num) throws InvalidDataException { 
		this.part_tran_srl_num = part_tran_srl_num;
	}
	public String getPart_tran_srl_num() {
		return this.part_tran_srl_num;
	}

	public void setValue_date(String value_date) throws InvalidDataException { 
		try { 
			this.value_date = dateFormat1.parse(value_date);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("value_date is blank or contains incorrect value.");
		}
	}
	public Date getValue_date() {
		return this.value_date;
	}

	public void setTran_crncy_code(String tran_crncy_code) throws InvalidDataException { 
		this.tran_crncy_code = tran_crncy_code;
	}
	public String getTran_crncy_code() {
		return this.tran_crncy_code;
	}

	public void setRef_tran_amt(String ref_tran_amt) throws InvalidDataException { 
		try { 
			this.ref_tran_amt = Double.parseDouble(ref_tran_amt);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("ref_tran_amt is blank or contains incorrect value.");
		}
	}
	public Double getRef_tran_amt() {
		return this.ref_tran_amt;
	}

	public void setRef_tran_crncy(String ref_tran_crncy) throws InvalidDataException { 
		this.ref_tran_crncy = ref_tran_crncy;
	}
	public String getRef_tran_crncy() {
		return this.ref_tran_crncy;
	}

	public void setRate(String rate) throws InvalidDataException { 
		this.rate = rate;
	}
	public String getRate() {
		return this.rate;
	}

	public void setTran_particular(String tran_particular) throws InvalidDataException { 
		this.tran_particular = tran_particular;
	}
	public String getTran_particular() {
		return this.tran_particular;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

	public void setBank_code(String bank_code) throws InvalidDataException { 
		this.bank_code = bank_code;
	}
	public String getBank_code() {
		return this.bank_code;
	}

	public void setPstd_flg(String pstd_flg) throws InvalidDataException { 
		this.pstd_flg = pstd_flg;
	}
	public String getPstd_flg() {
		return this.pstd_flg;
	}

	public void setOnline_batch(String online_batch) throws InvalidDataException { 
		this.online_batch = online_batch;
	}
	public String getOnline_batch() {
		return this.online_batch;
	}

	public void setUser_id(String User_id) throws InvalidDataException { 
		this.User_id = User_id;
	}
	public String getUser_id() {
		return this.User_id;
	}

	public void setStatus(String status) throws InvalidDataException { 
		this.status = status;
	}
	public String getStatus() {
		return this.status;
	}

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setAcct_name(String acct_name) throws InvalidDataException { 
		this.acct_name = acct_name;
	}
	public String getAcct_name() {
		return this.acct_name;
	}

	public void setTD_Liquidation_Type(String TD_Liquidation_Type) throws InvalidDataException { 
		this.TD_Liquidation_Type = TD_Liquidation_Type;
	}
	public String getTD_Liquidation_Type() {
		return this.TD_Liquidation_Type;
	}

	public void setAccount_Type(String Account_Type) throws InvalidDataException { 
		this.Account_Type = Account_Type;
	}
	public String getAccount_Type() {
		return this.Account_Type;
	}

	public void setTOD_Grant_Amount(String TOD_Grant_Amount) {
		try { 
			this.TOD_Grant_Amount = Double.parseDouble(TOD_Grant_Amount);
		} catch (Exception ex) {
			this.TOD_Grant_Amount = 0.0;
		}
	}
	public Double getTOD_Grant_Amount() {
		return this.TOD_Grant_Amount;
	}

	public void setCA_Scheme_Code(String CA_Scheme_Code) {
		this.CA_Scheme_Code = CA_Scheme_Code;
	}
	public String getCA_Scheme_Code() {
		return this.CA_Scheme_Code;
	}

	public void setSYSTEM(String SYSTEM) {
		this.SYSTEM = SYSTEM;
	}
	public String getSYSTEM() {
		return this.SYSTEM;
	}

	public void setREM_TYPE(String REM_TYPE) {
		this.REM_TYPE = REM_TYPE;
	}
	public String getREM_TYPE() {
		return this.REM_TYPE;
	}

	public void setBRANCH(String BRANCH) {
		this.BRANCH = BRANCH;
	}
	public String getBRANCH() {
		return this.BRANCH;
	}

	public void setTRAN_CURR(String TRAN_CURR) {
		this.TRAN_CURR = TRAN_CURR;
	}
	public String getTRAN_CURR() {
		return this.TRAN_CURR;
	}

	public void setTRAN_AMT(String TRAN_AMT) {
		try { 
			this.TRAN_AMT = Double.parseDouble(TRAN_AMT);
		} catch (Exception ex) {
			this.TRAN_AMT = 0.0;
		}
	}
	public Double getTRAN_AMT() {
		return this.TRAN_AMT;
	}

	public void setUSD_EQV_AMT(String USD_EQV_AMT) {
		try { 
			this.USD_EQV_AMT = Double.parseDouble(USD_EQV_AMT);
		} catch (Exception ex) {
			this.USD_EQV_AMT = 0.0;
		}
	}
	public Double getUSD_EQV_AMT() {
		return this.USD_EQV_AMT;
	}

	public void setINR_AMOUNT(String INR_AMOUNT) {
		try { 
			this.INR_AMOUNT = Double.parseDouble(INR_AMOUNT);
		} catch (Exception ex) {
			this.INR_AMOUNT = 0.0;
		}
	}
	public Double getINR_AMOUNT() {
		return this.INR_AMOUNT;
	}

	public void setPURPOSE_CODE(String PURPOSE_CODE) {
		this.PURPOSE_CODE = PURPOSE_CODE;
	}
	public String getPURPOSE_CODE() {
		return this.PURPOSE_CODE;
	}

	public void setPURPOSE_DESC(String PURPOSE_DESC) {
		this.PURPOSE_DESC = PURPOSE_DESC;
	}
	public String getPURPOSE_DESC() {
		return this.PURPOSE_DESC;
	}

	public void setREM_CUST_ID(String REM_CUST_ID) {
		this.REM_CUST_ID = REM_CUST_ID;
	}
	public String getREM_CUST_ID() {
		return this.REM_CUST_ID;
	}

	public void setREM_NAME(String REM_NAME) {
		this.REM_NAME = REM_NAME;
	}
	public String getREM_NAME() {
		return this.REM_NAME;
	}

	public void setREM_ADD1(String REM_ADD1) {
		this.REM_ADD1 = REM_ADD1;
	}
	public String getREM_ADD1() {
		return this.REM_ADD1;
	}

	public void setREM_ADD2(String REM_ADD2) {
		this.REM_ADD2 = REM_ADD2;
	}
	public String getREM_ADD2() {
		return this.REM_ADD2;
	}

	public void setREM_ADD3(String REM_ADD3) {
		this.REM_ADD3 = REM_ADD3;
	}
	public String getREM_ADD3() {
		return this.REM_ADD3;
	}

	public void setREM_CITY(String REM_CITY) {
		this.REM_CITY = REM_CITY;
	}
	public String getREM_CITY() {
		return this.REM_CITY;
	}

	public void setREM_CNTRY_CODE(String REM_CNTRY_CODE) {
		this.REM_CNTRY_CODE = REM_CNTRY_CODE;
	}
	public String getREM_CNTRY_CODE() {
		return this.REM_CNTRY_CODE;
	}

	public void setBEN_CUST_ID(String BEN_CUST_ID) {
		this.BEN_CUST_ID = BEN_CUST_ID;
	}
	public String getBEN_CUST_ID() {
		return this.BEN_CUST_ID;
	}

	public void setBEN_NAME(String BEN_NAME) {
		this.BEN_NAME = BEN_NAME;
	}
	public String getBEN_NAME() {
		return this.BEN_NAME;
	}

	public void setBEN_ADD1(String BEN_ADD1) {
		this.BEN_ADD1 = BEN_ADD1;
	}
	public String getBEN_ADD1() {
		return this.BEN_ADD1;
	}

	public void setBEN_ADD2(String BEN_ADD2) {
		this.BEN_ADD2 = BEN_ADD2;
	}
	public String getBEN_ADD2() {
		return this.BEN_ADD2;
	}

	public void setBEN_ADD3(String BEN_ADD3) {
		this.BEN_ADD3 = BEN_ADD3;
	}
	public String getBEN_ADD3() {
		return this.BEN_ADD3;
	}

	public void setBEN_CITY(String BEN_CITY) {
		this.BEN_CITY = BEN_CITY;
	}
	public String getBEN_CITY() {
		return this.BEN_CITY;
	}

	public void setBEN_CNTRY_CODE(String BEN_CNTRY_CODE) {
		this.BEN_CNTRY_CODE = BEN_CNTRY_CODE;
	}
	public String getBEN_CNTRY_CODE() {
		return this.BEN_CNTRY_CODE;
	}

	public void setCLIENT_ACC_NO(String CLIENT_ACC_NO) {
		this.CLIENT_ACC_NO = CLIENT_ACC_NO;
	}
	public String getCLIENT_ACC_NO() {
		return this.CLIENT_ACC_NO;
	}

	public void setCPTY_AC_NO(String CPTY_AC_NO) {
		this.CPTY_AC_NO = CPTY_AC_NO;
	}
	public String getCPTY_AC_NO() {
		return this.CPTY_AC_NO;
	}

	public void setBEN_ACCT_NO(String BEN_ACCT_NO) {
		this.BEN_ACCT_NO = BEN_ACCT_NO;
	}
	public String getBEN_ACCT_NO() {
		return this.BEN_ACCT_NO;
	}

	public void setBEN_BIC(String BEN_BIC) {
		this.BEN_BIC = BEN_BIC;
	}
	public String getBEN_BIC() {
		return this.BEN_BIC;
	}

	public void setREM_ACCT_NO(String REM_ACCT_NO) {
		this.REM_ACCT_NO = REM_ACCT_NO;
	}
	public String getREM_ACCT_NO() {
		return this.REM_ACCT_NO;
	}

	public void setREM_BIC(String REM_BIC) {
		this.REM_BIC = REM_BIC;
	}
	public String getREM_BIC() {
		return this.REM_BIC;
	}

	public void setTRN_DATE(String TRN_DATE) {
		try { 
			this.TRN_DATE = dateFormat1.parse(TRN_DATE);
		} catch (Exception ex) {
			Calendar c = Calendar.getInstance();
			c.set(1900, 1, 1, 1, 1, 1);
			this.TRN_DATE = c.getTime();
		}
	}
	public Date getTRN_DATE() {
		return this.TRN_DATE;
	}

	public void setProduct_desc(String Product_desc) throws InvalidDataException { 
		this.Product_desc = Product_desc;
	}
	public String getProduct_desc() {
		return this.Product_desc;
	}

	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }

}
