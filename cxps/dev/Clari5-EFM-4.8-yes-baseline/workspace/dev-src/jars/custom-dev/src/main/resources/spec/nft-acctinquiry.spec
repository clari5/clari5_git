cxps.noesis.glossary.entity.nft-acctinquiry{
        db-name = NFT_ACCTINQUIRY
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
                
	       { name = branchid, column = branchid, type = "string:50"}
	       { name = acct-id, column = acct_id, type = "string:50"}
	       { name = avl-bal, column = avl_bal, type = "number:30"}
	       { name = branchiddesc, column = branchiddesc, type = "string:50"}
               { name = user-id, column = user_id, type = "string:50"}
	       { name = acct-status, column = acct_status, type = "string:50"}
	       { name = rm-code-flag, column = rm_code_flag, type = "string:50"}
	       { name = menu-id, column = menu_id, type = "string:50"}
	       { name = host-id, column = host_id, type = "string:50"}
	       { name = cust-id, column = cust_id, type = "string:50"}
	       { name = sys-time, column = sys_time, type = timestamp}
	       { name = eventts, column = eventts, type = timestamp}
	       { name = sync-status, column = sync_status, type = "string:50"}
	       { name = server-id, column = server_id, type = "number:30"}
	      
 ]

}
