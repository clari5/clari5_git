package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_StaticinfochangeEvent extends Event {
	private Date tran_date;
	private String branch_id;
	private String Entitytype;
	private String initsubentityval;
	private String finalsubentityval;
	private String acct_id;
	private String cust_id;
	private String channel;
	private Date sys_time;
	private String host_id;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setTran_date((String) msgMap.get("tran_date"));
		setBranch_id((String) msgMap.get("branch_id"));
		setEntitytype((String) msgMap.get("Entitytype"));
		setInitsubentityval((String) msgMap.get("initsubentityval"));
		setFinalsubentityval((String) msgMap.get("finalsubentityval"));
		setAcct_id((String) msgMap.get("acct_id"));
		setCust_id((String) msgMap.get("cust_id"));
		setChannel((String) msgMap.get("channel"));
		setSys_time1((String) msgMap.get("sys_time"));
		setHost_id((String) msgMap.get("host_id"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_StaticinfochangeEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAcct_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		/*
		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		*/
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setTran_date(String tran_date) throws InvalidDataException { 
		try { 
			this.tran_date = dateFormat1.parse(tran_date);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("tran_date is blank or contains incorrect value.");
		}
	}
	public Date getTran_date() {
		return this.tran_date;
	}

	public void setBranch_id(String branch_id) throws InvalidDataException { 
		this.branch_id = branch_id;
	}
	public String getBranch_id() {
		return this.branch_id;
	}

	public void setEntitytype(String Entitytype) throws InvalidDataException { 
		this.Entitytype = Entitytype;
	}
	public String getEntitytype() {
		return this.Entitytype;
	}

	public void setInitsubentityval(String initsubentityval) {
		this.initsubentityval = initsubentityval;
	}
	public String getInitsubentityval() {
		return this.initsubentityval;
	}

	public void setFinalsubentityval(String finalsubentityval) {
		this.finalsubentityval = finalsubentityval;
	}
	public String getFinalsubentityval() {
		return this.finalsubentityval;
	}

	public void setAcct_id(String acct_id) throws InvalidDataException { 
		this.acct_id = acct_id;
	}
	public String getAcct_id() {
		return this.acct_id;
	}

	public void setCust_id(String cust_id) throws InvalidDataException { 
		this.cust_id = cust_id;
	}
	public String getCust_id() {
		return this.cust_id;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getChannel() {
		return this.channel;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

}
