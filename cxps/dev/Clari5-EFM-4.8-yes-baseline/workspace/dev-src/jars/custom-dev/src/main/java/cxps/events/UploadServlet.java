package cxps.events;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cxps.events.CustomException;
import cxps.events.ConfigFileReader;
import cxps.apex.utils.CxpsLogger;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by vijay on 10/8/17.
 */
public class UploadServlet extends HttpServlet {

    CxpsLogger logger = CxpsLogger.getLogger(UploadServlet.class);

    private String requestFileName = null;
    private String fileBackupPath = null;
    private String timeStamp = null;


    private void doEither(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sessionName;
        String userId = null;
        sessionName = request.getSession(false);
        if(sessionName!=null) {
            userId = (String) sessionName.getAttribute("userID");
        } else {
            logger.debug("Session is null. ");
        }
        String res = "";
       // if(userId!=null) {

                logger.info("Servlet is calling");
                timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                ConfigFileReader reader = new ConfigFileReader("bulkupload-clari5.conf");
                String uploadDirectory = reader.getFileBackUpPath();

                logger.info("Cxps Logger Upload Servlet is calling");

                UploadService uploadService = new UploadService();

                new File(uploadDirectory).mkdirs();

                if (ServletFileUpload.isMultipartContent(request)) {
                    try {
                        List<FileItem> multiparts = new ServletFileUpload(
                                new DiskFileItemFactory()).parseRequest(request);
                        for (FileItem item : multiparts) {
                            if (!item.isFormField()) {
                                File f = new File(item.getName());
                                long size = f.getTotalSpace();
                                System.out.println("Size : "+size);
                                requestFileName = f.getName();
                                String content = item.getContentType();
                                System.out.println("Content : " + content);
                                String extention = FilenameUtils.getExtension(requestFileName);
                                System.out.println("Extention : " + extention);
                                fileBackupPath = uploadDirectory + File.separator + timeStamp + requestFileName;
                                item.write(new File(fileBackupPath));
                                switch (extention) {
                                    case "csv":
                                        System.out.println("This is csv file");
                                        uploadService.readCSVFile(fileBackupPath, response, request);
                                        break;                                    
                                    default:
                                        System.out.println("No extention found");
                                        throw new CustomException("No Extention found");
                                }
                            }
                        }
                        res = "Success fully Uploaded";
                    }  catch (Exception e) {
                        res = "This is not multipart request";
                        System.out.println(e);
                        e.printStackTrace();
                    }
                } else {
                    res = "This is not multipart request";
                }

            /*} else {
                logger.debug("Session is not valid. Please refresh the page.");
                res = "Session is not valid. Please refresh the page. ";
            }*/
        try {
            PrintWriter out = response.getWriter();
            out.write(res);
            out.flush();
            out.close();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }


    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Do Get is calling");
        try {
            doEither(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            doEither(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
