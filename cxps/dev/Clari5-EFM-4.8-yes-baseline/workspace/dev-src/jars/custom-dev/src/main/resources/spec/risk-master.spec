cxps.noesis.glossary.entity.risk-master{
        db-name = RISK_MASTER
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = value-code, column = value_code, type = "string:50"}
		{ name = value-desc, column = value_desc, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
