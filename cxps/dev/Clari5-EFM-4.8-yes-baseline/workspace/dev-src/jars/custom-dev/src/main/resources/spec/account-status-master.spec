cxps.noesis.glossary.entity.account-status-master{
        db-name = ACCOUNT_STATUS_MASTER
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = cod-module, column = cod_module, type = "string:50"}
		{ name = cod-acct-status, column = cod_acct_status, type = "number:10", key=true}
		{ name = txt-acct-status, column = txt_acct_status, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
