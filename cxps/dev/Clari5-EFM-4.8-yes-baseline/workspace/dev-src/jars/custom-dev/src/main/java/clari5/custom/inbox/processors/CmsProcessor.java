package clari5.custom.inbox.processors;

import clari5.aml.cms.newjira.JiraInstance;
import clari5.inbox.Cl5IbxItem;
import clari5.inbox.InboxDaemon;
import clari5.inbox.InboxResponseTransformer;
import clari5.inbox.mappers.Cl5IbxItemMapper;
import clari5.inbox.usermanagement.RoleBasedAccess;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import clari5.trace.CxTracer;
import clari5.trace.TracerException;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class CmsProcessor implements clari5.inbox.processors.IInboxProcessor {

    private static CxpsLogger logger = CxpsLogger.getLogger(CmsProcessor.class);

    private static JiraClient jiraClient;
    private static boolean isJiraClientInitialized = false;
    private static Hocon responseKeys = null;


    private int COMMENT_SUCCESS = 201;
    private int TRANSITION_SUCCESS = 204;

    @Override
    public void processMessage(CxTracer tracer, String eventName, String event) throws TracerException.Fatal {

        if (!isJiraClientInitialized) {
            String jiraInstanceName = null;
            try {
                JiraInstance instance = new JiraInstance("jira-instance");
                jiraClient = new JiraClient(instance.getInstanceParams());
                isJiraClientInitialized = true;
                responseKeys = ((InboxDaemon) Clari5.getResource("inbox")).getResponseKeys();
                if (responseKeys != null) {
                    Map<String, String> keyLabelMap = new LinkedHashMap<>();
                    for (String bankKey : responseKeys.getKeysAsList()) {
                        keyLabelMap.put(bankKey, responseKeys.getString(bankKey));
                    }
                }
            } catch (JiraClientException.Configuration configuration) {
                configuration.printStackTrace();
            }
        }

        switch (eventName) {
            case "cms-inquiry":
                processCmsInquiry(tracer, event);
                break;
            case "cms-response":
                processCmsResponse(tracer, event);
                break;
        }
    }

    protected void processCmsInquiry(CxTracer tracer, String event) throws TracerException.Fatal {
        CxJson eventJson;
        try {
            eventJson = CxJson.parse(event);
            String issueData = eventJson.getString("issueData");
            CxJson issueJson = CxJson.parse(issueData);
            String entityType = issueJson.getString("entityType");
            String entityId = issueJson.getString("entityId");
            String issueKey = issueJson.getString("issueKey");
            String parentId = issueJson.getString("parentId");
            String branchId = issueJson.getString("branchId");
            String branchUserIdAsAssignedByJira = issueJson.getString("User_ID");
          
            //Get userId for the incoming clarification
            String status = "failure";
            if( branchUserIdAsAssignedByJira != null && !branchUserIdAsAssignedByJira.isEmpty() && !branchUserIdAsAssignedByJira.equals("null")){
                status = "success"; // userId was available ...
                branchUserIdAsAssignedByJira= branchUserIdAsAssignedByJira.toUpperCase();
                   }
            switch ( status ) {
                case "success":                 
                    issueJson.put("userId", branchUserIdAsAssignedByJira);
                    issueJson.put("updatedBy", branchUserIdAsAssignedByJira);
                    issueJson.put("createdBy", branchUserIdAsAssignedByJira);
                    Cl5IbxItem ibxItem = new Cl5IbxItem();                    
                    try {
                        ibxItem.from(issueJson.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        tracer.setException("Error while converting jsonObject to Cl5IBxItem object", e);
                    }
                    try (RDBMSSession session = Rdbms.getAppSession()) {
                        Cl5IbxItemMapper mapper = session.getMapper(Cl5IbxItemMapper.class);
                        mapper.insert(ibxItem);
                        tracer.setPassed(session);
                        session.commit();
                    }
                    break;

             /*
            If the userId is not present, then send a comment to Jira saying the branch id doesnot exist.
             */
                case "failure":
                    String comment = "User_ID is required";
                    logger.error(comment);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("body", comment);

                    CxJson responseJson = jiraClient.addCommentRequest(jsonObject.toString(), issueKey);
                    int responseCode = responseJson.getInt("responseCode", -1);
                    if (responseCode == COMMENT_SUCCESS) {
                        jsonObject = new JSONObject();
                        JSONObject transitionObject = new JSONObject();
                        String transitionId = jiraClient.getTransitionId("clarification-error-transition");
                        transitionObject.put("id", transitionId);
                        jsonObject.put("transition", transitionObject);

                        responseJson = jiraClient.changeTransitionState(jsonObject.toString(), issueKey);
                        responseCode = responseJson.getInt("responseCode", -1);
                        if (responseCode == TRANSITION_SUCCESS) {

                            try (RDBMSSession session = Rdbms.getAppSession()) {
                                tracer.setPassed(session);
                                session.commit();
                            }
                        } else {
                            if (responseJson.hasKey("responseCode")) {
                                responseJson.delete("responseCode");
                            }
                            Exception exception = new Exception(responseJson.toString());
                            try {
                                throw exception;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        if (responseJson.hasKey("responseCode")) {
                            responseJson.delete("responseCode");
                        }
                        Exception exception = new Exception(responseJson.toString());
                        try {
                            throw exception;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    break;
            }

        } catch (RdbmsException e) {
            e.printStackTrace();
            tracer.setException("Error in RDBMS", e);
        } catch (IOException e) {
            tracer.setException("Error while sending response to jira", e);
            e.printStackTrace();
        } catch (Exception e) {
            tracer.setException("Error", e);
            e.printStackTrace();
        }

    }

    protected void processCmsResponse(CxTracer tracer, String event) throws TracerException.Fatal {
        try {
            JSONObject eventJson = new JSONObject(event);
            JSONObject issueDataJson = (JSONObject) eventJson.get("issueData");
            String issueKey = issueDataJson.getString("issueKey");
            String itemId = issueDataJson.getString("itemId");
            String userId = issueDataJson.getString("userId");
            String comment = null;
            String inboxReply = "";
            String suspicious = "";
            if (((InboxDaemon) Clari5.getResource("inbox")).getResponseTransformer() != null) {
                JSONObject response = issueDataJson.getJSONObject("response");
                String additionalComment = issueDataJson.getString("addComment");
                if (additionalComment != null && !additionalComment.isEmpty()) {
                    response.put("additionalComment", additionalComment);
                }
                try {
                    inboxReply = InboxResponseTransformer.transformResponse(response, responseKeys);
                } catch (TracerException e) {
                    e.printStackTrace();
                    tracer.setException("Missing implementation ", e);
                }

                inboxReply = "Reply from " + userId + " :\n" + inboxReply;
            } else {
                if (issueDataJson.has("comment")) {
                    comment = issueDataJson.getString("comment");
                    inboxReply = "Reply from " + userId + " :" + comment;
                }
            }

            //At Jira UI, user needs to be shown along with the comment

            String hasAttachments = issueDataJson.getString("hasAttachments");
            if (hasAttachments.equalsIgnoreCase("Y")) {
                addAttachmentToIssue(issueKey, itemId);
            }

            //Sending to JIRA
            JSONObject jsonObject = new JSONObject();
            // jsonObject.put("body", comment);
            jsonObject.put("body", inboxReply);

            CxJson responseJson = jiraClient.addCommentRequest(jsonObject.toString(), issueKey);
            int responseCode = responseJson.getInt("responseCode", -1);
            if (responseCode == COMMENT_SUCCESS) {
                jsonObject = new JSONObject();
                JSONObject transitionObject = new JSONObject();
                 suspicious = issueDataJson.getJSONObject("response").getString("Check");
                 Hocon h =new Hocon();
                h.loadFromContext("suspecious-customFields.conf");
                JSONObject updateFields = new JSONObject();
                JSONObject wrapperJson = new JSONObject();
                String suspiciousByBranch = h.getString("jira-field.Suspecious-By-Branch");

                if (suspicious.equalsIgnoreCase("Y")) {
                    String transitionId = jiraClient.getTransitionId("clarification-received-transition");
                    transitionObject.put("id", transitionId);
                    jsonObject.put("transition", transitionObject);
                    updateFields.put(suspiciousByBranch,"Yes");
                    wrapperJson.put("fields",updateFields);
                    jiraClient.updateIssueRequest(wrapperJson.toString(), issueKey);
                }  else if(suspicious.equalsIgnoreCase("N")) {
                    String transitionIdClosed = h.getString("transitions.clarificaiton-closed-transition");
                    logger.debug("clarificaiton-closed-transition-id: "+transitionIdClosed);
                    transitionObject.put("id", transitionIdClosed);
                    jsonObject.put("transition", transitionObject);
                    updateFields.put(suspiciousByBranch,"No");
                    wrapperJson.put("fields",updateFields);
                    jiraClient.updateIssueRequest(wrapperJson.toString(), issueKey);
                }              

                responseJson = jiraClient.changeTransitionState(jsonObject.toString(), issueKey);


                responseCode = responseJson.getInt("responseCode", -1);
                logger.info("trnasition response code "+responseCode);

                if (responseCode == TRANSITION_SUCCESS) {
                    try (RDBMSSession session = Rdbms.getAppSession()) {
                        tracer.setPassed(session);
                        session.commit();
                    }
                }else logger.warn("Failed to change the transition state");
                    /*else {
                    if (responseJson.hasKey("responseCode")) {
                        responseJson.delete("responseCode");
                    }
                    Exception exception = new Exception(responseJson.toString());
                    try {
                        throw exception;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }*/
            }

        } catch (MalformedURLException e) {
            tracer.setException("Exception while forming url ", e);
            e.printStackTrace();
        } catch (ProtocolException e) {
            tracer.setException("Protocol Exception", e);
            e.printStackTrace();
        } catch (IOException e) {
            tracer.setException("Exception while I/O ", e);
            e.printStackTrace();
        } catch (SQLException e) {
            tracer.setException("SQL Exception ", e);
            e.printStackTrace();
        } catch (JiraClientException e) {
            e.printStackTrace();
            tracer.setException("Exception while posting to Jira", e);
        } catch (RdbmsException e) {
            e.printStackTrace();
        }
    }

    public static void addAttachmentToIssue(String issueKey, String itemId) throws SQLException, IOException, JiraClientException, JiraClientException.Configuration {

        logger.debug(" Sending the following files to JIRA ");

        try (Connection con = Rdbms.getAppConnection()) {
            InputStream inputStream;
            String statement = "select filename,  attachment from CL5_IBX_ATTACHMENT where item_id = ?";
            try (PreparedStatement preparedStatement = con.prepareStatement(statement)) {
                preparedStatement.setString(1, itemId);
                try (ResultSet result = preparedStatement.executeQuery()) {
                    //For multiple attachments
                    while (result.next()) {
                        Blob blob = result.getBlob("attachment");
                        String filename = result.getString("fileName");
                        logger.debug(filename);
                        if (blob == null) inputStream = new ByteArrayInputStream("".getBytes());
                        else inputStream = blob.getBinaryStream();
                        jiraClient.sendAttachment(issueKey, filename, inputStream);
                        inputStream.close();
                        if (blob != null) {
                            blob.free();
                        }
                    }
                }
            }
        }
    }
}
