    package clari5.upload.ui;

    import java.util.List;

    /**
     * Created by shishir on 25/5/17.
     */
    public class DeleteCandidates {
        List<String> headers;
        List<String> values;
        String tableName;
        DeleteCandidates(List<String> headers, List<String> values, String tableName) {
            this.headers = headers;
            this.values = values;
            this.tableName = tableName;
        }

        public String getDeleteQuery() {
            String deleteSql = "delete from " + tableName +" where ";
            String where = "";
            int noCol = headers.size();
            for (int i = 0; i < noCol; ++i) {
                String currentHeader = this.headers.get(i).toString();
                String currentValue = this.values.get(i).toString();
                if (currentHeader.equals("LCHG_TIME") || currentHeader.equals("RCRE_TIME")) continue;
                if (this.values.get(i).equals("null") ) {
                    where += currentHeader + " is null  ";
                } else {
                    where += currentHeader + " = '" + currentValue + "'";
                }
                if ( i != noCol-1) where +=" and ";
            }
            deleteSql += " " + where;
            return deleteSql;
        }
    }
