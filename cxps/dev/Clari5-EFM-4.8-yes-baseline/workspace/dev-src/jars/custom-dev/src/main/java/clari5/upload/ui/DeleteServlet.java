		package clari5.upload.ui;

		import javax.servlet.ServletException;
		import javax.servlet.annotation.WebServlet;
		import javax.servlet.http.HttpServlet;
		import javax.servlet.http.HttpServletRequest;
		import javax.servlet.http.HttpServletResponse;
		import javax.servlet.http.HttpSession;

		import java.io.IOException;
		import java.io.PrintWriter;
		import java.sql.*;
		import java.util.Arrays;
		import clari5.rdbms.Rdbms;

		/*This performs the
		 *  DELETE operation  based on the primary Key */

		@WebServlet("/DeleteServlet")
		public class DeleteServlet extends HttpServlet {
			private static final long serialVersionUID = 1L;

			protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
				doEither(request, response);
			}

			protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
				doEither(request, response);
			}

			@SuppressWarnings("null")
			private void doEither(HttpServletRequest request,
				HttpServletResponse response) throws IOException {
				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				PrintWriter out = response.getWriter();
				HttpSession session;
				String tableName = "";
				int count = 0;
				String userId;
				String status;
				String deleteSql;
				try {
					session = request.getSession();
					//con = DBConnection.getDBConnection();
					con = Rdbms.getAppConnection();
				/*	if (session == null || session.getAttribute("USERID")== null){
						try {
							request.getRequestDispatcher("/expiry.jsp").forward(request,response);

						} catch (ServletException e) {
							e.printStackTrace();
						}
					}*/
					tableName = (String) session.getAttribute("myId");
					userId = (String) session.getAttribute("userId");
				DeleteCandidates dc;
				for(String s : request.getParameterValues("deletebox")) {
					String[] columnWithHeaders = s.split("#");

					dc = new DeleteCandidates(Arrays.asList(columnWithHeaders[0].split(",")),
							Arrays.asList(columnWithHeaders[1].split(",")), tableName);
					deleteSql = dc.getDeleteQuery();
					try {
						ps = con.prepareStatement(deleteSql);
						int result = ps.executeUpdate();
						if (result > 0)
							++count;
						con.commit();
						status = "Success";
					} catch (Exception e) {
						e.printStackTrace();
						status = "Failure";
					}
					status = UploadUiAudit.updateAudit(tableName, userId, status,columnWithHeaders[1].split(",")[0], "", "Delete");
				}

					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"" + count+ "\"+\" ROW(S) SUCCESSFULLY DELETED\");");
					out.println("window.location.href='DisplayTableData?table_name='+'"+ tableName + "';");
					out.println("</script>");
				} catch (Exception e) {
					e.printStackTrace();
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"" + count+ "\"+\" ROW(S) COULDNOT BE DELETED\");");
					out.println("window.location.href='DisplayTableData?table_name='+'"+ tableName + "';");
					out.println("</script>");
				} finally {
					try {
						if (con != null)
							con.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
					try {
						if (ps != null)
							ps.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
					try {
						if (rs != null)
							rs.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
				}
			}

		}
