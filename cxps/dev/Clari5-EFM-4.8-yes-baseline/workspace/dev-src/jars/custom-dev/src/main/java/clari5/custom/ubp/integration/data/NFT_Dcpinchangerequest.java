package clari5.custom.ubp.integration.data;

public class NFT_Dcpinchangerequest extends ITableData {
    private String tableName = "NFT_DCPINCHANGE";
    private String event_type = "NFT_Dcpinchangerequest";
    private String   EVENT_ID;
    private String   succ_fail_flg;
    private String   card_no;
    private String   rqst_chnl;
    private String   error_desc;
    private String   sys_time;
    private String   error_code;
    private String    online_offline;
    private String    cust_id;
    private String    host_id;
    private String time_slot;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getOnline_offline() {
        return online_offline;
    }

    public void setOnline_offline(String online_offline) {
        this.online_offline = online_offline;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getSucc_fail_flg() {
        return succ_fail_flg;
    }

    public void setSucc_fail_flg(String succ_fail_flg) {
        this.succ_fail_flg = succ_fail_flg;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getRqst_chnl() {
        return rqst_chnl;
    }

    public void setRqst_chnl(String rqst_chnl) {
        this.rqst_chnl = rqst_chnl;
    }

    public String getError_desc() {
        return error_desc;
    }

    public void setError_desc(String error_desc) {
        this.error_desc = error_desc;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    /*public String getOnline_offline() {
        return Online_offline;
    }

    public void setOnline_offline(String online_offline) {
        Online_offline = online_offline;
    }
    */

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot = time_slot;
    }
}

