package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class FT_WMTxnEvent extends Event {
	private String WMS_Ref_No;
	private String TXN_Ref_No;
	private String Cust_Id;
	private String AcctId;
	private String Trade_Date;
	private Double Amount;
	private String Scheme_Name;
	private String Scheme_Type;
	private String Tran_Type;
	private String Sub_Tran_Type;
	private String RM_Code;
	private String Tran_Code;
	private Date sys_time;
	private String host_id;
	private Date eventts;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setWMS_Ref_No((String) msgMap.get("WMS_Ref_No"));
		setTXN_Ref_No((String) msgMap.get("TXN_Ref_No"));
		setCust_Id((String) msgMap.get("Cust_Id"));
		setAcctId((String) msgMap.get("AcctId"));
		setTrade_Date((String) msgMap.get("Trade_Date"));
		setAmount((String) msgMap.get("Amount"));
		setScheme_Name((String) msgMap.get("Scheme_Name"));
		setScheme_Type((String) msgMap.get("Scheme_Type"));
		setTran_Type((String) msgMap.get("Tran_Type"));
		setSub_Tran_Type((String) msgMap.get("Sub_Tran_Type"));
		setRM_Code((String) msgMap.get("RM_Code"));
		setTran_Code((String) msgMap.get("Tran_Code"));
		setSys_time1((String) msgMap.get("sys_time"));
		setHost_id((String) msgMap.get("host_id"));
		setEventts1((String) msgMap.get("eventts"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.FT_WMTxnEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAcctId ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		
		String cxCifId = "";
		String hostCustKey = getCust_Id();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setWMS_Ref_No(String WMS_Ref_No) throws InvalidDataException { 
		this.WMS_Ref_No = WMS_Ref_No;
	}
	public String getWMS_Ref_No() {
		return this.WMS_Ref_No;
	}

	public void setTXN_Ref_No(String TXN_Ref_No) throws InvalidDataException { 
		this.TXN_Ref_No = TXN_Ref_No;
	}
	public String getTXN_Ref_No() {
		return this.TXN_Ref_No;
	}

	public void setCust_Id(String Cust_Id) throws InvalidDataException { 
		this.Cust_Id = Cust_Id;
	}
	public String getCust_Id() {
		return this.Cust_Id;
	}

	public void setAcctId(String AcctId) throws InvalidDataException { 
		this.AcctId = AcctId;
	}
	public String getAcctId() {
		return this.AcctId;
	}

	public void setTrade_Date(String Trade_Date) throws InvalidDataException { 
		this.Trade_Date = Trade_Date;
	}
	public String getTrade_Date() {
		return this.Trade_Date;
	}

	public void setAmount(String Amount) throws InvalidDataException { 
		try { 
			this.Amount = Double.parseDouble(Amount);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("Amount is blank or contains incorrect value.");
		}
	}
	public Double getAmount() {
		return this.Amount;
	}

	public void setScheme_Name(String Scheme_Name) throws InvalidDataException { 
		this.Scheme_Name = Scheme_Name;
	}
	public String getScheme_Name() {
		return this.Scheme_Name;
	}

	public void setScheme_Type(String Scheme_Type) throws InvalidDataException { 
		this.Scheme_Type = Scheme_Type;
	}
	public String getScheme_Type() {
		return this.Scheme_Type;
	}

	public void setTran_Type(String Tran_Type) throws InvalidDataException { 
		this.Tran_Type = Tran_Type;
	}
	public String getTran_Type() {
		return this.Tran_Type;
	}

	public void setSub_Tran_Type(String Sub_Tran_Type) throws InvalidDataException { 
		this.Sub_Tran_Type = Sub_Tran_Type;
	}
	public String getSub_Tran_Type() {
		return this.Sub_Tran_Type;
	}

	public void setRM_Code(String RM_Code) throws InvalidDataException { 
		this.RM_Code = RM_Code;
	}
	public String getRM_Code() {
		return this.RM_Code;
	}

	public void setTran_Code(String Tran_Code) throws InvalidDataException { 
		this.Tran_Code = Tran_Code;
	}
	public String getTran_Code() {
		return this.Tran_Code;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }


	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }

}
