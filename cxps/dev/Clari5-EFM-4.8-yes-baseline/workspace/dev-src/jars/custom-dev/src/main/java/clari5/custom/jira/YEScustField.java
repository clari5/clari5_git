package clari5.custom.jira;
/*
Created by Deepak
*/
 class YEScustField {

     private String jiraId;
     private String jiraType;
     private String taskType;
     private String value;
     private String fieldName;

     public String getFieldName() {
         return fieldName;
     }

     public void setFieldName(String fieldName) {
         this.fieldName = fieldName;
     }

     public String getJiraId() {
         return jiraId;
     }

     public void setJiraId(String jiraId) {
         this.jiraId = jiraId;
     }

     public String getJiraType() {
         return jiraType;
     }

     public void setJiraType(String jiraType) {
         this.jiraType = jiraType;
     }

     public String getTaskType() {
         return taskType;
     }

     public void setTaskType(String taskType) {
         this.taskType = taskType;
     }

     public String getValue() {
         return value;
     }

     public void setValue(String value) {
         this.value = value;
     }

    @Override
    public String toString() {
        return "YEScustField{" +
                "jiraId='" + jiraId + '\'' +
                ", jiraType='" + jiraType + '\'' +
                ", taskType='" + taskType + '\'' +
                ", value='" + value + '\'' +
                ", fieldName='" + fieldName + '\'' +
                '}';
    }
}

