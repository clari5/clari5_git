cxps.noesis.glossary.entity.branch-master{
        db-name = BRANCH_MASTER1
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = brcode, column = brcode, type = "string:50"}
		{ name = brname, column = brname, type = "string:50"}
		{ name = category, column = category, type = "string:50"}
		{ name = cluster-name, column = cluster_name, type = "string:50"}
		{ name = region, column = region, type = "string:50"}
		{ name = state, column = state, type = "string:50"}
		{ name = city, column = city, type = "string:50"}
		{ name = branch-classification, column = branch_classification, type = "string:50"}
		{ name = dbbl-name, column = dbbl_name, type = "string:50"}
		{ name = bbl-name, column = bbl_name, type = "string:50"}
		{ name = csdl-name, column = csdl_name, type = "string:50"}
		{ name = cbl-name, column = cbl_name, type = "string:50"}
		{ name = rsdl-name, column = rsdl_name, type = "string:50"}
		{ name = sr-rbl-name, column = sr_rbl_name, type = "string:50"}
		{ name = bbh-name, column = bbh_name, type = "string:50"}
		{ name = rbl-name, column = rbl_name, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
