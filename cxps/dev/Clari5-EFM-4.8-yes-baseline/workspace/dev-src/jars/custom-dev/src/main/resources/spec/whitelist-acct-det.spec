cxps.noesis.glossary.entity.whitelist-acct-det{
        db-name = WHITELIST_ACCT_DET
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = account-id, column = account_id, type = "string:50", key=true }
		{ name = customer-id, column = customer_id, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
