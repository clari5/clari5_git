package clari5.custom.ubp.integration.builder;

import clari5.custom.ubp.integration.audit.AuditManager;
import clari5.custom.ubp.integration.audit.LogLevel;
import clari5.custom.ubp.integration.builder.data.Action;
import clari5.custom.ubp.integration.db.DBTask;
import clari5.platform.util.Hocon;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ActionMap extends HashMap<String, List<Action>> {
    public static CxpsLogger logger = CxpsLogger.getLogger(ActionMap.class);
    private static final long serialVersionUID = -4124541844681468730L;
    private static ActionMap actions = null;

    private ActionMap() {
        Hocon h = new Hocon();
        try {
            h.loadFromContext("tbl-action-mapping.config");
            Set<String> keys = h.getKeys();
            for (String k : keys) {
                Hocon th = h.get(k);
                Set<String> ths = th.getKeys();
                List<Action> la = new ArrayList<Action>();
                for (String kt : ths) {
                    Action a = new Action();
                    a.setDestinationTableName(kt);
                    a.setAction(th.getString(kt + ".Action"));
                    a.setSourceKeys(th.getStringList(kt + ".SourceKeys"));
                    a.setWhere(th.getStringList(kt + ".Where"));
                    a.setTarget(th.getString(kt + ".Target"));
                    a.setValue(th.getString(kt + ".Value"));
                    la.add(a);
                }
                this.put(k, la);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            throw e1;
        } finally {
        }
    }

    /*void process(JSONObject row) throws Exception {
        System.out.println("row data in ActionMap is: " + row);
        String eventname = row.getString("event-name");
        //getJSONObject("msgBody").
        System.out.println("eventname is : " + eventname);
        List<Action> actions = this.get(eventname);
        for (Action a : actions) {
            switch (a.getAction()) {
                case "SET": {
                    List<String> values = new ArrayList<String>();
                    for (String source : a.getSourceKeys()) {
                        values.add(row.getString(source));
                    }
                    DBTask.update(a.getDestinationTableName(), a.getTarget(), a.getValue(),
                            a.getWhere().toArray(new String[0]), values.toArray(new String[0]));
                }
            }
        }
    }*/

    void process(JSONObject row) throws Exception {
        String eventname = row.getString("event-name");
        List<Action> actions = this.get(eventname);
        if(actions != null && actions.size() > 0) {
            for(Action a : actions) {
                switch(a.getAction()) {
                    case "SET": {
                        List<String> values = new ArrayList<String>();
                        for(String source : a.getSourceKeys()) {
                            values.add(row.getString(source));
                        }
                        DBTask.update(a.getDestinationTableName(), a.getTarget(), a.getValue(),
                                a.getWhere().toArray(new String[0]), values.toArray(new String[0]));
                    }
                }
            }
        }
        else {
            logger.info("No action configured for the event, "+eventname);
        }
    }

    public static ActionMap getActionMap() {
        if (actions == null) {
            actions = new ActionMap();
        }
        return actions;
    }
}
