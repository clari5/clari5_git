package clari5.custom.ubp.integration.audit;

public interface IAuditClient {
	void log(String message);
	void log(LogLevel level, String message);
}
