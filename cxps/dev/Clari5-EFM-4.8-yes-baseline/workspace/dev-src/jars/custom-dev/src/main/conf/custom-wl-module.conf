wl {
  class : clari5.aml.wl.Wl
  refresh : 600
  indexInMem : false
  indexDir : ${CX_DATA_DIR}"/wl-lucene-index"
  jobDir : ${CX_DATA_DIR}"/wl-job"
  searchThreadCount : 4
  indexThreadCount : 4
  batchSize : 1000
  machineID : M1
  autoReindexOn : true
  reindexTimestamp : ${CX_DATA_DIR}"/reindex-timestamp.txt"
  reindexCommitSize : 400000
  indexRefreshTime : 1864000
  stopWordsFilePath : ${CX_DATA_DIR}"/wl-stop-words.txt"
  fieldsInfo : {
    #only these fields will be accepted by WL. else it should report error
    name { }
    country { analyzerName : lowercase }
    country_name { analyzerName : lowercase }
    dob { }
    list { analyze : false }
    entity_type { analyze : false }
    id { }
    mailing_address { }
    permanent_address { }
    resi_phone { }
    pan { }
    office_number { }
    doi{ }
    customer_address { }
    mobile_no { }
    cust_mobile { }
    cust_dob { }
  }
  defaultWt : 1
  listNameField : list
  #WlListTypeEnabled : [UN,OFAC,ACCUITY,CSV,OFAC,UN]
  WlListTypeEnabled : [ACCUITY,CSV]
  scorer {
    # used for both indexing and searching
    algoMetaInfo {
      simHash {
        nshingle : 3
        nfeature : 40
      }
    }
    enabled : [ simHash , charCount ]

    # used only for searching
    # these are default values and will be over-ridden by search-config values.
    extraSearchCount : 0
    individualCutoffs {
      simHash : 0.3
      charCount : 0.5
    }
  }
  uploader {
    table {
      table_name : CUSTOMER_MASTER
      mappings {
        #fieldinWl : #fieldinSource
        id:UCIC
        name : customer_name
        permanent_address : COMPLETE_PERMANENTADDR
        mailing_address :   COMPLETE_MAILINGADDR
        pan:PAN
        cust_dob:DOB
        doi:DATE_OF_INCORPORATION
        mobile_no:MOBILE
        
      }
    }
    accuity {
      mappings {
        #fieldinWl : #fieldinSource
        entity_type : [entityType]
        list : [listCode]
        name : [name]
        dob : [dob]
        addr1 : [address1]
        addr2 : [address2]
        city : [city]
        country : [country]
        country_name : [countryName]
        bic : [bic]
      }
    }
    csv {
      rowDelim : "\n"
      colDelim : "|"
      mappings {
        #fieldinWl : #fieldinSource
        name : [name]
        dob : [dob]
        pan : [pan]
        customer_address : [customer_address]
        doi : [doi]
        mobile_no: [mobile_no]
        #resi_phone : [tel_number]
        #office_number : [tel_number]
        list : [list_code]
      }
    }
    ofac {
      mappings {
        name : [firstName,lastName]
        dob :  [dateOfBirth]
        addr1 : [address1]
        addr2 : [address2,address3]
        country : [country]
        country_name : [country]
        city : [city]
      }
    }
    un {
      mappings {
        name : [FIRST_NAME,SECOND_NAME,THIRD_NAME,FOURTH_NAME]
        dob  : [DATE]
        addr1 : [STREET]
        addr2 : []
        country : [COUNTRY]
        country_name : [COUNTRY]
        city : [CITY]

      }
    }
  }
  searcher {
    customer {
      mappings {
        #fieldinCustomer : #fieldInWL
        name : name
        dob : dob
        nationality : country_name
        resAddr : addr1
        resAddrCity : city
        pan : tax_id
        bic : bic
        passport_number : passport_number
        mailing_address :mailing_address
        permanent_address:permanent_address
        tel_number:resi_phone
        pan:pan
        tel_number:office_number
        doi:doi
        customer_address:customer_address
        mobile_no : mobile_no
        dob:cust_dob
        MOBILE:cust_mobile
        DOB:cust_dob
      }
    }
    default {
      mappings {
        #fieldinCustomer : #fieldInWL
        name : name
        dob : dob
        nationality : country_name
        resAddr : addr1
        resAddrCity : city
        pan : tax_id
        bic : bic
        emirates_id : emirates_id
        mobile_no : mobile_no
        passport_number : passport_number
        mailing_address:mailing_address
        permanent_address:permanent_address
        #tel_number:resi_phone
        pan:pan
        #tel_number:office_number
        doi:doi
        customer_address:customer_address
        MOBILE:cust_mobile
        DOB:cust_dob
      }
    }
    beneficiary {
      mappings {
        #fieldinEvent : #fieldInWL
        BEN_NAME : name
        BEN_ADD1 : addr1
        BEN_ADD2 : addr2
        BEN_CNTRY_CODE : country
        BEN_BIC : bic
      }
    }
    remitter {
      mappings {
        #fieldinEvent : #fieldInWL
        REM_NAME : name
        REM_ADD1 : addr1
        REM_ADD2 : addr2
        REM_CNTRY_CODE : country
        REM_BIC : bic
      }
    }
  }
  csvBatchSearch {
    batchSize : 5000
    inPath : ${CX_DATA_DIR}"/wl-batch-search-in-path"
    outPath : ${CX_DATA_DIR}"/wl-batch-search-out-path"
    errPath : ${CX_DATA_DIR}"/wl-batch-search-err-path"
    rowDelim : "\n"
    colDelim : "|"
  }
}
