package clari5.custom.jira;

import java.util.*;

import clari5.platform.applayer.Clari5;
import clari5.platform.util.CxJson;
import clari5.aml.cms.jira.custom.IFieldAppender;
import clari5.platform.util.Hocon;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cxps.apex.utils.CxpsLogger;

import clari5.rdbms.Rdbms;


/*

Created by Deepak
*/
public class CustomFieldAppender implements IFieldAppender {

    private static CxpsLogger logger = CxpsLogger.getLogger(CustomFieldAppender.class);
    Map<String, YEScustField> fieldMap;
    Hocon customFieldParam = new Hocon();

    public CustomFieldAppender() {
        fieldMap = new HashMap<>();
    }

    public void init() {
        logger.info("Init method in CustomFieldAppender");
        Hocon fieldHocon = new Hocon();
        fieldHocon.loadFromContext("yesjiracustomfields.conf");
        Hocon moduleHocon = fieldHocon.get("clari5.yes.yesjiracustomfields");
        List<String> moduleList = moduleHocon.getKeysAsList();
        customFieldParam.loadFromContext("custFieldParammap.conf");
        fieldMap.clear();
        for (String moduleId : moduleList) {
            Hocon customHocon = moduleHocon.get(moduleId);
            Set<String> keys = customHocon.getKeys();
            for (String fieldName : keys) {
                YEScustField custField = new YEScustField();
                Hocon h = customHocon.get(fieldName);
                custField.setFieldName(fieldName);
                custField.setJiraId(h.getString("jiraId"));
                custField.setJiraType(h.getString("jiraType"));
                custField.setTaskType(h.getString("taskType"));
                custField.setValue(h.getString("fieldValue"));
                fieldMap.put(moduleId + ":" + fieldName, custField);
            }
        }
        logger.info("[init] FieldMap " + fieldMap.toString());
    }

    public void refresh() {

    }


    public Map<String, String> customFieldsJson(CxJson caseJson, String taskType, String moduleId) {
        logger.info("case json is " + caseJson.toString() +"\n taskType ["+taskType+"] and moduleId ["+moduleId+"]" );
        Map<String, String> newFieldsMap = new HashMap<>();

        switch (taskType) {
            case "cmsIncidents":
                String entityName =caseJson.getString(customFieldParam.getString("wsFields.wsName"));
                String wsId = caseJson.getString(customFieldParam.getString("wsFields.wsId"));
                //String entityName = caseJson.getString("customfield_10505");
                logger.info("enttityname is [" + entityName+"]");

                switch (entityName.toLowerCase().trim()) {
                    case "customer":

                        wsId = wsId.substring(4);
                        logger.info(" Customer with id [" + wsId+"]");
                        return getCustomerFields(wsId, caseJson, moduleId, null);

                    case "account":

                        wsId = wsId.substring(4);
                        logger.info("Account with id ["+ wsId+"]");
                        return getAccountFields(wsId, caseJson, moduleId);

                    default:
                        return newFieldsMap;
                }
        }

        return newFieldsMap;


    }

    public Connection getDBConnection() {
        Connection con = null;
        try {
            con = Rdbms.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }


    public Map<String, String> getCustomerFields(String cifnumber, CxJson caseJson, String moduleId, String acctNumber) {
        Map<String, String> newFieldsMap = new HashMap<>();
        Connection con = null;
        Map<String, String> dbFields = new HashMap<>();
        ResultSet rs = null;
        PreparedStatement psmt = null;
        try {
            logger.info("[getCustomerFields] Going to get connection ");
            con = getDBConnection();

            String query = "select  CUSTOMER_MASTER.MAILING_CITY,CUSTOMER_MASTER.MAILING_COUNTRY,CUSTOMER_MASTER.UCIC,CUSTOMER_MASTER.MAILING_ADDRESS_1,CUSTOMER_MASTER.MAILING_ADDRESS_2,CUSTOMER_MASTER.MAILING_ADDRESS_3,CUSTOMER_MASTER.MOBILE,CUSTOMER_MASTER.TURNOVER,CUSTOMER_MASTER.EMAIL_ID,CUSTOMER_MASTER.CUSTOMER_TYPE||'|'||CUSTOMER_MASTER.CUSTOMER_DESCRIPTION AS CUSTOMER_TYPE,CUSTOMER_MASTER.BUSINESS,CUSTOMER_MASTER.NATURE_OF_BUSINESS,CUSTOMER_MASTER.CUSTOMER_NAME,CUSTOMER_MASTER.DOB ,CUSTOMER_MASTER.MAILING_ADDRESS_1,CUSTOMER_MASTER.MAILING_ADDRESS_2,CUSTOMER_MASTER.MAILING_ADDRESS_3 from CUSTOMER_MASTER where  CUSTOMER_MASTER.CUSTOMER_ID =?";

            logger.info("[getCustomerFields] Query [" + query + "] and prameters [" + cifnumber + "]");
            psmt = con.prepareStatement(query);
            psmt.setString(1, cifnumber);
            rs = psmt.executeQuery();
            logger.info("[getCustomerFields] Result set data [" + rs.toString() + "]");
            if (rs.next()) {

                dbFields.put("dob", objToStr(rs.getDate("DOB")));
                dbFields.put("ucic", rs.getString("UCIC"));
                dbFields.put("mailing_city", rs.getString("MAILING_CITY"));
                dbFields.put("mailing_country", rs.getString("MAILING_COUNTRY"));
                dbFields.put("mobileNo", rs.getString("MOBILE"));
                dbFields.put("emailId", rs.getString("EMAIL_ID"));
                dbFields.put("turnover", rs.getString("TURNOVER"));
                dbFields.put("mailing_address1", rs.getString("MAILING_ADDRESS_1"));
                dbFields.put("mailing_address2", rs.getString("MAILING_ADDRESS_2"));
                dbFields.put("mailing_address3", rs.getString("MAILING_ADDRESS_3"));
                dbFields.put("customer_type", objToStr(rs.getString("CUSTOMER_TYPE")));
                dbFields.put("business", rs.getString("BUSINESS"));
                dbFields.put("natureof_business", rs.getString("NATURE_OF_BUSINESS"));
                dbFields.put("customer_name", rs.getString("CUSTOMER_NAME"));
                dbFields.put("account_status", "NA");
                dbFields.put("acccount_open_date", "NA");
                dbFields.put("account_type", "NA");
                dbFields.put("branch", "NA");

                logger.info("[getCustomerFields] Custom db fields " + dbFields.toString());

            }
        } catch (SQLException sqe) {
            logger.error("[getCustomerFields] Failed on db operation " + sqe.getMessage() + " Cause " + sqe.getCause());
        } catch (Exception e) {
            logger.error("[getCustomerFields]  Unable to append the custom fields " + e.getMessage() + " Cause " + e.getCause());
        } finally {
            if (con != null) {
                try {
                    rs.close();
                    psmt.close();
                    con.close();
                } catch (SQLException ignore) {
                }
            }

            for (Map.Entry<String, YEScustField> fielddata : fieldMap.entrySet()) {
                try {
                    YEScustField custField = fielddata.getValue();
                    newFieldsMap.put(custField.getJiraId(), getCustomFieldValue(dbFields.get(customFieldParam.getString("custFields." + custField.getJiraId()))));
                } catch (Exception e) {
                    logger.error("[getCustomerFields] unable to get custom field data " + fielddata.toString() + "check custFieldParammap.conf file with jiraId" + "\n Exception " + e.getMessage() + " Cause " + e.getCause());
                    continue;
                }
            }


        }
        dbFields.clear();
        logger.info("[getCustomerFields] custom-field map  of customer data  " + newFieldsMap.toString());
        return newFieldsMap;
    }


    public Map<String, String> getAccountFields(String acctNumber, CxJson caseJson, String moduleId) {
        Map<String, String> newFieldsMap = new HashMap<>();
        Connection con = null;
        Map<String, String> dbFields = new HashMap<>();
        ResultSet rs = null;
        PreparedStatement psmt = null;

        try {
            logger.info("[getAccountFields] Going to get connection ");

            con = getDBConnection();
            logger.info("[getAccountFields] Fetching data from customer_details connection " + con.toString());

            String query = "select ACCOUNT_MASTER.ACCCOUNT_OPEN_DATE,ACCOUNT_MASTER.ACCOUNT_STATUS||'|'||ACCOUNT_STATUS_MASTER.TXT_ACCT_STATUS AS ACCOUNT_STATUS ,ACCOUNT_MASTER.ACCOUNT_TYPE,ACCOUNT_MASTER.BRANCH_CODE||'|'||BRANCH_MASTER.BRANCH_NAME AS BRANCH_CODE ,CUSTOMER_MASTER.MAILING_CITY,CUSTOMER_MASTER.MAILING_COUNTRY,\n" +
                    "CUSTOMER_MASTER.UCIC,CUSTOMER_MASTER.MAILING_ADDRESS_1,CUSTOMER_MASTER.MAILING_ADDRESS_2,\n" +
                    "CUSTOMER_MASTER.MAILING_ADDRESS_3,CUSTOMER_MASTER.MOBILE,CUSTOMER_MASTER.TURNOVER,CUSTOMER_MASTER.EMAIL_ID,CUSTOMER_MASTER.CUSTOMER_TYPE||'|'||CUSTOMER_MASTER.CUSTOMER_DESCRIPTION AS CUSTOMER_TYPE,CUSTOMER_MASTER.BUSINESS,CUSTOMER_MASTER.NATURE_OF_BUSINESS,CUSTOMER_MASTER.CUSTOMER_NAME,CUSTOMER_MASTER.DOB ,\n" +
                    "ACCOUNT_MASTER.AVL_BAL from ACCOUNT_MASTER, CUSTOMER_MASTER,ACCOUNT_STATUS_MASTER,BRANCH_MASTER  where  ACCOUNT_MASTER.ACCOUNT_ID = ? and ACCOUNT_MASTER.CUSTOMER_ID=CUSTOMER_MASTER.CUSTOMER_ID and ACCOUNT_MASTER.ACCOUNT_STATUS=ACCOUNT_STATUS_MASTER.COD_ACCT_STATUS and ACCOUNT_MASTER.BRANCH_CODE=BRANCH_MASTER.BRANCH_SOL_ID";
            logger.info("[getAccountFields] Query " + query + " and parameter is [" + acctNumber + "]");

            psmt = con.prepareStatement(query);
            psmt.setString(1, acctNumber);

            rs = psmt.executeQuery();
            logger.info("[getAccountFields] Result set data [" + rs.toString() + "]");

            if (rs.next()) {
                dbFields.put("dob", objToStr(rs.getDate("DOB")));
                dbFields.put("ucic", rs.getString("UCIC"));
                dbFields.put("mailing_city", rs.getString("MAILING_CITY"));
                dbFields.put("mailing_country", rs.getString("MAILING_COUNTRY"));
                dbFields.put("mobileNo", rs.getString("MOBILE"));
                dbFields.put("emailId", rs.getString("EMAIL_ID"));
                dbFields.put("turnover", rs.getString("TURNOVER"));
                dbFields.put("mailing_address1", rs.getString("MAILING_ADDRESS_1"));
                dbFields.put("mailing_address2", rs.getString("MAILING_ADDRESS_2"));
                dbFields.put("mailing_address3", rs.getString("MAILING_ADDRESS_3"));
                dbFields.put("customer_type", objToStr(rs.getString("CUSTOMER_TYPE")));
                dbFields.put("business", rs.getString("BUSINESS"));
                dbFields.put("natureof_business", rs.getString("NATURE_OF_BUSINESS"));
                dbFields.put("customer_name", rs.getString("CUSTOMER_NAME"));
                dbFields.put("account_status", objToStr(rs.getString("ACCOUNT_STATUS")));
                dbFields.put("acccount_open_date", objToStr(rs.getDate("ACCCOUNT_OPEN_DATE")));
                dbFields.put("account_type", rs.getString("ACCOUNT_TYPE"));
                dbFields.put("branch", objToStr(rs.getString("BRANCH_CODE")));
                dbFields.put("avl_bal", objToStr(Integer.toString(rs.getInt("AVL_BAL"))));

                logger.info("[getAccountFields] Custom db fields " + dbFields.toString());


            }
        } catch (SQLException sqe) {
            logger.error("[getAccountFields] Failed on db operation " + sqe.getMessage() + " Cause " + sqe.getCause());
        } catch (Exception e) {
            logger.error("[getAccountFields]  Unable to append the custom fields " + e.getMessage() + " Cause " + e.getCause());
        } finally {

            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ignore) {
                }
            }
        }
        for (Map.Entry<String, YEScustField> fielddata : fieldMap.entrySet()) {
            try {
                YEScustField custField = fielddata.getValue();
                newFieldsMap.put(custField.getJiraId(), getCustomFieldValue(dbFields.get(customFieldParam.getString("custFields." + custField.getJiraId()))));
            } catch (Exception e) {
                logger.error("[getAccountFields] unable to get custom field data " + fielddata.toString() + "check custFieldParammap.conf file with jiraId" + "\n Exception " + e.getMessage() + " Cause " + e.getCause());
                continue;
            }
        }

        dbFields.clear();
        logger.info("[getAccountFields] custom field map  of account data  " + newFieldsMap.toString());
        return newFieldsMap;
    }


    public static String getCustomFieldValue(String value) {
        if (value != null && !"".equals(value)) {
            return value;
        } else {
            return "NA";
        }

    }

    public String objToStr(Object value) {
        try {
            int idx;
            if (null == value) {
                return "NA";

            }else if( (idx = value.toString().indexOf('|'))!= -1){
                String val ="";
                String oldVal = value.toString();
                if(value.toString().length() ==1)return "NA|NA";
                val = (idx == 0) ? "NA"+oldVal: ((oldVal.length()-1)-idx ==0)? oldVal+"NA":oldVal;
                return val;
            }
            else
                 return value.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "NA";
        }
    }

   /* public static void main(String[] args)throws Exception {
        Clari5.batchBootstrap("dbcon","syncer-clari5.conf");
        System.out.println("start proceess");
        String json = "{\"parent\":{\"key\":\"EFM-1195\"},\"project\":{\"key\":\"EFM\"},\"issuetype\":{\"id\":\"5\"},\"assignee\":{\"name\":\"Bob\"},\"customfield_10900\":\"alert \",\"summary\":\"12345 | Finacle Core:CHECK_CUSTOM_FIELD_INTU:alert \",\"customfield_10503\":\"Evidence for trigger\\r\\ncxps.events.ft_coreaccttxnevent\\r\\n11est\\r\\n\\r\\n\\r\\n\",\"\":\"https://dn.placeholder.com:1111/efm/cef/index.html\",\"customfield_10211\":\"CHECK_CUSTOM_FIELD_INTU\",\"customfield_11607\":\"11est4A_F_12345CHECK_CUSTOM_FIELD_INTU\",\"customfield_10513\":\"CUSTOMER\",\"customfield_10516\":\"11 | Finacle Core\",\"customfield_11401\":\"A_F_151\",\"customfield_10505\":\"account\",\"customfield_10213\":\"2018-03-16 15:04:47.372\",\"customfield_10212\":\"500\",\"customfield_10214\":\"12345 | Finacle Core\",\"customfield_10610\":\"L3\",\"customfield_10703\":\"Not Available\",\"customfield_10704\":\"Not Available\",\"customfield_10705\":\"Not Available\",\"customfield_10215\":\"Not Available\"}";
        CustomFieldAppender customFieldAppender = new CustomFieldAppender();
        customFieldAppender.init();
        System.out.println(customFieldAppender.customFieldsJson(CxJson.parse(json),"cmsIncidents","EFM").toString());

        System.out.println("End processor");


    }*/

   
}

