package cxps.events;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;

import clari5.custom.Queries;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.dbcon.QueryMapper;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_DCPinChangeRequestEvent extends Event {
	// private String source;
	private String host_id;
	private Date sys_time;
	private String event_id;
	private String succ_fail_flg;
	private String error_code;
	private String error_desc;
	private String card_no;
	private String cust_id;
	private String time_slot;
	private String Online_offline;
	private String rqst_chnl;
	private String eventtype;
	private String eventsubtype;
	private String event_name;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		// setSource((String) msgMap.get("source"));
		setHost_id((String) msgMap.get("host_id"));
		setSys_time1((String) msgMap.get("sys_time"));
		setEvent_id((String) msgMap.get("event_id"));
		setSucc_fail_flg((String) msgMap.get("succ_fail_flg"));
		setError_code((String) msgMap.get("error_code"));
		setError_desc((String) msgMap.get("error_desc"));
		setCard_no((String) msgMap.get("card_no"));
		setCust_id((String) msgMap.get("cust_id"));
		setTime_slot((String) msgMap.get("time_slot"));
		setOnline_offline((String) msgMap.get("Online_offline"));
		setRqst_chnl((String) msgMap.get("rqst_chnl"));
		setEventtype((String) msgMap.get("eventtype"));
		setEventsubtype((String) msgMap.get("eventsubtype"));
		setEvent_name((String) msgMap.get("event_name"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_DCPinChangeRequestEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		/*
		String hostAcctKey = getAccountId ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		*/
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/

		/*String cxPaymentcardKey = "";
		if (getCard_no() != null && getCard_no().trim().length() > 0) {
			cxPaymentcardKey = h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), getCard_no());
		} else {
			System.out.println("PayementCard Key found null..................");
		}
		WorkspaceInfo wip = new WorkspaceInfo(Constants.KEY_REF_TYP_PAYMENTCARD, cxPaymentcardKey);
		wsInfoSet.add(wip);
*/

		String cxCifId = "";
		String hostCustKey = getCust_id();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		cxLog.fexit();
		return wsInfoSet;
	}

/*	
public void setSource(String source) {
		this.source = source;
	}
	public String getSource() {
		return this.source;
	}

*/
	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setSys_time1(String sys_time) {
		try { 
			this.sys_time = dateFormat1.parse(sys_time);
		} catch (Exception ex) {
			Calendar c = Calendar.getInstance();
			c.set(1900, 1, 1, 1, 1, 1);
			this.sys_time = c.getTime();
		}
	}
	public void setSys_time(Date sys_time){
		this.sys_time = sys_time;
	}
	public Date getSys_time() {
		return this.sys_time;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}
	public String getEvent_id() {
		return this.event_id;
	}

	public void setSucc_fail_flg(String succ_fail_flg) {
		this.succ_fail_flg = succ_fail_flg;
	}
	public String getSucc_fail_flg() {
		return this.succ_fail_flg;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public String getError_code() {
		return this.error_code;
	}

	public void setError_desc(String error_desc) {
		this.error_desc = error_desc;
	}
	public String getError_desc() {
		return this.error_desc;
	}

	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getCard_no() {
		return this.card_no;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getCust_id() {
		return this.cust_id;
	}

	public void setTime_slot(String time_slot) {
		this.time_slot = time_slot;
	}
	public String getTime_slot() {
		return this.time_slot;
	}

	public void setOnline_offline(String Online_offline) {
		this.Online_offline = Online_offline;
	}
	public String getOnline_offline() {
		return this.Online_offline;
	}

	public void setRqst_chnl(String rqst_chnl) {
		this.rqst_chnl = rqst_chnl;
	}
	public String getRqst_chnl() {
		return this.rqst_chnl;
	}

	public void setEventtype(String eventtype) throws InvalidDataException { 
		this.eventtype = eventtype;
	}
	public String getEventtype() {
		return this.eventtype;
	}

	public void setEventsubtype(String eventsubtype) throws InvalidDataException { 
		this.eventsubtype = eventsubtype;
	}
	public String getEventsubtype() {
		return this.eventsubtype;
	}

	public void setEvent_name(String event_name) throws InvalidDataException { 
		this.event_name = event_name;
	}
	public String getEvent_name() {
		return this.event_name;
	}

}
