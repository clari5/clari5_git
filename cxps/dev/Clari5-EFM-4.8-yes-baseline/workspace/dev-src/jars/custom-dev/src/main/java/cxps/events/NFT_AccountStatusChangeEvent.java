package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_AccountStatusChangeEvent extends Event {
	private String account_id;
	private String cust_id;
	private String init_acct_status;
	private String final_acct_status;
	private Double avl_bal;
	private String user_id;
	private String branch_id;
	private String branch_id_desc;
	private Date acct_open_dt;
//	private Date eventts;
	private Date sys_time;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setAccount_id((String) msgMap.get("account_id"));
		setCust_id((String) msgMap.get("cust_id"));
		setInit_acct_status((String) msgMap.get("init_acct_status"));
		setFinal_acct_status((String) msgMap.get("final_acct_status"));
		setAvl_bal((String) msgMap.get("avl_bal"));
		setUser_id((String) msgMap.get("user_id"));
		setBranch_id((String) msgMap.get("branch_id"));
		setBranch_id_desc((String) msgMap.get("branch_id_desc"));
		setAcct_open_dt((String) msgMap.get("acct_open_dt"));
//		setEventts1((String) msgMap.get("eventts"));
		setSys_time1((String) msgMap.get("sys_time"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_AccountStatusChange");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAccount_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		String cxCifId = "";
		String hostCustKey = getCust_id();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setAccount_id(String account_id) throws InvalidDataException { 
		this.account_id = account_id;
	}
	public String getAccount_id() {
		return this.account_id;
	}

	public void setCust_id(String cust_id) throws InvalidDataException { 
		this.cust_id = cust_id;
	}
	public String getCust_id() {
		return this.cust_id;
	}

	public void setInit_acct_status(String init_acct_status) throws InvalidDataException { 
		this.init_acct_status = init_acct_status;
	}
	public String getInit_acct_status() {
		return this.init_acct_status;
	}

	public void setFinal_acct_status(String final_acct_status) throws InvalidDataException { 
		this.final_acct_status = final_acct_status;
	}
	public String getFinal_acct_status() {
		return this.final_acct_status;
	}

	public void setAvl_bal(String avl_bal) throws InvalidDataException { 
		try { 
			this.avl_bal = Double.parseDouble(avl_bal);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("avl_bal is blank or contains incorrect value.");
		}
	}
	public Double getAvl_bal() {
		return this.avl_bal;
	}

	public void setUser_id(String user_id) throws InvalidDataException { 
		this.user_id = user_id;
	}
	public String getUser_id() {
		return this.user_id;
	}

	public void setBranch_id(String branch_id) throws InvalidDataException { 
		this.branch_id = branch_id;
	}
	public String getBranch_id() {
		return this.branch_id;
	}

	public void setBranch_id_desc(String branch_id_desc) throws InvalidDataException { 
		this.branch_id_desc = branch_id_desc;
	}
	public String getBranch_id_desc() {
		return this.branch_id_desc;
	}

	public void setAcct_open_dt(String acct_open_dt) throws InvalidDataException { 
		try { 
			this.acct_open_dt = dateFormat1.parse(acct_open_dt);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("acct_open_dt is blank or contains incorrect value.");
		}
	}
	public Date getAcct_open_dt() {
		return this.acct_open_dt;
	}

/*	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }*/
	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

}
