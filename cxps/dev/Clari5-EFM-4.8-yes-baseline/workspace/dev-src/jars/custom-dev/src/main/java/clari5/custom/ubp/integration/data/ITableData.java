package clari5.custom.ubp.integration.data;

import clari5.custom.ubp.integration.builder.EventBuilder;
import clari5.custom.ubp.integration.builder.MsgMetaData;
import clari5.custom.ubp.integration.data.bootstrap.TableMap;

public abstract class ITableData {



	public boolean process() throws Exception {
		MsgMetaData m = TableMap.getTableMap().get(this.getTableName());
		EventBuilder eb = new EventBuilder();
		eb.process(this);
		return true;
	}
	public abstract String getTableName();
}
