package cxps.events;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;

import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;


public class NFT_FreezeAcctEvent extends Event {
    //private String eventId;
    private String cust_Id;
    private String acct_Id;
    private String debit_Freeze;
    private Double aval_Balance;
    private Date sys_time;
    private transient SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private transient SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    private transient SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");

    
    public String getCust_Id() {
        return cust_Id;
    }

    public void setCust_Id(String cust_Id) {
        if (StringUtils.isNullOrEmpty(cust_Id)) {
            throw new IllegalArgumentException("Value is required: 'cust_id'");
        } else {
            this.cust_Id = cust_Id;
        }
    }

    public String getDebit_Freeze() {
        return debit_Freeze;
    }

    public void setDebit_Freeze(String debit_Freeze) {
        if (StringUtils.isNullOrEmpty(debit_Freeze)) {
            throw new IllegalArgumentException("Value is required: 'debit_freeze'");
        } else {
            this.debit_Freeze = debit_Freeze;
        }
    }

    public String getAcct_Id() {
        return acct_Id;
    }

    public void setAcct_Id(String acct_Id) {
        if (StringUtils.isNullOrEmpty(acct_Id)) {
            throw new IllegalArgumentException("Value is required: 'acct_id'");
        } else {
            this.acct_Id = acct_Id;
        }
    }
   
    public void setSys_time1(String sys_time1) throws InvalidDataException {
	   try {
		  this.sys_time = dateFormat1.parse(sys_time1);
               } catch (Exception ex) {									                            ex.printStackTrace();
											                            throw new InvalidDataException("sys_time is blank or contains incorrect value.");															                    }
	        }
     public void setSys_time(Date sys_time) throws InvalidDataException {
                 this.sys_time = sys_time;
       }
	    public Date getSys_time() {
		    return this.sys_time;
      }

    public Double getAval_Balance() {
        return aval_Balance;
    }


    public void setAval_Balance(String aval_Balance) {
        if (StringUtils.isNullOrEmpty(aval_Balance)) {
            throw new IllegalArgumentException("Value is required: aval_balance");
        } else {
            try {
                double aval_Balancenew = Double.parseDouble(aval_Balance);
                this.aval_Balance = aval_Balancenew;
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid Value>field:aval_balance>value:" + aval_Balance);
            }
        }
    }

    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setEventType("nft");
        setEventSubType("freeze_acct");
        setEventName("nft_freeze_acct");
        setHostId("F");
        setAval_Balance((String) msgMap.get("aval-balance"));
        setCust_Id((String) msgMap.get("cust_id;"));
        setAcct_Id((String) msgMap.get("acct_id"));
        setDebit_Freeze((String) msgMap.get("debit_freeze"));
	setSys_time1((String) msgMap.get("sys_time"));
    }

    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_FreezeAcctEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
	/*	String hostAcctKey = getAccount_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
*/



     String cxCifId = "";
		String hostCustKey = getCust_Id();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
		cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(),hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		cxLog.fexit();
		return wsInfoSet;
	}
}



