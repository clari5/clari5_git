package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_ChequeReturnEvent extends Event {
	private String acct_id;
	private String user_id;
	private String cust_id;
	private String branchid;
	private String branchiddesc;
	private String acct_status;
	private String acct_name;
	private String Cheque_Number;
	private String Rejection_Code;
	private String Rejection_Reason;
	private String Transaction_numonic_code;
	private String host_id;
	private Date sys_time;
	private Date eventts;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setAcct_id((String) msgMap.get("acct_id"));
		setUser_id((String) msgMap.get("user_id"));
		setCust_id((String) msgMap.get("cust_id"));
		setBranchid((String) msgMap.get("branchid"));
		setBranchiddesc((String) msgMap.get("branchiddesc"));
		setAcct_status((String) msgMap.get("acct_status"));
		setAcct_name((String) msgMap.get("acct_name"));
		setCheque_Number((String) msgMap.get("Cheque_Number"));
		setRejection_Code((String) msgMap.get("Rejection_Code"));
		setRejection_Reason((String) msgMap.get("Rejection_Reason"));
		setTransaction_numonic_code((String) msgMap.get("Transaction_numonic_code"));
		setHost_id((String) msgMap.get("host_id"));
		setSys_time1((String) msgMap.get("sys_time"));
		setEventts1((String) msgMap.get("eventts"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_ChequeReturnEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAcct_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		/*
		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		*/
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setAcct_id(String acct_id) throws InvalidDataException { 
		this.acct_id = acct_id;
	}
	public String getAcct_id() {
		return this.acct_id;
	}

	public void setUser_id(String user_id) throws InvalidDataException { 
		this.user_id = user_id;
	}
	public String getUser_id() {
		return this.user_id;
	}

	public void setCust_id(String cust_id) throws InvalidDataException { 
		this.cust_id = cust_id;
	}
	public String getCust_id() {
		return this.cust_id;
	}

	public void setBranchid(String branchid) throws InvalidDataException { 
		this.branchid = branchid;
	}
	public String getBranchid() {
		return this.branchid;
	}

	public void setBranchiddesc(String branchiddesc) throws InvalidDataException { 
		this.branchiddesc = branchiddesc;
	}
	public String getBranchiddesc() {
		return this.branchiddesc;
	}

	public void setAcct_status(String acct_status) throws InvalidDataException { 
		this.acct_status = acct_status;
	}
	public String getAcct_status() {
		return this.acct_status;
	}

	public void setAcct_name(String acct_name) throws InvalidDataException { 
		this.acct_name = acct_name;
	}
	public String getAcct_name() {
		return this.acct_name;
	}

	public void setCheque_Number(String Cheque_Number) throws InvalidDataException { 
		this.Cheque_Number = Cheque_Number;
	}
	public String getCheque_Number() {
		return this.Cheque_Number;
	}

	public void setRejection_Code(String Rejection_Code) {
		this.Rejection_Code = Rejection_Code;
	}
	public String getRejection_Code() {
		return this.Rejection_Code;
	}

	public void setRejection_Reason(String Rejection_Reason) {
		this.Rejection_Reason = Rejection_Reason;
	}
	public String getRejection_Reason() {
		return this.Rejection_Reason;
	}

	public void setTransaction_numonic_code(String Transaction_numonic_code) throws InvalidDataException { 
		this.Transaction_numonic_code = Transaction_numonic_code;
	}
	public String getTransaction_numonic_code() {
		return this.Transaction_numonic_code;
	}

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }
		
	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }
	
}
