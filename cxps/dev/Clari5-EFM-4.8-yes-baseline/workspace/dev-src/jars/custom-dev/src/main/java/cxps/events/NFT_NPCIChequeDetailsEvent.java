package cxps.events;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import java.text.ParseException;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class NFT_NPCIChequeDetailsEvent extends Event {
	private String Cheque_Num;
	private String Account_Id;
	private Double Amount;
	private String Newly_Opened_Account;
	private String Cheque_Number_t;
	private String micr_no_t;
	private String SAN_t;
	private String TC_t;
	private String in_out_flag;
	private Date sys_time;
	private String host_id;
	private Date eventts;
	@Override
	public void fromMap (Map<String, ? extends Object> msgMap) throws InvalidDataException {
		super.fromMap(msgMap);
		setCheque_Num((String) msgMap.get("Cheque_Num"));
		setAccount_Id((String) msgMap.get("Account_Id"));
		setAmount((String) msgMap.get("Amount"));
		setNewly_Opened_Account((String) msgMap.get("Newly_Opened_Account"));
		setCheque_Number_t((String) msgMap.get("Cheque_Number_t"));
		setMicr_no_t((String) msgMap.get("micr_no_t"));
		setSAN_t((String) msgMap.get("SAN_t"));
		setTC_t((String) msgMap.get("TC_t"));
		setIn_out_flag((String) msgMap.get("in_out_flag"));
		setSys_time1((String) msgMap.get("sys_time"));
		setHost_id((String) msgMap.get("host_id"));
		setEventts1((String) msgMap.get("eventts"));
	}

	public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_NPCIChequeDetailsEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
		String cxAcctKey = "";
		String hostAcctKey = getAccount_Id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
		/*
		String cxUserKey = "";
		cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getUserId());
		WorkspaceInfo wuser = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
		wsInfoSet.add(wuser);
		*/


		/*
		String cxBranchKey = "";
		cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(),getBranchId());
		WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_BRANCH,cxBranchKey);
		wsInfoSet.add(wbr);
		*/


		/*
		String cxCifId = "";
		String hostCustKey = getCustId();
		if (null != hostCustKey && hostCustKey.trim().length() > 0) {
			cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
		} else {
			cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);
		}
		wsInfoSet.add(new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId));
		*/
		cxLog.fexit();
		return wsInfoSet;
	}

	public void setCheque_Num(String Cheque_Num) throws InvalidDataException { 
		this.Cheque_Num = Cheque_Num;
	}
	public String getCheque_Num() {
		return this.Cheque_Num;
	}

	public void setAccount_Id(String Account_Id) throws InvalidDataException { 
		this.Account_Id = Account_Id;
	}
	public String getAccount_Id() {
		return this.Account_Id;
	}

	public void setAmount(String Amount) throws InvalidDataException { 
		try { 
			this.Amount = Double.parseDouble(Amount);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new InvalidDataException("Amount is blank or contains incorrect value.");
		}
	}
	public Double getAmount() {
		return this.Amount;
	}

	public void setNewly_Opened_Account(String Newly_Opened_Account) throws InvalidDataException { 
		this.Newly_Opened_Account = Newly_Opened_Account;
	}
	public String getNewly_Opened_Account() {
		return this.Newly_Opened_Account;
	}

	public void setCheque_Number_t(String Cheque_Number_t) throws InvalidDataException { 
		this.Cheque_Number_t = Cheque_Number_t;
	}
	public String getCheque_Number_t() {
		return this.Cheque_Number_t;
	}

	public void setMicr_no_t(String micr_no_t) throws InvalidDataException { 
		this.micr_no_t = micr_no_t;
	}
	public String getMicr_no_t() {
		return this.micr_no_t;
	}

	public void setSAN_t(String SAN_t) throws InvalidDataException { 
		this.SAN_t = SAN_t;
	}
	public String getSAN_t() {
		return this.SAN_t;
	}

	public void setTC_t(String TC_t) throws InvalidDataException { 
		this.TC_t = TC_t;
	}
	public String getTC_t() {
		return this.TC_t;
	}

	public void setIn_out_flag(String in_out_flag) throws InvalidDataException { 
		this.in_out_flag = in_out_flag;
	}
	public String getIn_out_flag() {
		return this.in_out_flag;
	}

	public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

	public void setHost_id(String host_id) throws InvalidDataException { 
		this.host_id = host_id;
	}
	public String getHost_id() {
		return this.host_id;
	}

	public void setEventts1(String eventts1) {
                try {
                        this.eventts = dateFormat1.parse(eventts1);
                } catch (Exception ex) {
                        Calendar c = Calendar.getInstance();
                        c.set(1900, 1, 1, 1, 1, 1);
                        this.eventts = c.getTime();
                }
        }
	public void setEventts(Date eventts) throws InvalidDataException {
                        this.eventts = eventts;
        }
        public Date getEventts() {
                return this.eventts;
        }

}
