package cxps.events;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;

import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import cxps.noesis.constants.EvtConst;
import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;
import org.json.JSONException;
import org.json.JSONObject;


public class NFT_EmployeeBranchEvent extends Event {
    private String employee_ID;
    private String notice_period;
    private String branch;
    private String current_Branch;
    private Date sys_time;
    private Date branch_Joining_Date;
    private String designation;

    private transient SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private transient SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    private transient SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");

    
    public String getEmployee_ID() {
        return employee_ID;
    }

    public void setEmployee_ID(String employee_ID) {
        if (StringUtils.isNullOrEmpty(employee_ID)) {
            throw new IllegalArgumentException("Value is required: 'employee_ID'");
        } else {
            this.employee_ID = employee_ID;
        }
    }

    public String getNotice_period() {
        return notice_period;
    }

    public void setNotice_period(String notice_period) {
        if (StringUtils.isNullOrEmpty(notice_period)) {
            throw new IllegalArgumentException("Value is required: 'notice_period'");
        } else {
            this.notice_period = notice_period;
        }
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
            this.branch = branch;
        }
    
   
    public void setSys_time1(String sys_time1) throws InvalidDataException {
                try {
                        this.sys_time = dateFormat1.parse(sys_time1);
                } catch (Exception ex) {
                        ex.printStackTrace();
                        throw new InvalidDataException("sys_time is blank or contains incorrect value.");
                }
        }
        public void setSys_time(Date sys_time) throws InvalidDataException {
                        this.sys_time = sys_time;
        }
        public Date getSys_time() {
                return this.sys_time;
        }

    public void setBranch_Joining_Date1(String branch_Joining_Date) {
        try {
		  this.branch_Joining_Date = dateFormat1.parse(branch_Joining_Date);
               } catch (Exception ex) {									              				                   ex.printStackTrace();
              	
                 //throw new InvalidDataException("branch_Joining_Date is blank or contains incorrect value.");
    		}
	}
/*    public void setBranch_Joining_Date(Date branch_Joining_Date) throws InvalidDataException {
                        this.branch_Joining_Date = branch_Joining_Date;
        }*/
        public Date getBranch_Joining_Date() {
                return this.branch_Joining_Date;
        }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        if (StringUtils.isNullOrEmpty(designation)) {
            throw new IllegalArgumentException("Value is required: 'designation'");
        } else {
            this.designation = designation;
        }
    }
	
	public String getCurrent_Branch() {
        return current_Branch;
    }

    public void setCurrent_Branch(String current_Branch) {
            this.current_Branch = current_Branch;
        }
    
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
/*        setEventType("nft");
        setEventSubType("freeze_acct");
        setEventName("nft_freeze_acct");
        setHostId("F"); */
        setEmployee_ID((String) msgMap.get("employee_id"));
        setNotice_period((String) msgMap.get("notice_period"));
        setBranch((String) msgMap.get("branch"));
        setCurrent_Branch((String) msgMap.get("current_branch"));
	setSys_time1((String) msgMap.get("sys_time"));
	setBranch_Joining_Date1((String) msgMap.get("branch_joining_date"));
	setDesignation((String) msgMap.get("designation"));
    }

    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
		ICXLog cxLog = CXLog.fenter("derive.NFT_EmployeeBranchEvent");
		Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
		CxKeyHelper h = Hfdb.getCxKeyHelper();
//		String cxAcctKey = "";
	/*	String hostAcctKey = getAccount_id ();
		if (hostAcctKey == null || hostAcctKey.trim().length() == 0) {
			cxLog.ferrexit("Nil-HostAcctKey");
		}
		cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey.trim());
		if (!h.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
			cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey);
		}
		WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
		wi.addParam("acctId", cxAcctKey);
		wsInfoSet.add(wi);
	*/
		String cxUserKey = "";
                cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(),getEmployee_ID());
                WorkspaceInfo wbr = new WorkspaceInfo(Constants.KEY_REF_TYP_USER,cxUserKey);
                wsInfoSet.add(wbr);
		return wsInfoSet;
  }
}



