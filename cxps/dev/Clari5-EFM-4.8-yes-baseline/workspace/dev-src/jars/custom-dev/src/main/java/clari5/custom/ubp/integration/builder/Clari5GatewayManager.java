package clari5.custom.ubp.integration.builder;

import clari5.custom.ubp.integration.audit.AuditManager;
import clari5.custom.ubp.integration.config.BepCon;
import clari5.platform.util.CxRest;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;
import clari5.platform.util.ECClient;

public class Clari5GatewayManager {
    public static CxpsLogger logger = CxpsLogger.getLogger(Clari5GatewayManager.class);
    static {
        ECClient.configure(null);
    }

    public static void send(String event_id,JSONObject json, long event_ts) throws Exception {
        String entity_id = "yesbankEntity";
        boolean status= ECClient.enqueue("HOST",entity_id,event_id,json.toString());
        System.out.println("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
    }
}
