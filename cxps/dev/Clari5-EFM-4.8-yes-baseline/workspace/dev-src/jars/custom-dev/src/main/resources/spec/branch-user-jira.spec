cxps.noesis.glossary.entity.branch-user-jira{
        db-name = BRANCH_USER_JIRA
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
	
		{ name = branch-sol-id, column = branch_sol_id, type = "string:50"}
		{ name = group, column = group, type = "string:50"}
		{ name = segment, column = segment, type = "string:50"}
		{ name = user-id, column = user_id, type = "string:50"}
		{ name = level, column = level, type = "string:50"}
	       	{ name = rcre-time, column = rcre_time, type = timestamp}
		{ name = rcre-user, column = rcre_user, type = "string:50"}
		{ name = lchg-time, column = lchg_time, type = timestamp}		
		{ name = lchg-user, column = lchg_user, type = "string:50"}
			       
	      
 ]

}
