cxps.noesis.glossary.entity.ft-core{
        db-name = FT_CORE
        generate = true
        db_column_quoted = true
        tablespace = CXPS_USERS
        attributes = [
                
	       { name = cust-id, column = cust_id, type = "string:50"}
	       { name = ucic-id, column = ucic_id, type = "string:50"}
	       { name = product-code, column = product_code, type = "string:50"}
	       { name = fcc-product-code, column = fcc_product_code, type = "string:50"}
               { name = drcr, column = drcr, type = "string:50"}
	       { name = tran-amount, column = tran_amount, type = "number:30"}
	       { name = branch-id, column = branch_id, type = "string:50"}
	       { name = td-maturity-date, column = td_maturity_date, type = "string:50"}
	       { name = eff-available-balance, column = eff_available_balance, type = "string:50"}
	       { name = channel-id, column = channel_id, type = "string:50"}
	       { name = tran-numonic-code, column = tran_numonic_code, type = "string:50"}
	       { name = inst-status, column = inst_status, type = "string:50"}
	       { name = host-type, column = host_type, type = "string:50"}
	       { name = instrument-type, column = instrument_type, type = "string:50"}
	       { name = instrument-number, column = instrument_number, type = "string:50"}
	       { name = atm-id, column = atm_id, type = "string:50"}
	       { name = isin-number, column = isin_number, type = "string:50"}
	       { name = host-type, column = host_type, type = "string:50"}
	       { name = instrument-type, column = instrument_type, type = "string:50"}
	       { name = tran-date, column = tran_date, type = timestamp}
	       { name = pstd-date, column = pstd_date, type = timestamp}
	       { name = tran-id, column = tran_id, type = "string:50"}
	       { name = reference-srl-num, column = reference_srl_num, type = "string:50"}
	       { name = value-date, column = value_date, type = timestamp}
	       { name = tran-crncy-code, column = tran_crncy_code, type = "string:50"}
	       { name = ref-tran-amount, column = ref_tran_amount, type = "number:30"}
	       { name = ref-tran-crncy, column = ref_tran_crncy, type = "string:50"}
	       { name = tran-particular, column = tran_particular, type = "string:4000"}
	       { name = sys-time, column = sys_time, type = timestamp}
	       { name = bank-code, column = bank_code, type = "string:50"}
	       { name = pstd-flg, column = pstd_flg, type = "string:50"}
	       { name = online-batch, column = online_batch, type = "string:50"}
	       { name = user-id, column = user_id, type = "string:50"}
	       { name = status, column = status, type = "string:50"}
	       { name = host-id, column = host_id, type = "string:50"}
	       { name = td-liquidation-type, column = td_liquidation_type, type = "string:50"}
	       { name = tod-grant-amount, column = tod_grant_amount, type = "number:30"}
	       { name = ca-scheme-code, column = ca_scheme_code, type = "string:50"}
	       { name = system, column = system, type = "string:50"}
	       { name = rem-type, column = rem_type, type = "string:50"}
	       { name = branch, column = branch, type = "string:50"}
	       { name = tran-curr, column = tran_curr, type = "string:50"}
	       { name = tran-amt, column = tran_amt, type = "number:30"}
	       { name = usd-eqv-amt, column = usd_eqv_amt, type = "number:30"}
	       { name = inr-amount, column = inr_amount, type = "string:50"}
	       { name = purpose-code, column = purpose_code, type = "string:50"}
	       { name = purpose-desc, column = purpose_desc, type = "string:50"}
	       { name = rem-cust-id, column = rem_cust_id, type = "string:50"}
	       { name = rem-name, column = rem_name, type = "string:50"}
	       { name = rem-add1, column = rem_add1, type = "string:50"}
	       { name = rem-add2, column = rem_add2, type = "string:50"}
	       { name = rem-add3, column = rem_add3, type = "string:50"}
	       { name = rem-city, column = rem_city, type = "string:50"}
	       { name = rem-cntry-code, column = rem_cntry_code, type = "string:50"}
	       { name = ben-cust-id, column = ben_cust_id, type = "string:50"}
	       { name = ben-name, column = ben_name, type = "string:50"}
	       { name = ben-add1, column = ben_add1, type = "string:50"}
	       { name = ben-add2, column = ben_add2, type = "string:50"}
	       { name = ben-add3, column = ben_add3, type = "string:50"}
	       { name = ben-city, column = ben_city, type = "string:50"}
	       { name = ben-cntry-code, column = ben_cntry_code, type = "string:50"}
	       { name = client-acc-no, column = client_acc_no, type = "string:50"}
	       { name = cpty-ac-no, column = cpty_ac_no, type = "string:50"}
	       { name = ben-acct-no, column = ben_acct_no, type = "string:50"}
	       { name = ben-bic, column = ben_bic, type = "string:50"}
	       { name = rem-acct-no, column = rem_acct_no, type = "string:50"}
	       { name = rem-bic, column = rem_bic, type = "string:50"}
	       { name = trn-date, column = trn_date, type = "string:50"}
	       { name = product-desc, column = product_desc, type = "string:50"}
	       { name = sync-status, column = sync_status, type = "string:50"}
	       { name = acct-id, column = acct_id, type = "string:50"}
	       { name = insertion-time, column = insertion_time, type = timestamp}
	       { name = server-id, column = server_id, type = "number:30"}
	      
 ]

}
