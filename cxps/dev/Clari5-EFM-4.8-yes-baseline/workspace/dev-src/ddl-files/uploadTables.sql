  CREATE TABLE "ACCOUNT_STATUS_MASTER"

   (           "COD_MODULE" VARCHAR2(100 BYTE),

                "COD_ACCT_STATUS" NUMBER,

                "TXT_ACCT_STATUS" VARCHAR2(100 BYTE),

                "RCRE_TIME" TIMESTAMP (6),

                "RCRE_USER" VARCHAR2(50 BYTE),

                "LCHG_TIME" TIMESTAMP (6),

                "LCHG_USER" VARCHAR2(50 BYTE),

                 PRIMARY KEY ("COD_ACCT_STATUS")

   ) ;

  CREATE TABLE "CUSTOM_TXN_MNEMONIC"

   (           "COD_TXN_MNEMONIC" NUMBER,

                "COD_TXN_LITERAL" VARCHAR2(100 BYTE),

                "TXT_TXN_DESC" VARCHAR2(500 BYTE),

                "TRANSACTION_MODE" VARCHAR2(100 BYTE),

                "SUB_TRAN_MODE" VARCHAR2(100 BYTE),

                "TRANSCTION_SUB_TYPE" VARCHAR2(500 BYTE),

                "TRANSACTION_CHANNEL" VARCHAR2(100 BYTE),

                "COD_DR_CR" VARCHAR2(100 BYTE),

                "USER_ID" VARCHAR2(100 BYTE),

                "RCRE_TIME" TIMESTAMP (6),

                "RCRE_USER" VARCHAR2(50 BYTE),

                "LCHG_TIME" TIMESTAMP (6),

                "LCHG_USER" VARCHAR2(50 BYTE)

   ) ;

  CREATE TABLE "BRANCH_USER_JIRA"

   (           "Branch_Sol_Id" VARCHAR2(20 BYTE),

                "Group" VARCHAR2(20 BYTE),

                "Segment" VARCHAR2(20 BYTE),

                "User_ID" VARCHAR2(20 BYTE),

                "Level" VARCHAR2(20 BYTE),

                "RCRE_TIME" TIMESTAMP (6),

                "RCRE_USER" VARCHAR2(50 BYTE),

                "LCHG_TIME" TIMESTAMP (6),

                "LCHG_USER" VARCHAR2(50 BYTE)

   ) ;

  CREATE TABLE "PROF_CODE_DESC"

   (           "PROF_CODE" NUMBER,

                "PROF_DESC" VARCHAR2(100 BYTE),

                "RCRE_TIME" TIMESTAMP (6),

                "RCRE_USER" VARCHAR2(50 BYTE),

                "LCHG_TIME" TIMESTAMP (6),

                "LCHG_USER" VARCHAR2(50 BYTE),

                 PRIMARY KEY ("PROF_CODE")

   ) ;

  CREATE TABLE "RISK_MASTER"

   (           "VALUE_CODE" VARCHAR2(10 BYTE),

                "VALUE_DESC" VARCHAR2(10 BYTE),

                "RCRE_TIME" TIMESTAMP (6),

                "RCRE_USER" VARCHAR2(50 BYTE),

                "LCHG_TIME" TIMESTAMP (6),

                "LCHG_USER" VARCHAR2(50 BYTE)

   ) ;

  CREATE TABLE "WHITELIST_ACCT_DET"

   (           "ACCOUNT_ID" VARCHAR2(500 BYTE),

                "CUSTOMER_ID" VARCHAR2(500 BYTE),

                "RCRE_TIME" TIMESTAMP (6),

                "RCRE_USER" VARCHAR2(50 BYTE),

                "LCHG_TIME" TIMESTAMP (6),

                "LCHG_USER" VARCHAR2(50 BYTE),

                 PRIMARY KEY ("ACCOUNT_ID")

   ) ;



