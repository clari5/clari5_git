CREATE TABLE "ACCOUNT_STATUS_MASTER" 
   (	"COD_MODULE" VARCHAR2(100), 
	"COD_ACCT_STATUS" NUMBER, 
	"TXT_ACCT_STATUS" VARCHAR2(100), 
	 PRIMARY KEY ("COD_ACCT_STATUS")


);



CREATE TABLE "BRANCH_MASTER" 
   (	"BRANCH_SOL_ID" NUMBER(5,0) NOT NULL ENABLE, 
	"BRANCH_NAME" VARCHAR2(360) NOT NULL ENABLE, 
	"BRANCH_ADDRESS_1" VARCHAR2(105), 
	"BRANCH_ADDRESS_2" VARCHAR2(105), 
	"BRANCH_ADDRESS_3" VARCHAR2(105), 
	"CITY_NAME" VARCHAR2(105), 
	"STATE" VARCHAR2(105), 
	"PIN_CODE" VARCHAR2(105), 
	"BRANCH_MANAGER_EMAIL_ID" VARCHAR2(105), 
	"REGIONAL_MANAGER_EMAIL_ID" VARCHAR2(105), 
	"BRANCH_CREATION_DATE" DATE, 
	"BRANCH_GROUP" VARCHAR2(105), 
	"IS_NOC_BRANCH" VARCHAR2(105), 
	"LAST_MODIFIED_DATE" TIMESTAMP (6), 
	"VERSION" NUMBER
   );


