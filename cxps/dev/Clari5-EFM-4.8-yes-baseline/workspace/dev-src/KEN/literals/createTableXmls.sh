XML_SD='tableMetadata'
XML_EXT="$*.xml"

TEMPLATE_SD='tablesTemplate'
TEMPLATE_EXT="$*.template"

pattern=""
> tmp.$$
for file in `ls -F "$XML_SD"/*"$XML_EXT"|grep -v /$`
do
	xmlFileName=`basename $file ".xml"` ;
	xmlFileContent=$(cat $file | tr -d '\012')
	pattern="$pattern $xmlFileName"
	echo $xmlFileContent >> tmp.$$
done
	
for template in `ls -F "$TEMPLATE_SD"/*"$TEMPLATE_EXT"|grep -v /$` 
do
	templateFileName=`basename $template ".template"`
	path='../xml/table/'
	cp $template $path$templateFileName.xml
	i=0
	for pat in $pattern
	do
		let i=i+1
		p='@@@@@'$pat'@@@@@'
		r=`sed -n ${i}p tmp.$$`
		e="s,${p},${r},"
		sed -i "$e" $path$templateFileName.xml
	done
	echo "Written $templateFileName.xml"
done
\rm -f tmp.$$

