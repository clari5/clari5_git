import json
import subprocess as sp
import os
import sys

CXPS_HOME = os.environ['CXPS_HOME']

def getGitStatus():
	git_command = ['git', 'status', '--porcelain']
	repository = os.path.dirname(CXPS_HOME)
	git_query = sp.Popen(git_command, cwd=repository, stdout=sp.PIPE, stderr=sp.PIPE)
	(git_status, git_err) = git_query.communicate()
	if(len(git_status) == 0):
		#print("Everything is committed locally")
		return True
	else:
		#print("Missing Commit")
		return False	

def get_bitbucket_hash_value():
	#res = requests.get('https://api.bitbucket.org/2.0/repositories/clari5/clari5_git/commits')
	bitbucket_api = 'https://api.bitbucket.org/2.0/repositories/clari5/clari5_git/commits'	
	curl_command = ['curl', bitbucket_api]
	query = sp.Popen(curl_command, stdout=sp.PIPE, stderr=sp.PIPE)
	(bit_response, bit_error) = query.communicate()
	#print(bit_response)
	resp_dic = json.loads(bit_response)
	#print(resp_dic["values"][0]["hash"])
	val = resp_dic["values"][0]["hash"]
	val = val[0:7]
	val = int(val, 16)
	return val

def read_bitbucket_response():
	with open("response.json", "r") as read_json:
		resp_dic = json.load(read_json)
		print(resp_dic["values"][2]["hash"])

def get_local_git_hash_value():
	repository = os.path.dirname(CXPS_HOME)
	git_command = ['git', 'rev-parse', 'HEAD']
	git_query = sp.Popen(git_command,cwd=repository,stdout=sp.PIPE, stderr=sp.PIPE)
	(git_out, git_err) = git_query.communicate()
	#print(git_out)
	val = git_out
	val = val[0:7]
	val = int(val, 16)
	return val

if __name__ == '__main__':
	if(not getGitStatus()):
		#print("unversioned")
                with open("bitbucketversion.txt", 'w') as f:
                        f.write("unversioned")
		sys.exit()	
	#read_bitbucket_response()
	local_hash_value = get_local_git_hash_value()
	bitbucket_hash_value = get_bitbucket_hash_value()
	if(bitbucket_hash_value == local_hash_value):
		#print(bitbucket_hash_value)
		with open("bitbucketversion.txt", 'w') as f:
			f.write(str(bitbucket_hash_value))
	else:
		#print("unversioned")
                with open("bitbucketversion.txt", 'w') as f:
                        f.write("unversioned")	
	#print("unversioned")
