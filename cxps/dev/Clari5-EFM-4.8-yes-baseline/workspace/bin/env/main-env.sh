:

# Database configurations

[ -z "${DB_TYPE}" ] && DB_TYPE=sqlserver
[[ "${DB_TYPE}" = "oracleXE" ]] && DB_TYPE=oracle

# Database type
export DB_TYPE
export SECURITY_TYPE=${DB_TYPE}

# Database connection
case ${DB_TYPE} in
	"mysql")
        export DB_IP="localhost"
        export DB_PORT="3306"
        export DB_SID="cxpsadm_${USER}_48dev"
        export DB_DIALECT="org.hibernate.dialect.MySQLDialect"
		;;
    "oracle")
		# lan
        #export DB_IP="192.168.5.51"
        export DB_IP="192.168.5.70"
        export DB_PORT="1521"
        export DB_SID="db12c"

        #export DB_SID="testdb12c"

		# azure
        #export DB_IP="52.172.8.206"
        #export DB_SID="oracle12c"

		# local
         #export DB_IP="127.0.0.1"
       #  export DB_SID="orcl"


        export DB_DIALECT="org.hibernate.dialect.Oracle10gDialect"
        ;;
    "sqlserver")
		# lan
        export DB_IP="192.168.5.76"
        export DB_PORT="1433"
        export DB_SID="CXPSWINSQL"

		# azure
        #export DB_IP="52.172.40.121" #azure
        #export DB_SID="dbsid" #azure

        # local
        #export DB_IP="127.0.0.1"
        #export DB_SID="$(hostname -s)"

        export DB_DIALECT="org.hibernate.dialect.SQLServer2012Dialect"
        ;;
esac

# Database users
export DB_USER="cxpsadm_${USER}_48dev"
export SEC_DB_USER=${DB_USER}
export SECURITY_USER=${DB_USER}

# Security schema for cas
export SECURITY_IP=${DB_IP}
export SECURITY_PORT=${DB_PORT}
export SECURITY_SID=${DB_SID}

# Password management
export RICE_PMSEED="c_xps123"
export RICE_PMTYPE="SIMPLE"
export SECURITY_PMSEED="c_xps123"
export SECURITY_PMTYPE="SIMPLE"
export DB_PMTYPE=$RICE_PMTYPE
export DB_PMSEED=$RICE_PMSEED
export CC_PMTYPE=$RICE_PMTYPE
export CC_PMSEED=$RICE_PMSEED
export WL_PMSEED=$RICE_PMSEED
export WL_PMTYPE=$RICE_PMTYPE

# Other remaining env params
export DN="http://$(hostname -f):5000"
export LOCAL_DN="http://$(hostname -f):5000"
export CX_DATA_DIR="${DEP_BASE}/data"
export BASE_Q_LOCATION="${CX_DATA_DIR}/CMQ"
export KENDEPLOY_PATH="${DEP_BASE}/KEN"
export CXPS_JIRA_HOME="${HOME}/atlassian/jira"
export CMQ_ARCHIVE_DIR="${CX_DATA_DIR}/CMQ_ARCHIVE"
export LOG_Q_LOCATION="${CX_DATA_DIR}/APPLOG"
export CAS_IDLE_TIMEOUT="300000"
export AJP_PORT="9090"

# Jira credentials
export JIRA_USER_ID="cxpsaml"
export JIRA_PASSWORD="Y3hwc2FtbA=="

# Values for JIRA CLIENT auth
export INSTANCEID="clari5"
export APPID="clari5"
export APPSECRET="clari5"
export APP_HOST="`hostname -f`"

# JAVA_OPTS set to override JAVA_OPTS from meta conf
export JAVA_OPTS="-Xmx2g"
export JIRA_CUSTOM_FIELD_APPENDER=""
export CI_EVIDENCE_DETAILS="customfield_11903"
export SD_DOI="customfield_11904"
export IBX_USR_GRP_CLASS="clari5.custom.inbox.processors.cluster.CustomClusterAssigner"
