package clari5.custom.services;
//checking

/**
 * @author shishir
 * @since 10/01/2018
 */
public class MailFields {
    private String smtpHost;
    private String smtpPort;
    private String to;
    private String cc;
    private String sender;
    private String senderMailPass;
    private String subject;
    private String message;
    private String ioTimeout;
    private String conTimeOut;


    public String getSmtpHost() {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderMailPass() {
        return senderMailPass;
    }

    public void setSenderMailPass(String senderMailPass) {
        this.senderMailPass = senderMailPass;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIoTimeout() {
        return ioTimeout;
    }

    public void setIoTimeout(String ioTimeout) {
        this.ioTimeout = ioTimeout;
    }

    public String getConTimeOut() {
        return conTimeOut;
    }

    public void setConTimeOut(String conTimeOut) {
        this.conTimeOut = conTimeOut;
    }

    public String getTo() {return to;}

    public void setTo(String to) {this.to = to;}

    public String getCc() {return cc;}

    public void setCc(String cc) {this.cc = cc; }

    @Override
    public String toString() {
        return "MailFields{" +
                "smtpHost='" + smtpHost + '\'' +
                ", smtpPort='" + smtpPort + '\'' +
                ", to='" + to + '\'' +
                ", cc='" + cc + '\'' +
                ", sender='" + sender + '\'' +
                ", senderMailPass='" + senderMailPass + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", ioTimeout='" + ioTimeout + '\'' +
                ", conTimeOut='" + conTimeOut + '\'' +
                '}';
    }
}
