import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";


const appRoutes: Routes = [
    {
        path: 'clari5-cc',
       /* children: [
            {
                path: '',
                //component: 
            },
           /* {
                path: 'cc',
                component: MainAppComponent,
                children: [
                    {
                        path: '',
                        component: LicenseInfoComponent
                    }, {
                        path: 'licenseinfo',
                        component: LicenseInfoComponent
                    }, {
                        path: 'topologyupload',
                        component: TopologyUploadComponent
                    }, {
                        path: 'topologyinfo',
                        component: TopologyInfoComponent
                    }, {
                        path: 'download',
                        component: DownloadResourceComponent
                    }, {
                        path: 'releasehistory',
                        component: ReleaseHistoryComponent
                    }, {
                        path: 'config/:version/:editable',
                        component: ConfigComponent,
                    }, {
                        path: 'currentconfig',
                        component: CurrentConfigComponent,
                    }, {
                        path: 'deployrelease',
                        component: VersionComponent
                    }, {
                        path: 'latestuploadedversion',
                        component: LatestUploadedVersionComponent
                    }, {
                        path: 'finaldeploy',
                        component: DeployComponent
                    }, {
                        path: 'uploadrelease',
                        component: UploadReleaseComponent
                    }, {
                        path: 'machineinfo',
                        component: MachinesComponent
                    }, {
                        path: 'dbmigration/:schema/:version/:conflict/:dbtype',
                        component: DbMigrationComponent
                    }, {
                        path: 'processStatus',
                        component: ProcessStatusComponent
                    }
                ]
            },*/
          /*  {
                path: 'uac',
                children: [
                    {
                        path: '',
                        component: UACLandingComponent,
                        children: [
                            {
                                path: '',
                                redirectTo: 'security-master',
                                pathMatch: 'prefix'
                            },
                            {
                                path: 'security-master',
                                component: SecurityMasterComponent,
                            },
                            {
                                path: 'group-app',
                                component: GroupAppStaticComponent
                            },
                            {
                                path: 'group-app-edit',
                                component: GroupAppComponent
                            },
                            {
                                path: 'group-app-verify',
                                component: GroupAppVerifyComponent
                            },
                            {
                                path: 'security-master-edit',
                                component: SecurityMasterEditComponent
                            },
                            {
                                path: 'security-master-verify',
                                component: SecurityMasterVerifyComponent
                            },
                            {
                                path: 'block-user',
                                component: BlockUserComponent
                            },
                            {
                                path: 'block-user-verify',
                                component: BlockUserVerifyComponent
                            },
                            {
                                path: 'audit-criteria',
                                component: AuditCriteriaComponent
                            }
                        ]
                    }
                ]
            },*/
          /*  {

                path: 'mq',

                children: [
                    {
                        path: '',
                        component: MqMainPageComponent
                    },
                    {
                        path: 'mqgraph',
                        component: MqGraphPage
                    },
                    {
                        path: 'mq-mainpage',
                        component: MqMainPageComponent
                    },

                    {
                        path: 'fileview',
                        component: MqFileViewComponent
                    },
                    {
                        path: 'messageview',
                        component: MqMessageViewComponent
                    }
                ]
            }

        ]*/
    }
]

export const AppRouter: ModuleWithProviders = RouterModule.forRoot(appRoutes);
