// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_LinkdebitcardEventMapper extends EventMapper<NFT_LinkdebitcardEvent> {

public NFT_LinkdebitcardEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_LinkdebitcardEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_LinkdebitcardEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_LinkdebitcardEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_LinkdebitcardEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_LinkdebitcardEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_LinkdebitcardEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTwoFaMode());
            preparedStatement.setString(i++, obj.getTwoFaStatus());
            preparedStatement.setString(i++, obj.getCardNumber());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getObdxTransactionName());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getPrimaryAccountNumber());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustSegment());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getObdxModuleName());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getSecondaryAccountNumbers());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getSessionId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_LINKDEBITCARD]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_LinkdebitcardEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_LinkdebitcardEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_LINKDEBITCARD"));
        putList = new ArrayList<>();

        for (NFT_LinkdebitcardEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TWO_FA_MODE",  obj.getTwoFaMode());
            p = this.insert(p, "TWO_FA_STATUS",  obj.getTwoFaStatus());
            p = this.insert(p, "CARD_NUMBER",  obj.getCardNumber());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "OBDX_TRANSACTION_NAME",  obj.getObdxTransactionName());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "PRIMARY_ACCOUNT_NUMBER",  obj.getPrimaryAccountNumber());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_SEGMENT",  obj.getCustSegment());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "OBDX_MODULE_NAME",  obj.getObdxModuleName());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "SECONDARY_ACCOUNT_NUMBERS",  obj.getSecondaryAccountNumbers());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "SESSION_ID",  obj.getSessionId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_LINKDEBITCARD"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_LINKDEBITCARD]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_LINKDEBITCARD]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_LinkdebitcardEvent obj = new NFT_LinkdebitcardEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTwoFaMode(rs.getString("TWO_FA_MODE"));
    obj.setTwoFaStatus(rs.getString("TWO_FA_STATUS"));
    obj.setCardNumber(rs.getString("CARD_NUMBER"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setObdxTransactionName(rs.getString("OBDX_TRANSACTION_NAME"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setPrimaryAccountNumber(rs.getString("PRIMARY_ACCOUNT_NUMBER"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustSegment(rs.getString("CUST_SEGMENT"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setObdxModuleName(rs.getString("OBDX_MODULE_NAME"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setSecondaryAccountNumbers(rs.getString("SECONDARY_ACCOUNT_NUMBERS"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setSessionId(rs.getString("SESSION_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_LINKDEBITCARD]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_LinkdebitcardEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_LinkdebitcardEvent> events;
 NFT_LinkdebitcardEvent obj = new NFT_LinkdebitcardEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_LINKDEBITCARD"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_LinkdebitcardEvent obj = new NFT_LinkdebitcardEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTwoFaMode(getColumnValue(rs, "TWO_FA_MODE"));
            obj.setTwoFaStatus(getColumnValue(rs, "TWO_FA_STATUS"));
            obj.setCardNumber(getColumnValue(rs, "CARD_NUMBER"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setObdxTransactionName(getColumnValue(rs, "OBDX_TRANSACTION_NAME"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setPrimaryAccountNumber(getColumnValue(rs, "PRIMARY_ACCOUNT_NUMBER"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustSegment(getColumnValue(rs, "CUST_SEGMENT"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setObdxModuleName(getColumnValue(rs, "OBDX_MODULE_NAME"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setSecondaryAccountNumbers(getColumnValue(rs, "SECONDARY_ACCOUNT_NUMBERS"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setSessionId(getColumnValue(rs, "SESSION_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_LINKDEBITCARD]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_LINKDEBITCARD]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TWO_FA_MODE\",\"TWO_FA_STATUS\",\"CARD_NUMBER\",\"IP_CITY\",\"OBDX_TRANSACTION_NAME\",\"USER_ID\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"PRIMARY_ACCOUNT_NUMBER\",\"SYS_TIME\",\"CUST_SEGMENT\",\"CUST_ID\",\"IP_COUNTRY\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"DEVICE_ID\",\"SECONDARY_ACCOUNT_NUMBERS\",\"ADDR_NETWORK\",\"HOST_ID\",\"SESSION_ID\"" +
              " FROM EVENT_NFT_LINKDEBITCARD";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TWO_FA_MODE],[TWO_FA_STATUS],[CARD_NUMBER],[IP_CITY],[OBDX_TRANSACTION_NAME],[USER_ID],[ERROR_DESC],[SUCC_FAIL_FLG],[PRIMARY_ACCOUNT_NUMBER],[SYS_TIME],[CUST_SEGMENT],[CUST_ID],[IP_COUNTRY],[ERROR_CODE],[OBDX_MODULE_NAME],[DEVICE_ID],[SECONDARY_ACCOUNT_NUMBERS],[ADDR_NETWORK],[HOST_ID],[SESSION_ID]" +
              " FROM EVENT_NFT_LINKDEBITCARD";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TWO_FA_MODE`,`TWO_FA_STATUS`,`CARD_NUMBER`,`IP_CITY`,`OBDX_TRANSACTION_NAME`,`USER_ID`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`PRIMARY_ACCOUNT_NUMBER`,`SYS_TIME`,`CUST_SEGMENT`,`CUST_ID`,`IP_COUNTRY`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`DEVICE_ID`,`SECONDARY_ACCOUNT_NUMBERS`,`ADDR_NETWORK`,`HOST_ID`,`SESSION_ID`" +
              " FROM EVENT_NFT_LINKDEBITCARD";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_LINKDEBITCARD (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TWO_FA_MODE\",\"TWO_FA_STATUS\",\"CARD_NUMBER\",\"IP_CITY\",\"OBDX_TRANSACTION_NAME\",\"USER_ID\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"PRIMARY_ACCOUNT_NUMBER\",\"SYS_TIME\",\"CUST_SEGMENT\",\"CUST_ID\",\"IP_COUNTRY\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"DEVICE_ID\",\"SECONDARY_ACCOUNT_NUMBERS\",\"ADDR_NETWORK\",\"HOST_ID\",\"SESSION_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TWO_FA_MODE],[TWO_FA_STATUS],[CARD_NUMBER],[IP_CITY],[OBDX_TRANSACTION_NAME],[USER_ID],[ERROR_DESC],[SUCC_FAIL_FLG],[PRIMARY_ACCOUNT_NUMBER],[SYS_TIME],[CUST_SEGMENT],[CUST_ID],[IP_COUNTRY],[ERROR_CODE],[OBDX_MODULE_NAME],[DEVICE_ID],[SECONDARY_ACCOUNT_NUMBERS],[ADDR_NETWORK],[HOST_ID],[SESSION_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TWO_FA_MODE`,`TWO_FA_STATUS`,`CARD_NUMBER`,`IP_CITY`,`OBDX_TRANSACTION_NAME`,`USER_ID`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`PRIMARY_ACCOUNT_NUMBER`,`SYS_TIME`,`CUST_SEGMENT`,`CUST_ID`,`IP_COUNTRY`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`DEVICE_ID`,`SECONDARY_ACCOUNT_NUMBERS`,`ADDR_NETWORK`,`HOST_ID`,`SESSION_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

