export class CommonFunctions {
    
    logDebugMsg(str : string, obj1? : Object, obj2? : Object ) : void {
        console.log(str, obj1, obj2);
    }
    alertDebugMsg(str : string):void {
        alert(str);
    }
}