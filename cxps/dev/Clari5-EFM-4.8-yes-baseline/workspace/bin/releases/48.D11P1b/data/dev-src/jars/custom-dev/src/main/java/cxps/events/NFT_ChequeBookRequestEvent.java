// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_CHEQUEBOOKREQUEST", Schema="rice")
public class NFT_ChequeBookRequestEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=6) public Integer noOfLeavIsud;
       @Field(size=200) public String accountId;
       @Field(size=11) public Double avlBal;
       @Field(size=200) public String system;
       @Field public java.sql.Timestamp issueDate;
       @Field(size=200) public String endcheqnumber;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String customerId;
       @Field public java.sql.Timestamp eventts;
       @Field(size=200) public String hostId;
       @Field(size=200) public String begincheqnumber;


    @JsonIgnore
    public ITable<NFT_ChequeBookRequestEvent> t = AEF.getITable(this);

	public NFT_ChequeBookRequestEvent(){}

    public NFT_ChequeBookRequestEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("ChequeBookRequest");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setNoOfLeavIsud(EventHelper.toInt(json.getString("No_of_Leav_isud")));
            setAccountId(json.getString("Account_Id"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setSystem(json.getString("System"));
            setIssueDate(EventHelper.toTimestamp(json.getString("Issue_Date")));
            setEndcheqnumber(json.getString("EndCheqNumber"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustomerId(json.getString("Customer_ID"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setHostId(json.getString("host_id"));
            setBegincheqnumber(json.getString("BeginCheqNumber"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NCB"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public Integer getNoOfLeavIsud(){ return noOfLeavIsud; }

    public String getAccountId(){ return accountId; }

    public Double getAvlBal(){ return avlBal; }

    public String getSystem(){ return system; }

    public java.sql.Timestamp getIssueDate(){ return issueDate; }

    public String getEndcheqnumber(){ return endcheqnumber; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustomerId(){ return customerId; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getHostId(){ return hostId; }

    public String getBegincheqnumber(){ return begincheqnumber; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setNoOfLeavIsud(Integer val){ this.noOfLeavIsud = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setSystem(String val){ this.system = val; }
    public void setIssueDate(java.sql.Timestamp val){ this.issueDate = val; }
    public void setEndcheqnumber(String val){ this.endcheqnumber = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustomerId(String val){ this.customerId = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setBegincheqnumber(String val){ this.begincheqnumber = val; }

    /* Custom Getters*/
    @JsonIgnore
    public Integer getNo_of_Leav_isud(){ return noOfLeavIsud; }
    @JsonIgnore
    public String getAccount_Id(){ return accountId; }
    @JsonIgnore
    public Double getAvl_bal(){ return avlBal; }
    @JsonIgnore
    public java.sql.Timestamp getIssue_Date(){ return issueDate; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getCustomer_Id(){ return customerId; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_ChequeBookRequestEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.customerId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_ChequeBookRequest");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "ChequeBookRequest");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}