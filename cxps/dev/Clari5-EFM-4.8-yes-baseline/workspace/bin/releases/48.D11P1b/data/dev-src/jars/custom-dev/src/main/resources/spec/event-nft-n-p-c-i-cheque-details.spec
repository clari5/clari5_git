cxps.events.event.nft-n-p-c-i-cheque-details{
  table-name : EVENT_NFT_NPCICHEQUEDETAILS
  event-mnemonic: NP
  workspaces : {
    ACCOUNT: Account_Id
  }
  event-attributes : {
	cheque_num: {db : true ,raw_name : Cheque_Num ,type : "string:200", custom-getter:Cheque_Num}
	account_id: {db : true ,raw_name : Account_Id ,type : "string:200", custom-getter:Account_Id}
	amount: {db : true ,raw_name : Amount ,type : "number:11,2"}
	newly_opened_account: {db : true ,raw_name : Newly_Opened_Account ,type : "string:200", custom-getter:Newly_Opened_Account}
	cheque_number_t: {db : true ,raw_name : Cheque_Number_t ,type : "string:200", custom-getter:Cheque_Number_t}
	micr_no_t: {db : true ,raw_name : micr_no_t ,type : "string:200", custom-getter:Micr_no_t}
	san_t: {db : true ,raw_name : SAN_t ,type : "string:200", custom-getter:SAN_t}
	tc_t: {db : true ,raw_name : TC_t ,type : "string:200", custom-getter:TC_t}
	in_out_flag: {db : true ,raw_name : in_out_flag ,type : "string:200", custom-getter:In_out_flag}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	host_id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id}
	eventts: {db : true ,raw_name : eventts ,type : timestamp}
	}
}
