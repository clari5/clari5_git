cxps.events.event.nft-cheque-return{
table-name : EVENT_NFT_CHEQUERETURN
  event-mnemonic: NCR
  workspaces : {
    ACCOUNT : acct_id
    }
  event-attributes : {
	acct_id: {db : true ,raw_name : acct_id ,type : "string:200", custom-getter:Acct_id}
	user_id: {db : true ,raw_name : user_id ,type : "string:200", custom-getter:User_id}
	cust_id: {db : true ,raw_name : cust_id ,type : "string:200", custom-getter:Cust_id}
	branch_id: {db : true ,raw_name : branchid ,type : "string:200"}
	branch_id_desc: {db : true ,raw_name : branchiddesc ,type : "string:200"}
	acct_status: {db : true ,raw_name : acct_status ,type : "string:200", custom-getter:Acct_status}
	acct_name: {db : true ,raw_name : acct_name ,type : "string:200", custom-getter:Acct_name}
	cheque_number: {db : true ,raw_name : Cheque_Number ,type : "string:200", custom-getter:Cheque_Number}
	rejection_code: {db : true ,raw_name : Rejection_Code ,type : "string:200", custom-getter:Rejection_Code}
	rejection_reason: {db : true ,raw_name : Rejection_Reason ,type : "string:200", custom-getter:Rejection_Reason}
	transaction_numonic_code: {db : true ,raw_name : Transaction_numonic_code ,type : "string:200", custom-getter:Transaction_numonic_code}
	host_id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	eventts: {db : true ,raw_name : eventts ,type : timestamp}
	}
}
