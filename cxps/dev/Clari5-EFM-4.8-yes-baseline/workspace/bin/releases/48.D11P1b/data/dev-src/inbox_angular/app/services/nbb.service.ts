import {
    Injectable
} from '@angular/core';
import { Headers, Http, Response, RequestOptions ,URLSearchParams       } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { CommonFunctions } from './common.functions';
import { NbbDataService } from '../services/nbb.data.service';
import { MethodCall } from '@angular/compiler';



@Injectable()
export class NbbService extends CommonFunctions {

    nbbDataService: NbbDataService;
    http: Http;
    public isC: boolean[] = [];
    public PROGRAM_TYPE: string = "";
    public DropDown: boolean = true;
    public RadioButton: boolean = true;
    public thirdParty: {};
    public question: string = "";
    public option: any;
    public questionId: any;
    public selectedAns = new Array();
    public payload:any;


    constructor(http: Http, nbbDataService: NbbDataService) {
        super();
        this.nbbDataService = nbbDataService;
        this.http = http;
    }

    getLoadData(dataName?: string, usersUrl?: string,method?:any ,postData?: any) {
       // super.logDebugMsg('before call ', postData);

      //  let headers = new Headers({ 'Content-Type': 'text/html' });
        let myParams = new URLSearchParams();
        myParams.append("method",method);
        myParams.append("payload",postData);
      //  let body = method&postData;
        
       // let options = new RequestOptions({params: myParams });
        return this.http.get(usersUrl+myParams)
            .map(res => res.text() ? res.json() : res)
            .subscribe((data) => {

                switch (dataName) {
                     case "scenario":
                        this.nbbDataService.allscenarios = data;//data.templates;
                        this.nbbDataService.scenarios = data.scenariosList;
                        super.logDebugMsg('init inside scenario list ', this.nbbDataService.scenarios);
                        super.logDebugMsg('init inside allscenarios ', this.nbbDataService.allscenarios);
                        break;
                    case "tempscenario":
                        this.nbbDataService.test = data;//JSON.parse( data);
                        super.logDebugMsg('scenarioList temp scenarios', this.nbbDataService.test);
                        break;
                    case "onselect":
                        this.nbbDataService.tags = data.eventList;//data;//JSON.parse( data);
                        super.logDebugMsg('event tags ', this.nbbDataService.tags);
                        this.nbbDataService.other = data;//data;//JSON.parse( data);
                        super.logDebugMsg('all data ', this.nbbDataService.tags);
                        break;
                    default:
                        super.logDebugMsg('inside default ');
                        this.nbbDataService.res = data.resp;
                        super.logDebugMsg('status', this.nbbDataService.res);
                }

            },
            (err) => {
                super.alertDebugMsg(err);
            });
    }
}
