clari5.custom.mapper {
        entity {
                DEVICE_BINDING {
                       generate = true
                        attributes:[
                                { name: USER_ID, type="string:50", key=true},
                                { name: CUST_ID ,type ="string:50", key=true},
                                { name: DEVICE_ID ,type ="string:50", key=true},
                                { name: DB_DATE ,type = timestamp},
                                { name: RCRE_TIME ,type = timestamp},
                                { name: RCRE_USER ,type = "string:50"},
                                { name: LCHG_TIME ,type = timestamp},
                                { name: LCHG_USER ,type = "string:50"}
                                ]
                        }
        }
}
