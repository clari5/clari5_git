cxps.events.event.nft-credential-change{
  table-name : EVENT_NFT_CREDENTIALCHANGE
  event-mnemonic: NCC
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
       host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        addr-net-local: {db :true ,raw_name : addr_net_local ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        change-type: {db : true ,raw_name : change_type ,type: "string:200" }
        change-mode: {db : true ,raw_name : change_mode ,type: "string:200" }
        first-time-flag: {db : true ,raw_name : first_time_flag ,type: "string:10"}
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        risk-band:{db : true ,raw_name :risk_band ,type : "string:200" ,derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
 }
}
