package clari5.natm.calculation;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/*****
 * @author: suryakant
 * @since : 12/02/2020
 */

public class EventCheckDaemon extends CxpsRunnable implements ICxResource {

    private static CxpsLogger logger = CxpsLogger.getLogger(EventCheckDaemon.class);
    static String updateEventBacklog;
    static Hocon hocon;
    static {
        hocon = new Hocon();
        hocon.loadFromContext("natmQueryFilter.conf");
        updateEventBacklog = hocon.getString("updateFlagEvent");
    }

    @Override
    protected Object getData() throws RuntimeFatalException {
        return AmountCalculation.getDataEventBacklog();
    }

    protected void processData(Object o) throws RuntimeFatalException {
        logger.info("Processing data to check event backlog");

        if (o instanceof ConcurrentLinkedQueue) {
            ConcurrentLinkedQueue<HashMap> queue = (ConcurrentLinkedQueue<HashMap>) o;

            try (RDBMSSession session = Rdbms.getAppSession()) {
                try (CxConnection connection = session.getCxConnection()) {

                    while (!queue.isEmpty()) {

                        HashMap map = queue.poll();
                        String custId = (String) map.get("CUST_ID");
                        String acctId = (String) map.get("ACCT_ID");
                        double cumTxnAmount = (Double) map.get("CUM_TXN_AMT");
                        String eventList = (String) map.get("EVENT_ID");
                        String eventId = (String) map.get("TRIGGERING_EVENT_ID");

                        String eventArray[] = eventList.split(",");
                        if(ThresholdCalculation.isEventAvailable(eventArray)=="Y") {

                            ThresholdCalculation.insertIncidentTable(connection, custId, acctId, cumTxnAmount, eventList, eventId);
                            try(PreparedStatement preparedStatement = connection.prepareStatement(updateEventBacklog)){
                                preparedStatement.setString(1,"Y");
                                preparedStatement.setString(2,eventId);
                                preparedStatement.executeUpdate();
                                connection.commit();
                            }

                        }
                    }
                    connection.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void configure(Hocon h) {
        logger.debug("Natm Data fetched Configured");
    }




}
