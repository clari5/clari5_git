package clari5.custom.db.wl;


import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class FuzzyMatcherDaemon  implements ICxResource {


    public static CxpsLogger logger = CxpsLogger.getLogger(FuzzyMatcherDaemon.class);

    @Override
    public void configure(Hocon h) {

        String weekDay = h.getString("weekday");
        String monthDay = h.getString("monthday");
        String startTime = h.getString("start_time");
        String frequency = h.getString("frequency");
        long interval;
        Date dt;

        logger.info("starting the fuzzy matcher process at frequence", frequency);

        if ("WEEKLY".equalsIgnoreCase(frequency) || "MONTHLY".equalsIgnoreCase(frequency) || "DAILY".equalsIgnoreCase(frequency)) {
            dt = getDate(frequency, weekDay, monthDay, startTime);
            interval = getInterval(frequency);
        } else {
            logger.error("Not a valid frequency[DAILY, WEEKLY, MONTHLY] found in configuration");
            return;
        }

        Timer timer=new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                logger.info("started fetching the records from the database");
                FuzzyMatcher fuzzyMatcher=new FuzzyMatcher();
                fuzzyMatcher.getDataFromTable();
            }
        };
        logger.info("fetching records date : ", dt, " Start Time : ", startTime, " interval : ", interval);
        timer.scheduleAtFixedRate(task, dt, interval);

    }

    private Date getDate(String frequency, String weekDay, String monthDay, String startTime) {

        Calendar cal=Calendar.getInstance();
        try {

            String[] time = startTime.split(":");//time is in hh:mm:ss format
            if ("WEEKLY".equalsIgnoreCase(frequency)) {
                int diff = Integer.parseInt(weekDay) - cal.get(Calendar.DAY_OF_WEEK);
                if (diff < 0) {
                    diff += 7;
                }
                cal.add(Calendar.DAY_OF_MONTH, diff);
            } else if ("MONTHLY".equalsIgnoreCase(frequency)) {
                if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(monthDay)) {
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, Integer.parseInt(monthDay)); //if start day is less current day then move to next month
                } else {
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), Integer.parseInt(monthDay));

                }
            } else if ("DAILY".equalsIgnoreCase(frequency)) {
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
                cal.set(Calendar.MINUTE, Integer.parseInt(time[1]));
                cal.set(Calendar.SECOND, Integer.parseInt(time[2]));
                if (cal.getTime().before(new Date())) {
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                }
            }

            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
            cal.set(Calendar.MINUTE, Integer.parseInt(time[1]));
            cal.set(Calendar.SECOND, Integer.parseInt(time[2]));

            return cal.getTime();//currently based on specified day of month only
        } catch (Exception ex) {
            logger.error("Error :  exception occurred during getDate from config parameters frequency,weekDay,monthDay,starttime : %s , %s, %s, %s", frequency, weekDay, monthDay, startTime);
        }

        return null;
    }

    private long getInterval(String frequecy) {
        long dayMillis = 1000 * 60 * 60 * 24;//millisecond for a day.
        long interval = dayMillis;
        if ("WEEKLY".equalsIgnoreCase(frequecy)) {
            interval = dayMillis * 7;
        } else if ("MONTHLY".equalsIgnoreCase(frequecy)) {
            interval = dayMillis * 30;
        } else if ("DAILY".equalsIgnoreCase(frequecy)) {
            return interval;
        }

        return interval;
    }
}

