package clari5.custom.db;

/**
 * @author shishir
 * @since 11/01/2018
 */
public interface IDBConnection {
    Object getConnection();

    void closeConnection();
}
