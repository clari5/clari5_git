package clari5.custom.dedupe;

import clari5.aml.web.wl.search.WlQuery;
import clari5.aml.web.wl.search.WlRecordDetails;
import clari5.aml.web.wl.search.WlResult;
import clari5.aml.web.wl.search.WlSearcher;
import clari5.aml.wle.MatchedRecord;
import clari5.aml.wle.MatchedResults;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.rdbms.Rdbms;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.UUID;

public class WatchListResponseFetcher {

    private static final CxpsLogger logger = CxpsLogger.getLogger(WatchListResponseFetcher.class);

    static {
        Unirest.setObjectMapper(new ObjectMapper() {
            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return CxJson.json2obj(value, valueType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public String writeValue(Object value) {
                return CxJson.obj2json(value);
            }
        });
        Unirest.setTimeouts(120000, 120000);
        Unirest.setConcurrency(500, 20);
    }

    public static String createRequest(LinkedHashMap<String, String> modifiedCustomerRecordMap, String ruleName) {

        if (modifiedCustomerRecordMap.isEmpty()) return null;

        JSONObject reqObject = new JSONObject();
        JSONArray reqArray = new JSONArray();
        reqObject.put("ruleName", ruleName);
        logger.debug("ruleName before entering the loop -> " + ruleName);

        ModifiedCustomer
                .newModifiedCustomer.getStringList("query." + ruleName).forEach(entity -> {
            JSONObject entityObject = new JSONObject();
            entityObject.put(ModifiedCustomer.newModifiedCustomer.getString("query.confList" + ruleName + "." + entity + ".nameKey"),
                    ModifiedCustomer.newModifiedCustomer.getString("query.confList" + ruleName + "." + entity + ".nameValue"));
            if (entity.equalsIgnoreCase("DOB") || entity.equalsIgnoreCase("DATE_OF_INCORPORATION")) {
                entityObject.put(ModifiedCustomer.newModifiedCustomer.getString("query.confList" + ruleName + "." + entity + ".valueKey"),
                        modifiedCustomerRecordMap.get(entity) != null ? modifiedCustomerRecordMap.get(entity) : "");
            } else if (entity.equalsIgnoreCase("MOBILE")) {
                entityObject.put(ModifiedCustomer.newModifiedCustomer.getString("query.confList" + ruleName + "." + entity + ".valueKey"),
                        modifiedCustomerRecordMap.get(entity) != null ? modifiedCustomerRecordMap.get(entity).length() > 10 ?
                                modifiedCustomerRecordMap.get(entity).substring((modifiedCustomerRecordMap.get(entity).length() - 10))
                                : modifiedCustomerRecordMap.get(entity) : "");
            } else if (entity.equalsIgnoreCase("PHONE_OFFICE") || entity.equalsIgnoreCase("PHONE_RESIDENCE") ||
                    entity.equalsIgnoreCase("HAND_PHONE")) {
                entityObject.put(ModifiedCustomer.newModifiedCustomer.getString("query.confList" + ruleName + "." + entity + ".valueKey"),
                        modifiedCustomerRecordMap.get(entity) != null ? modifiedCustomerRecordMap.get(entity).length() > 8 ?
                                modifiedCustomerRecordMap.get(entity).substring((modifiedCustomerRecordMap.get(entity).length() - 8))
                                : modifiedCustomerRecordMap.get(entity) : "");
            } else {
                entityObject.put(ModifiedCustomer.newModifiedCustomer.getString("query.confList" + ruleName + "." + entity + ".valueKey"),
                        modifiedCustomerRecordMap.get(entity) != null ? modifiedCustomerRecordMap.get(entity) : "");
            }
            reqArray.put(entityObject);
        });
        reqObject.put("fields", reqArray);
        logger.debug("json object from createRequest --> " + reqObject);
        /*System.out.println("ruleName -> "+ruleName);
        System.out.println("json object -> "+reqObject);*/
        return reqObject.toString();
    }

    public static String watchListReqUrl(JSONObject reqObject) {

        if (reqObject == null) return null;

        String url = System.getenv("LOCAL_DN") + ModifiedCustomer.newModifiedCustomer.getString("query.url");
        try {
            url = url + "?q=" + URLEncoder.encode(reqObject.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }

    public static String reponseReciever(String queryJson, String custId, String ruleName, String flag) {

        logger.debug("query json: ---> " + queryJson);
        String response = "";
      try {
            WlQuery query = new WlQuery();
            query.from(queryJson);

            logger.info("Query object created from json : ", queryJson);

            WlSearcher searcher = new WlSearcher();
            WlResult result = searcher.search(query);
	      //WlResult result = null;
            MatchedResults matched = result.getMatchedResults();
            Collection<MatchedRecord> recordList = matched.getMatchedRecords();
            Collection<MatchedRecord> recordListWithDetails = new ArrayList<>();
            for(MatchedRecord record : recordList) {
                WlRecordDetails wlDetails = new WlRecordDetails() ;
                String outputDetails = wlDetails.getRecordDetails(record.getDocId());

                record.setDetails(outputDetails);

            }
          response = result.toJson();
        //  throw new Exception("TEST");  
         


        } catch (Exception e) {
            System.out.println("unable to fetch response from WL for URL: ---> " + queryJson);
            insertRetry(queryJson,custId,ruleName);
            e.getCause();
            e.printStackTrace();
            /*if (flag.equalsIgnoreCase("Fresh")) {
                RetryWatchList.insertDudupeRetry(custId, ruleName, url);
            } else if (flag.equalsIgnoreCase("Retry")) {
                RetryWatchList.updateDedupeRetry("F2", custId, ruleName);
            }*/
        }
        return response;
    }

    public static void  insertRetry(String queryJson,String custId,String ruleName){

        try(RDBMSSession session = Rdbms.getAppSession()){
            try(CxConnection connection = session.getCxConnection()){
                try(PreparedStatement statement = connection.prepareStatement("INSERT INTO DEDUPE_RETRY\n" +
                        "(CUST_ID, STATUS, REQUEST_JSON, SERIAL_NO, RETRY_COUNT, RULE_NAME)\n" +
                        "VALUES(?, ?,?, ?, ?, ?)")){
                    UUID randomNumber = UUID.randomUUID();
                    statement.setString(1, custId);
                    statement.setInt(4, randomNumber.hashCode());
                    statement.setString(2, "F");
                    statement.setString(3, queryJson);
                    statement.setInt(5, 0);
                    statement.setString(6,ruleName);
                    statement.executeQuery();
                    connection.commit();
                    connection.close();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        }

        public static String retryAgainInsert(String queryJson, String custId, String ruleName, String flag, int serialNo,int count){
            logger.debug("query json: ---> " + queryJson);
            String response = "";
            try {
                WlQuery query = new WlQuery();
                query.from(queryJson);

                logger.info("Query object created from json : ", queryJson);

                WlSearcher searcher = new WlSearcher();
                WlResult result = searcher.search(query);
                //WlResult result = null;
                MatchedResults matched = result.getMatchedResults();
                Collection<MatchedRecord> recordList = matched.getMatchedRecords();
                Collection<MatchedRecord> recordListWithDetails = new ArrayList<>();
                for(MatchedRecord record : recordList) {
                    WlRecordDetails wlDetails = new WlRecordDetails() ;
                    String outputDetails = wlDetails.getRecordDetails(record.getDocId());

                    record.setDetails(outputDetails);

                }
                response = result.toJson();
                //  throw new Exception("TEST");



            } catch (Exception e) {
                logger.info("unable to fetch response from WL for URL: ---> " + queryJson);
                logger.info("Retry Count: "+count);
            }

            return response;
        }

}
