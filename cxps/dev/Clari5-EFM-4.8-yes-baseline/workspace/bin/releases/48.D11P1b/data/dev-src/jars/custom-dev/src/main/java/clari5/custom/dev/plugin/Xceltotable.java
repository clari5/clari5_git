package clari5.custom.dev.plugin;

import clari5.platform.applayer.CxpsDaemon;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import groovy.transform.Synchronized;
import org.apache.kafka.common.protocol.types.Field;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Xceltotable extends CxpsDaemon implements ICxResource {

    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(Xceltotable.class);


    @Override
    public Object getData() throws RuntimeFatalException {
        System.out.println("inside getDate()");
        return getFilename();
    }

    @Override
    public void processData(Object o) throws RuntimeFatalException {

        //Get Data from db which is having status I and insert to DB'

        try {
            System.out.println("PROCESS DATA");
            if (o != null) {
                String val = (String) o;
                if (val != null) {
                    String value[] = val.split(":");
                    String fileName = value[0];
                    String tableName = value[1];
                    Xcelread xlsins = new Xcelread();
                    List<String> status = xlsins.insertXcl(fileName, tableName);
                    if (status != null && status.get(0) == "success") {
                        String sql = " update CUSTOM_CL5_UPLOAD_STATS set FILE_UPLOAD_STATS = 'C', UPDATED_TIME = CURRENT_TIMESTAMP , DB_INS_ROWS = '"+status.get(1)+ "' where FILENAME = '" + fileName + "'";
                        try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
                            int i = ps.executeUpdate();
                            if (i > 0) {
                                cxpsLogger.info("Sucessfully completed insertion into table");
                                con.commit();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        String sql = " update CUSTOM_CL5_UPLOAD_STATS set FILE_UPLOAD_STATS = 'E',EXCEPTION = '" + status.get(0) + "', UPDATED_TIME = CURRENT_TIMESTAMP where FILENAME = '" + fileName + "'";
                        try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
                            int i = ps.executeUpdate();
                            if (i > 0) {
                                cxpsLogger.info("Exception While inserting record to DB also updating the values to main table");
                                con.commit();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    public void configure(Hocon h, Hocon location) {

        // this.configure(h);
    }


    public synchronized String getFilename() {
        String fileName = null;
        String tableName = null;
        boolean flag = false;
        String sql = "select FILENAME,TABLENAME from CUSTOM_CL5_UPLOAD_STATS where FILE_UPLOAD_STATS = 'U' and rownum <= 1";
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                flag = true;
                fileName = rs.getString("FILENAME");
                tableName = rs.getString("TABLENAME");
            }
            cxpsLogger.info("File Name for processing is : [ " + fileName + "]");
            if (flag) {
                String sql2 = " update CUSTOM_CL5_UPLOAD_STATS set FILE_UPLOAD_STATS = 'I',UPDATED_TIME = CURRENT_TIMESTAMP where FILENAME = '" + fileName + "'";
                PreparedStatement ps2 = con.prepareStatement(sql2);
                ps2.executeUpdate();
                con.commit();
                return fileName + ":" + tableName;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}
