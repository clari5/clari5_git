package clari5.custom.yes.integration.data;

public class FT_CardsTxn extends ITableData {
	private String tableName = "FT_CARDS";
	private String event_type = "FT_CardsTxn";
    private String event_id;
	private String sys_time;
	private String host_id;
	private String txn_date;
	private String acct_no = "";
	private String succ_fail_flg;
	private String source;
	private String proc_code;
	private String transaction_code;
	private String mcc;
	private String merchant_name;
	private String merchant_id = "";
	private String user_type;
	private String pos_id;
	private String pos_entry_mode;
	private String city_code;
	private String status = "";
	private String time_slot;
	private String  msg_type="";
	private String txn_amt;
	private String billing_amt;
	private String txn_reference_no;
	private String   avl_bal;
	private String resp_code = "";
	private String bank_code;
	private String payee_name;
	private String payee_id;
	private String bin;
	private String  txn_br_city_code = "";
	private String home_br_city_code = "";
	private String emp_id = "";
	private String card_type_flg = "";
	private String ip_address;
	private String country_code;
	private String domestic_int_flg = "";
	private String channel = "";
	private String cust_id = "";
	private String card_no = "";
	private String card_type ="";



	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}

	public void setSys_time(String sys_time) {
		this.sys_time = sys_time;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public void setTxn_date(String txn_date) {
		this.txn_date = txn_date;
	}

	public void setAcct_no(String acct_no) {
		this.acct_no = acct_no;
	}

	public void setSucc_fail_flg(String succ_fail_flg) {
		this.succ_fail_flg = succ_fail_flg;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setProc_code(String proc_code) {
		this.proc_code = proc_code;
	}

	public void setTransaction_code(String transaction_code) {
		this.transaction_code = transaction_code;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	public void setMerchant_name(String merchant_name) {
		this.merchant_name = merchant_name;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public void setPos_id(String pos_id) {
		this.pos_id = pos_id;
	}

	public void setPos_entry_mode(String pos_entry_mode) {
		this.pos_entry_mode = pos_entry_mode;
	}

	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTime_slot(String time_slot) {
		this.time_slot = time_slot;
	}

	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}

	public void setTxn_amt(String txn_amt) {
		this.txn_amt = txn_amt;
	}

	public void setBilling_amt(String billing_amt) {
		this.billing_amt = billing_amt;
	}

	public void setTxn_reference_no(String txn_reference_no) {
		this.txn_reference_no = txn_reference_no;
	}

	public void setAvl_bal(String avl_bal) {
		this.avl_bal = avl_bal;
	}

	public void setResp_code(String resp_code) {
		this.resp_code = resp_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public void setPayee_name(String payee_name) {
		this.payee_name = payee_name;
	}

	public void setPayee_id(String payee_id) {
		this.payee_id = payee_id;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public void setTxn_br_city_code(String txn_br_city_code) {
		this.txn_br_city_code = txn_br_city_code;
	}

	public void setHome_br_city_code(String home_br_city_code) {
		this.home_br_city_code = home_br_city_code;
	}

	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}

	public void setCard_type_flg(String card_type_flg) {
		this.card_type_flg = card_type_flg;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public void setDomestic_int_flg(String domestic_int_flg) {
		this.domestic_int_flg = domestic_int_flg;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}

	public String getTableName() {
		return tableName;
	}

	public String getEvent_type() {
		return event_type;
	}

	public String getSys_time() {
		return sys_time;
	}

	public String getHost_id() {
		return host_id;
	}

	public String getTxn_date() {
		return txn_date;
	}

	public String getAcct_no() {
		return acct_no;
	}

	public String getSucc_fail_flg() {
		return succ_fail_flg;
	}

	public String getSource() {
		return source;
	}

	public String getProc_code() {
		return proc_code;
	}

	public String getTransaction_code() {
		return transaction_code;
	}

	public String getMcc() {
		return mcc;
	}

	public String getMerchant_name() {
		return merchant_name;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public String getUser_type() {
		return user_type;
	}

	public String getPos_id() {
		return pos_id;
	}

	public String getPos_entry_mode() {
		return pos_entry_mode;
	}

	public String getCity_code() {
		return city_code;
	}

	public String getStatus() {
		return status;
	}

	public String getTime_slot() {
		return time_slot;
	}

	public String getMsg_type() {
		return msg_type;
	}

	public String getTxn_amt() {
		return txn_amt;
	}

	public String getBilling_amt() {
		return billing_amt;
	}

	public String getTxn_reference_no() {
		return txn_reference_no;
	}

	public String getAvl_bal() {
		return avl_bal;
	}

	public String getResp_code() {
		return resp_code;
	}

	public String getBank_code() {
		return bank_code;
	}

	public String getPayee_name() {
		return payee_name;
	}

	public String getPayee_id() {
		return payee_id;
	}

	public String getBin() {
		return bin;
	}

	public String getTxn_br_city_code() {
		return txn_br_city_code;
	}

	public String getHome_br_city_code() {
		return home_br_city_code;
	}

	public String getEmp_id() {
		return emp_id;
	}

	public String getCard_type_flg() {
		return card_type_flg;
	}

	public String getIp_address() {
		return ip_address;
	}

	public String getCountry_code() {
		return country_code;
	}

	public String getDomestic_int_flg() {
		return domestic_int_flg;
	}

	public String getChannel() {
		return channel;
	}

	public String getCust_id() {
		return cust_id;
	}

	public String getCard_no() {
		return card_no;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getCard_type() {
		return card_type;
	}

	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
}
