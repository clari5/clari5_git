package clari5.custom.inbox.processors.daemon;

import clari5.custom.inbox.processors.db.CustomIboxData;
import clari5.custom.inbox.processors.retryservice.RetryTatMailingService;
import clari5.custom.inbox.processors.retryservice.RetryTatMailingServiceImpl;
import clari5.custom.inbox.processors.tat.Cl5IbxRetry;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.util.concurrent.ConcurrentLinkedQueue;

public class RetryDaemon extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(RetryDaemon.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
        //System.out.println("inside getData in retry: ");
        logger.debug("inside getData in retry: ");
        return CustomIboxData.getCl5IbxRetryItemData();
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        try {
            if (o instanceof ConcurrentLinkedQueue) {
                //System.out.println("inside if retry");
                logger.debug("inside if retry");
                ConcurrentLinkedQueue<Cl5IbxRetry> createdItems = (ConcurrentLinkedQueue<Cl5IbxRetry>) o;
                //System.out.println("retry created items: " + createdItems);
                logger.debug("retry created items: " + createdItems);
                RetryTatMailingService retryTat = new RetryTatMailingServiceImpl();
                retryTat.sendMail(createdItems);

                retryTat = null;

                //calling garbage collector
                System.gc();
            }

            Thread.sleep(Long.parseLong(CustomIboxData.hocon.getString("tat.retrySleep")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void configure(Hocon h) {

        logger.debug("Retry Tat Inbox Daemon Configured");
        //System.out.println("Retry Tat Inbox Daemon Configured");
    }
}
