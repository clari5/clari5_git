package clari5.custom.jasper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CustMatch {
    String id  ;
    String cust_id;
    Map<String, Set<String>> matchResult =  new HashMap<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Set<String>> getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(Map<String, Set<String>> matchResult) {
        this.matchResult = matchResult;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    @Override
    public String toString() {
        return "CustMatch{" +
                "id='" + id + '\'' +
                ", cust_id='" + cust_id + '\'' +
                ", matchResult=" + matchResult.toString() +
                '}';
    }

}
