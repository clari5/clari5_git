package clari5.custom.dev.plugin;


import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

//import clari5.custom.deactivation.test.CardServer;
//import clari5.custom.deactivation.userid.controller.NetBankingUserIdController;


public class RequestHandler extends Application {

    private Set<Object> singletons = new HashSet<>();

    @Override
    public Set<Class<?>> getClasses() {
        return null;
    }

    public RequestHandler() {
        singletons.add(new Getdata());
       // singletons.add(NetBankingUserIdController.class);
       // singletons.add(CardServer.class);
    }

    @Override
    public Set<Object> getSingletons() {
        return this.singletons;
    }
}
