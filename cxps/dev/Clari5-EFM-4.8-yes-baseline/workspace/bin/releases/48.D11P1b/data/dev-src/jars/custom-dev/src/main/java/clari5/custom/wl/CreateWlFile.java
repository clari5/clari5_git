package clari5.custom.wl;


import clari5.custom.db.wl.TableTemplates;
import clari5.custom.db.wl.WlListGen;
import clari5.platform.applayer.Clari5;
import clari5.tools.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import java.io.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.File;
import java.io.FileWriter;


/**
 * @author shishir
 * @since 22/06/2018
 */
public class CreateWlFile {

    private static final CxpsLogger logger = CxpsLogger.getLogger(CreateWlFile.class);
    static Hocon hocon;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("custom-wl-indexing.conf");

    }

    public void getTableList() {
        logger.info("Start time " + LocalDateTime.now());
        List<String> list = hocon.getStringList("tables");
        logger.info("Tables List " + list.toString());

        WlListGen wlListGen = new WlListGen(hocon);

        for (String tableName : list) {

            TableTemplates tableTemplates = wlListGen.createTblTemplate(tableName.toLowerCase());
            genCsvFile(tableTemplates);
            tableTemplates.getHeader().clear();
            tableTemplates.getValues().clear();
        }

        logger.info("End time " + LocalDateTime.now());

    }

    public void genCsvFile(TableTemplates tableTemplates) {

        String outputPath = hocon.get("csv-file").getString("op-path");
        String listName=(hocon.get("csv-file").get("listName").getString("branch_master"));

        /**
         * fetching the HOME property
         * and assigning to output path
         */

        if (outputPath!=null || !outputPath.isEmpty()) {

              outputPath = System.getenv("HOME");
        }

        logger.info("Table Template size " + tableTemplates.size());

        FileWriter fileWriter = null;
        try {

            // output path is /home/manoj/
            File dir = new File(outputPath + "/data/tmp/wl");

            if (!dir.exists()) {
                dir.mkdirs();
            }
            /**
             * changing the name of the file to listName_tableName
             *
             */
            String csvPath = dir.getPath() + "/"+listName.toUpperCase()+"_"+ tableTemplates.getTableName() + "."+listName.toLowerCase();
            File file = new File(csvPath);

            if (file.exists()) {

                file.delete();
            }
            if (file.createNewFile()) {
                logger.info("File is created " + file.getAbsolutePath());
            } else {
                logger.warn("Failed to create a file " + file.getAbsolutePath());
                return;
            }

            fileWriter = new FileWriter(file);
            int headerLength = tableTemplates.getHeader().size();
            writeCsv(fileWriter, tableTemplates.getHeader(), headerLength);
            writeCsv(fileWriter, tableTemplates.getValues(), headerLength);


            /**
             * converting into zip
             * (outputPath + "/data/tmp/wl");
             *
             * removing the .csv and converting the filename to .zip
             */
            String zipname=null;
            if(file.getName().lastIndexOf(".")>0){
                zipname=file.getName().substring(0,file.getName().lastIndexOf(".")).concat(".zip");
            }
            //FileOutputStream fos = new FileOutputStream(outputPath+"/data/tmp/wl/"+zipname); // backup
            String zippath=System.getenv("CX_DATA_DIR")+"/tas/upDir/";
            File zipFilePath=new File(zippath);
            /**
             * check whether this path exist or not
             * if not create it
             */
            if(!zipFilePath.exists()){
                zipFilePath.mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(zippath+zipname);
            ZipOutputStream zipOS = new ZipOutputStream(fos);
            

            /**
             * closing the file writer
             *
             */
            if(fileWriter!=null){
                fileWriter.close();

            }

            // invoking the function for writing the csv to a zip file
            writeToZipFile(csvPath,zipOS);
            //moveToZip(outputPath+"/data/tmp/wl/"+zipname);
            zipOS.close();
            fos.close();




        } catch (IOException io) {
            logger.error("Failed to generate wl file " + io.getMessage() + "\n Cause " + io.getCause());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException io) {
                logger.error("Failed wile closing file writer " + io.getMessage() + "Cause" + io.getCause());
            }
        }

    }

    public static void writeToZipFile(String path, ZipOutputStream zipStream) throws FileNotFoundException, IOException {
        System.out.println("Writing file : '" + path + "' to zip file");
        File aFile = new File(path);
        //File aFile=new File("/home/manoj/Desktop/dump/CSV_branch_master.csv");
        aFile.setExecutable(true);
        FileInputStream fis = new FileInputStream(aFile);
        ZipEntry zipEntry = new ZipEntry(aFile.getName());
        zipStream.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipStream.write(bytes, 0, length);
        }
        zipStream.closeEntry();
        fis.close();
   }




    public void writeCsv(FileWriter writer, List<String> list, int headLength) throws IOException {
        String delimiter = hocon.get("csv-file").getString("delimiter");

        int headcount = 0;
        for (String fact : list) {

            if (headcount != headLength - 1) {
                headcount++;
                writer.write(fact + delimiter);
            } else {
                headcount = 0;
                writer.write(fact);
                writer.write("\n");
            }
        }

    }


    public static void main(String[] args) {
        //System.out.println(getConnection());
        Clari5.batchBootstrap("efm", "efm-clari5.conf");
        System.out.println("Start time:" + LocalDateTime.now());
        CreateWlFile createWlFile = new CreateWlFile();
        createWlFile.getTableList();
        System.out.println("End time:" + LocalDateTime.now());
    }
}
