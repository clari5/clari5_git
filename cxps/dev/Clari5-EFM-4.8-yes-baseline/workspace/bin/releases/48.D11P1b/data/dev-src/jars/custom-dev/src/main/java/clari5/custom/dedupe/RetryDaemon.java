package clari5.custom.dedupe;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import cxps.apex.utils.StringUtils;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

 public class RetryDaemon extends CxpsRunnable implements ICxResource{
    private static final CxpsLogger logger = CxpsLogger.getLogger(RetryDaemon.class);

     @Override
    protected Object getData() throws RuntimeFatalException{
        return RetryCustomer.getRetryData();
    }
     @Override
    protected void processData(Object o) throws RuntimeFatalException {
   //       JSONObject retryJson = (JSONObject) o;
//        WatchListResponseFetcher.reponseReciever(retryJson.toString(), null, null, null);
        logger.debug("Inside the Process data");
        if(o instanceof ConcurrentLinkedQueue){

            ConcurrentLinkedQueue<JSONObject> dataQueue = (ConcurrentLinkedQueue<JSONObject>) o;
            while (!dataQueue.isEmpty()) {
               JSONObject jsonObject = dataQueue.poll();



                for ( int count = 1; count < 4; count++) {
                    String response = WatchListResponseFetcher.retryAgainInsert(jsonObject.getString("Request_Json"),jsonObject.getString("CUST_ID"), jsonObject.getString("Rule_name"), null,jsonObject.getInt("Serial_no"),count);

                    if(response.trim().length() > 1){

                        RetryCustomer.getStatusChange(jsonObject.getInt("Serial_no"),"C",count);

                        LinkedHashMap<String, LinkedList<String>> wlResultSetMap = CustIdListWL.getResultSetMap(response,jsonObject.get("Rule_name").toString());
                        InsertWlExactMatch insertWlExactMatch = new InsertWlExactMatch();

                        LinkedHashMap<String, String> modifiedCustomerRecord = new LinkedHashMap<>();
                        modifiedCustomerRecord.put("CUST_ID",jsonObject.getString("CUST_ID"));

                        insertWlExactMatch.insertResultSet(wlResultSetMap, modifiedCustomerRecord);
                        break;
                    }else{
                        if(count==3){
                            logger.info("Tried Maximum time ......not able to insert");
                            RetryCustomer.getStatusChange(jsonObject.getInt("Serial_no"),"E",count);
                        }
                    }
                }

            }

        }
    }

     @Override
     public void configure(Hocon h) {
         logger.debug("Retry Configured");
     }

}
