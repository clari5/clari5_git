package clari5.custom.dedupe;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import clari5.tools.util.Hocon;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;


public class ModifiedCustomer {

    private static final CxpsLogger logger = CxpsLogger.getLogger(ModifiedCustomer.class);
    protected static List<String> ruleList = new LinkedList<>();
    static Hocon newModifiedCustomer;
    static private boolean bootstrap = false;
    static int responseCount = 0;

    static {
        if (!bootstrap) {
            synchronized (ModifiedCustomer.class) {
                if (!bootstrap) {
                    newModifiedCustomer = new Hocon();
                    newModifiedCustomer.loadFromContext("new_modified_customers.conf");
                    ruleList = newModifiedCustomer.getStringList("query.ruleName");
                    bootstrap = true;
                }
            }
        }
    }


    synchronized static ConcurrentLinkedQueue<LinkedHashMap> getData() {

        ConcurrentLinkedQueue<LinkedHashMap> queue = new ConcurrentLinkedQueue<>();

        try (RDBMSSession session = (RDBMSSession) Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                queue = ModifiedCustomer.getModifiedCstomer(connection);
                if (!queue.isEmpty()) {
                    for (LinkedHashMap<String, String> modifiedCustomerRecordMap : queue) {
                        if (!modifiedCustomerRecordMap.isEmpty()) {
                            ModifiedCustomer.updateModifiedCstomer(connection, modifiedCustomerRecordMap);
                        }
                    }
                } else {
                    return null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queue;
    }


    public void process(LinkedHashMap<String, String> modifiedCustomerRecordMap) {
        try {
            logger.debug("Fresh data picked and stored inside map -->" + modifiedCustomerRecordMap);
            if (!modifiedCustomerRecordMap.isEmpty()) {
                logger.debug("ruleList Size: " + ruleList + "\nrule list value:  " + ruleList);
                ruleList.forEach(ruleName -> {
                    String watchListResponse = "";
                    try {
                        logger.debug("iterating over ruleList: " + ruleName);
                        long beforeTime = System.currentTimeMillis();
                        System.out.println("Thread name hiting WL API: ---> " + Thread.currentThread().getName() +
                                " for ruleName: ----> " + ruleName);
                        watchListResponse = WatchListResponseFetcher.reponseReciever(
                                WatchListResponseFetcher.createRequest(modifiedCustomerRecordMap, ruleName),
                                modifiedCustomerRecordMap.get("CUST_ID"), ruleName, "Fresh");
                        long responseTime = System.currentTimeMillis() - beforeTime;
                        logger.debug("reponse from watchList --> [" + responseTime + "]ms  --> " + ruleName + " --> " + watchListResponse);
                        if (watchListResponse != null || !watchListResponse.isEmpty()) {
                            long resultSetProcess = System.currentTimeMillis();
                            LinkedHashMap<String, LinkedList<String>> wlResultSetMap = CustIdListWL.getResultSetMap(watchListResponse, ruleName);
                            logger.debug("result set processing time: ---> " + (System.currentTimeMillis() - resultSetProcess));
                            responseCount = responseCount + wlResultSetMap.size();
                            logger.debug("total responsecount; ------>>> " + responseCount);
                                logger.debug("WL Result Set size for customer id -->>>>" + modifiedCustomerRecordMap.get("CUST_ID") +
                                        " is --->>>> " + wlResultSetMap.size());
                            long dbInsertStartTime = System.currentTimeMillis();
                            InsertWlExactMatch insertWlExactMatch = new InsertWlExactMatch();
                            insertWlExactMatch.insertResultSet(wlResultSetMap, modifiedCustomerRecordMap);
                            long dbInsertEndTime = System.currentTimeMillis() - dbInsertStartTime;
                            logger.debug("DB insertion per record:[ " + dbInsertEndTime + " ]ms");
                        }
                    } catch (Exception e) {
                        System.out.println("Error getting response. Response fetched is : ----> " + watchListResponse);
                    }
                });
            }
        } catch (Exception e) {
            System.out.println("Error decoding result for integral customer: ----> " + modifiedCustomerRecordMap.get("CUST_ID"));
            e.printStackTrace();
        }
    }

    private static ConcurrentLinkedQueue<LinkedHashMap> getModifiedCstomer(CxConnection connection) {

        LinkedHashMap<String, String> modifiedData = new LinkedHashMap<>();
        ConcurrentLinkedQueue<LinkedHashMap> queue = new ConcurrentLinkedQueue<LinkedHashMap>();

        try (PreparedStatement ps = connection.prepareStatement(newModifiedCustomer.getString("query.select"))) {
            ps.setString(1, "F");
            logger.debug("Select fresh data from  new_modified_customer table: --> " + newModifiedCustomer.getString("query.select"));
            try (ResultSet rs = ps.executeQuery()) {
                int count = rs.getMetaData().getColumnCount();

                while (rs.next()) {
                    modifiedData = new LinkedHashMap<>();
                    for (int i = 1; i <= count; i++) {
                        modifiedData.put(rs.getMetaData().getColumnName(i), rs.getString(rs.getMetaData().getColumnName(i)));
                    }
                    queue.add(modifiedData);
                }
                connection.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return queue;
    }

    private static void updateModifiedCstomer(CxConnection connection, LinkedHashMap<String, String> custId) {

        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(newModifiedCustomer.getString("query.update"));
            ps.setString(1, "P");
            ps.setString(2, custId.get("CUST_ID"));
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}