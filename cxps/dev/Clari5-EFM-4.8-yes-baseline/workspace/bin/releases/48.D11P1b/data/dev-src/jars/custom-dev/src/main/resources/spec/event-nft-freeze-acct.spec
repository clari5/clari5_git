cxps.events.event.nft-freeze-acct{
  table-name : EVENT_NFT_FREEZEACCT
  event-mnemonic: NF
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
	cust_id: {db : true ,raw_name : cust_Id ,type : "string:200", custom-getter:Cust_Id}
	acct_id: {db : true ,raw_name : acct_Id ,type : "string:200", custom-getter:Acct_Id}
	debit_freeze: {db : true ,raw_name : debit_Freeze ,type : "string:200", custom-getter:Debit_Freeze}
	aval_balance: {db : true ,raw_name : aval_Balance ,type : "number:11,2", custom-getter:Aval_Balance}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	}
}
