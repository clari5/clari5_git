// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_EipoapplicationEventMapper extends EventMapper<FT_EipoapplicationEvent> {

public FT_EipoapplicationEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_EipoapplicationEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_EipoapplicationEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_EipoapplicationEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_EipoapplicationEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_EipoapplicationEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_EipoapplicationEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTwoFaMode());
            preparedStatement.setDouble(i++, obj.getBid1Amount());
            preparedStatement.setDouble(i++, obj.getBid2Amount());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getCustSegment());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getIpoType());
            preparedStatement.setDouble(i++, obj.getBid7Amount());
            preparedStatement.setDouble(i++, obj.getBid8Amount());
            preparedStatement.setDouble(i++, obj.getTotalApplicationAmount());
            preparedStatement.setDouble(i++, obj.getBid5Amount());
            preparedStatement.setDouble(i++, obj.getBid6Amount());
            preparedStatement.setDouble(i++, obj.getBid9Amount());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setDouble(i++, obj.getBid4Amount());
            preparedStatement.setDouble(i++, obj.getBid3Amount());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getBid13Amount());
            preparedStatement.setString(i++, obj.getTwoFaStatus());
            preparedStatement.setDouble(i++, obj.getBid12Amount());
            preparedStatement.setDouble(i++, obj.getBid14Amount());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getApplicantPan());
            preparedStatement.setString(i++, obj.getObdxTransactionName());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setDouble(i++, obj.getBid16Amount());
            preparedStatement.setDouble(i++, obj.getBid17Amount());
            preparedStatement.setString(i++, obj.getIpoId());
            preparedStatement.setDouble(i++, obj.getBid10Amount());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getObdxModuleName());
            preparedStatement.setDouble(i++, obj.getBid11Amount());
            preparedStatement.setString(i++, obj.getCasaAcNo());
            preparedStatement.setString(i++, obj.getApplicantType());
            preparedStatement.setDouble(i++, obj.getBid15Amount());
            preparedStatement.setDouble(i++, obj.getBid18Amount());
            preparedStatement.setDouble(i++, obj.getAccountBalance());
            preparedStatement.setString(i++, obj.getRiskBand());
            preparedStatement.setString(i++, obj.getApplicantName());
            preparedStatement.setString(i++, obj.getApplicantUniqueIdentifier());
            preparedStatement.setString(i++, obj.getSessionId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_EIPOAPPLICATION]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_EipoapplicationEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_EipoapplicationEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_EIPOAPPLICATION"));
        putList = new ArrayList<>();

        for (FT_EipoapplicationEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TWO_FA_MODE",  obj.getTwoFaMode());
            p = this.insert(p, "BID1_AMOUNT", String.valueOf(obj.getBid1Amount()));
            p = this.insert(p, "BID2_AMOUNT", String.valueOf(obj.getBid2Amount()));
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "CUST_SEGMENT",  obj.getCustSegment());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "IPO_TYPE",  obj.getIpoType());
            p = this.insert(p, "BID7_AMOUNT", String.valueOf(obj.getBid7Amount()));
            p = this.insert(p, "BID8_AMOUNT", String.valueOf(obj.getBid8Amount()));
            p = this.insert(p, "TOTAL_APPLICATION_AMOUNT", String.valueOf(obj.getTotalApplicationAmount()));
            p = this.insert(p, "BID5_AMOUNT", String.valueOf(obj.getBid5Amount()));
            p = this.insert(p, "BID6_AMOUNT", String.valueOf(obj.getBid6Amount()));
            p = this.insert(p, "BID9_AMOUNT", String.valueOf(obj.getBid9Amount()));
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "BID4_AMOUNT", String.valueOf(obj.getBid4Amount()));
            p = this.insert(p, "BID3_AMOUNT", String.valueOf(obj.getBid3Amount()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "BID13_AMOUNT", String.valueOf(obj.getBid13Amount()));
            p = this.insert(p, "TWO_FA_STATUS",  obj.getTwoFaStatus());
            p = this.insert(p, "BID12_AMOUNT", String.valueOf(obj.getBid12Amount()));
            p = this.insert(p, "BID14_AMOUNT", String.valueOf(obj.getBid14Amount()));
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "APPLICANT_PAN",  obj.getApplicantPan());
            p = this.insert(p, "OBDX_TRANSACTION_NAME",  obj.getObdxTransactionName());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "BID16_AMOUNT", String.valueOf(obj.getBid16Amount()));
            p = this.insert(p, "BID17_AMOUNT", String.valueOf(obj.getBid17Amount()));
            p = this.insert(p, "IPO_ID",  obj.getIpoId());
            p = this.insert(p, "BID10_AMOUNT", String.valueOf(obj.getBid10Amount()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "OBDX_MODULE_NAME",  obj.getObdxModuleName());
            p = this.insert(p, "BID11_AMOUNT", String.valueOf(obj.getBid11Amount()));
            p = this.insert(p, "CASA_AC_NO",  obj.getCasaAcNo());
            p = this.insert(p, "APPLICANT_TYPE",  obj.getApplicantType());
            p = this.insert(p, "BID15_AMOUNT", String.valueOf(obj.getBid15Amount()));
            p = this.insert(p, "BID18_AMOUNT", String.valueOf(obj.getBid18Amount()));
            p = this.insert(p, "ACCOUNT_BALANCE", String.valueOf(obj.getAccountBalance()));
            p = this.insert(p, "RISK_BAND",  obj.getRiskBand());
            p = this.insert(p, "APPLICANT_NAME",  obj.getApplicantName());
            p = this.insert(p, "APPLICANT_UNIQUE_IDENTIFIER",  obj.getApplicantUniqueIdentifier());
            p = this.insert(p, "SESSION_ID",  obj.getSessionId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_EIPOAPPLICATION"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_EIPOAPPLICATION]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_EIPOAPPLICATION]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_EipoapplicationEvent obj = new FT_EipoapplicationEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTwoFaMode(rs.getString("TWO_FA_MODE"));
    obj.setBid1Amount(rs.getDouble("BID1_AMOUNT"));
    obj.setBid2Amount(rs.getDouble("BID2_AMOUNT"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setCustSegment(rs.getString("CUST_SEGMENT"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setIpoType(rs.getString("IPO_TYPE"));
    obj.setBid7Amount(rs.getDouble("BID7_AMOUNT"));
    obj.setBid8Amount(rs.getDouble("BID8_AMOUNT"));
    obj.setTotalApplicationAmount(rs.getDouble("TOTAL_APPLICATION_AMOUNT"));
    obj.setBid5Amount(rs.getDouble("BID5_AMOUNT"));
    obj.setBid6Amount(rs.getDouble("BID6_AMOUNT"));
    obj.setBid9Amount(rs.getDouble("BID9_AMOUNT"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setBid4Amount(rs.getDouble("BID4_AMOUNT"));
    obj.setBid3Amount(rs.getDouble("BID3_AMOUNT"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setBid13Amount(rs.getDouble("BID13_AMOUNT"));
    obj.setTwoFaStatus(rs.getString("TWO_FA_STATUS"));
    obj.setBid12Amount(rs.getDouble("BID12_AMOUNT"));
    obj.setBid14Amount(rs.getDouble("BID14_AMOUNT"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setApplicantPan(rs.getString("APPLICANT_PAN"));
    obj.setObdxTransactionName(rs.getString("OBDX_TRANSACTION_NAME"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setBid16Amount(rs.getDouble("BID16_AMOUNT"));
    obj.setBid17Amount(rs.getDouble("BID17_AMOUNT"));
    obj.setIpoId(rs.getString("IPO_ID"));
    obj.setBid10Amount(rs.getDouble("BID10_AMOUNT"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setObdxModuleName(rs.getString("OBDX_MODULE_NAME"));
    obj.setBid11Amount(rs.getDouble("BID11_AMOUNT"));
    obj.setCasaAcNo(rs.getString("CASA_AC_NO"));
    obj.setApplicantType(rs.getString("APPLICANT_TYPE"));
    obj.setBid15Amount(rs.getDouble("BID15_AMOUNT"));
    obj.setBid18Amount(rs.getDouble("BID18_AMOUNT"));
    obj.setAccountBalance(rs.getDouble("ACCOUNT_BALANCE"));
    obj.setRiskBand(rs.getString("RISK_BAND"));
    obj.setApplicantName(rs.getString("APPLICANT_NAME"));
    obj.setApplicantUniqueIdentifier(rs.getString("APPLICANT_UNIQUE_IDENTIFIER"));
    obj.setSessionId(rs.getString("SESSION_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_EIPOAPPLICATION]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_EipoapplicationEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_EipoapplicationEvent> events;
 FT_EipoapplicationEvent obj = new FT_EipoapplicationEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_EIPOAPPLICATION"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_EipoapplicationEvent obj = new FT_EipoapplicationEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTwoFaMode(getColumnValue(rs, "TWO_FA_MODE"));
            obj.setBid1Amount(EventHelper.toDouble(getColumnValue(rs, "BID1_AMOUNT")));
            obj.setBid2Amount(EventHelper.toDouble(getColumnValue(rs, "BID2_AMOUNT")));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setCustSegment(getColumnValue(rs, "CUST_SEGMENT"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setIpoType(getColumnValue(rs, "IPO_TYPE"));
            obj.setBid7Amount(EventHelper.toDouble(getColumnValue(rs, "BID7_AMOUNT")));
            obj.setBid8Amount(EventHelper.toDouble(getColumnValue(rs, "BID8_AMOUNT")));
            obj.setTotalApplicationAmount(EventHelper.toDouble(getColumnValue(rs, "TOTAL_APPLICATION_AMOUNT")));
            obj.setBid5Amount(EventHelper.toDouble(getColumnValue(rs, "BID5_AMOUNT")));
            obj.setBid6Amount(EventHelper.toDouble(getColumnValue(rs, "BID6_AMOUNT")));
            obj.setBid9Amount(EventHelper.toDouble(getColumnValue(rs, "BID9_AMOUNT")));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setBid4Amount(EventHelper.toDouble(getColumnValue(rs, "BID4_AMOUNT")));
            obj.setBid3Amount(EventHelper.toDouble(getColumnValue(rs, "BID3_AMOUNT")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setBid13Amount(EventHelper.toDouble(getColumnValue(rs, "BID13_AMOUNT")));
            obj.setTwoFaStatus(getColumnValue(rs, "TWO_FA_STATUS"));
            obj.setBid12Amount(EventHelper.toDouble(getColumnValue(rs, "BID12_AMOUNT")));
            obj.setBid14Amount(EventHelper.toDouble(getColumnValue(rs, "BID14_AMOUNT")));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setApplicantPan(getColumnValue(rs, "APPLICANT_PAN"));
            obj.setObdxTransactionName(getColumnValue(rs, "OBDX_TRANSACTION_NAME"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setBid16Amount(EventHelper.toDouble(getColumnValue(rs, "BID16_AMOUNT")));
            obj.setBid17Amount(EventHelper.toDouble(getColumnValue(rs, "BID17_AMOUNT")));
            obj.setIpoId(getColumnValue(rs, "IPO_ID"));
            obj.setBid10Amount(EventHelper.toDouble(getColumnValue(rs, "BID10_AMOUNT")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setObdxModuleName(getColumnValue(rs, "OBDX_MODULE_NAME"));
            obj.setBid11Amount(EventHelper.toDouble(getColumnValue(rs, "BID11_AMOUNT")));
            obj.setCasaAcNo(getColumnValue(rs, "CASA_AC_NO"));
            obj.setApplicantType(getColumnValue(rs, "APPLICANT_TYPE"));
            obj.setBid15Amount(EventHelper.toDouble(getColumnValue(rs, "BID15_AMOUNT")));
            obj.setBid18Amount(EventHelper.toDouble(getColumnValue(rs, "BID18_AMOUNT")));
            obj.setAccountBalance(EventHelper.toDouble(getColumnValue(rs, "ACCOUNT_BALANCE")));
            obj.setRiskBand(getColumnValue(rs, "RISK_BAND"));
            obj.setApplicantName(getColumnValue(rs, "APPLICANT_NAME"));
            obj.setApplicantUniqueIdentifier(getColumnValue(rs, "APPLICANT_UNIQUE_IDENTIFIER"));
            obj.setSessionId(getColumnValue(rs, "SESSION_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_EIPOAPPLICATION]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_EIPOAPPLICATION]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TWO_FA_MODE\",\"BID1_AMOUNT\",\"BID2_AMOUNT\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"DEVICE_ID\",\"IPO_TYPE\",\"BID7_AMOUNT\",\"BID8_AMOUNT\",\"TOTAL_APPLICATION_AMOUNT\",\"BID5_AMOUNT\",\"BID6_AMOUNT\",\"BID9_AMOUNT\",\"ADDR_NETWORK\",\"BID4_AMOUNT\",\"BID3_AMOUNT\",\"HOST_ID\",\"BID13_AMOUNT\",\"TWO_FA_STATUS\",\"BID12_AMOUNT\",\"BID14_AMOUNT\",\"IP_CITY\",\"APPLICANT_PAN\",\"OBDX_TRANSACTION_NAME\",\"USER_ID\",\"BID16_AMOUNT\",\"BID17_AMOUNT\",\"IPO_ID\",\"BID10_AMOUNT\",\"SYS_TIME\",\"CUST_ID\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"BID11_AMOUNT\",\"CASA_AC_NO\",\"APPLICANT_TYPE\",\"BID15_AMOUNT\",\"BID18_AMOUNT\",\"ACCOUNT_BALANCE\",\"RISK_BAND\",\"APPLICANT_NAME\",\"APPLICANT_UNIQUE_IDENTIFIER\",\"SESSION_ID\"" +
              " FROM EVENT_FT_EIPOAPPLICATION";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TWO_FA_MODE],[BID1_AMOUNT],[BID2_AMOUNT],[ERROR_DESC],[SUCC_FAIL_FLG],[CUST_SEGMENT],[IP_COUNTRY],[DEVICE_ID],[IPO_TYPE],[BID7_AMOUNT],[BID8_AMOUNT],[TOTAL_APPLICATION_AMOUNT],[BID5_AMOUNT],[BID6_AMOUNT],[BID9_AMOUNT],[ADDR_NETWORK],[BID4_AMOUNT],[BID3_AMOUNT],[HOST_ID],[BID13_AMOUNT],[TWO_FA_STATUS],[BID12_AMOUNT],[BID14_AMOUNT],[IP_CITY],[APPLICANT_PAN],[OBDX_TRANSACTION_NAME],[USER_ID],[BID16_AMOUNT],[BID17_AMOUNT],[IPO_ID],[BID10_AMOUNT],[SYS_TIME],[CUST_ID],[ERROR_CODE],[OBDX_MODULE_NAME],[BID11_AMOUNT],[CASA_AC_NO],[APPLICANT_TYPE],[BID15_AMOUNT],[BID18_AMOUNT],[ACCOUNT_BALANCE],[RISK_BAND],[APPLICANT_NAME],[APPLICANT_UNIQUE_IDENTIFIER],[SESSION_ID]" +
              " FROM EVENT_FT_EIPOAPPLICATION";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TWO_FA_MODE`,`BID1_AMOUNT`,`BID2_AMOUNT`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`CUST_SEGMENT`,`IP_COUNTRY`,`DEVICE_ID`,`IPO_TYPE`,`BID7_AMOUNT`,`BID8_AMOUNT`,`TOTAL_APPLICATION_AMOUNT`,`BID5_AMOUNT`,`BID6_AMOUNT`,`BID9_AMOUNT`,`ADDR_NETWORK`,`BID4_AMOUNT`,`BID3_AMOUNT`,`HOST_ID`,`BID13_AMOUNT`,`TWO_FA_STATUS`,`BID12_AMOUNT`,`BID14_AMOUNT`,`IP_CITY`,`APPLICANT_PAN`,`OBDX_TRANSACTION_NAME`,`USER_ID`,`BID16_AMOUNT`,`BID17_AMOUNT`,`IPO_ID`,`BID10_AMOUNT`,`SYS_TIME`,`CUST_ID`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`BID11_AMOUNT`,`CASA_AC_NO`,`APPLICANT_TYPE`,`BID15_AMOUNT`,`BID18_AMOUNT`,`ACCOUNT_BALANCE`,`RISK_BAND`,`APPLICANT_NAME`,`APPLICANT_UNIQUE_IDENTIFIER`,`SESSION_ID`" +
              " FROM EVENT_FT_EIPOAPPLICATION";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_EIPOAPPLICATION (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TWO_FA_MODE\",\"BID1_AMOUNT\",\"BID2_AMOUNT\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"DEVICE_ID\",\"IPO_TYPE\",\"BID7_AMOUNT\",\"BID8_AMOUNT\",\"TOTAL_APPLICATION_AMOUNT\",\"BID5_AMOUNT\",\"BID6_AMOUNT\",\"BID9_AMOUNT\",\"ADDR_NETWORK\",\"BID4_AMOUNT\",\"BID3_AMOUNT\",\"HOST_ID\",\"BID13_AMOUNT\",\"TWO_FA_STATUS\",\"BID12_AMOUNT\",\"BID14_AMOUNT\",\"IP_CITY\",\"APPLICANT_PAN\",\"OBDX_TRANSACTION_NAME\",\"USER_ID\",\"BID16_AMOUNT\",\"BID17_AMOUNT\",\"IPO_ID\",\"BID10_AMOUNT\",\"SYS_TIME\",\"CUST_ID\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"BID11_AMOUNT\",\"CASA_AC_NO\",\"APPLICANT_TYPE\",\"BID15_AMOUNT\",\"BID18_AMOUNT\",\"ACCOUNT_BALANCE\",\"RISK_BAND\",\"APPLICANT_NAME\",\"APPLICANT_UNIQUE_IDENTIFIER\",\"SESSION_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TWO_FA_MODE],[BID1_AMOUNT],[BID2_AMOUNT],[ERROR_DESC],[SUCC_FAIL_FLG],[CUST_SEGMENT],[IP_COUNTRY],[DEVICE_ID],[IPO_TYPE],[BID7_AMOUNT],[BID8_AMOUNT],[TOTAL_APPLICATION_AMOUNT],[BID5_AMOUNT],[BID6_AMOUNT],[BID9_AMOUNT],[ADDR_NETWORK],[BID4_AMOUNT],[BID3_AMOUNT],[HOST_ID],[BID13_AMOUNT],[TWO_FA_STATUS],[BID12_AMOUNT],[BID14_AMOUNT],[IP_CITY],[APPLICANT_PAN],[OBDX_TRANSACTION_NAME],[USER_ID],[BID16_AMOUNT],[BID17_AMOUNT],[IPO_ID],[BID10_AMOUNT],[SYS_TIME],[CUST_ID],[ERROR_CODE],[OBDX_MODULE_NAME],[BID11_AMOUNT],[CASA_AC_NO],[APPLICANT_TYPE],[BID15_AMOUNT],[BID18_AMOUNT],[ACCOUNT_BALANCE],[RISK_BAND],[APPLICANT_NAME],[APPLICANT_UNIQUE_IDENTIFIER],[SESSION_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TWO_FA_MODE`,`BID1_AMOUNT`,`BID2_AMOUNT`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`CUST_SEGMENT`,`IP_COUNTRY`,`DEVICE_ID`,`IPO_TYPE`,`BID7_AMOUNT`,`BID8_AMOUNT`,`TOTAL_APPLICATION_AMOUNT`,`BID5_AMOUNT`,`BID6_AMOUNT`,`BID9_AMOUNT`,`ADDR_NETWORK`,`BID4_AMOUNT`,`BID3_AMOUNT`,`HOST_ID`,`BID13_AMOUNT`,`TWO_FA_STATUS`,`BID12_AMOUNT`,`BID14_AMOUNT`,`IP_CITY`,`APPLICANT_PAN`,`OBDX_TRANSACTION_NAME`,`USER_ID`,`BID16_AMOUNT`,`BID17_AMOUNT`,`IPO_ID`,`BID10_AMOUNT`,`SYS_TIME`,`CUST_ID`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`BID11_AMOUNT`,`CASA_AC_NO`,`APPLICANT_TYPE`,`BID15_AMOUNT`,`BID18_AMOUNT`,`ACCOUNT_BALANCE`,`RISK_BAND`,`APPLICANT_NAME`,`APPLICANT_UNIQUE_IDENTIFIER`,`SESSION_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

