"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var common_functions_1 = require("./common.functions");
var nbb_data_service_1 = require("../services/nbb.data.service");
var NbbService = (function (_super) {
    __extends(NbbService, _super);
    function NbbService(http, nbbDataService) {
        var _this = _super.call(this) || this;
        _this.isC = [];
        _this.PROGRAM_TYPE = "";
        _this.DropDown = true;
        _this.RadioButton = true;
        _this.question = "";
        _this.selectedAns = new Array();
        _this.nbbDataService = nbbDataService;
        _this.http = http;
        return _this;
    }
    NbbService.prototype.getLoadData = function (dataName, usersUrl, method, postData) {
        // super.logDebugMsg('before call ', postData);
        var _this = this;
        //  let headers = new Headers({ 'Content-Type': 'text/html' });
        var myParams = new http_1.URLSearchParams();
        myParams.append("method", method);
        myParams.append("payload", postData);
        //  let body = method&postData;
        // let options = new RequestOptions({params: myParams });
        return this.http.get(usersUrl + myParams)
            .map(function (res) { return res.text() ? res.json() : res; })
            .subscribe(function (data) {
            switch (dataName) {
                case "scenario":
                    _this.nbbDataService.allscenarios = data; //data.templates;
                    _this.nbbDataService.scenarios = data.scenariosList;
                    _super.prototype.logDebugMsg.call(_this, 'init inside scenario list ', _this.nbbDataService.scenarios);
                    _super.prototype.logDebugMsg.call(_this, 'init inside allscenarios ', _this.nbbDataService.allscenarios);
                    break;
                case "tempscenario":
                    _this.nbbDataService.test = data; //JSON.parse( data);
                    _super.prototype.logDebugMsg.call(_this, 'scenarioList temp scenarios', _this.nbbDataService.test);
                    break;
                case "onselect":
                    _this.nbbDataService.tags = data.eventList; //data;//JSON.parse( data);
                    _super.prototype.logDebugMsg.call(_this, 'event tags ', _this.nbbDataService.tags);
                    _this.nbbDataService.other = data; //data;//JSON.parse( data);
                    _super.prototype.logDebugMsg.call(_this, 'all data ', _this.nbbDataService.tags);
                    break;
                default:
                    _super.prototype.logDebugMsg.call(_this, 'inside default ');
                    _this.nbbDataService.res = data.resp;
                    _super.prototype.logDebugMsg.call(_this, 'status', _this.nbbDataService.res);
            }
        }, function (err) {
            _super.prototype.alertDebugMsg.call(_this, err);
        });
    };
    return NbbService;
}(common_functions_1.CommonFunctions));
NbbService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, nbb_data_service_1.NbbDataService])
], NbbService);
exports.NbbService = NbbService;
//# sourceMappingURL=nbb.service.js.map