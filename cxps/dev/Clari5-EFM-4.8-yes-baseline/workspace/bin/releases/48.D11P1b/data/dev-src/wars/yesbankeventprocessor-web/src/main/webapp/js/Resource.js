
  $('.dropdown-menu a').click(function(){
    $('#selected').text($(this).text());
  });


$("body").on("click", "#btnUpload", function () {
        var allowedFiles = [".xlsx"];
        var fileUpload = $("#fileUpload");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;
    });

$("#clsbtn").on("click", function () {
           window.location.reload();

        });


$(document).ready(function(){


     document.getElementById("comnt").style.display = "none";
    $('.comnt').hide();
    $('.invalid').hide();
      $('.comnt1').hide();
    $('.invalid1').hide();
   //API for Getting Uploadtables From Cl5 upload Table
    //window.location.origin
        $.ajax({ url:"rest/xlupload/getTables",
        "type":"POST",
        "async":false,
        context: document.body,
        success: function(result){
            var output = result;
            var display = document.getElementById("usr");

              for(var i=0;i<output.length;i++){
                var opt = output[i];
                console.log(opt);
                var el = document.createElement("option");
                el.setAttribute("id", opt);
                el.textContent = opt;
                el.value = opt;
                display.appendChild(el);
              }
        }});


        //Cancel button clicked for refresh

        $("#cancelbtn").on("click", function () {
            console.log(" cancel button clicked");
            document.getElementById('usr').value='select'
            document.getElementById('exampleFormControlFile1').value=''

        });

          

        //validate button clicked for file validation

         $("#validatebtn").on("click", function () {
            console.log(" validatebtn button clicked");
              var id = $("#usr").children(":selected").attr("id");
               if(id == null)
               {
                    alert("Please select a Table"); 

               }
               else{
              console.log("tableName: [ "+id+" ]");
              //Reference the FileUpload element.

        var fi = $("#exampleFormControlFile1")[0];
          console.log("File Name : [ "+fi.value+ "] ");
        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
                console.log("regex : [ "+regex+ "] ");
        if(fi.value === "" )
        {
            alert("Please select an Excel file.");

        }
         else if (regex.test(fi.value.toLowerCase())) {
            //checking file size
            if (fi.files.length > 0) { 
                for (var i = 0; i <= fi.files.length - 1; i++) { 
                    const fsize = fi.files.item(i).size; 
                    const file = Math.round((fsize / 1024)); 
                    // The size of the file. 
                    if (file >= 10000) { 
                        alert("File too Big, please select a file less than 4mb"); 
                    } else { 
                    console.log("file size : [ "+file+ "]")
                    } 
                } 
            } 
            var filename = $('#exampleFormControlFile1').val().split('\\').pop();
            console.log("File Name : [ "+filename+"] ");
            var fileappend = new FormData();

              var files = $('#exampleFormControlFile1')[0].files[0]; 
              var tableName = document.getElementById('usr').value;
              console.log("tableName is : [ "+tableName+ " ]");
	       var user=window.user;
              console.log("user : "+user);
	      tableName = tableName + ":" + user;
              fileappend.append('file', files);
              fileappend.append('tableName',tableName);

             console.log("file : "+fileappend);
             //var val = {"fielname":filename,"file":+fileappend};

            
            $.ajax({ url: "rest/xlupload/validate",

                    "type":"POST",
                    "async":false,
                    "data" : fileappend,
                     processData: false,
                    contentType: false,
                    context: document.body,
                    success: function(data){
                    console.log("api sucess : [ "+data+ " ] ");
                    var result = data ;
                    if(result == "validatesuccess"){
                        console.log("validation sucess please submit it for insertion");
                            $("#comnt").modal('show');
                             $('.comnt').show();
                         }
                    else{
                        console.log("validation failed");
                        $("#invalid").modal('show');

                    }

                }});



        
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
 
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        //ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fi.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        //ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fi.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
            window.location.reload();
        }
       }
            
        });


       
            

        //submit button clicked for file validation & uploading file

          $("#submitbtn").on("click", function () {
            console.log(" submitbtn button clicked");

                var id = $("#usr").children(":selected").attr("id");
                  if(id == null)
               {
                    alert("Please select a Table"); 

               }
               else{
              console.log("tableName: [ "+id+" ]");
              //Reference the FileUpload element.
        var fi = $("#exampleFormControlFile1")[0];
          console.log("File Name : [ "+fi.value+ "] ");
        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
                console.log("regex : [ "+regex+ "] ");
        if(fi.value === "" )
        {
            alert("Please select an Excel file.");

        }
         else if (regex.test(fi.value.toLowerCase())) {
            //checking file size
            if (fi.files.length > 0) { 
                for (var i = 0; i <= fi.files.length - 1; i++) { 
                    const fsize = fi.files.item(i).size; 
                    const file = Math.round((fsize / 1024)); 
                    // The size of the file. 
                    if (file >= 10000) { 
                        alert("File too Big, please select a file less than 4mb"); 
                    } else { 
                    console.log("file size : [ "+file+ "]")
                    } 
                } 
            } 
            var filename = $('#exampleFormControlFile1').val().split('\\').pop();
            console.log("File Name : [ "+filename+"] ");
            var fileappend = new FormData();

              var files = $('#exampleFormControlFile1')[0].files[0]; 
              var tableName = document.getElementById('usr').value;
 	      var user=window.user;
              console.log("user : "+user);
	      tableName = tableName + ":" + user;
              fileappend.append('file', files);
              fileappend.append('tableName',tableName);
             console.log("file : "+fileappend);
             //var val = {"fielname":filename,"file":+fileappend};   
	    // var login={"username": user}; 
		//console.log("login"+login);
	     // var loginuser=JSON.stringify(loginuser);
		 // console.log("loginuser"+loginuser);
            $.ajax({ url:  "rest/xlupload/submit",

                    "type":"POST",
                    "async":false,
                    "data" : fileappend,
                     processData: false,
		    contentType:false,
                    context: document.body,
                    success: function(data){
                    console.log("api sucess : [ "+data+ " ] ");
                    var result = data ;
                    if(result == "submitsuccess"){
                        console.log("submit is Successfull ");
                            $("#comnt1").modal('show');
                             $('.comnt1').show();
                         }
                    else{
                        console.log("submit failed");
                        $("#invalid1").modal('show');

                    }

                }});



        
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
 
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        //ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fi.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        //ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fi.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
            window.location.reload();

        }
        }

        });

       //change function
      /* $("#usr").change(function() {
  var id = $(this).children(":selected").attr("id");
  console.log(id);
})**/



});




'use strict';
var HttpClient = function() {
    this.get = function(aUrl, hmethod, data, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState === 4 && anHttpRequest.status === 200)
                aCallback(anHttpRequest.responseText);
        };
        anHttpRequest.open(hmethod, aUrl, true);
        anHttpRequest.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        anHttpRequest.send(data);
    }
};

/*var login={"username":userName };
login=JSON.stringify(login);
console.log("===================================>"+login);*/
var client = new HttpClient();
var userName = "";

function getUserName(id, callback) {
    const theurl = '/clari5-cc/rest/1.0/session/validate/' + id;
    const hmethod = 'GET';
    const data = null;

    client.get(theurl, hmethod, data, function(response) {
        userName = response;
        callback(response);
    });
}




