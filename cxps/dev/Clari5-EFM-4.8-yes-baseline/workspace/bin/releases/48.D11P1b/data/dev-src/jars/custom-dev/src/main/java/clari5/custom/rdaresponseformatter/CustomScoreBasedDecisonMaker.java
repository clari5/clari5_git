package clari5.custom.rdaresponseformatter;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rda.*;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.shared.IWSEvent;
import org.json.JSONObject;

import java.sql.*;
import java.util.*;

public class CustomScoreBasedDecisonMaker implements IResponseFormatter {
    private static final CxpsLogger logger = CxpsLogger.getLogger(CustomScoreBasedDecisonMaker.class);

    static private boolean bootstrap = false;

    static HashMap<String, String> riskBandMap = new HashMap<>();
    static HashMap<String, Double> data = new HashMap<>();
    static Map<String, List<String>> event_facts = new HashMap<>();
    // this map will contain the un weighted fact with the scenario score
    static Map<String,Double> unWeightedFact=new HashMap<>();

    static clari5.platform.util.Hocon riskBand;
    public CustomScoreBasedDecisonMaker() {
        isAlertRequired = Action.YES;
        isFrRequired = Action.YES;
    }
    public CustomScoreBasedDecisonMaker(FormatterType type) {
        this.type = type;
        isAlertRequired = Action.YES;
        isFrRequired = Action.YES;
    }

    static {
        riskBand = new Hocon();
        riskBand.loadFromContext("riskband.conf");
        // key is band range and the band
        riskBand.getStringList("band.rangeList").forEach(k -> riskBandMap.put(k, riskBand.getString("band." + k)));
        logger.debug("risk band map ---> " + riskBandMap);
        tabletomap();
        eventFactMap();
    }

    private FormatterType type;
    private String action;
    private int score;
    private String scoreRiskBand;
    private String scenarioName;
    private Action isFrRequired;
    private Action isAlertRequired;
    private String customRemarks;
    private String key;


    public String getScoreRiskBand() {
        return scoreRiskBand;
    }

    public void setScoreRiskBand(String scoreRiskBand) {
        this.scoreRiskBand = scoreRiskBand;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public FormatterType getType() {
        return type;
    }

    public void setType(FormatterType type) {
        this.type = type;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public Action getIsFrRequired() {
        return isFrRequired;
    }

    public void setIsFrRequired(Action isFrRequired) {
        this.isFrRequired = isFrRequired;
    }

    public Action getIsAlertRequired() {
        return isAlertRequired;
    }

    public void setIsAlertRequired(Action isAlertRequired) {
        this.isAlertRequired = isAlertRequired;
    }

    public void setCustomRemarks(String customRemarks) {
        this.customRemarks = customRemarks;
    }

    private static void tabletomap() {
        long start = System.currentTimeMillis();
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        try (CxConnection cxConnection = rdbms.get();
             PreparedStatement ps = cxConnection.prepareStatement(riskBand.getString("band.selectQuery"));
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                data.put(rs.getString("FACTNAME"), Double.valueOf(rs.getString("WEIGHT")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("data map: ---> " + data.size());
        System.out.println("CustomScoreBasedDecisonMaker-tabletomap --> " + (System.currentTimeMillis() - start));
        logger.debug("CustomScoreBasedDecisonMaker-tabletomap --> " + (System.currentTimeMillis() - start));
    }

    private static void eventFactMap() {
        long start = System.currentTimeMillis();
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        try (CxConnection cxConnection = rdbms.get();
             PreparedStatement ps = cxConnection.prepareStatement(riskBand.getString("band.selectEventQuery"));
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String[] fact = rs.getString("FACT_NAME").split("\\|");
                event_facts.put(rs.getString("EVENT_NAME"), Arrays.asList(fact));
            }
            System.out.println("event facts bootstrapped: "+event_facts);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("event_facts map: ---> " + event_facts.size());
        System.out.println("CustomScoreBasedDecisonMaker-eventFactMap --> " + (System.currentTimeMillis() - start));
        logger.debug("CustomScoreBasedDecisonMaker-eventFactMap --> " + (System.currentTimeMillis() - start));
    }

    @Override
    public IResponseFormatter populate(DecisionMaker.ResponseDecision responseDecision, FactSupressible factSupressible, IWSEvent iwsEvent, Set<WorkspaceInfo> set) {
        long start = System.currentTimeMillis();
        int weightedMaxScore=0;
        String weightedScoreMaxScoreFactName=null;
        int maxScore=0;
        String maxScoreFactName=null;
        CustomScoreBasedDecisonMaker csbdm = new CustomScoreBasedDecisonMaker(this.type);
        csbdm.action = responseDecision.orginalResponse;
        /**
         *     to get the highest score and fact name from weighted list and unweighted list
         */
        for(RDAActionInfo info : responseDecision.rdaActionInfoList) {
            if (weightedMaxScore < info.getScore() && iwsEvent.getSimpleName().equalsIgnoreCase("NFT_IbLoginEvent") && data.containsKey(info.getScenarioName())) {
                weightedMaxScore = info.getScore();
                weightedScoreMaxScoreFactName = info.getScenarioName();
            }
            else if(maxScore < info.getScore() && iwsEvent.getSimpleName().equalsIgnoreCase("NFT_IbLoginEvent") && !data.containsKey(info.getScenarioName())){
                maxScore = info.getScore();
                maxScoreFactName = info.getScenarioName();
            }
            else if(maxScore < info.getScore() && !iwsEvent.getSimpleName().equalsIgnoreCase("NFT_IbLoginEvent") && !data.containsKey(info.getScenarioName())){
                maxScore=info.getScore();
                maxScoreFactName=info.getScenarioName();
            }
        }
        logger.info("[CustomScoreBasedDecisonMaker] Weighted Data "+weightedScoreMaxScoreFactName +"\t"+weightedMaxScore+"\n"+"UnWeighted Data "+maxScoreFactName+"\t"+maxScore);
        if (responseDecision.finalActionObj != null) {
            csbdm.scenarioName = responseDecision.finalActionObj.getScenarioName();
            csbdm.key = iwsEvent.getEventId();
            String eventName = iwsEvent.getSimpleName();
            double num = 0;
            double denom = 0;

            try {
                List<String> scenarios = event_facts.get(eventName);
                if (scenarios != null && scenarios.size() > 0 && eventName.equalsIgnoreCase("NFT_IbLoginEvent")) {
                    for (RDAActionInfo info : responseDecision.rdaActionInfoList) {
                        try {
                            if (scenarios.contains(info.getScenarioName()) && eventName.equalsIgnoreCase("NFT_IbLoginEvent")) {
                                num += data.get(info.getScenarioName()) * info.getScore(); // scenario score
                                denom += data.get(info.getScenarioName());
                            } else if (!scenarios.contains(info.getScenarioName()) && eventName.equalsIgnoreCase("NFT_IbLoginEvent")) {
                                logger.info("[CustomScoreBasedDecisonMaker]Fact name does not match with RDA Scenario name");
                            }
                        } catch (Exception e) {
                            logger.debug("[CustomScoreBasedDecisonMaker]Please check table contains the scenario name with its weight: " + info.getScenarioName() + " "+e.getCause());
                        }
                    }
                } else if (scenarios.size() == 0 || scenarios == null || scenarios.isEmpty()) {
                    logger.info("[CustomScoreBasedDecisonMaker] Event is not mapped in event to fact name" + eventName);
                }
            }catch (Exception e){
                logger.debug("[CustomScoreBasedDecisonMaker] This event is not mapped in event to fact name table"+e.getCause());
            }
            /*else {
                for (RDAActionInfo info : responseDecision.rdaActionInfoList) {
                    try {
                            // data contains the scenario name and weighted score
                        if (data.containsKey(info.getScenarioName())) {
                            logger.debug("factname ------> " + info.getScenarioName() + " Score ------> " + info.getScore());
                            num += data.get(info.getScenarioName()) * info.getScore();
                            denom += data.get(info.getScenarioName());
                        } else {
                            logger.debug("Scenario " + info.getScenarioName() + "does not exist in table, hence taking the default weight as 1");
                            num += 1.0 * info.getScore();
                            denom += 1.0;
                        }

                        logger.debug("num: " + num + " \nscenarioname: " + info.getScenarioName() + " \nscore " + info.getScore() + " \nweight " + data.get(info.getScenarioName()));

                    } catch (Exception e) {
                        logger.debug("please check table contains the scenario name with its weight: " + info.getScenarioName());
                        e.printStackTrace();
                    }
                }
            }*/


            if (num != 0 && denom != 0 && eventName.equalsIgnoreCase("NFT_IbLoginEvent")) {
                logger.debug("[CustomScoreBasedDecisonMaker]num ---> " + num + " denom ---> "+denom);
                csbdm.setScore((int) Math.ceil(num / denom));
                /**
                 * Comparison is with the weighted score vs unweighted score
                 * maxScore is for unweighted score
                 *
                 * weighted-600,200,300
                 * unweighted-400,400,100
                 * avg weighted-250,400,190
                 */
                //if(weightedMaxScore > maxScore ){
                if(csbdm.getScore() > maxScore ){ // signifies the average score is highest of weighted avg category
                    // get the weighted average score
                    csbdm.setScore((int) Math.ceil(num / denom));
                    csbdm.setScoreRiskBand(calculateScoreRiskBand(csbdm.getScore()));
                    csbdm.setScenarioName(weightedScoreMaxScoreFactName);
                    logger.debug("[CustomScoreBasedDecisonMaker]Found UnWeighted score less than Weighted Average Score"+maxScore+ "< "+csbdm.getScore());
                }
                else if(csbdm.getScore()<maxScore){ // average score is less than the unweighted avg category
                    csbdm.setScore(maxScore);
                    // calculate the band
                    csbdm.setScoreRiskBand(calculateScoreRiskBand(maxScore));
                    csbdm.setScenarioName(maxScoreFactName);
                    logger.debug("[CustomScoreBasedDecisonMaker]Found UnWeighted score greater than Weighted Average Score"+maxScore+ " > "+csbdm.getScore());

                }
            }
            else if(num==0 && denom==0 && eventName.equalsIgnoreCase("NFT_IbLoginEvent")){
                csbdm.setScore(maxScore);
                csbdm.setScoreRiskBand(calculateScoreRiskBand(maxScore));
                csbdm.setScenarioName(maxScoreFactName);
                logger.debug("[CustomScoreBasedDecisonMaker] Found Num and Denom zero and event is NFT_IbLoginEvent");


            } else if(num==0 && denom==0 && !eventName.equalsIgnoreCase("NFT_IbLoginEvent")){ // other than ib login event
                csbdm.setScore(maxScore);
                csbdm.setScoreRiskBand(calculateScoreRiskBand(maxScore));
                csbdm.setScenarioName(maxScoreFactName);
                logger.debug("[CustomScoreBasedDecisonMaker] Found Num and Denom zero and event is not NFT_IbLoginEvent");
            }
            else if (denom == 0) {
                throw new ArithmeticException("[CustomScoreBasedDecisonMaker]weight is zero for " + this.getScenarioName() + "weight should be atleast 1");
            }
            else {
                logger.debug("[CustomScoreBasedDecisonMaker]Nothing was matched with anycondition: numerator --> " + num + " denominator ---> "+denom);
                //csbdm.setScore((int) (num/denom));
                csbdm.setScore(maxScore);
                csbdm.setScoreRiskBand(calculateScoreRiskBand(maxScore));
                csbdm.setScenarioName(maxScoreFactName);
                logger.debug("[CustomScoreBasedDecisonMaker] Assigning Max Score with Fact Name");
            }

        }
        logger.debug("[CustomScoreBasedDecisonMaker-populate] --> " + (System.currentTimeMillis() - start));
        return csbdm;
    }

    private String calculateScoreRiskBand(int score) {
        String riskBand=null;
        for (Map.Entry<String, String> entry : riskBandMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String band[] = key.split("-");
            if (score>= Integer.valueOf(band[0]) && score <= Integer.valueOf(band[1])) {
                riskBand=value;
            }
        }
        return riskBand;
    }


    @Override
    public String getFormattedResponse() {
        switch (type) {
            case DECISION:
                return this.action;

            case DECISIONWITHSCN:
                return this.action + "|" + this.scenarioName;

            case DECISIONASJSON:
                CxJson cxJson = new CxJson();
                cxJson.put("advice", this.action);
                return cxJson.toJson();

            case DECISIONWITHSCNASJSON:
                CxJson respJson = new CxJson();
                respJson.put("advice", this.action);
                respJson.put("scenario-name", this.scenarioName);
                return respJson.toJson();
            default:

                return this.action + "|" + this.scoreRiskBand + "|" + this.scenarioName + "|" + this.key;
        }
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public Action isFrRequired() {
        return isFrRequired;
    }

    @Override
    public String getCustomRemarks() {
        return customRemarks;
    }

    @Override
    public Action isAlertRequired() {
        return isAlertRequired;
    }

    @Override
    public JSONObject tojson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", type);
        jsonObject.put("action", action);
        jsonObject.put("score", score);
        jsonObject.put("scoreRiskBand", scoreRiskBand);
        jsonObject.put("scenarioName", scenarioName);
        jsonObject.put("isFrRequired", isFrRequired);
        jsonObject.put("isAlertRequired", isAlertRequired);
        jsonObject.put("customRemarks", customRemarks);
        return jsonObject;
    }

    @Override
    public IResponseFormatter fromJson(JSONObject jsonObject) {
        this.action = jsonObject.getString("action");
        this.isFrRequired = Action.valueOf(jsonObject.getString("isFrRequired"));
        this.isAlertRequired = Action.valueOf(jsonObject.getString("isAlertRequired"));
        this.score = jsonObject.getInt("score");
        this.scoreRiskBand = jsonObject.getString("scoreRiskBand");
        this.type = FormatterType.valueOf(jsonObject.getString("type"));
        this.scenarioName = jsonObject.getString("scenarioName");
        if (jsonObject.has("customRemarks")) {
            this.customRemarks = jsonObject.getString("customRemarks");
        }
        return this;
    }

    @Override
    public void setFormatterType(FormatterType formatterType) {
        this.type = formatterType;
    }
}
