cxps.events.event.nft-employee-branch{
  table-name : EVENT_NFT_EMPLOYEEBRANCH
  event-mnemonic: NE
  workspaces : {
   //USER: employee_id
  }
  event-attributes : {
	employee_id: {db : true ,raw_name : employee_ID ,type : "string:200", custom-getter:Employee_ID}
	notice_period: {db : true ,raw_name : notice_period ,type : "string:200", custom-getter:Notice_period}
	branch: {db : true ,raw_name : branch ,type : "string:200"}
	current_branch: {db : true ,raw_name : current_Branch ,type : "string:200", custom-getter:Current_Branch}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	branch_joining_date: {db : true ,raw_name : branch_Joining_Date ,type : timestamp, custom-getter:Branch_Joining_Date}
	desig_nation: {db : true ,raw_name : designation ,type : "string:200", custom-getter:Designation}
	}
}

