cxps.events.event.ft-wms{
  table-name : EVENT_FT_WMS
  event-mnemonic: WMS
  workspaces : {
    ACCOUNT: account-no
    CUSTOMER: cust_id
  }
  event-attributes : {
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        wms-account-number: {db : true ,raw_name : wms_account_number ,type: "string:200"}
        wms-account-risk-profile: {db : true ,raw_name : wms_account_risk_profile ,type: "string:200"}
        fund-house-code: {db : true ,raw_name : fund_house_code ,type: "string:200"}
        scheme-code: {db : true ,raw_name : scheme_code ,type: "string:200"}
        amount: {db : true ,raw_name : amount ,type: "number:20,3"}
        is-suitable-flg: {db : true ,raw_name : is_suitable_flg ,type: "string:10"}
        account-no: {db : true ,raw_name : account_no ,type: "string:200"}
        accountbalance: {db : true ,raw_name : accountbalance ,type: "number:20,3"}
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
        risk-band:{db : true ,raw_name :risk_band ,type : "string:200",derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}

}
}
