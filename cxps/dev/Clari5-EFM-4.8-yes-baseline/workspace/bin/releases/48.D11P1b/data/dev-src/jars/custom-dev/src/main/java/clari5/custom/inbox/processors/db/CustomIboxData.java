package clari5.custom.inbox.processors.db;

import clari5.custom.dedupe.DedupDaemon;
import clari5.custom.inbox.processors.tat.Cl5IbxRetry;
import clari5.custom.inbox.processors.tat.CustomCL5IbxItem;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CustomIboxData {
    private static final CxpsLogger logger = CxpsLogger.getLogger(DedupDaemon.class);

    public static Hocon hocon;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("inbox-tat.conf");
    }

    public synchronized static ConcurrentLinkedQueue<CustomCL5IbxItem> getCl5IbxItemData() {
        ConcurrentLinkedQueue<CustomCL5IbxItem> createdItems = new ConcurrentLinkedQueue<>();
        CustomCL5IbxItem cl5IbxItem = null;
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection();
                 PreparedStatement ps = connection.prepareStatement(hocon.getString("tat.query"))) {
                ps.setString(1, "created");
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        java.sql.Timestamp createdOn = null;
                        if (rs.getTimestamp("CREATED_ON") != null && checkTATBreached(rs.getTimestamp("CREATED_ON"))) {
                            createdOn = rs.getTimestamp("CREATED_ON");
                            cl5IbxItem = new CustomCL5IbxItem();
                            cl5IbxItem.setAppId(rs.getString("APP_ID") != null ? rs.getString("APP_ID") : "");
                            cl5IbxItem.setMessage(rs.getString("MSG") != null ? rs.getString("MSG") : "");
                            cl5IbxItem.setCreatedOn(createdOn);
                            cl5IbxItem.setUpdateOn(rs.getTimestamp("UPDATED_ON") != null ? rs.getTimestamp("UPDATED_ON") : Timestamp.valueOf(""));
                            cl5IbxItem.setCreatedBy(rs.getString("CREATED_BY") != null ? rs.getString("CREATED_BY") : "");
                            cl5IbxItem.setAssignedTo(rs.getString("ASSIGNED_TO") != null ? rs.getString("ASSIGNED_TO") : "");
                            cl5IbxItem.setUpdatedBy(rs.getString("UPDATED_BY") != null ? rs.getString("UPDATED_BY") : "");
                            cl5IbxItem.setState(rs.getString("STATE") != null ? rs.getString("STATE") : "");
                            cl5IbxItem.setIsRead(rs.getString("IS_READ") != null ? rs.getString("IS_READ") : "");
                            cl5IbxItem.setMessageFormat(rs.getString("MSG_FORMAT") != null ? rs.getString("MSG_FORMAT") : "");
                            cl5IbxItem.setParentId(rs.getString("PARENT_ID") != null ? rs.getString("PARENT_ID") : "");
                            cl5IbxItem.setNotificationId(rs.getString("NOTIFICATION_ID") != null ? rs.getString("NOTIFICATION_ID") : "");
                            cl5IbxItem.setItemType(rs.getString("ITEM_TYPE") != null ? rs.getString("ITEM_TYPE") : "");
                            cl5IbxItem.setTat(rs.getTimestamp("TAT") != null ? rs.getTimestamp("TAT") : Timestamp.valueOf(""));
                            cl5IbxItem.setItemId(rs.getString("ITEM_ID") != null ? rs.getString("ITEM_ID") : "");
                            cl5IbxItem.setHasAttachments(rs.getString("HAS_ATTACHMENTS") != null ? rs.getString("HAS_ATTACHMENTS") : "");
                            createdItems.add(cl5IbxItem);
                        } else {
                            logger.debug("CREATED_ON is either empty or not breated the TAT time. Hence ignored for ITEM_ID: " + rs.getString("ITEM_ID"));
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.debug("Check data for CL5_IBX_ITEM: " + cl5IbxItem);
            }
        }
        return createdItems;
    }

    public synchronized static void updateCl5IbxItemData(Cl5IbxRetry item) {
        //System.out.println("Retry daemon item: " + item);
        logger.debug("Retry daemon item: " + item);
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection();
                 PreparedStatement ps = connection.prepareStatement(hocon.getString("tat.updateQuery"))) {
                ps.setString(1, item.getEmailFlag());
                ps.setString(2, item.getRetryCount());
                ps.setString(3, item.getRetryID());
                ps.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.debug("Check data for CUSTOM_CL5_IBX_ITEM in retry: " + item);
            }
        }
    }

    public synchronized static void insertCl5IbxItemData(Cl5IbxRetry item) {
        //System.out.println("data inside insertCl5IbxItemData: " + item);
        logger.debug("data inside insertCl5IbxItemData: " + item);
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection();
                 PreparedStatement ps = connection.prepareStatement(hocon.getString("tat.insertQuery"))) {
                ps.setString(1, item.getRetryID());
                ps.setString(2, item.getItemId());
                ps.setString(3, item.getRetryCount());
                ps.setString(4, item.getEmailFlag());
                ps.setString(5, item.getEmailId());
                ps.setString(6, item.getEmailSubject());
                ps.setString(7, item.getEmailBody());
                ps.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.debug("Check data for CL5_IBX_ITEM: " + item);
                //System.out.println("Check data for CL5_IBX_ITEM: " + item);
            }
        }
    }

    //, , , , , ,
    public synchronized static ConcurrentLinkedQueue<Cl5IbxRetry> getCl5IbxRetryItemData() {
        ConcurrentLinkedQueue<Cl5IbxRetry> retryItems = new ConcurrentLinkedQueue<>();
        Cl5IbxRetry retry = null;
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection();
                 PreparedStatement ps = connection.prepareStatement(hocon.getString("tat.retryQuery"))) {
                ps.setString(1, "R");
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        retry = new Cl5IbxRetry();
                        retry.setRetryID(rs.getString("RETRY_ID") != null ? rs.getString("RETRY_ID") : "");
                        retry.setItemId(rs.getString("ITEM_ID") != null ? rs.getString("ITEM_ID") : "");
                        retry.setRetryCount(rs.getString("RETRY_COUNT") != null ? rs.getString("RETRY_COUNT") : "");
                        retry.setEmailFlag(rs.getString("EMAIL_FLAG") != null ? rs.getString("EMAIL_FLAG") : "");
                        retry.setEmailId(rs.getString("EMAIL_ID") != null ? rs.getString("EMAIL_ID") : "");
                        retry.setEmailSubject(rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "");
                        retry.setEmailBody(rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "");
                        retryItems.add(retry);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.debug("Check data for CUSTOM_CL5_IBX_ITEM: " + retry);
            }
        }
        return retryItems;
    }

    public static boolean checkItemIsPresent(CustomCL5IbxItem ibxItem) {
        boolean flag = false;
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection();
                 PreparedStatement ps = connection.prepareStatement(hocon.getString("tat.checkItemId"))) {
                ps.setString(1, ibxItem.getItemId());
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        if (rs.getInt("COUNT(*)") > 0) {
                            flag = true;
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.debug("Check data for CUSTOM_CL5_IBX_ITEM: " + ibxItem);
            }
        }
        return flag;
    }

    public static boolean checkTATBreached(java.sql.Timestamp createdOn) {
        long diff = System.currentTimeMillis() - createdOn.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000);
        logger.debug("TAT difference in minute: " + diffMinutes);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        if (diffMinutes > Integer.parseInt(hocon.getString("tat.breachTime"))) {
            return true;
        } else return false;
    }
}