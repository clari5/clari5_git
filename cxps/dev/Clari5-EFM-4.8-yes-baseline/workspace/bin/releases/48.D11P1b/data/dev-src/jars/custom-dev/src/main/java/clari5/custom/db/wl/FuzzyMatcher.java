    package clari5.custom.db.wl;

    import clari5.platform.applayer.Clari5;
    import clari5.platform.dbcon.CxConnection;
    import clari5.platform.logger.CxpsLogger;
    import clari5.platform.util.CxRest;
    import clari5.platform.rdbms.RDBMS;
    import clari5.platform.util.Hocon;
    import com.mashape.unirest.http.HttpResponse;
    import com.mashape.unirest.http.exceptions.UnirestException;
    import org.json.JSONArray;
    import org.json.JSONObject;

    import java.io.UnsupportedEncodingException;
    import java.net.URLEncoder;
    import java.sql.*;
    import java.util.*;


    public class FuzzyMatcher {

        /**
         * read the rules from the configuration file
         **/
        static String name = "";
        static String address = "";
        private static final CxpsLogger logger = CxpsLogger.getLogger(FuzzyMatcher.class);
        private static Map<String, String> records = new HashMap<>();

        static Hocon hocon;

        static {
            hocon = new Hocon();
            hocon.loadFromContext("fuzzy-rule.conf");
            name = hocon.getString("clari5.fuzzy-rule.NAME");
            address = hocon.getString("clari5.fuzzy-rule.ADDRESS");
        }

        /**
         * creating the request json
         */

        public void getMatchScore(String name_value, String address_value) {
            JSONObject obj = new JSONObject();
            JSONArray array = null;

            Hocon factHocon = hocon.get("clari5.fuzzy-rule");
            List<String> rulesList = factHocon.getStringList("rules");
            for (String rule : rulesList) {
                logger.info("iterating the rules" + rule);

                obj.put("ruleName", rule);
                JSONObject jo1 = new JSONObject();


                if (rule.equalsIgnoreCase("REPORT_WL_NAME_RULE")) {

                    array = new JSONArray();
                    jo1.put("name", name);
                    jo1.put("value", name_value);
                } else if (rule.equalsIgnoreCase("REPORT_WL_ADDRESS_RULE")) {

                    array = new JSONArray();
                    jo1.put("name", address);
                    jo1.put("value", address_value);

                }
                array.put(jo1);
                obj.put("fields", array);
                //System.out.println(obj.toString());

                //String url = "http://manoj.customerxps.com:5000" + "/efm/wlsearch?q=" + URLEncoder.encode(obj.toString());
                String url = null;
                try {
                    url = System.getenv("LOCAL_DN")+"/efm/wlsearch?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


                JSONObject jsonObject=null;
                String status="";
                jsonObject=new JSONObject(getHttpResponse(obj,url));
                //System.out.println("---response---");
                //System.out.println(getHttpResponse(obj,url));

                logger.info(getHttpResponse(obj,url));




                status=jsonObject.getJSONObject("status").get("status").toString();


                if(status.equalsIgnoreCase("success")) {
                   logger.info("status found success");
                    int records = 0;

                    try {
                        if(jsonObject.has("matchedResults")) {
                            records = (Integer) jsonObject.getJSONObject("matchedResults").get("total");
                            //System.out.println(records);

                            JSONArray jsonArray = null;
                            if (records != 0) {
                                logger.info("found records");
                                jsonArray = new JSONArray(jsonObject.getJSONObject("matchedResults").get("matchedRecords").toString());


                                for (int i = 0; i < records; i++) {
                                    JSONObject jobj = jsonArray.getJSONObject(i);
                                    //System.out.println(jobj);
                                    String id = jobj.getString("id");

                                    String finalId=id.substring(5);
                                    //System.out.println(finalId);
                                    logger.info("id matched:"+finalId);

                                    String listName = jobj.getString("listName");
                                    //System.out.println(listName);
                                    logger.info("list name is:"+listName);


                                    if (listName != null && !"".equals(listName)) {
                                        double match_score = jobj.getDouble("score") * 100;

                                        //System.out.println(jobj.getDouble("score"));
                                        //System.out.println(jobj.getJSONArray("fields").getJSONObject(0));


                                        JSONArray arr = jobj.getJSONArray("fields");
                                        for(Object object : arr ){
                                            JSONObject jobj1 = (JSONObject) object;
                                            String attribute=jobj1.getString("name");
                                            //System.out.println(jobj1.getString("name"));


                                            if(attribute.equalsIgnoreCase("name")){

                                                String sql = "update ft_core_archive_report set name_score=? where tran_id=?";
                                                System.out.println(finalId);
                                                updateWatchListScore(match_score,sql,finalId);

                                            }
                                            else if(attribute.equalsIgnoreCase("ben_addr")){

                                                String sql = "update ft_core_archive_report set address_score=? where tran_id=?";
                                                System.out.println(finalId);
                                                updateWatchListScore(match_score,sql,finalId);

                                            }

                                        }





                                        //System.out.println("parsing json"+attribute);








                                        /*System.out.println("score is"+match_score);
                                        updateWatchListScore(match_score,finalId);*/

                                    }
                                }
                            }


                        }
                        else
                        {
                            //System.out.println("no matched results");
                            logger.info("no matched results");
                        }
                    }catch (NullPointerException ne){
                        //System.out.println("error in json object");
                        logger.error("error in json object");

                    }


                }

            }


        }


        public void getDataFromTable() {
            Connection con = getConnection();
            int count = 0;
            try {
                Statement st = con.createStatement();
                //String sql="select tran_id,cust_id from ft_core_archive_report";
                String sql = "select cust_id,tran_id,ben_name,ben_address from ft_core_archive_report";

                ResultSet rs = st.executeQuery(sql);
                // int count=0;
                while (rs.next()) {

                    String cust_id = rs.getString(1);
                    //String tran_id = rs.getString(2);
                    String ben_name = rs.getString(3);
                    String ben_address = rs.getString(4);

                    if (records.containsKey(cust_id)) {


                        records.put(cust_id, records.get(cust_id) + "," + ben_name + ":" + ben_address);
                        //String tran_id = rs.getString(2);


                    } else {

                        records.put(cust_id, ben_name + ":" + ben_address);

                    }
                }
                for (Map.Entry<String, String> entry : records.entrySet()) {

                   // System.out.println(entry.getKey()+ " "+entry.getValue());
                    String[] list = entry.getValue().split(",");

                    int numberOfBeneficiary=list.length;

                    if(numberOfBeneficiary>=5){
                        System.out.println(list.length);
                        System.out.println(entry.getKey()+ " "+entry.getValue());

                        for (String i : list) {

                            String[] actualValue = i.split(":");
                            //count++;
                            getMatchScore(actualValue[0], actualValue[1]);

                        }

                    }
                        /*
                        for (String i : list) {

                            String[] actualValue = i.split(":");
                            //count++;
                            getMatchScore(actualValue[0], actualValue[1]);

                        }*/



                }

            } catch (SQLException e) {
                e.printStackTrace();
            }


        }

        /**
         * @param obj
         * @param url
         * @return response as string
         */
        public String getHttpResponse(JSONObject obj, String url) {

            HttpResponse resp = null;
            String response = "";

            String instanceId = System.getenv("INSTANCEID");
            String appSecret = System.getenv("APPSECRET");

            try {
                //System.out.println("WL_ONBOARDING_CUST: url=" + url);
                resp = CxRest.get(url).header("accept", "application/json")
                        .header("Content-Type", "application/json")
                        .header("mode", "PROG").queryString("msg", obj.toString())
                        .basicAuth(instanceId, appSecret).asString();


                response = (String) resp.getBody();
                //System.out.println(resp);
                //System.out.println(response);


            } catch (UnirestException e) {
                e.printStackTrace();
                System.out.println("Watchlist is not reachable");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("WL_ONBOARDING_CUST : Exception in parsing");


            }

            return response;


        }


        /*
         * fetch the connection
         */
        public static CxConnection getConnection() {
            RDBMS rdbms = (RDBMS) Clari5.rdbms();
            if (rdbms == null)
                throw new RuntimeException("RDBMS as a resource is unavailable");
            return rdbms.getConnection();
        }

       public static void main(String[] r) {
           Clari5.batchBootstrap("test", "efm-clari5.conf");
            System.out.println("------------------------");
            FuzzyMatcher fuzzyMatcher = new FuzzyMatcher();
            fuzzyMatcher.getDataFromTable();
            System.out.println("------------------------");
            //fuzzyMatcher.getMatchScorer();
        }


        public boolean updateWatchListScore(double matchscore,String sql,String id) {
            System.out.println("Wait Updating records");
            Connection connection = getConnection();
            PreparedStatement ps = null;

            try {
                //String sql = "update ft_core_archive_report set name_score=?,address_score=? where tran_id=?";
                ps = connection.prepareStatement(sql);
                ps.setDouble(1,matchscore);
                System.out.println(matchscore+"is score");
                //ps.setDouble(2,50);
                ps.setString(2,id);
                System.out.println(id+"is id");

                ps.executeUpdate();
                connection.commit();
                System.out.println("records updated in the database");
                //connection.close();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                if(connection!=null){
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }
            }
            return  false;
        }

    }


