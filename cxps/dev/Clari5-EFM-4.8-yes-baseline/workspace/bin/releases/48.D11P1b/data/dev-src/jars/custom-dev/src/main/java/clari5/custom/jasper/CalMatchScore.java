package clari5.custom.jasper;



import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

import java.util.*;

public class CalMatchScore {
    private static final CxpsLogger logger = CxpsLogger.getLogger(CalMatchScore.class);

    static Hocon hocon;
    final List<String> validRules = new ArrayList<>();
    final static List<String> matchType = new ArrayList<>();
    private String custType;


    static {
        hocon = new Hocon();
        hocon.loadFromContext("wl-rules-name.conf");
    }



    public List<String> getMatchType(Map<String, Double> maxScore) {

        matchType.clear();

        Hocon hoc = hocon.get("wl-reports-rules");
        validateScore(maxScore);

        if (StringUtils.isNullOrEmpty(custType)) {
            logger.warn("custType must be individual or non-individual in NEW_ACCT_DETAILS table");
            return matchType;
        }
        List<String> mtList = hoc.get("cust-type").get(custType).getStringList("match-type-list");

        for (String key : mtList) {
            List<String> rules = hoc.get("matchType").getStringList(key);

            boolean flag = true;
            for (String ruleName : rules) {
                if (!compareTo(ruleName)) {
                    flag = false;
                    break;
                }
            }
            if (flag) matchType.add(key);
        }
        setMatchedRules(hoc);

        validRules.clear();
        return matchType;
    }


    private  void setMatchedRules(Hocon hocon){
        List<String>tmp = new ArrayList<>();
        tmp.addAll(matchType);

        for (String key : tmp) {
            int idx = matchType.indexOf(key);
            List<String> rules = hocon.get("matchType").getStringList(key);
            StringBuilder newRules = new StringBuilder();
            for (String rule: rules) {
                newRules.append(rule.split("_")[0]).append("_");
            }

            matchType.remove(idx);

            matchType.add(idx,newRules.substring(0, newRules.length()-1));
        }
        tmp.clear();
    }

    private void validateScore(Map<String, Double> maxScore) {
        Hocon hoc = hocon.get("wl-reports-rules").get("maxScore");
        for (Map.Entry<String, Double> map : maxScore.entrySet()) {
            if (Double.parseDouble(hoc.getString(map.getKey())) <= map.getValue()) {
                validRules.add(map.getKey());
            }
        }
    }


    private boolean compareTo(String rulesName) {
        return validRules.contains(rulesName);
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }


}
