package clari5.custom.db.wl;

import clari5.custom.db.DBConnection;
import clari5.custom.db.JDBCConnection;

import clari5.tools.util.Hocon;
import cxps.apex.utils.CxpsLogger;

import java.sql.*;

public class WlListGen extends DBConnection {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlListGen.class);

    TableTemplates tableTemplates;
    Hocon hocon;

    public WlListGen(Hocon hocon) {
        jdbcConnection = new JDBCConnection();
        this.hocon = hocon;
    }

    public TableTemplates createTblTemplate(String tableName) {
        tableTemplates = new TableTemplates();
        tableTemplates.setTableName(tableName);


        try {
            Connection con = getJDBCConnection();
            String wlId = hocon.get("csv-file").get("id").getString(tableName);

            String query = "select * from " + tableName;
            PreparedStatement psmt = con.prepareStatement(query);


            ResultSet resultSet = psmt.executeQuery();

            logger.info("Connection [" + con + "] query [" + query + "] \n Result set " + resultSet.toString());
            if (resultSet == null) {
                logger.info("resultset null");
                return tableTemplates;
            }

            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            logger.info("ResultSetMetaData " + resultSetMetaData);
            for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                String columnName = resultSetMetaData.getColumnName(i);
                if (columnName.equalsIgnoreCase(wlId)) tableTemplates.getHeader().add("id");
                else tableTemplates.getHeader().add(columnName);
            }
            tableTemplates.getHeader().add("LIST_CODE");

            setTableData(resultSet, resultSetMetaData);
            return tableTemplates;
        } catch (SQLException sql) {
            logger.error("Failed to get the header of " + tableName + "Cause " + sql.getCause());

        } finally {
            closeJDBCConnection();
        }

        return tableTemplates;
    }


    public void setTableData(ResultSet resultSet, ResultSetMetaData resMetaData) throws SQLException {
        String listName = hocon.get("csv-file").get("listName").getString(tableTemplates.getTableName());
        while (resultSet.next()) {
            for (int i = 1; i <= resMetaData.getColumnCount(); i++) {
                Object data = resultSet.getObject(i);


                if (data != null) {
                    tableTemplates.getValues().add(data.toString());
                } else tableTemplates.getValues().add("");
            }
            tableTemplates.getValues().add(listName);

        }
    }

}
