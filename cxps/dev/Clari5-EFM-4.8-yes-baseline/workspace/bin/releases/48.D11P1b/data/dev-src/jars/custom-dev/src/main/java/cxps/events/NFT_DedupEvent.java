// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name = "EVENT_NFT_DEDUP", Schema = "rice")
public class NFT_DedupEvent extends Event implements IArchivable {

    @Field(size = 100, key = true)
    public String eventId;
    @Field
    public Timestamp eventDate;
    @Field
    public Boolean isPostTransaction;
    @Field(size = 200)
    public String landlineNumber;
    @Field(size = 7)
    public Integer accountCount;
    @Field(size = 11)
    public Double nameMatchScore;
    @Field(size = 11)
    public Double brpermanantAddrScore;
    @Field(size = 7)
    public Integer mobNonStaffMatchedCount;
    @Field(size = 11)
    public Double permanantAddrScore;
    @Field(size = 200)
    public String handPhone;
    @Field(size = 7)
    public Integer panMatchedCount;
    @Field(size = 200)
    public String completePerAddrChanged;
    @Field(size = 200)
    public String nameMatchedlist;
    @Field(size = 7)
    public Integer emailMatchedCount;
    @Field(size = 200)
    public String passportNumber;
    @Field(size = 11)
    public Double scorePercentage = 100.00;
    @Field(size = 200)
    public String nameChanged;
    @Field(size = 200)
    public String dobCustId;
    @Field(size = 11)
    public Double score;
    @Field(size = 200)
    public String permanentAddr1;
    @Field(size = 200)
    public String permanentAddr3;
    @Field(size = 200)
    public String permanentAddr2;
    @Field(size = 7)
    public Integer intermediateLandlineCount;
    @Field(size = 200)
    public String mobNonstaffCustId;
    @Field(size = 200)
    public String hostId;
    @Field(size = 7)
    public Integer passportMatchedCount;
    @Field(size = 200)
    public String pan;
    @Field(size = 200)
    public String completeMailingAddr;
    @Field(size = 8000)
    public String permanentaddrMatchedlist;
    @Field(size = 200)
    public String landlineCustId;
    @Field(size = 200)
    public String accountId;
    @Field(size = 200)
    public String custName;
    @Field(size = 200)
    public String completePermanentAddr;
    @Field(size = 7)
    public Integer mobMatchedCount;
    @Field(size = 7)
    public Integer dobMatchedCount;
    @Field
    public java.sql.Timestamp sysTime;
    @Field(size = 200)
    public String completeMailingAddrChanged;
    @Field(size = 7)
    public Integer brcompleteMailingAddrCount;
    @Field(size = 200)
    public String emailId;
    @Field(size = 200)
    public String mobCustId;
    @Field(size = 200)
    public String officeLandlineNumber;
    @Field(size = 11)
    public Double brmailingAddrScore;
    @Field
    public java.sql.Timestamp dob;
    @Field(size = 200)
    public String matchPhoneCustId;
    @Field(size = 7)
    public Integer landlineMatchedCount;
    @Field(size = 200)
    public String productCode;
    @Field(size = 200)
    public String mobStaffCustId;
    @Field(size = 200)
    public String panCustId;
    @Field
    public java.sql.Timestamp doi;
    @Field(size = 200)
    public String entityType;
    @Field(size = 200)
    public String mobNumber;
    @Field(size = 200)
    public String passportCustId;
    @Field(size = 200)
    public String mailingAddr3;
    @Field(size = 7)
    public Integer officephoneMatchedCount;
    @Field(size = 200)
    public String doiCustId;
    @Field(size = 11)
    public Double mailingAddrScore;
    @Field(size = 200)
    public String homeLandlineNumber;
    @Field(size = 7)
    public Integer completePerAddrCount;
    @Field(size = 7)
    public Integer intermediatehandphoneCount;
    @Field(size = 7)
    public Integer intermediateofficelandCount;
    @Field(size = 7)
    public Integer mobCount;
    @Field(size = 8000)
    public String branchaddrMatchedlist;
    @Field(size = 7)
    public Integer completeMailingAddrCount;
    @Field(size = 7)
    public Integer phoneCount;
    @Field(size = 10)
    public String isAccountClosed;
    @Field(size = 200)
    public String custId;
    @Field(size = 7)
    public Integer doiMatchedCount;
    @Field(size = 200)
    public String emailCustId;
    @Field(size = 200)
    public String officePhoneCustId;
    @Field(size = 7)
    public Integer brcompletePerAddrCount;
    @Field(size = 8000)
    public String mailingaddrMatchedlist;
    @Field(size = 200)
    public String mailingAddr2;
    @Field(size = 200)
    public String mailingAddr1;
    @Field(size = 200)
    public String ucicId;


    @JsonIgnore
    public ITable<NFT_DedupEvent> t = AEF.getITable(this);

    public NFT_DedupEvent() {
    }

    public NFT_DedupEvent(CxConnection con, Date from, Date to) {
        this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
     * This method is used to populate the event object using the data
     * coming in message body of event json from external system.
     */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Dedup");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "") + json.getString("event_id"));

        if (getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

        setLandlineNumber(json.getString("landlineNumber"));
        setAccountCount(EventHelper.toInt(json.getString("accountCount")));
        setNameMatchScore(EventHelper.toDouble(json.getString("nameMatchScore")));
        setBrpermanantAddrScore(EventHelper.toDouble(json.getString("brpermanantAddrScore")));
        setMobNonStaffMatchedCount(EventHelper.toInt(json.getString("mobNonStaffMatchedCount")));
        setPermanantAddrScore(EventHelper.toDouble(json.getString("permanantAddrScore")));
        setHandPhone(json.getString("handPhone"));
        setPanMatchedCount(EventHelper.toInt(json.getString("panMatchedCount")));
        setCompletePerAddrChanged(json.getString("completePerAddrChanged"));
        setNameMatchedlist(json.getString("nameMatchedList"));
        setEmailMatchedCount(EventHelper.toInt(json.getString("emailMatchedCount")));
        setPassportNumber(json.getString("passportNumber"));
        setScorePercentage(EventHelper.toDouble(json.getString("scorePercentage")));
        setNameChanged(json.getString("nameChanged"));
        setDobCustId(json.getString("dob_custId"));
        setScore(EventHelper.toDouble(json.getString("score")));
        setPermanentAddr1(json.getString("permanentAddr1"));
        setPermanentAddr3(json.getString("permanentAddr3"));
        setPermanentAddr2(json.getString("permanentAddr2"));
        setIntermediateLandlineCount(EventHelper.toInt(json.getString("intermediateLandlineCount")));
        setMobNonstaffCustId(json.getString("mob_nonstaff_custId"));
        setHostId(json.getString("host_id"));
        setPassportMatchedCount(EventHelper.toInt(json.getString("passportMatchedCount")));
        setPan(json.getString("pan"));
        setCompleteMailingAddr(json.getString("completeMailingAddr"));
        setPermanentaddrMatchedlist(json.getString("permanentaddrMatchedList"));
        setLandlineCustId(json.getString("landline_custId"));
        setAccountId(json.getString("accountId"));
        setCustName(json.getString("custName"));
        setCompletePermanentAddr(json.getString("completePermanentAddr"));
        setMobMatchedCount(EventHelper.toInt(json.getString("mobMatchedCount")));
        setDobMatchedCount(EventHelper.toInt(json.getString("dobMatchedCount")));
        setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
        setCompleteMailingAddrChanged(json.getString("completeMailingAddrChanged"));
        setBrcompleteMailingAddrCount(EventHelper.toInt(json.getString("brcompleteMailingAddrCount")));
        setEmailId(json.getString("emailId"));
        setMobCustId(json.getString("mob_custId"));
        setOfficeLandlineNumber(json.getString("officeLandlineNumber"));
        setBrmailingAddrScore(EventHelper.toDouble(json.getString("brmailingAddrScore")));
        setDob(EventHelper.toTimestamp(json.getString("dob")));
        setMatchPhoneCustId(json.getString("match_phone_custId"));
        setLandlineMatchedCount(EventHelper.toInt(json.getString("landlineMatchedCount")));
        setProductCode(json.getString("product_code"));
        setMobStaffCustId(json.getString("mob_staff_custId"));
        setPanCustId(json.getString("pan_custId"));
        setDoi(EventHelper.toTimestamp(json.getString("doi")));
        setEntityType(json.getString("entity_type"));
        setMobNumber(json.getString("mobNumber"));
        setPassportCustId(json.getString("passport_custId"));
        setMailingAddr3(json.getString("mailingAddr3"));
        setOfficephoneMatchedCount(EventHelper.toInt(json.getString("officephoneMatchedCount")));
        setDoiCustId(json.getString("doi_custId"));
        setMailingAddrScore(EventHelper.toDouble(json.getString("mailingAddrScore")));
        setHomeLandlineNumber(json.getString("homeLandlineNumber"));
        setCompletePerAddrCount(EventHelper.toInt(json.getString("completePerAddrCount")));
        setIntermediatehandphoneCount(EventHelper.toInt(json.getString("intermediatehandphoneCount")));
        setIntermediateofficelandCount(EventHelper.toInt(json.getString("intermediateofficelandCount")));
        setMobCount(EventHelper.toInt(json.getString("mobCount")));
        setBranchaddrMatchedlist(json.getString("branchaddrMatchedList"));
        setCompleteMailingAddrCount(EventHelper.toInt(json.getString("completeMailingAddrCount")));
        setPhoneCount(EventHelper.toInt(json.getString("phoneCount")));
        setCustId(json.getString("custId"));
        setDoiMatchedCount(EventHelper.toInt(json.getString("doiMatchedCount")));
        setEmailCustId(json.getString("email_custId"));
        setOfficePhoneCustId(json.getString("office_phone_custId"));
        setBrcompletePerAddrCount(EventHelper.toInt(json.getString("brcompletePerAddrCount")));
        setMailingaddrMatchedlist(json.getString("mailingMatchedList"));
        setMailingAddr2(json.getString("mailingAddr2"));
        setMailingAddr1(json.getString("mailingAddr1"));
        setUcicId(json.getString("ucicId"));

        setDerivedValues();
        DedupWlScore dedupWlScore = new DedupWlScore();
        Object event = dedupWlScore.getWlScore(this);
        if (event instanceof NFT_DedupEvent) {
            fromMap((NFT_DedupEvent) event);
        }

    }

    private void fromMap(NFT_DedupEvent nft_dedupEvent) {
        this.setEventId(nft_dedupEvent.getEventId());
        this.setEventDate(nft_dedupEvent.getEventDate());
        this.setLandlineNumber(nft_dedupEvent.getLandlineNumber());
        this.setAccountCount(nft_dedupEvent.getAccountCount());
        this.setNameMatchScore(nft_dedupEvent.getNameMatchScore());
        this.setMobNonStaffMatchedCount(nft_dedupEvent.getMobNonStaffMatchedCount());
        this.setPermanantAddrScore(nft_dedupEvent.getPermanantAddrScore());
        this.setHandPhone(nft_dedupEvent.getHandPhone());
        this.setPanMatchedCount(nft_dedupEvent.getPanMatchedCount());
        this.setCompletePerAddrChanged(nft_dedupEvent.getCompletePerAddrChanged());
        this.setEmailMatchedCount(nft_dedupEvent.getEmailMatchedCount());
        this.setPassportNumber(nft_dedupEvent.getPassportNumber());
        this.setScorePercentage(nft_dedupEvent.getScorePercentage());
        this.setNameChanged(nft_dedupEvent.getNameChanged());
        this.setBrmailingAddrScore(nft_dedupEvent.getBrmailingAddrScore());// mailing score
        this.setScore(nft_dedupEvent.getScore());
        this.setBrcompleteMailingAddrCount(nft_dedupEvent.getBrcompleteMailingAddrCount()); // completemailing count
        this.setPermanentAddr1(nft_dedupEvent.getPermanentAddr1());
        this.setPermanentAddr3(nft_dedupEvent.getPermanentAddr3());
        this.setPermanentAddr2(nft_dedupEvent.getPermanentAddr2());
        this.setIntermediateLandlineCount(nft_dedupEvent.getIntermediateLandlineCount());
        this.setPassportMatchedCount(nft_dedupEvent.getPassportMatchedCount());
        this.setPan(nft_dedupEvent.getPan());
        this.setCompleteMailingAddr(nft_dedupEvent.getCompleteMailingAddr());
        this.setBrcompletePerAddrCount(nft_dedupEvent.getBrcompletePerAddrCount()); // complete permananet count
        this.setAccountId(nft_dedupEvent.getAccountId());
        this.setCustName(nft_dedupEvent.getCustName());
        this.setCompletePermanentAddr(nft_dedupEvent.getCompletePermanentAddr());
        this.setMobMatchedCount(nft_dedupEvent.getMobMatchedCount());
        this.setDobMatchedCount(nft_dedupEvent.getDobMatchedCount());
        this.setCompleteMailingAddrChanged(nft_dedupEvent.getCompleteMailingAddrChanged());
        this.setEmailId(nft_dedupEvent.getEmailId());
        this.setOfficeLandlineNumber(nft_dedupEvent.getOfficeLandlineNumber());
        this.setDob(nft_dedupEvent.getDob());
        this.setLandlineMatchedCount(nft_dedupEvent.getLandlineMatchedCount());
//        this.setProductCode(nft_dedupEvent.getProductCode());
        this.setDoi(nft_dedupEvent.getDoi());
        this.setBrpermanantAddrScore(nft_dedupEvent.getBrpermanantAddrScore()); // br permanent add score
        this.setMobNumber(nft_dedupEvent.getMobNumber());
        this.setMailingAddr3(nft_dedupEvent.getMailingAddr3());
        this.setOfficephoneMatchedCount(nft_dedupEvent.getOfficephoneMatchedCount());
        this.setMailingAddrScore(nft_dedupEvent.getMailingAddrScore());
        this.setHomeLandlineNumber(nft_dedupEvent.getHomeLandlineNumber());
        this.setCompletePerAddrCount(nft_dedupEvent.getCompletePerAddrCount());
        this.setIntermediatehandphoneCount(nft_dedupEvent.getIntermediatehandphoneCount());
        this.setIntermediateofficelandCount(nft_dedupEvent.getIntermediateofficelandCount());
        this.setMobCount(nft_dedupEvent.getMobCount());
        this.setCompleteMailingAddrCount(nft_dedupEvent.getCompleteMailingAddrCount());
        this.setPhoneCount(nft_dedupEvent.getPhoneCount());
        this.setDoiMatchedCount(nft_dedupEvent.getDobMatchedCount());
        this.setMailingAddr2(nft_dedupEvent.getMailingAddr2());
        this.setMailingAddr1(nft_dedupEvent.getMailingAddr1());
        this.setBranchaddrMatchedlist(nft_dedupEvent.getBranchaddrMatchedlist());
        this.setPermanentaddrMatchedlist(nft_dedupEvent.getPermanentaddrMatchedlist());
        this.setNameMatchedlist(nft_dedupEvent.getNameMatchedlist());
        this.setMailingaddrMatchedlist(nft_dedupEvent.getMailingaddrMatchedlist());

        this.setMobNonstaffCustId(nft_dedupEvent.getMobNonstaffCustId());
        this.setDobCustId(nft_dedupEvent.getDobCustId());
        this.setMobCustId(nft_dedupEvent.getMobCustId());
        this.setMatchPhoneCustId(nft_dedupEvent.getMatchPhoneCustId());
        this.setMobStaffCustId(nft_dedupEvent.getMobStaffCustId());
        this.setPanCustId(nft_dedupEvent.getPanCustId());
        this.setDoiCustId(nft_dedupEvent.getDoiCustId());
        this.setEmailCustId(nft_dedupEvent.getEmailCustId());
        this.setPassportCustId(nft_dedupEvent.getPassportCustId());
        this.setLandlineCustId(nft_dedupEvent.getLandlineCustId());
        this.setOfficePhoneCustId(nft_dedupEvent.getOfficePhoneCustId());


        //this.setUcicId(String ucicId)
    }

    private void setDerivedValues() {
        setIsAccountClosed(cxps.events.CustomFieldDerivator.getIsAccountClosed(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() {
        return "ND";
    }

    public String getEventId() {
        return this.eventId;
    }

    public Timestamp getEventDate() {
        return this.eventDate;
    }

    public String getLandlineNumber() {
        return landlineNumber;
    }

    public Integer getAccountCount() {
        return accountCount;
    }

    public Double getNameMatchScore() {
        return nameMatchScore;
    }

    public Double getBrpermanantAddrScore() {
        return brpermanantAddrScore;
    }

    public Integer getMobNonStaffMatchedCount() {
        return mobNonStaffMatchedCount;
    }

    public Double getPermanantAddrScore() {
        return permanantAddrScore;
    }

    public String getHandPhone() {
        return handPhone;
    }

    public Integer getPanMatchedCount() {
        return panMatchedCount;
    }

    public String getCompletePerAddrChanged() {
        return completePerAddrChanged;
    }

    public String getNameMatchedlist() {
        return nameMatchedlist;
    }

    public Integer getEmailMatchedCount() {
        return emailMatchedCount;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public Double getScorePercentage() {
        return scorePercentage;
    }

    public String getNameChanged() {
        return nameChanged;
    }

    public String getDobCustId() {
        return dobCustId;
    }

    public Double getScore() {
        return score;
    }

    public String getPermanentAddr1() {
        return permanentAddr1;
    }

    public String getPermanentAddr3() {
        return permanentAddr3;
    }

    public String getPermanentAddr2() {
        return permanentAddr2;
    }

    public Integer getIntermediateLandlineCount() {
        return intermediateLandlineCount;
    }

    public String getMobNonstaffCustId() {
        return mobNonstaffCustId;
    }

    public String getHostId() {
        return hostId;
    }

    public Integer getPassportMatchedCount() {
        return passportMatchedCount;
    }

    public String getPan() {
        return pan;
    }

    public String getCompleteMailingAddr() {
        return completeMailingAddr;
    }

    public String getPermanentaddrMatchedlist() {
        return permanentaddrMatchedlist;
    }

    public String getLandlineCustId() {
        return landlineCustId;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getCustName() {
        return custName;
    }

    public String getCompletePermanentAddr() {
        return completePermanentAddr;
    }

    public Integer getMobMatchedCount() {
        return mobMatchedCount;
    }

    public Integer getDobMatchedCount() {
        return dobMatchedCount;
    }

    public java.sql.Timestamp getSysTime() {
        return sysTime;
    }

    public String getCompleteMailingAddrChanged() {
        return completeMailingAddrChanged;
    }

    public Integer getBrcompleteMailingAddrCount() {
        return brcompleteMailingAddrCount;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getMobCustId() {
        return mobCustId;
    }

    public String getOfficeLandlineNumber() {
        return officeLandlineNumber;
    }

    public Double getBrmailingAddrScore() {
        return brmailingAddrScore;
    }

    public java.sql.Timestamp getDob() {
        return dob;
    }

    public String getMatchPhoneCustId() {
        return matchPhoneCustId;
    }

    public Integer getLandlineMatchedCount() {
        return landlineMatchedCount;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getMobStaffCustId() {
        return mobStaffCustId;
    }

    public String getPanCustId() {
        return panCustId;
    }

    public java.sql.Timestamp getDoi() {
        return doi;
    }

    public String getEntityType() {
        return entityType;
    }

    public String getMobNumber() {
        return mobNumber;
    }

    public String getPassportCustId() {
        return passportCustId;
    }

    public String getMailingAddr3() {
        return mailingAddr3;
    }

    public Integer getOfficephoneMatchedCount() {
        return officephoneMatchedCount;
    }

    public String getDoiCustId() {
        return doiCustId;
    }

    public Double getMailingAddrScore() {
        return mailingAddrScore;
    }

    public String getHomeLandlineNumber() {
        return homeLandlineNumber;
    }

    public Integer getCompletePerAddrCount() {
        return completePerAddrCount;
    }

    public Integer getIntermediatehandphoneCount() {
        return intermediatehandphoneCount;
    }

    public Integer getIntermediateofficelandCount() {
        return intermediateofficelandCount;
    }

    public Integer getMobCount() {
        return mobCount;
    }

    public String getBranchaddrMatchedlist() {
        return branchaddrMatchedlist;
    }

    public Integer getCompleteMailingAddrCount() {
        return completeMailingAddrCount;
    }

    public Integer getPhoneCount() {
        return phoneCount;
    }

    public String getCustId() {
        return custId;
    }

    public Integer getDoiMatchedCount() {
        return doiMatchedCount;
    }

    public String getEmailCustId() {
        return emailCustId;
    }

    public String getOfficePhoneCustId() {
        return officePhoneCustId;
    }

    public Integer getBrcompletePerAddrCount() {
        return brcompletePerAddrCount;
    }

    public String getMailingaddrMatchedlist() {
        return mailingaddrMatchedlist;
    }

    public String getMailingAddr2() {
        return mailingAddr2;
    }

    public String getMailingAddr1() {
        return mailingAddr1;
    }

    public String getUcicId() {
        return ucicId;
    }

    public String getIsAccountClosed() {
        return isAccountClosed;
    }

    /* Setters */
    public void setEventId(String val) {
        this.eventId = val;
    }

    public void setEventDate(Timestamp val) {
        this.eventDate = val;
    }

    public void setLandlineNumber(String val) {
        this.landlineNumber = val;
    }

    public void setAccountCount(Integer val) {
        this.accountCount = val;
    }

    public void setNameMatchScore(Double val) {
        this.nameMatchScore = val;
    }

    public void setBrpermanantAddrScore(Double val) {
        this.brpermanantAddrScore = val;
    }

    public void setMobNonStaffMatchedCount(Integer val) {
        this.mobNonStaffMatchedCount = val;
    }

    public void setPermanantAddrScore(Double val) {
        this.permanantAddrScore = val;
    }

    public void setHandPhone(String val) {
        this.handPhone = val;
    }

    public void setPanMatchedCount(Integer val) {
        this.panMatchedCount = val;
    }

    public void setCompletePerAddrChanged(String val) {
        this.completePerAddrChanged = val;
    }

    public void setNameMatchedlist(String val) {
        this.nameMatchedlist = val;
    }

    public void setEmailMatchedCount(Integer val) {
        this.emailMatchedCount = val;
    }

    public void setPassportNumber(String val) {
        this.passportNumber = val;
    }

    public void setScorePercentage(Double val) {
        this.scorePercentage = val;
    }

    public void setNameChanged(String val) {
        this.nameChanged = val;
    }

    public void setDobCustId(String val) {
        this.dobCustId = val;
    }

    public void setScore(Double val) {
        this.score = val;
    }

    public void setPermanentAddr1(String val) {
        this.permanentAddr1 = val;
    }

    public void setPermanentAddr3(String val) {
        this.permanentAddr3 = val;
    }

    public void setPermanentAddr2(String val) {
        this.permanentAddr2 = val;
    }

    public void setIntermediateLandlineCount(Integer val) {
        this.intermediateLandlineCount = val;
    }

    public void setMobNonstaffCustId(String val) {
        this.mobNonstaffCustId = val;
    }

    public void setHostId(String val) {
        this.hostId = val;
    }

    public void setPassportMatchedCount(Integer val) {
        this.passportMatchedCount = val;
    }

    public void setPan(String val) {
        this.pan = val;
    }

    public void setCompleteMailingAddr(String val) {
        this.completeMailingAddr = val;
    }

    public void setPermanentaddrMatchedlist(String val) {
        this.permanentaddrMatchedlist = val;
    }

    public void setLandlineCustId(String val) {
        this.landlineCustId = val;
    }

    public void setAccountId(String val) {
        this.accountId = val;
    }

    public void setCustName(String val) {
        this.custName = val;
    }

    public void setCompletePermanentAddr(String val) {
        this.completePermanentAddr = val;
    }

    public void setMobMatchedCount(Integer val) {
        this.mobMatchedCount = val;
    }

    public void setDobMatchedCount(Integer val) {
        this.dobMatchedCount = val;
    }

    public void setSysTime(java.sql.Timestamp val) {
        this.sysTime = val;
    }

    public void setCompleteMailingAddrChanged(String val) {
        this.completeMailingAddrChanged = val;
    }

    public void setBrcompleteMailingAddrCount(Integer val) {
        this.brcompleteMailingAddrCount = val;
    }

    public void setEmailId(String val) {
        this.emailId = val;
    }

    public void setMobCustId(String val) {
        this.mobCustId = val;
    }

    public void setOfficeLandlineNumber(String val) {
        this.officeLandlineNumber = val;
    }

    public void setBrmailingAddrScore(Double val) {
        this.brmailingAddrScore = val;
    }

    public void setDob(java.sql.Timestamp val) {
        this.dob = val;
    }

    public void setMatchPhoneCustId(String val) {
        this.matchPhoneCustId = val;
    }

    public void setLandlineMatchedCount(Integer val) {
        this.landlineMatchedCount = val;
    }

    public void setProductCode(String val) {
        this.productCode = val;
    }

    public void setMobStaffCustId(String val) {
        this.mobStaffCustId = val;
    }

    public void setPanCustId(String val) {
        this.panCustId = val;
    }

    public void setDoi(java.sql.Timestamp val) {
        this.doi = val;
    }

    public void setEntityType(String val) {
        this.entityType = val;
    }

    public void setMobNumber(String val) {
        this.mobNumber = val;
    }

    public void setPassportCustId(String val) {
        this.passportCustId = val;
    }

    public void setMailingAddr3(String val) {
        this.mailingAddr3 = val;
    }

    public void setOfficephoneMatchedCount(Integer val) {
        this.officephoneMatchedCount = val;
    }

    public void setDoiCustId(String val) {
        this.doiCustId = val;
    }

    public void setMailingAddrScore(Double val) {
        this.mailingAddrScore = val;
    }

    public void setHomeLandlineNumber(String val) {
        this.homeLandlineNumber = val;
    }

    public void setCompletePerAddrCount(Integer val) {
        this.completePerAddrCount = val;
    }

    public void setIntermediatehandphoneCount(Integer val) {
        this.intermediatehandphoneCount = val;
    }

    public void setIntermediateofficelandCount(Integer val) {
        this.intermediateofficelandCount = val;
    }

    public void setMobCount(Integer val) {
        this.mobCount = val;
    }

    public void setBranchaddrMatchedlist(String val) {
        this.branchaddrMatchedlist = val;
    }

    public void setCompleteMailingAddrCount(Integer val) {
        this.completeMailingAddrCount = val;
    }

    public void setPhoneCount(Integer val) {
        this.phoneCount = val;
    }

    public void setCustId(String val) {
        this.custId = val;
    }

    public void setDoiMatchedCount(Integer val) {
        this.doiMatchedCount = val;
    }

    public void setEmailCustId(String val) {
        this.emailCustId = val;
    }

    public void setOfficePhoneCustId(String val) {
        this.officePhoneCustId = val;
    }

    public void setBrcompletePerAddrCount(Integer val) {
        this.brcompletePerAddrCount = val;
    }

    public void setMailingaddrMatchedlist(String val) {
        this.mailingaddrMatchedlist = val;
    }

    public void setMailingAddr2(String val) {
        this.mailingAddr2 = val;
    }

    public void setMailingAddr1(String val) {
        this.mailingAddr1 = val;
    }

    public void setUcicId(String val) {
        this.ucicId = val;
    }

    public void setIsAccountClosed(String val) {
        this.isAccountClosed = val;
    }

    /* Custom Getters*/
    @JsonIgnore
    public java.sql.Timestamp getSys_time() {
        return sysTime;
    }

    @JsonIgnore
    public String getProduct_code() {
        return productCode;
    }


    /**
     * This method is used to return a set of WorkspaceInfo which contains the
     * information about the workspaces and it's corresponding entity which can be
     * derived for an event.
     */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_DedupEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
     * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
     */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
     * This method is used to return a json to be used in Fraud Resolution.
     * The fields present in this json are the fields which were marked with 'fr:true' in
     * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
     */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Dedup");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Dedup");
        if (getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }

    public NFT_DedupEvent setDedupData() throws InvalidDataException {
        NFT_DedupEvent nft_dedupEvent = new NFT_DedupEvent();
        try {
            nft_dedupEvent.setCustName(this.getCustName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        nft_dedupEvent.setEntityType(this.getEntityType());
        nft_dedupEvent.setCompletePermanentAddr(this.getCompletePermanentAddr());
        nft_dedupEvent.setCompleteMailingAddr(this.getCompleteMailingAddr());
        nft_dedupEvent.setCompleteMailingAddrChanged(this.getCompleteMailingAddrChanged());
        nft_dedupEvent.setCompletePerAddrChanged(this.getCompletePerAddrChanged());
        nft_dedupEvent.setNameChanged(this.getNameChanged());
        return nft_dedupEvent;
    }

    @Override
    public String toString() {
        return "NFT_DedupEvent{" +
                "eventId='" + eventId + '\'' +
                ", eventDate=" + eventDate +
                ", isPostTransaction=" + isPostTransaction +
                ", landlineNumber='" + landlineNumber + '\'' +
                ", accountCount=" + accountCount +
                ", nameMatchScore=" + nameMatchScore +
                ", mobNonStaffMatchedCount=" + mobNonStaffMatchedCount +
                ", permanantAddrScore=" + permanantAddrScore +
                ", handPhone='" + handPhone + '\'' +
                ", panMatchedCount=" + panMatchedCount +
                ", completePerAddrChanged='" + completePerAddrChanged + '\'' +
                ", emailMatchedCount=" + emailMatchedCount +
                ", passportNumber='" + passportNumber + '\'' +
                ", scorePercentage=" + scorePercentage +
                ", nameChanged='" + nameChanged + '\'' +
                ", score=" + score +
                ", permanentAddr1='" + permanentAddr1 + '\'' +
                ", permanentAddr3='" + permanentAddr3 + '\'' +
                ", permanentAddr2='" + permanentAddr2 + '\'' +
                ", intermediateLandlineCount=" + intermediateLandlineCount +
                ", hostId='" + hostId + '\'' +
                ", passportMatchedCount=" + passportMatchedCount +
                ", pan='" + pan + '\'' +
                ", completeMailingAddr='" + completeMailingAddr + '\'' +
                ", accountId='" + accountId + '\'' +
                ", custName='" + custName + '\'' +
                ", completePermanentAddr='" + completePermanentAddr + '\'' +
                ", mobMatchedCount=" + mobMatchedCount +
                ", dobMatchedCount=" + dobMatchedCount +
                ", sysTime=" + sysTime +
                ", completeMailingAddrChanged='" + completeMailingAddrChanged + '\'' +
                ", emailId='" + emailId + '\'' +
                ", officeLandlineNumber='" + officeLandlineNumber + '\'' +
                ", dob=" + dob +
                ", landlineMatchedCount=" + landlineMatchedCount +
                ", productCode='" + productCode + '\'' +
                ", doi=" + doi +
                ", entityType='" + entityType + '\'' +
                ", mobNumber='" + mobNumber + '\'' +
                ", mailingAddr3='" + mailingAddr3 + '\'' +
                ", officephoneMatchedCount=" + officephoneMatchedCount +
                ", mailingAddrScore=" + mailingAddrScore +
                ", homeLandlineNumber='" + homeLandlineNumber + '\'' +
                ", completePerAddrCount=" + completePerAddrCount +
                ", intermediatehandphoneCount=" + intermediatehandphoneCount +
                ", intermediateofficelandCount=" + intermediateofficelandCount +
                ", mobCount=" + mobCount +
                ", completeMailingAddrCount=" + completeMailingAddrCount +
                ", phoneCount=" + phoneCount +
                ", custId='" + custId + '\'' +
                ", doiMatchedCount=" + doiMatchedCount +
                ", mailingAddr2='" + mailingAddr2 + '\'' +
                ", mailingAddr1='" + mailingAddr1 + '\'' +
                ", ucicId='" + ucicId + '\'' +
                ", t=" + t +
                '}';
    }

    /**
     * This method is supposed to return a proper user understandable
     * message to be shown on the UI for end user.
     * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
     * his account on 10th Feb, 2017 at 01:00 pm
     */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if (message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}