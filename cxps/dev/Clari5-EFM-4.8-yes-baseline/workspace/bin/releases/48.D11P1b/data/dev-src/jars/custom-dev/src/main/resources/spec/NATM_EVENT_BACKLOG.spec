clari5.custom.mapper {
        entity {
                NATM_EVENT_BACKLOG {
                       generate = true
                        attributes:[
                                { name: ACCT_ID, type="string:50" },
                                { name: FLAG ,type ="string:10" }
                                { name: CUST_ID ,type ="string:30" }
				{ name: TRIGGERING_EVENT_ID ,type ="string:100" , key=true }
				{ name: CUM_TXN_AMT ,type ="number:30,2" }
				{ name: EVENT_ID ,type ="CLOB" }
                                ]
                                indexes {
         			      FLAG : [ FLAG ]
        }
                        }
        }
}

