// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_CoreAcctTxnEventMapper extends EventMapper<FT_CoreAcctTxnEvent> {

public FT_CoreAcctTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_CoreAcctTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_CoreAcctTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_CoreAcctTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_CoreAcctTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_CoreAcctTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_CoreAcctTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getUserType());
            preparedStatement.setDouble(i++, obj.getTodGrantAmount());
            preparedStatement.setTimestamp(i++, obj.getValueDate());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getBenCity());
            preparedStatement.setString(i++, obj.getNonHomeBranch());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setDouble(i++, obj.getTranAmount());
            preparedStatement.setString(i++, obj.getBranch());
            preparedStatement.setString(i++, obj.getBenCntryCode());
            preparedStatement.setString(i++, obj.getRefTranCrncy());
            preparedStatement.setString(i++, obj.getRelation());
            preparedStatement.setString(i++, obj.getPurposeDesc());
            preparedStatement.setString(i++, obj.getRemName());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getBenBic());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getCustType());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getProductDesc());
            preparedStatement.setString(i++, obj.getTransactionMode());
            preparedStatement.setDouble(i++, obj.getCumTxnAmt());
            preparedStatement.setString(i++, obj.getMtMessage());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setTimestamp(i++, obj.getTdMaturityDate());
            preparedStatement.setString(i++, obj.getBenAcctNo());
            preparedStatement.setString(i++, obj.getBenCustId());
            preparedStatement.setLong(i++, obj.getCodeMsgType());
            preparedStatement.setDouble(i++, obj.getThresholdAmt());
            preparedStatement.setTimestamp(i++, obj.getPstdDate());
            preparedStatement.setString(i++, obj.getInstStatus());
            preparedStatement.setString(i++, obj.getSystem());
            preparedStatement.setString(i++, obj.getAtmId());
            preparedStatement.setString(i++, obj.getProductCode());
            preparedStatement.setString(i++, obj.getDrCr());
            preparedStatement.setString(i++, obj.getClientAccNo());
            preparedStatement.setString(i++, obj.getRemBic());
            preparedStatement.setString(i++, obj.getAccountOpenDateFlag());
            preparedStatement.setString(i++, obj.getProductTypeFlag());
            preparedStatement.setString(i++, obj.getPstdFlg());
            preparedStatement.setString(i++, obj.getStatus());
            preparedStatement.setString(i++, obj.getHostType());
            preparedStatement.setString(i++, obj.getRemAdd1());
            preparedStatement.setString(i++, obj.getTxnKey());
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setString(i++, obj.getRemType());
            preparedStatement.setString(i++, obj.getCommodity());
            preparedStatement.setString(i++, obj.getWhiteListFlag());
            preparedStatement.setString(i++, obj.getFccProductCode());
            preparedStatement.setString(i++, obj.getRemAdd3());
            preparedStatement.setString(i++, obj.getInrAmount());
            preparedStatement.setString(i++, obj.getRemAdd2());
            preparedStatement.setTimestamp(i++, obj.getTrnDate());
            preparedStatement.setString(i++, obj.getHmDate());
            preparedStatement.setString(i++, obj.getPurposeCode());
            preparedStatement.setString(i++, obj.getChannelId());
            preparedStatement.setString(i++, obj.getSubTranMode());
            preparedStatement.setString(i++, obj.getBenAdd3());
            preparedStatement.setString(i++, obj.getCaSchemeCode());
            preparedStatement.setString(i++, obj.getBenAdd2());
            preparedStatement.setString(i++, obj.getRemAcctNo());
            preparedStatement.setString(i++, obj.getBenAdd1());
            preparedStatement.setString(i++, obj.getTrannumonicCode());
            preparedStatement.setString(i++, obj.getRemCustId());
            preparedStatement.setString(i++, obj.getInstrumentNumber());
            preparedStatement.setString(i++, obj.getAutheriserUserId());
            preparedStatement.setString(i++, obj.getBenName());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getIsinNumber());
            preparedStatement.setString(i++, obj.getTranParticular());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getReferenceSrlNum());
            preparedStatement.setString(i++, obj.getBankCode());
            preparedStatement.setDouble(i++, obj.getUsdEqvAmt());
            preparedStatement.setString(i++, obj.getTdLiquidationType());
            preparedStatement.setString(i++, obj.getRemCntryCode());
            preparedStatement.setString(i++, obj.getCptyAcNo());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getTranCurr());
            preparedStatement.setDouble(i++, obj.getEffAvailableBalance());
            preparedStatement.setString(i++, obj.getInstrumentType());
            preparedStatement.setDouble(i++, obj.getRefTranAmt());
            preparedStatement.setString(i++, obj.getRemCity());
            preparedStatement.setString(i++, obj.getUcicId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_COREACCT]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_CoreAcctTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_CoreAcctTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_COREACCT"));
        putList = new ArrayList<>();

        for (FT_CoreAcctTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "USER_TYPE",  obj.getUserType());
            p = this.insert(p, "TOD_GRANT_AMOUNT", String.valueOf(obj.getTodGrantAmount()));
            p = this.insert(p, "VALUE_DATE", String.valueOf(obj.getValueDate()));
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "BEN_CITY",  obj.getBenCity());
            p = this.insert(p, "NON_HOME_BRANCH",  obj.getNonHomeBranch());
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TRAN_AMOUNT", String.valueOf(obj.getTranAmount()));
            p = this.insert(p, "BRANCH",  obj.getBranch());
            p = this.insert(p, "BEN_CNTRY_CODE",  obj.getBenCntryCode());
            p = this.insert(p, "REF_TRAN_CRNCY",  obj.getRefTranCrncy());
            p = this.insert(p, "RELATION",  obj.getRelation());
            p = this.insert(p, "PURPOSE_DESC",  obj.getPurposeDesc());
            p = this.insert(p, "REM_NAME",  obj.getRemName());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "BEN_BIC",  obj.getBenBic());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CUST_TYPE",  obj.getCustType());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "PRODUCT_DESC",  obj.getProductDesc());
            p = this.insert(p, "TRANSACTION_MODE",  obj.getTransactionMode());
            p = this.insert(p, "CUM_TXN_AMT", String.valueOf(obj.getCumTxnAmt()));
            p = this.insert(p, "MT_MESSAGE",  obj.getMtMessage());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TD_MATURITY_DATE", String.valueOf(obj.getTdMaturityDate()));
            p = this.insert(p, "BEN_ACCT_NO",  obj.getBenAcctNo());
            p = this.insert(p, "BEN_CUST_ID",  obj.getBenCustId());
            p = this.insert(p, "CODE_MSG_TYPE", String.valueOf(obj.getCodeMsgType()));
            p = this.insert(p, "THRESHOLD_AMT", String.valueOf(obj.getThresholdAmt()));
            p = this.insert(p, "PSTD_DATE", String.valueOf(obj.getPstdDate()));
            p = this.insert(p, "INST_STATUS",  obj.getInstStatus());
            p = this.insert(p, "SYSTEM",  obj.getSystem());
            p = this.insert(p, "ATM_ID",  obj.getAtmId());
            p = this.insert(p, "PRODUCT_CODE",  obj.getProductCode());
            p = this.insert(p, "DR_CR",  obj.getDrCr());
            p = this.insert(p, "CLIENT_ACC_NO",  obj.getClientAccNo());
            p = this.insert(p, "REM_BIC",  obj.getRemBic());
            p = this.insert(p, "ACCOUNTOPENDATEFLAG",  obj.getAccountOpenDateFlag());
            p = this.insert(p, "PRODUCTTYPEFLAG",  obj.getProductTypeFlag());
            p = this.insert(p, "PSTD_FLG",  obj.getPstdFlg());
            p = this.insert(p, "STATUS",  obj.getStatus());
            p = this.insert(p, "HOST_TYPE",  obj.getHostType());
            p = this.insert(p, "REM_ADD1",  obj.getRemAdd1());
            p = this.insert(p, "TXN_KEY",  obj.getTxnKey());
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "REM_TYPE",  obj.getRemType());
            p = this.insert(p, "COMMODITY",  obj.getCommodity());
            p = this.insert(p, "WHITELISTFLAG",  obj.getWhiteListFlag());
            p = this.insert(p, "FCC_PRODUCT_CODE",  obj.getFccProductCode());
            p = this.insert(p, "REM_ADD3",  obj.getRemAdd3());
            p = this.insert(p, "INR_AMOUNT",  obj.getInrAmount());
            p = this.insert(p, "REM_ADD2",  obj.getRemAdd2());
            p = this.insert(p, "TRN_DATE", String.valueOf(obj.getTrnDate()));
            p = this.insert(p, "HM_DATE",  obj.getHmDate());
            p = this.insert(p, "PURPOSE_CODE",  obj.getPurposeCode());
            p = this.insert(p, "CHANNEL_ID",  obj.getChannelId());
            p = this.insert(p, "SUB_TRAN_MODE",  obj.getSubTranMode());
            p = this.insert(p, "BEN_ADD3",  obj.getBenAdd3());
            p = this.insert(p, "CA_SCHEME_CODE",  obj.getCaSchemeCode());
            p = this.insert(p, "BEN_ADD2",  obj.getBenAdd2());
            p = this.insert(p, "REM_ACCT_NO",  obj.getRemAcctNo());
            p = this.insert(p, "BEN_ADD1",  obj.getBenAdd1());
            p = this.insert(p, "TRANNUMONIC_CODE",  obj.getTrannumonicCode());
            p = this.insert(p, "REM_CUST_ID",  obj.getRemCustId());
            p = this.insert(p, "INSTRUMENT_NUMBER",  obj.getInstrumentNumber());
            p = this.insert(p, "AUTHERISER_USER_ID",  obj.getAutheriserUserId());
            p = this.insert(p, "BEN_NAME",  obj.getBenName());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "ISIN_NUMBER",  obj.getIsinNumber());
            p = this.insert(p, "TRAN_PARTICULAR",  obj.getTranParticular());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "REFERENCE_SRL_NUM",  obj.getReferenceSrlNum());
            p = this.insert(p, "BANK_CODE",  obj.getBankCode());
            p = this.insert(p, "USD_EQV_AMT", String.valueOf(obj.getUsdEqvAmt()));
            p = this.insert(p, "TD_LIQUIDATION_TYPE",  obj.getTdLiquidationType());
            p = this.insert(p, "REM_CNTRY_CODE",  obj.getRemCntryCode());
            p = this.insert(p, "CPTY_AC_NO",  obj.getCptyAcNo());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "TRAN_CURR",  obj.getTranCurr());
            p = this.insert(p, "EFF_AVAILABLE_BALANCE", String.valueOf(obj.getEffAvailableBalance()));
            p = this.insert(p, "INSTRUMENT_TYPE",  obj.getInstrumentType());
            p = this.insert(p, "REF_TRAN_AMT", String.valueOf(obj.getRefTranAmt()));
            p = this.insert(p, "REM_CITY",  obj.getRemCity());
            p = this.insert(p, "UCIC_ID",  obj.getUcicId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_COREACCT"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_COREACCT]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_COREACCT]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_CoreAcctTxnEvent obj = new FT_CoreAcctTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setUserType(rs.getString("USER_TYPE"));
    obj.setTodGrantAmount(rs.getDouble("TOD_GRANT_AMOUNT"));
    obj.setValueDate(rs.getTimestamp("VALUE_DATE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setBenCity(rs.getString("BEN_CITY"));
    obj.setNonHomeBranch(rs.getString("NON_HOME_BRANCH"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTranAmount(rs.getDouble("TRAN_AMOUNT"));
    obj.setBranch(rs.getString("BRANCH"));
    obj.setBenCntryCode(rs.getString("BEN_CNTRY_CODE"));
    obj.setRefTranCrncy(rs.getString("REF_TRAN_CRNCY"));
    obj.setRelation(rs.getString("RELATION"));
    obj.setPurposeDesc(rs.getString("PURPOSE_DESC"));
    obj.setRemName(rs.getString("REM_NAME"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setBenBic(rs.getString("BEN_BIC"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCustType(rs.getString("CUST_TYPE"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setProductDesc(rs.getString("PRODUCT_DESC"));
    obj.setTransactionMode(rs.getString("TRANSACTION_MODE"));
    obj.setCumTxnAmt(rs.getDouble("CUM_TXN_AMT"));
    obj.setMtMessage(rs.getString("MT_MESSAGE"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTdMaturityDate(rs.getTimestamp("TD_MATURITY_DATE"));
    obj.setBenAcctNo(rs.getString("BEN_ACCT_NO"));
    obj.setBenCustId(rs.getString("BEN_CUST_ID"));
    obj.setCodeMsgType(rs.getLong("CODE_MSG_TYPE"));
    obj.setThresholdAmt(rs.getDouble("THRESHOLD_AMT"));
    obj.setPstdDate(rs.getTimestamp("PSTD_DATE"));
    obj.setInstStatus(rs.getString("INST_STATUS"));
    obj.setSystem(rs.getString("SYSTEM"));
    obj.setAtmId(rs.getString("ATM_ID"));
    obj.setProductCode(rs.getString("PRODUCT_CODE"));
    obj.setDrCr(rs.getString("DR_CR"));
    obj.setClientAccNo(rs.getString("CLIENT_ACC_NO"));
    obj.setRemBic(rs.getString("REM_BIC"));
    obj.setAccountOpenDateFlag(rs.getString("ACCOUNTOPENDATEFLAG"));
    obj.setProductTypeFlag(rs.getString("PRODUCTTYPEFLAG"));
    obj.setPstdFlg(rs.getString("PSTD_FLG"));
    obj.setStatus(rs.getString("STATUS"));
    obj.setHostType(rs.getString("HOST_TYPE"));
    obj.setRemAdd1(rs.getString("REM_ADD1"));
    obj.setTxnKey(rs.getString("TXN_KEY"));
    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setRemType(rs.getString("REM_TYPE"));
    obj.setCommodity(rs.getString("COMMODITY"));
    obj.setWhiteListFlag(rs.getString("WHITELISTFLAG"));
    obj.setFccProductCode(rs.getString("FCC_PRODUCT_CODE"));
    obj.setRemAdd3(rs.getString("REM_ADD3"));
    obj.setInrAmount(rs.getString("INR_AMOUNT"));
    obj.setRemAdd2(rs.getString("REM_ADD2"));
    obj.setTrnDate(rs.getTimestamp("TRN_DATE"));
    obj.setHmDate(rs.getString("HM_DATE"));
    obj.setPurposeCode(rs.getString("PURPOSE_CODE"));
    obj.setChannelId(rs.getString("CHANNEL_ID"));
    obj.setSubTranMode(rs.getString("SUB_TRAN_MODE"));
    obj.setBenAdd3(rs.getString("BEN_ADD3"));
    obj.setCaSchemeCode(rs.getString("CA_SCHEME_CODE"));
    obj.setBenAdd2(rs.getString("BEN_ADD2"));
    obj.setRemAcctNo(rs.getString("REM_ACCT_NO"));
    obj.setBenAdd1(rs.getString("BEN_ADD1"));
    obj.setTrannumonicCode(rs.getString("TRANNUMONIC_CODE"));
    obj.setRemCustId(rs.getString("REM_CUST_ID"));
    obj.setInstrumentNumber(rs.getString("INSTRUMENT_NUMBER"));
    obj.setAutheriserUserId(rs.getString("AUTHERISER_USER_ID"));
    obj.setBenName(rs.getString("BEN_NAME"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setIsinNumber(rs.getString("ISIN_NUMBER"));
    obj.setTranParticular(rs.getString("TRAN_PARTICULAR"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setReferenceSrlNum(rs.getString("REFERENCE_SRL_NUM"));
    obj.setBankCode(rs.getString("BANK_CODE"));
    obj.setUsdEqvAmt(rs.getDouble("USD_EQV_AMT"));
    obj.setTdLiquidationType(rs.getString("TD_LIQUIDATION_TYPE"));
    obj.setRemCntryCode(rs.getString("REM_CNTRY_CODE"));
    obj.setCptyAcNo(rs.getString("CPTY_AC_NO"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setTranCurr(rs.getString("TRAN_CURR"));
    obj.setEffAvailableBalance(rs.getDouble("EFF_AVAILABLE_BALANCE"));
    obj.setInstrumentType(rs.getString("INSTRUMENT_TYPE"));
    obj.setRefTranAmt(rs.getDouble("REF_TRAN_AMT"));
    obj.setRemCity(rs.getString("REM_CITY"));
    obj.setUcicId(rs.getString("UCIC_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_COREACCT]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_CoreAcctTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_CoreAcctTxnEvent> events;
 FT_CoreAcctTxnEvent obj = new FT_CoreAcctTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_COREACCT"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_CoreAcctTxnEvent obj = new FT_CoreAcctTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setUserType(getColumnValue(rs, "USER_TYPE"));
            obj.setTodGrantAmount(EventHelper.toDouble(getColumnValue(rs, "TOD_GRANT_AMOUNT")));
            obj.setValueDate(EventHelper.toTimestamp(getColumnValue(rs, "VALUE_DATE")));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setBenCity(getColumnValue(rs, "BEN_CITY"));
            obj.setNonHomeBranch(getColumnValue(rs, "NON_HOME_BRANCH"));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTranAmount(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMOUNT")));
            obj.setBranch(getColumnValue(rs, "BRANCH"));
            obj.setBenCntryCode(getColumnValue(rs, "BEN_CNTRY_CODE"));
            obj.setRefTranCrncy(getColumnValue(rs, "REF_TRAN_CRNCY"));
            obj.setRelation(getColumnValue(rs, "RELATION"));
            obj.setPurposeDesc(getColumnValue(rs, "PURPOSE_DESC"));
            obj.setRemName(getColumnValue(rs, "REM_NAME"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setBenBic(getColumnValue(rs, "BEN_BIC"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCustType(getColumnValue(rs, "CUST_TYPE"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setProductDesc(getColumnValue(rs, "PRODUCT_DESC"));
            obj.setTransactionMode(getColumnValue(rs, "TRANSACTION_MODE"));
            obj.setCumTxnAmt(EventHelper.toDouble(getColumnValue(rs, "CUM_TXN_AMT")));
            obj.setMtMessage(getColumnValue(rs, "MT_MESSAGE"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTdMaturityDate(EventHelper.toTimestamp(getColumnValue(rs, "TD_MATURITY_DATE")));
            obj.setBenAcctNo(getColumnValue(rs, "BEN_ACCT_NO"));
            obj.setBenCustId(getColumnValue(rs, "BEN_CUST_ID"));
            obj.setCodeMsgType(EventHelper.toLong(getColumnValue(rs, "CODE_MSG_TYPE")));
            obj.setThresholdAmt(EventHelper.toDouble(getColumnValue(rs, "THRESHOLD_AMT")));
            obj.setPstdDate(EventHelper.toTimestamp(getColumnValue(rs, "PSTD_DATE")));
            obj.setInstStatus(getColumnValue(rs, "INST_STATUS"));
            obj.setSystem(getColumnValue(rs, "SYSTEM"));
            obj.setAtmId(getColumnValue(rs, "ATM_ID"));
            obj.setProductCode(getColumnValue(rs, "PRODUCT_CODE"));
            obj.setDrCr(getColumnValue(rs, "DR_CR"));
            obj.setClientAccNo(getColumnValue(rs, "CLIENT_ACC_NO"));
            obj.setRemBic(getColumnValue(rs, "REM_BIC"));
            obj.setAccountOpenDateFlag(getColumnValue(rs, "ACCOUNTOPENDATEFLAG"));
            obj.setProductTypeFlag(getColumnValue(rs, "PRODUCTTYPEFLAG"));
            obj.setPstdFlg(getColumnValue(rs, "PSTD_FLG"));
            obj.setStatus(getColumnValue(rs, "STATUS"));
            obj.setHostType(getColumnValue(rs, "HOST_TYPE"));
            obj.setRemAdd1(getColumnValue(rs, "REM_ADD1"));
            obj.setTxnKey(getColumnValue(rs, "TXN_KEY"));
            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setRemType(getColumnValue(rs, "REM_TYPE"));
            obj.setCommodity(getColumnValue(rs, "COMMODITY"));
            obj.setWhiteListFlag(getColumnValue(rs, "WHITELISTFLAG"));
            obj.setFccProductCode(getColumnValue(rs, "FCC_PRODUCT_CODE"));
            obj.setRemAdd3(getColumnValue(rs, "REM_ADD3"));
            obj.setInrAmount(getColumnValue(rs, "INR_AMOUNT"));
            obj.setRemAdd2(getColumnValue(rs, "REM_ADD2"));
            obj.setTrnDate(EventHelper.toTimestamp(getColumnValue(rs, "TRN_DATE")));
            obj.setHmDate(getColumnValue(rs, "HM_DATE"));
            obj.setPurposeCode(getColumnValue(rs, "PURPOSE_CODE"));
            obj.setChannelId(getColumnValue(rs, "CHANNEL_ID"));
            obj.setSubTranMode(getColumnValue(rs, "SUB_TRAN_MODE"));
            obj.setBenAdd3(getColumnValue(rs, "BEN_ADD3"));
            obj.setCaSchemeCode(getColumnValue(rs, "CA_SCHEME_CODE"));
            obj.setBenAdd2(getColumnValue(rs, "BEN_ADD2"));
            obj.setRemAcctNo(getColumnValue(rs, "REM_ACCT_NO"));
            obj.setBenAdd1(getColumnValue(rs, "BEN_ADD1"));
            obj.setTrannumonicCode(getColumnValue(rs, "TRANNUMONIC_CODE"));
            obj.setRemCustId(getColumnValue(rs, "REM_CUST_ID"));
            obj.setInstrumentNumber(getColumnValue(rs, "INSTRUMENT_NUMBER"));
            obj.setAutheriserUserId(getColumnValue(rs, "AUTHERISER_USER_ID"));
            obj.setBenName(getColumnValue(rs, "BEN_NAME"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setIsinNumber(getColumnValue(rs, "ISIN_NUMBER"));
            obj.setTranParticular(getColumnValue(rs, "TRAN_PARTICULAR"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setReferenceSrlNum(getColumnValue(rs, "REFERENCE_SRL_NUM"));
            obj.setBankCode(getColumnValue(rs, "BANK_CODE"));
            obj.setUsdEqvAmt(EventHelper.toDouble(getColumnValue(rs, "USD_EQV_AMT")));
            obj.setTdLiquidationType(getColumnValue(rs, "TD_LIQUIDATION_TYPE"));
            obj.setRemCntryCode(getColumnValue(rs, "REM_CNTRY_CODE"));
            obj.setCptyAcNo(getColumnValue(rs, "CPTY_AC_NO"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setTranCurr(getColumnValue(rs, "TRAN_CURR"));
            obj.setEffAvailableBalance(EventHelper.toDouble(getColumnValue(rs, "EFF_AVAILABLE_BALANCE")));
            obj.setInstrumentType(getColumnValue(rs, "INSTRUMENT_TYPE"));
            obj.setRefTranAmt(EventHelper.toDouble(getColumnValue(rs, "REF_TRAN_AMT")));
            obj.setRemCity(getColumnValue(rs, "REM_CITY"));
            obj.setUcicId(getColumnValue(rs, "UCIC_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_COREACCT]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_COREACCT]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"USER_TYPE\",\"TOD_GRANT_AMOUNT\",\"VALUE_DATE\",\"CHANNEL\",\"BEN_CITY\",\"NON_HOME_BRANCH\",\"ONLINE_BATCH\",\"TRAN_AMOUNT\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"REF_TRAN_CRNCY\",\"RELATION\",\"PURPOSE_DESC\",\"REM_NAME\",\"TRAN_ID\",\"BEN_BIC\",\"HOST_ID\",\"CUST_TYPE\",\"TRAN_AMT\",\"PRODUCT_DESC\",\"TRANSACTION_MODE\",\"CUM_TXN_AMT\",\"MT_MESSAGE\",\"BRANCH_ID\",\"SYS_TIME\",\"TRAN_CRNCY_CODE\",\"TD_MATURITY_DATE\",\"BEN_ACCT_NO\",\"BEN_CUST_ID\",\"CODE_MSG_TYPE\",\"THRESHOLD_AMT\",\"PSTD_DATE\",\"INST_STATUS\",\"SYSTEM\",\"ATM_ID\",\"PRODUCT_CODE\",\"DR_CR\",\"CLIENT_ACC_NO\",\"REM_BIC\",\"ACCOUNTOPENDATEFLAG\",\"PRODUCTTYPEFLAG\",\"PSTD_FLG\",\"STATUS\",\"HOST_TYPE\",\"REM_ADD1\",\"TXN_KEY\",\"ACCT_ID\",\"REM_TYPE\",\"COMMODITY\",\"WHITELISTFLAG\",\"FCC_PRODUCT_CODE\",\"REM_ADD3\",\"INR_AMOUNT\",\"REM_ADD2\",\"TRN_DATE\",\"HM_DATE\",\"PURPOSE_CODE\",\"CHANNEL_ID\",\"SUB_TRAN_MODE\",\"BEN_ADD3\",\"CA_SCHEME_CODE\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"BEN_ADD1\",\"TRANNUMONIC_CODE\",\"REM_CUST_ID\",\"INSTRUMENT_NUMBER\",\"AUTHERISER_USER_ID\",\"BEN_NAME\",\"USER_ID\",\"ISIN_NUMBER\",\"TRAN_PARTICULAR\",\"CUST_ID\",\"REFERENCE_SRL_NUM\",\"BANK_CODE\",\"USD_EQV_AMT\",\"TD_LIQUIDATION_TYPE\",\"REM_CNTRY_CODE\",\"CPTY_AC_NO\",\"TRAN_DATE\",\"TRAN_CURR\",\"EFF_AVAILABLE_BALANCE\",\"INSTRUMENT_TYPE\",\"REF_TRAN_AMT\",\"REM_CITY\",\"UCIC_ID\"" +
              " FROM EVENT_FT_COREACCT";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [USER_TYPE],[TOD_GRANT_AMOUNT],[VALUE_DATE],[CHANNEL],[BEN_CITY],[NON_HOME_BRANCH],[ONLINE_BATCH],[TRAN_AMOUNT],[BRANCH],[BEN_CNTRY_CODE],[REF_TRAN_CRNCY],[RELATION],[PURPOSE_DESC],[REM_NAME],[TRAN_ID],[BEN_BIC],[HOST_ID],[CUST_TYPE],[TRAN_AMT],[PRODUCT_DESC],[TRANSACTION_MODE],[CUM_TXN_AMT],[MT_MESSAGE],[BRANCH_ID],[SYS_TIME],[TRAN_CRNCY_CODE],[TD_MATURITY_DATE],[BEN_ACCT_NO],[BEN_CUST_ID],[CODE_MSG_TYPE],[THRESHOLD_AMT],[PSTD_DATE],[INST_STATUS],[SYSTEM],[ATM_ID],[PRODUCT_CODE],[DR_CR],[CLIENT_ACC_NO],[REM_BIC],[ACCOUNTOPENDATEFLAG],[PRODUCTTYPEFLAG],[PSTD_FLG],[STATUS],[HOST_TYPE],[REM_ADD1],[TXN_KEY],[ACCT_ID],[REM_TYPE],[COMMODITY],[WHITELISTFLAG],[FCC_PRODUCT_CODE],[REM_ADD3],[INR_AMOUNT],[REM_ADD2],[TRN_DATE],[HM_DATE],[PURPOSE_CODE],[CHANNEL_ID],[SUB_TRAN_MODE],[BEN_ADD3],[CA_SCHEME_CODE],[BEN_ADD2],[REM_ACCT_NO],[BEN_ADD1],[TRANNUMONIC_CODE],[REM_CUST_ID],[INSTRUMENT_NUMBER],[AUTHERISER_USER_ID],[BEN_NAME],[USER_ID],[ISIN_NUMBER],[TRAN_PARTICULAR],[CUST_ID],[REFERENCE_SRL_NUM],[BANK_CODE],[USD_EQV_AMT],[TD_LIQUIDATION_TYPE],[REM_CNTRY_CODE],[CPTY_AC_NO],[TRAN_DATE],[TRAN_CURR],[EFF_AVAILABLE_BALANCE],[INSTRUMENT_TYPE],[REF_TRAN_AMT],[REM_CITY],[UCIC_ID]" +
              " FROM EVENT_FT_COREACCT";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`USER_TYPE`,`TOD_GRANT_AMOUNT`,`VALUE_DATE`,`CHANNEL`,`BEN_CITY`,`NON_HOME_BRANCH`,`ONLINE_BATCH`,`TRAN_AMOUNT`,`BRANCH`,`BEN_CNTRY_CODE`,`REF_TRAN_CRNCY`,`RELATION`,`PURPOSE_DESC`,`REM_NAME`,`TRAN_ID`,`BEN_BIC`,`HOST_ID`,`CUST_TYPE`,`TRAN_AMT`,`PRODUCT_DESC`,`TRANSACTION_MODE`,`CUM_TXN_AMT`,`MT_MESSAGE`,`BRANCH_ID`,`SYS_TIME`,`TRAN_CRNCY_CODE`,`TD_MATURITY_DATE`,`BEN_ACCT_NO`,`BEN_CUST_ID`,`CODE_MSG_TYPE`,`THRESHOLD_AMT`,`PSTD_DATE`,`INST_STATUS`,`SYSTEM`,`ATM_ID`,`PRODUCT_CODE`,`DR_CR`,`CLIENT_ACC_NO`,`REM_BIC`,`ACCOUNTOPENDATEFLAG`,`PRODUCTTYPEFLAG`,`PSTD_FLG`,`STATUS`,`HOST_TYPE`,`REM_ADD1`,`TXN_KEY`,`ACCT_ID`,`REM_TYPE`,`COMMODITY`,`WHITELISTFLAG`,`FCC_PRODUCT_CODE`,`REM_ADD3`,`INR_AMOUNT`,`REM_ADD2`,`TRN_DATE`,`HM_DATE`,`PURPOSE_CODE`,`CHANNEL_ID`,`SUB_TRAN_MODE`,`BEN_ADD3`,`CA_SCHEME_CODE`,`BEN_ADD2`,`REM_ACCT_NO`,`BEN_ADD1`,`TRANNUMONIC_CODE`,`REM_CUST_ID`,`INSTRUMENT_NUMBER`,`AUTHERISER_USER_ID`,`BEN_NAME`,`USER_ID`,`ISIN_NUMBER`,`TRAN_PARTICULAR`,`CUST_ID`,`REFERENCE_SRL_NUM`,`BANK_CODE`,`USD_EQV_AMT`,`TD_LIQUIDATION_TYPE`,`REM_CNTRY_CODE`,`CPTY_AC_NO`,`TRAN_DATE`,`TRAN_CURR`,`EFF_AVAILABLE_BALANCE`,`INSTRUMENT_TYPE`,`REF_TRAN_AMT`,`REM_CITY`,`UCIC_ID`" +
              " FROM EVENT_FT_COREACCT";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_COREACCT (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"USER_TYPE\",\"TOD_GRANT_AMOUNT\",\"VALUE_DATE\",\"CHANNEL\",\"BEN_CITY\",\"NON_HOME_BRANCH\",\"ONLINE_BATCH\",\"TRAN_AMOUNT\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"REF_TRAN_CRNCY\",\"RELATION\",\"PURPOSE_DESC\",\"REM_NAME\",\"TRAN_ID\",\"BEN_BIC\",\"HOST_ID\",\"CUST_TYPE\",\"TRAN_AMT\",\"PRODUCT_DESC\",\"TRANSACTION_MODE\",\"CUM_TXN_AMT\",\"MT_MESSAGE\",\"BRANCH_ID\",\"SYS_TIME\",\"TRAN_CRNCY_CODE\",\"TD_MATURITY_DATE\",\"BEN_ACCT_NO\",\"BEN_CUST_ID\",\"CODE_MSG_TYPE\",\"THRESHOLD_AMT\",\"PSTD_DATE\",\"INST_STATUS\",\"SYSTEM\",\"ATM_ID\",\"PRODUCT_CODE\",\"DR_CR\",\"CLIENT_ACC_NO\",\"REM_BIC\",\"ACCOUNTOPENDATEFLAG\",\"PRODUCTTYPEFLAG\",\"PSTD_FLG\",\"STATUS\",\"HOST_TYPE\",\"REM_ADD1\",\"TXN_KEY\",\"ACCT_ID\",\"REM_TYPE\",\"COMMODITY\",\"WHITELISTFLAG\",\"FCC_PRODUCT_CODE\",\"REM_ADD3\",\"INR_AMOUNT\",\"REM_ADD2\",\"TRN_DATE\",\"HM_DATE\",\"PURPOSE_CODE\",\"CHANNEL_ID\",\"SUB_TRAN_MODE\",\"BEN_ADD3\",\"CA_SCHEME_CODE\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"BEN_ADD1\",\"TRANNUMONIC_CODE\",\"REM_CUST_ID\",\"INSTRUMENT_NUMBER\",\"AUTHERISER_USER_ID\",\"BEN_NAME\",\"USER_ID\",\"ISIN_NUMBER\",\"TRAN_PARTICULAR\",\"CUST_ID\",\"REFERENCE_SRL_NUM\",\"BANK_CODE\",\"USD_EQV_AMT\",\"TD_LIQUIDATION_TYPE\",\"REM_CNTRY_CODE\",\"CPTY_AC_NO\",\"TRAN_DATE\",\"TRAN_CURR\",\"EFF_AVAILABLE_BALANCE\",\"INSTRUMENT_TYPE\",\"REF_TRAN_AMT\",\"REM_CITY\",\"UCIC_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[USER_TYPE],[TOD_GRANT_AMOUNT],[VALUE_DATE],[CHANNEL],[BEN_CITY],[NON_HOME_BRANCH],[ONLINE_BATCH],[TRAN_AMOUNT],[BRANCH],[BEN_CNTRY_CODE],[REF_TRAN_CRNCY],[RELATION],[PURPOSE_DESC],[REM_NAME],[TRAN_ID],[BEN_BIC],[HOST_ID],[CUST_TYPE],[TRAN_AMT],[PRODUCT_DESC],[TRANSACTION_MODE],[CUM_TXN_AMT],[MT_MESSAGE],[BRANCH_ID],[SYS_TIME],[TRAN_CRNCY_CODE],[TD_MATURITY_DATE],[BEN_ACCT_NO],[BEN_CUST_ID],[CODE_MSG_TYPE],[THRESHOLD_AMT],[PSTD_DATE],[INST_STATUS],[SYSTEM],[ATM_ID],[PRODUCT_CODE],[DR_CR],[CLIENT_ACC_NO],[REM_BIC],[ACCOUNTOPENDATEFLAG],[PRODUCTTYPEFLAG],[PSTD_FLG],[STATUS],[HOST_TYPE],[REM_ADD1],[TXN_KEY],[ACCT_ID],[REM_TYPE],[COMMODITY],[WHITELISTFLAG],[FCC_PRODUCT_CODE],[REM_ADD3],[INR_AMOUNT],[REM_ADD2],[TRN_DATE],[HM_DATE],[PURPOSE_CODE],[CHANNEL_ID],[SUB_TRAN_MODE],[BEN_ADD3],[CA_SCHEME_CODE],[BEN_ADD2],[REM_ACCT_NO],[BEN_ADD1],[TRANNUMONIC_CODE],[REM_CUST_ID],[INSTRUMENT_NUMBER],[AUTHERISER_USER_ID],[BEN_NAME],[USER_ID],[ISIN_NUMBER],[TRAN_PARTICULAR],[CUST_ID],[REFERENCE_SRL_NUM],[BANK_CODE],[USD_EQV_AMT],[TD_LIQUIDATION_TYPE],[REM_CNTRY_CODE],[CPTY_AC_NO],[TRAN_DATE],[TRAN_CURR],[EFF_AVAILABLE_BALANCE],[INSTRUMENT_TYPE],[REF_TRAN_AMT],[REM_CITY],[UCIC_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`USER_TYPE`,`TOD_GRANT_AMOUNT`,`VALUE_DATE`,`CHANNEL`,`BEN_CITY`,`NON_HOME_BRANCH`,`ONLINE_BATCH`,`TRAN_AMOUNT`,`BRANCH`,`BEN_CNTRY_CODE`,`REF_TRAN_CRNCY`,`RELATION`,`PURPOSE_DESC`,`REM_NAME`,`TRAN_ID`,`BEN_BIC`,`HOST_ID`,`CUST_TYPE`,`TRAN_AMT`,`PRODUCT_DESC`,`TRANSACTION_MODE`,`CUM_TXN_AMT`,`MT_MESSAGE`,`BRANCH_ID`,`SYS_TIME`,`TRAN_CRNCY_CODE`,`TD_MATURITY_DATE`,`BEN_ACCT_NO`,`BEN_CUST_ID`,`CODE_MSG_TYPE`,`THRESHOLD_AMT`,`PSTD_DATE`,`INST_STATUS`,`SYSTEM`,`ATM_ID`,`PRODUCT_CODE`,`DR_CR`,`CLIENT_ACC_NO`,`REM_BIC`,`ACCOUNTOPENDATEFLAG`,`PRODUCTTYPEFLAG`,`PSTD_FLG`,`STATUS`,`HOST_TYPE`,`REM_ADD1`,`TXN_KEY`,`ACCT_ID`,`REM_TYPE`,`COMMODITY`,`WHITELISTFLAG`,`FCC_PRODUCT_CODE`,`REM_ADD3`,`INR_AMOUNT`,`REM_ADD2`,`TRN_DATE`,`HM_DATE`,`PURPOSE_CODE`,`CHANNEL_ID`,`SUB_TRAN_MODE`,`BEN_ADD3`,`CA_SCHEME_CODE`,`BEN_ADD2`,`REM_ACCT_NO`,`BEN_ADD1`,`TRANNUMONIC_CODE`,`REM_CUST_ID`,`INSTRUMENT_NUMBER`,`AUTHERISER_USER_ID`,`BEN_NAME`,`USER_ID`,`ISIN_NUMBER`,`TRAN_PARTICULAR`,`CUST_ID`,`REFERENCE_SRL_NUM`,`BANK_CODE`,`USD_EQV_AMT`,`TD_LIQUIDATION_TYPE`,`REM_CNTRY_CODE`,`CPTY_AC_NO`,`TRAN_DATE`,`TRAN_CURR`,`EFF_AVAILABLE_BALANCE`,`INSTRUMENT_TYPE`,`REF_TRAN_AMT`,`REM_CITY`,`UCIC_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

