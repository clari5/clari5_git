package clari5.custom.jasper;

import clari5.custom.db.CustMaster;
import clari5.custom.db.NewModifiedCustomer;

import clari5.custom.yes.db.*;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import cxps.apex.utils.CxpsLogger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CustWlDaemon extends CxpsRunnable implements ICxResource {
    private static final Queue<CustFact> eventDatas = new ConcurrentLinkedQueue<>();
    static final Set<String> custIds = new HashSet<>();
    private static final CxpsLogger logger = CxpsLogger.getLogger(CustWlDaemon.class);
    static NewModifiedCustomer newModifiedCustomer = new NewModifiedCustomer();
    final static  String[] typeList= {"PAN", "PASSPORT"};

    static int offset = 0;
    static int limit = 10;

    @Override
    public void configure(Hocon h) {
        offset = h.getInt("offset");
        limit = h.getInt("limit");
    }

    @Override
    public void release() {
        custIds.clear();
        eventDatas.clear();
    }

    @Override
    public void refresh() {
        custIds.clear();
        eventDatas.clear();
    }

    @Override
    public ConfigType getType() {
        return null;
    }

    @Override
    public void configure(Hocon h, Hocon location) {

    }

    @Override
    public void refresh(Hocon conf, Hocon location) {

    }

    @Override
    protected Object getData() throws RuntimeFatalException {

        CustFact custFactPayload = getNextCustId();
        CustRequestPayload custRequestPayload = new CustRequestPayload();

        int passInsertCount=0;
        int panInsertCount=0;
        CustMatch match=null;

        try {
            for (int i = 0; i < typeList.length; i++)
            {
                String type = typeList[i];
                Queue<CxJson> queue = custRequestPayload.setWlReqValues(custFactPayload.getNewModifiedCustomers(),type);
                logger.info("The data in the queue [" +queue+"]");
                match= custRequestPayload.sendBatchWlReq(queue);
                logger.info("Match Result obtained ["+match+"]" );


            }
            logger.info("PanInsertCount ["+panInsertCount+"] PassportInsertCount ["+passInsertCount+"]");
            newModifiedCustomer.update(match.getCust_id(),"C"," ");
        } catch (NullPointerException np) {
            logger.warn("No fresh record in NEW_MODIFIED_CUSTOMERS" +np.getMessage() );
        }
        return match;
    }


    @Override
    protected void processData(Object o) throws RuntimeFatalException {


    }



    protected synchronized static CustFact extractEvent() {
        logger.debug("No of cust-id to be processed : " + eventDatas.size() + "and Current Thread Id is " + Thread.currentThread().getId());
        while (!eventDatas.isEmpty()) {
            CustFact data = eventDatas.poll();
            custIds.remove(data.getCust_id());
            return data;
        }
        return null;
    }



    protected static CustFact getNextCustId() throws NullPointerException {
        CustFact data = extractEvent();
        if (data != null) {
            logger.debug("[" + Thread.currentThread().getName() + "] Retrieving  NEW_MODIFIED_CUSTOMERS  data  and  customer_id [" + data.getCust_id() + "]");
            return data;
        }
        try {

            List<NewModifiedCustomers> newModifiedCustList = newModifiedCustomer.getFreshKeys(offset, limit);
            for (NewModifiedCustomers newModifiedCustomers : newModifiedCustList) {

                if (!custIds.contains(newModifiedCustomers.getCustId()) || custIds.isEmpty()) {
                    custIds.add(newModifiedCustomers.getCustId());
                    eventDatas.add(new CustFact(newModifiedCustomers, newModifiedCustomers.getCustId()));
                }
            }

        } catch (Exception io) {
            logger.error("Failed to convert Cust summary to cust details" + io.getMessage() + "\n Cause" + io.getCause());
        }
        if ((data = extractEvent()) != null) {
            logger.debug("[" + Thread.currentThread().getName() + "] Retrieving NEW_MODIFIED_CUSTOMERS    data  of  [" + data.toString() + "]");
            return data;
        }


        return data;
    }
}

