cxps.events.event.ft-epi{
  table-name : EVENT_FT_EPI
  event-mnemonic: EPI
  workspaces : {
    	ACCOUNT: transaction_account
	CUSTOMER: cust_id
  }
  event-attributes : {
         host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        merchant-id: {db : true ,raw_name : merchant_id ,type: "string:200" }
        merchant-category: {db : true ,raw_name : merchant_category ,type: "string:200" }
        merchant-type: {db : true ,raw_name : merchant_type ,type: "string:200" }
        transaction-amount: {db : true ,raw_name : transaction_amount ,type: "number:20,3" }
        pay-via: {db : true ,raw_name : pay_via ,type: "string:200" }
        transaction-account: {db : true ,raw_name : transaction_account ,type: "string:200" }
        account-balance: {db : true ,raw_name : account_balance ,type: "number:20,3" }
        transaction-type: {db : true ,raw_name : transaction_type ,type: "string:200" }
        ref1: {db : true ,raw_name : ref1 ,type: "string:200" }
        ref2: {db : true ,raw_name : ref2 ,type: "string:200" }
        ref3: {db : true ,raw_name : ref3 ,type: "string:200" }
        ref4: {db : true ,raw_name : ref4 ,type: "string:200" }
        ref5: {db : true ,raw_name : ref5 ,type: "string:200" }
        ref6: {db : true ,raw_name : ref6 ,type: "string:200" }
        ref7: {db : true ,raw_name : ref7 ,type: "string:200" }
        ref8: {db : true ,raw_name : ref8 ,type: "string:200" }
        ref9: {db : true ,raw_name : ref9 ,type: "string:200" }
        ref10: {db : true ,raw_name : ref10 ,type: "string:200" }
        ref11: {db : true ,raw_name : ref11 ,type: "string:200" }
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
        risk-band:{db : true ,raw_name :risk_band ,type : "string:200", derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}
      
}
}
