package clari5.custom.inbox.processors.retryservice;

import clari5.custom.inbox.processors.tat.Cl5IbxRetry;;

import java.util.concurrent.ConcurrentLinkedQueue;

public interface RetryTatMailingService {
    public void sendMail(ConcurrentLinkedQueue<Cl5IbxRetry> items);
}
