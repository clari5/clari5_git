clari5.custom.yes.db {
        entity {
                new-acct-details {
                     attributes = [
                                { name :acct-id,        	 type : "string:50",   	key=true}
                                { name :cust-id,       		 type : "string:50",   	key=false}
                                { name :cust-name,      	 type : "string:20",    key=false}
                                { name :cust-type,      	 type : "string:20",    key=false}
                                { name :dob,             	 type: date }
                                { name :pan,           		 type : "string:50", 	key=false}
                                { name :mobile-no,	         type : "string:11", 	key=false}
                                { name :address,           	 type : "string:100", 	key=false}
                                { name :office-no,          	 type : "string:11", 	key=false}
                                { name :resi-no,           	 type : "string:11", 	key=false}
                                { name :date-of-incorporation, 	 type : date}
                                { name :acct-open-date,     	 type : date}
                                { name :status,           	 type : "string:1",	key=false}
                                { name :remarks,           	 type : "string:8000", 	key=false}
                                { name :match-results,      	 type : "string:8000", 	key=false}
                                { name :match-type,         	 type : "string:500", 	key=false}
                                { name :last-modified-time, 	 type : timestamp}
                                ]
                     criteria-query {
                          name: NewAcctDetails
                          summary =[acct-id, cust-id, cust-name, cust-type, dob, pan, mobile-no, address, office-no, resi-no, date-of-incorporation, acct-open-date, status, remarks, match-type, last-modified-time] 
                          where {
                               status: equal-clause
                          }
                          order = ["last-modified-time"]
                     }
                     indexes {
                           new-acct-status-idx : [ status ]
                }



                }
        }

}
