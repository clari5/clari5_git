// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_IBLOGIN", Schema="rice")
public class NFT_IbLoginEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String addrNetLocal;
       @Field(size=200) public String twoFAStatus;
       @Field(size=200) public String agent;
       @Field(size=200) public String deviceBinding;
       @Field(size=200) public String iPCountry;
       @Field(size=200) public String errorDesc;
       @Field(size=200) public String source;
       @Field(size=200) public String deviceId;
       @Field(size=200) public String cookieEnabledFlag;
       @Field(size=200) public String timeSlot;
       @Field(size=200) public String javaEnabledFlag;
       @Field(size=200) public String userLoanOnlyFlag;
       @Field(size=200) public String distBtCurrLastUsedIp;
       @Field(size=200) public String addrNetwork;
       @Field(size=200) public String iPCity;
       @Field(size=200) public String deviceType;
       @Field(size=200) public String browserTimeZone;
       @Field public java.sql.Timestamp deviceIdDate;
       @Field(size=200) public String browserName;
       @Field(size=200) public String twoFAMode;
       @Field(size=200) public String systemPassword;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String firstLogin;
       @Field(size=200) public String screenRes;
       @Field(size=200) public String userCCOnlyFlag;
       @Field(size=200) public String sessionId;
       @Field(size=200) public String browserLang;
       @Field(size=200) public String countryCode;
       @Field(size=20) public Double distance;
       @Field(size=200) public String keys;
       @Field(size=200) public String succFailFlg;
       @Field(size=200) public String loginIdStatus;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String jSStatus;
       @Field(size=200) public String javaStatus;
       @Field(size=200) public String systemUserId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String mobileEmulator;
       @Field(size=200) public String timeZone;
       @Field(size=200) public String timeZoneOffset;
       @Field(size=200) public String browserPlugin;
       @Field(size=200) public String deviceIdCount;
       @Field(size=200) public String os;
       @Field(size=200) public String screenResolution;
       @Field(size=200) public String userId;
       @Field(size=200) public String mobileOS;
       @Field(size=200) public String custId;
       @Field public java.sql.Timestamp cookieDateTimeStamp;
       @Field(size=200) public String browserVersion;
       @Field(size=200) public String ipAddress;
       @Field(size=200) public String riskBand;
       @Field(size=200) public String userAgent;


    @JsonIgnore
    public ITable<NFT_IbLoginEvent> t = AEF.getITable(this);

	public NFT_IbLoginEvent(){}

    public NFT_IbLoginEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("IbLogin");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAddrNetLocal(json.getString("addr_net_local"));
            setTwoFAStatus(json.getString("2fa_status"));
            setAgent(json.getString("agent"));
            setIPCountry(json.getString("ip_country"));
            setErrorDesc(json.getString("error_desc"));
            setSource(json.getString("source"));
            setDeviceId(json.getString("device_id"));
            setCookieEnabledFlag(json.getString("cookie_enabled_flag"));
            setJavaEnabledFlag(json.getString("java_enabled_flag"));
            setUserLoanOnlyFlag(json.getString("user_loan_only_flag"));
            setDistBtCurrLastUsedIp(json.getString("dist_bt_curr_last_used_ip"));
            setAddrNetwork(json.getString("addr_network"));
            setIPCity(json.getString("ip_city"));
            setDeviceType(json.getString("device_type"));
            setBrowserTimeZone(json.getString("browser_time_zone"));
            setDeviceIdDate(EventHelper.toTimestamp(json.getString("device_id_date")));
            setBrowserName(json.getString("browser_name"));
            setTwoFAMode(json.getString("2fa_mode"));
            setSystemPassword(json.getString("system_password"));
            setErrorCode(json.getString("error_code"));
            setFirstLogin(json.getString("first_login"));
            setScreenRes(json.getString("screen_res"));
            setUserCCOnlyFlag(json.getString("user_cc_only_flag"));
            setSessionId(json.getString("session_id"));
            setBrowserLang(json.getString("browser_lang"));
            setKeys(json.getString("keys"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setLoginIdStatus(json.getString("login_id_status"));
            setCustSegment(json.getString("cust_segment"));
            setJSStatus(json.getString("js_status"));
            setJavaStatus(json.getString("java_status"));
            setSystemUserId(json.getString("system_user_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setMobileEmulator(json.getString("mobile_emulator"));
            setTimeZone(json.getString("time_zone"));
            setTimeZoneOffset(json.getString("time_zone_offset"));
            setBrowserPlugin(json.getString("browser_plugin"));
            setOs(json.getString("os"));
            setScreenResolution(json.getString("screen_resolution"));
            setUserId(json.getString("user_id"));
            setMobileOS(json.getString("mobile_os"));
            setCustId(json.getString("cust_id"));
            setCookieDateTimeStamp(EventHelper.toTimestamp(json.getString("cookie_date_time_stamp")));
            setBrowserVersion(json.getString("browser_version"));
            setIpAddress(json.getString("ip_address"));
            setRiskBand(json.getString("risk_band"));
            setUserAgent(json.getString("user_agent"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setDeviceBinding(clari5.custom.eventpreprocessors.LoginPreprocessor.saveInRdbms(this));setTimeSlot(clari5.custom.eventpreprocessors.LoginPreprocessor.getTimeSlot(this));setCountryCode(clari5.custom.eventpreprocessors.LoginPreprocessor.getCountryCode(this));setDistance(cxps.events.CustomFieldDerivator.getDistanceBetweenIP(this));setDeviceIdCount(clari5.custom.eventpreprocessors.LoginPreprocessor.deviceCount(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "IBL"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAddrNetLocal(){ return addrNetLocal; }

    public String getTwoFAStatus(){ return twoFAStatus; }

    public String getAgent(){ return agent; }

    public String getIPCountry(){ return iPCountry; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSource(){ return source; }

    public String getDeviceId(){ return deviceId; }

    public String getCookieEnabledFlag(){ return cookieEnabledFlag; }

    public String getJavaEnabledFlag(){ return javaEnabledFlag; }

    public String getUserLoanOnlyFlag(){ return userLoanOnlyFlag; }

    public String getDistBtCurrLastUsedIp(){ return distBtCurrLastUsedIp; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getIPCity(){ return iPCity; }

    public String getDeviceType(){ return deviceType; }

    public String getBrowserTimeZone(){ return browserTimeZone; }

    public java.sql.Timestamp getDeviceIdDate(){ return deviceIdDate; }

    public String getBrowserName(){ return browserName; }

    public String getTwoFAMode(){ return twoFAMode; }

    public String getSystemPassword(){ return systemPassword; }

    public String getErrorCode(){ return errorCode; }

    public String getFirstLogin(){ return firstLogin; }

    public String getScreenRes(){ return screenRes; }

    public String getUserCCOnlyFlag(){ return userCCOnlyFlag; }

    public String getSessionId(){ return sessionId; }

    public String getBrowserLang(){ return browserLang; }

    public String getKeys(){ return keys; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getLoginIdStatus(){ return loginIdStatus; }

    public String getCustSegment(){ return custSegment; }

    public String getJSStatus(){ return jSStatus; }

    public String getJavaStatus(){ return javaStatus; }

    public String getSystemUserId(){ return systemUserId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getMobileEmulator(){ return mobileEmulator; }

    public String getTimeZone(){ return timeZone; }

    public String getTimeZoneOffset(){ return timeZoneOffset; }

    public String getBrowserPlugin(){ return browserPlugin; }

    public String getOs(){ return os; }

    public String getScreenResolution(){ return screenResolution; }

    public String getUserId(){ return userId; }

    public String getMobileOS(){ return mobileOS; }

    public String getCustId(){ return custId; }

    public java.sql.Timestamp getCookieDateTimeStamp(){ return cookieDateTimeStamp; }

    public String getBrowserVersion(){ return browserVersion; }

    public String getIpAddress(){ return ipAddress; }

    public String getRiskBand(){ return riskBand; }

    public String getUserAgent(){ return userAgent; }
    public String getDeviceBinding(){ return deviceBinding; }

    public String getTimeSlot(){ return timeSlot; }

    public String getCountryCode(){ return countryCode; }

    public Double getDistance(){ return distance; }

    public String getDeviceIdCount(){ return deviceIdCount; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAddrNetLocal(String val){ this.addrNetLocal = val; }
    public void setTwoFAStatus(String val){ this.twoFAStatus = val; }
    public void setAgent(String val){ this.agent = val; }
    public void setIPCountry(String val){ this.iPCountry = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSource(String val){ this.source = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setCookieEnabledFlag(String val){ this.cookieEnabledFlag = val; }
    public void setJavaEnabledFlag(String val){ this.javaEnabledFlag = val; }
    public void setUserLoanOnlyFlag(String val){ this.userLoanOnlyFlag = val; }
    public void setDistBtCurrLastUsedIp(String val){ this.distBtCurrLastUsedIp = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setIPCity(String val){ this.iPCity = val; }
    public void setDeviceType(String val){ this.deviceType = val; }
    public void setBrowserTimeZone(String val){ this.browserTimeZone = val; }
    public void setDeviceIdDate(java.sql.Timestamp val){ this.deviceIdDate = val; }
    public void setBrowserName(String val){ this.browserName = val; }
    public void setTwoFAMode(String val){ this.twoFAMode = val; }
    public void setSystemPassword(String val){ this.systemPassword = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setFirstLogin(String val){ this.firstLogin = val; }
    public void setScreenRes(String val){ this.screenRes = val; }
    public void setUserCCOnlyFlag(String val){ this.userCCOnlyFlag = val; }
    public void setSessionId(String val){ this.sessionId = val; }
    public void setBrowserLang(String val){ this.browserLang = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setLoginIdStatus(String val){ this.loginIdStatus = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setJSStatus(String val){ this.jSStatus = val; }
    public void setJavaStatus(String val){ this.javaStatus = val; }
    public void setSystemUserId(String val){ this.systemUserId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setMobileEmulator(String val){ this.mobileEmulator = val; }
    public void setTimeZone(String val){ this.timeZone = val; }
    public void setTimeZoneOffset(String val){ this.timeZoneOffset = val; }
    public void setBrowserPlugin(String val){ this.browserPlugin = val; }
    public void setOs(String val){ this.os = val; }
    public void setScreenResolution(String val){ this.screenResolution = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setMobileOS(String val){ this.mobileOS = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setCookieDateTimeStamp(java.sql.Timestamp val){ this.cookieDateTimeStamp = val; }
    public void setBrowserVersion(String val){ this.browserVersion = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setRiskBand(String val){ this.riskBand = val; }
    public void setUserAgent(String val){ this.userAgent = val; }
    public void setDeviceBinding(String val){ this.deviceBinding = val; }
    public void setTimeSlot(String val){ this.timeSlot = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setDistance(Double val){ this.distance = val; }
    public void setDeviceIdCount(String val){ this.deviceIdCount = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_IbLoginEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String terminalKey= h.getCxKeyGivenHostKey(WorkspaceName.TERMINAL, getHostId(), this.deviceId);
        wsInfoSet.add(new WorkspaceInfo("Terminal", terminalKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.addrNetwork);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_IbLogin");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "IbLogin");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
