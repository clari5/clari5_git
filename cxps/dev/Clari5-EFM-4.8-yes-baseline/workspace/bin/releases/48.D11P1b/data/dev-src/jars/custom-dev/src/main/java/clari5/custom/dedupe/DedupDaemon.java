package clari5.custom.dedupe;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DedupDaemon extends CxpsRunnable implements ICxResource {

    private static final CxpsLogger logger = CxpsLogger.getLogger(DedupDaemon.class);

    //static ConcurrentLinkedQueue<LinkedHashMap> dataQueue;

    @Override
    protected Object getData() throws RuntimeFatalException {
        return ModifiedCustomer.getData();
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside Process Data --> " + o);
        if (o instanceof ConcurrentLinkedQueue) {
            ConcurrentLinkedQueue<LinkedHashMap> dataQueue = (ConcurrentLinkedQueue<LinkedHashMap>) o;

            while (!dataQueue.isEmpty()) {
                LinkedHashMap<String, String> d = dataQueue.poll();
                new ModifiedCustomer().process(d);
                /*try {
                    logger.debug("before runExcecutor--> ");
                    ModifiedCustomerThread.runExecutor();
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
            }
        }
    }

    @Override
    public void configure(Hocon h) {
        logger.debug("Dedup Configured");
    }
}
