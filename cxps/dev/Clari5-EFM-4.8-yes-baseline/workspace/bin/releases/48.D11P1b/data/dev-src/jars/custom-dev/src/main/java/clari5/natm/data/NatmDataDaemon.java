package clari5.natm.data;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/*****
 *    @author : Suryakant
 *    @since : 05/02/2020
 */

public class NatmDataDaemon extends CxpsRunnable implements ICxResource {

    private static final CxpsLogger logger = CxpsLogger.getLogger(NatmDataDaemon.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
        return NatmDataFetch.getNatmData();
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside the Process Data for NATM Daemon");

        if (o instanceof ConcurrentLinkedQueue) {
            ConcurrentLinkedQueue<HashMap> natmDataQueue = (ConcurrentLinkedQueue<HashMap>) o;

            try (RDBMSSession session = Rdbms.getAppSession()) {
                try (CxConnection connection = session.getCxConnection()) {

                    while (!natmDataQueue.isEmpty()) {
                        HashMap jsonObject = natmDataQueue.poll();
                        NatmDataInsert.dataInsertNatmTable(jsonObject, connection);
                    }
                    connection.close();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void configure(Hocon h) {
        logger.debug("Natm Data fetched Configured");
    }
}
