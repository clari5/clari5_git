// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_WMTxnEventMapper extends EventMapper<FT_WMTxnEvent> {

public FT_WMTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_WMTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_WMTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_WMTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_WMTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_WMTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_WMTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setDouble(i++, obj.getAmount());
            preparedStatement.setString(i++, obj.getSubTranType());
            preparedStatement.setString(i++, obj.getTxnRefNo());
            preparedStatement.setString(i++, obj.getWmsRefNo());
            preparedStatement.setString(i++, obj.getSchemeName());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getTranCode());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getRmCode());
            preparedStatement.setString(i++, obj.getTradeDate());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getSchemeType());
            preparedStatement.setString(i++, obj.getHostId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_WMTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_WMTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_WMTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_WMTXN"));
        putList = new ArrayList<>();

        for (FT_WMTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "AMOUNT", String.valueOf(obj.getAmount()));
            p = this.insert(p, "SUB_TRAN_TYPE",  obj.getSubTranType());
            p = this.insert(p, "TXN_REF_NO",  obj.getTxnRefNo());
            p = this.insert(p, "WMS_REF_NO",  obj.getWmsRefNo());
            p = this.insert(p, "SCHEME_NAME",  obj.getSchemeName());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "TRAN_CODE",  obj.getTranCode());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "RM_CODE",  obj.getRmCode());
            p = this.insert(p, "TRADE_DATE",  obj.getTradeDate());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "SCHEME_TYPE",  obj.getSchemeType());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_WMTXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_WMTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_WMTXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_WMTxnEvent obj = new FT_WMTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setAmount(rs.getDouble("AMOUNT"));
    obj.setSubTranType(rs.getString("SUB_TRAN_TYPE"));
    obj.setTxnRefNo(rs.getString("TXN_REF_NO"));
    obj.setWmsRefNo(rs.getString("WMS_REF_NO"));
    obj.setSchemeName(rs.getString("SCHEME_NAME"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setTranCode(rs.getString("TRAN_CODE"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setRmCode(rs.getString("RM_CODE"));
    obj.setTradeDate(rs.getString("TRADE_DATE"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setSchemeType(rs.getString("SCHEME_TYPE"));
    obj.setHostId(rs.getString("HOST_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_WMTXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_WMTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_WMTxnEvent> events;
 FT_WMTxnEvent obj = new FT_WMTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_WMTXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_WMTxnEvent obj = new FT_WMTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setAmount(EventHelper.toDouble(getColumnValue(rs, "AMOUNT")));
            obj.setSubTranType(getColumnValue(rs, "SUB_TRAN_TYPE"));
            obj.setTxnRefNo(getColumnValue(rs, "TXN_REF_NO"));
            obj.setWmsRefNo(getColumnValue(rs, "WMS_REF_NO"));
            obj.setSchemeName(getColumnValue(rs, "SCHEME_NAME"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setTranCode(getColumnValue(rs, "TRAN_CODE"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setRmCode(getColumnValue(rs, "RM_CODE"));
            obj.setTradeDate(getColumnValue(rs, "TRADE_DATE"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setSchemeType(getColumnValue(rs, "SCHEME_TYPE"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_WMTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_WMTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCT_ID\",\"AMOUNT\",\"SUB_TRAN_TYPE\",\"TXN_REF_NO\",\"WMS_REF_NO\",\"SCHEME_NAME\",\"SYS_TIME\",\"CUST_ID\",\"TRAN_CODE\",\"EVENTTS\",\"RM_CODE\",\"TRADE_DATE\",\"TRAN_TYPE\",\"SCHEME_TYPE\",\"HOST_ID\"" +
              " FROM EVENT_FT_WMTXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCT_ID],[AMOUNT],[SUB_TRAN_TYPE],[TXN_REF_NO],[WMS_REF_NO],[SCHEME_NAME],[SYS_TIME],[CUST_ID],[TRAN_CODE],[EVENTTS],[RM_CODE],[TRADE_DATE],[TRAN_TYPE],[SCHEME_TYPE],[HOST_ID]" +
              " FROM EVENT_FT_WMTXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCT_ID`,`AMOUNT`,`SUB_TRAN_TYPE`,`TXN_REF_NO`,`WMS_REF_NO`,`SCHEME_NAME`,`SYS_TIME`,`CUST_ID`,`TRAN_CODE`,`EVENTTS`,`RM_CODE`,`TRADE_DATE`,`TRAN_TYPE`,`SCHEME_TYPE`,`HOST_ID`" +
              " FROM EVENT_FT_WMTXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_WMTXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCT_ID\",\"AMOUNT\",\"SUB_TRAN_TYPE\",\"TXN_REF_NO\",\"WMS_REF_NO\",\"SCHEME_NAME\",\"SYS_TIME\",\"CUST_ID\",\"TRAN_CODE\",\"EVENTTS\",\"RM_CODE\",\"TRADE_DATE\",\"TRAN_TYPE\",\"SCHEME_TYPE\",\"HOST_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCT_ID],[AMOUNT],[SUB_TRAN_TYPE],[TXN_REF_NO],[WMS_REF_NO],[SCHEME_NAME],[SYS_TIME],[CUST_ID],[TRAN_CODE],[EVENTTS],[RM_CODE],[TRADE_DATE],[TRAN_TYPE],[SCHEME_TYPE],[HOST_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCT_ID`,`AMOUNT`,`SUB_TRAN_TYPE`,`TXN_REF_NO`,`WMS_REF_NO`,`SCHEME_NAME`,`SYS_TIME`,`CUST_ID`,`TRAN_CODE`,`EVENTTS`,`RM_CODE`,`TRADE_DATE`,`TRAN_TYPE`,`SCHEME_TYPE`,`HOST_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

