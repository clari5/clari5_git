// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_EPI", Schema="rice")
public class FT_EpiEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=20) public Double transactionAmount;
       @Field(size=200) public String ref11;
       @Field(size=200) public String ref10;
       @Field(size=200) public String errorDesc;
       @Field(size=10) public String succFailFlg;
       @Field(size=200) public String ref2;
       @Field(size=200) public String ref1;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String ipCountry;
       @Field(size=200) public String deviceId;
       @Field(size=200) public String transactionAccount;
       @Field(size=200) public String merchantType;
       @Field(size=200) public String payVia;
       @Field(size=200) public String addrNetwork;
       @Field(size=200) public String merchantCategory;
       @Field(size=2) public String hostId;
       @Field(size=200) public String transactionType;
       @Field(size=20) public String twoFaStatus;
       @Field(size=200) public String ipCity;
       @Field(size=200) public String obdxTransactionName;
       @Field(size=200) public String userId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String obdxModuleName;
       @Field(size=200) public String ref8;
       @Field(size=200) public String ref7;
       @Field(size=20) public Double accountBalance;
       @Field(size=200) public String ref9;
       @Field(size=200) public String riskBand;
       @Field(size=200) public String ref4;
       @Field(size=200) public String ref3;
       @Field(size=200) public String ref6;
       @Field(size=200) public String ref5;
       @Field(size=200) public String merchantId;
       @Field(size=200) public String sessionId;


    @JsonIgnore
    public ITable<FT_EpiEvent> t = AEF.getITable(this);

	public FT_EpiEvent(){}

    public FT_EpiEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Epi");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setTransactionAmount(EventHelper.toDouble(json.getString("transaction_amount")));
            setRef11(json.getString("ref11"));
            setRef10(json.getString("ref10"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setRef2(json.getString("ref2"));
            setRef1(json.getString("ref1"));
            setCustSegment(json.getString("cust_segment"));
            setIpCountry(json.getString("ip_country"));
            setDeviceId(json.getString("device_id"));
            setTransactionAccount(json.getString("transaction_account"));
            setMerchantType(json.getString("merchant_type"));
            setPayVia(json.getString("pay_via"));
            setAddrNetwork(json.getString("addr_network"));
            setMerchantCategory(json.getString("merchant_category"));
            setHostId(json.getString("host_id"));
            setTransactionType(json.getString("transaction_type"));
            setTwoFaStatus(json.getString("2fa_status"));
            setIpCity(json.getString("ip_city"));
            setObdxTransactionName(json.getString("obdx_transaction_name"));
            setUserId(json.getString("user_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setObdxModuleName(json.getString("obdx_module_name"));
            setRef8(json.getString("ref8"));
            setRef7(json.getString("ref7"));
            setAccountBalance(EventHelper.toDouble(json.getString("account_balance")));
            setRef9(json.getString("ref9"));
            setRef4(json.getString("ref4"));
            setRef3(json.getString("ref3"));
            setRef6(json.getString("ref6"));
            setRef5(json.getString("ref5"));
            setMerchantId(json.getString("merchant_id"));
            setSessionId(json.getString("session_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setRiskBand(cxps.events.CustomFieldDerivator.checkInstanceofEvent(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "EPI"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public Double getTransactionAmount(){ return transactionAmount; }

    public String getRef11(){ return ref11; }

    public String getRef10(){ return ref10; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getRef2(){ return ref2; }

    public String getRef1(){ return ref1; }

    public String getCustSegment(){ return custSegment; }

    public String getIpCountry(){ return ipCountry; }

    public String getDeviceId(){ return deviceId; }

    public String getTransactionAccount(){ return transactionAccount; }

    public String getMerchantType(){ return merchantType; }

    public String getPayVia(){ return payVia; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getMerchantCategory(){ return merchantCategory; }

    public String getHostId(){ return hostId; }

    public String getTransactionType(){ return transactionType; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public String getIpCity(){ return ipCity; }

    public String getObdxTransactionName(){ return obdxTransactionName; }

    public String getUserId(){ return userId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getObdxModuleName(){ return obdxModuleName; }

    public String getRef8(){ return ref8; }

    public String getRef7(){ return ref7; }

    public Double getAccountBalance(){ return accountBalance; }

    public String getRef9(){ return ref9; }

    public String getRef4(){ return ref4; }

    public String getRef3(){ return ref3; }

    public String getRef6(){ return ref6; }

    public String getRef5(){ return ref5; }

    public String getMerchantId(){ return merchantId; }

    public String getSessionId(){ return sessionId; }
    public String getRiskBand(){ return riskBand; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setTransactionAmount(Double val){ this.transactionAmount = val; }
    public void setRef11(String val){ this.ref11 = val; }
    public void setRef10(String val){ this.ref10 = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setRef2(String val){ this.ref2 = val; }
    public void setRef1(String val){ this.ref1 = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setTransactionAccount(String val){ this.transactionAccount = val; }
    public void setMerchantType(String val){ this.merchantType = val; }
    public void setPayVia(String val){ this.payVia = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setMerchantCategory(String val){ this.merchantCategory = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTransactionType(String val){ this.transactionType = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setObdxTransactionName(String val){ this.obdxTransactionName = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setObdxModuleName(String val){ this.obdxModuleName = val; }
    public void setRef8(String val){ this.ref8 = val; }
    public void setRef7(String val){ this.ref7 = val; }
    public void setAccountBalance(Double val){ this.accountBalance = val; }
    public void setRef9(String val){ this.ref9 = val; }
    public void setRef4(String val){ this.ref4 = val; }
    public void setRef3(String val){ this.ref3 = val; }
    public void setRef6(String val){ this.ref6 = val; }
    public void setRef5(String val){ this.ref5 = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setSessionId(String val){ this.sessionId = val; }
    public void setRiskBand(String val){ this.riskBand = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_EpiEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.transactionAccount);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Epi");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Epi");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}