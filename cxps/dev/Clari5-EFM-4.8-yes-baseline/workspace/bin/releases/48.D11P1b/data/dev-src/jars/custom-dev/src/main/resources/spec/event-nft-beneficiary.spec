cxps.events.event.nft-beneficiary{
  table-name : EVENT_NFT_BENEFICIARY
  event-mnemonic: BE
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        beneficiary-name: {db : true ,raw_name : beneficiary_name ,type: "string:200" }
        benf-status: {db : true ,raw_name : benf_status ,type: "string:200" }
        group-payee-id: {db : true ,raw_name : group_payee_id ,type: "string:200" }
        benf-nickname1: {db : true ,raw_name : benf_nickname1 ,type: "string:200" }
        benf-type1: {db : true ,raw_name : benf_type1 ,type: "string:200" }
        benf-unique-value1: {db : true ,raw_name : benf_unique_value1 ,type: "string:200" }
        benf-flag1: {db : true ,raw_name : benf_flag1 ,type: "string:200" }
        benf-payee-id1: {db : true ,raw_name : benf_payee_id1 ,type: "string:200" }
        payee-accttype1: {db : true ,raw_name : payee_accttype1 ,type: "string:200" }
        benf-nickname2: {db : true ,raw_name : benf_nickname2 ,type: "string:200" }
        benf-type2: {db : true ,raw_name : benf_type2 ,type: "string:200" }
        benf-unique-value2: {db : true ,raw_name : benf_unique_value2 ,type: "string:200" }
        benf-flag2: {db : true ,raw_name : benf_flag2 ,type: "string:200" }
        benf-payee-id2: {db : true ,raw_name : benf_payee_id2 ,type: "string:200" }
        payee-accttype2: {db : true ,raw_name : payee_accttype2 ,type: "string:200" }
        benf-nickname3: {db : true ,raw_name : benf_nickname3 ,type: "string:200" }
        benf-type3: {db : true ,raw_name : benf_type3 ,type: "string:200" }
        benf-unique-value3: {db : true ,raw_name : benf_unique_value3 ,type: "string:200" }
        benf-flag3: {db : true ,raw_name : benf_flag3 ,type: "string:200" }
        benf-payee-id3: {db : true ,raw_name : benf_payee_id3 ,type: "string:200" }
        payee-accttype3: {db : true ,raw_name : payee_accttype3 ,type: "string:200" }
        benf-nickname4: {db : true ,raw_name : benf_nickname4 ,type: "string:200" }
        benf-type4: {db : true ,raw_name : benf_type4 ,type: "string:200" }
        benf-unique-value4: {db : true ,raw_name : benf_unique_value4 ,type: "string:200" }
        benf-flag4: {db : true ,raw_name : benf_flag4 ,type: "string:200" }
        benf-payee-id4: {db : true ,raw_name : benf_payee_id4 ,type: "string:200" }
        payee-accttype4: {db : true ,raw_name : payee_accttype4 ,type: "string:200" }
        benf-nickname5: {db : true ,raw_name : benf_nickname5 ,type: "string:200" }
        benf-type5: {db : true ,raw_name : benf_type5 ,type: "string:200" }
        benf-unique-value5: {db : true ,raw_name : benf_unique_value5 ,type: "string:200" }
        benf-flag5: {db : true ,raw_name : benf_flag5 ,type: "string:200" }
        benf-payee-id5: {db : true ,raw_name : benf_payee_id5 ,type: "string:200" }
        payee-accttype5: {db : true ,raw_name : payee_accttype5 ,type: "string:200" }
        benf-nickname6: {db : true ,raw_name : benf_nickname6 ,type: "string:200" }
        benf-type6: {db : true ,raw_name : benf_type6 ,type: "string:200" }
        benf-unique-value6: {db : true ,raw_name : benf_unique_value6 ,type: "string:200" }
        benf-flag6: {db : true ,raw_name : benf_flag6 ,type: "string:200" }
        benf-payee-id6: {db : true ,raw_name : benf_payee_id6 ,type: "string:200" }
        payee-accttype6: {db : true ,raw_name : payee_accttype6 ,type: "string:200" }
        benf-nickname7: {db : true ,raw_name : benf_nickname7 ,type: "string:200" }
        benf-type7: {db : true ,raw_name : benf_type7 ,type: "string:200" }
        benf-unique-value7: {db : true ,raw_name : benf_unique_value7 ,type: "string:200" }
        benf-flag7: {db : true ,raw_name : benf_flag7 ,type: "string:200" }
        benf-payee-id7: {db : true ,raw_name : benf_payee_id7 ,type: "string:200" }
        payee-accttype7: {db : true ,raw_name : payee_accttype7 ,type: "string:200" }
        benf-nickname8: {db : true ,raw_name : benf_nickname8 ,type: "string:200" }
        benf-type8: {db : true ,raw_name : benf_type8 ,type: "string:200" }
        benf-unique-value8: {db : true ,raw_name : benf_unique_value8 ,type: "string:200" }
        benf-flag8: {db : true ,raw_name : benf_flag8 ,type: "string:200" }
        benf-payee-id8: {db : true ,raw_name : benf_payee_id8 ,type: "string:200" }
        payee-accttype8: {db : true ,raw_name : payee_accttype8 ,type: "string:200" }
        benf-nickname9: {db : true ,raw_name : benf_nickname9 ,type: "string:200" }
        benf-type9: {db : true ,raw_name : benf_type_9 ,type: "string:200" }
        benf-unique-value9: {db : true ,raw_name : benf_unique_value9 ,type: "string:200" }
        benf-flag9: {db : true ,raw_name : benf_flag9 ,type: "string:200" }
        benf-payee-id9: {db : true ,raw_name : benf_payee_id9 ,type: "string:200" }
        payee-accttype9: {db : true ,raw_name : payee_accttype9 ,type: "string:200" }
        benf-nickname10: {db : true ,raw_name : benf_nickname10 ,type: "string:200" }
        benf-type10: {db : true ,raw_name : benf_type10 ,type: "string:200" }
        benf-unique-value10: {db : true ,raw_name : benf_unique_value10 ,type: "string:200" }
        benf-flag10: {db : true ,raw_name : benf_flag10 ,type: "string:200" }
        benf-payee-id10: {db : true ,raw_name : benf_payee_id10 ,type: "string:200" }
        payee-accttype10: {db : true ,raw_name : payee_accttype10 ,type: "string:200" }
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
	session-id:{db : true ,raw_name :session_id ,type : "string:200"}
	risk-band:{db : true ,raw_name :risk_band ,type : "string:200", derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}
        benf-addition:{db : true ,raw_name :benf_addition ,type : "string:20", derivation :"""cxps.events.CustomFieldDerivator.SaveBeneficiaryDb(this)"""}
}
}
