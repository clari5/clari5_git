clari5.custom.mapper {
        entity {
                NFT_CHEQRETURN {
                       generate = true
                        attributes:[
                                { name: EVENT_ID, type="string:50" , key=true },
                                { name: BRANCHID ,type ="string:50" }
                                { name: CHEQUE_NUMBER ,type ="string:50" }
				{ name: BRANCHIDDESC ,type ="string:50" }
                                { name: EVENTTS ,type =timestamp }
				{ name: REJECTION_CODE ,type ="string:50" }
                                { name: HOST_ID ,type ="string:50" }
				{ name: ACCT_ID ,type ="string:50" }
                                { name: USER_ID ,type ="string:50" }
				{ name: ACCT_STATUS ,type ="string:50" }
                                { name: TRANSACTION_NUMONIC_CODE ,type ="string:50" }
				{ name: REJECTION_REASON ,type ="string:50" }
                                { name: SYS_TIME ,type =timestamp }
				{ name: ACCT_NAME ,type ="string:50" }
                                { name: CUST_ID ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:400" ,default="NEW" }
				{ name: SERVER_ID ,type ="number" }
                                ]
                        }
        }
}
