clari5.custom.mapper {
        entity {
                NFT_DEDUPE {
                       generate = true
                        attributes:[
                                { name: EVENT_ID , type="string:50",key =true},
                                { name: HOST_ID ,type ="string:50"  }
                                { name: COMPLETEMAILINGADDR ,type ="string:100" }
				{ name: EMAILID ,type ="string:50" }
				{ name: PAN ,type ="string:12" }
				{ name: PASSPORTNUMBER ,type ="string:20" }
				{ name: UCICID ,type ="string:50" }
				{ name: ACCOUNTID ,type ="string:50" }
				{ name: COMPLETEPERMANENTADDR ,type ="string:200" }
				{ name: DOB ,type ="date" }
				{ name: PERMANENTADDR1 ,type ="string:200" }
				{ name: PERMANENTADDR2 ,type ="string:200" }
				{ name: OFFICELANDLINENUMBER ,type ="string:50" }
				{ name: PERMANENTADDR3 ,type ="string:200" }
				{ name: DOI ,type ="date" }
				{ name: COMPLETEMAILINGADDRCHANGED ,type ="string:50" }	
				{ name: ACCOUNTCOUNT ,type ="string:50" }
				{ name: LANDLINENUMBER ,type ="string:50" }
				{ name: PRODUCT_CODE ,type ="string:50" }
				{ name: MAILINGADDR1 ,type ="string:50" } 
				{ name: MAILINGADDR2 ,type ="string:50" }
				{ name: MOBNUMBER ,type ="string:50" }
				{ name: MAILINGADDR3 ,type ="string:50" }
				{ name: CUSTID ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: HOMELANDLINENUMBER ,type ="string:50" }
				{ name: COMPLETEPERADDRCHANGED ,type ="string:200" }
				{ name: CUSTNAME ,type ="string:50" }
				{ name: HANDPHONE ,type ="string:50" }
				{ name: ENTITY_TYPE ,type ="string:50" }
				{ name: NAMECHANGED ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:4000" , default="NEW" }
				{ name: SERVER_ID ,type ="number" }
                                ]
                        }
        }
}

