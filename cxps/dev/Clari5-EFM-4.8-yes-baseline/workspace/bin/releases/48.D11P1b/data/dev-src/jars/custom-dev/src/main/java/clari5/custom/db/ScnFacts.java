package clari5.custom.db;

public class ScnFacts {
    private String scnName;
    private String workspaceName;
    private String content;

    public String getScnName() {
        return scnName;
    }

    public void setScnName(String scnName) {
        this.scnName = scnName;
    }

    public String getWorkspaceName() {
        return workspaceName;
    }

    public void setWorkspaceName(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ScnFacts{" +
                "scnName='" + scnName + '\'' +
                ", workspaceName='" + workspaceName + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

}
