cxps.events.event.nft-dedup{
  table-name : EVENT_NFT_DEDUP
  event-mnemonic: ND
  workspaces : {
    CUSTOMER: custId
  }
  event-attributes : {
	ucic-id: {db : true ,raw_name : ucicId ,type : "string:200"}
	cust-id: {db : true ,raw_name : custId ,type : "string:200"}
	cust-name: {db : true ,raw_name : custName ,type : "string:200"}
	mob-number: {db : true ,raw_name : mobNumber ,type : "string:200"}
	landline-number: {db : true ,raw_name : landlineNumber ,type : "string:200"}
	home-landline-number: {db : true ,raw_name : homeLandlineNumber ,type : "string:200"}
	office-landline-number: {db : true ,raw_name : officeLandlineNumber ,type : "string:200"}
	email-id: {db : true ,raw_name : emailId ,type : "string:200"}
	permanent-addr1: {db : true ,raw_name : permanentAddr1 ,type : "string:200"}
	permanent-addr2: {db : true ,raw_name : permanentAddr2 ,type : "string:200"}
	permanent-addr3: {db : true ,raw_name : permanentAddr3 ,type : "string:200"}
	mailing-addr1: {db : true ,raw_name : mailingAddr1 ,type : "string:200"}
	mailing-addr2: {db : true ,raw_name : mailingAddr2 ,type : "string:200"}
	mailing-addr3: {db : true ,raw_name : mailingAddr3 ,type : "string:200"}
	pan: {db : true ,raw_name : pan ,type : "string:200"}
	passport-number: {db : true ,raw_name : passportNumber ,type : "string:200"}
	doi: {db : true ,raw_name : doi ,type : timestamp}
	dob: {db : true ,raw_name : dob ,type : timestamp}
	hand-phone: {db : true ,raw_name : handPhone ,type : "string:200"}
	complete-permanent-addr: {db : true ,raw_name : completePermanentAddr ,type : "string:200"}
	complete-mailing-addr: {db : true ,raw_name : completeMailingAddr ,type : "string:200"}
	name-changed: {db : true ,raw_name : nameChanged ,type : "string:200"}
	complete-per-addr-changed: {db : true ,raw_name : completePerAddrChanged ,type : "string:200"}
	complete-mailing-addr-changed: {db : true ,raw_name : completeMailingAddrChanged ,type : "string:200"}
	score: {db : true ,raw_name : score ,type : "number:11,2"}
	score-percentage: {db : true ,raw_name : scorePercentage ,type : "number:11,2"}
	name-match-score: {db : true ,raw_name : nameMatchScore ,type : "number:11,2"}
	permanant-addr-score: {db : true ,raw_name : permanantAddrScore ,type : "number:11,2"}
        brpermanant-addr-score : {db : true ,raw_name : brpermanantAddrScore ,type : "number:11,2"}
	mailing-addr-score: {db : true ,raw_name : mailingAddrScore ,type : "number:11,2"}
	brmailing-addr-score : {db : true,raw_name : brmailingAddrScore, type : "number:11,2"}
	account-id: {db : true ,raw_name : accountId ,type : "string:200"}
	product-code: {db : true ,raw_name : product_code ,type : "string:200", custom-getter:Product_code}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	host-id: {db : true ,raw_name : host_id ,type : "string:200"}
	entity-type: {db : true ,raw_name : entity_type ,type : "string:200"}
	mob-matched-count: {db : true ,raw_name : mobMatchedCount ,type : "number:7"}
	landline-matched-count: {db : true ,raw_name : landlineMatchedCount ,type : "number:7"}
	officephone-matched-count: {db : true ,raw_name : officephoneMatchedCount ,type : "number:7"}
	email-matched-count: {db : true ,raw_name : emailMatchedCount ,type : "number:7"}
	pan-matched-count: {db : true ,raw_name : panMatchedCount ,type : "number:7"}
	passport-matched-count: {db : true ,raw_name : passportMatchedCount ,type : "number:7"}
	mob-non-staff-matched-count: {db : true ,raw_name : mobNonStaffMatchedCount ,type : "number:7"}
	complete-per-addr-count: {db : true ,raw_name : completePerAddrCount ,type : "number:7"}
	complete-mailing-addr-count: {db : true ,raw_name : completeMailingAddrCount ,type : "number:7"}
	brcomplete-per-addr-count: {db : true ,raw_name : brcompletePerAddrCount ,type : "number:7"}
        brcomplete-mailing-addr-count: {db : true ,raw_name : brcompleteMailingAddrCount ,type : "number:7"}
	phone-count: {db : true ,raw_name : phoneCount ,type : "number:7"}
	mob-count: {db : true ,raw_name : mobCount ,type : "number:7"}
	intermediate-landline-count: {db : true ,raw_name : intermediateLandlineCount ,type : "number:7"}
	intermediateofficeland-count: {db : true ,raw_name : intermediateofficelandCount ,type : "number:7"}
	intermediatehandphone-count: {db : true ,raw_name : intermediatehandphoneCount ,type : "number:7"}
	dob-matched-count: {db : true ,raw_name : dobMatchedCount ,type : "number:7"}
	doi-matched-count: {db : true ,raw_name : doiMatchedCount ,type : "number:7"}
	account-count: {db : true ,raw_name : accountCount ,type : "number:7"}
        name-matchedlist:{db:true,raw_name:nameMatchedList,type:"string:200"}
        mailingaddr-matchedlist:{db:true,raw_name:mailingMatchedList,type:"string:8000"}
        permanentaddr-matchedlist:{db:true,raw_name:permanentaddrMatchedList,type:"string:8000"}
        branchaddr-matchedlist:{db:true,raw_name:branchaddrMatchedList,type:"string:8000"}
	 mob-staff-cust-id:{db:true,raw_name:mob_staff_custId,type:"string:200"}
        mob-nonstaff-cust-id:{db:true,raw_name:mob_nonstaff_custId,type:"string:200"}
        mob-cust-id:{db:true,raw_name:mob_custId,type:"string:200"}
        landline-cust-id:{db:true,raw_name:landline_custId,type:"string:200"}
        email-cust-id:{db:true,raw_name:email_custId,type:"string:200"}
        pan-cust-id:{db:true,raw_name:pan_custId,type:"string:200"}
        passport-cust-id:{db:true,raw_name:passport_custId,type:"string:200"}
        dob-cust-id:{db:true,raw_name:dob_custId,type:"string:200"}
        doi-cust-id:{db:true,raw_name:doi_custId,type:"string:200"}
        match-phone-cust-id:{db:true,raw_name:match_phone_custId,type:"string:200"}
        office-phone-cust-id:{db:true,raw_name:office_phone_custId,type:"string:200"}
        is-account-closed : {db:true, type:"string:10",derivation : """cxps.events.CustomFieldDerivator.getIsAccountClosed(this)"""}
	}
}
