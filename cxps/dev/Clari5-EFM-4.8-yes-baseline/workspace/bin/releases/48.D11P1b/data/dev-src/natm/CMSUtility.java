package clari5.aml.cms.newjira;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.jira.display.IEvidenceFormatter;
import clari5.aml.cms.jira.util.IssueBuilder;
import clari5.cmssync.Queries;
import clari5.hfdb.CmsFeedback;
import clari5.hfdb.CmsFeedbackKey;
import clari5.hfdb.Hfdb;
import clari5.hfdb.HostEvent;
import clari5.hfdb.mappers.CmsFeedBackCustomMapper;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.QueryMapper;
import clari5.platform.jira.JiraClientException;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import clari5.trace.ConType;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.TracerUtil;
import clari5.trace.mappers.EventMapper;
import clari5.trace.wsrel.WsRelationUtil;
import cxps.apex.shared.IWSEvent;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.IncidentEvent;
import cxps.noesis.utils.AmlCaseEvent;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bisman on 12/19/16.
 */
public class CMSUtility {
    protected static CxpsLogger logger = CxpsLogger.getLogger(CMSUtility.class);

    protected CmsModule cmsMod;
    protected JiraClient jiraClient;
    protected ICmsFormatter formatter;
    protected String nextUser;
    protected static Map<String, Map<String, CustomField>> caseToJiraMap = new ConcurrentHashMap<>();
    protected static Map<String, Map<String, CustomField>> jiraToCaseMap = new ConcurrentHashMap<>();
    protected static final String NA = "Not Available";
    private static Map<String, CMSUtility> moduleMap = new HashMap<>();
    private static final String DN_PLACEHOLDER = "https://dn.placeholder.com:1111/";

    private CMSUtility() {
    }

    public static CMSUtility getInstance(String module) throws CmsException.Configuration {
        CMSUtility c = moduleMap.get(module);
        if (c == null) {
            synchronized (CMSUtility.class) {
                c = moduleMap.get(module);
                if (c == null) {
                    c = new CMSUtility();
                    c.bootstrap(module);
                    moduleMap.put(module, c);
                }
            }
        }
        return c;
    }

    private void bootstrap(String moduleId) throws CmsException.Configuration {
        CmsJira cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
        fillStaticData(cmsJira, moduleId);
        cmsMod = (CmsModule) cmsJira.get(moduleId);
        JiraInstance jiraInstance = cmsMod.getInstance();
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        formatter = null;
        try {
            Class<?> assignmentClass = Class.forName(cmsJira.getCmsFormatter());
            Object obj = assignmentClass.newInstance();
            formatter = (ICmsFormatter) obj;
        } catch (Exception e) {
            logger.fatal("CmsUtility class not available", e);
            throw new CmsException.Configuration("Cant create instance of CMSFormatter: " + cmsJira.getCmsFormatter() + ":" + e.getMessage());
        }
    }


    public CxJson populateCaseJson(IncidentEvent event) throws CmsException {
        ICXLog icxLog = CXLog.fenter("CMSUtility.populateCaseJson");
        String eventJson = event.convertToJson(event);
        Hocon h = new Hocon(eventJson);
        logger.debug(" Processing JSON  : " + eventJson);
        icxLog.ckpt("case event json " + eventJson);

        String case_issueTypeId = String.valueOf(jiraClient.getInstanceParams().getTaskTypeId());

        nextUser = formatter.getCaseAssignee(event);

        CxJson case_fieldsJson = new CxJson();
        CxJson projectJson = new CxJson();

        projectJson.put("key", event.getProject());
        case_fieldsJson.put("project", projectJson);
        CxJson case_issueTypeJson = new CxJson();
        case_issueTypeJson.put("id", case_issueTypeId);
        case_fieldsJson.put("issuetype", case_issueTypeJson);
        if (nextUser != null && !"".equals(nextUser)) {
            CxJson assigneeJson = new CxJson();
            assigneeJson.put("name", nextUser);
            case_fieldsJson.put("assignee", assigneeJson);
        }

        String jiraId_EntityName = cmsMod.getCustomFields().get("CaseEvent-entityName").getJiraId();
        case_fieldsJson.put(jiraId_EntityName, h.getString("caseEntityName"));

        String jiraId_caseEntityId = cmsMod.getCustomFields().get("CaseEvent-displayKey").getJiraId();
        case_fieldsJson.put(jiraId_caseEntityId, getFormattedCaseEntityId(h));

        case_fieldsJson.put("summary", h.getString("caseDisplayKey", null));

        String jiraId_case_score = cmsMod.getCustomFields().get("CaseEvent-score").getJiraId();
        case_fieldsJson.put(jiraId_case_score, h.getString("caseScore"));

        if (h.getKeys().contains("noOfStr")) {
            if (h.getString("noOfStr", null) != null) {
                String jiraId_noOfStr = cmsMod.getCustomFields().get("CaseEvent-noOfStr").getJiraId();
                case_fieldsJson.put(jiraId_noOfStr, h.getString("noOfStr"));
            }

            if (h.getString("lastFiledStr", null) != null) {
                String jiraId_lastFiledStr = cmsMod.getCustomFields().get("CaseEvent-lastFiledStr").getJiraId();
                case_fieldsJson.put(jiraId_lastFiledStr, h.getString("lastFiledStr"));
            }
        }

        if (h.getString("description", null) != null) {
            case_fieldsJson.put("description", h.getString("description").replaceAll("###", "\r\n"));
        }

        CxJson prevCase_fieldsJson = case_fieldsJson;
        addMissingFields(case_fieldsJson, "case", h.getString("moduleId"));
        case_fieldsJson = appendCustomFields(prevCase_fieldsJson, "case", case_fieldsJson, h.getString("moduleId"));
        case_fieldsJson = getRequestFormat(case_fieldsJson);
        icxLog.ckpt("Populated case json " + case_fieldsJson);
        icxLog.fexit();
        return case_fieldsJson;
    }

    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException {
        ICXLog icxLog = CXLog.fenter("CMSUtility.populateIncidentJson");
        String eventJson = event.convertToJson(event);
        // Creation of Incidents
        Hocon h = new Hocon(eventJson);

        if (isupdate) {
            CxJson updateJson = populateUpdateJson(event);
            return updateJson;
        }

        String incidentIssueTypeId = String.valueOf(jiraClient.getInstanceParams().getSubtaskTypeId());
        nextUser = formatter.getIncidentAssignee(event);

        CxJson incident_fieldsJson = new CxJson();

        CxJson projectJson = new CxJson();
        projectJson.put("key", event.getProject());

        CxJson caseIdJson = new CxJson();
        caseIdJson.put("key", jiraParentId);

        incident_fieldsJson.put("parent", caseIdJson);
        incident_fieldsJson.put("project", projectJson);
        CxJson incident_issueTypeJson = new CxJson();
        incident_issueTypeJson.put("id", incidentIssueTypeId);
        incident_fieldsJson.put("issuetype", incident_issueTypeJson);

        if (nextUser != null && !"".equals(nextUser)) {
            CxJson assigneeJson = new CxJson();
            assigneeJson.put("name", nextUser);
            incident_fieldsJson.put("assignee", assigneeJson);
        }

        if (h.getString("description", null) != null) {
            incident_fieldsJson.put("description", h.getString("description").replaceAll("###", "\r\n"));
        }

        if (h.getString("summary", null) != null) {
            String jiraId_message = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();
            if (h.getString("message", null) != null) {
                incident_fieldsJson.put(jiraId_message, h.getString("message"));
            }
            incident_fieldsJson.put("summary", h.getString("displayKey") + ":" + checkSummaryLength(h.getString("summary")));
        } else {
            incident_fieldsJson.put("summary", h.getString("displayKey"));
        }


        if (h.get("evidence") != null) {
            CxJson evd;
            try {
                evd = CxJson.parse(h.get("evidence").getJSON());
            } catch (Exception e) {
                // This exception is never expected to come.
                logger.error("Cmsincident parsing failed", e.getStackTrace().toString());
                throw new CmsException(e.getStackTrace().toString());
            }

            CxJson value = null;

            if (evd.hasKey("evidenceData")) {
                value = evd.get("evidenceData");
            } else {
                value = evd;
            }

            String evidence = getEvidencesData(value ,event);
            String jiraId_evidence = cmsMod.getCustomFields().get("CmsIncident-evidence").getJiraId();
            incident_fieldsJson.put(jiraId_evidence, evidence);

            incident_fieldsJson.put(cmsMod.getCustomFields().get("CmsIncident-evidence-details").getJiraId(), DN_PLACEHOLDER + "efm/cef/index.html");
        }


        String jiraId_factname = cmsMod.getCustomFields().get("CmsIncident-factName").getJiraId();
        incident_fieldsJson.put(jiraId_factname, h.getString("factname"));

        String jiraId_triggerId = cmsMod.getCustomFields().get("trigger_Id").getJiraId();
        incident_fieldsJson.put(jiraId_triggerId, h.getString("trigger_Id"));

        String jiraId_caseEntityName = cmsMod.getCustomFields().get("CaseEvent-entityName").getJiraId();
        incident_fieldsJson.put(jiraId_caseEntityName, h.getString("caseEntityName"));

        String jiraId_caseEntityId = cmsMod.getCustomFields().get("CaseEvent-displayKey").getJiraId();
        incident_fieldsJson.put(jiraId_caseEntityId, getFormattedCaseEntityId(h));

        String jiraId_entityIdType = cmsMod.getCustomFields().get("CmsIncident-entityId").getJiraId();
        incident_fieldsJson.put(jiraId_entityIdType, h.getString("entityId"));

        String jiraId_incidentEntityName = cmsMod.getCustomFields().get("CmsIncident-entityName").getJiraId();
        incident_fieldsJson.put(jiraId_incidentEntityName, h.getString("entityName"));

        String jiraId_dateTime = cmsMod.getCustomFields().get("CmsIncident-timestamp").getJiraId();
        incident_fieldsJson.put(jiraId_dateTime, new Timestamp(h.getLong("date_time")).toString());

        if (h.getString("incidentScore", null) != null) {
            String jiraId_incident_score = cmsMod.getCustomFields().get("CmsIncident-score").getJiraId();
            incident_fieldsJson.put(jiraId_incident_score, h.getString("incidentScore"));
        }

        String jiraId_displayKey = cmsMod.getCustomFields().get("CmsIncident-displayKey").getJiraId();
        incident_fieldsJson.put(jiraId_displayKey, getFormattedIncidentEntityId(h));

        String jiraId_risklevel = cmsMod.getCustomFields().get("CmsIncident-riskLevel").getJiraId();
        incident_fieldsJson.put(jiraId_risklevel, h.getString("riskLevel"));

        if (h.getKeys().contains("noOfStr")) {
            if (h.getString("noOfStr", null) != null) {
                String jiraId_noOfStr = cmsMod.getCustomFields().get("CmsIncident-numOfStr").getJiraId();
                incident_fieldsJson.put(jiraId_noOfStr, h.getString("noOfStr"));
            }

            if (h.getString("lastFiledStr", null) != null) {
                String jiraId_lastFiledStr = cmsMod.getCustomFields().get("CmsIncident-lastFiledStr").getJiraId();
                incident_fieldsJson.put(jiraId_lastFiledStr, h.getString("lastFiledStr"));
            }
        }

        if (h.getKeys().contains("merchant") && h.getString("merchant", null) != null) {
            String jiraId_merchant = cmsMod.getCustomFields().get("merchant").getJiraId();
            incident_fieldsJson.put(jiraId_merchant, h.getString("merchant"));
        }

        if (h.getKeys().contains("operator") && h.getString("operator", null) != null) {
            String jiraId_operator = cmsMod.getCustomFields().get("operator").getJiraId();
            incident_fieldsJson.put(jiraId_operator, h.getString("operator"));
        }

        if (h.getKeys().contains("device") && h.getString("device", null) != null) {
            String jiraId_device = cmsMod.getCustomFields().get("device").getJiraId();
            incident_fieldsJson.put(jiraId_device, h.getString("device"));
        }

        if (h.getKeys().contains("branch") && h.getString("branch", null) != null) {
            String jiraId_branch = cmsMod.getCustomFields().get("CmsIncident-branch").getJiraId();
            incident_fieldsJson.put(jiraId_branch, h.getString("branch"));
        }


        CxJson eventCxJson = CxJson.parse(eventJson);
        if (h.getKeys().contains("cmsSuspicionDetails") && h.get("cmsSuspicionDetails") != null && !eventCxJson.get("cmsSuspicionDetails").equals(null)) {
            String json = h.get("cmsSuspicionDetails").getJSON();

            CxJson cxJson = CxJson.parse(json);
            CxJson GroundsOfSuspicion = cxJson.get("groundsOfSuspicion");

            String jiraId_mainPersonName = cmsMod.getCustomFields().get("mainPersonName").getJiraId();
            incident_fieldsJson.put(jiraId_mainPersonName, cxJson.getString("mainPersonName", ""));

            Map<String, clari5.aml.cms.newjira.CustomField> reverseMap = caseToJiraMap.get(h.getString("moduleId"));

            for (Map.Entry<String, clari5.aml.cms.newjira.CustomField> entry : reverseMap.entrySet()) {
                CxJson dropdownFieldJson = new CxJson();
                clari5.aml.cms.newjira.CustomField customField = entry.getValue();
                customField.setCaseFieldName(entry.getKey());
                String fieldName = customField.getCaseFieldName();
                String fieldType = customField.getJiraType();
                String jiraId = customField.getJiraId();
                if (fieldType.startsWith("dropdown")) {
                    String res = cxJson.getString(fieldName, "");
                    if (res.isEmpty() || res.equals("")) continue;
                    String[] refCategory = fieldType.split("-", 3);
                    if (refCategory.length == 3) {
                        String codeVal = Hfdb.getRefCodeVal(refCategory[2], res);
                        if (codeVal != null && !"".equals(codeVal)) {
                            res = codeVal;
                        }
                    }
                    dropdownFieldJson.put("value", res);
                    incident_fieldsJson.put(jiraId, dropdownFieldJson);
                }

                if (fieldType.equals("string") && !fieldName.startsWith("CmsIncident") && !fieldName.startsWith("CaseEvent") && !fieldName.startsWith("CmsResolution")) {
                    if (cxJson.hasKey(fieldName) && !cxJson.getString(fieldName, "").equals(""))
                        incident_fieldsJson.put(jiraId, cxJson.getString(fieldName, ""));
                    else if (GroundsOfSuspicion.hasKey(fieldName) && !GroundsOfSuspicion.get(fieldName).equals(""))
                        incident_fieldsJson.put(jiraId, GroundsOfSuspicion.get(fieldName).toString());
                }
            }
            if (cxJson.hasKey("accountsDetails")) {
                Map<String, Set<String>> allTrans = new HashMap<>();
                CxJson jsonElement = CxJson.parse(cxJson.getString("accountsDetails", ""));
                Map<String, Set<String>> transMap = getTransMap(jsonElement);
                if (transMap != null && transMap.size() > 0) {
                    if (allTrans.size() == 0) {
                        allTrans = transMap;
                    } else {
                        allTrans = mergeTrans(allTrans, transMap);
                    }
                }
                if (allTrans != null && allTrans.size() > 0) {
                    String jiraId_accountsDetails = cmsMod.getCustomFields().get("accountsDetails").getJiraId();
                    String transactionValue = getTransactions(allTrans);
                    incident_fieldsJson.put(jiraId_accountsDetails, transactionValue);
                }
            }
        }


        if (cmsMod.getCustomFields().containsKey("accountsDetails") && h.get("evidence") != null) {
            String jiraId_accountsDetails = cmsMod.getCustomFields().get("accountsDetails").getJiraId();
            if (!incident_fieldsJson.hasKey(jiraId_accountsDetails) && !h.getString("eventname").equals("MSTR")) {
                Map<String, Set<String>> acctMap = populateAccountAndTransactionDetails(h.get("evidence").getJSON());
                if (acctMap != null && acctMap.size() > 0) {
                    String transactionValue = getTransactions(acctMap);
                    incident_fieldsJson.put(jiraId_accountsDetails, transactionValue);
                }
            }
        }

        CxJson prevIncident_fieldsJson = incident_fieldsJson;
        addMissingFields(incident_fieldsJson, "cmsIncidents", h.getString("moduleId"));
        incident_fieldsJson = appendCustomFields(prevIncident_fieldsJson, "cmsIncidents", incident_fieldsJson, h.getString("moduleId"));
        incident_fieldsJson = getRequestFormat(incident_fieldsJson);
        icxLog.ckpt("Populated Incident json " + incident_fieldsJson);
        icxLog.fexit();
        return incident_fieldsJson;
    }


    private String checkSummaryLength(String summary) {
        if (summary.length() > 240) {
            summary = summary.substring(0, 240) + "...";
            summary = summary.replace("\\", "");                   // Summary doesn't allow escaped characters.
        }
        return summary;
    }

    public CxJson populateUpdateJson(IncidentEvent event) throws CmsException {

        ICXLog icxLog = CXLog.fenter("CMSUtility.populateUpdateJson");

        String eventJson = event.convertToJson(event);
        // Creation of Incidents
        Hocon h = new Hocon(eventJson);

        CxJson updateJson = new CxJson();

        try {
            String score = h.getString("incidentScore");
            String riskLevel = h.getString("riskLevel");
            String evidence = null;

            if (h.get("evidence") != null) {
                evidence = h.get("evidence").getJSON();
            }

            // If cumulative sum has to be displayed in Jira here corresponding
            // fields need to be added.
            if (score != null && !"".equals(score)) {
                String customId = cmsMod.getCustomFields().get("CmsIncident-score").getJiraId();
                updateJson.put(customId, score);
            }
            if (riskLevel != null && !"".equals(riskLevel)) {
                String customId = cmsMod.getCustomFields().get("CmsIncident-riskLevel").getJiraId();
                updateJson.put(customId, riskLevel);
            }
            if (evidence != null && !"".equals(evidence)) {
                CxJson evd;
                try {
                    evd = CxJson.parse(evidence);
                } catch (Exception e) {
                    // This exception is never expected to come.
                    logger.error("Cmsincident parsing failed", e);
                    throw new CmsException(e);
                }

                CxJson value = evd.get("evidenceData", evd);
                String updateEvidence = getEvidencesData(value,event);
                String jiraId_evidence = cmsMod.getCustomFields().get("CmsIncident-evidence").getJiraId();
                updateJson.put(jiraId_evidence, updateEvidence);

                updateJson.put(cmsMod.getCustomFields().get("CmsIncident-evidence-details").getJiraId(), DN_PLACEHOLDER + "efm/cef/index.html");
            }

        } catch (Exception e) {
            throw new CmsException(e.getMessage());
        }
        icxLog.ckpt("Populated Incident update json " + updateJson);
        icxLog.fexit();
        return updateJson;
    }

    public CxJson formatClosureJson(CxJson resJson, CxJson eventJson, String moduleId) throws CmsException.Configuration {

        ICXLog icxLog = CXLog.fenter("CMSUtility.formatClosureJson");
        CxJson caseJson = eventJson.get("customFields");
        fillStaticData(null, moduleId);
        Map<String, CustomField> reverseMap = jiraToCaseMap.get(moduleId);

        CxJson suspJson = new CxJson();
        CxJson caseValueJson = null;
        CxJson pathJson = null;
        CxJson cmsAccDetails = null;

        for (Map.Entry<String, CustomField> entry : reverseMap.entrySet()) {
            if (!caseJson.hasKey(entry.getKey())) {
                continue;
            }

            CustomField customField = entry.getValue();
            JiraConstants enumValue = JiraConstants.getEnum(customField.getCaseFieldName().replace('-', '_').toUpperCase());

            String fieldName = customField.getCaseFieldName();
            String fieldType = customField.getJiraType();
            String caseValue = caseJson.getString(entry.getKey(), "");
            if (enumValue.getValue().equals("unknown")) {
                try {
                    switch (fieldName) {
                        case "accountsDetails":
                            caseValueJson = getCaseCustomValue(fieldName, fieldType, caseValue);
                            cmsAccDetails = CxJson.parse(caseValueJson.get(fieldName).toString());
                            pathJson = getPathJson(fieldName, cmsAccDetails);
                            suspJson = IssueBuilder.mergeJsons(suspJson, pathJson);
                            break;
                        case "alertIndicator":
                            caseValueJson = getCaseCustomValue(fieldName, fieldType, caseValue);
                            suspJson = IssueBuilder.mergeJsons(suspJson, caseValueJson);
                            break;
                        default:
                            suspJson.put(customField.getCaseFieldName(), caseJson.getString(entry.getKey(), ""));
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        suspJson = CxJson.parse(caseValueJson.toString());
                    } catch (IOException e1) {
                    }
                }
                continue;
            }

            String value = enumValue.getValue();
            if (resJson.hasKey(value)) {
                continue;
            }
            if (fieldType.equalsIgnoreCase("dateTime") && !caseJson.getString(entry.getKey(), "").equals("0")) {
                resJson.put(value, java.sql.Timestamp.valueOf(caseJson.getString(entry.getKey(), "")).getTime());
            } else
                resJson.put(value, caseJson.getString(entry.getKey(), ""));
        }

        resJson.put("unknown", suspJson);
        icxLog.ckpt("Forming case closure json" + resJson);
        icxLog.fexit();
        return resJson;

    }

    public JiraClient getJiraInstance() {
        return jiraClient;
    }

    private Map<String, Set<String>> getTransMap(CxJson value) {
        Map<String, Set<String>> transMap = new HashMap<>();
        try {
            if (value != null) {
                Iterator<CxJson> accounts = value.iterator();
                for (Iterator<CxJson> it1 = accounts; it1.hasNext(); ) {
                    CxJson accountJson = it1.next();
                    String accountId = accountJson.getString("accountId", "");
                    if (accountId != null) {
                        Set<String> trans = new HashSet<>();
                        CxJson jsonElement = accountJson.get("transactions");
                        if (jsonElement != null) {
                            Iterator<CxJson> transactionsArray = jsonElement.iterator();
                            if (transactionsArray != null) {

                                for (Iterator<CxJson> it = transactionsArray; it.hasNext(); ) {
                                    CxJson tr = it.next();
                                    String transId = tr.getString("transactionId", "");
                                    trans.add(transId);
                                }
                            }
                        }
                        if (trans.size() > 0) {
                            transMap.put(accountId, trans);
                        }
                    }
                }
            }
        } catch (Exception e) {
            return transMap;
        }
        return transMap;
    }

    private synchronized void fillStaticData(clari5.aml.cms.newjira.CmsJira cmsJira, String moduleId) throws CmsException.Configuration {

        if (jiraToCaseMap.containsKey(moduleId) && caseToJiraMap.containsKey(moduleId) &&
                caseToJiraMap.get(moduleId) != null && jiraToCaseMap.get(moduleId) != null) {
            return;
        }
        if (cmsJira == null) cmsJira = (clari5.aml.cms.newjira.CmsJira) Clari5.getResource("newcmsjira");

        clari5.aml.cms.newjira.CmsModule cmsModule = (clari5.aml.cms.newjira.CmsModule) cmsJira.get(moduleId);
        /* This can happen if we encounter a project code that is not listed as a
        department in the cmsjira modules config. In such cases either the project issues have to be
        deleted from jira or the project has to be added back to modules.departments
         */
        if (null == cmsModule) {
            throw new CmsException.Configuration("Cms config error : [" + moduleId + "] not bootstrapped. Please check clari5.modules.modules.[departments] configuration.");
        }
        if (!caseToJiraMap.containsKey(moduleId) || caseToJiraMap.get(moduleId) == null) {
            caseToJiraMap.put(moduleId, cmsModule.getCustomFields());
        }
        if (!jiraToCaseMap.containsKey(moduleId) || jiraToCaseMap.get(moduleId) == null) {
            Map<String, CustomField> reverseMap = new HashMap<>();
            for (Map.Entry<String, clari5.aml.cms.newjira.CustomField> entry : caseToJiraMap.get(moduleId).entrySet()) {
                clari5.aml.cms.newjira.CustomField customField = entry.getValue();
                customField.setCaseFieldName(entry.getKey());
                reverseMap.put(customField.getJiraId(), customField);
            }
            jiraToCaseMap.put(moduleId, reverseMap);
        }
    }

    private CxJson getRequestFormat(CxJson CxJson) {
        CxJson wrapperJson = new CxJson();
        wrapperJson.put("fields", CxJson);
        return wrapperJson;
    }

    private void addMissingFields(CxJson customFieldsJson, String taskType, String moduleId) {
        for (Map.Entry<String, clari5.aml.cms.newjira.CustomField> entry : jiraToCaseMap.get(moduleId).entrySet()) {
            clari5.aml.cms.newjira.CustomField customField = entry.getValue();
            if (!customFieldsJson.hasKey(entry.getKey())) {
                boolean isIgnorable = false;
                if (!customField.getTaskType().equals(taskType)) {
                    isIgnorable = true;
                }
                if (!isIgnorable) {
                    String jiraId = entry.getKey();
                    String jiraType = customField.getJiraType();
                    CxJson jiraJson = getJiraCustomValue(jiraId, jiraType);
                    if (jiraJson == null)
                        continue;
                    if (jiraJson.getString(jiraId).equalsIgnoreCase(NA)) {
                        customFieldsJson.put(jiraId, jiraJson.getString(jiraId));
                    } else
                        customFieldsJson.put(jiraId, jiraJson.get(jiraId));
                }
            }
        }
    }

    private CxJson appendCustomFields(CxJson caseJson, String caseOrIncident, CxJson taskJson, String moduleId) {
        clari5.aml.cms.newjira.CmsJira cmsJira = (clari5.aml.cms.newjira.CmsJira) Clari5.getResource("newcmsjira");
        CxJson newTaskjson;
        Map<String, String> newFieldsMap;
        if (cmsJira.fieldAppender != null) {
            newFieldsMap = cmsJira.fieldAppender.customFieldsJson(caseJson, caseOrIncident, moduleId);
            if (newFieldsMap != null && newFieldsMap.size() != 0) {
                newTaskjson = IssueBuilder.removeDuplicates(taskJson, newFieldsMap);
                return newTaskjson;
            }
        }
        return taskJson;
    }

    private CxJson getJiraCustomValue(String jiraId, String jiraType) {
        CxJson customJson = new CxJson();
        if (jiraType.equals("string")) {
            customJson.put(jiraId, NA);
            return customJson;
        }
        if (jiraType.equals("dateTime")) {
            customJson.put(jiraId, NA);
            return customJson;
        }
        if (jiraType.equals("amountAsString")) {
            customJson.put(jiraId, NA);
            return customJson;
        }

        if (jiraType.equals("stringList")) {
            CxJson jsonArray = new CxJson();
            customJson.put(jiraId, jsonArray.toString());
            return customJson;
        }

        if (jiraType.startsWith("dropdown")) {
            CxJson dropdownFieldJson = new CxJson();
            dropdownFieldJson.put("value", "Select a value");
            customJson.put(jiraId, dropdownFieldJson);
            return customJson;
        }

        //Add more custom field types here
        return null;
    }

    protected boolean getSuppressionDur(String factname, String entityId) {
        ICXLog icxLog = CXLog.fenter("CMSUtility.getSuppressionDur");
        RDBMSSession session = null;
        try {
            session = Rdbms.getAppSession();
            CmsFeedBackCustomMapper mapper = session.getMapper(CmsFeedBackCustomMapper.class);
            CmsFeedbackKey key = new CmsFeedbackKey();
            key.setEntityId(entityId);
            key.setFactName(factname);
            CmsFeedback feedback = mapper.select(key);

            if (feedback != null) {
                Timestamp expiry = feedback.getSuppTo();
                icxLog.ckpt("Suppression Date is " + new java.util.Date(expiry.getTime()));
                java.util.Date date = new java.util.Date();
                Timestamp current = new Timestamp(date.getTime());
                icxLog.fexit();
                if (expiry.before(current)) return false;
                else return true;
            } else {
                return false;
            }
        } finally {
            if (session != null) {
                session.rollback();
                session.close();
            }
        }
    }

    private String getEvidenceSource(CxJson e) {
        if (e == null) return "";
        return e.getString("incidentSource", "");
    }

    private void appendEventMap(Map<String, HostEvent> evMap) {
        if (evMap != null) {
            evMap.clear();
        }
    }

    private String getEvidencesData(CxJson value,IncidentEvent event) throws CmsException {

        if (value == null) {
            return "Evidence details are not available for this incident.\n";
        }

        try {
            clari5.aml.cms.newjira.CmsJira cmsJira = (clari5.aml.cms.newjira.CmsJira) Clari5.getResource("newcmsjira");
            Class evidenceFormatter = Class.forName(cmsJira.getEvidenceFormatter());
            Object obj = evidenceFormatter.newInstance();
            IEvidenceFormatter iEvidenceFormatter = (IEvidenceFormatter) obj;
            int eventCountInEvidence = cmsJira.getMaxEventCountInEvidence();

            Iterator<CxJson> evidences = value.iterator();
            StringBuilder resEvid = new StringBuilder();
            Map<String, HostEvent> evMap = new HashMap<>();
            for (Iterator<CxJson> it = evidences; it.hasNext(); ) {
                CxJson evidence = it.next(); /* start for*/
                try {
                    switch (getEvidenceSource(evidence)) {
                        case "RDE":
                            /*
                                In order to obtain evidence data from Event Tables formatEvidenceRde is replaced
                                with formatEvidenceSam.
                             */
                            resEvid.append(iEvidenceFormatter.formatEvidenceSam(evidence, evMap));
                            break;
                        case "SAM":
                            resEvid.append(iEvidenceFormatter.formatEvidenceSam(evidence, evMap, eventCountInEvidence,event));
                            appendEventMap(evMap);
                            break;
                        case "RISK":
                            resEvid.append(iEvidenceFormatter.formatEvidenceRisk(evidence, evMap));
                            appendEventMap(evMap);
                            break;
                        case "SWIFT":
                            resEvid.append(iEvidenceFormatter.formatSwiftMessage(evidence, evMap));
                            appendEventMap(evMap);
                            break;
                        case "MSTR":
                            resEvid.append(iEvidenceFormatter.formatMSTREvidence(evidence, evMap));
                            appendEventMap(evMap);
                            break;
                        default:
                            resEvid.append(iEvidenceFormatter.formatEvidenceDefault(evidence, evMap));
                            appendEventMap(evMap);
                            break;
                    }
                } catch (CmsException.Retry ex) {
                    throw ex;
                } catch(Exception inner) {
                    /* perform the same activity as for default.
                    It is possible that the next evidence set in the for is proper, so we have an inner
                    exception block, and then continue to the next item in the loop
                     */
                    logger.error("Error : Evidence is not in proper format for [" + evidence + "]", inner);
                    resEvid.append(iEvidenceFormatter.formatEvidenceDefault(evidence, evMap));
                    appendEventMap(evMap);
                }
            }
            resEvid.append("\r\n");
            return resEvid.toString();
        } catch (CmsException.Retry ex) {
            throw ex;
        }catch (Exception e) {
            logger.error("Error : Evidence is not in proper format", e);
            return value.toString();
        }
    }


    private String getTransactions(Map<String, Set<String>> transMap) {
        try {
            if (transMap != null && transMap.size() > 0) {
                StringBuilder transactions = new StringBuilder();
                for (Map.Entry<String, Set<String>> account : transMap.entrySet()) {
                    transactions.append(account.getKey());
                    Set<String> eventBusinessKeys = account.getValue();
                    if (eventBusinessKeys != null && eventBusinessKeys.size() > 0) {
                        transactions.append("\r\n");
                        for (String businessKey : eventBusinessKeys) {
                            transactions.append(businessKey).append("\r\n");
                        }
                        int index = transactions.lastIndexOf("\r\n");
                        transactions.delete(index, index + 2);
                    }
                    transactions.append("\r\n\r\n");
                }
                int index = transactions.lastIndexOf("\r\n\r\n");
                transactions.delete(index, index + 4);
                return transactions.toString();
            }
        } catch (Exception e) {
            return " ";
        }
        return " ";
    }

    private Map<String, Set<String>> mergeTrans(Map<String, Set<String>> map1, Map<String, Set<String>> map2) {
        Map<String, Set<String>> resMap = new HashMap<>();
        resMap.putAll(map1);
        for (Map.Entry<String, Set<String>> entry : map2.entrySet()) {
            if (!resMap.containsKey(entry.getKey())) {
                resMap.put(entry.getKey(), entry.getValue());
            } else {
                Set<String> trans = entry.getValue();
                Set<String> resTrans = resMap.get(entry.getKey());
                resTrans.addAll(trans);
            }
        }
        return resMap;
    }

    public String getFormattedCaseEntityId(Hocon h) {
        String entityName = h.getString("caseEntityName");
        String displayKey = h.getString("caseDisplayKey");
        String entityId = Hfdb.getCxKeyGivenDisplayKey(displayKey, entityName);
        if (entityName.equalsIgnoreCase("CUSTOMER")) {
            String wsName = "Customer";
            String url = System.getenv("SECURE_DN") + "/efm/io/index.jsp?refId=" + entityId + "&cName=" + wsName + "&jUID=cxpsaml&src=jira";
            displayKey += "| [Details|" + url + "]";
            return displayKey;
        }
        return displayKey;
    }

    public String getFormattedIncidentEntityId(Hocon h) {
        String entityName = h.getString("entityName");
        String displayKey = h.getString("displayKey");
        String entityId = Hfdb.getCxKeyGivenDisplayKey(displayKey, entityName);
        if (entityName.equalsIgnoreCase("ACCOUNT")) {
            String wsName = "Account";
            String url = System.getenv("SECURE_DN") + "/efm/io/index.jsp?refId=" + entityId + "&cName=" + wsName + "&jUID=cxpsaml&src=jira";
            displayKey += "| [Details|" + url + "]";
            return displayKey;
        }
        return displayKey;
    }


    private Map<String, Set<String>> populateAccountAndTransactionDetails(String evidenceJson) {
        Map<String, Set<String>> allTrans = new HashMap<>();
        final String FT_ACCOUNT_TXN_EVENT = "FT_AccountTxn";

        Set<String> ftAccountTxnEventIds = new HashSet<>();
        Set<String> trans = new HashSet<>();
        Set<String> eventIds = new HashSet<>();
        try {

            String ftAccountTxnEventMnemonic = WsRelationUtil.getMnemonicNameIgnoreCase(FT_ACCOUNT_TXN_EVENT);

            if(ftAccountTxnEventMnemonic == null){
                logger.error("Mnemonic for FT_AccountTxn not found. Returning empty map for account and transaction details.");
                return allTrans;
            }

            Hocon h = new Hocon(evidenceJson);

            for (Hocon evidence : h.getList("evidenceData")) {
                eventIds.addAll(evidence.getStringList("eventIds"));
                if (eventIds.isEmpty()) return allTrans;
                eventIds.forEach(id -> {
                    if (id.startsWith(ftAccountTxnEventMnemonic)) ftAccountTxnEventIds.add(id);
                });
            }

            if(ftAccountTxnEventIds.isEmpty()) return allTrans;

            RDBMS rdbms = Clari5.rdbms();
            if (rdbms == null)
                throw new RuntimeException("RDBMS as a resource is unavailable");

            List<IWSEvent> ftAccountTxnEvents;

            try (CxConnection con = rdbms.get()) {
                EventMapper mapper = EventMapper.getInstance("FT_AccountTxnEvent");
                if (mapper == null) {
                    logger.error("EventMapper for FT_AccountTxnEvent not found while populating Account and Transaction Details. Returning empty map");
                    return allTrans;
                }
                ConnectionWrapper wrapper = ConnectionWrapper.getConnectionWrapper(con, ConType.RDBMS);
                ftAccountTxnEvents = mapper.getAllEvents(wrapper, ftAccountTxnEventIds);

                if (ftAccountTxnEvents == null || ftAccountTxnEvents.size() == 0 || ftAccountTxnEvents.size() < ftAccountTxnEventIds.size()) {
                    Date to = new Date(System.currentTimeMillis());
                    Date from = new Date(System.currentTimeMillis() - 366 * 24 * 3600 * 1000L);

                    ftAccountTxnEvents = mapper.getAllEventsFromArchive(wrapper, ftAccountTxnEventIds, from, to);
                }

            }

            if (ftAccountTxnEvents == null || ftAccountTxnEvents.isEmpty())
                return allTrans;

            for (IWSEvent ev : ftAccountTxnEvents) {
                CxJson json = CxJson.parse(CxJson.obj2json(ev, CxJson.JsonCase.LOWER_CASE));

                String actId = Hfdb.getCxKeyGivenDisplayKey(json.getString("accountid") + " | Finacle Core", "ACCOUNT");
                String tranid = json.getString("tranid") + "_"
                        + getTransactionDate(json.getString("trandate"))
                        + "_" + json.getString("parttransrlnum");
                trans.add(tranid);
                allTrans.put(actId, trans);
            }
        } catch (Exception e) {
            System.err.println("Encountered an exception while populating accounts and transactions details. [" + e.getMessage() + "]");
            e.printStackTrace(System.err);
        }

        return allTrans;
    }

    private String getTransactionDate(String date) {

        if (date == null || "".equalsIgnoreCase(date))
            return null;

        date = date.substring(0, 10).replaceAll("-","");
        return date;

    }

    protected CxJson getCaseCustomValue(String fieldName, String jiraType, String value) {
        CxJson caseValueJson = new CxJson();

        if (fieldName.equals("accountsDetails")) {
            String[] accounts = value.split("\r\n\r\n");
            CxJson jsonArray = new CxJson();
            if (accounts.length > 0) {
                for (String account : accounts) {
                    String[] accTr = account.split("\r\n");
                    CxJson accountJson = new CxJson().put("accountId", accTr[0]);
                    CxJson transactions = new CxJson();
                    for (int i = 1; i < accTr.length; i++) {
                        CxJson trans = new CxJson();
                        trans.put("transactionId", accTr[i]);
                        transactions.append(trans);
                    }
                    if (transactions.count() > 0) {
                        accountJson.put("transactions", transactions);
                    }
                    jsonArray.append(accountJson);
                }

            }
            return caseValueJson.put(fieldName, jsonArray);
        }

        if (fieldName.equals("alertIndicator")) {
            String[] alerts = value.replace("\\r\\n", "\r\n").split("\\r\\n|\\n|\\r");
            CxJson JsonArray = new CxJson();
            if (alerts.length > 0) {
                for (String line : alerts) {
                    JsonArray.append(line);
                }
            }
            caseValueJson.put(fieldName, JsonArray);
            return caseValueJson;
        }

        return caseValueJson;
    }

    protected CxJson getPathJson(String fieldName, CxJson value) {
        return new CxJson().put(fieldName, value);
    }

    protected String getSuppDur(CxJson event) {
        return formatter.getSuppressionDuration(event);
    }

    // returns Parent_Id of an incident from combined Parent&IncidentId
    public String getParentId(String id) {
        if (id == null) return "";
        int index = id.indexOf("_");
        String parentId = id.substring(0, index);
        return parentId;
    }

    //returns Incident_Id of an incident from combined Parent&IncidentId
    public String getIncidentId(String id) {
        if (id == null) return "";
        int index = id.indexOf("_");
        String incidentId = id.substring(index + 1);
        return incidentId;

    }
}
