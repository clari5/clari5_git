package clari5.custom.db;

import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;

/**
 * @author shishir
 * @since 11/01/2018
 */
public class RDBMSConnection implements IDBConnection {
    private RDBMSSession rdbmsSession;

    @Override
    public Object getConnection() {
        rdbmsSession = Rdbms.getAppSession();
        return rdbmsSession;
    }

    @Override
    public void closeConnection() {
        if (rdbmsSession != null) rdbmsSession.close();
    }
}
