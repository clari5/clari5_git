package clari5.aml.cms.newjira;

import clari5.aml.cms.CmsException;
import clari5.aml.commons.Incident;
import clari5.hfdb.Hfdb;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.util.CxJson;
import clari5.platform.util.ECClient;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Evidence;
import cxps.eoi.IncidentEvent;
import org.json.JSONArray;
import org.json.JSONException;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
/**
 * Created by bisman on 12/16/16.
 */
public class DefaultCmsFormatter implements ICmsFormatter {

    protected static CxpsLogger logger = CxpsLogger.getLogger(DefaultCmsFormatter.class);

    protected static Map<String, Map<String, Integer>> groupUserCasesMap;
    protected CmsJira cmsJira;

    public DefaultCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    protected void processGroupUserCasesMap(JiraClient jiraClient, String userGroup) throws CmsException {
        if (groupUserCasesMap == null) {
            groupUserCasesMap = new HashMap<>();
        }
        if (!groupUserCasesMap.containsKey(userGroup)) {
            CxJson responseJson;
            try {
                responseJson = jiraClient.getUsers(userGroup);
            } catch (JiraClientException.Configuration e) {
                throw new CmsException.Configuration(e.getMessage());
            } catch (JiraClientException.Data e) {
                throw new CmsException.Data(e.getMessage());
            } catch (JiraClientException.IssueDoesNotExist e) {
                throw new CmsException.IssueDoesNotExist(e.getMessage());
            } catch (JiraClientException.Retry e) {
                throw new CmsException.Retry(e.getMessage());
            } catch (JiraClientException e) {
                throw new CmsException(e.getMessage());
            }
            if (responseJson == null) {
                return;
            }

            Map<String, Integer> userCasesMap = new HashMap<>();
            CxJson users = responseJson.get("users").get("items");
            Iterator<CxJson> it = users.iterator();

            while (it.hasNext()) {
                String userName = it.next().getString("name", "");
                if (userName.isEmpty()) continue;
                Integer noOfCases = 0; //getCases(userName,jiraClient);
                userCasesMap.put(userName, noOfCases);
            }
            groupUserCasesMap.put(userGroup, userCasesMap);
        }
    }

    @Override
    public String getCaseAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        String moduleId = caseEvent.getModuleId();
        CmsModule cmsModule = (CmsModule) cmsJira.get(moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        String userGroup = cmsModule.getUserGroup();

        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        synchronized (userGroup) {
            Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
            for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
                if (entry.getValue() < noOfCases) {
                    noOfCases = entry.getValue();
                    resNextUser = entry.getKey();
                }
            }
            userCasesMap.put(resNextUser, ++noOfCases);
        }
        return resNextUser;
    }

    @Override
    public String getIncidentAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        CmsModule cmsModule = (CmsModule) cmsJira.get(caseEvent.getModuleId());
        Map<String, String> departmentMap = cmsModule.getDepartments();
        String userGroup = cmsModule.getUserGroup();
        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        synchronized (userGroup) {
            Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
            for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
                if (entry.getValue() < noOfCases) {
                    noOfCases = entry.getValue();
                    resNextUser = entry.getKey();
                }
            }
            userCasesMap.put(resNextUser, ++noOfCases);
        }
        return resNextUser;
    }

    @Override
    public String getProjectId(IncidentEvent event) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        String projectCode = Hfdb.getRefCodeVal("FACT_PROJECT_MAPPING", event.getFactname());
        return projectCode == null ? ((CmsModule) cmsJira.get(event.getModuleId())).getProjectCode() : projectCode;
    }

    @Override
    public String getSuppressionDuration(CxJson event) throws JSONException {
        return event.getString("suppressDur", "OD");
    }

    @Override
    public String getParentEntityId(IncidentEvent incidentEvent) {
        return incidentEvent.getCaseEntityId();
    }

    @Override
    public String getChildEntityId(IncidentEvent incidentEvent) {
        return incidentEvent.getEntityId();
    }

    @Override
    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException {
        return CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event , jiraParentId , isupdate);
    }

    @Override
    public CxJson populateCaseJson(IncidentEvent event) throws CmsException {
        return CMSUtility.getInstance(event.getModuleId()).populateCaseJson(event);
    }
    @Override
    public void postAssignmentAction(IncidentEvent incidentEvent) throws CmsException {
        RDBMSSession session = null;
        if (incidentEvent.getProject().equals("NATM")) {
            logger.debug("postAssignmentAction: IncidentEvent is NATM ");
            try {
                session = Rdbms.getAppSession(); // ignore all exceptions here
                logger.debug("postAssignmentAction:Calling Incident table update status to C");
                incidentEvent.setStatus("C");
                incidentEvent.update(session);
                session.commit();
                logger.debug("postAssignmentAction:IncidentProcessed");
                // icxLog.fexit();
            } catch (Exception e) {
                if (session != null) session.rollback();
                logger.fatal("postAssignmentAction:Exception Saving in incident table ", e);
                e.printStackTrace();
            } finally {
                if (session != null) session.close();
            }


            Connection conn = Rdbms.getConnection();
            PreparedStatement pmst = null;
            try {
                StringBuilder params = new StringBuilder();
                String eventIds = eventId(incidentEvent.getEvidence());

                String[] event = eventIds.split(",");
                String entityId = incidentEvent.getEntityId();
                String accountId = entityId.substring(4, entityId.length());

                for (int i = 0; i < event.length; i++) params.append("?,");
                StringBuilder sb = new StringBuilder("UPDATE ACCOUNT_TRANSACTION set UPDATED_THRESHOLD = (select SUM(Tran_amount) *2  from EVENT_FT_COREACCT WHERE EVENT_ID IN (").append(params.toString().substring(0, params.lastIndexOf(","))).append(") ),CUM_TXN_AMT=0,TS_COUNT=0 WHERE ACCOUNT_ID=?");
                pmst = conn.prepareStatement(sb.toString());
                logger.info("ACCOUNT_TRANSACTION Query " + sb);
                int count = 1;
                for (String map : event) pmst.setString(count++, map);
                pmst.setString(count, accountId);
                logger.debug("postAssignmentAction:Set AccoutnId");
                pmst.executeUpdate();
                logger.debug("postAssignmentAction:Updated");
                conn.commit();
                logger.debug("postAssignmentAction:Proceed");
            } catch (SQLException e) {
                logger.debug("ERROR in ACCOUNT_TRANSACTION");
                e.printStackTrace();
            } finally {
                try {
                    if (pmst != null) pmst.close();
                    if (conn != null) conn.close();
                } catch (SQLException e) {
                    logger.debug("ERROR occured during update for ACCOUNT_TRANSACTION :", e.getMessage());
                }

            }


            //event fileds set incident// eventity is-ws key , ws name , time, fact name
            // call tostring - will return event json string - json string
            //event id = entityid_system.currenttimeinmillsis
            // ECClient.enqueue("HOST", eventid, json string);

            EventFields eventFields = new EventFields();
            eventFields.setEventId(incidentEvent.getEventId().trim());
            eventFields.setFactName(incidentEvent.getFactname().trim());
            eventFields.setSysTime(Calendar.getInstance().getTime());
            eventFields.setWsKey(incidentEvent.getEntityId().trim().replace("A_F_", ""));
            eventFields.setWsName("account");
            logger.info("NFT_Scnclosure event is "+eventFields.toString()+incidentEvent.getCaseEntityId().trim());
            String event_id = incidentEvent.getEntityId()+System.currentTimeMillis();
            ECClient.enqueue("HOST" , event_id , eventFields.toString());
            logger.info("writing to mq "+event_id);
        }
    }


    public String eventId(Evidence evidence) {

        StringBuilder eventIds = new StringBuilder();
        JSONObject js = new JSONObject(evidence);
        logger.debug("postAssignmentAction: evidence is NATM"+evidence.getEvidenceData());
        JSONObject jsonObject = (JSONObject) js.getJSONArray("evidenceData").get(0);
        JSONArray jsa = jsonObject.getJSONArray("eventIds");
        for (int i = 0; i < jsa.length(); i++) {
            eventIds.append(jsa.getString(i));
            if (i != jsa.length() - 1) eventIds.append(",");

        }
        return eventIds.toString();
    }
}
