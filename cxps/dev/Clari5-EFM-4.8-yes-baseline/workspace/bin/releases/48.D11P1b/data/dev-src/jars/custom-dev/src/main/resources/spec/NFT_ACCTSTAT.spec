clari5.custom.yes.db {
        entity {
                NFT_ACCTSTAT {
                       generate = true
                        attributes:[
                                { name: EVENT_ID , type="string:4000" ,key=true },
                                { name: HOST_ID ,type ="string:50" }
                                { name: ACCOUNT_ID ,type ="string:50" }
				{ name: AVL_BAL ,type ="number:30,2" }
				{ name: INIT_ACCT_STATUS ,type ="string:50" }
				{ name: USER_ID ,type ="string:500"  }
				{ name: BRANCH_ID ,type ="string:50"  }
				{ name: FINAL_ACCT_STATUS ,type ="string:50" }
				{ name: BRANCH_ID_DESC ,type ="string:50" }
				{ name: CUST_ID ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: EVENTTS ,type =timestamp }
				{ name: ACCT_OPEN_DT ,type =timestamp }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
				{ name: INSERTION_TIME ,type =timestamp }
				{ name: SERVER_ID ,type ="number" }	
				{ name: REASON_DESC ,type ="string:120" }			
                                ]
                        }
        }
}

