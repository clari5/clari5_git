clari5.custom.mapper {
        entity {
                CUST_FIRST_LOGIN {
                       generate = true
                        attributes:[
                                { name: CUST_ID ,type ="string:50", key=true},
                                { name: FIRST_LOGIN_DATE ,type = timestamp}
                                ]
                        }
        }
}
