clari5.custom.yes.db {
        entity {
                customer-master {
                     attributes = [
                                { name:customer-id	,type :"number:10",    key=true}
				{ name:ucic	,type :"string:36",    key=false}
				{ name:dob	,type :"date",    key=false}
				{ name:customer-name	,type :"string:500",    key=false}
				{ name:age	,type :"number",    key=false}
				{ name:cod-mis-cust-code-2	,type :"string:9",    key=false}
				{ name:customer-risk	,type :"string:36",    key=false}
				{ name:email-id	,type :"string:200",    key=false}
				{ name:last-kyc-update-date	,type :"string:360",    key=false}
				{ name:mobile	,type :"string:20",    key=false}
				{ name:phone-office	,type :"string:20",    key=false}
				{ name:permanent-address-1	,type :"string:200",    key=false}
				{ name:permanent-address-2	,type :"string:200",    key=false}
				{ name:permanent-address-3	,type :"string:200",    key=false}
				{ name:permanent-city	,type :"string:35",    key=false}
				{ name:permanent-country	,type :"string:35",    key=false}
				{ name:mailing-address-1	,type :"string:200",    key=false}
				{ name:mailing-address-2	,type :"string:200",    key=false}
				{ name:mailing-address-3	,type :"string:200",    key=false}
				{ name:mailing-city	,type :"string:35",    key=false}
				{ name:mailing-country	,type :"string:35",    key=false}
				{ name:phone-residence	,type :"string:100",    key=false}
				{ name:hand-phone	,type :"string:15",    key=false}
				{ name:turnover	,type :"string:360",    key=false}
				{ name:date-of-incorporation	,type :"date",    key=false}
				{ name:passport	,type :"string:35",    key=false}
				{ name:pan	,type :"string:20",    key=false}
				{ name:emp-id	,type :"string:16",    key=false}
				{ name:rm-code	,type :"number",    key=false}
				{ name:gender	,type :"char",    key=false}
				{ name:staff	,type :"char",    key=false}
				{ name:customer-type	,type :"string:3",    key=false}
				{ name:profession-code	,type :"number:3",    key=false}
				{ name:flag-type-class	,type :"string:35",    key=false}
				{ name:cust-nationality	,type :"string:45",    key=false}
				{ name:cust-minor	,type :"string:1",    key=false}
				{ name:signature-update-date	,type :"date",    key=false}
				{ name:hsm-flag	,type :"string:1",    key=false}
				{ name:customer-segment	,type :"string:360",    key=false}
				{ name:cbr-code	,type :"string:20",    key=false}
				{ name:partner-segment	,type :"string:27",    key=false}
				{ name:customer-description	,type :"string:130",    key=false}
				{ name:resi-number	,type :"string:20",    key=false}
				{ name:income	,type :"number:10",    key=false}
				{ name:expected-turnover	,type :"number",    key=false}
				{ name:business	,type :"string:20",    key=false}
				{ name:nature-of-business	,type :"number",    key=false}
				{ name:complete-permanentaddr	,type :"string:40",    key=false}
				{ name:complete-mailingaddr	,type :"string:40",    key=false}
				{ name:created-on	,type :"timestamp",    key=false}
				{ name:updated-on	,type :"timestamp",    key=false}
				{ name:cxcifid	,type :"string:50",    key=false}
				{ name:primarycxcifid	,type :"string:50",    key=false}
				{ name:na	,type :"string:100",    key=false}
				{ name:nanum	,type :"number",    key=false}
				{ name:nadate	,type :"date",    key=false}
				{ name:natimestamp	,type :"timestamp",    key=false}
				{ name:last-modified-time	,type :"timestamp",    key=false}
				{ name:version	,type :"number",    key=false}
				{ name:lec	,type :"string:60",    key=false}
                                { name:mobile1 ,type :"string:11",key=false}
                                
                                { name:cust-id	,type :"string:60",    key=false}
                                { name:customer-creation-date ,type :"string:11",key=false}
                                ]

                }
        }

}
