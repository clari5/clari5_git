// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCOUNTINQUIRY", Schema="rice")
public class NFT_AccountInquiryEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String acctId;
       @Field(size=200) public String branchIdDesc;
       @Field(size=200) public String userId;
       @Field(size=200) public String rmCodeFlag;
       @Field(size=200) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String menuId;
       @Field public java.sql.Timestamp eventts;
       @Field(size=11) public Double avlBal;
       @Field(size=200) public String noncustKey;
       @Field(size=200) public String hostId;
       @Field(size=200) public String custType;
       @Field(size=200) public String acctStatus;


    @JsonIgnore
    public ITable<NFT_AccountInquiryEvent> t = AEF.getITable(this);

	public NFT_AccountInquiryEvent(){}

    public NFT_AccountInquiryEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AccountInquiry");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctId(json.getString("acct_id"));
            setBranchIdDesc(json.getString("branchiddesc"));
            setUserId(json.getString("user_id"));
            setRmCodeFlag(json.getString("RM_Code_flag"));
            setBranchId(json.getString("branchid"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setMenuId(json.getString("menu_id"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setHostId(json.getString("host_id"));
            setCustType(json.getString("cust_type"));
            setAcctStatus(json.getString("acct_status"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setNoncustKey(cxps.noesis.core.EventHelper.concat(getUserId(), getAcctId()));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAcctId(){ return acctId; }

    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getUserId(){ return userId; }

    public String getRmCodeFlag(){ return rmCodeFlag; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getMenuId(){ return menuId; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public Double getAvlBal(){ return avlBal; }

    public String getHostId(){ return hostId; }

    public String getCustType(){ return custType; }

    public String getAcctStatus(){ return acctStatus; }
    public String getNoncustKey(){ return noncustKey; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setRmCodeFlag(String val){ this.rmCodeFlag = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setMenuId(String val){ this.menuId = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCustType(String val){ this.custType = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setNoncustKey(String val){ this.noncustKey = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getAcct_id(){ return acctId; }
    @JsonIgnore
    public String getUser_id(){ return userId; }
    @JsonIgnore
    public String getRM_Code_flag(){ return rmCodeFlag; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getCust_id(){ return custId; }
    @JsonIgnore
    public String getMenu_id(){ return menuId; }
    @JsonIgnore
    public Double getAvl_bal(){ return avlBal; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }
    @JsonIgnore
    public String getCust_type(){ return custType; }
    @JsonIgnore
    public String getAcct_status(){ return acctStatus; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_AccountInquiryEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        //String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.noncustKey);
        //wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        //String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        //wsInfoSet.add(new WorkspaceInfo("User", userKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AccountInquiry");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AccountInquiry");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
