package cxps.events;


import clari5.custom.Queries;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import clari5.platform.dbcon.QueryMapper;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import org.apache.hadoop.yarn.webapp.hamlet.Hamlet;

public class DbQueries {

    PreparedStatement ps, ps1, ps2,ps3,ps4,ps5;
    ResultSet result, result1, result2,result3,result4,result5;
    int dataFetched = 0;
    int data[] = new int[4];

    QueryMapper mapper = QueryMapper.getInstance(Rdbms.getAppDbType(), "clari5.custom");
    private static final CxpsLogger logger = CxpsLogger.getLogger(DbQueries.class);
/*
     getMobiledata method fetches the count of ucic id where the mobile from the json is  matched with the
     CUSTOMER_MASTER based on the following three criteria.
     1.SELECT_MOB_STAFF -matches the last 10 digits of the mobile number with
         last 10 digits of either mobile or phone residence or office phone of staff
     2.SELECT_MOB_NONSTAFF-matches the last 10 digits of the mobile number with
         last 10 digits of either mobile or phone residence or office phone of non-staff
     3.SELECT_MOB - matches the last 10 digits of the mobile number with
         last 10 digits of  mobile
 */

    public int[] getMobiledata(Connection con, String mobile, String ucic) {

        try {

            String mobileNo = mobile.trim().substring((mobile.length() - 10), mobile.length());
            String mobStaff = mapper.get(Queries.SELECT_MOB_STAFF);
            String mobNonStaff = mapper.get(Queries.SELECT_MOB_NONSTAFF);
            String mobile1 = mapper.get(Queries.SELECT_MOB);
            logger.info("mobile No [" +mobileNo+"] and ucic Id ["+ucic+"] and Query for mobStaff["+mobStaff+"] and Query for mobNonStaff ["+mobNonStaff+"] and Query for mobile 1 ["+mobile1+"] and Connection ["+con+"]");
            ps = con.prepareStatement(mobStaff);
            ps1 = con.prepareStatement(mobNonStaff);
            ps2 = con.prepareStatement(mobile1);
            ps.setString(1, ucic);
            ps.setString(2, mobileNo);
            ps.setString(3, mobileNo);
            ps.setString(4, mobileNo);
            ps1.setString(1, ucic);
            ps1.setString(2, mobileNo);
            ps1.setString(3, mobileNo);
            ps1.setString(4, mobileNo);
            ps2.setString(2, mobileNo);
            ps2.setString(1, ucic);
            result = ps.executeQuery();
            result1 = ps1.executeQuery();
            result2 = ps2.executeQuery();
            while (result.next()) {
                data[0] = result.getInt(1);
            }
            while (result1.next()) {
                data[1] = result1.getInt(1);
            }
            while (result2.next()) {
                data[2] = result2.getInt(1);
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null || result1 != null || result2 != null) {
                    result.close();
                    result1.close();
                    result2.close();
                }
                if (ps != null || ps1 != null || ps2 != null) {
                    ps.close();
                    ps1.close();
                    ps2.close();
                }

            } catch (Exception e) {
                logger.info("Exception in mobile [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
        }
        return data;
    }
    public List<String>  getMobileStaffcust(Connection con, String mobile, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        try {

            String mobileNo = mobile.trim().substring((mobile.length() - 10), mobile.length());
            String custmobStaff = mapper.get(Queries.CUST_MOB_STAFF);
            ps = con.prepareStatement(custmobStaff);
            ps.setString(1, ucic);
            ps.setString(2, mobileNo);
            ps.setString(3, mobileNo);
            ps.setString(4, mobileNo);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null || ps1 != null || ps2 != null) {
                    ps.close();
                }
            } catch (Exception e) {
                logger.info("Exception in mobile [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
        }
        return custDetails;
    }
    public List<String>  getMobileNonStaffdata(Connection con, String mobile, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        try {

            String mobileNo = mobile.trim().substring((mobile.length() - 10), mobile.length());
            String custmobNonStaff = mapper.get(Queries.CUST_MOB_NONSTAFF);
            ps = con.prepareStatement(custmobNonStaff);
            ps.setString(1, ucic);
            ps.setString(2, mobileNo);
            ps.setString(3, mobileNo);
            ps.setString(4, mobileNo);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null || ps1 != null || ps2 != null) {
                    ps.close();
                }
            } catch (Exception e) {
                logger.info("Exception in mobile [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
        }
        return custDetails;
    }

    public List<String>  getCustMobData(Connection con, String mobile, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        try {

            String mobileNo = mobile.trim().substring((mobile.length() - 10), mobile.length());
            String custmob = mapper.get(Queries.CUST_MOB);
            ps = con.prepareStatement(custmob);
            ps.setString(1, ucic);
            ps.setString(2, mobileNo);

            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null || ps1 != null || ps2 != null) {
                    ps.close();
                }
            } catch (Exception e) {
                logger.info("Exception in mobile [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
        }
        return custDetails;
    }
    /*
    gethomeLandline method fetches the count of ucic id where the homelandline from the json is
     matched with the CUSTOMER_MASTER based on the following  criteria.
     1.SELECT_LANDLINE- matches the last 8 digit of homelandline with phone residence or
     phone office

     2.SELECT_MATCH_PHONE- matches the last 8 digit of homelandline with phone office ,phone residence or
     hand phone
     */

    public int[] gethomeLandline(Connection con, String homeland, String ucic) {
        String homelandline = homeland.trim().substring((homeland.length() - 8), homeland.length());
        String homelandLine = mapper.get(Queries.SELECT_LANDLINE);
        String matchPhone =mapper.get(Queries.SELECT_MATCH_PHONE);
        logger.info("homeland [" +homeland+"] and ucic Id ["+ucic+"] and Query for homelandLine["+homelandLine+"] and Query for matchPhone ["+matchPhone+"]and Connection ["+con+"]");

        try {
            ps = con.prepareStatement(homelandLine);
            ps1 = con.prepareStatement(matchPhone);
            ps.setString(1, ucic);
            ps.setString(2, homelandline);
            ps.setString(3, homelandline);
            ps1.setString(1,homelandline);
            ps1.setString(2,homelandline);
            ps1.setString(3,homelandline);
            ps1.setString(4,ucic);
            result = ps.executeQuery();
            result1=ps1.executeQuery();
            while (result.next()) {
                data[0] = result.getInt(1);
            }
            while (result1.next()) {
                data[1] = result1.getInt(1);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                    result1.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();
                    ps1.close();
                }

            } catch (Exception e) {
                logger.info("Ecxeption in homelandline [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return data;
        }
    }

    public List<String>  getcustLandline(Connection con, String homeland, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String homelandline = homeland.trim().substring((homeland.length() - 8), homeland.length());
        String landLine = mapper.get(Queries.CUST_LANDLINE);
        try {
            ps = con.prepareStatement(landLine);
            ps.setString(1, ucic);
            ps.setString(2, homelandline);
            ps.setString(3, homelandline);

            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null) {
                    ps.close();

                }

            } catch (Exception e) {
                logger.info("Ecxeption in homelandline [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }

    public List<String>  getcustmatchPhone(Connection con, String homeland, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String homelandline = homeland.trim().substring((homeland.length() - 8), homeland.length());
        String matchPhone = mapper.get(Queries.CUST_MATCH_PHONE);
        try {
            ps = con.prepareStatement(matchPhone);
            ps.setString(1,homelandline);
            ps.setString(2,homelandline);
            ps.setString(3,homelandline);
            ps.setString(4,ucic);

            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null) {
                    ps.close();

                }

            } catch (Exception e) {
                logger.info("Ecxeption in homelandline [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }
    /*
    getofficeLandline method fetches the count of ucic id where the officeLandline from the json is
     matched with the CUSTOMER_MASTER based on the following  criteria.

    1.SELECT_LANDLINE - matches the last 8 digit of officeLand with phone residence or
     phone office
    2.SELECT_MATCH_PHONE- matches the last 8 digit of homelandline with phone office ,phone residence or
     hand phone

     */

    public int[] getofficeLandline(Connection con, String officeLandline, String ucic) {
        String officeLand = officeLandline.trim().substring((officeLandline.length() - 8), officeLandline.length());
        String officelandLine = mapper.get(Queries.SELECT_LANDLINE);
        String matchPhone =mapper.get(Queries.SELECT_MATCH_PHONE);
        logger.info("officeLand [" +officeLandline+"] and ucic Id ["+ucic+"] and Query for matchPhone["+matchPhone+"] and Query for officelandLine ["+officelandLine+"]and Connection ["+con+"]");
        try {
            ps = con.prepareStatement(officelandLine);
            ps1 = con.prepareStatement(matchPhone);
            ps.setString(1, ucic);
            ps.setString(2, officeLand);
            ps.setString(3, officeLand);
            ps1.setString(1,officeLand);
            ps1.setString(2,officeLand);
            ps1.setString(3,officeLand);
            ps1.setString(4,ucic);
            result = ps.executeQuery();
            result1=ps1.executeQuery();
            while (result.next()) {
                data[0] = result.getInt(1);
            }
            while (result1.next()) {
                data[1] = result1.getInt(1);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                    result1.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();
                    ps1.close();
                }

            } catch (Exception e) {
                logger.info("Exception in officelandline [" +e.getMessage()+" and Cause ["+e.getCause()+"]");

            }
            return data;
        }
    }
    public List<String> getcustLandline1(Connection con, String officeLandline, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String officeLand = officeLandline.trim().substring((officeLandline.length() - 8), officeLandline.length());
        String custlandline1 = mapper.get(Queries.CUST_LANDLINE);
        try {
            ps = con.prepareStatement(custlandline1);
            ps.setString(1, ucic);
            ps.setString(2, officeLand);
            ps.setString(3, officeLand);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in officelandline [" +e.getMessage()+" and Cause ["+e.getCause()+"]");

            }
            return custDetails;
        }
    }


    public List<String> getcustmatchphone1(Connection con, String officeLandline, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String officeLand = officeLandline.trim().substring((officeLandline.length() - 8), officeLandline.length());
        String custMatchPhone = mapper.get(Queries.CUST_MATCH_PHONE);
        try {
            ps = con.prepareStatement(custMatchPhone);
            ps.setString(1,officeLand);
            ps.setString(2,officeLand);
            ps.setString(3,officeLand);
            ps.setString(4,ucic);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null ) {
                    result.close();

                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in officelandline [" +e.getMessage()+" and Cause ["+e.getCause()+"]");

            }
            return custDetails;
        }
    }
    /*
  getemail method fetches the count of ucic id where the email from the json is
     matched with the email of the CUSTOMER_MASTER table

     */

    public int getemail(Connection con, String email, String ucic) {
        String emailId = mapper.get(Queries.SELECT_EMAIL);
        logger.info("email [" +email+"] and ucic Id ["+ucic+"] and Query for emailId["+emailId+"]and Connection ["+con+"]" );
        try {
            ps = con.prepareStatement(emailId);
            ps.setString(1, ucic);
            ps.setString(2, email);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in email [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return dataFetched;
        }
    }

    public List<String> getcustemail(Connection con, String email, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String emailId = mapper.get(Queries.CUST_EMAIL);
        logger.info("email [" +email+"] and ucic Id ["+ucic+"] and Query for emailId["+emailId+"]and Connection ["+con+"]" );
        try {
            ps = con.prepareStatement(emailId);
            ps.setString(1, ucic);
            ps.setString(2, email);
            result = ps.executeQuery();

            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in email [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails ;
        }
    }

    /*
    getpan method fetches the count of ucic id where the pan from the json is
     matched with the pan of the CUSTOMER_MASTER table
     */
    public int getpan(Connection con, String pan, String ucic) {
        String panNum = mapper.get(Queries.SELECT_PAN);
        logger.info("pan [" +pan+"] and ucic Id ["+ucic+"] and Query for panNum["+panNum+"]and Connection ["+con+"]");
        try {
            ps = con.prepareStatement(panNum);
            ps.setString(1, ucic);
            ps.setString(2, pan);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in pan [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return dataFetched;
        }
    }
    public List<String> getCustpan(Connection con, String pan, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String panNum = mapper.get(Queries.CUST_PAN);
        logger.info("pan [" +pan+"] and ucic Id ["+ucic+"] and Query for panNum["+panNum+"]and Connection ["+con+"]");
        try {
            ps = con.prepareStatement(panNum);
            ps.setString(1, ucic);
            ps.setString(2, pan);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in pan [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }
    /*
     getpassport method fetches the count of ucic id where the passport from the json is
     matched with the passport of the CUSTOMER_MASTER table
     */

    public int getpassport(Connection con, String passport, String ucic) {
        String passtortNum = mapper.get(Queries.SELECT_PASSPORT);
        logger.info("passport [" +passport+"] and ucic Id ["+ucic+"] and Query for passtortNum["+passtortNum+"]and Connection ["+con+"]");
        try {
            ps = con.prepareStatement(passtortNum);
            ps.setString(1, ucic);
            ps.setString(2, passport);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in passport [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return dataFetched;
        }
    }

    public List<String> getCustpassport(Connection con, String passport, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String passtortNum = mapper.get(Queries.CUST_PASSPORT);
        logger.info("passport [" +passport+"] and ucic Id ["+ucic+"] and Query for passtortNum["+passtortNum+"]and Connection ["+con+"]");
        try {
            ps = con.prepareStatement(passtortNum);
            ps.setString(1, ucic);
            ps.setString(2, passport);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in passport [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }
    /*
    getdob method fetches the count of ucic id where the dob from the json is
     matched with the dob of the CUSTOMER_MASTER table
     */

    public int getdob(Connection con, java.util.Date dob, String ucic) {
        String dob1 = mapper.get(Queries.SELECT_DOB);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        logger.info("dob [" +dob+"] and ucic Id ["+ucic+"] and Query for passtortNum["+dob1+"] and Connection ["+con+"]");


        //java.sql.Date dob2 = new java.sql.Date(dob.getTime());

        try {
            ps = con.prepareStatement(dob1);
            ps.setString(1, simpleDateFormat.format(dob));
            ps.setString(2,ucic);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }

            System.out.println("dob "+dataFetched);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in dob [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return dataFetched;
        }
    }

    public List<String> getCustdob(Connection con, java.util.Date dob, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String dob1 = mapper.get(Queries.CUST_DOB);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        logger.info("dob [" +dob+"] and ucic Id ["+ucic+"] and Query for passtortNum["+dob1+"] and Connection ["+con+"]");


        //java.sql.Date dob2 = new java.sql.Date(dob.getTime());

        try {
            ps = con.prepareStatement(dob1);
            ps.setString(1, simpleDateFormat.format(dob));
            ps.setString(2,ucic);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in dob [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }

    /*
   getdoi method fetches the count of ucic id where the doi from the json is
    matched with the doi of the CUSTOMER_MASTER table
    */
    public int getdoi(Connection con, java.util.Date doi, String ucic) {
        String doi1 =mapper.get(Queries.SELECT_DOI);
       // Date doi2 = new Date(doi.getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        logger.info("dob [" +doi+"] and ucic Id ["+ucic+"] and Query for doi1["+doi1+"]and Connection ["+con+"]");
        try {
            ps=con.prepareStatement(doi1);
            ps.setString(1,simpleDateFormat.format(doi));
            ps.setString(2,ucic);
            result=ps.executeQuery();
            while (result.next()){
                dataFetched=result.getInt(1);
            }System.out.println("doi "+dataFetched);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in doi [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return dataFetched;
        }
    }

    public List<String> getCustdoi(Connection con, java.util.Date doi, String ucic) {
        List<String> custDetails= new ArrayList<String>();
        String doi1 =mapper.get(Queries.CUST_DOI);
        // Date doi2 = new Date(doi.getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        logger.info("dob [" +doi+"] and ucic Id ["+ucic+"] and Query for doi1["+doi1+"]and Connection ["+con+"]");
        try {
            ps=con.prepareStatement(doi1);
            ps.setString(1,simpleDateFormat.format(doi));
            ps.setString(2,ucic);
            result=ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (ps != null) {
                    ps.close();
                }

            } catch (Exception e) {
                logger.info("Exception in doi [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }


    /*
    gethandphone  method fetches the count of ucic id where the handphone from the json is
     matched with the CUSTOMER_MASTER based on the following  criteria.
     1.SELECT_MATCH_PHONE- matches the last 8 digit of handphone with phone office ,phone residence or
     hand phone


     */
    public int gethandphone(Connection con, String handphone, String ucic) {
        String handPhone = handphone.trim().substring((handphone.length() - 8), handphone.length());

        String matchPhone =mapper.get(Queries.SELECT_MATCH_PHONE);
        logger.info("handphone [" +handphone+"] and ucic Id ["+ucic+"] and Query for matchPhone["+matchPhone+"]and Connection ["+con+"]");

        try {
            ps = con.prepareStatement(matchPhone);
            ps.setString(1, handPhone);
            ps.setString(2, handPhone);
            ps.setString(3, handPhone);
            ps.setString(4,ucic);
            result = ps.executeQuery();
            while (result.next()) {
                dataFetched = result.getInt(1);
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();

                }

            } catch (Exception e) {
                logger.info("Exception in handphone [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return dataFetched;
        }
    }
    public List<String> getCusthandphone(Connection con, String handphone, String ucic) {
        String handPhone = handphone.trim().substring((handphone.length() - 8), handphone.length());
        List<String> custDetails= new ArrayList<String>();
        String matchPhone =mapper.get(Queries.CUST_MATCH_PHONE);
        logger.info("handphone [" +handphone+"] and ucic Id ["+ucic+"] and Query for matchPhone["+matchPhone+"]and Connection ["+con+"]");

        try {
            ps = con.prepareStatement(matchPhone);
            ps.setString(1, handPhone);
            ps.setString(2, handPhone);
            ps.setString(3, handPhone);
            ps.setString(4,ucic);
            result = ps.executeQuery();
            int i=0;
            while (result.next() && i<=5) {

                custDetails.add(i, result.getString("CUSTOMER_ID"));

                i++;
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (result != null || result1 !=null) {
                    result.close();
                }
                if (ps != null||ps1 !=null) {
                    ps.close();

                }

            } catch (Exception e) {
                logger.info("Exception in handphone [" +e.getMessage()+" and Cause ["+e.getCause()+"]");
            }
            return custDetails;
        }
    }
}
