cxps.events.event.ft-fundtransfer{
table-name : EVENT_FT_FUNDTRANSFER
event-mnemonic : FT
        workspaces : {
        ACCOUNT: "account-number"
	CUSTOMER:"cust-id"
        NONCUSTOMER: "payee-comb"
}
event-attributes :{
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20"}
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20"}
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200"}
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200"}
        transaction-type: {db : true ,raw_name : transaction_type ,type: "string:200"}
        beneficiary-name: {db : true ,raw_name : beneficiary_name ,type: "string:200"}
        beneficiary-type1: {db : true ,raw_name : beneficiary_type1 ,type:"string:200"}
        payee-accounttype : {db : true ,raw_name : payee_accounttype ,type:"string:200"}
        customer-segment: {db : true ,raw_name : customer_segment ,type:"string:200"}
        transfer-amount: {db : true ,raw_name : transfer_amount ,type:"number:20,3"}
        currency: {db : true ,raw_name : currency ,type:"number:20,3"}
        exchange-rate: {db : true ,raw_name : exchange_rate ,type:"string:200"}
        settlement-amount: {db : true ,raw_name : settlement_amount ,type:"number:20,3"}
        debit-account-number: {db : true ,raw_name : debit_account_number ,type:"number:20,3"}
        balance-before-transaction: {db : true ,raw_name : balance_before_transaction ,type:"number:20,3"}
        balance-after-transaction: {db : true ,raw_name : balance_after_transaction ,type:"number:20,3"}
        payment-mode: {db : true ,raw_name : payment_mode ,type:"string:200"}
        tran-recurrance: {db : true ,raw_name : tran_recurrance ,type:"string:200"}
        frequency: {db : true ,raw_name : frequency ,type:"string:200"}
        start-date: {db : true ,raw_name : start_date ,type:timestamp}
        end-date: {db : true ,raw_name : end_date ,type:timestamp}
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
        risk-band:{db : true ,raw_name :risk_band ,type : "string:200" ,derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}
  payee-comb:{db : true ,raw_name :payee_comb ,type : "string:200" ,derivation : """cxps.events.CustomFieldDerivator.getPayeecombination(this)"""}
        beneficary-unique-identifier-1:{db : true ,raw_name :beneficary_unique_identifier_1 ,type : "string:200"}
        whitelist-flag:{db : true ,raw_name :whitelist_flag ,type : "string:10" ,derivation : """cxps.events.CustomFieldDerivator.getWhitelistflagforAccount(this)""" }
        account-number:{db : true ,raw_name :account_number ,type : "string:200"}
        ifsc-code:{db : true ,raw_name :ifsc_code ,type : "string:20"}
        group-payee-id:{db : true ,raw_name :group_payee_id ,type : "string:50"}
        benf-payee-id:{db : true ,raw_name :benf_payee_id ,type : "string:50"}
        benf-add-date:{db : true ,raw_name :benf_add_date ,type :timestamp ,derivation : """cxps.events.CustomFieldDerivator.getBeneficiarydetails(this)"""}
}
}
