package clari5.custom.yes.integration.data;

public class NFT_Employeebranch extends ITableData {
    private String tableName = "NFT_NEWEMPLOYEE";
    private String event_type = "NFT_Employeebranch";
    private String EVENT_ID;
    private String sys_time;
    private String branch_joining_date;
    private String employee_id;
    private String designation;
    private String host_id;
    private String branch;
    private String eventts;
    private String notice_period;
    private String current_branch;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }
   public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }
    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getBranch_joining_date() {
        return branch_joining_date;
    }

    public void setBranch_joining_date(String branch_joining_date) {
        this.branch_joining_date = branch_joining_date;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getNotice_period() {
        return notice_period;
    }

    public void setNotice_period(String notice_period) {
        this.notice_period = notice_period;
    }

    public String getCurrent_branch() {
        return current_branch;
    }

    public void setCurrent_branch(String current_branch) {
        this.current_branch = current_branch;
    }
}

