clari5.custom.mapper {
        entity {
                CUSTOM_CL5_UPLOAD_STATS {
                       generate = true
                        attributes:[
                                { name: CREATED_TIME, type=timestamp },
                                { name: UPDATED_TIME ,type =timestamp }
                                { name: TABLENAME ,type ="string:2000" }
				{ name: DB_INS_ROWS ,type ="string:2000" }
                                { name: EXCEPTION ,type ="string:2000" }
				{ name: FILE_UPLOAD_STATS ,type ="string:100" }
                                { name: FILENAME ,type ="string:2000" , key=true }
                                ]
                        }
        }
}
