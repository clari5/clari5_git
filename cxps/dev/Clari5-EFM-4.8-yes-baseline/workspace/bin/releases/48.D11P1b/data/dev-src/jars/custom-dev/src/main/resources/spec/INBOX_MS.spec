clari5.custom.yes.db {
        entity {
                inbox-ms {
                       generate = true
                        attributes:[
 				{ name :incident-id,        type : "string:50",   key=true}
                                { name :acct-sol-id,        type : "string:50",   key=false}
                                { name :email-status,       type : "string:1",    key=false}
                                { name :message,            type : "string:4000", key=false}
                                { name :remarks,            type : "string:4000", key=false}
                                { name :created-on,         type : timestamp}
                                { name :created-by,         type : "string:25",   key=false}
                                { name :updated-on,         type : timestamp}
                                { name :updated-by,         type : "string:25",   key=false}
				{ name: user_id ,type ="string:50" }

                      
                               ]
		criteria-query {
                          name: InboxMs
                          summary = [incident-id,acct-sol-id,email-status,message,remarks,created-on,created-by,updated-on,updated-by]
                          where {
                               email-status: equal-clause
                          }
                          order = ["created-on desc"]
                     }
				indexes {
             				  EMAIL_STATUS_IDX : [ EMAIL_STATUS ]
        }
                        }
        }
}
