<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF8" %>
    <%@ page import="javax.servlet.http.HttpSession" %>
        <%
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();

String session_id="";
if( cookies != null ) {
    for (int i = 0; i < cookies.length; i++) {
         cookie = cookies[i];
         if(cookie.getName().equals("session_id")){
            session_id = cookie.getValue();
          break;
        }
     }
}
      cookies= null;
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>BATCH TABLE UPLOAD</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="shortcut icon" href="./image/clari5-favicon.png">
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./css/Newfile.css">

    <script src ="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"></script>
   <script src="/YesBankEventProcessor/JavaScriptServlet"></script> 
   <script>
                   var user;
                   function onInit() {
                       var sessionId = '<%=session_id %>';
                       getUserName(sessionId, function(returnValue) {
                           console.log("SessionID"+sessionId);
                            user = returnValue;
                           <!--console.log("===============>Currrent User"+user);-->
                       });
                   }
    </script>
    <script  type="text/javascript" src ="./js/Resource.js"> </script>
    
</head>
<body onload = "onInit()">
<nav class="navbar">
    <div class="container-fluid" id="head"  style="background-color: #d9edf7">
        <div class="navbar-header" style="width: 40%">
            <a class="navbar-brand" href="#"><img src="./image/logo.jpg" alt="Clari5" width="20%" height="180%"></a>
        </div>
           <div class="collapse navbar-collapse" id="myNavbar">
               <ul class="nav navbar-nav">
                   <li class="active">
                       <h3 style="color: #3182b6">BATCH TABLE UPLOAD</h3>
                   </li>
              </ul>
          </div>
        </div>
</nav>



<nav class="navbar navbar-default">
  <div class="container-fluid">
  
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">UPLOAD</a></li>
    </ul>
  </div>
</nav> 
  
<div class="container" id="space" style="
    margin-top: 80px;
">
<div class="container-fluid">
  <div class="row">
  </div>
</div>
</div>

<div class="container" id ="old">
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
            <div class="dropdown">
           <select class="form-control" id="usr" name="Select" style="width: 250px;margin-left: 300px;"> 
              <option selected disabled value = "select"> Select table </option>
           </select>    
           </div>   
    </div>

   <div class="col-md-6">
            <div class = "col-sm-2" style="margin-top: 7px;" >
      <form>
  		<div class="form-group" id = "choose">
    			<input type="file" class="form-control-file" id="exampleFormControlFile1">
  		</div>
	    </form>
            </div>
    </div> 


    <div class="row">
    <div class="col-md-12" style="margin-top: 35px; ">
        <div class="wrapper" style="text-align: center;">
        <button type="button" id ="validatebtn" class="btn btn-info" onclick="onInit()" >Validate</button>
        <button type="button" id ="submitbtn" class="btn btn-info" onclick="onInit()"> Submit</button>
        <button type="button" id ="cancelbtn" class="btn btn-info"> Cancel</button>
        </div>
    </div>
    </div>

  </div></br>


  <div class="modal" id="comnt"    style= "margin-top: 100px;">
                            <div class="modal-dialog modal-sm" >
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header" style="text-align: center;">
                                        <h4 class="modal-title">Validation Sucessfull</h4>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body" style="text-align: center;">
                                    <img id="myImg" src="image/valid.png" alt="Snow" style="width: 14%;height: 36px;max-width:300px;">
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer"  style="text-align: center;">
                                        <h5 class="modal-title">Kindly Submit the File</h5>
                                       <!-- <button type="button"  id ="submitbtn"class="btn btn-warning" data-dismiss="modal">Submit</button> -->
                                        <button type="button" class="btn btn-warning" id = "clsbtn"  onClick="window.location.reload();" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                         <div class="modal" id="invalid"    style= "margin-top: 100px;">
                            <div class="modal-dialog modal-sm" >
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header" style="text-align: center;">
                                        <h4 class="modal-title">File Not Valid</h4>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body" style="text-align: center;">
                                    <img id="myImg" src="image/invalid.png" alt="Snow" style="width: 16%;max-width:300px;">
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer"  style="text-align: center;">
                                        <h5 class="modal-title">Kindly Check the File</h5>
                                        <button type="button" class="btn btn-warning" id = "clsbtn"  onClick="window.location.reload();" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="modal" id="comnt1"    style= "margin-top: 100px;">
                            <div class="modal-dialog modal-sm" >
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header" style="text-align: center;">
                                        <h4 class="modal-title">Submit Sucessfull</h4>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body" style="text-align: center;">
                                    <img id="myImg" src="image/valid.png" alt="Snow" style="width: 14%;height: 36px;max-width:300px;">
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer"  style="text-align: center;">
                                        <h5 class="modal-title">File Insertion Started.Kindly Check After Sometime</h5>
                                        <button type="button" class="btn btn-warning" id = "clsbtn"  onClick="window.location.reload();" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                         <div class="modal" id="invalid1"    style= "margin-top: 100px;">
                            <div class="modal-dialog modal-sm" >
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header" style="text-align: center;">
                                        <h4 class="modal-title">File Not Valid</h4>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body" style="text-align: center;">
                                    <img id="myImg" src="image/invalid.png" alt="Snow" style="width: 16%;max-width:300px;">
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer"  style="text-align: center;">
                                        <h5 class="modal-title">Kindly Check the File</h5>
                                        <button type="button" class="btn btn-warning" id = "clsbtn"  onClick="window.location.reload();" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>










 <div class="container">
    <div class="row">
      <div class="col-md-102 col-sm-12">
      </div>
  </div>
</div>






<!--<div class="container-fluid">
    <div class="row ">
        <div class="col-sm-2"></div>
        <div class="col-sm-9">
            <div class="col-sm-10">
					<form class="form-horizontal" >
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="input"></br>Select Event:</label>
							<form class="form-horizontal" id="id33" >
                            <div class="col-xs-8">
                                </br><select class="form-control" id="haha" name="Select" onchange=serv2("EventFieldServlet?data="+this.value) >
                                                <option> Select an Event </option>
                                            </select>
                            </div>
                        </div>
                <div class="form-horizontal" id="id2">
                </div>
				<div class="container" style="width: 80px; margin: 4% auto;">
				<button type="submit" class="btn btn-info btn-lg" id="id3" data-toggle="modal" data-target="#myModal"   onclick =serv3("GenerateJsonServlet")>Generate JSON</button>
            
        </div>
        <div class="col-sm-2"></div>
    </div>
</form> -->


<!-- <div class="container">
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4">
        <label class="control-label col-xs-3" for="input"></br>Select User:</label>
    </div>
    <div class="col-sm-4">
        <div class="dropdown">
    
                 <select name = "PROJECTS" button type="button" class="btn btn-info" data-toggle="dropdown">
            </button>><option>EFM</option> <option>CBS</option> <option>RDE</option></select>
            </div>
        </div>

    </div>
    <div class="col-sm-4">
        <button type="button" class="btn btn-info">ADD</button>
    </div>
  </div></br>
</div>
   




<div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">SHIFT 1</div>
    <div class="panel-body">Panel Content</div>
    <div class="panel-footer"></div>
  </div>
</div>
<div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">SHIFT 2</div>
    <div class="panel-body">Panel Content</div>
    <div class="panel-footer"></div>
  </div>
</div>
<div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">SHIFT 3</div>
    <div class="panel-body">Panel Content</div>
    <div class="panel-footer"></div>
  </div>
</div> -->




       <!-- <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">JSON</h4>
                    </div>
                    <div class="modal-body">
                        <p id="para"> </p>
                    </div>
                    <div class="modal-footer" id="myButtons">
                        <button type="button" class="btn btn-default" onclick=serv4("ApiServlet")>HIT URL</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"  onclick="refresh()">Refresh</button>
                    </div>
                </div>
            </div>
        </div>
   

</form> -->

<div class="container-fluid" id="foo">
    <footer class="container-fluid text-center">
        <span class="footerText"><b>©<a
                href="http://www.customerxps.com" target="_blank"> Customer<span style="color :red;">XP</span>s Softwares Private Limited
            </a></b></span>
    </footer>
</div>
</body>
</html>


