package clari5.custom.yes.integration.queue;

import clari5.custom.yes.integration.builder.Clari5GatewayManager;
import clari5.custom.yes.integration.config.BepCon;
import cxps.apex.utils.CxpsLogger;

import java.util.HashMap;

public class DataQueueManager {
	public static CxpsLogger logger = CxpsLogger.getLogger(DataQueueManager.class);

	private static int tableCounter = 0;
	// As of now boolean, change later.
	private static HashMap<String,String> tableToClassMap;
	private static boolean doneTables = false;
	static {
		try{
		tableToClassMap = BepCon.getConfig().getTableClassMap();
		logger.info("table to class map initialized");
	   }catch (Exception e){
			logger.info("Table to class mapping is not proper please check BatchProcessor.config");
		}
	}
	public static Class<?> nextTable(String tableName) throws Exception {

		String className=tableToClassMap.get(tableName);
		Class<?> tableC = null;
		tableC = Class.forName("clari5.custom.yes.integration.data."+className);
		return tableC;
	}
	public static synchronized boolean isDoneTables() {
		return doneTables;
	}
	public static synchronized void setDoneTables(boolean doneTables) {
		DataQueueManager.doneTables = doneTables;
	}
}
