clari5.custom.yes.db {
        entity {
                SAME_CUST_PASSPORT_DETAILS {
                       generate = true
                        attributes:[
                                { name: PASSPORT, type="string:50" },
                                { name: CREATED_ON ,type =timestamp }
                                { name: AUTO_SEQUENCE_NO ,type ="string:20" , key=true }
				{ name: MATCH_TYPE, type="string:100" }
                                { name: ID ,type ="string:30" }
                                { name: CUST_ID ,type ="string:25" }
                                ]
			criteria-query {
                          name:sameCustPassportDetails
                          summary =[auto-sequence-no,id,cust-id,match-type,passport,created_on]
                          where {
                               cust-id: equal-clause
                          }
                          order = ["created_on"]
                     }
				indexes {
         				     SAME_CUST_PASSPORT_DETAILS_IDX : [CUST_ID ]
        }
                        }
        }
}
