"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nbb_data_service_1 = require("../services/nbb.data.service");
var common_functions_1 = require("../services/common.functions");
var nbb_service_1 = require("../services/nbb.service");
var NbbMainComponent = (function (_super) {
    __extends(NbbMainComponent, _super);
    function NbbMainComponent(nbbDataService, nbbService) {
        var _this = _super.call(this) || this;
        _this.nbbDataService = nbbDataService;
        _this.nbbService = nbbService;
        //to load the existing templates list on load of the ui
        _this.postData = JSON.stringify({});
        //this.method = "init";
        console.log(_this.postData);
        //  console.log(this.method);
        _this.nbbService.getLoadData("scenario", _this.nbbDataService.url, "init", _this.postData);
        _this.postDataa = JSON.stringify({});
        //  this.method = "scenarioList";
        console.log(_this.postData);
        console.log(_this.method);
        // this.nbbService.getLoadData("tempscenario", this.nbbDataService.url,"scenarioList", this.postDataa);
        setTimeout(function () {
            _this.nbbService.getLoadData("tempscenario", _this.nbbDataService.url, "scenarioList", _this.postData);
        }, 2000);
        return _this;
    }
    //to replace the newline character
    NbbMainComponent.prototype.escapeRegExp = function (string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    };
    /* Define functin to find and replace specified term with replacement string */
    NbbMainComponent.prototype.replaceAll = function (str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    };
    //on click of the existing template
    NbbMainComponent.prototype.openScreen = function (val) {
        this.nbbDataService.showscreen = true;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.searchscenario = true;
        this.nbbDataService.showavilscreen = false;
        this.nbbDataService.scenario = val;
        this.nbbDataService.sname = val;
        console.log("scenario", this.nbbDataService.scenario);
        console.log("sname", this.nbbDataService.sname);
        //based on scenario parse and load the existing template data
        //this.nbbDataService.temp = this.nbbDataService.allscenarios[this.nbbDataService.scenario];
        this.nbbDataService.temp = JSON.parse(this.replaceAll(this.nbbDataService.allscenarios[this.nbbDataService.scenario], '\n', ' <br>'));
        console.log("temp data", this.nbbDataService.temp);
        //message
        /* this.nbbDataService.message = this.replaceAll(this.nbbDataService.temp.message, '<br>', '\n');
         //subject
         this.nbbDataService.subject = this.nbbDataService.temp.msgSubject;*/
        //checkbox
        this.nbbDataService.icheckbox = this.nbbDataService.temp.autoInboxChqFlg;
        // this.nbbDataService.echeckbox = this.nbbDataService.temp.autoEmailChqFlg;
        if (this.nbbDataService.icheckbox === "Y") {
            this.nbbDataService.officeLIST[0].checked = true;
            //this.nbbDataService.officeLIST[1].checked = true;
        } /*else if (this.nbbDataService.icheckbox === "Y" || this.nbbDataService.echeckbox === "N") {
            this.nbbDataService.officeLIST[0].checked = true;
            this.nbbDataService.officeLIST[1].checked = false;
        } else if (this.nbbDataService.icheckbox === "N" && this.nbbDataService.echeckbox === "Y") {
            this.nbbDataService.officeLIST[0].checked = false;
            this.nbbDataService.officeLIST[1].checked = true;
        } */
        else {
            this.nbbDataService.officeLIST[0].checked = false;
            //this.nbbDataService.officeLIST[1].checked = false;
        }
        //to load the eventlist
        /* if (typeof this.nbbDataService.test[this.nbbDataService.scenario] == 'undefined') {
             this.nbbDataService.eventlist = "undefined";
             console.log(this.nbbDataService.eventlist);
         } else {
             this.nbbDataService.eventlist = this.nbbDataService.test[this.nbbDataService.scenario]["eventList"];
             console.log("eventlist", this.nbbDataService.eventlist);
         }*/
    };
    //on click of the create template
    NbbMainComponent.prototype.showScreen = function () {
        this.nbbDataService.showscreen = false;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.searchscenario = true;
        this.nbbDataService.showavilscreen = true;
        this.nbbDataService.eventlist = [];
        this.nbbDataService.officeLIST[0].checked = false;
        this.nbbDataService.officeLIST[1].checked = false;
        this.nbbDataService.sname = "";
    };
    //on cleck of template manager
    NbbMainComponent.prototype.showhome = function () {
        var _this = this;
        this.nbbDataService.scenarios = [];
        this.nbbDataService.eventlist = [];
        this.nbbDataService.showscreen = true;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.showavilscreen = true;
        this.nbbDataService.searchscenario = false;
        //to load the existing scenario list
        this.postData = JSON.stringify({});
        // this.method = "init";
        console.log(this.postData);
        console.log(this.method);
        this.nbbService.getLoadData("scenario", this.nbbDataService.url, "init", this.postData);
        this.postDataa = JSON.stringify({});
        // this.method = "scenarioList";
        console.log(this.postData);
        console.log(this.method);
        // this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postDataa);
        setTimeout(function () {
            _this.nbbService.getLoadData("tempscenario", _this.nbbDataService.url, "scenarioList", _this.postData);
        }, 2000);
        this.nbbDataService.clear();
    };
    return NbbMainComponent;
}(common_functions_1.CommonFunctions));
NbbMainComponent = __decorate([
    core_1.Component({
        selector: 'kotak',
        template: "\n\n <div class=\"container-fluid\">\n    <div class=\"row content\">\n\t<!--left side of the ui-->\n        <div class=\"col-sm-3 \">\n            \n            <div class=\"panel-group\">\n                <div class=\"panel panel-default\" >\n                    <div class=\"panel-heading\" style=\"color: #fff; background-color: #2486b5;\">\n                        <h4 class=\"panel-title\">\n                            <a data-toggle=\"collapse\" href=\"#tm\" (click)=\"showhome()\">Inbox Manager</a>\n                        </h4>\n                    </div>\n                    <div id=\"tm\" class=\"panel-collapse collapse\" >\n                        <div class=\"btn-group-vertical\" style=\"height:400px;overflow:auto; width:290px\">\n                            <button type=\"button\" class=\"btn btn-default\" style=\"border: none;text-align:left\" (click)=\"showScreen()\">Create Template</button>\n                            <!-- to load the existing template-->\n                            <button type=\"button\" class=\"btn btn-default\" style=\"border: none;text-align:left\" *ngFor=\"let scen of this.nbbDataService?.scenarios\" (click)=\"openScreen(scen)\">{{scen}}</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n<!--center part of the ui where form is loaded-->\n        <div class=\"col-sm-6\">\n            <inbox></inbox>\n        </div>\n<!--rightside of the ui where eventlist and avilable tags will be loaded-->\n        <!--<div class=\"col-sm-3 \">\n            <h4 style=\"text-align: left; color: #2486b5; font-weight: bold;\">Avilable Tags</h4>\n            <div class=\"panel-group\">\n\t\t <!--to load the eventlist-->\n                <!--<div class=\"panel panel-default\" *ngFor=\"let event of this.nbbDataService?.eventlist\">\n                    <div class=\"panel-heading\" style=\"color: #fff; background-color: #2486b5;\">\n                        <h4 class=\"panel-title\">\n                            <a data-toggle=\"collapse\" href=\"#s{{event}}\" (click)=\"onEvent(this.nbbDataService.scenario,event)\">{{event}}</a>\n                        </h4>\n                    </div>\n\t\t\t <!--to load the avilable tags-->\n                   <!-- <div id=\"s{{event}}\" class=\"panel-collapse collapse\">\n                        <ul class=\"list-group\">\n                            <li style=\"list-style-type:none;margin-left: 20px;\" *ngFor=\"let tag of this.nbbDataService?.tags\">{{tag}}</li>\n                        </ul>\n                    </div>\n                </div>\n            </div>\n        </div>-->\n    </div>\n</div>\n                ",
    }),
    __metadata("design:paramtypes", [nbb_data_service_1.NbbDataService, nbb_service_1.NbbService])
], NbbMainComponent);
exports.NbbMainComponent = NbbMainComponent;
//# sourceMappingURL=nbb.main.component.js.map