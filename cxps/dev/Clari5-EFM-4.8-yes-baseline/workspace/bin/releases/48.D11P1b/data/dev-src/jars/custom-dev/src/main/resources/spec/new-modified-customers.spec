clari5.custom.yes.db {
        entity {
                new-modified-customers {
                     attributes = [
                                { name:id,                       type :"string:30",     key=false}
                                { name :cust-id,       		 type : "string:25",   	key=true}
                                { name :cust-name,      	 type : "string:20",    key=false}
                                { name :dob,             	 type: date }
                                { name :passport,                type : "string:50",    key=false}
                                { name :pan,           		 type : "string:50", 	key=false}
                                { name :address,           	 type : "string:100", 	key=false}
                                { name :date-of-incorporation, 	 type : date}
                                { name :status,           	 type : "string:1",	key=false}
                                { name :mobile,                  type : "string:20",    key=false}
                                { name :remarks,           	 type : "string:8000", 	key=false}
                                { name :created_on, 	 type : timestamp}
                                ]
                     criteria-query {
                          name:NewModifiedCustomers
                          summary =[id,cust-id, cust-name,  dob, pan, address, date-of-incorporation, status,mobile,remarks, last-modified-time] 
                          where {
                               status: equal-clause
                          }
                          order = ["created_on"]
                     }
                     indexes {
                           new-modified-customers-idx : [ status ]
                }



                }
        }

}
