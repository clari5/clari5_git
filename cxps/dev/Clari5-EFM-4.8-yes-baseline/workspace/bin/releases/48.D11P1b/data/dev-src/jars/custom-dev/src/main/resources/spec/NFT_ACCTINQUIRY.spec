clari5.custom.mapper.entity.NFT_ACCTINQUIRY {
                       attributes=[
                                { name: EVENT_ID , type="string:2000" , key=true  },
                                { name: BRANCHID ,type ="string:50" }
                                { name: ACCT_ID ,type ="string:50" }
				{ name: AVL_BAL ,type ="number:30,2" }
				{ name: BRANCHIDDESC ,type ="string:50" }
				{ name: USER_ID ,type ="string:50" }
				{ name: ACCT_STATUS ,type ="string:50" }
				{ name: RM_CODE_FLAG ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: EVENTTS ,type =timestamp }
				{ name: CUST_ID ,type ="string:50" }
				{ name: MENU_ID ,type ="string:50" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
				{ name: SERVER_ID ,type ="number"  }
                              ]
				indexes {
              				IDX_COMP_NFTACCTINQ : [ SYNC_STATUS,SERVER_ID  ]
					
        			  }
                        }
        

