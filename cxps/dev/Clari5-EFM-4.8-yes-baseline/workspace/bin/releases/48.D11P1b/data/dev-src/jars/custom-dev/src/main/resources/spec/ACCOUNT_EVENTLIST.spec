clari5.custom.mapper {
        entity {
                ACCOUNT_EVENTLIST {
                       generate = true
                        attributes:[
                                { name: ACCT_ID, type="string:50", key=true },
                                { name: CUM_TRAN_AMT ,type ="number:30,2" }
                                { name: EVENT_LIST ,type ="CLOB" }
                                ]
                        }
        }
}
