package cxps.events;

import clari5.custom.jasper.WlRequestPayload;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.util.Hocon;
import clari5.platform.util.CxRest;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.net.URLEncoder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;

public class WlRules  {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlRules.class);

    ICXLog cxLog = CXLog.fenter("WlRules");
    static String wlName = "";
    static String wlPermanentAddr = "";
    static String wlMailingAddr = "";
    static String wlBranchPermanentAddr = "";
    static String wlBranchMailingAddr = "";

   WlRules(Hocon hocon){
        wlName = hocon.getString("clari5.custom.wl-rule.NAME");
        wlPermanentAddr = hocon.getString("clari5.custom.wl-rule.PERMANENT_ADDRESS");
        wlMailingAddr = hocon.getString("clari5.custom.wl-rule.MAILING_ADDRESS");
        wlBranchPermanentAddr= hocon.getString("clari5.custom.wl-rule.BRANCH_PERMANENT_ADDRESS");
        wlBranchMailingAddr= hocon.getString("clari5.custom.wl-rule.BRANCH_MAILING_ADDRESS");


    }

    //public JSONObject createRequestJson(String ruleName, String entityType ,String nameChanged,String permanentAddrChanged ,String mailingAddrChanged)throws Exception {
    public JSONObject createRequestJson(String ruleName, NFT_DedupEvent classObj)throws Exception {
        JSONObject jsonObject = new JSONObject();
        JSONArray js=null;
            if (ruleName.equalsIgnoreCase("NAME_MATCH")) {
                if (classObj.getEntityType().equalsIgnoreCase("C") && classObj.getNameChanged().equalsIgnoreCase("Y") ) {
                        js = creatJson(wlName, classObj.getCustName());

                }
                 else if (classObj.getEntityType().equalsIgnoreCase("N")) {
                    js = creatJson(wlName, classObj.getCustName());
                }
            }
            if(ruleName.equalsIgnoreCase("MAILING_ADDRESS_PARTIAL_MATCH")) {
                if (classObj.getEntityType().equalsIgnoreCase("C") && classObj.getCompleteMailingAddrChanged().equalsIgnoreCase("Y")) {
                        js = creatJson(wlMailingAddr, classObj.getCompleteMailingAddr());
                }else if (classObj.getEntityType().equalsIgnoreCase("N")){
                        js = creatJson(wlMailingAddr, classObj.getCompleteMailingAddr());
                }

            } 
	 if(ruleName.equalsIgnoreCase("BRANCH_MAILING_ADDRESS_PARTIAL_MATCH")) { // added for branch_mailing_address
                if (classObj.getEntityType().equalsIgnoreCase("C") && classObj.getCompleteMailingAddrChanged().equalsIgnoreCase("Y")) {
                        js = creatJson(wlBranchMailingAddr, classObj.getCompleteMailingAddr());
                        System.out.println(js + "branch mailing address partical match");

                }else if (classObj.getEntityType().equalsIgnoreCase("N")){
                        js = creatJson(wlBranchMailingAddr, classObj.getCompleteMailingAddr());
                        System.out.println(js + "branch mailing address partical match else if");
                }

            }
	 if(ruleName.equalsIgnoreCase("BRANCH_PERMANENT_ADDRESS_PARTIAL_MATCH")) { // added for branch_permanent_address
               if (classObj.getEntityType().equalsIgnoreCase("C") && classObj.getCompletePerAddrChanged().equalsIgnoreCase("Y")) {
                        js = creatJson(wlBranchPermanentAddr, classObj.getCompletePermanentAddr());
                }else if (classObj.getEntityType().equalsIgnoreCase("N")){
                       js = creatJson(wlBranchPermanentAddr, classObj.getCompletePermanentAddr());
                }
              }

           if(ruleName.equalsIgnoreCase("PERMANENT_ADDRESS_PARTIAL_MATCH")) {
               if (classObj.getEntityType().equalsIgnoreCase("C") && classObj.getCompletePerAddrChanged().equalsIgnoreCase("Y")) {
                        js = creatJson(wlPermanentAddr, classObj.getCompletePermanentAddr());
                }else if (classObj.getEntityType().equalsIgnoreCase("N")){
                       js = creatJson(wlPermanentAddr, classObj.getCompletePermanentAddr());
                }
              }
            jsonObject.put("ruleName", ruleName);
            jsonObject.put("fields", js);
        return jsonObject;

    }
    private JSONArray creatJson(String name , String value)throws  Exception{

        StringBuilder sb = new StringBuilder("[");
        sb.append("{\"name\":\"").append(name).append("\",");
        sb.append("\"value\":\"").append(value).append("\"},");
        String trfsData = sb.toString().substring(0,sb.length()-1)+"]";
        JSONArray js= new JSONArray(trfsData);
        logger.info("Request Json " +js);
        return js;
    }
    public String sendWlRequest(JSONObject obj){
        String response="";
        try {
            logger.info("Request URL "+System.getenv("LOCAL_DN") + "/efm/wlsearch?q="+obj.toString());

            String url = System.getenv("LOCAL_DN")+"/efm/wlsearch?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
            System.out.println("Url is"+ url);
            String instanceId = System.getenv("INSTANCEID");
            String appSecret = System.getenv("APPSECRET");
            HttpResponse resp = null;
            try {
                resp = CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").basicAuth(instanceId, appSecret).asString();
                logger.info("Response Code in WlRules" +resp.getStatus());
                response = (String) resp.getBody();
                logger.info("Response  in WlRules" +response);
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        }catch (Exception e){
            logger.info("Error While getting the matched data from WL" +e.getMessage());
        }
        return response;
    }

}
