clari5.custom.mapper {
        entity {
                NFT_NEWEMPLOYEE {
                       generate = true
                        attributes:[
                                { name: EVENT_ID , type="string:4000" , key=true},
                                { name: SYS_TIME ,type =timestamp }
                                { name: BRANCH_JOINING_DATE ,type =timestamp }
				{ name: EMPLOYEE_ID ,type ="string:500" }
				{ name: DESIGNATION ,type ="string:500" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: BRANCH ,type ="string:500" }
				{ name: EVENTTS ,type =timestamp }
				{ name: NOTICE_PERIOD ,type ="string:50" }
				{ name: CURRENT_BRANCH ,type ="string:500" }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
				{ name: SERVER_ID ,type ="number" }
                            ]
                        }
        }
}

