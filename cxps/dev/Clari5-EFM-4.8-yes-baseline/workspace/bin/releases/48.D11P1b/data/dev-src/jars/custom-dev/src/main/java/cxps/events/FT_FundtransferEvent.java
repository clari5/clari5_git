// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_FUNDTRANSFER", Schema="rice")
public class FT_FundtransferEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=200) public String beneficiaryName;
       @Field(size=200) public String customerSegment;
       @Field(size=20) public String ifscCode;
       @Field(size=200) public String payeeAccounttype;
       @Field(size=200) public String errorDesc;
       @Field(size=10) public String succFailFlg;
       @Field(size=20) public Double balanceAfterTransaction;
       @Field(size=200) public String tranRecurrance;
       @Field public java.sql.Timestamp startDate;
       @Field(size=200) public String accountNumber;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String ipCountry;
       @Field(size=50) public String groupPayeeId;
       @Field(size=200) public String deviceId;
       @Field(size=200) public String frequency;
       @Field(size=200) public String exchangeRate;
       @Field(size=50) public String benfPayeeId;
       @Field(size=200) public String addrNetwork;
       @Field(size=200) public String paymentMode;
       @Field(size=20) public Double transferAmount;
       @Field(size=20) public Double currency;
       @Field(size=2) public String hostId;
       @Field(size=20) public Double settlementAmount;
       @Field(size=200) public String beneficaryUniqueIdentifier1;
       @Field(size=200) public String transactionType;
       @Field(size=20) public String twoFaStatus;
       @Field(size=200) public String ipCity;
       @Field(size=200) public String obdxTransactionName;
       @Field(size=200) public String beneficiaryType1;
       @Field(size=200) public String userId;
       @Field(size=20) public Double debitAccountNumber;
       @Field(size=20) public Double balanceBeforeTransaction;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String obdxModuleName;
       @Field public java.sql.Timestamp benfAddDate;
       @Field public java.sql.Timestamp endDate;
       @Field(size=200) public String riskBand;
       @Field(size=200) public String payeeComb;
       @Field(size=200) public String sessionId;
       @Field(size=10) public String whitelistFlag;


    @JsonIgnore
    public ITable<FT_FundtransferEvent> t = AEF.getITable(this);

	public FT_FundtransferEvent(){}

    public FT_FundtransferEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Fundtransfer");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setBeneficiaryName(json.getString("beneficiary_name"));
            setCustomerSegment(json.getString("customer_segment"));
            setIfscCode(json.getString("ifsc_code"));
            setPayeeAccounttype(json.getString("payee_accounttype"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setBalanceAfterTransaction(EventHelper.toDouble(json.getString("balance_after_transaction")));
            setTranRecurrance(json.getString("tran_recurrance"));
            setStartDate(EventHelper.toTimestamp(json.getString("start_date")));
            setAccountNumber(json.getString("account_number"));
            setCustSegment(json.getString("cust_segment"));
            setIpCountry(json.getString("ip_country"));
            setGroupPayeeId(json.getString("group_payee_id"));
            setDeviceId(json.getString("device_id"));
            setFrequency(json.getString("frequency"));
            setExchangeRate(json.getString("exchange_rate"));
            setBenfPayeeId(json.getString("benf_payee_id"));
            setAddrNetwork(json.getString("addr_network"));
            setPaymentMode(json.getString("payment_mode"));
            setTransferAmount(EventHelper.toDouble(json.getString("transfer_amount")));
            setCurrency(EventHelper.toDouble(json.getString("currency")));
            setHostId(json.getString("host_id"));
            setSettlementAmount(EventHelper.toDouble(json.getString("settlement_amount")));
            setBeneficaryUniqueIdentifier1(json.getString("beneficary_unique_identifier_1"));
            setTransactionType(json.getString("transaction_type"));
            setTwoFaStatus(json.getString("2fa_status"));
            setIpCity(json.getString("ip_city"));
            setObdxTransactionName(json.getString("obdx_transaction_name"));
            setBeneficiaryType1(json.getString("beneficiary_type1"));
            setUserId(json.getString("user_id"));
            setDebitAccountNumber(EventHelper.toDouble(json.getString("debit_account_number")));
            setBalanceBeforeTransaction(EventHelper.toDouble(json.getString("balance_before_transaction")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setObdxModuleName(json.getString("obdx_module_name"));
            setEndDate(EventHelper.toTimestamp(json.getString("end_date")));
            setSessionId(json.getString("session_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setBenfAddDate(cxps.events.CustomFieldDerivator.getBeneficiarydetails(this));setRiskBand(cxps.events.CustomFieldDerivator.checkInstanceofEvent(this));setPayeeComb(cxps.events.CustomFieldDerivator.getPayeecombination(this));setWhitelistFlag(cxps.events.CustomFieldDerivator.getWhitelistflagforAccount(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public String getBeneficiaryName(){ return beneficiaryName; }

    public String getCustomerSegment(){ return customerSegment; }

    public String getIfscCode(){ return ifscCode; }

    public String getPayeeAccounttype(){ return payeeAccounttype; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public Double getBalanceAfterTransaction(){ return balanceAfterTransaction; }

    public String getTranRecurrance(){ return tranRecurrance; }

    public java.sql.Timestamp getStartDate(){ return startDate; }

    public String getAccountNumber(){ return accountNumber; }

    public String getCustSegment(){ return custSegment; }

    public String getIpCountry(){ return ipCountry; }

    public String getGroupPayeeId(){ return groupPayeeId; }

    public String getDeviceId(){ return deviceId; }

    public String getFrequency(){ return frequency; }

    public String getExchangeRate(){ return exchangeRate; }

    public String getBenfPayeeId(){ return benfPayeeId; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getPaymentMode(){ return paymentMode; }

    public Double getTransferAmount(){ return transferAmount; }

    public Double getCurrency(){ return currency; }

    public String getHostId(){ return hostId; }

    public Double getSettlementAmount(){ return settlementAmount; }

    public String getBeneficaryUniqueIdentifier1(){ return beneficaryUniqueIdentifier1; }

    public String getTransactionType(){ return transactionType; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public String getIpCity(){ return ipCity; }

    public String getObdxTransactionName(){ return obdxTransactionName; }

    public String getBeneficiaryType1(){ return beneficiaryType1; }

    public String getUserId(){ return userId; }

    public Double getDebitAccountNumber(){ return debitAccountNumber; }

    public Double getBalanceBeforeTransaction(){ return balanceBeforeTransaction; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getObdxModuleName(){ return obdxModuleName; }

    public java.sql.Timestamp getEndDate(){ return endDate; }

    public String getSessionId(){ return sessionId; }
    public java.sql.Timestamp getBenfAddDate(){ return benfAddDate; }

    public String getRiskBand(){ return riskBand; }

    public String getPayeeComb(){ return payeeComb; }

    public String getWhitelistFlag(){ return whitelistFlag; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setBeneficiaryName(String val){ this.beneficiaryName = val; }
    public void setCustomerSegment(String val){ this.customerSegment = val; }
    public void setIfscCode(String val){ this.ifscCode = val; }
    public void setPayeeAccounttype(String val){ this.payeeAccounttype = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setBalanceAfterTransaction(Double val){ this.balanceAfterTransaction = val; }
    public void setTranRecurrance(String val){ this.tranRecurrance = val; }
    public void setStartDate(java.sql.Timestamp val){ this.startDate = val; }
    public void setAccountNumber(String val){ this.accountNumber = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setGroupPayeeId(String val){ this.groupPayeeId = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setFrequency(String val){ this.frequency = val; }
    public void setExchangeRate(String val){ this.exchangeRate = val; }
    public void setBenfPayeeId(String val){ this.benfPayeeId = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setPaymentMode(String val){ this.paymentMode = val; }
    public void setTransferAmount(Double val){ this.transferAmount = val; }
    public void setCurrency(Double val){ this.currency = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSettlementAmount(Double val){ this.settlementAmount = val; }
    public void setBeneficaryUniqueIdentifier1(String val){ this.beneficaryUniqueIdentifier1 = val; }
    public void setTransactionType(String val){ this.transactionType = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setObdxTransactionName(String val){ this.obdxTransactionName = val; }
    public void setBeneficiaryType1(String val){ this.beneficiaryType1 = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setDebitAccountNumber(Double val){ this.debitAccountNumber = val; }
    public void setBalanceBeforeTransaction(Double val){ this.balanceBeforeTransaction = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setObdxModuleName(String val){ this.obdxModuleName = val; }
    public void setEndDate(java.sql.Timestamp val){ this.endDate = val; }
    public void setSessionId(String val){ this.sessionId = val; }
    public void setBenfAddDate(java.sql.Timestamp val){ this.benfAddDate = val; }
    public void setRiskBand(String val){ this.riskBand = val; }
    public void setPayeeComb(String val){ this.payeeComb = val; }
    public void setWhitelistFlag(String val){ this.whitelistFlag = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_FundtransferEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountNumber);
        //wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.payeeComb);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        String cxCifId;
        String hostCustKey = getCustId();
        if (null != hostCustKey && hostCustKey.trim().length() > 0) {
            cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
        } else {
            cxCifId = h.getCxCifIdGivenCxAcctKey(session, accountKey);
        }
        WorkspaceInfo wi = new WorkspaceInfo("Account", accountKey);
        if (h.isValidCxKey(WorkspaceName.CUSTOMER, cxCifId)) {
            wi.addParam("cxCifID", cxCifId);
        }
        wsInfoSet.add(wi);
        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Fundtransfer");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Fundtransfer");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
