package clari5.custom.services;

import clari5.platform.util.Hocon;

/**
 * @author shishir
 * @since 10/01/2018
 */
public class KTKMailingServices extends MailServices {

    public KTKMailingServices(SMTPMailServices smtpMailServices, Hocon hocon) {
        configure(hocon);
        super.smtpMailServices =smtpMailServices;
    }


}
