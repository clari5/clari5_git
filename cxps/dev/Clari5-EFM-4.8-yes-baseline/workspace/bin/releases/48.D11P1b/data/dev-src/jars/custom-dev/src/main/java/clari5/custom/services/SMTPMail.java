package clari5.custom.services;

import cxps.apex.utils.CxpsLogger;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author shishir
 * @since 10/01/2018
 */

public class SMTPMail implements SMTPMailServices {
    private static CxpsLogger logger = CxpsLogger.getLogger(SMTPMail.class);
    MailFields mailFields;
    final Properties props = new Properties();

    public SMTPMail() {
        mailFields = null;
    }

    public SMTPMail(MailFields mailFields) {
        setMailFields(mailFields);
    }

    protected void setMailProps() {
        setMailProps(mailFields);
    }

    protected void setMailProps(MailFields mailFields) {

        props.put("mail.smtp.host", mailFields.getSmtpHost());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", mailFields.getSmtpPort());
    }

    /**
     * @param mailFields
     * @return 0 -failed to sent mail and 1- mail has sent successfully
     */
    @Override
    public int sendIndvMail(MailFields mailFields) {
	System.out.println("TEST");
        MimeMessage message = getMailMessage(mailFields);

        try {
            String receiverEmails = mailFields.getTo();
            InternetAddress[] parse = InternetAddress.parse(receiverEmails, true);

            /**
             * setFrom is to be set mandatorily otherwise transport will pick default.
             * address as system (userName@hostname)
             */
            message.setFrom(new InternetAddress(mailFields.getSender()));
            message.setRecipients(Message.RecipientType.TO, parse);
            message.setSubject(mailFields.getSubject());
            message.setText(mailFields.getMessage());

            Transport.send(message);
            logger.debug("Message has sent successfully");

            return 1;

        } catch (AddressException ae) {
            logger.error("failed to split email ids recieverEmails [" + mailFields.getTo() + "]");
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        } finally {
            props.clear();

        }
        return 0;
    }

    @Override
    public int sendMailToMulUser(MailFields mailFields) {

        MimeMessage message = getMailMessage(mailFields);

        try {
            String receiverEmails = mailFields.getTo();

            /**
             * setFrom is to be set mandatorily otherwise transport will pick default.
             * address as system (userName@hostname)
             */
            message.setFrom(new InternetAddress(mailFields.getSender()));
            message.setRecipients(Message.RecipientType.TO, splitMailIds(receiverEmails));
            message.setSubject(mailFields.getSubject());
            message.setText(mailFields.getMessage());

            Transport.send(message);
            logger.debug("Message has sent successfully");

            return 1;

        } catch (AddressException ae) {
            logger.error("failed to split email ids recieverEmails [" + mailFields.getTo() + "]");
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        } finally {
            props.clear();

        }
        return 0;
    }

    @Override
    public int sendMailwithCC(MailFields mailFields) {


        MimeMessage message = getMailMessage(mailFields);

        try {
            String receiverEmails = mailFields.getTo();

            /**
             * setFrom is to be set mandatorily otherwise transport will pick default.
             * address as system (userName@hostname)
             */
            message.setFrom(new InternetAddress(mailFields.getSender()));
            message.setRecipients(Message.RecipientType.TO, receiverEmails);
            message.setRecipients(Message.RecipientType.CC, splitMailIds(mailFields.getCc()));
            message.setSubject(mailFields.getSubject());
            message.setText(mailFields.getMessage());

            Transport.send(message);
            logger.debug("Message has sent successfully");

            return 1;

        } catch (AddressException ae) {
            logger.error("failed to split email ids recieverEmails [" + mailFields.getCc() + "]");
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        } finally {
            props.clear();

        }
        return 0;
    }

    @Override
    public int sendMailMulUsrCC(MailFields mailFields) {

        MimeMessage message = getMailMessage(mailFields);

        try {

            /**
             * setFrom is to be set mandatorily otherwise transport will pick default.
             * address as system (userName@hostname)
             */
            message.setFrom(new InternetAddress(mailFields.getSender()));
            message.setRecipients(Message.RecipientType.TO, splitMailIds(mailFields.getTo()));
            message.setRecipients(Message.RecipientType.CC, splitMailIds(mailFields.getCc()));
            message.setSubject(mailFields.getSubject());
            message.setText(mailFields.getMessage());

            Transport.send(message);
            logger.debug("Message has sent successfully");

            return 1;

        } catch (AddressException ae) {
            logger.error("failed to split email ids recieverEmails [" + mailFields.getCc() + "]");
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        } finally {
            props.clear();

        }
        return 0;
    }

    public MailFields getMailFields() {
        return mailFields;
    }

    public void setMailFields(MailFields mailFields) {
        this.mailFields = mailFields;
    }


    public MimeMessage getMailMessage(MailFields mailFields) {

        setMailFields(mailFields);
        setMailProps();

        return getMimeMessage();
    }

    public MimeMessage getMimeMessage() {

        if (null == mailFields) throw new NullPointerException("mail fields is not set " + mailFields.toString());

        logger.debug("Sending mail ,Data is [" + mailFields.toString() + "]");

        final String sender = mailFields.getSender();
        final String senderPass = mailFields.getSenderMailPass();

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(sender, senderPass);
                    }
                });

        return new MimeMessage(session);
    }

    private InternetAddress[] splitMailIds(String mailIDs) throws AddressException {

        String[] recipientList = mailIDs.split(",");
        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
        int counter = 0;
        for (String recipient : recipientList) {
            recipientAddress[counter] = new InternetAddress(recipient.trim());
            counter++;
        }
        return recipientAddress;
    }
}
