package clari5.natm.calculation;

import clari5.custom.yes.integration.config.BepCon;
import clari5.custom.yes.integration.db.DBTask;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.util.concurrent.ConcurrentLinkedQueue;

/*****
 *    @author : Suryakant
 *    @since : 07/02/2020
 */

public class AmountCalculationDaemon extends CxpsRunnable implements ICxResource {

    private static CxpsLogger logger = CxpsLogger.getLogger(AmountCalculationDaemon.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
       try {
           clari5.natm.calculation.BepCon.getConfig();
           clari5.natm.calculation.BepCon.updateServerIDInfoInController();
       }catch (Exception e){
           e.printStackTrace();
       }
        return AmountCalculation.getCustomData();
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.info("Processing data for amount calculation");

        if (o instanceof ConcurrentLinkedQueue) {
            ConcurrentLinkedQueue<JSONObject> queue = (ConcurrentLinkedQueue<JSONObject>) o;
            logger.info("Size of queue to process the Data"+queue.size());
            try (RDBMSSession session = Rdbms.getAppSession()) {
                try (CxConnection connection = session.getCxConnection()) {

                    while (!queue.isEmpty()) {
                       
                        JSONObject jsonObject = queue.poll();
                        ThresholdCalculation.getDataAccountWise(jsonObject,connection);
                    }
                    connection.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void configure(Hocon h) {
        logger.debug("Natm Data fetched Configured");
    }

}
