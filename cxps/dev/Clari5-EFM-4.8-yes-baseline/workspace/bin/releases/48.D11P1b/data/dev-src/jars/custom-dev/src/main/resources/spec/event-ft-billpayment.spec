cxps.events.event.ft-billpayment{
  table-name : EVENT_FT_BILLPAYMENT
  event-mnemonic: BIP
  workspaces : {
    CUSTOMER: cust_id,
    ACCOUNT: accountnumber
  }
  event-attributes : {
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        biller-category: {db : true ,raw_name : biller_category ,type: "string:20" }
        billerid: {db : true ,raw_name : billerid ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        unique-biller-relationship-number: {db : true ,raw_name : unique_biller_relationship_number ,type: "string:200"}
        paymenttype: {db : true ,raw_name : paymenttype ,type: "string:200"}
        paymentmode: {db : true ,raw_name : paymentmode ,type: "string:200"}
        amount: {db : true ,raw_name : amount ,type: "number:20,3"}
        accountnumber: {db : true ,raw_name : accountnumber ,type: "string:200"}
        autopay-amount-flag: {db : true ,raw_name : autopay_amount_flag ,type: "number:20,3"}
        schedule-pay-start-date: {db : true ,raw_name : schedule_pay_start_date ,type: timestamp} 
        schedule-pay-end-date: {db : true ,raw_name : schedule_pay_end_date ,type: timestamp}
        frequency: {db : true ,raw_name : frequency ,type: "string:200"}
        obdx-ref-number: {db : true ,raw_name : obdx_ref_number ,type: "string:200"}
        biller-type: {db : true ,raw_name : biller_type ,type: "string:200"}
        accountbalance: {db : true ,raw_name : accountbalance ,type: "number:20,3"}
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
        risk-band:{db : true ,raw_name :risk_band ,type : "string:200" ,derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}
        

}
}

