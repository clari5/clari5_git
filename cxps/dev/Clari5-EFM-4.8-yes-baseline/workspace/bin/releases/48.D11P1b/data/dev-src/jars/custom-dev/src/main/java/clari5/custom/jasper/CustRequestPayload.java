package clari5.custom.jasper;

import clari5.custom.yes.db.NewModifiedCustomers;
import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import clari5.platform.util.Hocon;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CustRequestPayload {
    private static final CxpsLogger logger = CxpsLogger.getLogger(CustRequestPayload.class);
    NewModifiedCustomers newModifiedCustomers;
    Map<String, Set<String>> matchedCustId = new HashMap<>();
    CustMatch custMatch =null;
    static Hocon hocon;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("wl-rules-name.conf");
    }

    List<String> rulesList =null;

    public Queue<CxJson> setWlReqValues(NewModifiedCustomers newModifiedCustomers, String type) {
        custMatch = new CustMatch();
        custMatch.setId(newModifiedCustomers.getId());
        custMatch.setCust_id(newModifiedCustomers.getCustId());
        this.newModifiedCustomers = newModifiedCustomers;
        Queue<CxJson> reqList = new ConcurrentLinkedQueue<>();
        if (type.equalsIgnoreCase("PAN")){
            rulesList =  Arrays.asList(new String[]{ "NAME_MAILINGADDR","NAME_PERMANENTADDR","NAME_DOB","NAME_DOI"});
        }else if (type.equalsIgnoreCase("PASSPORT")){
            rulesList =  Arrays.asList(new String[]{ "NAME_MAILINGADDR","NAME_PERMANENTADDR","NAME_DOB","NAME_PAN","NAME_MOBILE"});
        }
        for (String ruleName : rulesList) {
            reqList.add(createWlRes(ruleName));
        }
        logger.info("WL Request Record " + reqList.toString());
        return reqList;
    }



    private CxJson createWlRes(String ruleName) {
        Hocon hoc = hocon.get("wl-reports-rules").get("attributes");
        List<String> attributes = hoc.getStringList(ruleName);
        logger.info("The attributes fetched " + attributes);
        StringBuilder buildReq = new StringBuilder("{\"ruleName\":\"" + ruleName + "\",");
        buildReq.append("\"fields\":[");
        for (int i = 0; i < attributes.size(); i++) {
            String key = attributes.get(i);


            switch (key) {
                case "name":
                    buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getCustName() + "\"");
                    break;
                case "pan":
                    buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getPan() + "\"");
                    break;
                case "cust_dob":
                    if (newModifiedCustomers.getDob()!=null) {
                        buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getDob() + " 00:00:00.0\"");
                    }
                    else
                        buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getDob() + "\"");
                    break;
                case "doi":
                    if (newModifiedCustomers.getDateOfIncorporation()!=null){
                    buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getDateOfIncorporation() + " 00:00:00.0\"");}
                    else
                        buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getDateOfIncorporation() + "\"");
                    break;
                case "mailing_address":
                    buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getAddress() + "\"");
                    break;
                case "permanent_address":
                    buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getAddress() + "\"");
                    break;
                /*case "telNolast8":
                    buildReq.append("{\"telNolast8\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getTelNumber().substring(newModifiedCustomers.getTelNumber().length()-8) + "\"");
                    break;
                case "telNo":
                    buildReq.append("{\"telNo\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getTelNumber() + "\"");
                    break;*/

                case "mobile_no":
                    if (!StringUtils.isNullOrEmpty(newModifiedCustomers.getMobile()))
                    buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" + newModifiedCustomers.getMobile().substring(newModifiedCustomers.getMobile().length()-10) + "\"");
                    else
                        buildReq.append("{\"name\":\"" + key + "\",\"value\" :\"" +newModifiedCustomers.getMobile() + "\"");
                    break;

            }
            if (i != attributes.size() - 1) buildReq.append("},");
            else buildReq.append("}");

        }
        buildReq.append("]}");

        CxJson cxJson = null;
        try {
            cxJson = CxJson.parse(buildReq.toString());
        } catch (IOException io) {
            logger.error(" [WlRequestPayload.setWlReqValues ] Failed to parse json " + buildReq.toString() + "\n Message " + io.getMessage());
        }
        return cxJson;
    }

    public CustMatch sendBatchWlReq(Queue queue) {
        List<String> responseList = new ArrayList<>();
        matchedCustId.clear();
        while (!queue.isEmpty()) {
            sendWlRequest((CxJson) queue.poll(), responseList);
        }
        custMatch.setMatchResult(matchedCustId);
       return custMatch;
    }

    public void sendWlRequest(CxJson obj, List<String> responseList) {

        logger.info("Request URL " + System.getenv("LOCAL_DN") + "/efm/wlsearch?q=" + obj.toString());
        String response = "";
        try {
            String url = System.getenv("LOCAL_DN") + "/efm/wlsearch?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
            String instanceId = System.getenv("INSTANCEID");
            String appSecret = System.getenv("APPSECRET");
            HttpResponse resp;
            resp = CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").basicAuth(instanceId, appSecret).asString();
            response = (String) resp.getBody();

            logger.info("WL Respose data " + response);

            if (response != null && !response.isEmpty()) {
                getMatchedList(getMatchedCustId(CxJson.parse(response)), obj.getString("ruleName"));
            }


        } catch (UnirestException e) {
            e.printStackTrace();

        } catch (Exception e) {
            logger.error("Error While getting the matched data from WL" + e.getMessage() + "\n Cause " + e.getCause());
        }


    }

    public List<String> getMatchedCustId(CxJson cxJson) {
        List<String> matchedCustId = new ArrayList<>();
        CxJson matchedRecords = cxJson.get("matchedResults").get("matchedRecords");

        if (matchedRecords.isArray()&& matchedRecords!=null) {
            Iterator<CxJson> records = matchedRecords.iterator();
            while (records.hasNext()) {
                CxJson matchedRecord = records.next();
                matchedCustId.add(matchedRecord.getString("id"));
            }
        }
        logger.info("Matched CustId ["+matchedCustId+"]");
        return matchedCustId;
    }


    public Map<String, Set<String>> getMatchedList(List<String> matchCustId, String ruleName) {
        Set<String> rule = new HashSet<>();
        rule.clear();
        if (matchedCustId.isEmpty()) {

            for (String custId : matchCustId) {
                matchedCustId.put(custId, rule);
                matchedCustId.get(custId).add(ruleName);
            }
        } else {

            matchedCustId.forEach((key, value) -> {
                logger.info("Key in getMatchedList : " + key + " Value in getMatchedList : " + value);
                if (matchCustId.contains(key)) {
                    value.add(ruleName);
                    matchedCustId.put(key, value);
                } else matchedCustId.put(key, value);
            });

        }
        return matchedCustId;
    }

}



