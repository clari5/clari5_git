package cxps.events;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.Statement;
import java.util.HashSet;

import java.util.Set;
import clari5.custom.Queries;
import clari5.custom.dedupe.RetryCustomer;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.QueryMapper;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.utils.StringUtils;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import java.sql.Timestamp;
import oracle.sql.DATE;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import static clari5.rdbms.Rdbms.getAppConnection;
import clari5.rdbms.Rdbms;
import com.maxmind.geolite.MaxmindUtil;

import javax.mail.Session;
import clari5.platform.util.Hocon;


/*

Created by Deepak
*/

public class CustomFieldDerivator {

   
    static Hocon ignoreWorkspaceKey;
    static Hocon hocon;
    static ArrayList<String> ignrWsKeys = new ArrayList<>();
    static String selectAmountQuery;
    static String updateAmountQuery;
    static String updateNAmountQuery;
    static String productType;
    static String accntOpenDate;
    static String whiteList;
    static int maxAttempt;
    static String productCode;
    static String tranNumonicCode;
    static int numOfDays;

    static {
        ignoreWorkspaceKey = new Hocon();
        ignoreWorkspaceKey.loadFromContext("workspace-derivator.conf");
        ignrWsKeys.addAll(ignoreWorkspaceKey.getStringList("derivators.ignoredEntities"));
        hocon = new Hocon();
        hocon.loadFromContext("natmQueryFilter.conf");
        selectAmountQuery = hocon.getString("selectAmountQuery");
        updateAmountQuery = hocon.getString("updateAmountQuery");
        updateNAmountQuery = hocon.getString("updateNAmountQuery");
        productType = hocon.getString("productType");
        accntOpenDate = hocon.getString("accntOpenDate");
        whiteList = hocon.getString("whiteListFlag");
        maxAttempt = hocon.getInt("maxAttempt");
        productCode = hocon.getString("product_code");
        tranNumonicCode = hocon.getString("tran_mnemonic");
        numOfDays = hocon.getInt("numOfDays");
    }

    private static final CxpsLogger logger = CxpsLogger.getLogger(CustomFieldDerivator.class);
    private boolean isAccountClosed;

    public static String getHolidaydate(FT_CoreAcctTxnEvent event)
    {
        Timestamp time  = new Timestamp(event.sysTime.getTime());
        Date date = new Date(time.getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
        String result = simpleDateFormat.format(date);
        return result;
    }

    public static String getChannelFromTxnNumonicCode(String txnNumonicCode) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String channel = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select TRANSACTION_CHANNEL from TRANSACTION_NUMONIC_CODE where TRANSACTION_NUMONIC_CODE = ?");
            st.setString(1, txnNumonicCode);
            rs = st.executeQuery();
            if (rs.next()) {
                channel = rs.getString("TRANSACTION_CHANNEL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        System.out.println("getChannelFromTxnNumonicCode : " + channel);
        return channel;
    }

public static String getIsAccountClosed(NFT_DedupEvent nft_dedupEvent) {
        java.sql.Date accountOpenDt, accountClosedDt;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = Rdbms.getAppConnection();

            st = con.prepareStatement("SELECT MAX(ACCCOUNT_OPEN_DATE) AS ACCCOUNT_OPEN_DATE, MAX(ACCOUNT_CLOSED_DATE) AS ACCOUNT_CLOSED_DATE FROM ACCOUNT_MASTER WHERE CUSTOMER_ID = ?");
            st.setString(1, nft_dedupEvent.getCustId());
            System.out.println("SELECT MAX(ACCCOUNT_OPEN_DATE) AS ACCCOUNT_OPEN_DATE, MAX(ACCOUNT_CLOSED_DATE) AS ACCOUNT_CLOSED_DATE FROM ACCOUNT_MASTER WHERE CUSTOMER_ID = " + nft_dedupEvent.getCustId());
            rs = st.executeQuery();
            if (rs.next()) {
                System.out.println("Account open date" + rs.getDate("ACCCOUNT_OPEN_DATE"));
                System.out.println("Account closed date" + rs.getDate("ACCOUNT_CLOSED_DATE"));

                accountOpenDt = new java.sql.Date(rs.getDate("ACCCOUNT_OPEN_DATE").getTime());

                nft_dedupEvent.isAccountClosed = "false";

                if ( rs.getDate("ACCOUNT_CLOSED_DATE") == null) {
                    nft_dedupEvent.isAccountClosed = "false";
                } else {
                    accountOpenDt = new java.sql.Date(rs.getDate("ACCCOUNT_OPEN_DATE").getTime());

                    accountClosedDt = new java.sql.Date(rs.getDate("ACCOUNT_CLOSED_DATE").getTime());
                    long diff = accountOpenDt.getTime() - accountClosedDt.getTime();
                    long diffDays = diff / (24 * 60 * 60 * 1000);
                    System.out.println("total number of days -- " + diffDays);

                    if (diffDays > 365) {
                        nft_dedupEvent.isAccountClosed = "false";
                        System.out.println("Account false " + nft_dedupEvent.isAccountClosed);
                    } else if(diffDays>=0 && diffDays <=365){
                        nft_dedupEvent.isAccountClosed = "true";
                        System.out.println("Account true " + nft_dedupEvent.isAccountClosed);

                    }
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return nft_dedupEvent.isAccountClosed;
    }

public static double getCumTxnAmtCode(FT_CoreAcctTxnEvent event) {
        double thresholdAmount = 0;
//        String[] product_arr = productCode.split(",");
//        String[] tran_Numonic_Arr = tranNumonicCode.split(",");
//
//        List productList = Arrays.asList(product_arr);
//        List tranNumonicList = Arrays.asList(tran_Numonic_Arr);
//
//
//        double sumAmount = 0;
//        double cumAmount = 0;
//        double count = 0;
//        double thresholdAmount = 0;
//        String whiteListFlag = "";
//        String product_Type = "";
//        Date acccount_open_date = new Date(1800 - 01 - 01);
//
//        Calendar cal = Calendar.getInstance();
//        Date curr_date = new Date(event.sysTime.getTime());
//
//        cal.setTime(curr_date);
//        cal.add(Calendar.DATE, -numOfDays); // as per query filter, need sysDate minus 200 days required.
//        java.util.Date limitDate = cal.getTime();
//
//        if (event.drCr.equalsIgnoreCase("C") && !productList.contains(event.productCode) && !tranNumonicList.contains(event.trannumonicCode)) {
//            logger.info("Satisfy the condition for DRCR,PRODUCTCODE and Trannumoniccode ");
//
//            for (int attemptCount = 0; attemptCount < maxAttempt; attemptCount++) {
//                logger.info("number of attempt" + attemptCount);
//
//                try (Connection connection = Rdbms.getAppConnection();
//                     PreparedStatement selectPsmt = connection.prepareStatement(selectAmountQuery);
//                     PreparedStatement updatepsmt = connection.prepareStatement(updateAmountQuery);
//                     PreparedStatement ustate = connection.prepareStatement(updateNAmountQuery);
//                     PreparedStatement productTypestmt = connection.prepareStatement(productType);
//                     PreparedStatement AccntOpenstmt = connection.prepareStatement(accntOpenDate);
//                     PreparedStatement whiteListstmt = connection.prepareStatement(whiteList);) {
//
//                    whiteListstmt.setString(1, event.acctId);
//                    ResultSet resultWhiteList = whiteListstmt.executeQuery();
//
//                    if (resultWhiteList.next() && resultWhiteList.getString("ACCOUNT_ID") != null) {
//                        logger.info("Account_Id is whitelisted");
//                        return 0;
//                    } else {
//                        whiteListFlag = "N";
//                        event.whiteListFlag = "N";
//                    }
//
//                    productTypestmt.setString(1, event.productCode);
//                    ResultSet resultSet1 = productTypestmt.executeQuery();
//                    if (resultSet1.next() && resultSet1.getString("PRODUCT_TYPE") != null) {
//                        product_Type = resultSet1.getString("PRODUCT_TYPE");
//                        if (!product_Type.equalsIgnoreCase("SAVING") && !product_Type.equalsIgnoreCase("CURRENT")) {
//                            logger.info("Product_type is other than saving and current");
//                            return 0;
//                        }else{
//                            event.productTypeFlag = "Y";
//                        }
//                    }
//
//                    AccntOpenstmt.setString(1, event.acctId);
//                    ResultSet resultSet2 = AccntOpenstmt.executeQuery();
//                    if (resultSet2.next() && resultSet2.getDate("ACCCOUNT_OPEN_DATE") != null) {
//                        acccount_open_date = resultSet2.getDate("ACCCOUNT_OPEN_DATE");
//                        logger.info("Account open date of account is: "+acccount_open_date);
//                    }
//                    if ((acccount_open_date.after(limitDate) && acccount_open_date.before(curr_date)) || (acccount_open_date.equals(limitDate)) || (acccount_open_date.equals(curr_date))){
//                       event.accountOpenDateFlag="Y";
//                       logger.info("accountOpenDateFlag is Y");
//                    }
//
//
//                    if ((product_Type.equalsIgnoreCase("SAVING") || product_Type.equalsIgnoreCase("CURRENT")) && (event.accountOpenDateFlag).equalsIgnoreCase("Y")&& whiteListFlag.equals("N")) {
//                        logger.info("Account open satisfy the condition");
//
//                        selectPsmt.setString(1, event.acctId);
//                        ResultSet resultSet = selectPsmt.executeQuery();
//
//                        if (resultSet.next()) {
//                            sumAmount = resultSet.getDouble("CUM_TXN_AMT"); //100
//                            count = resultSet.getDouble("TS_COUNT");// 4
//                            thresholdAmount = resultSet.getDouble("UPDATED_THRESHOLD"); //400
//                        }
//
//                        event.cumTxnAmt = event.tranAmt + sumAmount; //200
//                        cumAmount = event.cumTxnAmt; //200
//
//                        if (thresholdAmount <= cumAmount) {
//
//                            updatepsmt.setDouble(1, 2 * (cumAmount));
//                            updatepsmt.setDouble(2, count + 1);
//                            long sys = System.currentTimeMillis();
//                            updatepsmt.setTimestamp(3, new Timestamp(sys));
//                            updatepsmt.setString(4, event.acctId);
//                            updatepsmt.setDouble(5, count);
//                            updatepsmt.executeUpdate();
//                            connection.commit();
//
//
//                            return thresholdAmount;
//                        } else {
//                            ustate.setDouble(1, cumAmount);
//                            ustate.setDouble(2, count + 1);
//                            long sys = System.currentTimeMillis();
//                            ustate.setTimestamp(3, new Timestamp(sys));
//                            ustate.setString(4, event.acctId);
//                            ustate.setDouble(5, count);
//                            ustate.executeUpdate();
//                            connection.commit();
//                            return thresholdAmount;
//                        }
//
//                    } else {
//                        return thresholdAmount;
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//
//                }
//
//            }
//        }

        return thresholdAmount;
    }

    public static String getSubtranmodeFromTxnNumonicCode(String txnNumonicCode) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String subtranmode = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select SUB_TRAN_MODE from TRANSACTION_NUMONIC_CODE where TRANSACTION_NUMONIC_CODE = ?");
            st.setString(1, txnNumonicCode);
            rs = st.executeQuery();
            if (rs.next()) {
                subtranmode = rs.getString("SUB_TRAN_MODE");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        System.out.println("getSubtranmodeFromTxnNumonicCode : " + subtranmode);
        return subtranmode;
    }


    public static String getaddedentitydetails(FT_FundtransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        Statement stmt = null;
        ResultSet result = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String addedentity = "";
        try {
            con = Rdbms.getAppConnection();
            stmt = con.createStatement();
//        String query = "select EVENT_DATE from EVENT_NFT_CREDENTIALCHANGE  where to_char(EVENT_DATE, 'YYYY-MM-DD HH:MI:SS') < ? and CUST_ID=?";
            String query = "select EVENT_DATE from EVENT_NFT_CREDENTIALCHANGE where EVENT_DATE >= to_timestamp(?,'DD/MM/YY HH24:MI:SS.FF') - 2 and CUST_ID=?";
            psmt = con.prepareStatement(query);
            psmt.setString(1, simpleDateFormat.format(event.eventDate));
            psmt.setString(2, event.custId);
            result = stmt.executeQuery(query);
            if (result.next()) {
                addedentity = "Y";
            } else addedentity = "N";

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }

                if (con != null) {
                    con.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return addedentity;
    }


    public static String deriveNonHomeBranch(FT_CoreAcctTxnEvent event) {
        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Connection con = null;
        PreparedStatement ps = null, ps1 = null, ps2 = null, ps3 = null;
        ResultSet result = null, result1 = null, result2 = null, result3 = null;
        int count = 0;
        try {
            con = Rdbms.getAppConnection();
            QueryMapper mapper = QueryMapper.getInstance(Rdbms.getAppDbType(), "clari5.custom");
            String userId = event.getUserId();
            String custId = event.getCustId();
            if (userId != null && !"".equals(userId)) {
                String loadquery = mapper.get(Queries.SELECT_TYPE);
                String loadbranchquery = mapper.get(Queries.SELECT_BRANCH);
                ps = con.prepareStatement(loadquery);
                ps.setString(1, userId);
                result = ps.executeQuery();
                if (result.next()) {
                    event.setUserType("MANUAL");
                    ps3 = con.prepareStatement(loadbranchquery);
                    ps3.setString(1, result.getString("BRANCH"));
                    result3 = ps3.executeQuery();
                    if (result3.next()) {
                        return "Y";
                    } else {
                        return "N";
                    }
                } else {
                    event.setUserType("SYSTEM");
                }
                //TODO set default value for non home branch if above condition fails
            }

            //TODO move this out in a separate function for better readability.
            if (custId != null && !"".equals(custId)) {
                String loadcustType = mapper.get(Queries.SELECT_CUST_TYPE);
                ps1 = con.prepareStatement(loadcustType);
                ps1.setString(1, custId);
                result1 = ps1.executeQuery();
                String custType = "";
                while (result1.next()) {
                    custType = result1.getString("CUSTOMER_TYPE");
                    String loadcustTyp = mapper.get(Queries.SELECT_CUST_FLG);
                    ps2 = con.prepareStatement(loadcustTyp);
                    ps2.setString(1, custType);
                    result2 = ps2.executeQuery();
                    if (result2.next()) {
                        String flag = result2.getString("FLAG");
                        event.setCustType(flag);
                    }
                }
            }

        } catch (Exception e) {

        } finally {
            try {
                if (result != null) result.close();
                if (ps != null) ps.close();
            } catch (Exception e) {
                return "";
            }
            try {
                if (con != null) con.close();
            } catch (Exception e) {
                return "";
            }
        }
        return "";
    }

    public static String getTransactionModeFromTxnNumonicCode(String trannumonicCode) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String transactionmode = "";
        int trancode = Integer.parseInt(trannumonicCode);
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select TRANSACTION_MODE from MNEMONIC_MASTER_NATM where COD_TXN_MNEMONIC = ?");
            st.setInt(1, trancode);
            rs = st.executeQuery();
            if (rs.next()) {
                transactionmode = rs.getString("TRANSACTION_MODE");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        System.out.println("getTransactionModeFromTxnNumonicCode : " + transactionmode);
        return transactionmode;
    }

    public static String getWhitelistflagforAccount(FT_FundtransferEvent event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String watchlist_flag = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select * from ENTITY_WHITELIST where ACCOUNTNO = ? and IFSC = ?");
            st.setString(1, event.accountNumber);
            st.setString(2, event.ifscCode);
            rs = st.executeQuery();
            if (rs.next()) {

                watchlist_flag = "Y";
            } else {
                watchlist_flag = "N";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return watchlist_flag;
    }

    public static String deriveUserType(FT_CoreAcctTxnEvent event) {
        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Connection con = null;
        PreparedStatement ps = null, ps1 = null;
        ResultSet result = null, result1 = null;
        String userType = "";
        try {
            con = Rdbms.getAppConnection();
            String userId = event.getUserId();
            ps = con.prepareStatement("select * from EMPLOYEEINFORMATION where EMPLOYEE_ID = ?");
            ps.setString(1, userId);
            result = ps.executeQuery();
            if (result.next()) {
                userType = "MANUAL";
            } else {
                userType = "SYSTEM";
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (result != null) result.close();
                if (ps != null) ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (con != null) con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return userType;
    }

    public static String getPayeecombination(FT_FundtransferEvent event) {

        String Combination_key = "";
        try {
            Combination_key = event.beneficiaryType1 + event.beneficaryUniqueIdentifier1;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return Combination_key;
    }

    public static String getRiskBand(String session_id, String cust_id) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String risk_band = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select RISK_BAND from EVENT_NFT_IBLOGIN where SESSION_ID = ? and CUST_ID = ?");
            st.setString(1, session_id);
            st.setString(2, cust_id);
            rs = st.executeQuery();
            while (rs.next()) {
                risk_band = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return risk_band;

    }

    public static String checkInstanceofEvent(Object event) {

        if (event instanceof FT_FundtransferEvent)
            return (String) getRiskBand(((FT_FundtransferEvent) event).getSessionId(), ((FT_FundtransferEvent) event).getCustId());

        if (event instanceof FT_BillpaymentEvent)
            return (String) getRiskBand(((FT_BillpaymentEvent) event).getSessionId(), ((FT_BillpaymentEvent) event).getCustId());

        if (event instanceof FT_EipoapplicationEvent)
            return (String) getRiskBand(((FT_EipoapplicationEvent) event).getSessionId(), ((FT_EipoapplicationEvent) event).getCustId());

        if (event instanceof FT_GenericenEvent)
            return (String) getRiskBand(((FT_GenericenEvent) event).getSessionId(), ((FT_GenericenEvent) event).getCustId());

        if (event instanceof FT_EpiEvent)
            return (String) getRiskBand(((FT_EpiEvent) event).getSessionId(), ((FT_EpiEvent) event).getCustId());

        if (event instanceof FT_WmsEvent)
            return (String) getRiskBand(((FT_WmsEvent) event).getSessionId(), ((FT_WmsEvent) event).getCustId());

        if (event instanceof NFT_CredentialChangeEvent)
            return (String) getRiskBand(((NFT_CredentialChangeEvent) event).getSessionId(), ((NFT_CredentialChangeEvent) event).getCustId());

        if (event instanceof NFT_BeneficiaryEvent)
            return (String) getRiskBand(((NFT_BeneficiaryEvent) event).getSessionId(), ((NFT_BeneficiaryEvent) event).getCustId());

        return null;
    }

    public static double getDistanceBetweenIP(NFT_IbLoginEvent event) {
        // con = Rdbms.getAppConnection();
        double distance = 0;
        String current_ip = "";
        // If cust_id is not available in the DISTANCE_IP table
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                if (checkAvailablecustomer(event.custId).equals("N")) {
                    try (PreparedStatement statement = connection.prepareStatement("INSERT INTO DISTANCE_IP (CUST_ID,CURRENT_IP) VALUES (?,?)")) {
                        statement.setString(1, event.custId);
                        statement.setString(2, event.getAddrNetwork());
                        statement.executeQuery();
                        connection.commit();
                    }
                } else {
                    if (checkAvailablecustomer(event.custId).equals("Y")) {
                        //if cust_id is available in DISTANCE_IP table
                        try (PreparedStatement ps = connection.prepareStatement("SELECT CURRENT_IP FROM DISTANCE_IP WHERE CUST_ID=?")) {
                            ps.setString(1, event.custId);
                            try (ResultSet rs = ps.executeQuery()) {
                                while (rs.next()) {
                                    current_ip = rs.getString("CURRENT_IP");
                                }
                                if (current_ip == null || current_ip.equalsIgnoreCase("null")) {
                                    //when customer is available in Distance_IP but current_IP is null.
                                    try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE DISTANCE_IP SET CURRENT_IP =? WHERE CUST_ID=?")) {
                                        preparedStatement.setString(1, event.getAddrNetwork());
                                        preparedStatement.setString(2, event.custId);
                                        preparedStatement.executeUpdate();
                                        connection.commit();
                                    }
                                } else {
                                    //swap current_ip with last_ip if CURRENT_IP is not null
                                    try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE DISTANCE_IP SET CURRENT_IP =?,LAST_IP=? WHERE CUST_ID=?")) {
                                        preparedStatement.setString(1, event.getAddrNetwork());
                                        preparedStatement.setString(2, current_ip);
                                        preparedStatement.setString(3, event.custId);
                                        preparedStatement.executeUpdate();
                                        connection.commit();
                                    }
                                }
                                distance = MaxmindUtil.getDistance(current_ip, event.getAddrNetwork());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.debug("Error processing data for event id: ---> " + event.getEventId() +
                        " and cust id from login event: ---> " + event.getCustId());
                e.printStackTrace();
            }
        }
        return distance;
    }

    public static String checkAvailablecustomer(String customer_id) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String Available_flag = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select * from DISTANCE_IP where CUST_ID = ?");
            st.setString(1, customer_id);
            rs = st.executeQuery();
            if (rs.next()) {
                Available_flag = "Y";
            } else {
                Available_flag = "N";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return Available_flag;

    }

    public static String SaveBeneficiaryDb(NFT_BeneficiaryEvent event) {
        // Method for beneficiary updated date
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String Benf_flag = "";
        try {
            con = Rdbms.getAppConnection();

            if (((NFT_BeneficiaryEvent) event).getCustId() != null && ((NFT_BeneficiaryEvent) event).getGroupPayeeId() != null && ((NFT_BeneficiaryEvent) event).getBenfFlag1() != null) {
                if (!event.benfFlag1.equals("E") || event.benfFlag1 != null) {

                    if (event.benfFlag1.equals("N")) {
                        InsertBeneficiarydetails(event);
                        Benf_flag = "created";
                    }
                    if (event.benfFlag1.equals("M")) {
                        UpdateBeneficiaryDetails(event);
                        Benf_flag = "updated";
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        return Benf_flag;

    }

    public static java.sql.Timestamp getBeneficiarydetails(FT_FundtransferEvent event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Timestamp result = null;

        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select CREATED_ON from BENEFICIARY_UDATE where GROUP_PAYEE_ID = ? and BENF_PAYEE_ID = ? and CUST_ID = ?");
            st.setString(1, event.groupPayeeId);
            st.setString(2, event.benfPayeeId);
            st.setString(3, event.custId);
            rs = st.executeQuery();
            while (rs.next()) {
                result = rs.getTimestamp(1);
            }
            return result != null ? result : new Timestamp(10000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {

            }
        }
        return result != null ? result : new Timestamp(10000);
    }

    public static void InsertBeneficiarydetails(Object event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            con = Rdbms.getAppConnection();
            if (event instanceof NFT_BeneficiaryEvent) {
                st = con.prepareStatement("INSERT INTO BENEFICIARY_UDATE (PAYEE_ACCTTYPE, CUST_ID, BENF_UNIQUE_VALUE, GROUP_PAYEE_ID, BENF_PAYEE_ID, CREATED_ON, BENF_FLAG, BENEFICIARY_NICKNAME, UPDATED_ON, BENF_TYPE) VALUES(?,?,?,?,?,?,?,?,?,?)");

                int i = 1;
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getPayeeAccttype1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getCustId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfUniqueValue1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getGroupPayeeId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfPayeeId1());
                st.setTimestamp(i++, ((NFT_BeneficiaryEvent) event).getSysTime());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfFlag1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfNickname1());
                st.setTimestamp(i++, ((NFT_BeneficiaryEvent) event).getSysTime());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfType1());

                st.executeUpdate();
                con.commit();
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
    }

    public static void UpdateBeneficiaryDetails(Object event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            con = Rdbms.getAppConnection();
            if (event instanceof NFT_BeneficiaryEvent) {
                st = con.prepareStatement("UPDATE BENEFICIARY_UDATE SET PAYEE_ACCTTYPE=?, BENF_UNIQUE_VALUE=?, BENF_FLAG=?, BENEFICIARY_NICKNAME=?, UPDATED_ON=?, BENF_TYPE=? WHERE CUST_ID=? AND GROUP_PAYEE_ID=? AND BENF_PAYEE_ID=?");

                int i = 1;
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getPayeeAccttype1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfUniqueValue1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfFlag1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfNickname1());
                st.setTimestamp(i++, ((NFT_BeneficiaryEvent) event).getSysTime());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfType1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getCustId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getGroupPayeeId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfPayeeId1());

                st.executeUpdate();
                con.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }

        }
    }

}
