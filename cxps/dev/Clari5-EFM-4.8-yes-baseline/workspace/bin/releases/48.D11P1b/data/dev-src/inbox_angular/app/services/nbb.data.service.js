"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var NbbDataService = (function () {
    function NbbDataService() {
        this.url = 'inbox/InboxTemp?';
        this.user = "SYSTEM";
        this.tag = [];
        this.showscreen = true;
        this.screenshow = true;
        this.showavilscreen = true;
        this.searchscenario = false;
        this.i = false;
        this.e = false;
        this.in = false;
        this.en = false;
        this.officeLIST = [
            { Id: 1, officeID: 1, officename: "Create Inbox Ticket Automatically", checked: false },
            { Id: 2, officeID: 2, officename: "Email Notification", checked: false },
        ];
    }
    NbbDataService.prototype.clear = function () {
        this.sname = "";
        this.ssubject = "";
        this.sinbox = "";
        this.subject = "";
        this.message = "";
        this.icheck = false;
        this.eventlist = [];
        this.noTemp = "";
        this.scenario = "";
        this.officeLIST[0].checked = false;
        this.officeLIST[1].checked = false;
    };
    return NbbDataService;
}());
NbbDataService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], NbbDataService);
exports.NbbDataService = NbbDataService;
//# sourceMappingURL=nbb.data.service.js.map