// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_IbLoginEventMapper extends EventMapper<NFT_IbLoginEvent> {

public NFT_IbLoginEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_IbLoginEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_IbLoginEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_IbLoginEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_IbLoginEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_IbLoginEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_IbLoginEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAddrNetLocal());
            preparedStatement.setString(i++, obj.getTwoFAStatus());
            preparedStatement.setString(i++, obj.getAgent());
            preparedStatement.setString(i++, obj.getDeviceBinding());
            preparedStatement.setString(i++, obj.getIPCountry());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getCookieEnabledFlag());
            preparedStatement.setString(i++, obj.getTimeSlot());
            preparedStatement.setString(i++, obj.getJavaEnabledFlag());
            preparedStatement.setString(i++, obj.getUserLoanOnlyFlag());
            preparedStatement.setString(i++, obj.getDistBtCurrLastUsedIp());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getIPCity());
            preparedStatement.setString(i++, obj.getDeviceType());
            preparedStatement.setString(i++, obj.getBrowserTimeZone());
            preparedStatement.setTimestamp(i++, obj.getDeviceIdDate());
            preparedStatement.setString(i++, obj.getBrowserName());
            preparedStatement.setString(i++, obj.getTwoFAMode());
            preparedStatement.setString(i++, obj.getSystemPassword());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getFirstLogin());
            preparedStatement.setString(i++, obj.getScreenRes());
            preparedStatement.setString(i++, obj.getUserCCOnlyFlag());
            preparedStatement.setString(i++, obj.getSessionId());
            preparedStatement.setString(i++, obj.getBrowserLang());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setDouble(i++, obj.getDistance());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getLoginIdStatus());
            preparedStatement.setString(i++, obj.getCustSegment());
            preparedStatement.setString(i++, obj.getJSStatus());
            preparedStatement.setString(i++, obj.getJavaStatus());
            preparedStatement.setString(i++, obj.getSystemUserId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getMobileEmulator());
            preparedStatement.setString(i++, obj.getTimeZone());
            preparedStatement.setString(i++, obj.getTimeZoneOffset());
            preparedStatement.setString(i++, obj.getBrowserPlugin());
            preparedStatement.setString(i++, obj.getDeviceIdCount());
            preparedStatement.setString(i++, obj.getOs());
            preparedStatement.setString(i++, obj.getScreenResolution());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getMobileOS());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setTimestamp(i++, obj.getCookieDateTimeStamp());
            preparedStatement.setString(i++, obj.getBrowserVersion());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setString(i++, obj.getRiskBand());
            preparedStatement.setString(i++, obj.getUserAgent());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_IBLOGIN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_IbLoginEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_IbLoginEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_IBLOGIN"));
        putList = new ArrayList<>();

        for (NFT_IbLoginEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ADDR_NET_LOCAL",  obj.getAddrNetLocal());
            p = this.insert(p, "TWO_F_A_STATUS",  obj.getTwoFAStatus());
            p = this.insert(p, "AGENT",  obj.getAgent());
            p = this.insert(p, "DEVICE_BINDING",  obj.getDeviceBinding());
            p = this.insert(p, "I_P_COUNTRY",  obj.getIPCountry());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "COOKIE_ENABLED_FLAG",  obj.getCookieEnabledFlag());
            p = this.insert(p, "TIME_SLOT",  obj.getTimeSlot());
            p = this.insert(p, "JAVA_ENABLED_FLAG",  obj.getJavaEnabledFlag());
            p = this.insert(p, "USER_LOAN_ONLY_FLAG",  obj.getUserLoanOnlyFlag());
            p = this.insert(p, "DIST_BT_CURR_LAST_USED_IP",  obj.getDistBtCurrLastUsedIp());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "I_P_CITY",  obj.getIPCity());
            p = this.insert(p, "DEVICE_TYPE",  obj.getDeviceType());
            p = this.insert(p, "BROWSER_TIME_ZONE",  obj.getBrowserTimeZone());
            p = this.insert(p, "DEVICE_ID_DATE", String.valueOf(obj.getDeviceIdDate()));
            p = this.insert(p, "BROWSER_NAME",  obj.getBrowserName());
            p = this.insert(p, "TWO_F_A_MODE",  obj.getTwoFAMode());
            p = this.insert(p, "SYSTEM_PASSWORD",  obj.getSystemPassword());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "FIRST_LOGIN",  obj.getFirstLogin());
            p = this.insert(p, "SCREEN_RES",  obj.getScreenRes());
            p = this.insert(p, "USER_C_C_ONLY_FLAG",  obj.getUserCCOnlyFlag());
            p = this.insert(p, "SESSION_ID",  obj.getSessionId());
            p = this.insert(p, "BROWSER_LANG",  obj.getBrowserLang());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "DISTANCE", String.valueOf(obj.getDistance()));
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "LOGIN_ID_STATUS",  obj.getLoginIdStatus());
            p = this.insert(p, "CUST_SEGMENT",  obj.getCustSegment());
            p = this.insert(p, "J_S_STATUS",  obj.getJSStatus());
            p = this.insert(p, "JAVA_STATUS",  obj.getJavaStatus());
            p = this.insert(p, "SYSTEM_USER_ID",  obj.getSystemUserId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "MOBILE_EMULATOR",  obj.getMobileEmulator());
            p = this.insert(p, "TIME_ZONE",  obj.getTimeZone());
            p = this.insert(p, "TIME_ZONE_OFFSET",  obj.getTimeZoneOffset());
            p = this.insert(p, "BROWSER_PLUGIN",  obj.getBrowserPlugin());
            p = this.insert(p, "DEVICE_ID_COUNT",  obj.getDeviceIdCount());
            p = this.insert(p, "OS",  obj.getOs());
            p = this.insert(p, "SCREEN_RESOLUTION",  obj.getScreenResolution());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "MOBILE_O_S",  obj.getMobileOS());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "COOKIE_DATE_TIME_STAMP", String.valueOf(obj.getCookieDateTimeStamp()));
            p = this.insert(p, "BROWSER_VERSION",  obj.getBrowserVersion());
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "RISK_BAND",  obj.getRiskBand());
            p = this.insert(p, "USER_AGENT",  obj.getUserAgent());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_IBLOGIN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_IBLOGIN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_IBLOGIN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_IbLoginEvent obj = new NFT_IbLoginEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAddrNetLocal(rs.getString("ADDR_NET_LOCAL"));
    obj.setTwoFAStatus(rs.getString("TWO_F_A_STATUS"));
    obj.setAgent(rs.getString("AGENT"));
    obj.setDeviceBinding(rs.getString("DEVICE_BINDING"));
    obj.setIPCountry(rs.getString("I_P_COUNTRY"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setCookieEnabledFlag(rs.getString("COOKIE_ENABLED_FLAG"));
    obj.setTimeSlot(rs.getString("TIME_SLOT"));
    obj.setJavaEnabledFlag(rs.getString("JAVA_ENABLED_FLAG"));
    obj.setUserLoanOnlyFlag(rs.getString("USER_LOAN_ONLY_FLAG"));
    obj.setDistBtCurrLastUsedIp(rs.getString("DIST_BT_CURR_LAST_USED_IP"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setIPCity(rs.getString("I_P_CITY"));
    obj.setDeviceType(rs.getString("DEVICE_TYPE"));
    obj.setBrowserTimeZone(rs.getString("BROWSER_TIME_ZONE"));
    obj.setDeviceIdDate(rs.getTimestamp("DEVICE_ID_DATE"));
    obj.setBrowserName(rs.getString("BROWSER_NAME"));
    obj.setTwoFAMode(rs.getString("TWO_F_A_MODE"));
    obj.setSystemPassword(rs.getString("SYSTEM_PASSWORD"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setFirstLogin(rs.getString("FIRST_LOGIN"));
    obj.setScreenRes(rs.getString("SCREEN_RES"));
    obj.setUserCCOnlyFlag(rs.getString("USER_C_C_ONLY_FLAG"));
    obj.setSessionId(rs.getString("SESSION_ID"));
    obj.setBrowserLang(rs.getString("BROWSER_LANG"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setDistance(rs.getDouble("DISTANCE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setLoginIdStatus(rs.getString("LOGIN_ID_STATUS"));
    obj.setCustSegment(rs.getString("CUST_SEGMENT"));
    obj.setJSStatus(rs.getString("J_S_STATUS"));
    obj.setJavaStatus(rs.getString("JAVA_STATUS"));
    obj.setSystemUserId(rs.getString("SYSTEM_USER_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setMobileEmulator(rs.getString("MOBILE_EMULATOR"));
    obj.setTimeZone(rs.getString("TIME_ZONE"));
    obj.setTimeZoneOffset(rs.getString("TIME_ZONE_OFFSET"));
    obj.setBrowserPlugin(rs.getString("BROWSER_PLUGIN"));
    obj.setDeviceIdCount(rs.getString("DEVICE_ID_COUNT"));
    obj.setOs(rs.getString("OS"));
    obj.setScreenResolution(rs.getString("SCREEN_RESOLUTION"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setMobileOS(rs.getString("MOBILE_O_S"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setCookieDateTimeStamp(rs.getTimestamp("COOKIE_DATE_TIME_STAMP"));
    obj.setBrowserVersion(rs.getString("BROWSER_VERSION"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setRiskBand(rs.getString("RISK_BAND"));
    obj.setUserAgent(rs.getString("USER_AGENT"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_IBLOGIN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_IbLoginEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_IbLoginEvent> events;
 NFT_IbLoginEvent obj = new NFT_IbLoginEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_IBLOGIN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_IbLoginEvent obj = new NFT_IbLoginEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAddrNetLocal(getColumnValue(rs, "ADDR_NET_LOCAL"));
            obj.setTwoFAStatus(getColumnValue(rs, "TWO_F_A_STATUS"));
            obj.setAgent(getColumnValue(rs, "AGENT"));
            obj.setDeviceBinding(getColumnValue(rs, "DEVICE_BINDING"));
            obj.setIPCountry(getColumnValue(rs, "I_P_COUNTRY"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setCookieEnabledFlag(getColumnValue(rs, "COOKIE_ENABLED_FLAG"));
            obj.setTimeSlot(getColumnValue(rs, "TIME_SLOT"));
            obj.setJavaEnabledFlag(getColumnValue(rs, "JAVA_ENABLED_FLAG"));
            obj.setUserLoanOnlyFlag(getColumnValue(rs, "USER_LOAN_ONLY_FLAG"));
            obj.setDistBtCurrLastUsedIp(getColumnValue(rs, "DIST_BT_CURR_LAST_USED_IP"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setIPCity(getColumnValue(rs, "I_P_CITY"));
            obj.setDeviceType(getColumnValue(rs, "DEVICE_TYPE"));
            obj.setBrowserTimeZone(getColumnValue(rs, "BROWSER_TIME_ZONE"));
            obj.setDeviceIdDate(EventHelper.toTimestamp(getColumnValue(rs, "DEVICE_ID_DATE")));
            obj.setBrowserName(getColumnValue(rs, "BROWSER_NAME"));
            obj.setTwoFAMode(getColumnValue(rs, "TWO_F_A_MODE"));
            obj.setSystemPassword(getColumnValue(rs, "SYSTEM_PASSWORD"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setFirstLogin(getColumnValue(rs, "FIRST_LOGIN"));
            obj.setScreenRes(getColumnValue(rs, "SCREEN_RES"));
            obj.setUserCCOnlyFlag(getColumnValue(rs, "USER_C_C_ONLY_FLAG"));
            obj.setSessionId(getColumnValue(rs, "SESSION_ID"));
            obj.setBrowserLang(getColumnValue(rs, "BROWSER_LANG"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setDistance(EventHelper.toDouble(getColumnValue(rs, "DISTANCE")));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setLoginIdStatus(getColumnValue(rs, "LOGIN_ID_STATUS"));
            obj.setCustSegment(getColumnValue(rs, "CUST_SEGMENT"));
            obj.setJSStatus(getColumnValue(rs, "J_S_STATUS"));
            obj.setJavaStatus(getColumnValue(rs, "JAVA_STATUS"));
            obj.setSystemUserId(getColumnValue(rs, "SYSTEM_USER_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setMobileEmulator(getColumnValue(rs, "MOBILE_EMULATOR"));
            obj.setTimeZone(getColumnValue(rs, "TIME_ZONE"));
            obj.setTimeZoneOffset(getColumnValue(rs, "TIME_ZONE_OFFSET"));
            obj.setBrowserPlugin(getColumnValue(rs, "BROWSER_PLUGIN"));
            obj.setDeviceIdCount(getColumnValue(rs, "DEVICE_ID_COUNT"));
            obj.setOs(getColumnValue(rs, "OS"));
            obj.setScreenResolution(getColumnValue(rs, "SCREEN_RESOLUTION"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setMobileOS(getColumnValue(rs, "MOBILE_O_S"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setCookieDateTimeStamp(EventHelper.toTimestamp(getColumnValue(rs, "COOKIE_DATE_TIME_STAMP")));
            obj.setBrowserVersion(getColumnValue(rs, "BROWSER_VERSION"));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setRiskBand(getColumnValue(rs, "RISK_BAND"));
            obj.setUserAgent(getColumnValue(rs, "USER_AGENT"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_IBLOGIN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_IBLOGIN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ADDR_NET_LOCAL\",\"TWO_F_A_STATUS\",\"AGENT\",\"DEVICE_BINDING\",\"I_P_COUNTRY\",\"ERROR_DESC\",\"SOURCE\",\"DEVICE_ID\",\"COOKIE_ENABLED_FLAG\",\"TIME_SLOT\",\"JAVA_ENABLED_FLAG\",\"USER_LOAN_ONLY_FLAG\",\"DIST_BT_CURR_LAST_USED_IP\",\"ADDR_NETWORK\",\"I_P_CITY\",\"DEVICE_TYPE\",\"BROWSER_TIME_ZONE\",\"DEVICE_ID_DATE\",\"BROWSER_NAME\",\"TWO_F_A_MODE\",\"SYSTEM_PASSWORD\",\"ERROR_CODE\",\"FIRST_LOGIN\",\"SCREEN_RES\",\"USER_C_C_ONLY_FLAG\",\"SESSION_ID\",\"BROWSER_LANG\",\"COUNTRY_CODE\",\"DISTANCE\",\"KEYS\",\"SUCC_FAIL_FLG\",\"LOGIN_ID_STATUS\",\"CUST_SEGMENT\",\"J_S_STATUS\",\"JAVA_STATUS\",\"SYSTEM_USER_ID\",\"SYS_TIME\",\"MOBILE_EMULATOR\",\"TIME_ZONE\",\"TIME_ZONE_OFFSET\",\"BROWSER_PLUGIN\",\"DEVICE_ID_COUNT\",\"OS\",\"SCREEN_RESOLUTION\",\"USER_ID\",\"MOBILE_O_S\",\"CUST_ID\",\"COOKIE_DATE_TIME_STAMP\",\"BROWSER_VERSION\",\"IP_ADDRESS\",\"RISK_BAND\",\"USER_AGENT\"" +
              " FROM EVENT_NFT_IBLOGIN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ADDR_NET_LOCAL],[TWO_F_A_STATUS],[AGENT],[DEVICE_BINDING],[I_P_COUNTRY],[ERROR_DESC],[SOURCE],[DEVICE_ID],[COOKIE_ENABLED_FLAG],[TIME_SLOT],[JAVA_ENABLED_FLAG],[USER_LOAN_ONLY_FLAG],[DIST_BT_CURR_LAST_USED_IP],[ADDR_NETWORK],[I_P_CITY],[DEVICE_TYPE],[BROWSER_TIME_ZONE],[DEVICE_ID_DATE],[BROWSER_NAME],[TWO_F_A_MODE],[SYSTEM_PASSWORD],[ERROR_CODE],[FIRST_LOGIN],[SCREEN_RES],[USER_C_C_ONLY_FLAG],[SESSION_ID],[BROWSER_LANG],[COUNTRY_CODE],[DISTANCE],[KEYS],[SUCC_FAIL_FLG],[LOGIN_ID_STATUS],[CUST_SEGMENT],[J_S_STATUS],[JAVA_STATUS],[SYSTEM_USER_ID],[SYS_TIME],[MOBILE_EMULATOR],[TIME_ZONE],[TIME_ZONE_OFFSET],[BROWSER_PLUGIN],[DEVICE_ID_COUNT],[OS],[SCREEN_RESOLUTION],[USER_ID],[MOBILE_O_S],[CUST_ID],[COOKIE_DATE_TIME_STAMP],[BROWSER_VERSION],[IP_ADDRESS],[RISK_BAND],[USER_AGENT]" +
              " FROM EVENT_NFT_IBLOGIN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ADDR_NET_LOCAL`,`TWO_F_A_STATUS`,`AGENT`,`DEVICE_BINDING`,`I_P_COUNTRY`,`ERROR_DESC`,`SOURCE`,`DEVICE_ID`,`COOKIE_ENABLED_FLAG`,`TIME_SLOT`,`JAVA_ENABLED_FLAG`,`USER_LOAN_ONLY_FLAG`,`DIST_BT_CURR_LAST_USED_IP`,`ADDR_NETWORK`,`I_P_CITY`,`DEVICE_TYPE`,`BROWSER_TIME_ZONE`,`DEVICE_ID_DATE`,`BROWSER_NAME`,`TWO_F_A_MODE`,`SYSTEM_PASSWORD`,`ERROR_CODE`,`FIRST_LOGIN`,`SCREEN_RES`,`USER_C_C_ONLY_FLAG`,`SESSION_ID`,`BROWSER_LANG`,`COUNTRY_CODE`,`DISTANCE`,`KEYS`,`SUCC_FAIL_FLG`,`LOGIN_ID_STATUS`,`CUST_SEGMENT`,`J_S_STATUS`,`JAVA_STATUS`,`SYSTEM_USER_ID`,`SYS_TIME`,`MOBILE_EMULATOR`,`TIME_ZONE`,`TIME_ZONE_OFFSET`,`BROWSER_PLUGIN`,`DEVICE_ID_COUNT`,`OS`,`SCREEN_RESOLUTION`,`USER_ID`,`MOBILE_O_S`,`CUST_ID`,`COOKIE_DATE_TIME_STAMP`,`BROWSER_VERSION`,`IP_ADDRESS`,`RISK_BAND`,`USER_AGENT`" +
              " FROM EVENT_NFT_IBLOGIN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_IBLOGIN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ADDR_NET_LOCAL\",\"TWO_F_A_STATUS\",\"AGENT\",\"DEVICE_BINDING\",\"I_P_COUNTRY\",\"ERROR_DESC\",\"SOURCE\",\"DEVICE_ID\",\"COOKIE_ENABLED_FLAG\",\"TIME_SLOT\",\"JAVA_ENABLED_FLAG\",\"USER_LOAN_ONLY_FLAG\",\"DIST_BT_CURR_LAST_USED_IP\",\"ADDR_NETWORK\",\"I_P_CITY\",\"DEVICE_TYPE\",\"BROWSER_TIME_ZONE\",\"DEVICE_ID_DATE\",\"BROWSER_NAME\",\"TWO_F_A_MODE\",\"SYSTEM_PASSWORD\",\"ERROR_CODE\",\"FIRST_LOGIN\",\"SCREEN_RES\",\"USER_C_C_ONLY_FLAG\",\"SESSION_ID\",\"BROWSER_LANG\",\"COUNTRY_CODE\",\"DISTANCE\",\"KEYS\",\"SUCC_FAIL_FLG\",\"LOGIN_ID_STATUS\",\"CUST_SEGMENT\",\"J_S_STATUS\",\"JAVA_STATUS\",\"SYSTEM_USER_ID\",\"SYS_TIME\",\"MOBILE_EMULATOR\",\"TIME_ZONE\",\"TIME_ZONE_OFFSET\",\"BROWSER_PLUGIN\",\"DEVICE_ID_COUNT\",\"OS\",\"SCREEN_RESOLUTION\",\"USER_ID\",\"MOBILE_O_S\",\"CUST_ID\",\"COOKIE_DATE_TIME_STAMP\",\"BROWSER_VERSION\",\"IP_ADDRESS\",\"RISK_BAND\",\"USER_AGENT\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ADDR_NET_LOCAL],[TWO_F_A_STATUS],[AGENT],[DEVICE_BINDING],[I_P_COUNTRY],[ERROR_DESC],[SOURCE],[DEVICE_ID],[COOKIE_ENABLED_FLAG],[TIME_SLOT],[JAVA_ENABLED_FLAG],[USER_LOAN_ONLY_FLAG],[DIST_BT_CURR_LAST_USED_IP],[ADDR_NETWORK],[I_P_CITY],[DEVICE_TYPE],[BROWSER_TIME_ZONE],[DEVICE_ID_DATE],[BROWSER_NAME],[TWO_F_A_MODE],[SYSTEM_PASSWORD],[ERROR_CODE],[FIRST_LOGIN],[SCREEN_RES],[USER_C_C_ONLY_FLAG],[SESSION_ID],[BROWSER_LANG],[COUNTRY_CODE],[DISTANCE],[KEYS],[SUCC_FAIL_FLG],[LOGIN_ID_STATUS],[CUST_SEGMENT],[J_S_STATUS],[JAVA_STATUS],[SYSTEM_USER_ID],[SYS_TIME],[MOBILE_EMULATOR],[TIME_ZONE],[TIME_ZONE_OFFSET],[BROWSER_PLUGIN],[DEVICE_ID_COUNT],[OS],[SCREEN_RESOLUTION],[USER_ID],[MOBILE_O_S],[CUST_ID],[COOKIE_DATE_TIME_STAMP],[BROWSER_VERSION],[IP_ADDRESS],[RISK_BAND],[USER_AGENT]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ADDR_NET_LOCAL`,`TWO_F_A_STATUS`,`AGENT`,`DEVICE_BINDING`,`I_P_COUNTRY`,`ERROR_DESC`,`SOURCE`,`DEVICE_ID`,`COOKIE_ENABLED_FLAG`,`TIME_SLOT`,`JAVA_ENABLED_FLAG`,`USER_LOAN_ONLY_FLAG`,`DIST_BT_CURR_LAST_USED_IP`,`ADDR_NETWORK`,`I_P_CITY`,`DEVICE_TYPE`,`BROWSER_TIME_ZONE`,`DEVICE_ID_DATE`,`BROWSER_NAME`,`TWO_F_A_MODE`,`SYSTEM_PASSWORD`,`ERROR_CODE`,`FIRST_LOGIN`,`SCREEN_RES`,`USER_C_C_ONLY_FLAG`,`SESSION_ID`,`BROWSER_LANG`,`COUNTRY_CODE`,`DISTANCE`,`KEYS`,`SUCC_FAIL_FLG`,`LOGIN_ID_STATUS`,`CUST_SEGMENT`,`J_S_STATUS`,`JAVA_STATUS`,`SYSTEM_USER_ID`,`SYS_TIME`,`MOBILE_EMULATOR`,`TIME_ZONE`,`TIME_ZONE_OFFSET`,`BROWSER_PLUGIN`,`DEVICE_ID_COUNT`,`OS`,`SCREEN_RESOLUTION`,`USER_ID`,`MOBILE_O_S`,`CUST_ID`,`COOKIE_DATE_TIME_STAMP`,`BROWSER_VERSION`,`IP_ADDRESS`,`RISK_BAND`,`USER_AGENT`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

