package clari5.custom.dedupe;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.*;

public class CustIdListWL {

    private static final CxpsLogger logger = CxpsLogger.getLogger(CustIdListWL.class);

    protected static LinkedHashMap<String, LinkedList<String>> getResultSetMap(String wlResponse, String ruleName) {
        LinkedList<String> customerIdfromWlResp = new LinkedList<>();
        LinkedHashMap<String, LinkedList<String>> custIdQueryMap = new LinkedHashMap<>();
        JSONObject jsonObject = new JSONObject(wlResponse);
        if (jsonObject.has("matchedResults")) {
            JSONArray responseArray = jsonObject.getJSONObject("matchedResults").has("matchedRecords") ?
                    jsonObject.getJSONObject("matchedResults").getJSONArray("matchedRecords") : new JSONArray();
            responseArray.forEach(response -> {
                if (response instanceof JSONObject) {
                    LinkedList<String> queryValue = new LinkedList<>();
                /*customerIdfromWlResp.add(((JSONObject) response).has("id")?
                        ((JSONObject) response).getString("id") : "");*/
                    queryValue.add(ruleName);
                    queryValue.add(((JSONObject) response).has("score") ? ((JSONObject) response).getString("score") : "");
                    JSONArray fieldArray = ((JSONObject) response).has("fields") ?
                            ((JSONObject) response).getJSONArray("fields") : new JSONArray();
                    fieldArray.forEach(fieldArrayJson -> {
                        if (fieldArrayJson instanceof JSONObject) {
                            queryValue.add((((JSONObject) fieldArrayJson).has("name")
                                    && ((JSONObject) fieldArrayJson).has("value")) ?
                                    (((JSONObject) fieldArrayJson).getString("name") + ":" + ((JSONObject) fieldArrayJson).getString("value")) : "");
                        }
                    });
                    custIdQueryMap.put(((JSONObject) response).has("id") ?
                            ((JSONObject) response).getString("id") : "", !queryValue.isEmpty() ? queryValue : new LinkedList<>());
                }
            });
        }
        return custIdQueryMap;
    }

    protected static String formatDate(String dateInstr) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");
        Date date = null;
        try {
            date = (Date) formatter.parse(dateInstr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
        return newFormat.format(date);
    }
}