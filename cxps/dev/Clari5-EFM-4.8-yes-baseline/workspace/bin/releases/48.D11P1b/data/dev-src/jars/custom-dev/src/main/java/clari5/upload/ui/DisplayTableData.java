        package clari5.upload.ui;

        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import javax.servlet.http.HttpSession;
        import java.io.IOException;
        import java.io.PrintWriter;
        import java.sql.*;
        import clari5.rdbms.Rdbms;
        /**
         * Servlet implementation class DisplayTableData
         */
        @WebServlet("/DisplayTableData")
        public class DisplayTableData extends HttpServlet {
            private static final long serialVersionUID = 1L;



            protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                doEither(request,response);
            }
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                doEither(request,response);
            }

            private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException {
               response.setContentType("text/html");
                PrintWriter out=response.getWriter();
                out.println("<html>");
                out.println("<head>");
                out.println("<link rel='stylesheet' type='text/css' href='css/pagination.css'>");
                out.println(" <link rel='stylesheet' href='/cdn/ext/css/bootstrap/bootstrap.min.css'>");
                out.println(" <link rel='stylesheet' href='css/main.css'>");
                out.println("<script type='text/javascript' src='/cdn/ext/bootstrap/4.1.3/js/bootstrap.min.js'></script>");
                out.println("<script type='text/javascript' src='js/search.js'></script>");
                out.println("<script src='/cdn/ext/bootstrap/4.1.3/js/bootstrap.min.js'></script>");
                out.println("<script type='text/javascript' src='js/paginationScript.js'></script>");
                out.println("<script type='text/javascript' src='js/validate.js'></script>");
                out.println("</head>");
                Connection con = null;
                PreparedStatement ps = null;
                HttpSession session;
                ResultSetMetaData rsMetaData;
                ResultSet rs = null;
                String tableName;
                String head;
                int noOfColumns;
                String columnNames = "";
                String data="";
                try {
                  session = request.getSession();
               /*     if (session == null || session.getAttribute("USERID")== null){
                        request.getRequestDispatcher("/expiry.jsp").forward(request,response);
                    }*/
                    tableName = request.getParameter("table_name");
                    session.setAttribute("myId",tableName);
                    //con = DBConnection.getDBConnection();
                        con = Rdbms.getAppConnection();
                    ps =con.prepareStatement("Select * from "+tableName);
                    rs = ps.executeQuery();
                     rsMetaData = rs.getMetaData();
                    noOfColumns =  rsMetaData.getColumnCount();
                    out.println("<body>");
                    out.println("<div class='row'><div class='col-xs-4'></div>");
                    out.println("<div class='col-xs-4'>");
                    out.println("<h4 font-family: 'Verdana'>Table Selected: "+tableName+"</h4></div>");
                    out.println("<div class='col-xs-4'></div></div>");
                    out.println("<br>");
                    out.println("<div class='row'><div class='col-xs-4'></div>");
                    out.println("<div class='col-xs-4'>");
                    out.println("<input class='form-control' id='filter' placeholder='Enter The Value to Search'/>");
                    out.println("</div>");
                    out.println("<div class='col-xs-4'>");
                    out.println("</div></div><br>");
                    out.println("<div class='row'>");
                    out.println("<div class='col-xs-1'>");
                    out.println("</div>");
                    out.println("<div class='col-xs-8'>");
                    out.println("<form action='DeleteServlet' method='get'>");
                    out.println("<table class='table table-bordered table-hover' id='tablepaging'>");
                    out.println("<thead class='bg-info'>");
                    for (int i = 1; i <= noOfColumns; i++) {
                        head =rsMetaData.getColumnName(i);
                        columnNames += head + ",";
                        out.println("<th>" + head + "</th>");
                    }
                        columnNames += "#";
                    out.println("<th>SELECT</th>");
                    out.println("</thead>");
                    out.println("</tr>");
                        String columns = "";
                    while (rs.next()) {
                        out.println("<tr>");
                        int c = 1;
                        columns = "";
                        while (c <= noOfColumns) {
                             if("date".equals(rs.getMetaData().getColumnTypeName(c).toLowerCase()))
                            {
                                Date s=rs.getDate(c);
                                out.println("<td class='col-md-1'>"+s+"</td>");
                            }
                              else
                              {
                              data = rs.getString(rsMetaData.getColumnName(c));
                              out.println("<td class='col-md-1'>" + data + "</td>");
                              }
                            columns += rs.getString(c) + ",";
                            ++c;
                        }
                        String checkboxValue = columnNames + columns;
                        out.println("<td class='col-md-1'><input type='checkbox'  id='parentId' value=\""+checkboxValue+"\" name='deletebox'></td>");
                        out.println("</tr>");
                        }
                        out.println("</table>");
                        out.println("</div> <div class='col-xs-3'></div> </div>");
                        out.println("<div class='row'><div class='col-xs-6'></div>");
                        out.println("<div class='col-xs-4' id='pageNavPosition'  class='pagination' >");
                        out.println("</div>");
                        out.println("<div class='col-xs-4'>");
                        out.println("</div></div>");
                        out.println("<div class='row'><div class='col-xs-5'></div>");
                        out.println("<div class='col-xs-2'>");
                        out.println("<input type='submit' value='Delete' class='btn btn-info' onclick='return validateCheckbox()'>");
                        out.println("</form>");
                        out.println("</div>");
                        out.println("<div class='col-xs-4'></div></div>");
                        out.println("<br>");
                        out.println("<script type='text/javascript'>");
                        out.println("var pager = new Pager('tablepaging', 5,'pager','pageNavPosition')");
                        out.println("pager.init()");
                        out.println("pager.showPage(1)");
                        out.println("</script>");
                        out.println("</body></html>");


                
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
           /*     catch (ServletException e) {
                    e.printStackTrace();
                }*/
                    finally {
                    try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
                    try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}
                    try { if (rs != null) rs.close();}catch (SQLException ex){ ex.printStackTrace();}
                }
                }

        }
