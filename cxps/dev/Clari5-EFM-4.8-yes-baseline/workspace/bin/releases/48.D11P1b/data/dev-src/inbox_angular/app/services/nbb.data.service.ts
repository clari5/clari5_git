import { Injectable } from '@angular/core';

@Injectable()
export class NbbDataService {
  public url = 'efm/InboxTemp?';
  public user: string = "SYSTEM";
  public tags: any;
  public tag: any = [];
  public avltag: any;
  public eventlist: any;
  public screen: any;
  public scenarios: any;
  public allscenarios: any;
  public showscreen: boolean = true;
  public screenshow: boolean = true;
  public showavilscreen: boolean = true;
  public searchscenario: boolean = false;
  public alert: any;
  public alert1: any;
  public other: any;
  public inbox: any;
  public scenario: any;
  public res: string;
  public test: any;
  public temp: any;
  public message: any;
  public subject: any;
  public icheckbox: any;
  public echeckbox: any;
  public i: any = false;
  public e: any = false;
  public sname: string;
  public ssubject: string;
  public sinbox: string;
  public icheck: any;
  public in: boolean = false;
  public en: boolean = false;
  public noTemp: string;
  public officeLIST: any[] = [
    { Id: 1, officeID: 1, officename: "Create Inbox Ticket Automatically", checked: false },
    { Id: 2, officeID: 2, officename: "Email Notification", checked: false },
  ]



  public clear(): void {
    this.sname = "";
    this.ssubject = "";
    this.sinbox = "";
    this.subject = "";
    this.message = "";
    this.icheck = false;
    this.eventlist = [];
    this.noTemp="";
    this.scenario="";
    this.officeLIST[0].checked = false;
    this.officeLIST[1].checked = false;
  }
  constructor() {
  }
}
