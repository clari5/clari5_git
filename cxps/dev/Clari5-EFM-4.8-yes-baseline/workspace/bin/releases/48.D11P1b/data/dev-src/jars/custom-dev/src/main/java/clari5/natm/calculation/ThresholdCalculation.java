package clari5.natm.calculation;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*****
 *    @author : Suryakant
 *    @since : 07/02/2020
 */

public class ThresholdCalculation {

    private static CxpsLogger logger = CxpsLogger.getLogger(ThresholdCalculation.class);

    static Hocon hocon;
    static String amountQuery;
    static String updateAmount;
    static String cumAmountUpdate;
    static String insertEventList;
    static String updateEventList;
    static String selectEventList;
    static String insertIncident;
    static String eventCheck;
    static String insertBacklogTable;
    static String natmFlagUpdate;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("natmQueryFilter.conf");
        amountQuery = hocon.getString("selectAmountQuery");
        updateAmount = hocon.getString("updateAmountQuery");
        cumAmountUpdate = hocon.getString("updateNAmountQuery");
        insertEventList = hocon.getString("insertAccountEventList");
        updateEventList = hocon.getString("updateAccountEventList");
        selectEventList = hocon.getString("checkAccountEvent");
        insertIncident = hocon.getString("insertIncidentTable");
        eventCheck = hocon.getString("eventCheck");
        insertBacklogTable = hocon.getString("insertEventBacklog");
        natmFlagUpdate = hocon.getString("updateNatmFlag");

    }

    public synchronized static void getDataAccountWise(JSONObject jsonObject, CxConnection connection) {

        logger.info("[ThresholdCalculation: getDataAccountWise() ]Inside the method where we are fetching the data from Account transaction table");

        double cumTxnAmount = 0;
        double count = 0;
        double thresholdAmount = 0;
        double calculatedCumTxnAmount = 0;
        String eventId = null;
        String custId = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(amountQuery)) {
            String acctId = jsonObject.getString("ACCT_ID");
            preparedStatement.setString(1, acctId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                cumTxnAmount = resultSet.getDouble("CUM_TXN_AMT");
                count = resultSet.getDouble("TS_COUNT");
                thresholdAmount = resultSet.getDouble("UPDATED_THRESHOLD");
            }

            calculatedCumTxnAmount = jsonObject.getDouble("TRAN_AMT") + cumTxnAmount;
            eventId = jsonObject.getString("EVENT_ID");
            custId = jsonObject.getString("CUST_ID");
          
            if(thresholdAmount <= calculatedCumTxnAmount){
                logger.info("[ThresholdCalculation: getDataAccountWise()] ThresholdAmount["+thresholdAmount+ "]for"+eventId + "is less than calculated cum txn amt"+calculatedCumTxnAmount);
                updateThresoldAmount(connection,acctId,count,calculatedCumTxnAmount,thresholdAmount,eventId,custId);

            }else{
                logger.info("[ThresholdCalculation: getDataAccountWise()] ThresholdAmount["+thresholdAmount+ "]for"+eventId + "is greater than calculated cum txn amt"+calculatedCumTxnAmount);
                updateCumulativeAmount(connection,acctId,count,calculatedCumTxnAmount,thresholdAmount,eventId);
              
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static  void updateThresoldAmount(CxConnection connection, String acctId, double count, double cumTxnAmount, double thresholdAmount, String eventId, String custId ){

        logger.info("[ThresholdCalculation:updateThresoldAmount()]Inside the method where updating threshold amount");
        try(PreparedStatement preparedStatement = connection.prepareStatement(updateAmount);
            PreparedStatement insertEvent = connection.prepareStatement(insertEventList);
            PreparedStatement updateEvent = connection.prepareStatement(updateEventList);
            PreparedStatement selectEvent = connection.prepareStatement(selectEventList);
            PreparedStatement updateFlag = connection.prepareStatement(natmFlagUpdate);){

            preparedStatement.setDouble(1, 2*(cumTxnAmount));
            preparedStatement.setDouble(2, count+1);
            long sysTime = System.currentTimeMillis();
            preparedStatement.setTimestamp(3,new Timestamp(sysTime));
            preparedStatement.setString(4,acctId);
            preparedStatement.setDouble(5,count);
            preparedStatement.executeUpdate();
            //connection.commit(); // commited for making single commit point

            //checking account is already exist in Account_Event_List Table
            selectEvent.setString(1,acctId);
            ResultSet resultSet = selectEvent.executeQuery();
            //String eventList  = resultSet.getString("EVENT_LIST");

            if(resultSet.next()){
                String eventList  = resultSet.getString("EVENT_LIST");
                //StringBuffer eventIdList = new StringBuffer(eventList);
               String eventIdList = null;

               if(eventList.equals("EMPTY")){
                   eventIdList =  "\"FC_" + eventId + "\"";

               }else {

                   eventIdList = eventList + ",\"FC_" + eventId + "\"";
               }
                eventList = eventIdList;
                String[] eventCheck = eventList.split(",");

                if(isEventAvailable(eventCheck)=="Y") {
                    //Here Alert will be generated and we will add in CL5_Incident table
                    insertIncidentTable(connection, custId, acctId, cumTxnAmount, eventList, eventId);
                    logger.info("[ThresholdCalculation] Data inserted in Incident table successfully");
                    updateEvent.setString(1,"EMPTY");
                    updateEvent.setDouble(2,cumTxnAmount);
                    updateEvent.setString(3,acctId);
                    // make the natm ft core table succes
                    updateFlag.setString(1,"SUCCESS");
                    updateFlag.setString(2,eventId);
                    updateEvent.executeUpdate();
                    logger.info("[ThresholdCalculation] Data Updated in Account Event List with account id"+acctId);
                    updateFlag.executeUpdate();
                    logger.info("[ThresholdCalculation] Data Updated in natm ft core with flag success for"+eventId);

                    connection.commit();

                }else{
                    insertEventBacklog(custId,acctId,cumTxnAmount,eventList,eventId);
                    logger.info("[ThresholdCalculation] Data inserted in natm backlog table successfully as the event is missing from event_ft_coreacct");

                    updateEvent.setString(1,"EMPTY");
                    updateEvent.setDouble(2,cumTxnAmount);
                    updateEvent.setString(3,acctId);
                    // added for updating success

                    updateFlag.setString(1,"SUCCESS");
                    updateFlag.setString(2,eventId);
                    updateEvent.executeUpdate();
                    logger.info("[ThresholdCalculation] Data Updated in Account Event List with account id"+acctId);

                    updateFlag.executeUpdate();
                    logger.info("[ThresholdCalculation] Data Updated in natm ft core with flag success for"+eventId);

                    connection.commit();

                }

            }else{

                //Here we will insert Data in CL5_INCIDENT Table
                String event = "\"FC_"+eventId+"\"";
               // String event = list.toString();
                String[] eventArray = event.split(",");


                if(isEventAvailable(eventArray)=="Y") {

                    insertIncidentTable(connection, custId, acctId, cumTxnAmount, event, eventId);
                    logger.info("[ThresholdCalculation] Data inserted in Incident Table...");

                    insertEvent.setString(1, acctId);
                    insertEvent.setDouble(3, cumTxnAmount);
                    insertEvent.setString(2, "EMPTY");
                    // added for updating success
                    updateFlag.setString(1,"SUCCESS");
                    updateFlag.setString(2,eventId);
                    insertEvent.executeUpdate();
                    logger.info("[ThresholdCalculation] Data inserted in Account EventList Table with account id"+acctId);
                    updateFlag.executeUpdate();
                    logger.info("[ThresholdCalculation] Data Updated in NATM_FT_CORE Table with Flg Success"+eventId);
                    logger.info("[ThresholdCalculation] Transaction Completed");
                    connection.commit();
                }else{
                    insertEventBacklog(custId, acctId,cumTxnAmount, event, eventId);
                    logger.info("[ThresholdCalculation] Data inserted in NATM Event BackLog Table...");

                    insertEvent.setString(1, acctId);
                    insertEvent.setDouble(3, cumTxnAmount);
                    insertEvent.setString(2, "EMPTY");

                    // added for updating success
                    updateFlag.setString(1,"SUCCESS");
                    updateFlag.setString(2,eventId);
                    insertEvent.executeUpdate();
                    logger.info("[ThresholdCalculation] Data inserted in Account EventList Table with account id"+acctId);

                    updateFlag.executeUpdate();
                    logger.info("[ThresholdCalculation]  Data Updated in NATM_FT_CORE Table with Flg Success"+eventId);
                    connection.commit();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public synchronized static void updateCumulativeAmount(CxConnection connection, String acctId, double count, double cumTxnAmount, double thresholdAmount, String eventId){

        logger.info("[ThresholdCalculation:updateCumulativeAmount()]updating the cumulative amount for"+acctId);
        try(PreparedStatement preparedStatement = connection.prepareStatement(cumAmountUpdate);
            PreparedStatement insertEvent = connection.prepareStatement(insertEventList);
            PreparedStatement updateEvent = connection.prepareStatement(updateEventList);
            PreparedStatement selectEvent = connection.prepareStatement(selectEventList);
            PreparedStatement updateFlag = connection.prepareStatement(natmFlagUpdate);

        ){

            preparedStatement.setDouble(1,cumTxnAmount);
            preparedStatement.setDouble(2,count+1);
            long sysTime = System.currentTimeMillis();
            preparedStatement.setTimestamp(3, new Timestamp(sysTime));
            preparedStatement.setString(4,acctId);
            preparedStatement.setDouble(5,count);
            preparedStatement.executeUpdate();
            //connection.commit();

            selectEvent.setString(1,acctId);
            ResultSet resultSet = selectEvent.executeQuery();


            if(resultSet.next()){
                String eventList  = resultSet.getString("EVENT_LIST");
                //StringBuffer eventIdList = new StringBuffer(eventList);
                String eventIdList = null;

                if(eventList.equals("EMPTY")){
                    eventIdList = "\"FC_" + eventId + "\"";
                }else {
                    eventIdList =  eventList + ",\"FC_" + eventId + "\"";
                }
                updateEvent.setString(1,eventIdList);
                updateEvent.setDouble(2,0);
                updateEvent.setString(3,acctId);

                updateFlag.setString(1,"SUCCESS");
                updateFlag.setString(2,eventId);
                updateEvent.executeUpdate();
                // added later for making natm ft core table
                logger.info("[ThresholdCalculation] Updating Data in Account Event list for"+acctId);
                updateFlag.executeUpdate();
                logger.info("[ThresholdCalculation] Updating Data in natm event core"+eventId);
                connection.commit();


            }else{
                insertEvent.setString(1,acctId);
                insertEvent.setString(2,"\"FC_"+eventId+"\"");
                insertEvent.setDouble(3,0);
                // added later for making natm ft core table

                updateFlag.setString(1,"SUCCESS");
                updateFlag.setString(2,eventId);
                insertEvent.executeUpdate();
                logger.info("[ThresholdCalculation] inserting Data in Account Event list for"+acctId);
                updateFlag.executeUpdate();
                logger.info("[ThresholdCalculation] Updating Data in natm event core"+eventId);

                connection.commit();

            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  static void insertIncidentTable(CxConnection connection, String custId, String acctId, double cumTxnAmount, String eventList,String eventId){
        logger.info("[ThresholdCalculation: insertIncidentTable()] isEventAvailable Retuned Y and going to insert data in incident table");
        String cumTxnAmountFormated = formatCumTxnAmt(cumTxnAmount);
        try(PreparedStatement insertStatement = connection.prepareStatement(insertIncident);){
            String intutionName = "S0056_NEW_ACCT_TXN_MONITORING_INTU";
            insertStatement.setString(1,"FC_"+eventId+"A_F_"+acctId+intutionName);
            insertStatement.setString(2,"CUSTOMER");
            insertStatement.setString(3, "C_F_"+custId);
            insertStatement.setString(4,"ACCOUNT");
            insertStatement.setString(5,"A_F_"+acctId);
            insertStatement.setString(6,intutionName);
            insertStatement.setString(7,"L3");   //changed line
            insertStatement.setString(8,"100");
            insertStatement.setString(9,"New Account Monitoring for account " +acctId+" total INR credit of amount " +cumTxnAmountFormated);
            insertStatement.setString(10,"EFM");
            insertStatement.setString(11,"FC_"+eventId+"A_F_"+acctId+intutionName);
            insertStatement.setString(12,"-1");
            insertStatement.setString(13,"0");
            insertStatement.setString(14,"A");
            insertStatement.setString(15,"F");
            insertStatement.setString(16,"100");
            insertStatement.setString(17,custId+" | FRM");
            insertStatement.setTimestamp(18,new Timestamp(System.currentTimeMillis()));
            insertStatement.setTimestamp(19, new Timestamp(System.currentTimeMillis()));

            JSONObject evidence = new JSONObject();
            JSONObject jObj = new JSONObject();
            JSONObject inner = new JSONObject();

//          //  String evidence = "{\n\"evidenceData\" : [ {\n \"evidenceMap\" : { },\n\"eventIds\" : [ "+eventList+" ],\n \"scnName\" :\""+intutionName+"\",\n \"addOnName\" : null,\n\"addOnScore\" : 0.0,\n" +
//                    "   \"baseScore\" : 0.0,\n" +
//                    "   \"swiftMessage\" : null,\n" +
//                    "   \"incidentSource\" : \"SAM\",\n" +
//                    "   \"risk1\" : null,\n" +
//                    "   \"risk2\" : null,\n" +
//                    "   \"modified\" : true,\n\"content\" : \"WSKEY:"+acctId+" | cumulativeamount:"+cumTxnAmount+"#/#"+intutionName+"#/#null#/#"+eventId+"\""+"\n} ]\n" +
//                    "}";

            jObj.put("evidenceMap",inner);
            jObj.put("eventIds",new JSONArray("["+eventList+"]"));
            jObj.put("scnName",intutionName);
            jObj.put("addOnName",JSONObject.NULL);
            jObj.put("addOnScore",0.0);
            jObj.put("baseScore",0.0);
            jObj.put("swiftMessage", JSONObject.NULL);
            jObj.put("incidentSource","SAM");
            jObj.put("risk1",JSONObject.NULL);
            jObj.put("risk2",JSONObject.NULL);
            jObj.put("modified",true);
            jObj.put("content","WSKEY:"+acctId+" | "+"cumulativeamount:"+cumTxnAmountFormated+"#/#"+intutionName+"#/#null#/#"+eventId);
            evidence.put("evidenceData",new JSONArray("["+jObj.toString()+"]"));
            //evidence.getJSONArray("evidenceData");
            Reader reader = new StringReader(evidence.toString());
            insertStatement.setClob(20,reader);
            insertStatement.setString(21,"{}");
            insertStatement.setString(22,acctId+"|FRM");
            insertStatement.setString(23,intutionName+" :New Account Monitoring for account  "+acctId+" total INR credit of amount "+cumTxnAmountFormated);
            insertStatement.setString(24,"SAM");
            insertStatement.executeUpdate();
            connection.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static  String formatCumTxnAmt(double cumTxnAmt){
        logger.info("[ThresholdCalculation:formatCumTxnAmt()] Formatting cum txn amt");
        String data= String.valueOf(cumTxnAmt);
        Double dblValue = Double.parseDouble(data);

        String str = String.format("%.2f", dblValue);
        return str;
    }

    public  static String isEventAvailable(String[] eventList){
        logger.info("[ThresholdCalculation: isEventAvailable()] Checking the event is availabe in event_ft_core table");

        String flag = "N";
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try(PreparedStatement eventCheckStatement = connection.prepareStatement(eventCheck)){

                    for(int i=0;i<eventList.length;i++) {

                        String eventId = eventList[i].replaceAll("\"","");

                       eventCheckStatement.setString(1,eventId.trim());
                       ResultSet resultEvent = eventCheckStatement.executeQuery();
                       if(resultEvent.next()){
                           flag = "Y";
                       }else{
                           logger.info("[ThresholdCalculation: isEventAvailable()] Event check Found"+eventList+"returning N");

                           return "N";
                       }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("[ThresholdCalculation: isEventAvailable()] Event check Found"+eventList+" returning "+flag);
        return flag;
    }

    public  static void insertEventBacklog( String custId, String acctId, double cumTxnAmount, String list, String eventId){
        logger.info("[ThresholdCalculation: insertEventBacklog()] Inserting Data in NATM BACK LOG Table");
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement insertStatement = connection.prepareStatement(insertBacklogTable)) {

                     insertStatement.setString(1,acctId);
                     insertStatement.setString(2,list);
                     insertStatement.setString(3,"N");
                     insertStatement.setString(4,custId);
                     insertStatement.setString(5,eventId);
                     insertStatement.setDouble(6,cumTxnAmount);
                     insertStatement.executeUpdate();
                     connection.commit();

                }
                connection.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
