ari5.custom.mapper {
        entity {
                ENTITY_WHITELIST {
                       generate = true
                        attributes:[
                                { name: ACCOUNTNO, type="string:50", key=true},
                                { name: IFSC ,type ="string:50", key=true},
                                { name: RCRE_TIME ,type = timestamp},
                                { name: RCRE_USER ,type = "string:50"},
                                { name: LCHG_TIME ,type = timestamp},
                                { name: LCHG_USER ,type = "string:50"}
                                
                                ]
                        }
        }
}
