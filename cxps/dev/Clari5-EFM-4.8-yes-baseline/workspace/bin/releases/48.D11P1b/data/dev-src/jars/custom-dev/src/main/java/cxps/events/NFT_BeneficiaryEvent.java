// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_BENEFICIARY", Schema="rice")
public class NFT_BeneficiaryEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=200) public String beneficiaryName;
       @Field(size=200) public String benfFlag1;
       @Field(size=200) public String benfFlag5;
       @Field(size=200) public String benfFlag4;
       @Field(size=200) public String benfFlag3;
       @Field(size=200) public String benfFlag2;
       @Field(size=200) public String errorDesc;
       @Field(size=200) public String payeeAccttype2;
       @Field(size=200) public String benfFlag9;
       @Field(size=200) public String payeeAccttype3;
       @Field(size=200) public String benfFlag8;
       @Field(size=200) public String payeeAccttype4;
       @Field(size=200) public String benfFlag7;
       @Field(size=200) public String payeeAccttype5;
       @Field(size=200) public String benfFlag6;
       @Field(size=200) public String groupPayeeId;
       @Field(size=200) public String payeeAccttype6;
       @Field(size=200) public String benfUniqueValue9;
       @Field(size=200) public String deviceId;
       @Field(size=200) public String payeeAccttype7;
       @Field(size=200) public String benfUniqueValue7;
       @Field(size=200) public String payeeAccttype8;
       @Field(size=200) public String benfUniqueValue8;
       @Field(size=200) public String payeeAccttype9;
       @Field(size=200) public String benfUniqueValue5;
       @Field(size=200) public String benfUniqueValue6;
       @Field(size=200) public String benfUniqueValue3;
       @Field(size=200) public String benfUniqueValue4;
       @Field(size=200) public String benfUniqueValue1;
       @Field(size=200) public String payeeAccttype10;
       @Field(size=200) public String benfUniqueValue2;
       @Field(size=200) public String benfFlag10;
       @Field(size=200) public String addrNetwork;
       @Field(size=2) public String hostId;
       @Field(size=200) public String obdxTransactionName;
       @Field(size=200) public String benfNickname10;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String obdxModuleName;
       @Field(size=200) public String benfPayeeId10;
       @Field(size=200) public String benfStatus;
       @Field(size=200) public String sessionId;
       @Field(size=200) public String benfUniqueValue10;
       @Field(size=200) public String benfType10;
       @Field(size=10) public String succFailFlg;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String ipCountry;
       @Field(size=200) public String benfType9;
       @Field(size=200) public String benfType8;
       @Field(size=200) public String benfType7;
       @Field(size=200) public String benfType6;
       @Field(size=200) public String benfPayeeId1;
       @Field(size=200) public String benfType5;
       @Field(size=200) public String benfType4;
       @Field(size=200) public String benfType3;
       @Field(size=200) public String benfType2;
       @Field(size=200) public String benfType1;
       @Field(size=200) public String benfNickname4;
       @Field(size=200) public String benfNickname5;
       @Field(size=20) public String twoFaStatus;
       @Field(size=200) public String benfNickname6;
       @Field(size=200) public String benfNickname7;
       @Field(size=200) public String ipCity;
       @Field(size=200) public String benfNickname1;
       @Field(size=200) public String benfNickname2;
       @Field(size=200) public String userId;
       @Field(size=200) public String benfNickname3;
       @Field(size=200) public String benfPayeeId3;
       @Field(size=200) public String benfPayeeId2;
       @Field(size=200) public String benfPayeeId5;
       @Field(size=200) public String benfPayeeId4;
       @Field(size=200) public String custId;
       @Field(size=200) public String benfPayeeId7;
       @Field(size=200) public String benfNickname8;
       @Field(size=200) public String benfPayeeId6;
       @Field(size=200) public String benfNickname9;
       @Field(size=200) public String benfPayeeId9;
       @Field(size=200) public String benfPayeeId8;
       @Field(size=200) public String riskBand;
       @Field(size=200) public String payeeAccttype1;
       @Field(size=20) public String benfAddition;


    @JsonIgnore
    public ITable<NFT_BeneficiaryEvent> t = AEF.getITable(this);

	public NFT_BeneficiaryEvent(){}

    public NFT_BeneficiaryEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Beneficiary");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setBeneficiaryName(json.getString("beneficiary_name"));
            setBenfFlag1(json.getString("benf_flag1"));
            setBenfFlag5(json.getString("benf_flag5"));
            setBenfFlag4(json.getString("benf_flag4"));
            setBenfFlag3(json.getString("benf_flag3"));
            setBenfFlag2(json.getString("benf_flag2"));
            setErrorDesc(json.getString("error_desc"));
            setPayeeAccttype2(json.getString("payee_accttype2"));
            setBenfFlag9(json.getString("benf_flag9"));
            setPayeeAccttype3(json.getString("payee_accttype3"));
            setBenfFlag8(json.getString("benf_flag8"));
            setPayeeAccttype4(json.getString("payee_accttype4"));
            setBenfFlag7(json.getString("benf_flag7"));
            setPayeeAccttype5(json.getString("payee_accttype5"));
            setBenfFlag6(json.getString("benf_flag6"));
            setGroupPayeeId(json.getString("group_payee_id"));
            setPayeeAccttype6(json.getString("payee_accttype6"));
            setBenfUniqueValue9(json.getString("benf_unique_value9"));
            setDeviceId(json.getString("device_id"));
            setPayeeAccttype7(json.getString("payee_accttype7"));
            setBenfUniqueValue7(json.getString("benf_unique_value7"));
            setPayeeAccttype8(json.getString("payee_accttype8"));
            setBenfUniqueValue8(json.getString("benf_unique_value8"));
            setPayeeAccttype9(json.getString("payee_accttype9"));
            setBenfUniqueValue5(json.getString("benf_unique_value5"));
            setBenfUniqueValue6(json.getString("benf_unique_value6"));
            setBenfUniqueValue3(json.getString("benf_unique_value3"));
            setBenfUniqueValue4(json.getString("benf_unique_value4"));
            setBenfUniqueValue1(json.getString("benf_unique_value1"));
            setPayeeAccttype10(json.getString("payee_accttype10"));
            setBenfUniqueValue2(json.getString("benf_unique_value2"));
            setBenfFlag10(json.getString("benf_flag10"));
            setAddrNetwork(json.getString("addr_network"));
            setHostId(json.getString("host_id"));
            setObdxTransactionName(json.getString("obdx_transaction_name"));
            setBenfNickname10(json.getString("benf_nickname10"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setErrorCode(json.getString("error_code"));
            setObdxModuleName(json.getString("obdx_module_name"));
            setBenfPayeeId10(json.getString("benf_payee_id10"));
            setBenfStatus(json.getString("benf_status"));
            setSessionId(json.getString("session_id"));
            setBenfUniqueValue10(json.getString("benf_unique_value10"));
            setBenfType10(json.getString("benf_type10"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setCustSegment(json.getString("cust_segment"));
            setIpCountry(json.getString("ip_country"));
            setBenfType9(json.getString("benf_type_9"));
            setBenfType8(json.getString("benf_type8"));
            setBenfType7(json.getString("benf_type7"));
            setBenfType6(json.getString("benf_type6"));
            setBenfPayeeId1(json.getString("benf_payee_id1"));
            setBenfType5(json.getString("benf_type5"));
            setBenfType4(json.getString("benf_type4"));
            setBenfType3(json.getString("benf_type3"));
            setBenfType2(json.getString("benf_type2"));
            setBenfType1(json.getString("benf_type1"));
            setBenfNickname4(json.getString("benf_nickname4"));
            setBenfNickname5(json.getString("benf_nickname5"));
            setTwoFaStatus(json.getString("2fa_status"));
            setBenfNickname6(json.getString("benf_nickname6"));
            setBenfNickname7(json.getString("benf_nickname7"));
            setIpCity(json.getString("ip_city"));
            setBenfNickname1(json.getString("benf_nickname1"));
            setBenfNickname2(json.getString("benf_nickname2"));
            setUserId(json.getString("user_id"));
            setBenfNickname3(json.getString("benf_nickname3"));
            setBenfPayeeId3(json.getString("benf_payee_id3"));
            setBenfPayeeId2(json.getString("benf_payee_id2"));
            setBenfPayeeId5(json.getString("benf_payee_id5"));
            setBenfPayeeId4(json.getString("benf_payee_id4"));
            setCustId(json.getString("cust_id"));
            setBenfPayeeId7(json.getString("benf_payee_id7"));
            setBenfNickname8(json.getString("benf_nickname8"));
            setBenfPayeeId6(json.getString("benf_payee_id6"));
            setBenfNickname9(json.getString("benf_nickname9"));
            setBenfPayeeId9(json.getString("benf_payee_id9"));
            setBenfPayeeId8(json.getString("benf_payee_id8"));
            setPayeeAccttype1(json.getString("payee_accttype1"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setRiskBand(cxps.events.CustomFieldDerivator.checkInstanceofEvent(this));setBenfAddition(cxps.events.CustomFieldDerivator.SaveBeneficiaryDb(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "BE"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public String getBeneficiaryName(){ return beneficiaryName; }

    public String getBenfFlag1(){ return benfFlag1; }

    public String getBenfFlag5(){ return benfFlag5; }

    public String getBenfFlag4(){ return benfFlag4; }

    public String getBenfFlag3(){ return benfFlag3; }

    public String getBenfFlag2(){ return benfFlag2; }

    public String getErrorDesc(){ return errorDesc; }

    public String getPayeeAccttype2(){ return payeeAccttype2; }

    public String getBenfFlag9(){ return benfFlag9; }

    public String getPayeeAccttype3(){ return payeeAccttype3; }

    public String getBenfFlag8(){ return benfFlag8; }

    public String getPayeeAccttype4(){ return payeeAccttype4; }

    public String getBenfFlag7(){ return benfFlag7; }

    public String getPayeeAccttype5(){ return payeeAccttype5; }

    public String getBenfFlag6(){ return benfFlag6; }

    public String getGroupPayeeId(){ return groupPayeeId; }

    public String getPayeeAccttype6(){ return payeeAccttype6; }

    public String getBenfUniqueValue9(){ return benfUniqueValue9; }

    public String getDeviceId(){ return deviceId; }

    public String getPayeeAccttype7(){ return payeeAccttype7; }

    public String getBenfUniqueValue7(){ return benfUniqueValue7; }

    public String getPayeeAccttype8(){ return payeeAccttype8; }

    public String getBenfUniqueValue8(){ return benfUniqueValue8; }

    public String getPayeeAccttype9(){ return payeeAccttype9; }

    public String getBenfUniqueValue5(){ return benfUniqueValue5; }

    public String getBenfUniqueValue6(){ return benfUniqueValue6; }

    public String getBenfUniqueValue3(){ return benfUniqueValue3; }

    public String getBenfUniqueValue4(){ return benfUniqueValue4; }

    public String getBenfUniqueValue1(){ return benfUniqueValue1; }

    public String getPayeeAccttype10(){ return payeeAccttype10; }

    public String getBenfUniqueValue2(){ return benfUniqueValue2; }

    public String getBenfFlag10(){ return benfFlag10; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getHostId(){ return hostId; }

    public String getObdxTransactionName(){ return obdxTransactionName; }

    public String getBenfNickname10(){ return benfNickname10; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getErrorCode(){ return errorCode; }

    public String getObdxModuleName(){ return obdxModuleName; }

    public String getBenfPayeeId10(){ return benfPayeeId10; }

    public String getBenfStatus(){ return benfStatus; }

    public String getSessionId(){ return sessionId; }

    public String getBenfUniqueValue10(){ return benfUniqueValue10; }

    public String getBenfType10(){ return benfType10; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getCustSegment(){ return custSegment; }

    public String getIpCountry(){ return ipCountry; }

    public String getBenfType9(){ return benfType9; }

    public String getBenfType8(){ return benfType8; }

    public String getBenfType7(){ return benfType7; }

    public String getBenfType6(){ return benfType6; }

    public String getBenfPayeeId1(){ return benfPayeeId1; }

    public String getBenfType5(){ return benfType5; }

    public String getBenfType4(){ return benfType4; }

    public String getBenfType3(){ return benfType3; }

    public String getBenfType2(){ return benfType2; }

    public String getBenfType1(){ return benfType1; }

    public String getBenfNickname4(){ return benfNickname4; }

    public String getBenfNickname5(){ return benfNickname5; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public String getBenfNickname6(){ return benfNickname6; }

    public String getBenfNickname7(){ return benfNickname7; }

    public String getIpCity(){ return ipCity; }

    public String getBenfNickname1(){ return benfNickname1; }

    public String getBenfNickname2(){ return benfNickname2; }

    public String getUserId(){ return userId; }

    public String getBenfNickname3(){ return benfNickname3; }

    public String getBenfPayeeId3(){ return benfPayeeId3; }

    public String getBenfPayeeId2(){ return benfPayeeId2; }

    public String getBenfPayeeId5(){ return benfPayeeId5; }

    public String getBenfPayeeId4(){ return benfPayeeId4; }

    public String getCustId(){ return custId; }

    public String getBenfPayeeId7(){ return benfPayeeId7; }

    public String getBenfNickname8(){ return benfNickname8; }

    public String getBenfPayeeId6(){ return benfPayeeId6; }

    public String getBenfNickname9(){ return benfNickname9; }

    public String getBenfPayeeId9(){ return benfPayeeId9; }

    public String getBenfPayeeId8(){ return benfPayeeId8; }

    public String getPayeeAccttype1(){ return payeeAccttype1; }
    public String getRiskBand(){ return riskBand; }

    public String getBenfAddition(){ return benfAddition; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setBeneficiaryName(String val){ this.beneficiaryName = val; }
    public void setBenfFlag1(String val){ this.benfFlag1 = val; }
    public void setBenfFlag5(String val){ this.benfFlag5 = val; }
    public void setBenfFlag4(String val){ this.benfFlag4 = val; }
    public void setBenfFlag3(String val){ this.benfFlag3 = val; }
    public void setBenfFlag2(String val){ this.benfFlag2 = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setPayeeAccttype2(String val){ this.payeeAccttype2 = val; }
    public void setBenfFlag9(String val){ this.benfFlag9 = val; }
    public void setPayeeAccttype3(String val){ this.payeeAccttype3 = val; }
    public void setBenfFlag8(String val){ this.benfFlag8 = val; }
    public void setPayeeAccttype4(String val){ this.payeeAccttype4 = val; }
    public void setBenfFlag7(String val){ this.benfFlag7 = val; }
    public void setPayeeAccttype5(String val){ this.payeeAccttype5 = val; }
    public void setBenfFlag6(String val){ this.benfFlag6 = val; }
    public void setGroupPayeeId(String val){ this.groupPayeeId = val; }
    public void setPayeeAccttype6(String val){ this.payeeAccttype6 = val; }
    public void setBenfUniqueValue9(String val){ this.benfUniqueValue9 = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setPayeeAccttype7(String val){ this.payeeAccttype7 = val; }
    public void setBenfUniqueValue7(String val){ this.benfUniqueValue7 = val; }
    public void setPayeeAccttype8(String val){ this.payeeAccttype8 = val; }
    public void setBenfUniqueValue8(String val){ this.benfUniqueValue8 = val; }
    public void setPayeeAccttype9(String val){ this.payeeAccttype9 = val; }
    public void setBenfUniqueValue5(String val){ this.benfUniqueValue5 = val; }
    public void setBenfUniqueValue6(String val){ this.benfUniqueValue6 = val; }
    public void setBenfUniqueValue3(String val){ this.benfUniqueValue3 = val; }
    public void setBenfUniqueValue4(String val){ this.benfUniqueValue4 = val; }
    public void setBenfUniqueValue1(String val){ this.benfUniqueValue1 = val; }
    public void setPayeeAccttype10(String val){ this.payeeAccttype10 = val; }
    public void setBenfUniqueValue2(String val){ this.benfUniqueValue2 = val; }
    public void setBenfFlag10(String val){ this.benfFlag10 = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setObdxTransactionName(String val){ this.obdxTransactionName = val; }
    public void setBenfNickname10(String val){ this.benfNickname10 = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setObdxModuleName(String val){ this.obdxModuleName = val; }
    public void setBenfPayeeId10(String val){ this.benfPayeeId10 = val; }
    public void setBenfStatus(String val){ this.benfStatus = val; }
    public void setSessionId(String val){ this.sessionId = val; }
    public void setBenfUniqueValue10(String val){ this.benfUniqueValue10 = val; }
    public void setBenfType10(String val){ this.benfType10 = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setBenfType9(String val){ this.benfType9 = val; }
    public void setBenfType8(String val){ this.benfType8 = val; }
    public void setBenfType7(String val){ this.benfType7 = val; }
    public void setBenfType6(String val){ this.benfType6 = val; }
    public void setBenfPayeeId1(String val){ this.benfPayeeId1 = val; }
    public void setBenfType5(String val){ this.benfType5 = val; }
    public void setBenfType4(String val){ this.benfType4 = val; }
    public void setBenfType3(String val){ this.benfType3 = val; }
    public void setBenfType2(String val){ this.benfType2 = val; }
    public void setBenfType1(String val){ this.benfType1 = val; }
    public void setBenfNickname4(String val){ this.benfNickname4 = val; }
    public void setBenfNickname5(String val){ this.benfNickname5 = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setBenfNickname6(String val){ this.benfNickname6 = val; }
    public void setBenfNickname7(String val){ this.benfNickname7 = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setBenfNickname1(String val){ this.benfNickname1 = val; }
    public void setBenfNickname2(String val){ this.benfNickname2 = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setBenfNickname3(String val){ this.benfNickname3 = val; }
    public void setBenfPayeeId3(String val){ this.benfPayeeId3 = val; }
    public void setBenfPayeeId2(String val){ this.benfPayeeId2 = val; }
    public void setBenfPayeeId5(String val){ this.benfPayeeId5 = val; }
    public void setBenfPayeeId4(String val){ this.benfPayeeId4 = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setBenfPayeeId7(String val){ this.benfPayeeId7 = val; }
    public void setBenfNickname8(String val){ this.benfNickname8 = val; }
    public void setBenfPayeeId6(String val){ this.benfPayeeId6 = val; }
    public void setBenfNickname9(String val){ this.benfNickname9 = val; }
    public void setBenfPayeeId9(String val){ this.benfPayeeId9 = val; }
    public void setBenfPayeeId8(String val){ this.benfPayeeId8 = val; }
    public void setPayeeAccttype1(String val){ this.payeeAccttype1 = val; }
    public void setRiskBand(String val){ this.riskBand = val; }
    public void setBenfAddition(String val){ this.benfAddition = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_BeneficiaryEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Beneficiary");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Beneficiary");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}