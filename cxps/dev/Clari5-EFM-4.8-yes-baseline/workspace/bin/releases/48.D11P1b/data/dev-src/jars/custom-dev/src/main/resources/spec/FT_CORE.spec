clari5.custom.yes.db.entity.FT_CORE {
                        attributes=[
                                { name: EVENT_ID , type="string:4000" , key=true  },
                                { name: CUST_ID ,type ="string:50" }
                                { name: UCIC_ID ,type ="string:50"}
				{ name: PRODUCT_CODE ,type ="string:50" }
				{ name: FCC_PRODUCT_CODE ,type ="string:50" }
				{ name: DRCR ,type ="string:50" }
				{ name: TRAN_AMOUNT ,type ="number:30,2" }
				{ name: BRANCH_ID ,type ="string:50" }
				{ name: TD_MATURITY_DATE ,type =timestamp }
				{ name: EFF_AVAILABLE_BALANCE ,type ="number:30,2" }
				{ name: CHANNEL_ID ,type ="string:50" }
				{ name: TRAN_NUMONIC_CODE ,type ="string:50" }
				{ name: INST_STATUS ,type ="string:50" }
				{ name: HOST_TYPE ,type ="string:50"  }
				{ name: INSTRUMENT_TYPE ,type ="string:50" }
				{ name: INSTRUMENT_NUMBER ,type ="string:50" }	
				{ name: ATM_ID ,type ="string:50" }
				{ name: ISIN_NUMBER ,type ="string:50" }
				{ name: TRAN_DATE ,type =timestamp }
				{ name: PSTD_DATE ,type =timestamp }
				{ name: TRAN_ID ,type ="string:50" }
				{ name: REFERENCE_SRL_NUM ,type ="string:50" }
				{ name: VALUE_DATE ,type =timestamp }
				{ name: TRAN_CRNCY_CODE ,type ="string:50" }		
				{ name: REF_TRAN_AMT ,type ="number:30,2" }
				{ name: REF_TRAN_CRNCY ,type ="string:50" }
				{ name: TRAN_PARTICULAR ,type ="string:4000" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: BANK_CODE ,type ="string:50" }
				{ name: PSTD_FLG ,type ="string:50" }
				{ name: ONLINE_BATCH ,type ="string:50" }
				{ name: USER_ID ,type ="string:50" }
				{ name: STATUS ,type ="string:50" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: TD_LIQUIDATION_TYPE ,type ="string:50" }
				{ name: TOD_GRANT_AMOUNT ,type ="number:30,2" }
				{ name: CA_SCHEME_CODE ,type ="string:50" }
				{ name: SYSTEM ,type ="string:50" }
				{ name: REM_TYPE ,type ="string:50" }
				{ name: BRANCH ,type ="string:50" }
				{ name: TRAN_CURR ,type ="string:50" }
				{ name: TRAN_AMT ,type ="number:30,2" }
				{ name: USD_EQV_AMT ,type ="number:30,2" }
				{ name: INR_AMOUNT ,type ="string:50" }
				{ name: PURPOSE_CODE ,type ="string:50" }
				{ name: PURPOSE_DESC ,type ="string:50" }
				{ name: REM_CUST_ID ,type ="string:50"}
				{ name: REM_NAME ,type ="string:50" }
				{ name: REM_ADD1 ,type ="string:50" }
				{ name: REM_ADD2 ,type ="string:50" }
				{ name: REM_ADD3 ,type ="string:50" }
				{ name: REM_CITY ,type ="string:500" }
				{ name: REM_CNTRY_CODE ,type ="string:50" }
				{ name: BEN_CUST_ID ,type ="string:50"}
				{ name: BEN_NAME ,type ="string:50" }
				{ name: BEN_ADD1 ,type ="string:50" }
				{ name: BEN_ADD2 ,type ="string:50" }
				{ name: BEN_ADD3 ,type ="string:50" }
				{ name: BEN_CITY ,type ="string:50" }
				{ name: BEN_CNTRY_CODE ,type ="string:50" }
				{ name: CLIENT_ACC_NO ,type ="string:50" }
				{ name: CPTY_AC_NO ,type ="string:50" }
				{ name: BEN_ACCT_NO ,type ="string:500" }
				{ name: BEN_BIC ,type ="string:50" }
				{ name: REM_ACCT_NO ,type ="string:50"}
				{ name: REM_BIC ,type ="string:50" }
				{ name: TRN_DATE ,type =timestamp }
				{ name: PRODUCT_DESC ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:4000" , default="NEW"}
				{ name: ACCT_ID ,type ="string:50" }
				{ name: INSERTION_TIME ,type =timestamp }
				{ name: SERVER_ID ,type ="number" }
				{ name: COD_MSG_TYPE ,type ="number:5" }
				{ name: BATCH_ID ,type ="number:22,11" }
				{ name: NATM_SYNC_STATUS ,type ="string:50" }
                                ]
				indexes {
						
               					IDX_FFR_CORE : [ SERVER_ID ]
						IDX_FTCORESTATUS : [ SYNC_STATUS ]
						SYNCBATCHID : [ NATM_SYNC_STATUS, BATCH_ID ]
								
       					 }
                        }
 


