// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_COREACCT", Schema="rice")
public class FT_CoreAcctTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String userType;
       @Field(size=11) public Double todGrantAmount;
       @Field public java.sql.Timestamp valueDate;
       @Field(size=200) public String channel;
       @Field(size=200) public String benCity;
       @Field(size=200) public String nonHomeBranch;
       @Field(size=200) public String onlineBatch;
       @Field(size=11) public Double tranAmount;
       @Field(size=200) public String branch;
       @Field(size=200) public String benCntryCode;
       @Field(size=200) public String refTranCrncy;
       @Field(size=200) public String relation;
       @Field(size=200) public String purposeDesc;
       @Field(size=200) public String remName;
       @Field(size=200) public String tranId;
       @Field(size=200) public String benBic;
       @Field(size=200) public String hostId;
       @Field(size=200) public String custType;
       @Field(size=11) public Double tranAmt;
       @Field(size=200) public String productDesc;
       @Field(size=200) public String transactionMode;
     public String nonHomeBranchRaw;
       @Field(size=11) public Double cumTxnAmt;
       @Field(size=200) public String mtMessage;
       @Field(size=200) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String tranCrncyCode;
       @Field public java.sql.Timestamp tdMaturityDate;
       @Field(size=200) public String benAcctNo;
       @Field(size=200) public String benCustId;
       @Field(size=11) public Long codeMsgType;
       @Field(size=11) public Double thresholdAmt;
       @Field public java.sql.Timestamp pstdDate;
       @Field(size=200) public String instStatus;
       @Field(size=200) public String system;
       @Field(size=200) public String atmId;
       @Field(size=200) public String productCode;
       @Field(size=200) public String drCr;
       @Field(size=200) public String clientAccNo;
       @Field(size=200) public String remBic;
       @Field(size=10) public String accountOpenDateFlag;
       @Field(size=10) public String productTypeFlag;
       @Field(size=200) public String pstdFlg;
       @Field(size=200) public String status;
       @Field(size=200) public String hostType;
       @Field(size=200) public String remAdd1;
       @Field(size=200) public String txnKey;
       @Field(size=200) public String acctId;
       @Field(size=200) public String remType;
       @Field(size=200) public String commodity;
       @Field(size=10) public String whiteListFlag;
       @Field(size=200) public String fccProductCode;
       @Field(size=200) public String remAdd3;
       @Field(size=200) public String inrAmount;
       @Field(size=200) public String remAdd2;
       @Field public java.sql.Timestamp trnDate;
       @Field(size=200) public String hmDate;
       @Field(size=200) public String purposeCode;
       @Field(size=200) public String channelId;
       @Field(size=200) public String subTranMode;
       @Field(size=200) public String benAdd3;
       @Field(size=200) public String caSchemeCode;
       @Field(size=200) public String benAdd2;
       @Field(size=200) public String remAcctNo;
       @Field(size=200) public String benAdd1;
       @Field(size=200) public String trannumonicCode;
       @Field(size=200) public String remCustId;
       @Field(size=200) public String instrumentNumber;
       @Field(size=200) public String autheriserUserId;
       @Field(size=200) public String benName;
       @Field(size=200) public String userId;
       @Field(size=200) public String isinNumber;
       @Field(size=200) public String tranParticular;
       @Field(size=200) public String custId;
       @Field(size=200) public String referenceSrlNum;
       @Field(size=200) public String bankCode;
       @Field(size=11) public Double usdEqvAmt;
       @Field(size=200) public String tdLiquidationType;
       @Field(size=200) public String remCntryCode;
       @Field(size=200) public String cptyAcNo;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=200) public String tranCurr;
       @Field(size=11) public Double effAvailableBalance;
       @Field(size=200) public String instrumentType;
       @Field(size=11) public Double refTranAmt;
       @Field(size=200) public String remCity;
       @Field(size=200) public String ucicId;


    @JsonIgnore
    public ITable<FT_CoreAcctTxnEvent> t = AEF.getITable(this);

	public FT_CoreAcctTxnEvent(){}

    public FT_CoreAcctTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("CoreAcctTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setUserType(json.getString("user_type"));
            setTodGrantAmount(EventHelper.toDouble(json.getString("TOD_Grant_Amount")));
            setValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setBenCity(json.getString("BEN_CITY"));
            setOnlineBatch(json.getString("online_batch"));
            setTranAmount(EventHelper.toDouble(json.getString("Tran_amount")));
            setBranch(json.getString("BRANCH"));
            setBenCntryCode(json.getString("BEN_CNTRY_CODE"));
            setRefTranCrncy(json.getString("ref_tran_crncy"));
            setRelation(json.getString("relation"));
            setPurposeDesc(json.getString("PURPOSE_DESC"));
            setRemName(json.getString("REM_NAME"));
            setTranId(json.getString("tran_id"));
            setBenBic(json.getString("BEN_BIC"));
            setHostId(json.getString("host_id"));
            setCustType(json.getString("cust_type"));
            setTranAmt(EventHelper.toDouble(json.getString("TRAN_AMT")));
            setProductDesc(json.getString("Product_desc"));
            setNonHomeBranchRaw(json.getString("nonHomeBranch"));
            setCumTxnAmt(EventHelper.toDouble(json.getString("cum_txn_amt")));
            setMtMessage(json.getString("mt_message"));
            setBranchId(json.getString("Branch_Id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTdMaturityDate(EventHelper.toTimestamp(json.getString("TD_Maturity_Date")));
            setBenAcctNo(json.getString("BEN_ACCT_NO"));
            setBenCustId(json.getString("BEN_CUST_ID"));
            setCodeMsgType(EventHelper.toLong(json.getString("cod_msg_type")));
            setPstdDate(EventHelper.toTimestamp(json.getString("pstd_date")));
            setInstStatus(json.getString("Inst_status"));
            setSystem(json.getString("SYSTEM"));
            setAtmId(json.getString("ATM_ID"));
            setProductCode(json.getString("product_code"));
            setDrCr(json.getString("DRCR"));
            setClientAccNo(json.getString("CLIENT_ACC_NO"));
            setRemBic(json.getString("REM_BIC"));
            setAccountOpenDateFlag(json.getString("accountOpenDateFlag"));
            setProductTypeFlag(json.getString("productTypeFlag"));
            setPstdFlg(json.getString("pstd_flg"));
            setStatus(json.getString("status"));
            setHostType(json.getString("Host_Type"));
            setRemAdd1(json.getString("REM_ADD1"));
            setAcctId(json.getString("acct_id"));
            setRemType(json.getString("REM_TYPE"));
            setCommodity(json.getString("commodity"));
            setWhiteListFlag(json.getString("whiteListFlag"));
            setFccProductCode(json.getString("FCC_Product_code"));
            setRemAdd3(json.getString("REM_ADD3"));
            setInrAmount(json.getString("INR_AMOUNT"));
            setRemAdd2(json.getString("REM_ADD2"));
            setTrnDate(EventHelper.toTimestamp(json.getString("TRN_DATE")));
            setPurposeCode(json.getString("PURPOSE_CODE"));
            setChannelId(json.getString("Channel_Id"));
            setBenAdd3(json.getString("BEN_ADD3"));
            setCaSchemeCode(json.getString("CA_Scheme_Code"));
            setBenAdd2(json.getString("BEN_ADD2"));
            setRemAcctNo(json.getString("REM_ACCT_NO"));
            setBenAdd1(json.getString("BEN_ADD1"));
            setTrannumonicCode(json.getString("Tran_numonic_code"));
            setRemCustId(json.getString("REM_CUST_ID"));
            setInstrumentNumber(json.getString("Instrument_Number"));
            setAutheriserUserId(json.getString("autheriser_user_id"));
            setBenName(json.getString("BEN_NAME"));
            setUserId(json.getString("User_id"));
            setIsinNumber(json.getString("ISIN_Number"));
            setTranParticular(json.getString("tran_particular"));
            setCustId(json.getString("cust_id"));
            setReferenceSrlNum(json.getString("reference_srl_num"));
            setBankCode(json.getString("bank_code"));
            setUsdEqvAmt(EventHelper.toDouble(json.getString("USD_EQV_AMT")));
            setTdLiquidationType(json.getString("TD_Liquidation_Type"));
            setRemCntryCode(json.getString("REM_CNTRY_CODE"));
            setCptyAcNo(json.getString("CPTY_AC_NO"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setTranCurr(json.getString("TRAN_CURR"));
            setEffAvailableBalance(EventHelper.toDouble(json.getString("Eff_Available_Balance")));
            setInstrumentType(json.getString("Instrument_Type"));
            setRefTranAmt(EventHelper.toDouble(json.getString("ref_tran_amt")));
            setRemCity(json.getString("REM_CITY"));
            setUcicId(json.getString("ucic_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setChannel(cxps.events.CustomFieldDerivator.getChannelFromTxnNumonicCode(getTrannumonicCode()));setNonHomeBranch(cxps.events.CustomFieldDerivator.deriveNonHomeBranch(this));setTransactionMode(cxps.events.CustomFieldDerivator.getTransactionModeFromTxnNumonicCode(getTrannumonicCode()));setThresholdAmt(cxps.events.CustomFieldDerivator.getCumTxnAmtCode(this));setTxnKey(cxps.noesis.core.EventHelper.concat(null, getTranId(), getTranDate()));setHmDate(cxps.events.CustomFieldDerivator.getHolidaydate(this));setSubTranMode(cxps.events.CustomFieldDerivator.getSubtranmodeFromTxnNumonicCode(getTrannumonicCode()));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getUserType(){ return userType; }

    public Double getTodGrantAmount(){ return todGrantAmount; }

    public java.sql.Timestamp getValueDate(){ return valueDate; }

    public String getBenCity(){ return benCity; }

    public String getOnlineBatch(){ return onlineBatch; }

    public Double getTranAmount(){ return tranAmount; }

    public String getBranch(){ return branch; }

    public String getBenCntryCode(){ return benCntryCode; }

    public String getRefTranCrncy(){ return refTranCrncy; }

    public String getRelation(){ return relation; }

    public String getPurposeDesc(){ return purposeDesc; }

    public String getRemName(){ return remName; }

    public String getTranId(){ return tranId; }

    public String getBenBic(){ return benBic; }

    public String getHostId(){ return hostId; }

    public String getCustType(){ return custType; }

    public Double getTranAmt(){ return tranAmt; }

    public String getProductDesc(){ return productDesc; }

    public String getNonHomeBranchRaw(){ return nonHomeBranchRaw; }

    public Double getCumTxnAmt(){ return cumTxnAmt; }

    public String getMtMessage(){ return mtMessage; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public java.sql.Timestamp getTdMaturityDate(){ return tdMaturityDate; }

    public String getBenAcctNo(){ return benAcctNo; }

    public String getBenCustId(){ return benCustId; }

    public Long getCodeMsgType(){ return codeMsgType; }

    public java.sql.Timestamp getPstdDate(){ return pstdDate; }

    public String getInstStatus(){ return instStatus; }

    public String getSystem(){ return system; }

    public String getAtmId(){ return atmId; }

    public String getProductCode(){ return productCode; }

    public String getDrCr(){ return drCr; }

    public String getClientAccNo(){ return clientAccNo; }

    public String getRemBic(){ return remBic; }

    public String getAccountOpenDateFlag(){ return accountOpenDateFlag; }

    public String getProductTypeFlag(){ return productTypeFlag; }

    public String getPstdFlg(){ return pstdFlg; }

    public String getStatus(){ return status; }

    public String getHostType(){ return hostType; }

    public String getRemAdd1(){ return remAdd1; }

    public String getAcctId(){ return acctId; }

    public String getRemType(){ return remType; }

    public String getCommodity(){ return commodity; }

    public String getWhiteListFlag(){ return whiteListFlag; }

    public String getFccProductCode(){ return fccProductCode; }

    public String getRemAdd3(){ return remAdd3; }

    public String getInrAmount(){ return inrAmount; }

    public String getRemAdd2(){ return remAdd2; }

    public java.sql.Timestamp getTrnDate(){ return trnDate; }

    public String getHmDate(){ return hmDate; }

    public String getPurposeCode(){ return purposeCode; }

    public String getChannelId(){ return channelId; }

    public String getBenAdd3(){ return benAdd3; }

    public String getCaSchemeCode(){ return caSchemeCode; }

    public String getBenAdd2(){ return benAdd2; }

    public String getRemAcctNo(){ return remAcctNo; }

    public String getBenAdd1(){ return benAdd1; }

    public String getTrannumonicCode(){ return trannumonicCode; }

    public String getRemCustId(){ return remCustId; }

    public String getInstrumentNumber(){ return instrumentNumber; }

    public String getAutheriserUserId(){ return autheriserUserId; }

    public String getBenName(){ return benName; }

    public String getUserId(){ return userId; }

    public String getIsinNumber(){ return isinNumber; }

    public String getTranParticular(){ return tranParticular; }

    public String getCustId(){ return custId; }

    public String getReferenceSrlNum(){ return referenceSrlNum; }

    public String getBankCode(){ return bankCode; }

    public Double getUsdEqvAmt(){ return usdEqvAmt; }

    public String getTdLiquidationType(){ return tdLiquidationType; }

    public String getRemCntryCode(){ return remCntryCode; }

    public String getCptyAcNo(){ return cptyAcNo; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getTranCurr(){ return tranCurr; }

    public Double getEffAvailableBalance(){ return effAvailableBalance; }

    public String getInstrumentType(){ return instrumentType; }

    public Double getRefTranAmt(){ return refTranAmt; }

    public String getRemCity(){ return remCity; }

    public String getUcicId(){ return ucicId; }
    public String getChannel(){ return channel; }

    public String getNonHomeBranch(){ return nonHomeBranch; }

    public String getTransactionMode(){ return transactionMode; }

    public Double getThresholdAmt(){ return thresholdAmt; }

    public String getTxnKey(){ return txnKey; }

    public String getSubTranMode(){ return subTranMode; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setUserType(String val){ this.userType = val; }
    public void setTodGrantAmount(Double val){ this.todGrantAmount = val; }
    public void setValueDate(java.sql.Timestamp val){ this.valueDate = val; }
    public void setBenCity(String val){ this.benCity = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranAmount(Double val){ this.tranAmount = val; }
    public void setBranch(String val){ this.branch = val; }
    public void setBenCntryCode(String val){ this.benCntryCode = val; }
    public void setRefTranCrncy(String val){ this.refTranCrncy = val; }
    public void setRelation(String val){ this.relation = val; }
    public void setPurposeDesc(String val){ this.purposeDesc = val; }
    public void setRemName(String val){ this.remName = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setBenBic(String val){ this.benBic = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCustType(String val){ this.custType = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setProductDesc(String val){ this.productDesc = val; }
    public void setNonHomeBranchRaw(String val){ this.nonHomeBranchRaw = val; }
    public void setCumTxnAmt(Double val){ this.cumTxnAmt = val; }
    public void setMtMessage(String val){ this.mtMessage = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTdMaturityDate(java.sql.Timestamp val){ this.tdMaturityDate = val; }
    public void setBenAcctNo(String val){ this.benAcctNo = val; }
    public void setBenCustId(String val){ this.benCustId = val; }
    public void setCodeMsgType(Long val){ this.codeMsgType = val; }
    public void setPstdDate(java.sql.Timestamp val){ this.pstdDate = val; }
    public void setInstStatus(String val){ this.instStatus = val; }
    public void setSystem(String val){ this.system = val; }
    public void setAtmId(String val){ this.atmId = val; }
    public void setProductCode(String val){ this.productCode = val; }
    public void setDrCr(String val){ this.drCr = val; }
    public void setClientAccNo(String val){ this.clientAccNo = val; }
    public void setRemBic(String val){ this.remBic = val; }
    public void setAccountOpenDateFlag(String val){ this.accountOpenDateFlag = val; }
    public void setProductTypeFlag(String val){ this.productTypeFlag = val; }
    public void setPstdFlg(String val){ this.pstdFlg = val; }
    public void setStatus(String val){ this.status = val; }
    public void setHostType(String val){ this.hostType = val; }
    public void setRemAdd1(String val){ this.remAdd1 = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setCommodity(String val){ this.commodity = val; }
    public void setWhiteListFlag(String val){ this.whiteListFlag = val; }
    public void setFccProductCode(String val){ this.fccProductCode = val; }
    public void setRemAdd3(String val){ this.remAdd3 = val; }
    public void setInrAmount(String val){ this.inrAmount = val; }
    public void setRemAdd2(String val){ this.remAdd2 = val; }
    public void setTrnDate(java.sql.Timestamp val){ this.trnDate = val; }
    public void setHmDate(String val){ this.hmDate = val; }
    public void setPurposeCode(String val){ this.purposeCode = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setBenAdd3(String val){ this.benAdd3 = val; }
    public void setCaSchemeCode(String val){ this.caSchemeCode = val; }
    public void setBenAdd2(String val){ this.benAdd2 = val; }
    public void setRemAcctNo(String val){ this.remAcctNo = val; }
    public void setBenAdd1(String val){ this.benAdd1 = val; }
    public void setTrannumonicCode(String val){ this.trannumonicCode = val; }
    public void setRemCustId(String val){ this.remCustId = val; }
    public void setInstrumentNumber(String val){ this.instrumentNumber = val; }
    public void setAutheriserUserId(String val){ this.autheriserUserId = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setIsinNumber(String val){ this.isinNumber = val; }
    public void setTranParticular(String val){ this.tranParticular = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setReferenceSrlNum(String val){ this.referenceSrlNum = val; }
    public void setBankCode(String val){ this.bankCode = val; }
    public void setUsdEqvAmt(Double val){ this.usdEqvAmt = val; }
    public void setTdLiquidationType(String val){ this.tdLiquidationType = val; }
    public void setRemCntryCode(String val){ this.remCntryCode = val; }
    public void setCptyAcNo(String val){ this.cptyAcNo = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setTranCurr(String val){ this.tranCurr = val; }
    public void setEffAvailableBalance(Double val){ this.effAvailableBalance = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setRefTranAmt(Double val){ this.refTranAmt = val; }
    public void setRemCity(String val){ this.remCity = val; }
    public void setUcicId(String val){ this.ucicId = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setNonHomeBranch(String val){ this.nonHomeBranch = val; }
    public void setTransactionMode(String val){ this.transactionMode = val; }
    public void setThresholdAmt(Double val){ this.thresholdAmt = val; }
    public void setTxnKey(String val){ this.txnKey = val; }
    public void setSubTranMode(String val){ this.subTranMode = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getUser_type(){ return userType; }
    @JsonIgnore
    public Double getTOD_Grant_Amount(){ return todGrantAmount; }
    @JsonIgnore
    public java.sql.Timestamp getValue_date(){ return valueDate; }
    @JsonIgnore
    public String getBEN_CITY(){ return benCity; }
    @JsonIgnore
    public String getOnline_batch(){ return onlineBatch; }
    @JsonIgnore
    public Double getTran_amount(){ return tranAmount; }
    @JsonIgnore
    public String getBRANCH(){ return branch; }
    @JsonIgnore
    public String getBEN_CNTRY_CODE(){ return benCntryCode; }
    @JsonIgnore
    public String getRef_tran_crncy(){ return refTranCrncy; }
    @JsonIgnore
    public String getPURPOSE_DESC(){ return purposeDesc; }
    @JsonIgnore
    public String getREM_NAME(){ return remName; }
    @JsonIgnore
    public String getTran_id(){ return tranId; }
    @JsonIgnore
    public String getBEN_BIC(){ return benBic; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }
    @JsonIgnore
    public String getCust_type(){ return custType; }
    @JsonIgnore
    public Double getTRAN_AMT(){ return tranAmt; }
    @JsonIgnore
    public String getProduct_desc(){ return productDesc; }
    @JsonIgnore
    public String getMt_message(){ return mtMessage; }
    @JsonIgnore
    public String getBranch_Id(){ return branchId; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getTran_crncy_code(){ return tranCrncyCode; }
    @JsonIgnore
    public java.sql.Timestamp getTD_Maturity_Date(){ return tdMaturityDate; }
    @JsonIgnore
    public String getBEN_ACCT_NO(){ return benAcctNo; }
    @JsonIgnore
    public String getBEN_CUST_ID(){ return benCustId; }
    @JsonIgnore
    public java.sql.Timestamp getPstd_date(){ return pstdDate; }
    @JsonIgnore
    public String getInst_status(){ return instStatus; }
    @JsonIgnore
    public String getSYSTEM(){ return system; }
    @JsonIgnore
    public String getATM_ID(){ return atmId; }
    @JsonIgnore
    public String getProduct_code(){ return productCode; }
    @JsonIgnore
    public String getDRCR(){ return drCr; }
    @JsonIgnore
    public String getCLIENT_ACC_NO(){ return clientAccNo; }
    @JsonIgnore
    public String getREM_BIC(){ return remBic; }
    @JsonIgnore
    public String getPstd_flg(){ return pstdFlg; }
    @JsonIgnore
    public String getHost_Type(){ return hostType; }
    @JsonIgnore
    public String getREM_ADD1(){ return remAdd1; }
    @JsonIgnore
    public String getAcct_id(){ return acctId; }
    @JsonIgnore
    public String getREM_TYPE(){ return remType; }
    @JsonIgnore
    public String getFCC_Product_code(){ return fccProductCode; }
    @JsonIgnore
    public String getREM_ADD3(){ return remAdd3; }
    @JsonIgnore
    public String getINR_AMOUNT(){ return inrAmount; }
    @JsonIgnore
    public String getREM_ADD2(){ return remAdd2; }
    @JsonIgnore
    public java.sql.Timestamp getTRN_DATE(){ return trnDate; }
    @JsonIgnore
    public String getPURPOSE_CODE(){ return purposeCode; }
    @JsonIgnore
    public String getChannel_Id(){ return channelId; }
    @JsonIgnore
    public String getBEN_ADD3(){ return benAdd3; }
    @JsonIgnore
    public String getCA_Scheme_Code(){ return caSchemeCode; }
    @JsonIgnore
    public String getBEN_ADD2(){ return benAdd2; }
    @JsonIgnore
    public String getREM_ACCT_NO(){ return remAcctNo; }
    @JsonIgnore
    public String getBEN_ADD1(){ return benAdd1; }
    @JsonIgnore
    public String getTran_numonic_code(){ return trannumonicCode; }
    @JsonIgnore
    public String getREM_CUST_ID(){ return remCustId; }
    @JsonIgnore
    public String getInstrument_Number(){ return instrumentNumber; }
    @JsonIgnore
    public String getautheriser_user_id(){ return autheriserUserId; }
    @JsonIgnore
    public String getBEN_NAME(){ return benName; }
    @JsonIgnore
    public String getUser_id(){ return userId; }
    @JsonIgnore
    public String getISIN_Number(){ return isinNumber; }
    @JsonIgnore
    public String getTran_particular(){ return tranParticular; }
    @JsonIgnore
    public String getCust_id(){ return custId; }
    @JsonIgnore
    public String getReference_srl_num(){ return referenceSrlNum; }
    @JsonIgnore
    public String getBank_code(){ return bankCode; }
    @JsonIgnore
    public Double getUSD_EQV_AMT(){ return usdEqvAmt; }
    @JsonIgnore
    public String getTD_Liquidation_Type(){ return tdLiquidationType; }
    @JsonIgnore
    public String getREM_CNTRY_CODE(){ return remCntryCode; }
    @JsonIgnore
    public String getCPTY_AC_NO(){ return cptyAcNo; }
    @JsonIgnore
    public java.sql.Timestamp getTran_date(){ return tranDate; }
    @JsonIgnore
    public String getTRAN_CURR(){ return tranCurr; }
    @JsonIgnore
    public Double getEff_Available_Balance(){ return effAvailableBalance; }
    @JsonIgnore
    public String getInstrument_Type(){ return instrumentType; }
    @JsonIgnore
    public Double getRef_tran_amt(){ return refTranAmt; }
    @JsonIgnore
    public String getREM_CITY(){ return remCity; }
    @JsonIgnore
    public String getUcic_id(){ return ucicId; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_CoreAcctTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        //String branchKey= h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(), this.branchId);
        //wsInfoSet.add(new WorkspaceInfo("Branch", branchKey));
        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctId);
        if (CustomFieldDerivator.ignrWsKeys.contains(accountKey)) {
            return new HashSet<WorkspaceInfo>();
        }
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        //String transactionKey= h.getCxKeyGivenHostKey(WorkspaceName.TRANSACTION, getHostId(), this.txnKey);
        //wsInfoSet.add(new WorkspaceInfo("Transaction", transactionKey));
        //String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        //wsInfoSet.add(new WorkspaceInfo("User", userKey));

        //String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        //wsInfoSet.add(new WorkspaceInfo("User", userKey));
        //String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        //if (CustomFieldDerivator.ignrWsKeys.contains(userKey)) {
            //return new HashSet<WorkspaceInfo>();
       // }
       // wsInfoSet.add(new WorkspaceInfo("User", userKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_CoreAcctTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "CoreAcctTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
