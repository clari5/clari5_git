clari5.custom.mapper {
        entity {
                NFT_NPCICHEQDETAIL {
                       generate = true
                        attributes:[
                                { name: EVENT_ID, type="string:50" ,key=true },
                                { name: ACCOUNT_ID ,type ="string:50" }
                                { name: TC_T ,type ="string:50" }
				{ name: MICR_NO_T ,type ="string:50" }
                                { name: SAN_T ,type ="string:50" }
				{ name: NEWLY_OPENED_ACCOUNT ,type ="string:50" }
                                { name: CHEQUE_NUM ,type ="string:50" }
				{ name: AMOUNT ,type ="number:30,2" }
                                { name: SYS_TIME ,type =timestamp }
				{ name: CHEQUE_NUMBER_T ,type ="string:50" }
                                { name: EVENTTS ,type =timestamp }
				{ name: IN_OUT_FLAG ,type ="string:50" }
                                { name: HOST_ID ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
                                { name: SERVER_ID ,type ="number" }
                                ]
                        }
        }
}
