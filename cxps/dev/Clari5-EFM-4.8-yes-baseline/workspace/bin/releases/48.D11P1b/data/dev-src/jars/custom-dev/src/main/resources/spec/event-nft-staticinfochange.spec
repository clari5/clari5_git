cxps.events.event.nft-staticinfochange{
  table-name : EVENT_NFT_STATICINFOCHANGE
  event-mnemonic: N
  workspaces : {
    ACCOUNT: acct_id
    CUSTOMER: cust-id
  }
  event-attributes : {
	tran-date: {db : true ,raw_name : tran_date ,type : timestamp,custom-getter:Tran_date}
	branch-id: {db : true ,raw_name : branch_id ,type : "string:200",custom-getter:Branch_id}
	Entitytype: {db : true ,raw_name : Entitytype ,type : "string:200"}
	initsubentityval: {db : true ,raw_name : initsubentityval ,type : "string:200"}
	finalsubentityval: {db : true ,raw_name : finalsubentityval ,type : "string:200"}
	acct-id: {db : true ,raw_name : acct_id ,type : "string:200",custom-getter:Acct_id}
	cust-id: {db : true ,raw_name : cust_id ,type : "string:200",custom-getter:Cust_id}
	channel: {db : true ,raw_name : channel ,type : "string:200"}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	host-id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id}
	}
}
