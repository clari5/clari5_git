package clari5.custom.core.utils;

import clari5.custom.db.Scenarios;

import clari5.custom.db.ScnFacts;
import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.*;


/**
 * @author shishir
 * @since 18/01/2018
 */

public class ScenariosContent {


    private static CxpsLogger logger = CxpsLogger.getLogger(ScenariosContent.class);

    final static Set<String> scnEventLists = new LinkedHashSet<>();
    static  String scnName ="";
    private void getEventList(String xml) {
        try {

            InputSource scnxml = new InputSource();
            scnxml.setCharacterStream(new StringReader(xml));

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder dbuilder = dbf.newDocumentBuilder();

            Document doc = dbuilder.parse(scnxml);
            doc.getDocumentElement().normalize();

            NodeList nodeList = doc.getElementsByTagName("def-query");

            NodeList tempOpenion = doc.getElementsByTagName("def-opinion");
            Node nodeOpnion = tempOpenion.item(0);

            Element  intu = (Element)nodeOpnion;
            scnName = intu.getAttribute("intuition");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    scnEventLists.add(eElement.getAttribute("collection"));

                }
            }

        } catch (Exception e) {
            logger.error("Failed to parse  " + e.getMessage() + " \n xml [" + xml + "]");
            scnEventLists.clear();

        }

    }



    private List<String> getEventFields(String eventName) {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("availableFields.conf");
        List<String> folderList = hocon.getStringList("eventFields." + eventName);
        return folderList;
    }


    /**
     * @return method will return list of only those scenarios which is written on ft_accounttxn event
     */
    public static  JSONObject getScnEventList(){
        Scenarios scnScenarios = new Scenarios();
        ScenariosContent content = new ScenariosContent();

        Map<String,ScnFacts>scncontentList =  scnScenarios.getScnContentMap();

        JSONObject jsonObject = new JSONObject();
        Set<String> ftscnList = new LinkedHashSet<>();

        try {

            String []wsList = {"account"};
            List<String> list= Arrays.asList(wsList);

            for (Map.Entry<String,ScnFacts> map : scncontentList.entrySet()) {
                String workspace = map.getValue().getWorkspaceName().toLowerCase();

                content.getEventList(map.getValue().getContent());

                JSONObject js = new JSONObject();

                // Display only ft_accounttxn event
                if (scnEventLists.contains("FT_CoreAcctTxnEvent") && scnEventLists.size() == 1 && list.contains(workspace)) {

                    ftscnList.add(scnName);
                    js.put("eventList", scnEventLists);
                    js.put("FT_CoreAcctTxnEvent", content.getEventFields("FT_CoreAcctTxnEvent"));
                    jsonObject.put(scnName, js);

                }
                scnEventLists.clear();

            }
            jsonObject.put("scenariosList", ftscnList);

        } catch (Exception e) {
            logger.error("Unable to create Json for eventList of scenarios " + e.getMessage());
        } finally {
            scnEventLists.clear();
        }

        return jsonObject;
    }
}
