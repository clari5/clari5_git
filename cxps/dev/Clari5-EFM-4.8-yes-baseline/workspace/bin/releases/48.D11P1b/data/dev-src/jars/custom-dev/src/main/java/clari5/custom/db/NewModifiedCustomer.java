package clari5.custom.db;

import clari5.custom.yes.db.NewModifiedCustomers;
import clari5.custom.yes.db.NewModifiedCustomersCriteria;
import clari5.custom.yes.db.NewModifiedCustomersSummary;
import clari5.custom.yes.db.mappers.NewAcctDetailsMapper;
import clari5.custom.yes.db.mappers.NewModifiedCustomersMapper;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import cxps.apex.utils.CxpsLogger;
import org.apache.ibatis.session.RowBounds;
import clari5.platform.rdbms.ObjectKeys;


import java.util.ArrayList;
import java.util.List;

public class NewModifiedCustomer extends DBConnection implements IDBOperation {
    private static final CxpsLogger logger = CxpsLogger.getLogger(NewModifiedCustomer.class);



    public  NewModifiedCustomer(){rdbmsConnection = new RDBMSConnection();}
    @Override
    public Object getAllRecords() {
        RDBMSSession rdbmsSession = getSession();


        try {
            NewModifiedCustomersMapper queue = rdbmsSession.getMapper(NewModifiedCustomersMapper.class);
            RowBounds rowBounds = new RowBounds(0, 10);
            List<NewModifiedCustomers> newModifiedCustomers = queue.getAllRecords(rowBounds);

            logger.info("NEW_MODIFIED_CUSTOMERS table record " + newModifiedCustomers.toString());
            return newModifiedCustomers;

        } catch (Exception e) {
            logger.error("Failed to fetch data from NEW_MODIFIED_CUSTOMERS table" + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public Object select(Object o) {
        NewModifiedCustomers newModifiedCustomer =new NewModifiedCustomers(getSession());

        String cust_id= (String) o;
        try {

            newModifiedCustomer.setCustId(cust_id);
            return newModifiedCustomer.select();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to fetch NEW_MODIFIED_CUSTOMERS table data of [" + cust_id + "] \n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;

    }

    @Override
    public int insert(Object o) {
        RDBMSSession session = getSession();
        int count = 0;
        NewModifiedCustomers newModifiedCustomer = (NewModifiedCustomers) o;

        try {

            count = newModifiedCustomer.insert(session);
            session.commit();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to insert into NEW_MODIFIED_CUSTOMERS " + newModifiedCustomer.toString() + "\n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }


        return count;
    }



    @Override
    public int update(Object o) {
        RDBMSSession session =getSession();
        int count=0;
        NewModifiedCustomers newModifiedCustomers =(NewModifiedCustomers)o;
        try {

            count = newModifiedCustomers.update(session);
            session.commit();

        }catch (RdbmsException rdbms){
            logger.error("Failed to update NEW_MODIFIED_CUSTOMERS"+newModifiedCustomers.toString());
            logger.error("Message "+rdbms.getMessage() +" Cause "+rdbms.getCause());
        }finally {
            closeRDMSSession();
        }
        return count;
    }

    /**
     * @return return fresh  record from NEW_MODIFIED_CUSTOMERS table
     */
    public List<NewModifiedCustomersSummary> getFreshRecord() {
        RDBMSSession rdbmsSession = getSession();

        try {

            NewModifiedCustomersMapper mapper = rdbmsSession.getMapper(NewModifiedCustomersMapper.class);
            NewModifiedCustomersCriteria criteria = new NewModifiedCustomersCriteria();
            criteria.setStatus("F");

            return mapper.searchNewModifiedCustomers(criteria);

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from NEW_MODIFIED_CUSTOMERS table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public int delete(Object o) {
        return 0;
    }


    public int update(String id,String status,String remarks){
        NewModifiedCustomers newModifiedCustomers =  (NewModifiedCustomers)select(id);

        newModifiedCustomers.setStatus(status);
        newModifiedCustomers.setRemarks(remarks);
        return update(newModifiedCustomers);
    }


    public List<NewModifiedCustomers> getFreshKeys(int offset,int limit) {
        try {
            RDBMSSession rdbmsSession = getSession();

            NewModifiedCustomersMapper queue = rdbmsSession.getMapper(NewModifiedCustomersMapper.class);
            RowBounds rowBounds = new RowBounds(offset, limit);


            List<NewModifiedCustomers> records = queue.selectRecords(new ObjectKeys(getFreshKeys(rdbmsSession)),rowBounds);

            return records;
        }
        catch (Exception e) {
            logger.error("failed to fetch pending  record from new modified customer details  table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }


    private List<String> getFreshKeys(RDBMSSession session){

        List<String> keys = new ArrayList<>();

        try {

            NewModifiedCustomersMapper mapper = session.getMapper(NewModifiedCustomersMapper.class);

            NewModifiedCustomersCriteria criteria = new NewModifiedCustomersCriteria();
            criteria.setStatus("F");
            List<NewModifiedCustomersSummary> newModifiedCustomersSummaries = mapper.searchNewModifiedCustomers(criteria);

            if (newModifiedCustomersSummaries.size()<=0){ return null;}

            for (NewModifiedCustomersSummary newcust :newModifiedCustomersSummaries) {
                keys.add(newcust.getCustId());
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  keys;

    }
}
