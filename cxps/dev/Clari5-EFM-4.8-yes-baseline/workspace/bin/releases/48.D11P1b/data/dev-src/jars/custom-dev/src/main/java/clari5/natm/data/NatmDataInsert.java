package clari5.natm.data;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.HashMap;


/*****
 *    @author : Suryakant
 *    @since : 06/02/2020
 */

public class NatmDataInsert {

    private static final CxpsLogger logger = CxpsLogger.getLogger(NatmDataInsert.class);
    static String insertNatmTable;
    static String updateSyncStatus; // added later for alert missing
    static Hocon hocon;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("natmQueryFilter.conf");
        insertNatmTable = hocon.getString("insertNatmData");
        updateSyncStatus = hocon.getString("updateNatmSync"); // added later for alert missing

    }


    public static void dataInsertNatmTable(HashMap jsonObject, CxConnection connection) {
        String eventId=jsonObject.get("EVENT_ID").toString();


        try (PreparedStatement statement = connection.prepareStatement(insertNatmTable);
            PreparedStatement updateSyncStatusStatement=connection.prepareStatement(updateSyncStatus)) {

            statement.setString(1, (String) jsonObject.get("SYSTEM"));
            statement.setString(2, (String) jsonObject.get("CUST_ID"));
            statement.setString(3, (String) jsonObject.get("CA_SCHEME_CODE"));
            statement.setString(4, (String) jsonObject.get("BEN_CUST_ID"));
            statement.setDouble(5, (Double) jsonObject.get("EFF_AVAILABLE_BALANCE"));
            statement.setString(6, (String) jsonObject.get("USER_ID"));
            statement.setString(7, (String) jsonObject.get("TRAN_CRNCY_CODE"));
            statement.setString(8, (String) jsonObject.get("PRODUCT_CODE"));
            statement.setTimestamp(9, (Timestamp) jsonObject.get("SYS_TIME"));
            statement.setDouble(10, (Double) jsonObject.get("TOD_GRANT_AMOUNT"));
            statement.setDouble(11, (Double) jsonObject.get("TRAN_AMT"));
            statement.setDouble(12, (Double) jsonObject.get("USD_EQV_AMT"));
            statement.setString(13, (String) jsonObject.get("STATUS"));
            statement.setString(14, (String) jsonObject.get("PURPOSE_CODE"));
            statement.setString(15, (String) jsonObject.get("REM_ADD1"));
            statement.setString(16, (String) jsonObject.get("BEN_BIC"));
            statement.setString(17, (String) jsonObject.get("REM_ADD2"));
            statement.setString(18, (String) jsonObject.get("FCC_PRODUCT_CODE"));
            statement.setString(19, (String) jsonObject.get("REM_ADD3"));
            statement.setString(20, (String) jsonObject.get("TRAN_ID"));
            statement.setString(21, (String) jsonObject.get("INSTRUMENT_TYPE"));
            statement.setString(22, (String) jsonObject.get("INR_AMOUNT"));
            statement.setString(23, (String) jsonObject.get("CLIENT_ACC_NO"));
            statement.setString(24, (String) jsonObject.get("CHANNEL_ID"));
            statement.setDouble(25, (Double) jsonObject.get("TRAN_AMOUNT"));
            statement.setString(26, (String) jsonObject.get("PURPOSE_DESC"));
            statement.setString(27, (String) jsonObject.get("BEN_ADD3"));
            statement.setString(28, (String) jsonObject.get("REM_TYPE"));
            statement.setString(29, (String) jsonObject.get("BEN_ADD1"));
            statement.setString(30, (String) jsonObject.get("BEN_ADD2"));
            statement.setTimestamp(31, (Timestamp) jsonObject.get("VALUE_DATE"));
            statement.setString(32, (String) jsonObject.get("ATM_ID"));
            statement.setString(33, (String) jsonObject.get("BEN_CITY"));
            statement.setString(34, (String) jsonObject.get("TRAN_NUMONIC_CODE"));
            statement.setString(35, (String) jsonObject.get("ISIN_NUMBER"));
            statement.setString(36, (String) jsonObject.get("TRAN_CURR"));
            //statement.setString(37, (String) jsonObject.get("EVENT_ID"));
            statement.setString(37, eventId);
            statement.setDouble(38, (Double) jsonObject.get("REF_TRAN_CRNCY"));
            statement.setString(39, (String) jsonObject.get("BEN_CNTRY_CODE"));
            statement.setTimestamp(40, (Timestamp) jsonObject.get("TRAN_DATE"));
            statement.setDouble(41, (Double) jsonObject.get("COD_MSG_TYPE"));
            statement.setString(42, (String) jsonObject.get("REM_NAME"));
           //Server Id
            statement.setDouble(43, 1);
            statement.setTimestamp(44, (Timestamp) jsonObject.get("TD_MATURITY_DATE"));
            statement.setString(45, (String) jsonObject.get("BEN_NAME"));
            statement.setString(46, (String) jsonObject.get("INSTRUMENT_NUMBER"));
            statement.setString(47, (String) jsonObject.get("REFERENCE_SRL_NUM"));
            statement.setString(48, "NEW");
            statement.setString(49, (String) jsonObject.get("PRODUCT_DESC"));
            statement.setString(50, (String) jsonObject.get("BEN_ACCT_NO"));
            statement.setString(51, (String) jsonObject.get("HOST_ID"));
            statement.setString(52, (String) jsonObject.get("TRAN_PARTICULAR"));
            statement.setString(53, (String) jsonObject.get("BANK_CODE"));
            statement.setString(54, (String) jsonObject.get("DRCR"));
            statement.setTimestamp(55, (Timestamp) jsonObject.get("INSERTION_TIME"));
            statement.setString(56, (String) jsonObject.get("BRANCH"));
            statement.setString(57, (String) jsonObject.get("ACCT_ID"));
            statement.setTimestamp(58, (Timestamp) jsonObject.get("PSTD_DATE"));
            statement.setDouble(59, (Double) jsonObject.get("REF_TRAN_AMT"));
            statement.setString(60, (String) jsonObject.get("CPTY_AC_NO"));
            statement.setString(61, (String) jsonObject.get("HOST_TYPE"));
            statement.setTimestamp(62, (Timestamp) jsonObject.get("TRN_DATE"));
            statement.setString(63, (String) jsonObject.get("INST_STATUS"));
            statement.setString(64, (String) jsonObject.get("PSTD_FLG"));
            statement.setString(65, (String) jsonObject.get("TD_LIQUIDATION_TYPE"));
            statement.setString(66, (String) jsonObject.get("REM_BIC"));
            statement.setString(67, (String) jsonObject.get("ONLINE_BATCH"));
            statement.setString(68, (String) jsonObject.get("REM_CNTRY_CODE"));
            statement.setString(69, (String) jsonObject.get("UCIC_ID"));
            statement.setString(70, (String) jsonObject.get("REM_CUST_ID"));
            statement.setString(71, (String) jsonObject.get("REM_ACCT_NO"));
            statement.setString(72, (String) jsonObject.get("REM_CITY"));
            statement.setString(73, (String) jsonObject.get("BRANCH_ID"));

            statement.executeQuery();
            updateSyncStatusStatement.setString(1,"SUCCESS");
            updateSyncStatusStatement.setString(2,eventId); // added for alert missing
            updateSyncStatusStatement.executeUpdate();
            connection.commit();


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
