package clari5.custom.yes.integration.builder;

import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;
import clari5.platform.util.ECClient;

public class Clari5GatewayManager {
    public static CxpsLogger logger = CxpsLogger.getLogger(Clari5GatewayManager.class);
    static {
        ECClient.configure(null);
    }

    public static boolean send(String event_id,JSONObject json, long event_ts) throws Exception {
        Long startTime=System.currentTimeMillis();
        String entity_id = "yes";
        boolean status= ECClient.enqueue("HOST",entity_id,event_id,json.toString());
        logger.info("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        Long endTime=System.currentTimeMillis();
        logger.info("[Clari5GatewayManager] Time Taken for getting response from enqueue"+(endTime-startTime));
        return status;
    }
}
