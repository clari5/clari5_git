// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_CREDENTIALCHANGE", Schema="rice")
public class NFT_CredentialChangeEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=200) public String addrNetLocal;
       @Field(size=20) public String twoFaStatus;
       @Field(size=200) public String ipCity;
       @Field(size=200) public String userId;
       @Field(size=200) public String errorDesc;
       @Field(size=10) public String succFailFlg;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String custId;
       @Field(size=200) public String ipCountry;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String deviceId;
       @Field(size=10) public String firstTimeFlag;
       @Field(size=200) public String riskBand;
       @Field(size=200) public String changeType;
       @Field(size=200) public String addrNetwork;
       @Field(size=2) public String hostId;
       @Field(size=200) public String changeMode;
       @Field(size=200) public String sessionId;


    @JsonIgnore
    public ITable<NFT_CredentialChangeEvent> t = AEF.getITable(this);

	public NFT_CredentialChangeEvent(){}

    public NFT_CredentialChangeEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("CredentialChange");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setAddrNetLocal(json.getString("addr_net_local"));
            setTwoFaStatus(json.getString("2fa_status"));
            setIpCity(json.getString("ip_city"));
            setUserId(json.getString("user_id"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustSegment(json.getString("cust_segment"));
            setCustId(json.getString("cust_id"));
            setIpCountry(json.getString("ip_country"));
            setErrorCode(json.getString("error_code"));
            setDeviceId(json.getString("device_id"));
            setFirstTimeFlag(json.getString("first_time_flag"));
            setChangeType(json.getString("change_type"));
            setAddrNetwork(json.getString("addr_network"));
            setHostId(json.getString("host_id"));
            setChangeMode(json.getString("change_mode"));
            setSessionId(json.getString("session_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setRiskBand(cxps.events.CustomFieldDerivator.checkInstanceofEvent(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NCC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public String getAddrNetLocal(){ return addrNetLocal; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public String getIpCity(){ return ipCity; }

    public String getUserId(){ return userId; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustSegment(){ return custSegment; }

    public String getCustId(){ return custId; }

    public String getIpCountry(){ return ipCountry; }

    public String getErrorCode(){ return errorCode; }

    public String getDeviceId(){ return deviceId; }

    public String getFirstTimeFlag(){ return firstTimeFlag; }

    public String getChangeType(){ return changeType; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getHostId(){ return hostId; }

    public String getChangeMode(){ return changeMode; }

    public String getSessionId(){ return sessionId; }
    public String getRiskBand(){ return riskBand; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setAddrNetLocal(String val){ this.addrNetLocal = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setFirstTimeFlag(String val){ this.firstTimeFlag = val; }
    public void setChangeType(String val){ this.changeType = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setChangeMode(String val){ this.changeMode = val; }
    public void setSessionId(String val){ this.sessionId = val; }
    public void setRiskBand(String val){ this.riskBand = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_CredentialChangeEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
       //wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

       WorkspaceInfo custInfo = new WorkspaceInfo("Customer", customerKey);
       custInfo.addParam("cxCifID", customerKey);
       wsInfoSet.add(custInfo);
       
        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_CredentialChange");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "CredentialChange");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
