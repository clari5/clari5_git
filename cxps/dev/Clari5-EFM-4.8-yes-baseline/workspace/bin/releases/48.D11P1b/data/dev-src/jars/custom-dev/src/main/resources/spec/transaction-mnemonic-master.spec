cxps.noesis.glossary.entity.transactional-mnemonic-master {
       db-name = TRANSACTIONAL_MNEMONIC_MASTER
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
		{ name = TRANSACTIONTYPE ,column = TRANSACTIONTYPE , type = "string:50", key=true }
                { name = TRANSUBTYPE ,column = TRANSUBTYPE , type = "string:50", key=true }
                { name = DESCRIPTION ,column = DESCRIPTION , type = "string:50", key=true }
                { name = AMOUNTTYPE ,column = AMOUNTTYPE , type = "string:50", key=true }
                { name = SECURITYID ,column = SECURITYID , type = "string:50", key=true }
                { name = CLS_CODE ,column = CLS_CODE , type = "string:50", key=true }
                { name = CLS_SUB_CODE ,column = CLS_SUB_CODE , type = "string:50", key=true }
                { name = TRANSACTIONGROUP ,column = TRANSACTIONGROUP , type = "string:50", key=true }
                { name = TRANSACTIONNAME ,column = TRANSACTIONNAME , type = "string:50", key=true }
                { name = ISCLEARING, column = ISCLEARING , type = "string:50", key=true }
                { name = ISTRANSFER ,column = ISTRANSFER , type = "string:50", key=true }
                { name = ISEFT ,column = ISEFT , type = "string:50", key=true }
                { name = CHANNELTYPE ,column = CHANNELTYPE , type = "string:50", key=true }
                { name = BASETYPE ,column = BASETYPE , type = "string:50", key=true }
                { name = ISCASH ,column = ISCASH , type = "string:50", key=true }
                { name = BEEHIVECHANNEL ,column = BEEHIVECHANNEL , type = "string:50", key=true }
                { name = UPDATETIMESTAMP ,column = UPDATETIMESTAMP, type = timestamp }

               ]
       }



