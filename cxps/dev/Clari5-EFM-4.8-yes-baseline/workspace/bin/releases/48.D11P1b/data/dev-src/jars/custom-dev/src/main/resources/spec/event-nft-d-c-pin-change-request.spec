cxps.events.event.nft-d-c-pin-change-request{
  table-name : EVENT_NFT_DCPINCHANGEREQUEST
  event-mnemonic: NDP
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
	host_id: {db : true ,raw_name : host_id ,type : "string:200" ,custom-getter:Host_id}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp ,custom-getter:Sys_time}
	succ_fail_flg: {db : true ,raw_name : succ_fail_flg ,type : "string:200" ,custom-getter:Succ_fail_flg}
	error_code: {db : true ,raw_name : error_code ,type : "string:200" ,custom-getter:Error_code}
	error_desc: {db : true ,raw_name : error_desc ,type : "string:200" ,custom-getter:Error_desc}
	card_no: {db : true ,raw_name : card_no ,type : "string:200" ,custom-getter:Card_no}
	cust_id: {db : true ,raw_name : cust_id ,type : "string:200" ,custom-getter:Cust_id}
	time_slot: {db : true ,raw_name : time_slot ,type : "string:200" ,custom-getter:Time_slot}
	online_offline: {db : true ,raw_name : Online_offline ,type : "string:200" ,custom-getter:Online_offline}
	rqst_chnl: {db : true ,raw_name : rqst_chnl ,type : "string:200" ,custom-getter:Rqst_chnl}
	event_type: {db : true ,raw_name : eventtype ,type : "string:200"}
	event_subtype: {db : true ,raw_name : eventsubtype ,type : "string:200"}
	event_name: {db : true ,raw_name : event_name ,type : "string:200" ,custom-getter:Event_name}
	}
}

