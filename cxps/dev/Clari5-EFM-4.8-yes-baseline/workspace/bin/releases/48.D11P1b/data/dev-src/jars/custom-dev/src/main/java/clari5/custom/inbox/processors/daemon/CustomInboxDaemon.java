package clari5.custom.inbox.processors.daemon;

import clari5.custom.inbox.processors.db.CustomIboxData;
import clari5.custom.inbox.processors.service.TatMailingService;
import clari5.custom.inbox.processors.service.TatMailingServiceImpl;
import clari5.custom.inbox.processors.tat.CustomCL5IbxItem;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.util.concurrent.ConcurrentLinkedQueue;

public class CustomInboxDaemon extends CxpsRunnable implements ICxResource {

    private static final CxpsLogger logger = CxpsLogger.getLogger(CustomInboxDaemon.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
        //System.out.println("inside getData: ");
        logger.debug("inside getData: ");
        return CustomIboxData.getCl5IbxItemData();
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
//        System.out.println("inside process data:");
        logger.debug("inside process data:");
        try {
            if (o instanceof ConcurrentLinkedQueue) {
                //System.out.println("inside if");
                logger.debug("inside if");
                ConcurrentLinkedQueue<CustomCL5IbxItem> createdItems = (ConcurrentLinkedQueue<CustomCL5IbxItem>) o;
                //System.out.println("created items: " + createdItems);
                logger.debug("created items: " + createdItems);
                TatMailingService tat = new TatMailingServiceImpl();
                tat.sendMail(createdItems);

                tat = null;

                //calling garbage collector
                System.gc();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void configure(Hocon h) {

        logger.debug("Custom Tat Inbox Daemon Configured");
        //System.out.println("Custom Tat Inbox Daemon Configured");
    }
}
