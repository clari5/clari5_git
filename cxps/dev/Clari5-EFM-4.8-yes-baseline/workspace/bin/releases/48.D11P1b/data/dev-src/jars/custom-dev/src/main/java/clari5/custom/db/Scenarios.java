package clari5.custom.db;

import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


/**
 * @author shishir
 * @since 22/01/18
 */


public class Scenarios extends DBConnection {
    private static final CxpsLogger logger = CxpsLogger.getLogger(Scenarios.class);

    Connection connection = null;
    ScnFacts scnFacts = null;

    public Scenarios() {
        jdbcConnection = new JDBCConnection();
    }

    private String getScnName(String snPath) {

        getScnWorkspace(snPath);
        String scnPath[] = snPath.split("/");

        String scnName  = scnPath[3].replace("_scenario.xml", "");

        scnFacts.setScnName(scnName);
        return scnName;
    }

    public  void getScnWorkspace(String scnPath){

        String workspace = scnPath.split("/")[1];
        scnFacts.setWorkspaceName(workspace);
    }



    public Map<String,ScnFacts> getScnContentMap (){
        Map<String,ScnFacts> scnContentMap = new HashMap<>();
        connection = (Connection) jdbcConnection.getConnection();
        String query = "select  \"path\",\"content\" from ARTIFACTMANAGEMENT where  \"path\"  like '%_scenario.xml' and \"status\"='Enabled'";

        try {
            PreparedStatement psmt = connection.prepareStatement(query);
            ResultSet rs = psmt.executeQuery();

            while (rs.next()) {
                scnFacts = new ScnFacts();
                scnFacts.setContent(rs.getString("content"));
                scnContentMap.put(getScnName(rs.getString("path")),scnFacts);
            }

            return scnContentMap;

        } catch (SQLException sql) {
            logger.error("Failed to get Scenarios name \n Query [" + query + "] Result "+scnContentMap.toString());

        } finally {
            closeJDBCConnection();
        }
        return scnContentMap;
    }

}
