package clari5.custom.jasper;


import clari5.custom.db.NewAccountDetails;
import clari5.custom.yes.db.NewAcctDetails;
import clari5.platform.util.CxJson;
import cxps.apex.utils.CxpsLogger;

import java.util.*;


/**
 * @author shishir
 * @since 21/05/2018
 */
public class UpdateMatchedResults {

    private static final CxpsLogger logger = CxpsLogger.getLogger(UpdateMatchedResults.class);


    private NewAcctDetails newAcctDetails = null;
    private NewAccountDetails newAccountDetails = null;

    UpdateMatchedResults(NewAccountDetails newAccountDetails) {
        this.newAccountDetails = newAccountDetails;
    }

    protected void matchResult(WlPayload wlPayload) {
        try {

            newAcctDetails = wlPayload.getNewAcctDetails();

            Matches matches = wlPayload.getMatchRecord();

            if (matches.getMatchType().isEmpty()) {
                newAcctDetails.setRemarks("rules not matched with any matched type");
                newAcctDetails.setMatchType("");
            } else {
                newAcctDetails.setRemarks("matched");
                newAcctDetails.setMatchType(matches.getMatchType().toString());
            }

            newAcctDetails.setStatus("C");

            String matchResultSize = matches.getMatchResult().toString();
            if (matchResultSize.length() > 8000) {
                matchResultSize = matchResultSize.substring(0, 8000);
            }
            newAcctDetails.setMatchResults(matchResultSize);


        } catch (Exception e) {

            logger.error("[UpdateMatchedResults.matchResult] Failed to update NEW_ACCT_DETAILS " + e.getMessage() + "\n Cause " + e.getCause());
            newAcctDetails.setStatus("E");
            newAcctDetails.setRemarks(e.getMessage());
        }

        logger.info(" NEW_ACCT_DETAILS table updated successfully  " + newAcctDetails.toString());

        newAccountDetails.update(newAcctDetails);
    }


}
