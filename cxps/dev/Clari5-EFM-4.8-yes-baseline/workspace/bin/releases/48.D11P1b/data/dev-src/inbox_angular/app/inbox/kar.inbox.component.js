"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nbb_data_service_1 = require("../services/nbb.data.service");
var common_functions_1 = require("../services/common.functions");
var nbb_service_1 = require("../services/nbb.service");
var KarInboxComponent = (function (_super) {
    __extends(KarInboxComponent, _super);
    function KarInboxComponent(nbbDataService, nbbService) {
        var _this = _super.call(this) || this;
        _this.nbbDataService = nbbDataService;
        _this.nbbService = nbbService;
        return _this;
    }
    //on click of create new template submit button
    KarInboxComponent.prototype.onClick = function (senario) {
        var flag = false;
        var echeckflag = "N";
        var icheckflag = "N";
        //checkbox validation
        this.nbbDataService.officeLIST.map(function (obj) {
            if (obj.checked === true) {
                flag = true; //set the flag again
                //this.checkbox=obj.officename;
                if (obj.officeID === 1) {
                    icheckflag = "Y";
                    obj.checked === false;
                }
                else if (obj.officeID === 2) {
                    echeckflag = "Y";
                    obj.checked === false;
                }
                else {
                    echeckflag = "Y";
                    icheckflag = "Y";
                    obj.checked === false;
                }
                console.log("checkbox", obj);
                obj.checked === false;
            }
        });
        if (flag === false) {
            // alert('atleast one checkbox should be checked');
            this.nbbDataService.alert = "false";
            flag = true; //reset the flag again
        }
        if (echeckflag == 'N' && icheckflag == 'N') {
            this.nbbDataService.alert = "false";
        }
        else {
            this.postData = JSON.stringify({
                "tempName": senario,
                "scnName": senario,
                //"autoEmailChqFlg": echeckflag,
                "autoInboxChqFlg": icheckflag,
                // "msgSubject": this.nbbDataService.ssubject,
                //"message": this.nbbDataService.sinbox,
                "createdBy": this.nbbDataService.user,
                "updatedBy": this.nbbDataService.user
            });
            this.method = "saveTemplate";
            console.log(this.postData);
            this.nbbService.getLoadData("", this.nbbDataService.url, this.method, this.postData);
        }
    };
    //on close of alert block
    KarInboxComponent.prototype.onClose = function () {
        var _this = this;
        this.nbbDataService.res = "";
        this.nbbDataService.showscreen = true;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.showavilscreen = true;
        this.nbbDataService.searchscenario = false;
        this.nbbDataService.scenarios = [];
        this.nbbDataService.eventlist = [];
        this.postData = JSON.stringify({});
        //  this.method = "init";
        console.log(this.postData);
        console.log(this.method);
        this.nbbService.getLoadData("scenario", this.nbbDataService.url, "init", this.postData);
        this.postDataa = JSON.stringify({});
        //this.method = "scenarioList";
        console.log(this.postData);
        console.log(this.method);
        // this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postDataa);
        setTimeout(function () {
            _this.nbbService.getLoadData("tempscenario", _this.nbbDataService.url, "scenarioList", _this.postData);
        }, 2000);
        this.nbbDataService.clear();
    };
    //on change event of check box
    KarInboxComponent.prototype.onCheck = function (event) {
        console.log("checkbox", event);
        // this.checkbox = item;
    };
    //on select of the scenario to load the event list
    /*  onSelect(event: any) {
          this.nbbDataService.scenario = event;
          console.log("scenario", this.nbbDataService.scenario);
          if (typeof this.nbbDataService.test[this.nbbDataService.scenario] == 'undefined') {
              this.nbbDataService.eventlist = "undefined";
              console.log(this.nbbDataService.eventlist);
          } else {
              this.nbbDataService.eventlist = this.nbbDataService.test[this.nbbDataService.scenario]["eventList"];
              console.log("eventlist", this.nbbDataService.eventlist);
          }
          this.nbbDataService.officeLIST[0].checked = false;
          this.nbbDataService.officeLIST[1].checked = false;
          this.nbbDataService.ssubject = "";
          this.nbbDataService.sinbox = "";
  
      }*/
    //on select of the existing template. event list, and the existing form data will be loaded 
    KarInboxComponent.prototype.onTempSelect = function (proj) {
        this.nbbDataService.scenario = proj;
        this.nbbDataService.sname = proj;
        console.log("scenario", this.nbbDataService.scenario);
        console.log("sname", this.nbbDataService.sname);
        var isAvailable = false;
        for (var x in this.nbbDataService.allscenarios["scenariosList"]) {
            console.log("X : ", this.nbbDataService.allscenarios["scenariosList"][x]);
        }
        ;
        var m = false;
        if (this.nbbDataService.allscenarios["scenariosList"].indexOf(this.nbbDataService.scenario) > -1) {
            m = true;
        }
        console.log("m", m);
        //to load the event list
        //typeof this.nbbDataService.test[this.nbbDataService.scenario] == 'undefined'
        if (!m) {
            this.nbbDataService.noTemp = "undefined";
            console.log(this.nbbDataService.eventlist);
            this.nbbDataService.showscreen = true;
            this.nbbDataService.screenshow = true;
            this.nbbDataService.searchscenario = false;
            this.nbbDataService.showavilscreen = true;
        }
        else {
            this.nbbDataService.showscreen = true;
            this.nbbDataService.screenshow = true;
            this.nbbDataService.searchscenario = true;
            this.nbbDataService.showavilscreen = false;
            //based on scenario parse and load the existing template data
            //this.nbbDataService.temp = this.nbbDataService.allscenarios[this.nbbDataService.scenario];
            this.nbbDataService.temp = JSON.parse(this.replaceAll(this.nbbDataService.allscenarios[this.nbbDataService.scenario], '\n', ' <br>'));
            console.log("temp data", this.nbbDataService.temp);
            //message
            /*this.nbbDataService.message = this.replaceAll(this.nbbDataService.temp.message, '<br>', '\n');
            console.log("message data", this.nbbDataService.temp.message);
            //subject
            this.nbbDataService.subject = this.nbbDataService.temp.msgSubject;*/
            //checkbox
            this.nbbDataService.icheckbox = this.nbbDataService.temp.autoInboxChqFlg;
            console.log("this.nbbDataService.icheckbox", this.nbbDataService.icheckbox);
            //this.nbbDataService.echeckbox = this.nbbDataService.temp.autoEmailChqFlg;
            //console.log("this.nbbDataService.echeckbox", this.nbbDataService.echeckbox);
            if (this.nbbDataService.icheckbox === "Y") {
                this.nbbDataService.officeLIST[0].checked = true;
                // this.nbbDataService.officeLIST[1].checked = true;
            } /*else if (this.nbbDataService.icheckbox === "Y" || this.nbbDataService.echeckbox === "N") {
                this.nbbDataService.officeLIST[0].checked = true;
               // this.nbbDataService.officeLIST[1].checked = false;
            } else if (this.nbbDataService.icheckbox === "N" && this.nbbDataService.echeckbox === "Y") {
                this.nbbDataService.officeLIST[0].checked = false;
                //this.nbbDataService.officeLIST[1].checked = true;
            } */
            else {
                this.nbbDataService.officeLIST[0].checked = false;
                //this.nbbDataService.officeLIST[1].checked = false;
            }
            this.nbbDataService.eventlist = this.nbbDataService.test[this.nbbDataService.scenario]["eventList"];
            console.log("eventlist", this.nbbDataService.eventlist);
        }
    };
    //to replace the newline character
    KarInboxComponent.prototype.escapeRegExp = function (string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    };
    /* Define functin to find and replace specified term with replacement string */
    KarInboxComponent.prototype.replaceAll = function (str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    };
    //on delete of the existing scenario
    KarInboxComponent.prototype.onDelete = function (senario) {
        this.postData = JSON.stringify({
            "tempName": senario,
            "scnName": senario,
            "updatedBy": this.nbbDataService.user
        });
        this.method = "deleteTempate";
        console.log(this.postData);
        console.log(this.method);
        this.nbbService.getLoadData("", this.nbbDataService.url, this.method, this.postData);
    };
    //on submit of the existing template.
    KarInboxComponent.prototype.onEdit = function (senario) {
        var flag = false;
        var echeckflag = "N";
        var icheckflag = "N";
        //checkbox validation
        this.nbbDataService.officeLIST.map(function (obj) {
            if (obj.checked === true) {
                flag = true; //set the flag again
                //this.checkbox=obj.officename;
                if (obj.officeID === 1) {
                    icheckflag = "Y";
                }
                else if (obj.officeID === 2) {
                    echeckflag = "Y";
                }
                else {
                    echeckflag = "Y";
                    icheckflag = "Y";
                }
                console.log("checkbox", obj);
            }
        });
        if (flag === false) {
            // alert('atleast one checkbox should be checked');
            this.nbbDataService.alert1 = "false";
            flag = true; //reset the flag again
        }
        if (echeckflag == 'N' && icheckflag == 'N') {
            this.nbbDataService.alert1 = "false";
        }
        else {
            this.postData = JSON.stringify({
                "tempName": senario,
                "scnName": senario,
                //"autoEmailChqFlg": echeckflag,
                "autoInboxChqFlg": icheckflag,
                // "msgSubject": sub,
                ///"message": inbox,
                "updatedBy": this.nbbDataService.user
            });
            this.method = "editTemplate";
            console.log(this.postData);
            console.log(this.method);
            this.nbbService.getLoadData("", this.nbbDataService.url, this.method, this.postData);
        }
    };
    //on close of the checkbox validation alert message
    KarInboxComponent.prototype.onClosealert = function () {
        this.nbbDataService.alert = "";
        this.nbbDataService.alert1 = "";
    };
    //on close of search template alert
    KarInboxComponent.prototype.onClosenoTemp = function () {
        this.nbbDataService.clear();
    };
    return KarInboxComponent;
}(common_functions_1.CommonFunctions));
KarInboxComponent = __decorate([
    core_1.Component({
        selector: 'inbox',
        template: require('../inbox/kar.inbox.template.html')
    }),
    __metadata("design:paramtypes", [nbb_data_service_1.NbbDataService, nbb_service_1.NbbService])
], KarInboxComponent);
exports.KarInboxComponent = KarInboxComponent;
//# sourceMappingURL=kar.inbox.component.js.map