<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    %>
    <%@ page import="java.sql.*, java.util.*,java.sql.ResultSetMetaData,java.beans.Statement,java.io.BufferedReader,java.io.File,java.io.FileInputStream,java.io.FileReader,java.io.IOException,java.io.PrintWriter"%>
    <%@ page import ="java.sql.Connection,java.sql.PreparedStatement,java.sql.ResultSet,java.sql.ResultSetMetaData,java.sql.SQLException,java.util.Iterator,java.util.StringTokenizer" %>
    <%@ page import="javax.servlet.ServletException,javax.servlet.http.HttpServlet,javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse,javax.servlet.http.HttpSession" %>
    <%@page import="org.apache.xmlbeans.impl.xb.xsdschema.Public" %>
     <%@ page import = "clari5.upload.ui.DBConnection" %>
     <%@ page import = "clari5.upload.ui.UploadUiAudit" %>
     <%@ page import = "clari5.upload.ui.User" %>

<%
Connection con= null;
PreparedStatement ps = null;
ResultSet rs=null;
ResultSetMetaData rsmd=null;
int columnCount=0;
String tableName = "";
String status = "";
String primary_key = "";
String primaryColName="";
String userId = (String)session.getAttribute("userId");
tableName=request.getParameter("table");
primary_key=request.getParameter("id");
String where = (String)session.getAttribute("where");
String audit_key="";

try
{
         con = DBConnection.getDBConnection();
         ps= con.prepareStatement("select * from "+tableName + where);
	 rs = ps.executeQuery();
	 rsmd = rs.getMetaData();
	 columnCount = rsmd.getColumnCount();
	 primaryColName=rsmd.getColumnName(1);
	 audit_key= request.getParameter(rsmd.getColumnName(1));
	 String updatrSql ="";
         System.out.println(" Table name ["+tableName+ "] columnCount ["+columnCount+"]");
	 while(rs.next()){
	 switch (columnCount)  {
                 case 5 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? , LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,userId);
                 break;


                 case 6 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=?,"+rsmd.getColumnName(2)+"=?,LCHG_TIME=systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,userId);
                 break;


                case 7: updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? , LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,userId);
                 break;

                case 8 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,userId);
                 break;

               case 9 : updatrSql ="update "+tableName+" set \""+rsmd.getColumnName(1)+"\"=? ,\""+rsmd.getColumnName(2)+"\"=? ,\""+rsmd.getColumnName(3)+"\"=? ,\""+rsmd.getColumnName(4)+"\"=?,\""+rsmd.getColumnName(5)+"\"=?,LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                          System.out.println("Query is ["+updatrSql+"]");
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,userId);
                 break;

                case 10 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,userId);
                 break;

                case 11 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,userId);
                 break;

                 case 12 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,userId);
                 break;

                case 13 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,userId);
                 break;

                case 14 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,userId);
                 break;

                case 15 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?,"+rsmd.getColumnName(11)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,request.getParameter(rsmd.getColumnName(11)));
                           ps.setString(12,userId);
                 break;

                case 16 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?,"+rsmd.getColumnName(11)+"=?,"+rsmd.getColumnName(12)+"=?,LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,request.getParameter(rsmd.getColumnName(11)));
                           ps.setString(12,request.getParameter(rsmd.getColumnName(12)));
                           ps.setString(13,userId);
                 break;

                 case 17 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?,"+rsmd.getColumnName(11)+"=?,"+rsmd.getColumnName(12)+"=?,"+rsmd.getColumnName(13)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,request.getParameter(rsmd.getColumnName(11)));
                           ps.setString(12,request.getParameter(rsmd.getColumnName(12)));
                           ps.setString(13,request.getParameter(rsmd.getColumnName(13)));
                           ps.setString(14,userId);
                 break;

                case 18 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?,"+rsmd.getColumnName(11)+"=?,"+rsmd.getColumnName(12)+"=?,"+rsmd.getColumnName(13)+"=?,"+rsmd.getColumnName(14)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,request.getParameter(rsmd.getColumnName(11)));
                           ps.setString(12,request.getParameter(rsmd.getColumnName(12)));
                           ps.setString(13,request.getParameter(rsmd.getColumnName(13)));
                           ps.setString(14,request.getParameter(rsmd.getColumnName(14)));
                           ps.setString(15,userId);
                 break;

                  case 19: updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?,"+rsmd.getColumnName(11)+"=?,"+rsmd.getColumnName(12)+"=?,"+rsmd.getColumnName(13)+"=?,"+rsmd.getColumnName(14)+"=?,"+rsmd.getColumnName(15)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,request.getParameter(rsmd.getColumnName(11)));
                           ps.setString(12,request.getParameter(rsmd.getColumnName(12)));
                           ps.setString(13,request.getParameter(rsmd.getColumnName(13)));
                           ps.setString(14,request.getParameter(rsmd.getColumnName(14)));
                           ps.setString(15,request.getParameter(rsmd.getColumnName(15)));
                           ps.setString(16,userId);
                 break;

                 case 20 : updatrSql ="update "+tableName+" set "+rsmd.getColumnName(1)+"=? ,"+rsmd.getColumnName(2)+"=? ,"+rsmd.getColumnName(3)+"=? ,"+rsmd.getColumnName(4)+"=?,"+rsmd.getColumnName(5)+"=?,"+rsmd.getColumnName(6)+"=?,"+rsmd.getColumnName(7)+"=?,"+rsmd.getColumnName(8)+"=?,"+rsmd.getColumnName(9)+"=?,"+rsmd.getColumnName(10)+"=?,"+rsmd.getColumnName(11)+"=?,"+rsmd.getColumnName(12)+"=?,"+rsmd.getColumnName(13)+"=?,"+rsmd.getColumnName(14)+"=?,"+rsmd.getColumnName(15)+"=?,"+rsmd.getColumnName(16)+"=?, LCHG_TIME =systimestamp,LCHG_USER=? " +where;
                 ps= con.prepareStatement(updatrSql);
                           ps.setString(1,request.getParameter(rsmd.getColumnName(1)));
                           ps.setString(2,request.getParameter(rsmd.getColumnName(2)));
                           ps.setString(3,request.getParameter(rsmd.getColumnName(3)));
                           ps.setString(4,request.getParameter(rsmd.getColumnName(4)));
                           ps.setString(5,request.getParameter(rsmd.getColumnName(5)));
                           ps.setString(6,request.getParameter(rsmd.getColumnName(6)));
                           ps.setString(7,request.getParameter(rsmd.getColumnName(7)));
                           ps.setString(8,request.getParameter(rsmd.getColumnName(8)));
                           ps.setString(9,request.getParameter(rsmd.getColumnName(9)));
                           ps.setString(10,request.getParameter(rsmd.getColumnName(10)));
                           ps.setString(11,request.getParameter(rsmd.getColumnName(11)));
                           ps.setString(12,request.getParameter(rsmd.getColumnName(12)));
                           ps.setString(13,request.getParameter(rsmd.getColumnName(13)));
                           ps.setString(14,request.getParameter(rsmd.getColumnName(14)));
                           ps.setString(15,request.getParameter(rsmd.getColumnName(15)));
                           ps.setString(16,request.getParameter(rsmd.getColumnName(16)));
                           ps.setString(17,userId);
                 break;
                 default:
                     System.out.println( "update failed for "+ tableName+ " \n Query is ["+updatrSql +"]"); break;
             }
             break;
             }
              System.out.println( "update  "+ tableName+ " \n Query is ["+updatrSql +"]");
             ps.executeUpdate();
        con.commit();

	 out.println("<script type=\"text/javascript\">");  
         out.println("alert(\"SUCCESSFULLY UPDATED\");");
         out.println("window.location.href='UpdateTable?table_name='+'"+tableName+"';");
         out.println("</script>");
          status = "Success";
}

catch(SQLException e)
{      
         out.println("<script type=\"text/javascript\">");
         out.println("alert(\"Exception while Updating\");");
         out.println("window.location.href='UpdateTable?table_name='+'"+tableName+"';");
         out.println("</script>");
	status="Failure";
        e.printStackTrace();
}
finally {
    try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
    try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}

        }
        status = UploadUiAudit.updateAudit(tableName,userId,status,audit_key,"","Update");


%>
