package clari5.custom.eventpreprocessors;

import clari5.platform.rdbms.RDBMSSession;

public interface Preprocessor {
    String countryCode(String ipAddress);
    void deviceBinding(RDBMSSession session, Object object);
    boolean fetchRecords(RDBMSSession session, Object object);
    String deviceIdCount(RDBMSSession session, Object object);
    void saveFirstLogin(RDBMSSession session, Object object);
    String timeSlots(Object object);
}
