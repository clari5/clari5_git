package clari5.custom.yes.integration.data;

public class NFT_Chequebookrequest extends ITableData {
    private String tableName = "NFT_CHEQBOOKREQ";
    private String event_type = "NFT_Chequebookrequest";
    private String  EVENT_ID;

    private String  endcheqnumber;
    private String account_id;
    private String avl_bal;
    private String customer_id;
    private String begincheqnumber;
    private String no_of_leav_isud;
    private String issue_date;
    private String sys_time;
    private String eventts;
    private String  system;
    private String host_id;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getEndcheqnumber() {
        return endcheqnumber;
    }

    public void setEndcheqnumber(String endcheqnumber) {
        this.endcheqnumber = endcheqnumber;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAvl_bal() {
        return avl_bal;
    }

    public void setAvl_bal(String avl_bal) {
        this.avl_bal = avl_bal;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getBegincheqnumber() {
        return begincheqnumber;
    }

    public void setBegincheqnumber(String begincheqnumber) {
        this.begincheqnumber = begincheqnumber;
    }

    public String getNo_of_leav_isud() {
        return no_of_leav_isud;
    }

    public void setNo_of_leav_isud(String no_of_leav_isud) {
        this.no_of_leav_isud = no_of_leav_isud;
    }

    public String getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(String issue_date) {
        this.issue_date = issue_date;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }
}

