import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpModule, RequestOptions } from "@angular/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { NbbMainComponent } from "./components/nbb.main.component";
import { NbbDataService } from "./services/nbb.data.service"
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NbbService } from './services/nbb.service';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { MentionModule } from 'angular-mentions/mention';
import { KarInboxComponent } from './inbox/kar.inbox.component';
import {PanelMenuModule, MenuItem} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
    declarations: [
        NbbMainComponent,KarInboxComponent
    ],
    imports: [
        BrowserModule,
        HttpModule, 
        ReactiveFormsModule, 
        FormsModule, 
        NgxMyDatePickerModule.forRoot(), 
        MultiselectDropdownModule, 
        MentionModule,
        PanelMenuModule,
        BrowserAnimationsModule
    ],
    providers: [

        NbbDataService, NbbService
    ],
    bootstrap: [
        NbbMainComponent
    ]
})
export class AppModule {
}



