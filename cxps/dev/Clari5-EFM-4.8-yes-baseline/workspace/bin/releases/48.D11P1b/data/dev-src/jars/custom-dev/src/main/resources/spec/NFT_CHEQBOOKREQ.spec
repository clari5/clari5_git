clari5.custom.mapper {
        entity {
                NFT_CHEQBOOKREQ {
                       generate = true
                        attributes:[
                                { name: EVENT_ID , type="string:2000" , key=true},
                                { name: ENDCHEQNUMBER ,type ="string:50" }
                                { name: ACCOUNT_ID ,type ="string:50" }
				{ name: AVL_BAL ,type ="string:50" }
				{ name: CUSTOMER_ID ,type ="string:50" }
				{ name: BEGINCHEQNUMBER ,type ="string:50" }
				{ name: NO_OF_LEAV_ISUD ,type ="string:50" }
				{ name: ISSUE_DATE ,type =timestamp }
				{ name: SYS_TIME ,type =timestamp }
				{ name: EVENTTS ,type =timestamp }
				{ name: SYSTEM ,type ="string:50" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
				{ name: SERVER_ID ,type ="number" }
                            ]
                        }
        }
}

