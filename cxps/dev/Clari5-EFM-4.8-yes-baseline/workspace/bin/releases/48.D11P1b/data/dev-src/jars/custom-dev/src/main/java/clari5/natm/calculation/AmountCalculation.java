package clari5.natm.calculation;

import clari5.custom.yes.integration.config.BepCon;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/*****
 *    @author : Suryakant
 *    @since : 07/02/2020
 */

public class AmountCalculation {
    private static CxpsLogger logger = CxpsLogger.getLogger(AmountCalculation.class);

    static Hocon hocon;
    static String natmCustomDataFetch;
    static String natmFlagUpdate;
    static String natmEventBacklog;
    static String natmInProgressCustomDataFetch;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("natmQueryFilter.conf");
        natmCustomDataFetch = hocon.getString("natmCustomDataFetch");
        natmFlagUpdate = hocon.getString("updateNatmFlag");
        natmEventBacklog = hocon.getString("selectEventBacklog");
        // added for in progress records
        natmInProgressCustomDataFetch=hocon.getString("natmInProgressCustomDataFetch");
    }

    public static ConcurrentLinkedQueue<JSONObject> getCustomData() {

        logger.info("Inside the method where we are fetching data from custom table");
        ConcurrentLinkedQueue<JSONObject> queue = new ConcurrentLinkedQueue<JSONObject>();

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(natmCustomDataFetch);
                     PreparedStatement updateFlag = connection.prepareStatement(natmFlagUpdate);
                     PreparedStatement progressStatement=connection.prepareStatement(natmInProgressCustomDataFetch);) {

                    /**
                     * progress statement to be executed for the records which are in I
                     */
                    progressStatement.setDouble(1, clari5.natm.calculation.BepCon.SERVER_ID);
                    ResultSet resultSet=progressStatement.executeQuery();

                        while(resultSet.next()){
                            String accountId = resultSet.getString("ACCT_ID");
                            Double transactionAmount = resultSet.getDouble("TRAN_AMT");
                            String eventId = resultSet.getString("EVENT_ID");
                            String customerId = resultSet.getString("CUST_ID");
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("ACCT_ID", accountId);
                            jsonObject.put("TRAN_AMT",transactionAmount);
                            jsonObject.put("EVENT_ID",eventId);
                            jsonObject.put("CUST_ID",customerId);
                            queue.add(jsonObject);
                            logger.info("Json String for records which are in I "+jsonObject.toString()+" Size of queue"+queue.size());
                        }


                        /**
                         * progress statement to be executed for the records which are in NEW
                         */
                        statement.setDouble(1,clari5.natm.calculation.BepCon.SERVER_ID);
                        resultSet=statement.executeQuery();
                        while (resultSet.next()){
                            String accountId = resultSet.getString("ACCT_ID");
                            Double transactionAmount = resultSet.getDouble("TRAN_AMT");
                            String eventId = resultSet.getString("EVENT_ID");
                            String customerId = resultSet.getString("CUST_ID");
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("ACCT_ID", accountId);
                            jsonObject.put("TRAN_AMT",transactionAmount);
                            jsonObject.put("EVENT_ID",eventId);
                            jsonObject.put("CUST_ID",customerId);
                            queue.add(jsonObject);
                            logger.info("Json String which are in NEW "+jsonObject.toString()+" Size of queue"+queue.size());
                            updateFlag.setString(1,"I");
                            updateFlag.setString(2,eventId);
                            updateFlag.executeUpdate();
                            connection.commit();
                        }

                }
                connection.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queue;
    }

    public static ConcurrentLinkedQueue<HashMap> getDataEventBacklog(){
        logger.info("Inside the method where we are fetching data from Event Backlog table");

        ConcurrentLinkedQueue<HashMap> queue = new ConcurrentLinkedQueue<HashMap>();

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(natmEventBacklog);) {
                    try (ResultSet resultSet = statement.executeQuery()) {

                        while (resultSet.next()) {
                             HashMap map = new HashMap();
                             map.put("ACCT_ID",resultSet.getString("ACCT_ID"));
                             map.put("EVENT_ID",resultSet.getString("EVENT_ID"));
                             map.put("CUST_ID",resultSet.getString("CUST_ID"));
                             map.put("TRIGGERING_EVENT_ID",resultSet.getString("TRIGGERING_EVENT_ID"));
                             map.put("CUM_TXN_AMT",resultSet.getDouble("CUM_TXN_AMT"));
                             queue.add(map);
                        }
                    }
                }
                connection.close();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return queue;
    }


}