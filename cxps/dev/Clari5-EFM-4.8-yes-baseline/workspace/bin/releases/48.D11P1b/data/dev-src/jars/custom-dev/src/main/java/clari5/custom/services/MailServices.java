package clari5.custom.services;

import clari5.platform.util.Hocon;

/**
 * @author shishir
 * @since 10/01/2018
 */
public abstract class MailServices {
    SMTPMailServices smtpMailServices;
    private MailFields mailFields;

    /**
     * Send mail to one user
     * @param mailFields
     * @return
     */
    public int sendIndvMail(MailFields mailFields) {
        return smtpMailServices.sendIndvMail(mailFields);
    }

    /**
     * Send mail to multiple users
     * @param mailFields
     * @return
     */
    public int sendMailToMulUser(MailFields mailFields) {
        return smtpMailServices.sendMailToMulUser(mailFields);
    }



    /**
     * Add multiple user in "CC" and single user in "TO" to send mail
     * @param mailFields
     * @return
     */
    public int sendMailwithCC(MailFields mailFields) {
        return smtpMailServices.sendMailwithCC(mailFields);
    }


    /**
     * Add multiple user in "CC" and multiple user in "TO" to send mail
     * @param mailFields
     * @return
     */
    public int sendMailMulUsrCC(MailFields mailFields) { return smtpMailServices.sendMailMulUsrCC(mailFields); }


    protected void configure(Hocon hocon) {

        mailFields = new MailFields();

        mailFields.setSmtpHost(hocon.getString("inbox-ms.smtp-host"));
        mailFields.setSmtpPort(hocon.getString("inbox-ms.smtp-port"));
        mailFields.setSender(hocon.getString("inbox-ms.smtp-sender"));
        mailFields.setSenderMailPass(hocon.getString("inbox-ms.smtp-senderpass"));
        mailFields.setIoTimeout(hocon.getString("inbox-ms.smtp-timeout"));
        mailFields.setConTimeOut(hocon.getString("inbox-ms.smtp-connectiontimeout"));
    }

    public MailFields getMailFields() {
        return mailFields;
    }
}
