package clari5.aml.cms.custom;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.EventFields;
import clari5.custom.db.RDBMSConnection;
import clari5.hfdb.Hfdb;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.jira.JiraClientException;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import org.json.JSONArray;
import org.json.JSONException;
import clari5.aml.cms.newjira.*;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by bisman on 12/16/16.
 */
public class CustomCmsFormatter extends clari5.aml.cms.newjira.DefaultCmsFormatter implements ICmsFormatter {

    static Hocon natmFacts;

    static {
        natmFacts = new Hocon();
        natmFacts.loadFromContext("natm_facts.conf");
    }

    public CustomCmsFormatter() throws CmsException {
        super();
    }

    @Override
    public String getCaseAssignee(Incident caseEvent) throws CmsException {
        System.out.println("custom CMs formatter get Case Assignee called...");
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        String moduleId = caseEvent.moduleId;
        CmsModule cmsModule = (CmsModule) cmsJira.get(moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        String userGroup = cmsModule.getUserGroup();

        String departmentName = caseEvent.project;
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        synchronized (userGroup) {
            Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
            for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
                if (entry.getValue() < noOfCases) {
                    noOfCases = entry.getValue();
                    resNextUser = entry.getKey();
                }
            }
            userCasesMap.put(resNextUser, ++noOfCases);
        }
        return resNextUser;
    }

    @Override
    public String getIncidentAssignee(Incident caseEvent) throws CmsException {
        System.out.println("custom CMs formatter get incident Assignee called...");
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }

        /**
         * Incident event FactName should exactly be the same as mentioned inside "natm_facts.conf" file.
         * If it will not match, NATM functionality for threshold upload will fail.
         */

        /*if (natmFacts.getStringList("fact_name.name").contains(caseEvent.factName)) {
            natmThresholdUpdate(caseEvent, getEventIdsFromIncident(caseEvent));
        }*/

        CmsModule cmsModule = (CmsModule) cmsJira.get(caseEvent.moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        String userGroup = cmsModule.getUserGroup();
        String departmentName = caseEvent.project;
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        synchronized (userGroup) {
            Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
            for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
                if (entry.getValue() < noOfCases) {
                    noOfCases = entry.getValue();
                    resNextUser = entry.getKey();
                }
            }
            userCasesMap.put(resNextUser, ++noOfCases);
        }
        return resNextUser;
    }

    /**
     * It consumes set of EventId's, adds up the txn amt for those eventId's and update the threshold for NATM.
     *
     * @param eventIds
     */
    private void natmThresholdUpdate(Incident incidentEvent, String eventIds) {
        logger.debug("Insdie Custom Cms Formatter natmThresholdupdate: ");
        RDBMSSession session = Rdbms.getAppSession();
        String sql = "UPDATE ACCOUNT_TRANSACTION set UPDATED_THRESHOLD = (select SUM(TRAN_AMT) * 2 " +
                "from EVENT_FT_COREACCT WHERE EVENT_ID IN (" + eventIds + ")),CUM_TXN_AMT=0,TS_COUNT=0 WHERE ACCOUNT_ID=?";
        logger.debug("update query for natm threshold upload: " + sql);
        try (CxConnection connection = session.getCxConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setString(1, incidentEvent.entityId.substring(4) != null ? incidentEvent.entityId.substring(4) : "");
                ps.executeUpdate();
                connection.commit();
            }
        } catch (SQLException e) {
            logger.debug("[CustomCmsFormater]:   "+ e.getMessage()
                    + " eventId's inside natm Threshold Update function: " + eventIds
                    + " factName inside natm Threshold Update function: " + incidentEvent.factName
                    + " entity_id inside natm Threshold Update function: " + incidentEvent.entityId);
            e.printStackTrace();
        }
    }

    /**
     * This function consumes incident object and return eventId's fromevidence
     *
     * @param incidentEvent
     * @return eventId's
     */
    private String getEventIdsFromIncident(Incident incidentEvent) {
        logger.debug("Inside Custom Cms Formatter getEventIdsFromIncident method...");
        JSONObject jsonObject = null;
        JSONArray array = null;
        StringBuilder sb = new StringBuilder("");
        try {
            jsonObject = new JSONObject(Optional.ofNullable(incidentEvent.evidence).get());
            array = jsonObject.has("evidenceData") ? jsonObject.getJSONArray("evidenceData") : new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = (JSONObject) array.get(i);
                List<String> list = Arrays.asList(object.get("eventIds").toString().replaceAll("\\[", "")
                        .replaceAll("\\]", "")
                        .replaceAll("\"", "").split(","));
                list.forEach((eventid) -> {
                    sb.append("'").append(eventid.trim()).append("',");
                });
            }
            sb.replace(sb.lastIndexOf(","), sb.length(), "");
            logger.debug("EventId's fetched successfully: " + sb.toString());
        } catch (Exception e) {
            logger.debug("[CustomCmsFormater]:   " + e.getMessage() + "\nJson object value --->  " + jsonObject + " \nJson Array value ---> " + array);
            e.printStackTrace();
        }
        return sb.toString();
    }

    public void postAssignmentAction(RDBMSSession session,Incident incidentEvent) throws CmsException {
//        CxConnection connection=null;
//        //session = Rdbms.getAppSession();
//
//        // RDBMS rdbms = (RDBMS) Clari5.rdbms();
//        Date sys_time = null;
//        if (incidentEvent.project.equals("NATM")) {
//            logger.debug("postAssignmentAction: IncidentEvent is NATM ");
//            logger.info("postAssignmentAction: IncidentEvent is NATM ");
//            try {
//                connection= session.getCxConnection();// ignore all exceptions here
//                logger.debug("postAssignmentAction:Calling Incident table update status to C");
//               logger.info("postAssignmentAction:Calling Incident table update status to C");
//                incidentEvent.status ="C";
//                incidentEvent.updateIncidentCloseState(incidentEvent,connection);
//                connection.commit();
//                session.commit();
//                logger.debug("postAssignmentAction:IncidentProcessed");
//                // icxLog.fexit();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            PreparedStatement pmst = null;
//            try{
//
//                StringBuilder params = new StringBuilder();
//                String triggerId = incidentEvent.processedeventid.trim();
//                String entityId = incidentEvent.entityId;
//                String accountId = entityId.substring(4, entityId.length());
//
//
//                StringBuilder sb = new StringBuilder("select SYS_TIME  from EVENT_FT_COREACCT WHERE EVENT_ID = ? AND ACCT_ID=?");
//                pmst = connection.prepareStatement(sb.toString());
//                logger.info("ACCOUNT_TRANSACTION Query " + sb);
//                pmst.setString(1,triggerId);
//                pmst.setString(2, accountId);
//                logger.debug("postAssignmentAction:Set AccoutnId");
//                ResultSet resultSet =  pmst.executeQuery();
//                if(resultSet.next()){
//                    sys_time = new Date(resultSet.getTimestamp("SYS_TIME").getTime());
//                }
//                if(sys_time==null || "".equals(sys_time) ) retry(incidentEvent);
//                logger.debug("postAssignmentAction:Proceed");
//            } catch (SQLException e) {
//                logger.debug("ERROR in ACCOUNT_TRANSACTION");
//                retry(incidentEvent);
//                e.printStackTrace();
//            } /*finally {
//                try {
//                    if (pmst != null) pmst.close();
//                    if (connection != null) connection.close();
//                } catch (SQLException e) {
//                    logger.debug("ERROR occured during update for ACCOUNT_TRANSACTION :", e.getMessage());
//                }
//
//            }*/
//
//
//            //event fileds set incident// eventity is-ws key , ws name , time, fact name
//            // call tostring - will return event json string - json string
//            //event id = entityid_system.currenttimeinmillsis
//            //ECClient.enqueue("HOST", eventid, json string);
//
//            EventFields eventFields = new EventFields();
//            eventFields.setEventId(incidentEvent.eventId.trim());
//            eventFields.setFactName(incidentEvent.factName.trim());
//            eventFields.setSysTime(sys_time);
//            eventFields.setWsKey(incidentEvent.entityId.trim().replace("A_F_", ""));
//            eventFields.setWsName("account");
//            logger.info("NFT_Scnclosure event is "+eventFields.toString()+incidentEvent.caseentityId.trim());
//            String event_id = incidentEvent.entityId+System.currentTimeMillis();
//            ECClient.enqueue("HOST" , event_id , eventFields.toString());
//            logger.info("writing to mq "+event_id);
//        }
    }

    public static void retry(Incident incidentEvent) {
        Date sys_time = null;
        StringBuilder sb = new StringBuilder("select SYS_TIME  from EVENT_FT_COREACCT WHERE EVENT_ID = ? AND ACCT_ID=?");
        try (Connection connection = Rdbms.getAppConnection();
             PreparedStatement selectPsmt = connection.prepareStatement(sb.toString());) {

            String triggerId = incidentEvent.eventId.trim();
            String entityId = incidentEvent.entityId;
            String accountId = entityId.substring(4, entityId.length());
            logger.info(" Inside Retry loop" + sb);

            selectPsmt.setString(1, triggerId);
            selectPsmt.setString(2, accountId);

            for (int i = 0; i < 3; i++) {
                logger.debug("Retry count" + i);
                ResultSet resultSet = selectPsmt.executeQuery();
                if (resultSet.next()) {
                    sys_time = new Date(resultSet.getTimestamp("SYS_TIME").getTime());
                }

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        EventFields eventFields = new EventFields();
        eventFields.setEventId(incidentEvent.eventId.trim());
        eventFields.setFactName(incidentEvent.factName.trim());
        eventFields.setSysTime(sys_time);
        eventFields.setWsKey(incidentEvent.entityId.trim().replace("A_F_", ""));
        eventFields.setWsName("account");
        logger.info("NFT_Scnclosure event is "+eventFields.toString()+incidentEvent.caseentityId.trim());
        String event_id = incidentEvent.entityId+System.currentTimeMillis();
        ECClient.enqueue("HOST" , event_id , eventFields.toString());
        logger.info("writing to mq "+event_id);
    }
}
