package clari5.custom.yes.integration.audit;

public interface IAuditClient {
	void log(String message);
	void log(LogLevel level, String message);
}
