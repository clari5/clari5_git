clari5.custom.mapper {
        entity {
                NFT_DCPINCHANGE {
                       generate = true
                        attributes:[
                                { name: EVENT_ID, type="string:50" ,key=true },
                                { name: SUCC_FAIL_FLG ,type ="string:50" }
                                { name: CARD_NO ,type ="string:50" }
				{ name: RQST_CHNL ,type ="number:30,2" }
                                { name: ERROR_DESC ,type ="string:50" }
				{ name: ERROR_CODE ,type ="string:50" }
                                { name: ONLINE_OFFLINE ,type ="string:50" }
				{ name: CUST_ID ,type ="string:50" }
                                { name: HOST_ID ,type ="string:50" }
				{ name: TIME_SLOT ,type ="string:50" }
                                { name: SYS_TIME ,type =timestamp }
				{ name: SYNC_STATUS ,type ="string:4000" , default="NEW" }
                                { name: TC_T ,type ="string:50" }
				{ name: SERVER_ID ,type ="number:10" }
                               ]
                        }
        }
}
