clari5.custom.yes.db {
        entity {
                FT_WMTXN {
                       generate = true
                        attributes:[
                                { name: EVENT_ID , type="string:4000" , key=true },
                                { name: TRAN_TYPE ,type ="string:500" }
                                { name: TRADE_DATE ,type ="string:500" }
				{ name: ACCTID ,type ="string:50" }
				{ name: WMS_REF_NO ,type ="string:500" }
				{ name: AMOUNT ,type ="number:30,2" }
				{ name: SCHEME_TYPE ,type ="string:50" }
				{ name: TRAN_CODE ,type ="string:50" }
				{ name: SUB_TRAN_TYPE ,type ="string:50" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: CUST_ID ,type ="string:50" }
				{ name: TXN_REF_NO ,type ="string:50" }
				{ name: SCHEME_NAME ,type ="string:50" }
				{ name: RM_CODE ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: EVENTTS ,type =timestamp }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
				{ name: SERVER_ID ,type ="number" }
                            ]
                        }
        }
}

