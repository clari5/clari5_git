// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_LINKDEBITCARD", Schema="rice")
public class NFT_LinkdebitcardEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=20) public String twoFaStatus;
       @Field(size=20) public String cardNumber;
       @Field(size=200) public String ipCity;
       @Field(size=200) public String obdxTransactionName;
       @Field(size=200) public String userId;
       @Field(size=200) public String errorDesc;
       @Field(size=10) public String succFailFlg;
       @Field(size=50) public String primaryAccountNumber;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String custId;
       @Field(size=200) public String ipCountry;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String obdxModuleName;
       @Field(size=200) public String deviceId;
       @Field(size=50) public String secondaryAccountNumbers;
       @Field(size=200) public String addrNetwork;
       @Field(size=2) public String hostId;
       @Field(size=200) public String sessionId;


    @JsonIgnore
    public ITable<NFT_LinkdebitcardEvent> t = AEF.getITable(this);

	public NFT_LinkdebitcardEvent(){}

    public NFT_LinkdebitcardEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Linkdebitcard");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setTwoFaStatus(json.getString("2fa_status"));
            setCardNumber(json.getString("card_number"));
            setIpCity(json.getString("ip_city"));
            setObdxTransactionName(json.getString("obdx_transaction_name"));
            setUserId(json.getString("user_id"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setPrimaryAccountNumber(json.getString("primary_account_number"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustSegment(json.getString("cust_segment"));
            setCustId(json.getString("cust_id"));
            setIpCountry(json.getString("ip_country"));
            setErrorCode(json.getString("error_code"));
            setObdxModuleName(json.getString("obdx_module_name"));
            setDeviceId(json.getString("device_id"));
            setSecondaryAccountNumbers(json.getString("secondary_account_numbers"));
            setAddrNetwork(json.getString("addr_network"));
            setHostId(json.getString("host_id"));
            setSessionId(json.getString("session_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "LDC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public String getCardNumber(){ return cardNumber; }

    public String getIpCity(){ return ipCity; }

    public String getObdxTransactionName(){ return obdxTransactionName; }

    public String getUserId(){ return userId; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getPrimaryAccountNumber(){ return primaryAccountNumber; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustSegment(){ return custSegment; }

    public String getCustId(){ return custId; }

    public String getIpCountry(){ return ipCountry; }

    public String getErrorCode(){ return errorCode; }

    public String getObdxModuleName(){ return obdxModuleName; }

    public String getDeviceId(){ return deviceId; }

    public String getSecondaryAccountNumbers(){ return secondaryAccountNumbers; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getHostId(){ return hostId; }

    public String getSessionId(){ return sessionId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setCardNumber(String val){ this.cardNumber = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setObdxTransactionName(String val){ this.obdxTransactionName = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setPrimaryAccountNumber(String val){ this.primaryAccountNumber = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setObdxModuleName(String val){ this.obdxModuleName = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setSecondaryAccountNumbers(String val){ this.secondaryAccountNumbers = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSessionId(String val){ this.sessionId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_LinkdebitcardEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Linkdebitcard");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Linkdebitcard");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}