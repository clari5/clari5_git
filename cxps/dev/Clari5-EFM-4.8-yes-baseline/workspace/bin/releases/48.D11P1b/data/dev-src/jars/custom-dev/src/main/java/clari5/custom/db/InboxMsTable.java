package clari5.custom.db;


import clari5.custom.yes.db.InboxMs;
import clari5.custom.yes.db.InboxMsCriteria;
import clari5.custom.yes.db.InboxMsSummary;
import clari5.custom.yes.db.mappers.InboxMsMapper;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import cxps.apex.utils.CxpsLogger;
import org.apache.ibatis.session.RowBounds;

import java.util.List;


/**
 * @author shishir
 * @since 11/01/2018
 */


public class InboxMsTable extends DBConnection implements IDBOperation {
    private static final CxpsLogger logger = CxpsLogger.getLogger(InboxMsTable.class);

    InboxMsTable() {
        rdbmsConnection = new RDBMSConnection();
    }

    @Override
    public Object getAllRecords() {

        RDBMSSession rdbmsSession = getSession();

        try {
            InboxMsMapper queue = rdbmsSession.getMapper(InboxMsMapper.class);
            RowBounds rowBounds = new RowBounds(0, 50);
            List<InboxMs> inboxMs = queue.getAllRecords(rowBounds);

            logger.info("inbox_ms table record " + inboxMs.toString());
            return inboxMs;

        } catch (Exception e) {
            logger.error("Failed to fetch data from inbox_ms table" + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public Object select(Object o) {

        RDBMSSession rdbmsSession = getSession();
        String incidentId = (String) o;

        try {
            InboxMs inboxMs = new InboxMs();

            inboxMs.setMapper(rdbmsSession);
            inboxMs.setIncidentId(incidentId);

            return inboxMs.select();

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from INBOX_MS table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    /**
     * @return return pending record from inbox_ms table
     */
    public List<InboxMsSummary> getPendingMailRecord() {
        RDBMSSession rdbmsSession = getSession();

        try {

            InboxMsMapper mapper = rdbmsSession.getMapper(InboxMsMapper.class);
            InboxMsCriteria criteria = new InboxMsCriteria();
            criteria.setEmailStatus("P");

            return mapper.searchInboxMs(criteria);

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from inbox_ms table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public int insert(Object o) {

        RDBMSSession rdbmsSession = getSession();
        int count = 0;
        InboxMs inboxMs = (InboxMs) o;
        try {

            count = inboxMs.insert(rdbmsSession);
            rdbmsSession.commit();

            logger.info(count +"row has inserted into inbox_ms table successfully");
            return count;

        }catch (Exception e ){
            logger.error("Failed to insert into inbox_ms table"+e.getMessage());

        }finally {
            closeRDMSSession();
        }

        return 0;
    }

    @Override
    public int update(Object o) {
        int count = 0;

        try {
            InboxMs inboxMs = (InboxMs) o;
            RDBMSSession rdbmsSession = getSession();

            count = inboxMs.update(rdbmsSession);

            rdbmsSession.commit();

            logger.info(count + " Row of  Inbox_MS table has updated successfully ");
            return count;

        } catch (RdbmsException.RecordAlreadyExists ex) {
            logger.error(" Fields are already present in Inbox_MS" + ex.getMessage());
        } catch (RdbmsException e)

        {
            logger.error("Failed to update INBOX_MS table " + e.getMessage());
        } catch (Exception e) {
            logger.info("Exception in updateEvent: " + e.getMessage() + " Cause: " + e.getCause());
        } finally {
            closeRDMSSession();
        }
        return count;
    }

    @Override
    public int delete(Object o) {
        return 0;
    }
}
