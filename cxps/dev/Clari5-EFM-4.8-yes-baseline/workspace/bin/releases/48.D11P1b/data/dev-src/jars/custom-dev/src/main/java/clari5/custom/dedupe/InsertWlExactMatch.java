package clari5.custom.dedupe;

import clari5.custom.mapper.DEDUPDETAILS;
import clari5.custom.mapper.mappers.DEDUPDETAILSMapper;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.rdbms.Rdbms;

import java.time.LocalDate;
import java.util.*;

public class InsertWlExactMatch {

    private static final CxpsLogger logger = CxpsLogger.getLogger(InsertWlExactMatch.class);

    protected void insertResultSet(LinkedHashMap<String, LinkedList<String>> wlResultSetMap,
                                                       LinkedHashMap<String, String> modifiedCustomerRecord) {
        //logger.debug("WL Result Set size for customer id -->>>> " + modifiedCustomerRecord.get("CUST_ID") + " is --->>>> " + wlResultSetMap.size());
        try (RDBMSSession session = (RDBMSSession) Rdbms.getAppSession()) {
            DEDUPDETAILSMapper mapper = session.getMapper(DEDUPDETAILSMapper.class);
            if (wlResultSetMap.size() > 0) {
                int count = 0;
                for (Map.Entry<String, LinkedList<String>> entry : wlResultSetMap.entrySet()) {
                    DEDUPDETAILS dedupdetails = new DEDUPDETAILS();
                    dedupdetails.setSERIALNO(UUID.randomUUID().toString());
                    dedupdetails.setCUSTOMERID(entry.getKey());
                    dedupdetails.setNEWMODIFIEDCUSTOMERID(modifiedCustomerRecord.get("CUST_ID"));
                    LinkedList<String> v = entry.getValue();
                    dedupdetails.setREPORTDATE(String.valueOf(LocalDate.now()));
                    dedupdetails.setWLRULENAME(!v.isEmpty() && v.get(0) != null ? v.get(0) : "");
                    dedupdetails.setWLRULESCORE(!v.isEmpty() && v.get(1) != null ? v.get(1) : "");
                    dedupdetails.setWLMATCHEDVALUE(!v.isEmpty() && v.size() > 3 ? v.get(2) + "," + v.get(3) : v.get(2));
                    mapper.insert(dedupdetails);
                    if(count++ > 1000) {
                        count = 0;
                        try {
                            session.commit();
                        } catch (RdbmsException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

                try {
                    session.commit();
                } catch (RdbmsException e) {
                    session.rollback();
                    e.printStackTrace();
                }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}