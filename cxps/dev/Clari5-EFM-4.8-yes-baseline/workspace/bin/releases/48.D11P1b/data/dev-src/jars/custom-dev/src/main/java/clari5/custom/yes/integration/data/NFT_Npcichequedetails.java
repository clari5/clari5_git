package clari5.custom.yes.integration.data;

public class NFT_Npcichequedetails extends ITableData  {
    private String tableName = "NFT_NPCICHEQDETAIL";
    private String event_type = "NFT_Npcichequedetails";
    private String EVENT_ID;
    private String account_id;
    private String tc_t;
    private String micr_no_t;
    private String san_t;
    private String newly_opened_account;
    private String cheque_num;
    private String amount;
    private String sys_time;
    private String cheque_number_t;
    private String eventts;
    private String in_out_flag;
    private String host_id;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getTc_t() {
        return tc_t;
    }

    public void setTc_t(String tc_t) {
        this.tc_t = tc_t;
    }

    public String getMicr_no_t() {
        return micr_no_t;
    }

    public void setMicr_no_t(String micr_no_t) {
        this.micr_no_t = micr_no_t;
    }

    public String getSan_t() {
        return san_t;
    }

    public void setSan_t(String san_t) {
        this.san_t = san_t;
    }

    public String getNewly_opened_account() {
        return newly_opened_account;
    }

    public void setNewly_opened_account(String newly_opened_account) {
        this.newly_opened_account = newly_opened_account;
    }

    public String getCheque_num() {
        return cheque_num;
    }

    public void setCheque_num(String cheque_num) {
        this.cheque_num = cheque_num;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getCheque_number_t() {
        return cheque_number_t;
    }

    public void setCheque_number_t(String cheque_number_t) {
        this.cheque_number_t = cheque_number_t;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getIn_out_flag() {
        return in_out_flag;
    }

    public void setIn_out_flag(String in_out_flag) {
        this.in_out_flag = in_out_flag;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }
}

