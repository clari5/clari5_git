package clari5.custom.db.wl;

import java.util.ArrayList;
import java.util.List;

public class TableTemplates {
    String tableName;
    final List<String> header = new ArrayList<>();
    final List<String> values = new ArrayList<>();

    public List<String> getHeader() {
        return header;
    }

    public List<String> getValues() {
        return values;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "TableTemplates{" +
                "tableName='" + tableName + '\'' +
                ", header=" + header.toString() +
                ", values=" + values.toString() +
                '}';
    }

    public String size() {
        return "TableTemplates{" +
                "tableName=" + tableName +
                "header=" + header.size() +
                ", values=" + values.size() +
                '}';
    }
}
