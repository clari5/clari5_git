package clari5.custom.db;

import clari5.custom.yes.db.InboxAutoReq;
import cxps.apex.utils.CxpsLogger;

import java.util.List;
import java.util.Set;

/**
 * @author shishir
 * @since 24/01/2018
 */


public class InboxTemplate {
    private static CxpsLogger logger = CxpsLogger.getLogger(InboxTemplate.class);

    private final InboxAutoReqTable inboxAutoReqTable;

    public  InboxTemplate(){
        inboxAutoReqTable = new InboxAutoReqTable();
    }

    /**
     *
     * @return method will fetch record from inbox_auto_req table
     */
    public List<InboxAutoReq> getAllRecords(){ return (List<InboxAutoReq>) inboxAutoReqTable.getAllRecords();
    }

    /**
     *
     * @param inboxAutoReq
     * @return no of row inserted into inbox_auto_req table
     */
    public  int insert(InboxAutoReq inboxAutoReq){
        return inboxAutoReqTable.insert(inboxAutoReq);
    }

    /**
     *
     * @param inboxAutoReq
     * @return no of row updated into inbox_auto_req table
     */
    public int update(InboxAutoReq inboxAutoReq){

        InboxAutoReq exitInboxAutoReq =  inboxAutoReqTable.select(inboxAutoReq.getScnName());
        inboxAutoReq.setCreatedBy(exitInboxAutoReq.getCreatedBy());

        return inboxAutoReqTable.update(inboxAutoReq);
    }


    /**
     *
     * @param scnName Scenarios Name
     * @return  no of row deleted from inbox_auto_req table based on @param
     */
    public  int delete(String scnName){
        return  inboxAutoReqTable.delete(scnName);
    }

    /**
     *
     * @param scnName Scenario Name
     * @return  fetch records from inbox_auto_req table based on @param
     */
    public InboxAutoReq select(String scnName){
        return inboxAutoReqTable.select(scnName);
    }


    public Set<String> getScnList(){
       return inboxAutoReqTable.getScnList();
    }
}
