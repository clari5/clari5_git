package clari5.custom.jasper;

import clari5.custom.db.NewAccountDetails;
import clari5.custom.yes.db.NewAcctDetails;
import clari5.custom.yes.db.NewAcctDetailsSummary;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import cxps.apex.utils.CxpsLogger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WlJsReportDaemon extends CxpsRunnable implements ICxResource {


    private static final Queue<WlPayload> eventDatas = new ConcurrentLinkedQueue<>();
    static final Set<String> acctIds = new HashSet<>();
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlJsReportDaemon.class);
    static NewAccountDetails newAccountDetails = new NewAccountDetails();
    static int offset = 0;
    static int limit = 10;

    @Override
    public void configure(Hocon h) {
        offset = h.getInt("offset");
        limit = h.getInt("limit");
    }

    @Override
    public void release() {
        acctIds.clear();
        eventDatas.clear();
    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void refresh() {
        acctIds.clear();
        eventDatas.clear();
    }


    @Override
    public void configure(Hocon h, Hocon location) {

    }

    @Override
    public void refresh(Hocon conf, Hocon location) {

    }

    @Override
    public ConfigType getType() {
        return null;
    }

    @Override
    protected Object getData() throws RuntimeFatalException {
        WlPayload wlPayload = getNextAcctId();
        WlRequestPayload wlRequestPayload = new WlRequestPayload();
        try {

            Queue<CxJson> queue = wlRequestPayload.setWlReqValues(wlPayload.getNewAcctDetails());

            wlPayload.setMatchRecord(wlRequestPayload.sendBatchWlReq(queue));
            logger.info("[getData] payload " + wlPayload.toString());


        } catch (NullPointerException np) {
            logger.warn("No fresh record in NEW_ACCT_DETAILS");
        }
        return wlPayload;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        try {
            WlPayload wlPayload = (WlPayload) o;
            UpdateMatchedResults updateMatchedResults = new UpdateMatchedResults(newAccountDetails);


            updateMatchedResults.matchResult(wlPayload);
        } catch (NullPointerException ne) {
        }

    }


    protected synchronized static WlPayload extractEvent() {
        logger.debug("No of acct-id to be processed : " + eventDatas.size() + "and Current Thread Id is " + Thread.currentThread().getId());
        while (!eventDatas.isEmpty()) {
            WlPayload data = eventDatas.poll();
            acctIds.remove(data.getAcct_id());
            return data;
        }
        return null;
    }


    protected static WlPayload getNextAcctId() throws NullPointerException {
        WlPayload data = extractEvent();
        if (data != null) {
            logger.debug("[" + Thread.currentThread().getName() + "] Retrieving  NEW_ACCT_DETAILS  data  of acct_id [" + data.getAcct_id() + "]");
            return data;
        }
        try {

            List<NewAcctDetails> newAcctList = newAccountDetails.getFreshKeys(offset, limit);

            for (NewAcctDetails newAcctDetails : newAcctList) {


                if (!acctIds.contains(newAcctDetails.getAcctId()) || acctIds.isEmpty()) {
                    acctIds.add(newAcctDetails.getAcctId());
                    eventDatas.add(new WlPayload(new Matches(), newAcctDetails, newAcctDetails.getAcctId()));

                }

            }
        } catch (Exception io) {
            logger.error("Failed to convert account summary to acct details" + io.getMessage() + "\n Cause" + io.getCause());
        }
        if ((data = extractEvent()) != null) {
            logger.debug("[" + Thread.currentThread().getName() + "] Retrieving NEW_ACCT_DETAILS    data  of acct_id [" + data.getAcct_id() + "]");
            return data;
        }

        return data;
    }

}