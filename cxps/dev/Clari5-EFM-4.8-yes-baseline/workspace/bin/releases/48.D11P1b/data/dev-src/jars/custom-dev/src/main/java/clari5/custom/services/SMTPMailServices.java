package clari5.custom.services;

/**
 * @author shishir
 * @since 10/01/2018
 */

public interface SMTPMailServices {
    /**
     * Send mail to one user
     * @param mailFields
     * @return
     */
    int sendIndvMail(MailFields mailFields);

    /**
     * Send mail to multiple users
     * @param mailFields
     * @return
     */
    int sendMailToMulUser(MailFields mailFields);

    /**
     * Add multiple user in "CC" and single user in "TO" to send mail
     * @param mailFields
     * @return
     */
    int sendMailwithCC(MailFields mailFields);

    /**
     * Add multiple user in "CC" and multiple user in "TO" to send mail
     * @param mailFields
     * @return
     */
    int sendMailMulUsrCC(MailFields mailFields);

}
