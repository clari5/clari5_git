package clari5.custom.jasper;

import clari5.custom.yes.db.NewAcctDetails;


public class WlPayload {
    private Matches matchRecord;
    private NewAcctDetails newAcctDetails;
    private String acct_id;


    public WlPayload(Matches matchRecord, NewAcctDetails newAcctDetails, String acct_id) {
        this.matchRecord = matchRecord;
        this.newAcctDetails = newAcctDetails;
        this.acct_id = acct_id;
    }

    public Matches getMatchRecord() {
        return matchRecord;
    }

    public void setMatchRecord(Matches matchRecord) {
        this.matchRecord = matchRecord;
    }

    public NewAcctDetails getNewAcctDetails() {
        return newAcctDetails;
    }

    public void setNewAcctDetails(NewAcctDetails newAcctDetails) {
        this.newAcctDetails = newAcctDetails;
    }

    public String getAcct_id() {
        return acct_id;
    }

    public void setAcct_id(String acct_id) {
        this.acct_id = acct_id;
    }


}
