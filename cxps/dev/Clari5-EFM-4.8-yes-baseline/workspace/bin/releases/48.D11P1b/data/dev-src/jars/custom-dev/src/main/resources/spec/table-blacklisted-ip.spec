clari5.custom.mapper {
        entity {
                BLACKLISTED_IP {
                       generate = true
                        attributes:[
		                 { name = blacklist-ip, column = blacklist_ip, type = "string:50", key=true },
                                 { name = START_TIME, column = START_TIME, type = timestamp },
                                 { name = END_TIME, column = END_TIME, type = timestamp },
                                 { name: RCRE_TIME ,type = timestamp},
                                 { name: RCRE_USER ,type = "string:50"},
                                 { name: LCHG_TIME ,type = timestamp},
                                 { name: LCHG_USER ,type = "string:50"}
  
                                ]
                        }
        }
}
