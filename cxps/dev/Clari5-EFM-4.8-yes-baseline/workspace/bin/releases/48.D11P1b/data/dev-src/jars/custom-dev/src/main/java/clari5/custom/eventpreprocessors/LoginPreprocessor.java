package clari5.custom.eventpreprocessors;

import clari5.custom.mapper.CUSTFIRSTLOGIN;
import clari5.custom.mapper.DEVICEBINDING;
import clari5.custom.mapper.DEVICEBINDINGKey;
import clari5.custom.mapper.mappers.CUSTFIRSTLOGINMapper;
import clari5.custom.mapper.mappers.DEVICEBINDINGMapper;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.rdbms.Rdbms;
import clari5.tools.util.Hocon;
import com.maxmind.geolite.MaxmindUtil;
import cxps.events.NFT_IbLoginEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class LoginPreprocessor implements Preprocessor {

    private static final CxpsLogger logger = CxpsLogger.getLogger(LoginPreprocessor.class);

    static HashMap<String, String> timeSlotMap = new HashMap<>();
    static Hocon slot;

    static {
        slot = new Hocon();
        slot.loadFromContext("timeslot.conf");
        slot.getStringList("slot.slotList").forEach(k -> timeSlotMap.put(k, slot.getString("slot." + k)));
    }

    private static NFT_IbLoginEvent getInstance(Object object) {
        if (object instanceof NFT_IbLoginEvent) {
            return (NFT_IbLoginEvent) object;
        }
        return null;
    }

    @Override
    public String countryCode(String ipAddress) {
        return MaxmindUtil.getCountryCode(ipAddress != null ? ipAddress : "");
    }

    @Override
    public void deviceBinding(RDBMSSession session, Object object) {
        long start = System.currentTimeMillis();
        try {
            DEVICEBINDINGMapper mapper = session.getMapper(DEVICEBINDINGMapper.class);
            DEVICEBINDING devicebinding = new DEVICEBINDING();
            devicebinding.setUSERID(getInstance(object).getUserId());
            devicebinding.setCUSTID(getInstance(object).getCustId());
            devicebinding.setDEVICEID(getInstance(object).getDeviceId());
            System.out.println("System time --> " + getInstance(object).getSysTime().getHours());
            System.out.println("System time --> " + getInstance(object).getSysTime().getMinutes());
            devicebinding.setDBDATE(getInstance(object).getSysTime());
            mapper.insert(devicebinding);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //session.commit();
                session.close();
                //RdbmsException
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("deviceBinding DB insertion--> " + (System.currentTimeMillis() - start));
        logger.debug("deviceBinding DB insertion--> " + (System.currentTimeMillis() - start));
    }

    public static String getCountryCode(Object object) {
        Preprocessor preprocessor = new LoginPreprocessor();
        return preprocessor.countryCode(getInstance(object).getAddrNetwork()) != null ?
                preprocessor.countryCode(getInstance(object).getAddrNetwork()) : "";
    }

    @Override
    public boolean fetchRecords(RDBMSSession session, Object object) {
        return getRecords(session, object);
    }

    private boolean getRecords(RDBMSSession session, Object object) {

        long start = System.currentTimeMillis();
        boolean flag = true;
        try {
            DEVICEBINDINGMapper mapper = session.getMapper(DEVICEBINDINGMapper.class);
            DEVICEBINDINGKey key = new DEVICEBINDINGKey();
            key.setUSERID(getInstance(object).getUserId());
            key.setCUSTID(getInstance(object).getCustId());
            key.setDEVICEID(getInstance(object).getDeviceId());
            DEVICEBINDING db = mapper.select(key);
            if (db == null) {
                flag = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.commit();
               // session.close();
            } catch (RdbmsException e) {
                e.printStackTrace();
            }
        }
        System.out.println("getRecords--> " + (System.currentTimeMillis() - start));
        logger.debug("getRecords--> " + (System.currentTimeMillis() - start));
        return flag;
    }

    private static RDBMSSession getSession() {
        return Rdbms.getAppSession();
    }

    public static String saveInRdbms(Object object) {
        long start = System.currentTimeMillis();
        boolean flag = false;
        Preprocessor preprocessor = new LoginPreprocessor();
        RDBMSSession session = getSession();
        if (!preprocessor.fetchRecords(session, object)) {
            preprocessor.deviceBinding( session,object);
            flag = true;
        }
        if (session != null) {
            session.close();
        } else throw new RuntimeException("unable to acquire Rdbms session...");
        System.out.println("saveInRdbms--> " + (System.currentTimeMillis() - start));
        logger.debug("saveInRdbms--> " + (System.currentTimeMillis() - start));
        return String.valueOf(flag);
    }

    public static String deviceCount(Object object) {
        long start = System.currentTimeMillis();
        boolean flag = saveInFirstLogin(object);
        logger.debug("first login of customer status: " + flag + " for cust_id " + getInstance(object).getCustId());
        Preprocessor preprocessor = new LoginPreprocessor();
        System.out.println("deviceCount--> " + (System.currentTimeMillis() - start));
        logger.debug("deviceCount--> " + (System.currentTimeMillis() - start));
        return preprocessor.deviceIdCount(getSession(), object);
    }

    @Override
    public String deviceIdCount(RDBMSSession session, Object object) {
        long start = System.currentTimeMillis();
        String count = "";
        try (CxConnection connection = session.getCxConnection()) {
            try (PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) AS DEVICECOUNT FROM DEVICE_BINDING WHERE CUST_ID = ?")) {
                ps.setString(1, getInstance(object).getCustId());
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        count = rs.getString("DEVICECOUNT");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                session.commit();
                session.close();;
            } catch (RdbmsException e) {
                e.printStackTrace();
            }
        }
        System.out.println("deviceIdCount override method--> " + (System.currentTimeMillis() - start));
        logger.debug("deviceIdCount override method--> " + (System.currentTimeMillis() - start));
        return count;
    }

    @Override
    public void saveFirstLogin(RDBMSSession session, Object object) {
        long start = System.currentTimeMillis();
        try {
            CUSTFIRSTLOGINMapper mapper = session.getMapper(CUSTFIRSTLOGINMapper.class);
            CUSTFIRSTLOGIN custfirstlogin = new CUSTFIRSTLOGIN();
            custfirstlogin.setCUSTID(getInstance(object).getCustId());
            custfirstlogin.setFIRSTLOGINDATE(getInstance(object).getSysTime());
            mapper.insert(custfirstlogin);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.commit();
                session.close();
            } catch (RdbmsException e) {
                e.printStackTrace();
            }
        }
        System.out.println("saveFirstLogin --> " + (System.currentTimeMillis() - start));
        logger.debug("saveFirstLogin --> " + (System.currentTimeMillis() - start));
    }

    public static String getTimeSlot(Object object) {
        Preprocessor preprocessor = new LoginPreprocessor();
        return preprocessor.timeSlots(object);
    }

    @Override
    public String timeSlots(Object object) {
        long start = System.currentTimeMillis();
        int hrs = getInstance(object).getSysTime().getHours();
        //int min = getInstance(object).getSysTime().getMinutes();

        StringBuilder sb = new StringBuilder();
        timeSlotMap.forEach((k, v) -> {
            String[] timeRange = k.split("-");
            if (hrs >= Integer.valueOf(timeRange[0]) && hrs < Integer.valueOf(timeRange[1])) {
                sb.append(v);
            }
        });
        System.out.println("time slot derived for time: " + getInstance(object).getSysTime() + " slot: " + sb);
        System.out.println("timeSlots --> " + (System.currentTimeMillis() - start));
        logger.debug("timeSlots --> " + (System.currentTimeMillis() - start));
        return sb.toString();
    }

    public static boolean saveInFirstLogin(Object object) {
        long start = System.currentTimeMillis();
        boolean flag = false;
        Preprocessor preprocessor = new LoginPreprocessor();
        NFT_IbLoginEvent login = getInstance(object);
        if (login.getFirstLogin().equalsIgnoreCase("Yes")) {
            RDBMSSession session = getSession();
            preprocessor.saveFirstLogin(session, object);
            flag = true;
            if (session != null) {
                session.close();
            } else throw new RuntimeException("unable to acquire Rdbms session...");
        }
        System.out.println("saveFirstLogin --> " + (System.currentTimeMillis() - start));
        logger.debug("saveFirstLogin --> " + (System.currentTimeMillis() - start));
        return flag;
    }
}