package clari5.custom.db;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author shishir
 * @since 11/01/2018
 */
public class JDBCConnection implements IDBConnection {
    private static final CxpsLogger logger = CxpsLogger.getLogger(MSDBOperation.class);
    private Connection connection;

    @Override
    public Object getConnection() {
        connection = Rdbms.getAppConnection();
        return connection;
    }

    @Override
    public void closeConnection() {
        try {
            if (connection != null) connection.close();
        } catch (SQLException sql) {
            logger.error(logger.getClass() + " Unable to close  connection ");
        }
    }

}
