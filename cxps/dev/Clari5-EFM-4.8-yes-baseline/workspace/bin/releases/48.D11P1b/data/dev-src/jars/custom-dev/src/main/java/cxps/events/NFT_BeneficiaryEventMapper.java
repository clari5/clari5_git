// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_BeneficiaryEventMapper extends EventMapper<NFT_BeneficiaryEvent> {

public NFT_BeneficiaryEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_BeneficiaryEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_BeneficiaryEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_BeneficiaryEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_BeneficiaryEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_BeneficiaryEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_BeneficiaryEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTwoFaMode());
            preparedStatement.setString(i++, obj.getBeneficiaryName());
            preparedStatement.setString(i++, obj.getBenfFlag1());
            preparedStatement.setString(i++, obj.getBenfFlag5());
            preparedStatement.setString(i++, obj.getBenfFlag4());
            preparedStatement.setString(i++, obj.getBenfFlag3());
            preparedStatement.setString(i++, obj.getBenfFlag2());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getPayeeAccttype2());
            preparedStatement.setString(i++, obj.getBenfFlag9());
            preparedStatement.setString(i++, obj.getPayeeAccttype3());
            preparedStatement.setString(i++, obj.getBenfFlag8());
            preparedStatement.setString(i++, obj.getPayeeAccttype4());
            preparedStatement.setString(i++, obj.getBenfFlag7());
            preparedStatement.setString(i++, obj.getPayeeAccttype5());
            preparedStatement.setString(i++, obj.getBenfFlag6());
            preparedStatement.setString(i++, obj.getGroupPayeeId());
            preparedStatement.setString(i++, obj.getPayeeAccttype6());
            preparedStatement.setString(i++, obj.getBenfUniqueValue9());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getPayeeAccttype7());
            preparedStatement.setString(i++, obj.getBenfUniqueValue7());
            preparedStatement.setString(i++, obj.getPayeeAccttype8());
            preparedStatement.setString(i++, obj.getBenfUniqueValue8());
            preparedStatement.setString(i++, obj.getPayeeAccttype9());
            preparedStatement.setString(i++, obj.getBenfUniqueValue5());
            preparedStatement.setString(i++, obj.getBenfUniqueValue6());
            preparedStatement.setString(i++, obj.getBenfUniqueValue3());
            preparedStatement.setString(i++, obj.getBenfUniqueValue4());
            preparedStatement.setString(i++, obj.getBenfUniqueValue1());
            preparedStatement.setString(i++, obj.getPayeeAccttype10());
            preparedStatement.setString(i++, obj.getBenfUniqueValue2());
            preparedStatement.setString(i++, obj.getBenfFlag10());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getObdxTransactionName());
            preparedStatement.setString(i++, obj.getBenfNickname10());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getObdxModuleName());
            preparedStatement.setString(i++, obj.getBenfPayeeId10());
            preparedStatement.setString(i++, obj.getBenfStatus());
            preparedStatement.setString(i++, obj.getSessionId());
            preparedStatement.setString(i++, obj.getBenfUniqueValue10());
            preparedStatement.setString(i++, obj.getBenfType10());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getCustSegment());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getBenfType9());
            preparedStatement.setString(i++, obj.getBenfType8());
            preparedStatement.setString(i++, obj.getBenfType7());
            preparedStatement.setString(i++, obj.getBenfType6());
            preparedStatement.setString(i++, obj.getBenfPayeeId1());
            preparedStatement.setString(i++, obj.getBenfType5());
            preparedStatement.setString(i++, obj.getBenfType4());
            preparedStatement.setString(i++, obj.getBenfType3());
            preparedStatement.setString(i++, obj.getBenfType2());
            preparedStatement.setString(i++, obj.getBenfType1());
            preparedStatement.setString(i++, obj.getBenfNickname4());
            preparedStatement.setString(i++, obj.getBenfNickname5());
            preparedStatement.setString(i++, obj.getTwoFaStatus());
            preparedStatement.setString(i++, obj.getBenfNickname6());
            preparedStatement.setString(i++, obj.getBenfNickname7());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getBenfNickname1());
            preparedStatement.setString(i++, obj.getBenfNickname2());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getBenfNickname3());
            preparedStatement.setString(i++, obj.getBenfPayeeId3());
            preparedStatement.setString(i++, obj.getBenfPayeeId2());
            preparedStatement.setString(i++, obj.getBenfPayeeId5());
            preparedStatement.setString(i++, obj.getBenfPayeeId4());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getBenfPayeeId7());
            preparedStatement.setString(i++, obj.getBenfNickname8());
            preparedStatement.setString(i++, obj.getBenfPayeeId6());
            preparedStatement.setString(i++, obj.getBenfNickname9());
            preparedStatement.setString(i++, obj.getBenfPayeeId9());
            preparedStatement.setString(i++, obj.getBenfPayeeId8());
            preparedStatement.setString(i++, obj.getRiskBand());
            preparedStatement.setString(i++, obj.getPayeeAccttype1());
            preparedStatement.setString(i++, obj.getBenfAddition());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_BENEFICIARY]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_BeneficiaryEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_BeneficiaryEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_BENEFICIARY"));
        putList = new ArrayList<>();

        for (NFT_BeneficiaryEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TWO_FA_MODE",  obj.getTwoFaMode());
            p = this.insert(p, "BENEFICIARY_NAME",  obj.getBeneficiaryName());
            p = this.insert(p, "BENF_FLAG1",  obj.getBenfFlag1());
            p = this.insert(p, "BENF_FLAG5",  obj.getBenfFlag5());
            p = this.insert(p, "BENF_FLAG4",  obj.getBenfFlag4());
            p = this.insert(p, "BENF_FLAG3",  obj.getBenfFlag3());
            p = this.insert(p, "BENF_FLAG2",  obj.getBenfFlag2());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "PAYEE_ACCTTYPE2",  obj.getPayeeAccttype2());
            p = this.insert(p, "BENF_FLAG9",  obj.getBenfFlag9());
            p = this.insert(p, "PAYEE_ACCTTYPE3",  obj.getPayeeAccttype3());
            p = this.insert(p, "BENF_FLAG8",  obj.getBenfFlag8());
            p = this.insert(p, "PAYEE_ACCTTYPE4",  obj.getPayeeAccttype4());
            p = this.insert(p, "BENF_FLAG7",  obj.getBenfFlag7());
            p = this.insert(p, "PAYEE_ACCTTYPE5",  obj.getPayeeAccttype5());
            p = this.insert(p, "BENF_FLAG6",  obj.getBenfFlag6());
            p = this.insert(p, "GROUP_PAYEE_ID",  obj.getGroupPayeeId());
            p = this.insert(p, "PAYEE_ACCTTYPE6",  obj.getPayeeAccttype6());
            p = this.insert(p, "BENF_UNIQUE_VALUE9",  obj.getBenfUniqueValue9());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "PAYEE_ACCTTYPE7",  obj.getPayeeAccttype7());
            p = this.insert(p, "BENF_UNIQUE_VALUE7",  obj.getBenfUniqueValue7());
            p = this.insert(p, "PAYEE_ACCTTYPE8",  obj.getPayeeAccttype8());
            p = this.insert(p, "BENF_UNIQUE_VALUE8",  obj.getBenfUniqueValue8());
            p = this.insert(p, "PAYEE_ACCTTYPE9",  obj.getPayeeAccttype9());
            p = this.insert(p, "BENF_UNIQUE_VALUE5",  obj.getBenfUniqueValue5());
            p = this.insert(p, "BENF_UNIQUE_VALUE6",  obj.getBenfUniqueValue6());
            p = this.insert(p, "BENF_UNIQUE_VALUE3",  obj.getBenfUniqueValue3());
            p = this.insert(p, "BENF_UNIQUE_VALUE4",  obj.getBenfUniqueValue4());
            p = this.insert(p, "BENF_UNIQUE_VALUE1",  obj.getBenfUniqueValue1());
            p = this.insert(p, "PAYEE_ACCTTYPE10",  obj.getPayeeAccttype10());
            p = this.insert(p, "BENF_UNIQUE_VALUE2",  obj.getBenfUniqueValue2());
            p = this.insert(p, "BENF_FLAG10",  obj.getBenfFlag10());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "OBDX_TRANSACTION_NAME",  obj.getObdxTransactionName());
            p = this.insert(p, "BENF_NICKNAME10",  obj.getBenfNickname10());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "OBDX_MODULE_NAME",  obj.getObdxModuleName());
            p = this.insert(p, "BENF_PAYEE_ID10",  obj.getBenfPayeeId10());
            p = this.insert(p, "BENF_STATUS",  obj.getBenfStatus());
            p = this.insert(p, "SESSION_ID",  obj.getSessionId());
            p = this.insert(p, "BENF_UNIQUE_VALUE10",  obj.getBenfUniqueValue10());
            p = this.insert(p, "BENF_TYPE10",  obj.getBenfType10());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "CUST_SEGMENT",  obj.getCustSegment());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "BENF_TYPE9",  obj.getBenfType9());
            p = this.insert(p, "BENF_TYPE8",  obj.getBenfType8());
            p = this.insert(p, "BENF_TYPE7",  obj.getBenfType7());
            p = this.insert(p, "BENF_TYPE6",  obj.getBenfType6());
            p = this.insert(p, "BENF_PAYEE_ID1",  obj.getBenfPayeeId1());
            p = this.insert(p, "BENF_TYPE5",  obj.getBenfType5());
            p = this.insert(p, "BENF_TYPE4",  obj.getBenfType4());
            p = this.insert(p, "BENF_TYPE3",  obj.getBenfType3());
            p = this.insert(p, "BENF_TYPE2",  obj.getBenfType2());
            p = this.insert(p, "BENF_TYPE1",  obj.getBenfType1());
            p = this.insert(p, "BENF_NICKNAME4",  obj.getBenfNickname4());
            p = this.insert(p, "BENF_NICKNAME5",  obj.getBenfNickname5());
            p = this.insert(p, "TWO_FA_STATUS",  obj.getTwoFaStatus());
            p = this.insert(p, "BENF_NICKNAME6",  obj.getBenfNickname6());
            p = this.insert(p, "BENF_NICKNAME7",  obj.getBenfNickname7());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "BENF_NICKNAME1",  obj.getBenfNickname1());
            p = this.insert(p, "BENF_NICKNAME2",  obj.getBenfNickname2());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "BENF_NICKNAME3",  obj.getBenfNickname3());
            p = this.insert(p, "BENF_PAYEE_ID3",  obj.getBenfPayeeId3());
            p = this.insert(p, "BENF_PAYEE_ID2",  obj.getBenfPayeeId2());
            p = this.insert(p, "BENF_PAYEE_ID5",  obj.getBenfPayeeId5());
            p = this.insert(p, "BENF_PAYEE_ID4",  obj.getBenfPayeeId4());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "BENF_PAYEE_ID7",  obj.getBenfPayeeId7());
            p = this.insert(p, "BENF_NICKNAME8",  obj.getBenfNickname8());
            p = this.insert(p, "BENF_PAYEE_ID6",  obj.getBenfPayeeId6());
            p = this.insert(p, "BENF_NICKNAME9",  obj.getBenfNickname9());
            p = this.insert(p, "BENF_PAYEE_ID9",  obj.getBenfPayeeId9());
            p = this.insert(p, "BENF_PAYEE_ID8",  obj.getBenfPayeeId8());
            p = this.insert(p, "RISK_BAND",  obj.getRiskBand());
            p = this.insert(p, "PAYEE_ACCTTYPE1",  obj.getPayeeAccttype1());
            p = this.insert(p, "BENF_ADDITION",  obj.getBenfAddition());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_BENEFICIARY"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_BENEFICIARY]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_BENEFICIARY]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_BeneficiaryEvent obj = new NFT_BeneficiaryEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTwoFaMode(rs.getString("TWO_FA_MODE"));
    obj.setBeneficiaryName(rs.getString("BENEFICIARY_NAME"));
    obj.setBenfFlag1(rs.getString("BENF_FLAG1"));
    obj.setBenfFlag5(rs.getString("BENF_FLAG5"));
    obj.setBenfFlag4(rs.getString("BENF_FLAG4"));
    obj.setBenfFlag3(rs.getString("BENF_FLAG3"));
    obj.setBenfFlag2(rs.getString("BENF_FLAG2"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setPayeeAccttype2(rs.getString("PAYEE_ACCTTYPE2"));
    obj.setBenfFlag9(rs.getString("BENF_FLAG9"));
    obj.setPayeeAccttype3(rs.getString("PAYEE_ACCTTYPE3"));
    obj.setBenfFlag8(rs.getString("BENF_FLAG8"));
    obj.setPayeeAccttype4(rs.getString("PAYEE_ACCTTYPE4"));
    obj.setBenfFlag7(rs.getString("BENF_FLAG7"));
    obj.setPayeeAccttype5(rs.getString("PAYEE_ACCTTYPE5"));
    obj.setBenfFlag6(rs.getString("BENF_FLAG6"));
    obj.setGroupPayeeId(rs.getString("GROUP_PAYEE_ID"));
    obj.setPayeeAccttype6(rs.getString("PAYEE_ACCTTYPE6"));
    obj.setBenfUniqueValue9(rs.getString("BENF_UNIQUE_VALUE9"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setPayeeAccttype7(rs.getString("PAYEE_ACCTTYPE7"));
    obj.setBenfUniqueValue7(rs.getString("BENF_UNIQUE_VALUE7"));
    obj.setPayeeAccttype8(rs.getString("PAYEE_ACCTTYPE8"));
    obj.setBenfUniqueValue8(rs.getString("BENF_UNIQUE_VALUE8"));
    obj.setPayeeAccttype9(rs.getString("PAYEE_ACCTTYPE9"));
    obj.setBenfUniqueValue5(rs.getString("BENF_UNIQUE_VALUE5"));
    obj.setBenfUniqueValue6(rs.getString("BENF_UNIQUE_VALUE6"));
    obj.setBenfUniqueValue3(rs.getString("BENF_UNIQUE_VALUE3"));
    obj.setBenfUniqueValue4(rs.getString("BENF_UNIQUE_VALUE4"));
    obj.setBenfUniqueValue1(rs.getString("BENF_UNIQUE_VALUE1"));
    obj.setPayeeAccttype10(rs.getString("PAYEE_ACCTTYPE10"));
    obj.setBenfUniqueValue2(rs.getString("BENF_UNIQUE_VALUE2"));
    obj.setBenfFlag10(rs.getString("BENF_FLAG10"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setObdxTransactionName(rs.getString("OBDX_TRANSACTION_NAME"));
    obj.setBenfNickname10(rs.getString("BENF_NICKNAME10"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setObdxModuleName(rs.getString("OBDX_MODULE_NAME"));
    obj.setBenfPayeeId10(rs.getString("BENF_PAYEE_ID10"));
    obj.setBenfStatus(rs.getString("BENF_STATUS"));
    obj.setSessionId(rs.getString("SESSION_ID"));
    obj.setBenfUniqueValue10(rs.getString("BENF_UNIQUE_VALUE10"));
    obj.setBenfType10(rs.getString("BENF_TYPE10"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setCustSegment(rs.getString("CUST_SEGMENT"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setBenfType9(rs.getString("BENF_TYPE9"));
    obj.setBenfType8(rs.getString("BENF_TYPE8"));
    obj.setBenfType7(rs.getString("BENF_TYPE7"));
    obj.setBenfType6(rs.getString("BENF_TYPE6"));
    obj.setBenfPayeeId1(rs.getString("BENF_PAYEE_ID1"));
    obj.setBenfType5(rs.getString("BENF_TYPE5"));
    obj.setBenfType4(rs.getString("BENF_TYPE4"));
    obj.setBenfType3(rs.getString("BENF_TYPE3"));
    obj.setBenfType2(rs.getString("BENF_TYPE2"));
    obj.setBenfType1(rs.getString("BENF_TYPE1"));
    obj.setBenfNickname4(rs.getString("BENF_NICKNAME4"));
    obj.setBenfNickname5(rs.getString("BENF_NICKNAME5"));
    obj.setTwoFaStatus(rs.getString("TWO_FA_STATUS"));
    obj.setBenfNickname6(rs.getString("BENF_NICKNAME6"));
    obj.setBenfNickname7(rs.getString("BENF_NICKNAME7"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setBenfNickname1(rs.getString("BENF_NICKNAME1"));
    obj.setBenfNickname2(rs.getString("BENF_NICKNAME2"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setBenfNickname3(rs.getString("BENF_NICKNAME3"));
    obj.setBenfPayeeId3(rs.getString("BENF_PAYEE_ID3"));
    obj.setBenfPayeeId2(rs.getString("BENF_PAYEE_ID2"));
    obj.setBenfPayeeId5(rs.getString("BENF_PAYEE_ID5"));
    obj.setBenfPayeeId4(rs.getString("BENF_PAYEE_ID4"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setBenfPayeeId7(rs.getString("BENF_PAYEE_ID7"));
    obj.setBenfNickname8(rs.getString("BENF_NICKNAME8"));
    obj.setBenfPayeeId6(rs.getString("BENF_PAYEE_ID6"));
    obj.setBenfNickname9(rs.getString("BENF_NICKNAME9"));
    obj.setBenfPayeeId9(rs.getString("BENF_PAYEE_ID9"));
    obj.setBenfPayeeId8(rs.getString("BENF_PAYEE_ID8"));
    obj.setRiskBand(rs.getString("RISK_BAND"));
    obj.setPayeeAccttype1(rs.getString("PAYEE_ACCTTYPE1"));
    obj.setBenfAddition(rs.getString("BENF_ADDITION"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_BENEFICIARY]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_BeneficiaryEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_BeneficiaryEvent> events;
 NFT_BeneficiaryEvent obj = new NFT_BeneficiaryEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_BENEFICIARY"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_BeneficiaryEvent obj = new NFT_BeneficiaryEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTwoFaMode(getColumnValue(rs, "TWO_FA_MODE"));
            obj.setBeneficiaryName(getColumnValue(rs, "BENEFICIARY_NAME"));
            obj.setBenfFlag1(getColumnValue(rs, "BENF_FLAG1"));
            obj.setBenfFlag5(getColumnValue(rs, "BENF_FLAG5"));
            obj.setBenfFlag4(getColumnValue(rs, "BENF_FLAG4"));
            obj.setBenfFlag3(getColumnValue(rs, "BENF_FLAG3"));
            obj.setBenfFlag2(getColumnValue(rs, "BENF_FLAG2"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setPayeeAccttype2(getColumnValue(rs, "PAYEE_ACCTTYPE2"));
            obj.setBenfFlag9(getColumnValue(rs, "BENF_FLAG9"));
            obj.setPayeeAccttype3(getColumnValue(rs, "PAYEE_ACCTTYPE3"));
            obj.setBenfFlag8(getColumnValue(rs, "BENF_FLAG8"));
            obj.setPayeeAccttype4(getColumnValue(rs, "PAYEE_ACCTTYPE4"));
            obj.setBenfFlag7(getColumnValue(rs, "BENF_FLAG7"));
            obj.setPayeeAccttype5(getColumnValue(rs, "PAYEE_ACCTTYPE5"));
            obj.setBenfFlag6(getColumnValue(rs, "BENF_FLAG6"));
            obj.setGroupPayeeId(getColumnValue(rs, "GROUP_PAYEE_ID"));
            obj.setPayeeAccttype6(getColumnValue(rs, "PAYEE_ACCTTYPE6"));
            obj.setBenfUniqueValue9(getColumnValue(rs, "BENF_UNIQUE_VALUE9"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setPayeeAccttype7(getColumnValue(rs, "PAYEE_ACCTTYPE7"));
            obj.setBenfUniqueValue7(getColumnValue(rs, "BENF_UNIQUE_VALUE7"));
            obj.setPayeeAccttype8(getColumnValue(rs, "PAYEE_ACCTTYPE8"));
            obj.setBenfUniqueValue8(getColumnValue(rs, "BENF_UNIQUE_VALUE8"));
            obj.setPayeeAccttype9(getColumnValue(rs, "PAYEE_ACCTTYPE9"));
            obj.setBenfUniqueValue5(getColumnValue(rs, "BENF_UNIQUE_VALUE5"));
            obj.setBenfUniqueValue6(getColumnValue(rs, "BENF_UNIQUE_VALUE6"));
            obj.setBenfUniqueValue3(getColumnValue(rs, "BENF_UNIQUE_VALUE3"));
            obj.setBenfUniqueValue4(getColumnValue(rs, "BENF_UNIQUE_VALUE4"));
            obj.setBenfUniqueValue1(getColumnValue(rs, "BENF_UNIQUE_VALUE1"));
            obj.setPayeeAccttype10(getColumnValue(rs, "PAYEE_ACCTTYPE10"));
            obj.setBenfUniqueValue2(getColumnValue(rs, "BENF_UNIQUE_VALUE2"));
            obj.setBenfFlag10(getColumnValue(rs, "BENF_FLAG10"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setObdxTransactionName(getColumnValue(rs, "OBDX_TRANSACTION_NAME"));
            obj.setBenfNickname10(getColumnValue(rs, "BENF_NICKNAME10"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setObdxModuleName(getColumnValue(rs, "OBDX_MODULE_NAME"));
            obj.setBenfPayeeId10(getColumnValue(rs, "BENF_PAYEE_ID10"));
            obj.setBenfStatus(getColumnValue(rs, "BENF_STATUS"));
            obj.setSessionId(getColumnValue(rs, "SESSION_ID"));
            obj.setBenfUniqueValue10(getColumnValue(rs, "BENF_UNIQUE_VALUE10"));
            obj.setBenfType10(getColumnValue(rs, "BENF_TYPE10"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setCustSegment(getColumnValue(rs, "CUST_SEGMENT"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setBenfType9(getColumnValue(rs, "BENF_TYPE9"));
            obj.setBenfType8(getColumnValue(rs, "BENF_TYPE8"));
            obj.setBenfType7(getColumnValue(rs, "BENF_TYPE7"));
            obj.setBenfType6(getColumnValue(rs, "BENF_TYPE6"));
            obj.setBenfPayeeId1(getColumnValue(rs, "BENF_PAYEE_ID1"));
            obj.setBenfType5(getColumnValue(rs, "BENF_TYPE5"));
            obj.setBenfType4(getColumnValue(rs, "BENF_TYPE4"));
            obj.setBenfType3(getColumnValue(rs, "BENF_TYPE3"));
            obj.setBenfType2(getColumnValue(rs, "BENF_TYPE2"));
            obj.setBenfType1(getColumnValue(rs, "BENF_TYPE1"));
            obj.setBenfNickname4(getColumnValue(rs, "BENF_NICKNAME4"));
            obj.setBenfNickname5(getColumnValue(rs, "BENF_NICKNAME5"));
            obj.setTwoFaStatus(getColumnValue(rs, "TWO_FA_STATUS"));
            obj.setBenfNickname6(getColumnValue(rs, "BENF_NICKNAME6"));
            obj.setBenfNickname7(getColumnValue(rs, "BENF_NICKNAME7"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setBenfNickname1(getColumnValue(rs, "BENF_NICKNAME1"));
            obj.setBenfNickname2(getColumnValue(rs, "BENF_NICKNAME2"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setBenfNickname3(getColumnValue(rs, "BENF_NICKNAME3"));
            obj.setBenfPayeeId3(getColumnValue(rs, "BENF_PAYEE_ID3"));
            obj.setBenfPayeeId2(getColumnValue(rs, "BENF_PAYEE_ID2"));
            obj.setBenfPayeeId5(getColumnValue(rs, "BENF_PAYEE_ID5"));
            obj.setBenfPayeeId4(getColumnValue(rs, "BENF_PAYEE_ID4"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setBenfPayeeId7(getColumnValue(rs, "BENF_PAYEE_ID7"));
            obj.setBenfNickname8(getColumnValue(rs, "BENF_NICKNAME8"));
            obj.setBenfPayeeId6(getColumnValue(rs, "BENF_PAYEE_ID6"));
            obj.setBenfNickname9(getColumnValue(rs, "BENF_NICKNAME9"));
            obj.setBenfPayeeId9(getColumnValue(rs, "BENF_PAYEE_ID9"));
            obj.setBenfPayeeId8(getColumnValue(rs, "BENF_PAYEE_ID8"));
            obj.setRiskBand(getColumnValue(rs, "RISK_BAND"));
            obj.setPayeeAccttype1(getColumnValue(rs, "PAYEE_ACCTTYPE1"));
            obj.setBenfAddition(getColumnValue(rs, "BENF_ADDITION"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_BENEFICIARY]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_BENEFICIARY]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TWO_FA_MODE\",\"BENEFICIARY_NAME\",\"BENF_FLAG1\",\"BENF_FLAG5\",\"BENF_FLAG4\",\"BENF_FLAG3\",\"BENF_FLAG2\",\"ERROR_DESC\",\"PAYEE_ACCTTYPE2\",\"BENF_FLAG9\",\"PAYEE_ACCTTYPE3\",\"BENF_FLAG8\",\"PAYEE_ACCTTYPE4\",\"BENF_FLAG7\",\"PAYEE_ACCTTYPE5\",\"BENF_FLAG6\",\"GROUP_PAYEE_ID\",\"PAYEE_ACCTTYPE6\",\"BENF_UNIQUE_VALUE9\",\"DEVICE_ID\",\"PAYEE_ACCTTYPE7\",\"BENF_UNIQUE_VALUE7\",\"PAYEE_ACCTTYPE8\",\"BENF_UNIQUE_VALUE8\",\"PAYEE_ACCTTYPE9\",\"BENF_UNIQUE_VALUE5\",\"BENF_UNIQUE_VALUE6\",\"BENF_UNIQUE_VALUE3\",\"BENF_UNIQUE_VALUE4\",\"BENF_UNIQUE_VALUE1\",\"PAYEE_ACCTTYPE10\",\"BENF_UNIQUE_VALUE2\",\"BENF_FLAG10\",\"ADDR_NETWORK\",\"HOST_ID\",\"OBDX_TRANSACTION_NAME\",\"BENF_NICKNAME10\",\"SYS_TIME\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"BENF_PAYEE_ID10\",\"BENF_STATUS\",\"SESSION_ID\",\"BENF_UNIQUE_VALUE10\",\"BENF_TYPE10\",\"SUCC_FAIL_FLG\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"BENF_TYPE9\",\"BENF_TYPE8\",\"BENF_TYPE7\",\"BENF_TYPE6\",\"BENF_PAYEE_ID1\",\"BENF_TYPE5\",\"BENF_TYPE4\",\"BENF_TYPE3\",\"BENF_TYPE2\",\"BENF_TYPE1\",\"BENF_NICKNAME4\",\"BENF_NICKNAME5\",\"TWO_FA_STATUS\",\"BENF_NICKNAME6\",\"BENF_NICKNAME7\",\"IP_CITY\",\"BENF_NICKNAME1\",\"BENF_NICKNAME2\",\"USER_ID\",\"BENF_NICKNAME3\",\"BENF_PAYEE_ID3\",\"BENF_PAYEE_ID2\",\"BENF_PAYEE_ID5\",\"BENF_PAYEE_ID4\",\"CUST_ID\",\"BENF_PAYEE_ID7\",\"BENF_NICKNAME8\",\"BENF_PAYEE_ID6\",\"BENF_NICKNAME9\",\"BENF_PAYEE_ID9\",\"BENF_PAYEE_ID8\",\"RISK_BAND\",\"PAYEE_ACCTTYPE1\",\"BENF_ADDITION\"" +
              " FROM EVENT_NFT_BENEFICIARY";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TWO_FA_MODE],[BENEFICIARY_NAME],[BENF_FLAG1],[BENF_FLAG5],[BENF_FLAG4],[BENF_FLAG3],[BENF_FLAG2],[ERROR_DESC],[PAYEE_ACCTTYPE2],[BENF_FLAG9],[PAYEE_ACCTTYPE3],[BENF_FLAG8],[PAYEE_ACCTTYPE4],[BENF_FLAG7],[PAYEE_ACCTTYPE5],[BENF_FLAG6],[GROUP_PAYEE_ID],[PAYEE_ACCTTYPE6],[BENF_UNIQUE_VALUE9],[DEVICE_ID],[PAYEE_ACCTTYPE7],[BENF_UNIQUE_VALUE7],[PAYEE_ACCTTYPE8],[BENF_UNIQUE_VALUE8],[PAYEE_ACCTTYPE9],[BENF_UNIQUE_VALUE5],[BENF_UNIQUE_VALUE6],[BENF_UNIQUE_VALUE3],[BENF_UNIQUE_VALUE4],[BENF_UNIQUE_VALUE1],[PAYEE_ACCTTYPE10],[BENF_UNIQUE_VALUE2],[BENF_FLAG10],[ADDR_NETWORK],[HOST_ID],[OBDX_TRANSACTION_NAME],[BENF_NICKNAME10],[SYS_TIME],[ERROR_CODE],[OBDX_MODULE_NAME],[BENF_PAYEE_ID10],[BENF_STATUS],[SESSION_ID],[BENF_UNIQUE_VALUE10],[BENF_TYPE10],[SUCC_FAIL_FLG],[CUST_SEGMENT],[IP_COUNTRY],[BENF_TYPE9],[BENF_TYPE8],[BENF_TYPE7],[BENF_TYPE6],[BENF_PAYEE_ID1],[BENF_TYPE5],[BENF_TYPE4],[BENF_TYPE3],[BENF_TYPE2],[BENF_TYPE1],[BENF_NICKNAME4],[BENF_NICKNAME5],[TWO_FA_STATUS],[BENF_NICKNAME6],[BENF_NICKNAME7],[IP_CITY],[BENF_NICKNAME1],[BENF_NICKNAME2],[USER_ID],[BENF_NICKNAME3],[BENF_PAYEE_ID3],[BENF_PAYEE_ID2],[BENF_PAYEE_ID5],[BENF_PAYEE_ID4],[CUST_ID],[BENF_PAYEE_ID7],[BENF_NICKNAME8],[BENF_PAYEE_ID6],[BENF_NICKNAME9],[BENF_PAYEE_ID9],[BENF_PAYEE_ID8],[RISK_BAND],[PAYEE_ACCTTYPE1],[BENF_ADDITION]" +
              " FROM EVENT_NFT_BENEFICIARY";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TWO_FA_MODE`,`BENEFICIARY_NAME`,`BENF_FLAG1`,`BENF_FLAG5`,`BENF_FLAG4`,`BENF_FLAG3`,`BENF_FLAG2`,`ERROR_DESC`,`PAYEE_ACCTTYPE2`,`BENF_FLAG9`,`PAYEE_ACCTTYPE3`,`BENF_FLAG8`,`PAYEE_ACCTTYPE4`,`BENF_FLAG7`,`PAYEE_ACCTTYPE5`,`BENF_FLAG6`,`GROUP_PAYEE_ID`,`PAYEE_ACCTTYPE6`,`BENF_UNIQUE_VALUE9`,`DEVICE_ID`,`PAYEE_ACCTTYPE7`,`BENF_UNIQUE_VALUE7`,`PAYEE_ACCTTYPE8`,`BENF_UNIQUE_VALUE8`,`PAYEE_ACCTTYPE9`,`BENF_UNIQUE_VALUE5`,`BENF_UNIQUE_VALUE6`,`BENF_UNIQUE_VALUE3`,`BENF_UNIQUE_VALUE4`,`BENF_UNIQUE_VALUE1`,`PAYEE_ACCTTYPE10`,`BENF_UNIQUE_VALUE2`,`BENF_FLAG10`,`ADDR_NETWORK`,`HOST_ID`,`OBDX_TRANSACTION_NAME`,`BENF_NICKNAME10`,`SYS_TIME`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`BENF_PAYEE_ID10`,`BENF_STATUS`,`SESSION_ID`,`BENF_UNIQUE_VALUE10`,`BENF_TYPE10`,`SUCC_FAIL_FLG`,`CUST_SEGMENT`,`IP_COUNTRY`,`BENF_TYPE9`,`BENF_TYPE8`,`BENF_TYPE7`,`BENF_TYPE6`,`BENF_PAYEE_ID1`,`BENF_TYPE5`,`BENF_TYPE4`,`BENF_TYPE3`,`BENF_TYPE2`,`BENF_TYPE1`,`BENF_NICKNAME4`,`BENF_NICKNAME5`,`TWO_FA_STATUS`,`BENF_NICKNAME6`,`BENF_NICKNAME7`,`IP_CITY`,`BENF_NICKNAME1`,`BENF_NICKNAME2`,`USER_ID`,`BENF_NICKNAME3`,`BENF_PAYEE_ID3`,`BENF_PAYEE_ID2`,`BENF_PAYEE_ID5`,`BENF_PAYEE_ID4`,`CUST_ID`,`BENF_PAYEE_ID7`,`BENF_NICKNAME8`,`BENF_PAYEE_ID6`,`BENF_NICKNAME9`,`BENF_PAYEE_ID9`,`BENF_PAYEE_ID8`,`RISK_BAND`,`PAYEE_ACCTTYPE1`,`BENF_ADDITION`" +
              " FROM EVENT_NFT_BENEFICIARY";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_BENEFICIARY (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TWO_FA_MODE\",\"BENEFICIARY_NAME\",\"BENF_FLAG1\",\"BENF_FLAG5\",\"BENF_FLAG4\",\"BENF_FLAG3\",\"BENF_FLAG2\",\"ERROR_DESC\",\"PAYEE_ACCTTYPE2\",\"BENF_FLAG9\",\"PAYEE_ACCTTYPE3\",\"BENF_FLAG8\",\"PAYEE_ACCTTYPE4\",\"BENF_FLAG7\",\"PAYEE_ACCTTYPE5\",\"BENF_FLAG6\",\"GROUP_PAYEE_ID\",\"PAYEE_ACCTTYPE6\",\"BENF_UNIQUE_VALUE9\",\"DEVICE_ID\",\"PAYEE_ACCTTYPE7\",\"BENF_UNIQUE_VALUE7\",\"PAYEE_ACCTTYPE8\",\"BENF_UNIQUE_VALUE8\",\"PAYEE_ACCTTYPE9\",\"BENF_UNIQUE_VALUE5\",\"BENF_UNIQUE_VALUE6\",\"BENF_UNIQUE_VALUE3\",\"BENF_UNIQUE_VALUE4\",\"BENF_UNIQUE_VALUE1\",\"PAYEE_ACCTTYPE10\",\"BENF_UNIQUE_VALUE2\",\"BENF_FLAG10\",\"ADDR_NETWORK\",\"HOST_ID\",\"OBDX_TRANSACTION_NAME\",\"BENF_NICKNAME10\",\"SYS_TIME\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"BENF_PAYEE_ID10\",\"BENF_STATUS\",\"SESSION_ID\",\"BENF_UNIQUE_VALUE10\",\"BENF_TYPE10\",\"SUCC_FAIL_FLG\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"BENF_TYPE9\",\"BENF_TYPE8\",\"BENF_TYPE7\",\"BENF_TYPE6\",\"BENF_PAYEE_ID1\",\"BENF_TYPE5\",\"BENF_TYPE4\",\"BENF_TYPE3\",\"BENF_TYPE2\",\"BENF_TYPE1\",\"BENF_NICKNAME4\",\"BENF_NICKNAME5\",\"TWO_FA_STATUS\",\"BENF_NICKNAME6\",\"BENF_NICKNAME7\",\"IP_CITY\",\"BENF_NICKNAME1\",\"BENF_NICKNAME2\",\"USER_ID\",\"BENF_NICKNAME3\",\"BENF_PAYEE_ID3\",\"BENF_PAYEE_ID2\",\"BENF_PAYEE_ID5\",\"BENF_PAYEE_ID4\",\"CUST_ID\",\"BENF_PAYEE_ID7\",\"BENF_NICKNAME8\",\"BENF_PAYEE_ID6\",\"BENF_NICKNAME9\",\"BENF_PAYEE_ID9\",\"BENF_PAYEE_ID8\",\"RISK_BAND\",\"PAYEE_ACCTTYPE1\",\"BENF_ADDITION\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TWO_FA_MODE],[BENEFICIARY_NAME],[BENF_FLAG1],[BENF_FLAG5],[BENF_FLAG4],[BENF_FLAG3],[BENF_FLAG2],[ERROR_DESC],[PAYEE_ACCTTYPE2],[BENF_FLAG9],[PAYEE_ACCTTYPE3],[BENF_FLAG8],[PAYEE_ACCTTYPE4],[BENF_FLAG7],[PAYEE_ACCTTYPE5],[BENF_FLAG6],[GROUP_PAYEE_ID],[PAYEE_ACCTTYPE6],[BENF_UNIQUE_VALUE9],[DEVICE_ID],[PAYEE_ACCTTYPE7],[BENF_UNIQUE_VALUE7],[PAYEE_ACCTTYPE8],[BENF_UNIQUE_VALUE8],[PAYEE_ACCTTYPE9],[BENF_UNIQUE_VALUE5],[BENF_UNIQUE_VALUE6],[BENF_UNIQUE_VALUE3],[BENF_UNIQUE_VALUE4],[BENF_UNIQUE_VALUE1],[PAYEE_ACCTTYPE10],[BENF_UNIQUE_VALUE2],[BENF_FLAG10],[ADDR_NETWORK],[HOST_ID],[OBDX_TRANSACTION_NAME],[BENF_NICKNAME10],[SYS_TIME],[ERROR_CODE],[OBDX_MODULE_NAME],[BENF_PAYEE_ID10],[BENF_STATUS],[SESSION_ID],[BENF_UNIQUE_VALUE10],[BENF_TYPE10],[SUCC_FAIL_FLG],[CUST_SEGMENT],[IP_COUNTRY],[BENF_TYPE9],[BENF_TYPE8],[BENF_TYPE7],[BENF_TYPE6],[BENF_PAYEE_ID1],[BENF_TYPE5],[BENF_TYPE4],[BENF_TYPE3],[BENF_TYPE2],[BENF_TYPE1],[BENF_NICKNAME4],[BENF_NICKNAME5],[TWO_FA_STATUS],[BENF_NICKNAME6],[BENF_NICKNAME7],[IP_CITY],[BENF_NICKNAME1],[BENF_NICKNAME2],[USER_ID],[BENF_NICKNAME3],[BENF_PAYEE_ID3],[BENF_PAYEE_ID2],[BENF_PAYEE_ID5],[BENF_PAYEE_ID4],[CUST_ID],[BENF_PAYEE_ID7],[BENF_NICKNAME8],[BENF_PAYEE_ID6],[BENF_NICKNAME9],[BENF_PAYEE_ID9],[BENF_PAYEE_ID8],[RISK_BAND],[PAYEE_ACCTTYPE1],[BENF_ADDITION]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TWO_FA_MODE`,`BENEFICIARY_NAME`,`BENF_FLAG1`,`BENF_FLAG5`,`BENF_FLAG4`,`BENF_FLAG3`,`BENF_FLAG2`,`ERROR_DESC`,`PAYEE_ACCTTYPE2`,`BENF_FLAG9`,`PAYEE_ACCTTYPE3`,`BENF_FLAG8`,`PAYEE_ACCTTYPE4`,`BENF_FLAG7`,`PAYEE_ACCTTYPE5`,`BENF_FLAG6`,`GROUP_PAYEE_ID`,`PAYEE_ACCTTYPE6`,`BENF_UNIQUE_VALUE9`,`DEVICE_ID`,`PAYEE_ACCTTYPE7`,`BENF_UNIQUE_VALUE7`,`PAYEE_ACCTTYPE8`,`BENF_UNIQUE_VALUE8`,`PAYEE_ACCTTYPE9`,`BENF_UNIQUE_VALUE5`,`BENF_UNIQUE_VALUE6`,`BENF_UNIQUE_VALUE3`,`BENF_UNIQUE_VALUE4`,`BENF_UNIQUE_VALUE1`,`PAYEE_ACCTTYPE10`,`BENF_UNIQUE_VALUE2`,`BENF_FLAG10`,`ADDR_NETWORK`,`HOST_ID`,`OBDX_TRANSACTION_NAME`,`BENF_NICKNAME10`,`SYS_TIME`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`BENF_PAYEE_ID10`,`BENF_STATUS`,`SESSION_ID`,`BENF_UNIQUE_VALUE10`,`BENF_TYPE10`,`SUCC_FAIL_FLG`,`CUST_SEGMENT`,`IP_COUNTRY`,`BENF_TYPE9`,`BENF_TYPE8`,`BENF_TYPE7`,`BENF_TYPE6`,`BENF_PAYEE_ID1`,`BENF_TYPE5`,`BENF_TYPE4`,`BENF_TYPE3`,`BENF_TYPE2`,`BENF_TYPE1`,`BENF_NICKNAME4`,`BENF_NICKNAME5`,`TWO_FA_STATUS`,`BENF_NICKNAME6`,`BENF_NICKNAME7`,`IP_CITY`,`BENF_NICKNAME1`,`BENF_NICKNAME2`,`USER_ID`,`BENF_NICKNAME3`,`BENF_PAYEE_ID3`,`BENF_PAYEE_ID2`,`BENF_PAYEE_ID5`,`BENF_PAYEE_ID4`,`CUST_ID`,`BENF_PAYEE_ID7`,`BENF_NICKNAME8`,`BENF_PAYEE_ID6`,`BENF_NICKNAME9`,`BENF_PAYEE_ID9`,`BENF_PAYEE_ID8`,`RISK_BAND`,`PAYEE_ACCTTYPE1`,`BENF_ADDITION`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

