package clari5.custom.yes.integration.builder;

import clari5.custom.yes.integration.builder.data.Message;
import clari5.custom.yes.integration.config.BepCon;
import clari5.custom.yes.integration.data.ITableData;
import clari5.custom.yes.integration.data.bootstrap.TableMap;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class EventBuilder {
    public static CxpsLogger logger = CxpsLogger.getLogger(EventBuilder.class);
    protected static TableMap tblJsonMappingConfig;
    private static HashMap<String, String> tableToClassMap;


    static {
        try {
            tblJsonMappingConfig = TableMap.getTableMap();
            if (tblJsonMappingConfig == null)
                logger.info(Thread.currentThread().getName()+ "[EventBuilder] ERROR : tblJsonMappingConfig is null.");
            tableToClassMap = BepCon.getConfig().getTableClassMap();
        } catch (Exception e) {
            logger.info(Thread.currentThread().getName()+ "[EventBuilder] Error while loading tbl-json-mapping config file");
        }
    }


    public boolean process(ITableData row) throws Exception {
        Long time1=System.currentTimeMillis();
        JSONObject jrow = new JSONObject(row);
        String tableName = jrow.getString("tableName");
        MsgMetaData msgmetas = tblJsonMappingConfig.get(tableName.toUpperCase());
        if (msgmetas == null) {
            logger.error(Thread.currentThread().getName()+"[EventBuilder] Could not load table to json mapping properties for " + tableName);
            throw new Exception(Thread.currentThread().getName()+ "[EventBuilder] Could not load table to json mapping properties for " + tableName);
        }

        Map.Entry<String, Message> msgmeta = msgmetas.entrySet().iterator().next();
        if (msgmeta != null) {
            //logger.info("[EventBuilder.process] msgmeta event name and it's fields " + msgmeta.toString());
        }
        String eventKey = msgmeta.getKey();
        String eventName = tableToClassMap.get(eventKey);
        eventName = eventName.toLowerCase();

        Map<String, String> msg = msgmeta.getValue().process(jrow);
        JSONObject original = new JSONObject(msg);

        long event_ts = System.currentTimeMillis();
        String event_id = eventName + System.currentTimeMillis();
        JSONObject finalEvent = processJson(original, eventName, event_id);
        logger.info(Thread.currentThread().getName()+"[EventBuilder] Final Event is generated is " + finalEvent);

      /*  try {
            File fileName = new File(System.getenv("CXPS_DEPLOYMENT") + "/batch-files");
            if (!fileName.exists()) fileName.mkdirs();
            //logger.info("[EventBuilder.process] message file path " + fileName.getAbsolutePath());
            Files.write(Paths.get(fileName.getAbsolutePath() + "/" + event_id + ".msg"), finalEvent.toString().getBytes());
            logger.info(Thread.currentThread().getName()+ "[EventBuilder] Message for event id" + event_id + "is written successfully in path " + fileName);
        } catch (Exception e) {
            logger.info("[EventBuilder] Debug path for message is not configured please configure else event log file will not be written");
        }*/
        Long time2=System.currentTimeMillis();
        logger.info(Thread.currentThread().getName()+"[EventBuilder] Forming JSON "+(time2-time1)+ "ms");

        boolean status = Clari5GatewayManager.send(event_id, finalEvent, event_ts);
        return status;
    }

    private JSONObject processJson(JSONObject msg, String eventName, String event_id) throws Exception {
        JSONObject j = new JSONObject();
        j.put("event-name", eventName);

        String[] eventdetails = eventName.split("\\_");
        j.put("EventType", eventdetails[0]);
        j.put("EventSubType", eventdetails[1]);
        j.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));
        return j;
    }


}
