clari5.custom.mapper {
        entity {
                DEDUPE_RETRY {
                       generate = true
                        attributes:[
                                { name: SERIAL_NO, type="number:10", key=true}
                                { name: REQUEST_JSON ,type ="string:300"},
                                { name: STATUS ,type ="string:5"},
                                { name: RETRY_COUNT ,type = "number:10"},
                                { name: CUST_ID ,type = "string:50"},
                                { name: rule_name,type = "String:300"}
                                ]
                        }
        }
}
