clari5.custom.yes.db {
        entity {
                CUSTOM_CL5_UPLOAD_AUDIT {
                       generate = true
                        attributes:[
                                { name: CREATED_TIME, type=timestamp , key=true },
                                { name: UPDATED_TIME ,type =timestamp  }
                                { name: TABLENAME ,type ="string:2000" }
				{ name: USERNAME ,type ="string:2000" }
                                { name: FILENAME ,type ="string:2000" }
				{ name: ACTIVITY ,type ="string:2000" }
                               ]
                        }
        }
}
