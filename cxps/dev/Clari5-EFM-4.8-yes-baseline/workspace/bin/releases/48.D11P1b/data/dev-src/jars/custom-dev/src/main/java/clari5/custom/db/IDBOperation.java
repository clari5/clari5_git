package clari5.custom.db;


/**
 * @author shishir
 * @since 11/01/2018
 */

public interface IDBOperation {

    /**
     *
     * @return all records from table
     */
    public Object getAllRecords();

    /**
     *
     * @param o primary key column value
     * @return records based on primary key
     */
    public Object select(Object o);

    /**
     *
     * @param o primary key column value
     * @return no of row inserted
     */
    public int insert(Object o);

    /**
     *
     * @param o primary key column value
     * @return no of row updated
     */
    public int update(Object o);

    /**
     *
     * @param o primary key column value
     * @return no of row deleted
     */
    public int delete(Object o);
}
