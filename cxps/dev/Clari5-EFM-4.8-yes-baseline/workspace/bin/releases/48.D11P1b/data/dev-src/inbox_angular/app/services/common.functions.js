"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonFunctions = (function () {
    function CommonFunctions() {
    }
    CommonFunctions.prototype.logDebugMsg = function (str, obj1, obj2) {
        console.log(str, obj1, obj2);
    };
    CommonFunctions.prototype.alertDebugMsg = function (str) {
        alert(str);
    };
    return CommonFunctions;
}());
exports.CommonFunctions = CommonFunctions;
//# sourceMappingURL=common.functions.js.map