// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_ChequeBookRequestEventMapper extends EventMapper<NFT_ChequeBookRequestEvent> {

public NFT_ChequeBookRequestEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_ChequeBookRequestEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_ChequeBookRequestEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_ChequeBookRequestEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_ChequeBookRequestEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_ChequeBookRequestEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_ChequeBookRequestEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setInt(i++, obj.getNoOfLeavIsud());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getSystem());
            preparedStatement.setTimestamp(i++, obj.getIssueDate());
            preparedStatement.setString(i++, obj.getEndcheqnumber());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustomerId());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getBegincheqnumber());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_CHEQUEBOOKREQUEST]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_ChequeBookRequestEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_ChequeBookRequestEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CHEQUEBOOKREQUEST"));
        putList = new ArrayList<>();

        for (NFT_ChequeBookRequestEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "NO_OF_LEAV_ISUD", String.valueOf(obj.getNoOfLeavIsud()));
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "SYSTEM",  obj.getSystem());
            p = this.insert(p, "ISSUE_DATE", String.valueOf(obj.getIssueDate()));
            p = this.insert(p, "ENDCHEQNUMBER",  obj.getEndcheqnumber());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUSTOMER_ID",  obj.getCustomerId());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "BEGINCHEQNUMBER",  obj.getBegincheqnumber());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_CHEQUEBOOKREQUEST"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_CHEQUEBOOKREQUEST]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_CHEQUEBOOKREQUEST]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_ChequeBookRequestEvent obj = new NFT_ChequeBookRequestEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setNoOfLeavIsud(rs.getInt("NO_OF_LEAV_ISUD"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setSystem(rs.getString("SYSTEM"));
    obj.setIssueDate(rs.getTimestamp("ISSUE_DATE"));
    obj.setEndcheqnumber(rs.getString("ENDCHEQNUMBER"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustomerId(rs.getString("CUSTOMER_ID"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setBegincheqnumber(rs.getString("BEGINCHEQNUMBER"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_CHEQUEBOOKREQUEST]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_ChequeBookRequestEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_ChequeBookRequestEvent> events;
 NFT_ChequeBookRequestEvent obj = new NFT_ChequeBookRequestEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CHEQUEBOOKREQUEST"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_ChequeBookRequestEvent obj = new NFT_ChequeBookRequestEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setNoOfLeavIsud(EventHelper.toInt(getColumnValue(rs, "NO_OF_LEAV_ISUD")));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setSystem(getColumnValue(rs, "SYSTEM"));
            obj.setIssueDate(EventHelper.toTimestamp(getColumnValue(rs, "ISSUE_DATE")));
            obj.setEndcheqnumber(getColumnValue(rs, "ENDCHEQNUMBER"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustomerId(getColumnValue(rs, "CUSTOMER_ID"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setBegincheqnumber(getColumnValue(rs, "BEGINCHEQNUMBER"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CHEQUEBOOKREQUEST]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CHEQUEBOOKREQUEST]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"NO_OF_LEAV_ISUD\",\"ACCOUNT_ID\",\"AVL_BAL\",\"SYSTEM\",\"ISSUE_DATE\",\"ENDCHEQNUMBER\",\"SYS_TIME\",\"CUSTOMER_ID\",\"EVENTTS\",\"HOST_ID\",\"BEGINCHEQNUMBER\"" +
              " FROM EVENT_NFT_CHEQUEBOOKREQUEST";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [NO_OF_LEAV_ISUD],[ACCOUNT_ID],[AVL_BAL],[SYSTEM],[ISSUE_DATE],[ENDCHEQNUMBER],[SYS_TIME],[CUSTOMER_ID],[EVENTTS],[HOST_ID],[BEGINCHEQNUMBER]" +
              " FROM EVENT_NFT_CHEQUEBOOKREQUEST";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`NO_OF_LEAV_ISUD`,`ACCOUNT_ID`,`AVL_BAL`,`SYSTEM`,`ISSUE_DATE`,`ENDCHEQNUMBER`,`SYS_TIME`,`CUSTOMER_ID`,`EVENTTS`,`HOST_ID`,`BEGINCHEQNUMBER`" +
              " FROM EVENT_NFT_CHEQUEBOOKREQUEST";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_CHEQUEBOOKREQUEST (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"NO_OF_LEAV_ISUD\",\"ACCOUNT_ID\",\"AVL_BAL\",\"SYSTEM\",\"ISSUE_DATE\",\"ENDCHEQNUMBER\",\"SYS_TIME\",\"CUSTOMER_ID\",\"EVENTTS\",\"HOST_ID\",\"BEGINCHEQNUMBER\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[NO_OF_LEAV_ISUD],[ACCOUNT_ID],[AVL_BAL],[SYSTEM],[ISSUE_DATE],[ENDCHEQNUMBER],[SYS_TIME],[CUSTOMER_ID],[EVENTTS],[HOST_ID],[BEGINCHEQNUMBER]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`NO_OF_LEAV_ISUD`,`ACCOUNT_ID`,`AVL_BAL`,`SYSTEM`,`ISSUE_DATE`,`ENDCHEQNUMBER`,`SYS_TIME`,`CUSTOMER_ID`,`EVENTTS`,`HOST_ID`,`BEGINCHEQNUMBER`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

