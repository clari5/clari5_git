// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_EIPOAPPLICATION", Schema="rice")
public class FT_EipoapplicationEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=20) public Double bid1Amount;
       @Field(size=20) public Double bid2Amount;
       @Field(size=200) public String errorDesc;
       @Field(size=10) public String succFailFlg;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String ipCountry;
       @Field(size=200) public String deviceId;
       @Field(size=200) public String ipoType;
       @Field(size=20) public Double bid7Amount;
       @Field(size=20) public Double bid8Amount;
       @Field(size=20) public Double totalApplicationAmount;
       @Field(size=20) public Double bid5Amount;
       @Field(size=20) public Double bid6Amount;
       @Field(size=20) public Double bid9Amount;
       @Field(size=200) public String addrNetwork;
       @Field(size=20) public Double bid4Amount;
       @Field(size=20) public Double bid3Amount;
       @Field(size=2) public String hostId;
       @Field(size=20) public Double bid13Amount;
       @Field(size=20) public String twoFaStatus;
       @Field(size=20) public Double bid12Amount;
       @Field(size=20) public Double bid14Amount;
       @Field(size=200) public String ipCity;
       @Field(size=200) public String applicantPan;
       @Field(size=200) public String obdxTransactionName;
       @Field(size=200) public String userId;
       @Field(size=20) public Double bid16Amount;
       @Field(size=20) public Double bid17Amount;
       @Field(size=200) public String ipoId;
       @Field(size=20) public Double bid10Amount;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String obdxModuleName;
       @Field(size=20) public Double bid11Amount;
       @Field(size=200) public String casaAcNo;
       @Field(size=200) public String applicantType;
       @Field(size=20) public Double bid15Amount;
       @Field(size=20) public Double bid18Amount;
       @Field(size=20) public Double accountBalance;
       @Field(size=200) public String riskBand;
       @Field(size=200) public String applicantName;
       @Field(size=200) public String applicantUniqueIdentifier;
       @Field(size=200) public String sessionId;


    @JsonIgnore
    public ITable<FT_EipoapplicationEvent> t = AEF.getITable(this);

	public FT_EipoapplicationEvent(){}

    public FT_EipoapplicationEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Eipoapplication");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setBid1Amount(EventHelper.toDouble(json.getString("bid1_amount")));
            setBid2Amount(EventHelper.toDouble(json.getString("bid2_amount")));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setCustSegment(json.getString("cust_segment"));
            setIpCountry(json.getString("ip_country"));
            setDeviceId(json.getString("device_id"));
            setIpoType(json.getString("ipo_type"));
            setBid7Amount(EventHelper.toDouble(json.getString("bid7_amount")));
            setBid8Amount(EventHelper.toDouble(json.getString("bid8_amount")));
            setTotalApplicationAmount(EventHelper.toDouble(json.getString("total_application_amount")));
            setBid5Amount(EventHelper.toDouble(json.getString("bid5_amount")));
            setBid6Amount(EventHelper.toDouble(json.getString("bid6_amount")));
            setBid9Amount(EventHelper.toDouble(json.getString("bid9_amount")));
            setAddrNetwork(json.getString("addr_network"));
            setBid4Amount(EventHelper.toDouble(json.getString("bid4_amount")));
            setBid3Amount(EventHelper.toDouble(json.getString("bid3_amount")));
            setHostId(json.getString("host_id"));
            setBid13Amount(EventHelper.toDouble(json.getString("bid13_amount")));
            setTwoFaStatus(json.getString("2fa_status"));
            setBid12Amount(EventHelper.toDouble(json.getString("bid12_amount")));
            setBid14Amount(EventHelper.toDouble(json.getString("bid14_amount")));
            setIpCity(json.getString("ip_city"));
            setApplicantPan(json.getString("applicant_pan"));
            setObdxTransactionName(json.getString("obdx_transaction_name"));
            setUserId(json.getString("user_id"));
            setBid16Amount(EventHelper.toDouble(json.getString("bid16_amount")));
            setBid17Amount(EventHelper.toDouble(json.getString("bid17_amount")));
            setIpoId(json.getString("ipo_id"));
            setBid10Amount(EventHelper.toDouble(json.getString("bid10_amount")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setObdxModuleName(json.getString("obdx_module_name"));
            setBid11Amount(EventHelper.toDouble(json.getString("bid11_amount")));
            setCasaAcNo(json.getString("casa_ac_no"));
            setApplicantType(json.getString("applicant_type"));
            setBid15Amount(EventHelper.toDouble(json.getString("bid15_amount")));
            setBid18Amount(EventHelper.toDouble(json.getString("bid18_amount")));
            setAccountBalance(EventHelper.toDouble(json.getString("account_balance")));
            setApplicantName(json.getString("applicant_name"));
            setApplicantUniqueIdentifier(json.getString("applicant_unique_identifier"));
            setSessionId(json.getString("session_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setRiskBand(cxps.events.CustomFieldDerivator.checkInstanceofEvent(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NEIP"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public Double getBid1Amount(){ return bid1Amount; }

    public Double getBid2Amount(){ return bid2Amount; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getCustSegment(){ return custSegment; }

    public String getIpCountry(){ return ipCountry; }

    public String getDeviceId(){ return deviceId; }

    public String getIpoType(){ return ipoType; }

    public Double getBid7Amount(){ return bid7Amount; }

    public Double getBid8Amount(){ return bid8Amount; }

    public Double getTotalApplicationAmount(){ return totalApplicationAmount; }

    public Double getBid5Amount(){ return bid5Amount; }

    public Double getBid6Amount(){ return bid6Amount; }

    public Double getBid9Amount(){ return bid9Amount; }

    public String getAddrNetwork(){ return addrNetwork; }

    public Double getBid4Amount(){ return bid4Amount; }

    public Double getBid3Amount(){ return bid3Amount; }

    public String getHostId(){ return hostId; }

    public Double getBid13Amount(){ return bid13Amount; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public Double getBid12Amount(){ return bid12Amount; }

    public Double getBid14Amount(){ return bid14Amount; }

    public String getIpCity(){ return ipCity; }

    public String getApplicantPan(){ return applicantPan; }

    public String getObdxTransactionName(){ return obdxTransactionName; }

    public String getUserId(){ return userId; }

    public Double getBid16Amount(){ return bid16Amount; }

    public Double getBid17Amount(){ return bid17Amount; }

    public String getIpoId(){ return ipoId; }

    public Double getBid10Amount(){ return bid10Amount; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getObdxModuleName(){ return obdxModuleName; }

    public Double getBid11Amount(){ return bid11Amount; }

    public String getCasaAcNo(){ return casaAcNo; }

    public String getApplicantType(){ return applicantType; }

    public Double getBid15Amount(){ return bid15Amount; }

    public Double getBid18Amount(){ return bid18Amount; }

    public Double getAccountBalance(){ return accountBalance; }

    public String getApplicantName(){ return applicantName; }

    public String getApplicantUniqueIdentifier(){ return applicantUniqueIdentifier; }

    public String getSessionId(){ return sessionId; }
    public String getRiskBand(){ return riskBand; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setBid1Amount(Double val){ this.bid1Amount = val; }
    public void setBid2Amount(Double val){ this.bid2Amount = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setIpoType(String val){ this.ipoType = val; }
    public void setBid7Amount(Double val){ this.bid7Amount = val; }
    public void setBid8Amount(Double val){ this.bid8Amount = val; }
    public void setTotalApplicationAmount(Double val){ this.totalApplicationAmount = val; }
    public void setBid5Amount(Double val){ this.bid5Amount = val; }
    public void setBid6Amount(Double val){ this.bid6Amount = val; }
    public void setBid9Amount(Double val){ this.bid9Amount = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setBid4Amount(Double val){ this.bid4Amount = val; }
    public void setBid3Amount(Double val){ this.bid3Amount = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setBid13Amount(Double val){ this.bid13Amount = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setBid12Amount(Double val){ this.bid12Amount = val; }
    public void setBid14Amount(Double val){ this.bid14Amount = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setApplicantPan(String val){ this.applicantPan = val; }
    public void setObdxTransactionName(String val){ this.obdxTransactionName = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setBid16Amount(Double val){ this.bid16Amount = val; }
    public void setBid17Amount(Double val){ this.bid17Amount = val; }
    public void setIpoId(String val){ this.ipoId = val; }
    public void setBid10Amount(Double val){ this.bid10Amount = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setObdxModuleName(String val){ this.obdxModuleName = val; }
    public void setBid11Amount(Double val){ this.bid11Amount = val; }
    public void setCasaAcNo(String val){ this.casaAcNo = val; }
    public void setApplicantType(String val){ this.applicantType = val; }
    public void setBid15Amount(Double val){ this.bid15Amount = val; }
    public void setBid18Amount(Double val){ this.bid18Amount = val; }
    public void setAccountBalance(Double val){ this.accountBalance = val; }
    public void setApplicantName(String val){ this.applicantName = val; }
    public void setApplicantUniqueIdentifier(String val){ this.applicantUniqueIdentifier = val; }
    public void setSessionId(String val){ this.sessionId = val; }
    public void setRiskBand(String val){ this.riskBand = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_EipoapplicationEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.casaAcNo);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Eipoapplication");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Eipoapplication");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}