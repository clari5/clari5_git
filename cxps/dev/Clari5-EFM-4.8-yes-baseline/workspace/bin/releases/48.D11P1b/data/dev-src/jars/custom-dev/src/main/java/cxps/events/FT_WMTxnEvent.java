// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_WMTXN", Schema="rice")
public class FT_WMTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String acctId;
       @Field(size=11) public Double amount;
       @Field(size=200) public String subTranType;
       @Field(size=200) public String txnRefNo;
       @Field(size=200) public String wmsRefNo;
       @Field(size=200) public String schemeName;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String tranCode;
       @Field public java.sql.Timestamp eventts;
       @Field(size=200) public String rmCode;
       @Field(size=200) public String tradeDate;
       @Field(size=200) public String tranType;
       @Field(size=200) public String schemeType;
       @Field(size=200) public String hostId;


    @JsonIgnore
    public ITable<FT_WMTxnEvent> t = AEF.getITable(this);

	public FT_WMTxnEvent(){}

    public FT_WMTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("WMTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctId(json.getString("AcctId"));
            setAmount(EventHelper.toDouble(json.getString("Amount")));
            setSubTranType(json.getString("Sub_Tran_Type"));
            setTxnRefNo(json.getString("TXN_Ref_No"));
            setWmsRefNo(json.getString("WMS_Ref_No"));
            setSchemeName(json.getString("Scheme_Name"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("Cust_Id"));
            setTranCode(json.getString("Tran_Code"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setRmCode(json.getString("RM_Code"));
            setTradeDate(json.getString("Trade_Date"));
            setTranType(json.getString("Tran_Type"));
            setSchemeType(json.getString("Scheme_Type"));
            setHostId(json.getString("host_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FW"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAcctId(){ return acctId; }

    public Double getAmount(){ return amount; }

    public String getSubTranType(){ return subTranType; }

    public String getTxnRefNo(){ return txnRefNo; }

    public String getWmsRefNo(){ return wmsRefNo; }

    public String getSchemeName(){ return schemeName; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getTranCode(){ return tranCode; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getRmCode(){ return rmCode; }

    public String getTradeDate(){ return tradeDate; }

    public String getTranType(){ return tranType; }

    public String getSchemeType(){ return schemeType; }

    public String getHostId(){ return hostId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setAmount(Double val){ this.amount = val; }
    public void setSubTranType(String val){ this.subTranType = val; }
    public void setTxnRefNo(String val){ this.txnRefNo = val; }
    public void setWmsRefNo(String val){ this.wmsRefNo = val; }
    public void setSchemeName(String val){ this.schemeName = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setTranCode(String val){ this.tranCode = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setRmCode(String val){ this.rmCode = val; }
    public void setTradeDate(String val){ this.tradeDate = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setSchemeType(String val){ this.schemeType = val; }
    public void setHostId(String val){ this.hostId = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getSub_Tran_Type(){ return subTranType; }
    @JsonIgnore
    public String getTXN_Ref_No(){ return txnRefNo; }
    @JsonIgnore
    public String getWMS_Ref_No(){ return wmsRefNo; }
    @JsonIgnore
    public String getScheme_Name(){ return schemeName; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getCust_Id(){ return custId; }
    @JsonIgnore
    public String getTran_Code(){ return tranCode; }
    @JsonIgnore
    public String getRM_Code(){ return rmCode; }
    @JsonIgnore
    public String getTrade_Date(){ return tradeDate; }
    @JsonIgnore
    public String getTran_Type(){ return tranType; }
    @JsonIgnore
    public String getScheme_Type(){ return schemeType; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_WMTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_WMTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "WMTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}