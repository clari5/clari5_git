clari5.custom.yes.db {

       entity {
                cluster-assignment {
		generate = false
                     attributes = [
				    { name= cluster-id,  	type="string:50", 	key=true  }
				    { name= branch-id, 		type="string:50" ,	key=false }
				    { name= group-id, 		type="string:50" ,	key=false }
				    { name= level-id, 		type="string:50" ,	key=false }
				    { name= segment-id, 	type="string:50" ,	key=false }				   
				    { name= cluster-level, 	type="number:10" ,	key=false }

			]
			unique-indexes {
                           clustr-assigmnt-idx : [ branch-id, group-id, level-id,segment-id, cluster-level ]
                }

		}
	}
}
