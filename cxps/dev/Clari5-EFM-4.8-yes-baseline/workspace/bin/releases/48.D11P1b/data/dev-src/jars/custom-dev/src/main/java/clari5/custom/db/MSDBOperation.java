package clari5.custom.db;


import clari5.custom.yes.db.InboxMsSummary;
import cxps.apex.utils.CxpsLogger;

import java.util.List;

/**
 * @author shishir
 * @since 11/01/2018
 * <p>
 * DB operations for Mail services
 */
public class MSDBOperation {
    private static CxpsLogger logger = CxpsLogger.getLogger(MSDBOperation.class);
    private final InboxMsTable inboxMs;
    private final InboxEmailTable inboxEmail;

    public MSDBOperation() {
        inboxMs = new InboxMsTable();
        inboxEmail = new InboxEmailTable();
    }

    public Object getAllInboxMsRecords() {
        return inboxMs.getAllRecords();
    }

    /**
     * @return return pending record from inbox_ms table
     */
    public List<InboxMsSummary> getPendingMailRecord() {
        return inboxMs.getPendingMailRecord();
    }

    /**
     * @param branchId
     * @return return email id from inbox_email table based on branchId
     */
    public String getEmailId(String branchId) {
        return inboxEmail.getEmailId(branchId);
    }

    /**
     * @param o
     * @return count of updated inbox_ms table row  based on incident_id
     */
    public int update(Object o) {
        return inboxMs.update(o);
    }

}
