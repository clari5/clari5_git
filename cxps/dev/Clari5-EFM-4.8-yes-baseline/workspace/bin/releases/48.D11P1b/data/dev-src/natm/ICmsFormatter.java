package clari5.aml.cms.newjira;

import clari5.aml.cms.CmsException;
import clari5.platform.util.CxJson;
import cxps.eoi.IncidentEvent;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by bisman on 12/15/16.
 */
public interface ICmsFormatter{
    String getProjectId(IncidentEvent event) throws CmsException;
    String getSuppressionDuration(CxJson caseClosureJSON) throws JSONException ;
    String getCaseAssignee(IncidentEvent caseEvent) throws CmsException;
    String getIncidentAssignee(IncidentEvent caseEvent) throws CmsException;
    String getParentEntityId(IncidentEvent incidentEvent);
    String getChildEntityId(IncidentEvent incidentEvent);
    CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException;
    CxJson populateCaseJson(IncidentEvent event) throws CmsException;
    void postAssignmentAction(IncidentEvent event) throws CmsException;
}

