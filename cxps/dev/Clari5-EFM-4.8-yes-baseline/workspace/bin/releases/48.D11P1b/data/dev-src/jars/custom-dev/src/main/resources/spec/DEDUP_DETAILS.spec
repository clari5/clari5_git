clari5.custom.mapper {
        entity {
                DEDUP_DETAILS {
                       generate = true
                        attributes:[
                                { name: SERIAL_NO, type="string:300" ,key=true },
                                { name: REPORT_DATE ,type ="date" }
                                { name: WL_RULE_SCORE ,type ="string:300" }
				{ name: WL_RULE_NAME, type="string:300" }
                                { name: WL_MATCHED_VALUE ,type ="string:2000" }
                                { name: CUSTOMER_ID ,type ="string:50" }
				{ name: NEW_MODIFIED_CUSTOMER_ID, type="string:50" }
                                { name: STAFF_FLAG ,type ="string:1" }
                                { name: UCIC_FLAG ,type ="string:1" }
                              ]
				
       indexes {
              				 IDX_CUSTID : [ CUSTOMER_ID ]
					 IDX_NEWCUSTID : [ NEW_MODIFIED_CUSTOMER_ID ]
					IDX_WL_RULE_NAME : [ WL_RULE_NAME ]
					IDX_HBRD : [ NEW_MODIFIED_CUSTOMER_ID,WL_RULE_NAME,UCIC_FLAG ]
 }
                        }
        }
}
