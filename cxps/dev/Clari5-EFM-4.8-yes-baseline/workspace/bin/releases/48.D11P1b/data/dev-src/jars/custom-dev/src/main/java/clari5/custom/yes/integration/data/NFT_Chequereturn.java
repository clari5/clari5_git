package clari5.custom.yes.integration.data;

public class NFT_Chequereturn extends ITableData {
    private String tableName = "NFT_CHEQRETURN";
    private String event_type = "NFT_Chequereturn";
    private String  EVENT_ID;
    private String  branchid;
    private String  cheque_number;
    private String  branchiddesc;
    private String  eventts;
    private String  rejection_Code;
    private String  host_id;
    private String  acct_id;
    private String  user_id;
    private String  acct_status;
    private String  transaction_numonic_code;
    private String  rejection_reason;
    private String  sys_time;
    private String  acct_name;
    private String  cust_id;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getCheque_number() {
        return cheque_number;
    }

    public void setCheque_number(String cheque_number) {
        this.cheque_number = cheque_number;
    }

    public String getBranchiddesc() {
        return branchiddesc;
    }

    public void setBranchiddesc(String branchiddesc) {
        this.branchiddesc = branchiddesc;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getRejection_Code() {
        return rejection_Code;
    }

    public void setRejection_Code(String rejection_Code) {
        this.rejection_Code = rejection_Code;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getAcct_id() {
        return acct_id;
    }

    public void setAcct_id(String acct_id) {
        this.acct_id = acct_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAcct_status() {
        return acct_status;
    }

    public void setAcct_status(String acct_status) {
        this.acct_status = acct_status;
    }

    public String getTransaction_numonic_code() {
        return transaction_numonic_code;
    }

    public void setTransaction_numonic_code(String transaction_numonic_code) {
        this.transaction_numonic_code = transaction_numonic_code;
    }

    public String getRejection_reason() {
        return rejection_reason;
    }

    public void setRejection_reason(String rejection_reason) {
        this.rejection_reason = rejection_reason;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getAcct_name() {
        return acct_name;
    }

    public void setAcct_name(String acct_name) {
        this.acct_name = acct_name;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }
}

