package clari5.custom.jasper;

import clari5.custom.yes.db.NewModifiedCustomers;

public class CustFact {

    private String cust_id;
    private NewModifiedCustomers newModifiedCustomers;

    public CustFact(NewModifiedCustomers newModifiedCustomers,String cust_id){
        this.cust_id=cust_id;
        this.newModifiedCustomers=newModifiedCustomers;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public NewModifiedCustomers getNewModifiedCustomers() {
        return newModifiedCustomers;
    }

    public void setNewModifiedCustomers(NewModifiedCustomers newModifiedCustomers) {
        this.newModifiedCustomers = newModifiedCustomers;
    }

    @Override
    public String toString() {
        return "CustFact{" +
                "cust_id='" + cust_id + '\'' +
                ", newModifiedCustomers=" + newModifiedCustomers.toString() +
                '}';
    }
}
