clari5.custom.yes.db{
        entity {
                inbox-auto-req {
                       generate = true
                        attributes:[
                       { name :temp-name,          type : "string:100", key : false}
                                { name :scn-name,           type : "string:100" ,key=true}
                                { name :auto-email-chq-flg, type : "string:1" ,key=false}
                                { name :auto-inbox-chq-flg, type : "string:1" ,key=false}
                                { name :msg_subject,        type : "string:500" ,key=false}
                                { name :message,            type : "string:4000" ,key=false}
                                { name :created-on,         type : timestamp}
                                { name :updated-on,         type : timestamp}
                                { name :created-by,         type : "string:25" ,key=false}
                                { name :updated-by,         type : "string:25" ,key=false}
                                ]
 criteria-query {
                             name: InboxAutoReq
                             summary = [temp-name,scn-name,auto-email-chq-flg,auto-inbox-chq-flg,msg_subject,message,created-on,updated-on,created-by,updated-by]
                             where {
                                   scn-name: equal-clause
                            }
                            order = ["created-on desc"]
                     }

                        }
        }
}
