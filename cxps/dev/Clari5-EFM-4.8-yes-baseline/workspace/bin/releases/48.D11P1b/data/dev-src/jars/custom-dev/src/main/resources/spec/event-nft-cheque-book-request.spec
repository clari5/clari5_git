cxps.events.event.nft-cheque-book-request{
  table-name : EVENT_NFT_CHEQUEBOOKREQUEST
  event-mnemonic: NCB
  workspaces : {
    ACCOUNT :account_id
    CUSTOMER :customer_id
  }
  event-attributes : {
	account_id: {db : true ,raw_name : Account_Id ,type : "string:200", custom-getter:Account_Id}
	customer_id: {db : true ,raw_name : Customer_ID ,type : "string:200" , custom-getter:Customer_Id }
	begincheqnumber: {db : true ,raw_name : BeginCheqNumber ,type : "string:200" }
	endcheqnumber: {db : true ,raw_name : EndCheqNumber ,type : "string:200"}
	no_of_leav_isud: {db : true ,raw_name : No_of_Leav_isud ,type : "number:6", custom-getter:No_of_Leav_isud}
	avl_bal: {db : true ,raw_name : avl_bal ,type : "number:11,2", custom-getter:Avl_bal}
	system: {db : true ,raw_name : System ,type : "string:200"}
	issue_date: {db : true ,raw_name : Issue_Date ,type : timestamp, custom-getter:Issue_Date}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	host_id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id}
	eventts: {db : true ,raw_name : eventts ,type : timestamp}
	}
}
