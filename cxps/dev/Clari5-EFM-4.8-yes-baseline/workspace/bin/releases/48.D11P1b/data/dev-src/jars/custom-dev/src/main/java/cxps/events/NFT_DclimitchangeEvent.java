// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_DCLIMITCHANGE", Schema="rice")
public class NFT_DclimitchangeEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String twoFaMode;
       @Field(size=20) public Double currentCombinedLimit;
       @Field(size=200) public String errorDesc;
       @Field(size=10) public String succFailFlg;
       @Field(size=200) public String custSegment;
       @Field(size=200) public String ipCountry;
       @Field(size=200) public String deviceId;
       @Field(size=200) public String addrNetwork;
       @Field(size=2) public String hostId;
       @Field(size=20) public Double currentIndividualLimit7;
       @Field(size=20) public String twoFaStatus;
       @Field(size=20) public Double proposedCombinedLimit;
       @Field(size=200) public String ipCity;
       @Field(size=20) public Double currentIndividualLimit4;
       @Field(size=20) public Double currentIndividualLimit3;
       @Field(size=200) public String obdxTransactionName;
       @Field(size=20) public Double currentIndividualLimit6;
       @Field(size=200) public String userId;
       @Field(size=20) public Double currentIndividualLimit5;
       @Field(size=20) public Double currentIndividualLimit2;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public Double currentIndividualLimit1;
       @Field(size=200) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String obdxModuleName;
       @Field(size=20) public Double proposedCombinedLimit6;
       @Field(size=20) public Double proposedCombinedLimit7;
       @Field(size=20) public Double proposedCombinedLimit4;
       @Field(size=20) public Double proposedCombinedLimit5;
       @Field(size=20) public Double proposedCombinedLimit2;
       @Field(size=20) public Double proposedCombinedLimit3;
       @Field(size=20) public Double proposedCombinedLimit1;
       @Field(size=200) public String sessionId;


    @JsonIgnore
    public ITable<NFT_DclimitchangeEvent> t = AEF.getITable(this);

	public NFT_DclimitchangeEvent(){}

    public NFT_DclimitchangeEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Dclimitchange");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTwoFaMode(json.getString("2fa_mode"));
            setCurrentCombinedLimit(EventHelper.toDouble(json.getString("current_combined_limit")));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setCustSegment(json.getString("cust_segment"));
            setIpCountry(json.getString("ip_country"));
            setDeviceId(json.getString("device_id"));
            setAddrNetwork(json.getString("addr_network"));
            setHostId(json.getString("host_id"));
            setCurrentIndividualLimit7(EventHelper.toDouble(json.getString("current_individual_limit7")));
            setTwoFaStatus(json.getString("2fa_status"));
            setProposedCombinedLimit(EventHelper.toDouble(json.getString("proposed_combined_limit")));
            setIpCity(json.getString("ip_city"));
            setCurrentIndividualLimit4(EventHelper.toDouble(json.getString("current_individual_limit4")));
            setCurrentIndividualLimit3(EventHelper.toDouble(json.getString("current_individual_limit3")));
            setObdxTransactionName(json.getString("obdx_transaction_name"));
            setCurrentIndividualLimit6(EventHelper.toDouble(json.getString("current_individual_limit6")));
            setUserId(json.getString("user_id"));
            setCurrentIndividualLimit5(EventHelper.toDouble(json.getString("current_individual_limit5")));
            setCurrentIndividualLimit2(EventHelper.toDouble(json.getString("current_individual_limit2")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCurrentIndividualLimit1(EventHelper.toDouble(json.getString("current_individual_limit1")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setObdxModuleName(json.getString("obdx_module_name"));
            setProposedCombinedLimit6(EventHelper.toDouble(json.getString("proposed_combined_limit6")));
            setProposedCombinedLimit7(EventHelper.toDouble(json.getString("proposed_combined_limit7")));
            setProposedCombinedLimit4(EventHelper.toDouble(json.getString("proposed_combined_limit4")));
            setProposedCombinedLimit5(EventHelper.toDouble(json.getString("proposed_combined_limit5")));
            setProposedCombinedLimit2(EventHelper.toDouble(json.getString("proposed_combined_limit2")));
            setProposedCombinedLimit3(EventHelper.toDouble(json.getString("proposed_combined_limit3")));
            setProposedCombinedLimit1(EventHelper.toDouble(json.getString("proposed_combined_limit1")));
            setSessionId(json.getString("session_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "DC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTwoFaMode(){ return twoFaMode; }

    public Double getCurrentCombinedLimit(){ return currentCombinedLimit; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getCustSegment(){ return custSegment; }

    public String getIpCountry(){ return ipCountry; }

    public String getDeviceId(){ return deviceId; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getHostId(){ return hostId; }

    public Double getCurrentIndividualLimit7(){ return currentIndividualLimit7; }

    public String getTwoFaStatus(){ return twoFaStatus; }

    public Double getProposedCombinedLimit(){ return proposedCombinedLimit; }

    public String getIpCity(){ return ipCity; }

    public Double getCurrentIndividualLimit4(){ return currentIndividualLimit4; }

    public Double getCurrentIndividualLimit3(){ return currentIndividualLimit3; }

    public String getObdxTransactionName(){ return obdxTransactionName; }

    public Double getCurrentIndividualLimit6(){ return currentIndividualLimit6; }

    public String getUserId(){ return userId; }

    public Double getCurrentIndividualLimit5(){ return currentIndividualLimit5; }

    public Double getCurrentIndividualLimit2(){ return currentIndividualLimit2; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public Double getCurrentIndividualLimit1(){ return currentIndividualLimit1; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getObdxModuleName(){ return obdxModuleName; }

    public Double getProposedCombinedLimit6(){ return proposedCombinedLimit6; }

    public Double getProposedCombinedLimit7(){ return proposedCombinedLimit7; }

    public Double getProposedCombinedLimit4(){ return proposedCombinedLimit4; }

    public Double getProposedCombinedLimit5(){ return proposedCombinedLimit5; }

    public Double getProposedCombinedLimit2(){ return proposedCombinedLimit2; }

    public Double getProposedCombinedLimit3(){ return proposedCombinedLimit3; }

    public Double getProposedCombinedLimit1(){ return proposedCombinedLimit1; }

    public String getSessionId(){ return sessionId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTwoFaMode(String val){ this.twoFaMode = val; }
    public void setCurrentCombinedLimit(Double val){ this.currentCombinedLimit = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setCustSegment(String val){ this.custSegment = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCurrentIndividualLimit7(Double val){ this.currentIndividualLimit7 = val; }
    public void setTwoFaStatus(String val){ this.twoFaStatus = val; }
    public void setProposedCombinedLimit(Double val){ this.proposedCombinedLimit = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setCurrentIndividualLimit4(Double val){ this.currentIndividualLimit4 = val; }
    public void setCurrentIndividualLimit3(Double val){ this.currentIndividualLimit3 = val; }
    public void setObdxTransactionName(String val){ this.obdxTransactionName = val; }
    public void setCurrentIndividualLimit6(Double val){ this.currentIndividualLimit6 = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setCurrentIndividualLimit5(Double val){ this.currentIndividualLimit5 = val; }
    public void setCurrentIndividualLimit2(Double val){ this.currentIndividualLimit2 = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCurrentIndividualLimit1(Double val){ this.currentIndividualLimit1 = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setObdxModuleName(String val){ this.obdxModuleName = val; }
    public void setProposedCombinedLimit6(Double val){ this.proposedCombinedLimit6 = val; }
    public void setProposedCombinedLimit7(Double val){ this.proposedCombinedLimit7 = val; }
    public void setProposedCombinedLimit4(Double val){ this.proposedCombinedLimit4 = val; }
    public void setProposedCombinedLimit5(Double val){ this.proposedCombinedLimit5 = val; }
    public void setProposedCombinedLimit2(Double val){ this.proposedCombinedLimit2 = val; }
    public void setProposedCombinedLimit3(Double val){ this.proposedCombinedLimit3 = val; }
    public void setProposedCombinedLimit1(Double val){ this.proposedCombinedLimit1 = val; }
    public void setSessionId(String val){ this.sessionId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_DclimitchangeEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Dclimitchange");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Dclimitchange");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}