package clari5.custom.jira;

import java.util.*;

import clari5.aml.cms.CmsException;
import clari5.platform.applayer.Clari5;
import clari5.platform.util.CxJson;
import clari5.aml.cms.jira.custom.IFieldAppender;
import clari5.platform.util.Hocon;
import java.util.Date;
import java.util.regex.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cxps.apex.utils.CxpsLogger;

import clari5.rdbms.Rdbms;


/*

Created by Deepak
*/
public class CustomFieldAppender implements IFieldAppender {

    private static CxpsLogger logger = CxpsLogger.getLogger(CustomFieldAppender.class);
    Map<String, YEScustField> fieldMap;
    Hocon customFieldParam = new Hocon();
    static int exeCount = 0;
    boolean flag =false;

    public CustomFieldAppender() {
        fieldMap = new HashMap<>();
    }

    public void init() {
        logger.info("Init method in CustomFieldAppender");
        Hocon fieldHocon = new Hocon();
        fieldHocon.loadFromContext("yesjiracustomfields.conf");
        Hocon moduleHocon = fieldHocon.get("clari5.yes.yesjiracustomfields");
        List<String> moduleList = moduleHocon.getKeysAsList();
        customFieldParam.loadFromContext("custFieldParammap.conf");
        fieldMap.clear();
        for (String moduleId : moduleList) {
            Hocon customHocon = moduleHocon.get(moduleId);
            Set<String> keys = customHocon.getKeys();
            for (String fieldName : keys) {
                YEScustField custField = new YEScustField();
                Hocon h = customHocon.get(fieldName);
                custField.setFieldName(fieldName);
                custField.setJiraId(h.getString("jiraId"));
                custField.setJiraType(h.getString("jiraType"));
                custField.setTaskType(h.getString("taskType"));
                custField.setValue(h.getString("fieldValue"));
                fieldMap.put(moduleId + ":" + fieldName, custField);
            }
        }
        logger.info("[init] FieldMap " + fieldMap.toString());
    }

    public void refresh() {

    }


    public Map<String, String> customFieldsJson(CxJson caseJson, String taskType, String moduleId) {
        logger.info("case json is " + caseJson.toString() + "\n taskType [" + taskType + "] and moduleId [" + moduleId + "]");
        Map<String, String> newFieldsMap = new HashMap<>();

        switch (taskType) {
            case "cmsIncidents":
                String entityName = caseJson.getString(customFieldParam.getString("wsFields.wsName"));
                String wsId = caseJson.getString(customFieldParam.getString("wsFields.wsId"));

                logger.info("enttityname is [" + entityName + "]");

                Map<String, String> fieldsData = new HashMap<>();
                switch (entityName.toLowerCase().trim()) {

                    case "customer":
                        try {
                            exeCount=0;
                            wsId = wsId.substring(4);
                            logger.info(" Customer with id [" + wsId + "]");

                            fieldsData = getCustomerFields(wsId, caseJson, moduleId, null);
                        } catch (CmsException cms) {
                            fieldsData.clear();
                            while (exeCount < 3) {
                                try {
                                    fieldsData = getCustomerFields(wsId, caseJson, moduleId, null);
                                } catch (CmsException cm) {
                                   logger.error("[getCustomerFields] Retry attempts " + exeCount + "Failed "+cm.getMessage());
                                }

                            }
                        }
                        return fieldsData;

                    case "account":

                        fieldsData.clear();
                        exeCount=0;
                        wsId = wsId.substring(4);
                        logger.info("Account with id [" + wsId + "]");
                        try {
                            fieldsData=  getAccountFields(wsId, caseJson, moduleId);
                        } catch (CmsException cms) {
                            fieldsData.clear();
                            while (exeCount < 3) {
                                try {
                                    fieldsData = getAccountFields(wsId, caseJson, moduleId);
                                } catch (CmsException cm) {
                                    logger.error("[getAccountFields] Retry attempts " + exeCount + "Failed ");
                                }

                            }
                        }
                        return fieldsData;


                    default:
                        return newFieldsMap;
                }
        }

        return newFieldsMap;


    }

    public static boolean isAlphaNumeric(String s) {
     Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]");
     Matcher matcher = regex.matcher(s);
     if(matcher.find()){
       return true;
     }else{
       
     return s != null && !s.isEmpty() && (s.matches("([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])") || s.matches("^[a-zA-Z]*$") || s.matches("([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z]|[0-9].*[a-zA-Z].*[0-9]|[a-zA-z].*[0-9].*[a-zA-Z])"));
    }
}

    public Connection getDBConnection() {
        Connection con = null;
        try {
            con = Rdbms.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }


    public Map<String, String> getCustomerFields(String cifnumber, CxJson caseJson, String moduleId, String acctNumber) throws CmsException {
        Map<String, String> newFieldsMap = new HashMap<>();
        Connection con = null;
        Map<String, String> dbFields = new HashMap<>();
        ResultSet rs = null;
        PreparedStatement psmt = null;
        try {
		  /* check whether custid is alphanumeric or not */
		flag = isAlphaNumeric(cifnumber);
		 if(flag)
            	   {
		            dbFields.put("dob", "NA");
		            dbFields.put("ucic", "NA");
		            dbFields.put("mailing_city", "NA");
		            dbFields.put("mailing_country", "NA");
		            dbFields.put("mobileNo", "NA");
		            dbFields.put("emailId", "NA");
		            dbFields.put("turnover", "NA");
		            dbFields.put("mailing_address1", "NA");
		            dbFields.put("mailing_address2", "NA");
		            dbFields.put("mailing_address3", "NA");
		            dbFields.put("customer_type", "NA");
		            dbFields.put("business", "NA");
		            dbFields.put("natureof_business", "NA");
		            dbFields.put("customer_name", "NA");
		            dbFields.put("account_status", "NA");
		            dbFields.put("acccount_open_date", "NA");
		            dbFields.put("account_type", "NA");
		            dbFields.put("branch", "NA");

		            logger.info("[getCustomerFields] Custom db fields " + dbFields.toString());

              	  }
		else
		{

           	 logger.info("[getCustomerFields] Going to get connection ");
            	 con = getDBConnection();

            String query = "select  CUSTOMER_MASTER.MAILING_CITY,CUSTOMER_MASTER.MAILING_COUNTRY,CUSTOMER_MASTER.UCIC,CUSTOMER_MASTER.MAILING_ADDRESS_1,CUSTOMER_MASTER.MAILING_ADDRESS_2,CUSTOMER_MASTER.MAILING_ADDRESS_3,CUSTOMER_MASTER.MOBILE,CUSTOMER_MASTER.TURNOVER,CUSTOMER_MASTER.EMAIL_ID,CUSTOMER_MASTER.CUSTOMER_TYPE||'|'||CUSTOMER_MASTER.CUSTOMER_DESCRIPTION AS CUSTOMER_TYPE,CUSTOMER_MASTER.BUSINESS,CUSTOMER_MASTER.NATURE_OF_BUSINESS,CUSTOMER_MASTER.CUSTOMER_NAME,CUSTOMER_MASTER.DOB ,CUSTOMER_MASTER.MAILING_ADDRESS_1,CUSTOMER_MASTER.MAILING_ADDRESS_2,CUSTOMER_MASTER.MAILING_ADDRESS_3 from CUSTOMER_MASTER where  CUSTOMER_MASTER.CUSTOMER_ID =?";

            logger.info("[getCustomerFields] Query [" + query + "] and prameters [" + cifnumber + "]");
            psmt = con.prepareStatement(query);
            psmt.setString(1, cifnumber);
            rs = psmt.executeQuery();
            logger.info("[getCustomerFields] Result set data [" + rs.toString() + "]");

               if (rs.next()) {

                    dbFields.put("dob", objToStr(rs.getDate("DOB")));
                    dbFields.put("ucic", rs.getString("UCIC"));
                    dbFields.put("mailing_city", rs.getString("MAILING_CITY"));
                    dbFields.put("mailing_country", rs.getString("MAILING_COUNTRY"));
                    dbFields.put("mobileNo", rs.getString("MOBILE"));
                    dbFields.put("emailId", rs.getString("EMAIL_ID"));
                    dbFields.put("turnover", rs.getString("TURNOVER"));
                    dbFields.put("mailing_address1", rs.getString("MAILING_ADDRESS_1"));
                    dbFields.put("mailing_address2", rs.getString("MAILING_ADDRESS_2"));
                    dbFields.put("mailing_address3", rs.getString("MAILING_ADDRESS_3"));
                    dbFields.put("customer_type", objToStr(rs.getString("CUSTOMER_TYPE")));
                    dbFields.put("business", rs.getString("BUSINESS"));
                    dbFields.put("natureof_business", rs.getString("NATURE_OF_BUSINESS"));
                    dbFields.put("customer_name", rs.getString("CUSTOMER_NAME"));
                    dbFields.put("account_status", "NA");
                    dbFields.put("acccount_open_date", "NA");
                    dbFields.put("account_type", "NA");
                    dbFields.put("branch", "NA");

                    logger.info("[getCustomerFields] Custom db fields " + dbFields.toString());

                }
            }
        } catch (SQLException sqe) {
            exeCount++;
            logger.error("[getCustomerFields] Failed on db operation " + sqe.getMessage() + " Cause " + sqe.getCause());
            throw new CmsException.Retry("[getCustomerFields] Retrying the Connection attempt " + exeCount);
        } catch (NullPointerException np) {
            exeCount++;
            logger.error("[getCustomerFields] Failed to connect with DB " + np.getMessage() + " Cause " + np.getCause());

            throw new CmsException.Retry(np.getCause() + "\n [getCustomerFields] Retrying the Connection attempt " + exeCount);
        } catch (Exception e) {
            logger.error("[getCustomerFields]  Unable to append the custom fields " + e.getMessage() + " Cause " + e.getCause());
        } finally {
            if (con != null) {
                try {
                    rs.close();
                    psmt.close();
                    con.close();
                } catch (SQLException ignore) {
                }
            }

            for (Map.Entry<String, YEScustField> fielddata : fieldMap.entrySet()) {
                try {
                    YEScustField custField = fielddata.getValue();
                    newFieldsMap.put(custField.getJiraId(), getCustomFieldValue(dbFields.get(customFieldParam.getString("custFields." + custField.getJiraId()))));
                } catch (Exception e) {
                    logger.error("[getCustomerFields] unable to get custom field data " + fielddata.toString() + "check custFieldParammap.conf file with jiraId" + "\n Exception " + e.getMessage() + " Cause " + e.getCause());
                    continue;
                }
            }


        }
        dbFields.clear();
        logger.info("[getCustomerFields] custom-field map  of customer data  " + newFieldsMap.toString());
        exeCount=3;
        return newFieldsMap;
    }


    public Map<String, String> getAccountFields(String acctNumber, CxJson caseJson, String moduleId) throws CmsException {
        Map<String, String> newFieldsMap = new HashMap<>();
        Connection con = null;
        Map<String, String> dbFields = new HashMap<>();
        ResultSet rs = null;
        PreparedStatement psmt = null;

        try {
	    
            /* check whether acctid is alphanumeric or not */
            flag = isAlphaNumeric(acctNumber);
	     if (flag)
               {
                    dbFields.put("dob", "NA");
                    dbFields.put("ucic", "NA");
                    dbFields.put("mailing_city", "NA");
                    dbFields.put("mailing_country", "NA");
                    dbFields.put("mobileNo", "NA");
                    dbFields.put("emailId", "NA");
                    dbFields.put("turnover", "NA");
                    dbFields.put("mailing_address1", "NA");
                    dbFields.put("mailing_address2", "NA");
                    dbFields.put("mailing_address3", "NA");
                    dbFields.put("customer_type", "NA");
                    dbFields.put("business", "NA");
                    dbFields.put("natureof_business", "NA");
                    dbFields.put("customer_name", "NA");
                    dbFields.put("account_status", "NA");
                    dbFields.put("acccount_open_date", "NA");
                    dbFields.put("account_type", "NA");
                    dbFields.put("branch", "NA");
                    dbFields.put("avl_bal", "NA");

                    logger.info("[getAccountFields] Custom db fields " + dbFields.toString());

                }
	else
	  {

            logger.info("[getAccountFields] Going to get connection ");

            con = getDBConnection();
            logger.info("[getAccountFields] Fetching data from customer_details connection " + con.toString());

            String query = "select ACCOUNT_MASTER.ACCCOUNT_OPEN_DATE,ACCOUNT_MASTER.ACCOUNT_STATUS||'|'||ACCOUNT_STATUS_MASTER.TXT_ACCT_STATUS AS ACCOUNT_STATUS ,ACCOUNT_MASTER.ACCOUNT_TYPE,ACCOUNT_MASTER.BRANCH_CODE||'|'||BRANCH_MASTER.BRANCH_NAME AS BRANCH_CODE ,CUSTOMER_MASTER.MAILING_CITY,CUSTOMER_MASTER.MAILING_COUNTRY,\n" +
                    "CUSTOMER_MASTER.UCIC,CUSTOMER_MASTER.MAILING_ADDRESS_1,CUSTOMER_MASTER.MAILING_ADDRESS_2,\n" +
                    "CUSTOMER_MASTER.MAILING_ADDRESS_3,CUSTOMER_MASTER.MOBILE,CUSTOMER_MASTER.TURNOVER,CUSTOMER_MASTER.EMAIL_ID,CUSTOMER_MASTER.CUSTOMER_TYPE||'|'||CUSTOMER_MASTER.CUSTOMER_DESCRIPTION AS CUSTOMER_TYPE,CUSTOMER_MASTER.BUSINESS,CUSTOMER_MASTER.NATURE_OF_BUSINESS,CUSTOMER_MASTER.CUSTOMER_NAME,CUSTOMER_MASTER.DOB ,\n" +
                    "ACCOUNT_MASTER.AVL_BAL from ACCOUNT_MASTER, CUSTOMER_MASTER,ACCOUNT_STATUS_MASTER,BRANCH_MASTER  where  ACCOUNT_MASTER.ACCOUNT_ID = ? and ACCOUNT_MASTER.CUSTOMER_ID=CUSTOMER_MASTER.CUSTOMER_ID and ACCOUNT_MASTER.ACCOUNT_STATUS=ACCOUNT_STATUS_MASTER.COD_ACCT_STATUS and ACCOUNT_MASTER.BRANCH_CODE=BRANCH_MASTER.BRANCH_SOL_ID";
            logger.info("[getAccountFields] Query " + query + " and parameter is [" + acctNumber + "]");

            psmt = con.prepareStatement(query);
            psmt.setString(1, acctNumber);

            rs = psmt.executeQuery();
            logger.info("[getAccountFields] Result set data [" + rs.toString() + "]");
                if (rs.next()) {
                    dbFields.put("dob", objToStr(rs.getDate("DOB")));
                    dbFields.put("ucic", rs.getString("UCIC"));
                    dbFields.put("mailing_city", rs.getString("MAILING_CITY"));
                    dbFields.put("mailing_country", rs.getString("MAILING_COUNTRY"));
                    dbFields.put("mobileNo", rs.getString("MOBILE"));
                    dbFields.put("emailId", rs.getString("EMAIL_ID"));
                    dbFields.put("turnover", rs.getString("TURNOVER"));
                    dbFields.put("mailing_address1", rs.getString("MAILING_ADDRESS_1"));
                    dbFields.put("mailing_address2", rs.getString("MAILING_ADDRESS_2"));
                    dbFields.put("mailing_address3", rs.getString("MAILING_ADDRESS_3"));
                    dbFields.put("customer_type", objToStr(rs.getString("CUSTOMER_TYPE")));
                    dbFields.put("business", rs.getString("BUSINESS"));
                    dbFields.put("natureof_business", rs.getString("NATURE_OF_BUSINESS"));
                    dbFields.put("customer_name", rs.getString("CUSTOMER_NAME"));
                    dbFields.put("account_status", objToStr(rs.getString("ACCOUNT_STATUS")));
                    dbFields.put("acccount_open_date", objToStr(rs.getDate("ACCCOUNT_OPEN_DATE")));
                    dbFields.put("account_type", rs.getString("ACCOUNT_TYPE"));
                    dbFields.put("branch", objToStr(rs.getString("BRANCH_CODE")));
                    dbFields.put("avl_bal", objToStr(rs.getDouble("AVL_BAL")) + "");

                    logger.info("[getAccountFields] Custom db fields " + dbFields.toString());


                }
            }
        } catch (SQLException sqe) {

            exeCount++;
            logger.error("[getAccountFields] Failed on db operation " + sqe.getMessage() + " Cause " + sqe.getCause());
            throw new CmsException.Retry("[getAccountFields] Retrying the Connection");

        } catch (NullPointerException np) {
            exeCount++;
            logger.error("[getAccountFields] Failed on Connect DB " + np.getMessage() + " Cause " + np.getCause());
            //System.out.println("[getAccountFields] Failed on Connect DB " + np.getMessage() + " Cause " + np.getCause());

            throw new CmsException.Retry(np.getCause() + "\n [getAccountFields] Retrying the Connection" + exeCount);
        } catch (Exception e) {
            logger.error("[getAccountFields]  Unable to append the custom fields " + e.getMessage() + " Cause " + e.getCause());
        } finally {

            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ignore) {
                }
            }
        }
        for (Map.Entry<String, YEScustField> fielddata : fieldMap.entrySet()) {
            try {
                YEScustField custField = fielddata.getValue();
                newFieldsMap.put(custField.getJiraId(), getCustomFieldValue(dbFields.get(customFieldParam.getString("custFields." + custField.getJiraId()))));
            } catch (Exception e) {
                logger.error("[getAccountFields] unable to get custom field data " + fielddata.toString() + "check custFieldParammap.conf file with jiraId" + "\n Exception " + e.getMessage() + " Cause " + e.getCause());
                continue;
            }
        }

        dbFields.clear();
        logger.info("[getAccountFields] custom field map  of account data  " + newFieldsMap.toString());
        exeCount=3;
        return newFieldsMap;
    }


    public static String getCustomFieldValue(String value) {
        if (value != null && !"".equals(value)) {
            return value;
        } else {
            return "NA";
        }

    }

    public String objToStr(Object value) {
        try {
            int idx;
            if (null == value) {
                return "NA";

            } else if ((idx = value.toString().indexOf('|')) != -1) {
                String val = "";
                String oldVal = value.toString();
                if (value.toString().length() == 1) return "NA|NA";
                val = (idx == 0) ? "NA" + oldVal : ((oldVal.length() - 1) - idx == 0) ? oldVal + "NA" : oldVal;
                return val;
            } else
                return value.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "NA";
        }
    }

/*    public static void main(String[] args)throws Exception {
        Clari5.batchBootstrap("dbcon","dbcon-module.conf");
        System.out.println("start proceess");
        String json = "{\"parent\":{\"key\":\"EFM-22547\"},\"project\":{\"key\":\"EFM\"},\"issuetype\":{\"id\":\"5\"},\"assignee\":{\"name\":\"RRE4094018\"},\"customfield_10900\":\"First credit via DD PO in a newly opened account \",\"summary\":\"106790400001550 | FRM:S0026_FST_CR_NEW_CASA_ACCT_DD_PO_INTU:First credit via DD PO in a newly opened account \",\"customfield_10503\":\"Evidence for trigger\\r\\ntran_id, Tran_amount, DRCR\\r\\n325812018072446343901119241,152504,C\\r\\n\\r\\n\\r\\n\",\"customfield_11903\":\"https://dn.placeholder.com:1111/efm/cef/index.html\",\"customfield_10211\":\"S0026_FST_CR_NEW_CASA_ACCT_DD_PO_INTU\",\"customfield_11607\":\"FTACCTTXNFCR-24-07-1808071653449392A_F_106790400001550S0026_FST_CR_NEW_CASA_ACCT_DD_PO_INTU\",\"customfield_10513\":\"CUSTOMER\",\"customfield_10516\":\"9702385 | FRM\",\"customfield_11401\":\"A_F_106790400001550\",\"customfield_10505\":\"account\",\"customfield_10213\":\"2018-07-25 11:49:49.912\",\"customfield_10212\":\"400\",\"customfield_10214\":\"106790400001550 | FRM\",\"customfield_10610\":\"L4\",\"customfield_10703\":\"Not Available\",\"customfield_10704\":\"Not Available\",\"customfield_10705\":\"Not Available\",\"customfield_12300\":\"Not Available\",\"customfield_10215\":\"Not Available\"}";
        CustomFieldAppender customFieldAppender = new CustomFieldAppender();
        customFieldAppender.init();
        System.out.println(customFieldAppender.customFieldsJson(CxJson.parse(json),"cmsIncidents","EFM").toString());
        System.out.println("End processor");


    }*/


}

