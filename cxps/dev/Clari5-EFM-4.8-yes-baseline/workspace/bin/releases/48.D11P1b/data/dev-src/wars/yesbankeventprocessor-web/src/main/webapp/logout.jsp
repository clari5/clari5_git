<%
        String userId = (String) session.getAttribute("USERID");

        String hostport = request.getServerName() + ":" +request.getServerPort();

        String temp = "/SecurityAdmin/UILoginService?j_action=logout";

        if(request.getRequestURL().toString().trim().startsWith("https"))
        {
                response.sendRedirect("https://"+hostport+temp);
        }
        else
        {
                response.sendRedirect("http://"+hostport+temp);
        }
%>
