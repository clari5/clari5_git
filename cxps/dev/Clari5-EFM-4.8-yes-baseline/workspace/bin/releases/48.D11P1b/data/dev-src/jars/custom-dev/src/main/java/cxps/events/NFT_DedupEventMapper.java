// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_DedupEventMapper extends EventMapper<NFT_DedupEvent> {

public NFT_DedupEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_DedupEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_DedupEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_DedupEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_DedupEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_DedupEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_DedupEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getLandlineNumber());
            preparedStatement.setInt(i++, obj.getAccountCount());
            preparedStatement.setDouble(i++, obj.getNameMatchScore());
            preparedStatement.setDouble(i++, obj.getBrpermanantAddrScore());
            preparedStatement.setInt(i++, obj.getMobNonStaffMatchedCount());
            preparedStatement.setDouble(i++, obj.getPermanantAddrScore());
            preparedStatement.setString(i++, obj.getHandPhone());
            preparedStatement.setInt(i++, obj.getPanMatchedCount());
            preparedStatement.setString(i++, obj.getCompletePerAddrChanged());
            preparedStatement.setString(i++, obj.getNameMatchedlist());
            preparedStatement.setInt(i++, obj.getEmailMatchedCount());
            preparedStatement.setString(i++, obj.getPassportNumber());
            preparedStatement.setDouble(i++, obj.getScorePercentage());
            preparedStatement.setString(i++, obj.getNameChanged());
            preparedStatement.setString(i++, obj.getDobCustId());
            preparedStatement.setDouble(i++, obj.getScore());
            preparedStatement.setString(i++, obj.getPermanentAddr1());
            preparedStatement.setString(i++, obj.getPermanentAddr3());
            preparedStatement.setString(i++, obj.getPermanentAddr2());
            preparedStatement.setInt(i++, obj.getIntermediateLandlineCount());
            preparedStatement.setString(i++, obj.getMobNonstaffCustId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setInt(i++, obj.getPassportMatchedCount());
            preparedStatement.setString(i++, obj.getPan());
            preparedStatement.setString(i++, obj.getCompleteMailingAddr());
            preparedStatement.setString(i++, obj.getPermanentaddrMatchedlist());
            preparedStatement.setString(i++, obj.getLandlineCustId());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getCustName());
            preparedStatement.setString(i++, obj.getCompletePermanentAddr());
            preparedStatement.setInt(i++, obj.getMobMatchedCount());
            preparedStatement.setInt(i++, obj.getDobMatchedCount());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCompleteMailingAddrChanged());
            preparedStatement.setInt(i++, obj.getBrcompleteMailingAddrCount());
            preparedStatement.setString(i++, obj.getEmailId());
            preparedStatement.setString(i++, obj.getMobCustId());
            preparedStatement.setString(i++, obj.getOfficeLandlineNumber());
            preparedStatement.setDouble(i++, obj.getBrmailingAddrScore());
            preparedStatement.setTimestamp(i++, obj.getDob());
            preparedStatement.setString(i++, obj.getMatchPhoneCustId());
            preparedStatement.setInt(i++, obj.getLandlineMatchedCount());
            preparedStatement.setString(i++, obj.getProductCode());
            preparedStatement.setString(i++, obj.getMobStaffCustId());
            preparedStatement.setString(i++, obj.getPanCustId());
            preparedStatement.setTimestamp(i++, obj.getDoi());
            preparedStatement.setString(i++, obj.getEntityType());
            preparedStatement.setString(i++, obj.getMobNumber());
            preparedStatement.setString(i++, obj.getPassportCustId());
            preparedStatement.setString(i++, obj.getMailingAddr3());
            preparedStatement.setInt(i++, obj.getOfficephoneMatchedCount());
            preparedStatement.setString(i++, obj.getDoiCustId());
            preparedStatement.setDouble(i++, obj.getMailingAddrScore());
            preparedStatement.setString(i++, obj.getHomeLandlineNumber());
            preparedStatement.setInt(i++, obj.getCompletePerAddrCount());
            preparedStatement.setInt(i++, obj.getIntermediatehandphoneCount());
            preparedStatement.setInt(i++, obj.getIntermediateofficelandCount());
            preparedStatement.setInt(i++, obj.getMobCount());
            preparedStatement.setString(i++, obj.getBranchaddrMatchedlist());
            preparedStatement.setInt(i++, obj.getCompleteMailingAddrCount());
            preparedStatement.setInt(i++, obj.getPhoneCount());
            preparedStatement.setString(i++, obj.getIsAccountClosed());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setInt(i++, obj.getDoiMatchedCount());
            preparedStatement.setString(i++, obj.getEmailCustId());
            preparedStatement.setString(i++, obj.getOfficePhoneCustId());
            preparedStatement.setInt(i++, obj.getBrcompletePerAddrCount());
            preparedStatement.setString(i++, obj.getMailingaddrMatchedlist());
            preparedStatement.setString(i++, obj.getMailingAddr2());
            preparedStatement.setString(i++, obj.getMailingAddr1());
            preparedStatement.setString(i++, obj.getUcicId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_DEDUP]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_DedupEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_DedupEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DEDUP"));
        putList = new ArrayList<>();

        for (NFT_DedupEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "LANDLINE_NUMBER",  obj.getLandlineNumber());
            p = this.insert(p, "ACCOUNT_COUNT", String.valueOf(obj.getAccountCount()));
            p = this.insert(p, "NAME_MATCH_SCORE", String.valueOf(obj.getNameMatchScore()));
            p = this.insert(p, "BRPERMANANT_ADDR_SCORE", String.valueOf(obj.getBrpermanantAddrScore()));
            p = this.insert(p, "MOB_NON_STAFF_MATCHED_COUNT", String.valueOf(obj.getMobNonStaffMatchedCount()));
            p = this.insert(p, "PERMANANT_ADDR_SCORE", String.valueOf(obj.getPermanantAddrScore()));
            p = this.insert(p, "HAND_PHONE",  obj.getHandPhone());
            p = this.insert(p, "PAN_MATCHED_COUNT", String.valueOf(obj.getPanMatchedCount()));
            p = this.insert(p, "COMPLETE_PER_ADDR_CHANGED",  obj.getCompletePerAddrChanged());
            p = this.insert(p, "NAME_MATCHEDLIST",  obj.getNameMatchedlist());
            p = this.insert(p, "EMAIL_MATCHED_COUNT", String.valueOf(obj.getEmailMatchedCount()));
            p = this.insert(p, "PASSPORT_NUMBER",  obj.getPassportNumber());
            p = this.insert(p, "SCORE_PERCENTAGE", String.valueOf(obj.getScorePercentage()));
            p = this.insert(p, "NAME_CHANGED",  obj.getNameChanged());
            p = this.insert(p, "DOB_CUST_ID",  obj.getDobCustId());
            p = this.insert(p, "SCORE", String.valueOf(obj.getScore()));
            p = this.insert(p, "PERMANENT_ADDR1",  obj.getPermanentAddr1());
            p = this.insert(p, "PERMANENT_ADDR3",  obj.getPermanentAddr3());
            p = this.insert(p, "PERMANENT_ADDR2",  obj.getPermanentAddr2());
            p = this.insert(p, "INTERMEDIATE_LANDLINE_COUNT", String.valueOf(obj.getIntermediateLandlineCount()));
            p = this.insert(p, "MOB_NONSTAFF_CUST_ID",  obj.getMobNonstaffCustId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "PASSPORT_MATCHED_COUNT", String.valueOf(obj.getPassportMatchedCount()));
            p = this.insert(p, "PAN",  obj.getPan());
            p = this.insert(p, "COMPLETE_MAILING_ADDR",  obj.getCompleteMailingAddr());
            p = this.insert(p, "PERMANENTADDR_MATCHEDLIST",  obj.getPermanentaddrMatchedlist());
            p = this.insert(p, "LANDLINE_CUST_ID",  obj.getLandlineCustId());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "CUST_NAME",  obj.getCustName());
            p = this.insert(p, "COMPLETE_PERMANENT_ADDR",  obj.getCompletePermanentAddr());
            p = this.insert(p, "MOB_MATCHED_COUNT", String.valueOf(obj.getMobMatchedCount()));
            p = this.insert(p, "DOB_MATCHED_COUNT", String.valueOf(obj.getDobMatchedCount()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "COMPLETE_MAILING_ADDR_CHANGED",  obj.getCompleteMailingAddrChanged());
            p = this.insert(p, "BRCOMPLETE_MAILING_ADDR_COUNT", String.valueOf(obj.getBrcompleteMailingAddrCount()));
            p = this.insert(p, "EMAIL_ID",  obj.getEmailId());
            p = this.insert(p, "MOB_CUST_ID",  obj.getMobCustId());
            p = this.insert(p, "OFFICE_LANDLINE_NUMBER",  obj.getOfficeLandlineNumber());
            p = this.insert(p, "BRMAILING_ADDR_SCORE", String.valueOf(obj.getBrmailingAddrScore()));
            p = this.insert(p, "DOB", String.valueOf(obj.getDob()));
            p = this.insert(p, "MATCH_PHONE_CUST_ID",  obj.getMatchPhoneCustId());
            p = this.insert(p, "LANDLINE_MATCHED_COUNT", String.valueOf(obj.getLandlineMatchedCount()));
            p = this.insert(p, "PRODUCT_CODE",  obj.getProductCode());
            p = this.insert(p, "MOB_STAFF_CUST_ID",  obj.getMobStaffCustId());
            p = this.insert(p, "PAN_CUST_ID",  obj.getPanCustId());
            p = this.insert(p, "DOI", String.valueOf(obj.getDoi()));
            p = this.insert(p, "ENTITY_TYPE",  obj.getEntityType());
            p = this.insert(p, "MOB_NUMBER",  obj.getMobNumber());
            p = this.insert(p, "PASSPORT_CUST_ID",  obj.getPassportCustId());
            p = this.insert(p, "MAILING_ADDR3",  obj.getMailingAddr3());
            p = this.insert(p, "OFFICEPHONE_MATCHED_COUNT", String.valueOf(obj.getOfficephoneMatchedCount()));
            p = this.insert(p, "DOI_CUST_ID",  obj.getDoiCustId());
            p = this.insert(p, "MAILING_ADDR_SCORE", String.valueOf(obj.getMailingAddrScore()));
            p = this.insert(p, "HOME_LANDLINE_NUMBER",  obj.getHomeLandlineNumber());
            p = this.insert(p, "COMPLETE_PER_ADDR_COUNT", String.valueOf(obj.getCompletePerAddrCount()));
            p = this.insert(p, "INTERMEDIATEHANDPHONE_COUNT", String.valueOf(obj.getIntermediatehandphoneCount()));
            p = this.insert(p, "INTERMEDIATEOFFICELAND_COUNT", String.valueOf(obj.getIntermediateofficelandCount()));
            p = this.insert(p, "MOB_COUNT", String.valueOf(obj.getMobCount()));
            p = this.insert(p, "BRANCHADDR_MATCHEDLIST",  obj.getBranchaddrMatchedlist());
            p = this.insert(p, "COMPLETE_MAILING_ADDR_COUNT", String.valueOf(obj.getCompleteMailingAddrCount()));
            p = this.insert(p, "PHONE_COUNT", String.valueOf(obj.getPhoneCount()));
            p = this.insert(p, "IS_ACCOUNT_CLOSED",  obj.getIsAccountClosed());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "DOI_MATCHED_COUNT", String.valueOf(obj.getDoiMatchedCount()));
            p = this.insert(p, "EMAIL_CUST_ID",  obj.getEmailCustId());
            p = this.insert(p, "OFFICE_PHONE_CUST_ID",  obj.getOfficePhoneCustId());
            p = this.insert(p, "BRCOMPLETE_PER_ADDR_COUNT", String.valueOf(obj.getBrcompletePerAddrCount()));
            p = this.insert(p, "MAILINGADDR_MATCHEDLIST",  obj.getMailingaddrMatchedlist());
            p = this.insert(p, "MAILING_ADDR2",  obj.getMailingAddr2());
            p = this.insert(p, "MAILING_ADDR1",  obj.getMailingAddr1());
            p = this.insert(p, "UCIC_ID",  obj.getUcicId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_DEDUP"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_DEDUP]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_DEDUP]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_DedupEvent obj = new NFT_DedupEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setLandlineNumber(rs.getString("LANDLINE_NUMBER"));
    obj.setAccountCount(rs.getInt("ACCOUNT_COUNT"));
    obj.setNameMatchScore(rs.getDouble("NAME_MATCH_SCORE"));
    obj.setBrpermanantAddrScore(rs.getDouble("BRPERMANANT_ADDR_SCORE"));
    obj.setMobNonStaffMatchedCount(rs.getInt("MOB_NON_STAFF_MATCHED_COUNT"));
    obj.setPermanantAddrScore(rs.getDouble("PERMANANT_ADDR_SCORE"));
    obj.setHandPhone(rs.getString("HAND_PHONE"));
    obj.setPanMatchedCount(rs.getInt("PAN_MATCHED_COUNT"));
    obj.setCompletePerAddrChanged(rs.getString("COMPLETE_PER_ADDR_CHANGED"));
    obj.setNameMatchedlist(rs.getString("NAME_MATCHEDLIST"));
    obj.setEmailMatchedCount(rs.getInt("EMAIL_MATCHED_COUNT"));
    obj.setPassportNumber(rs.getString("PASSPORT_NUMBER"));
    obj.setScorePercentage(rs.getDouble("SCORE_PERCENTAGE"));
    obj.setNameChanged(rs.getString("NAME_CHANGED"));
    obj.setDobCustId(rs.getString("DOB_CUST_ID"));
    obj.setScore(rs.getDouble("SCORE"));
    obj.setPermanentAddr1(rs.getString("PERMANENT_ADDR1"));
    obj.setPermanentAddr3(rs.getString("PERMANENT_ADDR3"));
    obj.setPermanentAddr2(rs.getString("PERMANENT_ADDR2"));
    obj.setIntermediateLandlineCount(rs.getInt("INTERMEDIATE_LANDLINE_COUNT"));
    obj.setMobNonstaffCustId(rs.getString("MOB_NONSTAFF_CUST_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setPassportMatchedCount(rs.getInt("PASSPORT_MATCHED_COUNT"));
    obj.setPan(rs.getString("PAN"));
    obj.setCompleteMailingAddr(rs.getString("COMPLETE_MAILING_ADDR"));
    obj.setPermanentaddrMatchedlist(rs.getString("PERMANENTADDR_MATCHEDLIST"));
    obj.setLandlineCustId(rs.getString("LANDLINE_CUST_ID"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setCustName(rs.getString("CUST_NAME"));
    obj.setCompletePermanentAddr(rs.getString("COMPLETE_PERMANENT_ADDR"));
    obj.setMobMatchedCount(rs.getInt("MOB_MATCHED_COUNT"));
    obj.setDobMatchedCount(rs.getInt("DOB_MATCHED_COUNT"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCompleteMailingAddrChanged(rs.getString("COMPLETE_MAILING_ADDR_CHANGED"));
    obj.setBrcompleteMailingAddrCount(rs.getInt("BRCOMPLETE_MAILING_ADDR_COUNT"));
    obj.setEmailId(rs.getString("EMAIL_ID"));
    obj.setMobCustId(rs.getString("MOB_CUST_ID"));
    obj.setOfficeLandlineNumber(rs.getString("OFFICE_LANDLINE_NUMBER"));
    obj.setBrmailingAddrScore(rs.getDouble("BRMAILING_ADDR_SCORE"));
    obj.setDob(rs.getTimestamp("DOB"));
    obj.setMatchPhoneCustId(rs.getString("MATCH_PHONE_CUST_ID"));
    obj.setLandlineMatchedCount(rs.getInt("LANDLINE_MATCHED_COUNT"));
    obj.setProductCode(rs.getString("PRODUCT_CODE"));
    obj.setMobStaffCustId(rs.getString("MOB_STAFF_CUST_ID"));
    obj.setPanCustId(rs.getString("PAN_CUST_ID"));
    obj.setDoi(rs.getTimestamp("DOI"));
    obj.setEntityType(rs.getString("ENTITY_TYPE"));
    obj.setMobNumber(rs.getString("MOB_NUMBER"));
    obj.setPassportCustId(rs.getString("PASSPORT_CUST_ID"));
    obj.setMailingAddr3(rs.getString("MAILING_ADDR3"));
    obj.setOfficephoneMatchedCount(rs.getInt("OFFICEPHONE_MATCHED_COUNT"));
    obj.setDoiCustId(rs.getString("DOI_CUST_ID"));
    obj.setMailingAddrScore(rs.getDouble("MAILING_ADDR_SCORE"));
    obj.setHomeLandlineNumber(rs.getString("HOME_LANDLINE_NUMBER"));
    obj.setCompletePerAddrCount(rs.getInt("COMPLETE_PER_ADDR_COUNT"));
    obj.setIntermediatehandphoneCount(rs.getInt("INTERMEDIATEHANDPHONE_COUNT"));
    obj.setIntermediateofficelandCount(rs.getInt("INTERMEDIATEOFFICELAND_COUNT"));
    obj.setMobCount(rs.getInt("MOB_COUNT"));
    obj.setBranchaddrMatchedlist(rs.getString("BRANCHADDR_MATCHEDLIST"));
    obj.setCompleteMailingAddrCount(rs.getInt("COMPLETE_MAILING_ADDR_COUNT"));
    obj.setPhoneCount(rs.getInt("PHONE_COUNT"));
    obj.setIsAccountClosed(rs.getString("IS_ACCOUNT_CLOSED"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setDoiMatchedCount(rs.getInt("DOI_MATCHED_COUNT"));
    obj.setEmailCustId(rs.getString("EMAIL_CUST_ID"));
    obj.setOfficePhoneCustId(rs.getString("OFFICE_PHONE_CUST_ID"));
    obj.setBrcompletePerAddrCount(rs.getInt("BRCOMPLETE_PER_ADDR_COUNT"));
    obj.setMailingaddrMatchedlist(rs.getString("MAILINGADDR_MATCHEDLIST"));
    obj.setMailingAddr2(rs.getString("MAILING_ADDR2"));
    obj.setMailingAddr1(rs.getString("MAILING_ADDR1"));
    obj.setUcicId(rs.getString("UCIC_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_DEDUP]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_DedupEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_DedupEvent> events;
 NFT_DedupEvent obj = new NFT_DedupEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DEDUP"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_DedupEvent obj = new NFT_DedupEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setLandlineNumber(getColumnValue(rs, "LANDLINE_NUMBER"));
            obj.setAccountCount(EventHelper.toInt(getColumnValue(rs, "ACCOUNT_COUNT")));
            obj.setNameMatchScore(EventHelper.toDouble(getColumnValue(rs, "NAME_MATCH_SCORE")));
            obj.setBrpermanantAddrScore(EventHelper.toDouble(getColumnValue(rs, "BRPERMANANT_ADDR_SCORE")));
            obj.setMobNonStaffMatchedCount(EventHelper.toInt(getColumnValue(rs, "MOB_NON_STAFF_MATCHED_COUNT")));
            obj.setPermanantAddrScore(EventHelper.toDouble(getColumnValue(rs, "PERMANANT_ADDR_SCORE")));
            obj.setHandPhone(getColumnValue(rs, "HAND_PHONE"));
            obj.setPanMatchedCount(EventHelper.toInt(getColumnValue(rs, "PAN_MATCHED_COUNT")));
            obj.setCompletePerAddrChanged(getColumnValue(rs, "COMPLETE_PER_ADDR_CHANGED"));
            obj.setNameMatchedlist(getColumnValue(rs, "NAME_MATCHEDLIST"));
            obj.setEmailMatchedCount(EventHelper.toInt(getColumnValue(rs, "EMAIL_MATCHED_COUNT")));
            obj.setPassportNumber(getColumnValue(rs, "PASSPORT_NUMBER"));
            obj.setScorePercentage(EventHelper.toDouble(getColumnValue(rs, "SCORE_PERCENTAGE")));
            obj.setNameChanged(getColumnValue(rs, "NAME_CHANGED"));
            obj.setDobCustId(getColumnValue(rs, "DOB_CUST_ID"));
            obj.setScore(EventHelper.toDouble(getColumnValue(rs, "SCORE")));
            obj.setPermanentAddr1(getColumnValue(rs, "PERMANENT_ADDR1"));
            obj.setPermanentAddr3(getColumnValue(rs, "PERMANENT_ADDR3"));
            obj.setPermanentAddr2(getColumnValue(rs, "PERMANENT_ADDR2"));
            obj.setIntermediateLandlineCount(EventHelper.toInt(getColumnValue(rs, "INTERMEDIATE_LANDLINE_COUNT")));
            obj.setMobNonstaffCustId(getColumnValue(rs, "MOB_NONSTAFF_CUST_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setPassportMatchedCount(EventHelper.toInt(getColumnValue(rs, "PASSPORT_MATCHED_COUNT")));
            obj.setPan(getColumnValue(rs, "PAN"));
            obj.setCompleteMailingAddr(getColumnValue(rs, "COMPLETE_MAILING_ADDR"));
            obj.setPermanentaddrMatchedlist(getColumnValue(rs, "PERMANENTADDR_MATCHEDLIST"));
            obj.setLandlineCustId(getColumnValue(rs, "LANDLINE_CUST_ID"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setCustName(getColumnValue(rs, "CUST_NAME"));
            obj.setCompletePermanentAddr(getColumnValue(rs, "COMPLETE_PERMANENT_ADDR"));
            obj.setMobMatchedCount(EventHelper.toInt(getColumnValue(rs, "MOB_MATCHED_COUNT")));
            obj.setDobMatchedCount(EventHelper.toInt(getColumnValue(rs, "DOB_MATCHED_COUNT")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCompleteMailingAddrChanged(getColumnValue(rs, "COMPLETE_MAILING_ADDR_CHANGED"));
            obj.setBrcompleteMailingAddrCount(EventHelper.toInt(getColumnValue(rs, "BRCOMPLETE_MAILING_ADDR_COUNT")));
            obj.setEmailId(getColumnValue(rs, "EMAIL_ID"));
            obj.setMobCustId(getColumnValue(rs, "MOB_CUST_ID"));
            obj.setOfficeLandlineNumber(getColumnValue(rs, "OFFICE_LANDLINE_NUMBER"));
            obj.setBrmailingAddrScore(EventHelper.toDouble(getColumnValue(rs, "BRMAILING_ADDR_SCORE")));
            obj.setDob(EventHelper.toTimestamp(getColumnValue(rs, "DOB")));
            obj.setMatchPhoneCustId(getColumnValue(rs, "MATCH_PHONE_CUST_ID"));
            obj.setLandlineMatchedCount(EventHelper.toInt(getColumnValue(rs, "LANDLINE_MATCHED_COUNT")));
            obj.setProductCode(getColumnValue(rs, "PRODUCT_CODE"));
            obj.setMobStaffCustId(getColumnValue(rs, "MOB_STAFF_CUST_ID"));
            obj.setPanCustId(getColumnValue(rs, "PAN_CUST_ID"));
            obj.setDoi(EventHelper.toTimestamp(getColumnValue(rs, "DOI")));
            obj.setEntityType(getColumnValue(rs, "ENTITY_TYPE"));
            obj.setMobNumber(getColumnValue(rs, "MOB_NUMBER"));
            obj.setPassportCustId(getColumnValue(rs, "PASSPORT_CUST_ID"));
            obj.setMailingAddr3(getColumnValue(rs, "MAILING_ADDR3"));
            obj.setOfficephoneMatchedCount(EventHelper.toInt(getColumnValue(rs, "OFFICEPHONE_MATCHED_COUNT")));
            obj.setDoiCustId(getColumnValue(rs, "DOI_CUST_ID"));
            obj.setMailingAddrScore(EventHelper.toDouble(getColumnValue(rs, "MAILING_ADDR_SCORE")));
            obj.setHomeLandlineNumber(getColumnValue(rs, "HOME_LANDLINE_NUMBER"));
            obj.setCompletePerAddrCount(EventHelper.toInt(getColumnValue(rs, "COMPLETE_PER_ADDR_COUNT")));
            obj.setIntermediatehandphoneCount(EventHelper.toInt(getColumnValue(rs, "INTERMEDIATEHANDPHONE_COUNT")));
            obj.setIntermediateofficelandCount(EventHelper.toInt(getColumnValue(rs, "INTERMEDIATEOFFICELAND_COUNT")));
            obj.setMobCount(EventHelper.toInt(getColumnValue(rs, "MOB_COUNT")));
            obj.setBranchaddrMatchedlist(getColumnValue(rs, "BRANCHADDR_MATCHEDLIST"));
            obj.setCompleteMailingAddrCount(EventHelper.toInt(getColumnValue(rs, "COMPLETE_MAILING_ADDR_COUNT")));
            obj.setPhoneCount(EventHelper.toInt(getColumnValue(rs, "PHONE_COUNT")));
            obj.setIsAccountClosed(getColumnValue(rs, "IS_ACCOUNT_CLOSED"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setDoiMatchedCount(EventHelper.toInt(getColumnValue(rs, "DOI_MATCHED_COUNT")));
            obj.setEmailCustId(getColumnValue(rs, "EMAIL_CUST_ID"));
            obj.setOfficePhoneCustId(getColumnValue(rs, "OFFICE_PHONE_CUST_ID"));
            obj.setBrcompletePerAddrCount(EventHelper.toInt(getColumnValue(rs, "BRCOMPLETE_PER_ADDR_COUNT")));
            obj.setMailingaddrMatchedlist(getColumnValue(rs, "MAILINGADDR_MATCHEDLIST"));
            obj.setMailingAddr2(getColumnValue(rs, "MAILING_ADDR2"));
            obj.setMailingAddr1(getColumnValue(rs, "MAILING_ADDR1"));
            obj.setUcicId(getColumnValue(rs, "UCIC_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DEDUP]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DEDUP]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"LANDLINE_NUMBER\",\"ACCOUNT_COUNT\",\"NAME_MATCH_SCORE\",\"BRPERMANANT_ADDR_SCORE\",\"MOB_NON_STAFF_MATCHED_COUNT\",\"PERMANANT_ADDR_SCORE\",\"HAND_PHONE\",\"PAN_MATCHED_COUNT\",\"COMPLETE_PER_ADDR_CHANGED\",\"NAME_MATCHEDLIST\",\"EMAIL_MATCHED_COUNT\",\"PASSPORT_NUMBER\",\"SCORE_PERCENTAGE\",\"NAME_CHANGED\",\"DOB_CUST_ID\",\"SCORE\",\"PERMANENT_ADDR1\",\"PERMANENT_ADDR3\",\"PERMANENT_ADDR2\",\"INTERMEDIATE_LANDLINE_COUNT\",\"MOB_NONSTAFF_CUST_ID\",\"HOST_ID\",\"PASSPORT_MATCHED_COUNT\",\"PAN\",\"COMPLETE_MAILING_ADDR\",\"PERMANENTADDR_MATCHEDLIST\",\"LANDLINE_CUST_ID\",\"ACCOUNT_ID\",\"CUST_NAME\",\"COMPLETE_PERMANENT_ADDR\",\"MOB_MATCHED_COUNT\",\"DOB_MATCHED_COUNT\",\"SYS_TIME\",\"COMPLETE_MAILING_ADDR_CHANGED\",\"BRCOMPLETE_MAILING_ADDR_COUNT\",\"EMAIL_ID\",\"MOB_CUST_ID\",\"OFFICE_LANDLINE_NUMBER\",\"BRMAILING_ADDR_SCORE\",\"DOB\",\"MATCH_PHONE_CUST_ID\",\"LANDLINE_MATCHED_COUNT\",\"PRODUCT_CODE\",\"MOB_STAFF_CUST_ID\",\"PAN_CUST_ID\",\"DOI\",\"ENTITY_TYPE\",\"MOB_NUMBER\",\"PASSPORT_CUST_ID\",\"MAILING_ADDR3\",\"OFFICEPHONE_MATCHED_COUNT\",\"DOI_CUST_ID\",\"MAILING_ADDR_SCORE\",\"HOME_LANDLINE_NUMBER\",\"COMPLETE_PER_ADDR_COUNT\",\"INTERMEDIATEHANDPHONE_COUNT\",\"INTERMEDIATEOFFICELAND_COUNT\",\"MOB_COUNT\",\"BRANCHADDR_MATCHEDLIST\",\"COMPLETE_MAILING_ADDR_COUNT\",\"PHONE_COUNT\",\"IS_ACCOUNT_CLOSED\",\"CUST_ID\",\"DOI_MATCHED_COUNT\",\"EMAIL_CUST_ID\",\"OFFICE_PHONE_CUST_ID\",\"BRCOMPLETE_PER_ADDR_COUNT\",\"MAILINGADDR_MATCHEDLIST\",\"MAILING_ADDR2\",\"MAILING_ADDR1\",\"UCIC_ID\"" +
              " FROM EVENT_NFT_DEDUP";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [LANDLINE_NUMBER],[ACCOUNT_COUNT],[NAME_MATCH_SCORE],[BRPERMANANT_ADDR_SCORE],[MOB_NON_STAFF_MATCHED_COUNT],[PERMANANT_ADDR_SCORE],[HAND_PHONE],[PAN_MATCHED_COUNT],[COMPLETE_PER_ADDR_CHANGED],[NAME_MATCHEDLIST],[EMAIL_MATCHED_COUNT],[PASSPORT_NUMBER],[SCORE_PERCENTAGE],[NAME_CHANGED],[DOB_CUST_ID],[SCORE],[PERMANENT_ADDR1],[PERMANENT_ADDR3],[PERMANENT_ADDR2],[INTERMEDIATE_LANDLINE_COUNT],[MOB_NONSTAFF_CUST_ID],[HOST_ID],[PASSPORT_MATCHED_COUNT],[PAN],[COMPLETE_MAILING_ADDR],[PERMANENTADDR_MATCHEDLIST],[LANDLINE_CUST_ID],[ACCOUNT_ID],[CUST_NAME],[COMPLETE_PERMANENT_ADDR],[MOB_MATCHED_COUNT],[DOB_MATCHED_COUNT],[SYS_TIME],[COMPLETE_MAILING_ADDR_CHANGED],[BRCOMPLETE_MAILING_ADDR_COUNT],[EMAIL_ID],[MOB_CUST_ID],[OFFICE_LANDLINE_NUMBER],[BRMAILING_ADDR_SCORE],[DOB],[MATCH_PHONE_CUST_ID],[LANDLINE_MATCHED_COUNT],[PRODUCT_CODE],[MOB_STAFF_CUST_ID],[PAN_CUST_ID],[DOI],[ENTITY_TYPE],[MOB_NUMBER],[PASSPORT_CUST_ID],[MAILING_ADDR3],[OFFICEPHONE_MATCHED_COUNT],[DOI_CUST_ID],[MAILING_ADDR_SCORE],[HOME_LANDLINE_NUMBER],[COMPLETE_PER_ADDR_COUNT],[INTERMEDIATEHANDPHONE_COUNT],[INTERMEDIATEOFFICELAND_COUNT],[MOB_COUNT],[BRANCHADDR_MATCHEDLIST],[COMPLETE_MAILING_ADDR_COUNT],[PHONE_COUNT],[IS_ACCOUNT_CLOSED],[CUST_ID],[DOI_MATCHED_COUNT],[EMAIL_CUST_ID],[OFFICE_PHONE_CUST_ID],[BRCOMPLETE_PER_ADDR_COUNT],[MAILINGADDR_MATCHEDLIST],[MAILING_ADDR2],[MAILING_ADDR1],[UCIC_ID]" +
              " FROM EVENT_NFT_DEDUP";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`LANDLINE_NUMBER`,`ACCOUNT_COUNT`,`NAME_MATCH_SCORE`,`BRPERMANANT_ADDR_SCORE`,`MOB_NON_STAFF_MATCHED_COUNT`,`PERMANANT_ADDR_SCORE`,`HAND_PHONE`,`PAN_MATCHED_COUNT`,`COMPLETE_PER_ADDR_CHANGED`,`NAME_MATCHEDLIST`,`EMAIL_MATCHED_COUNT`,`PASSPORT_NUMBER`,`SCORE_PERCENTAGE`,`NAME_CHANGED`,`DOB_CUST_ID`,`SCORE`,`PERMANENT_ADDR1`,`PERMANENT_ADDR3`,`PERMANENT_ADDR2`,`INTERMEDIATE_LANDLINE_COUNT`,`MOB_NONSTAFF_CUST_ID`,`HOST_ID`,`PASSPORT_MATCHED_COUNT`,`PAN`,`COMPLETE_MAILING_ADDR`,`PERMANENTADDR_MATCHEDLIST`,`LANDLINE_CUST_ID`,`ACCOUNT_ID`,`CUST_NAME`,`COMPLETE_PERMANENT_ADDR`,`MOB_MATCHED_COUNT`,`DOB_MATCHED_COUNT`,`SYS_TIME`,`COMPLETE_MAILING_ADDR_CHANGED`,`BRCOMPLETE_MAILING_ADDR_COUNT`,`EMAIL_ID`,`MOB_CUST_ID`,`OFFICE_LANDLINE_NUMBER`,`BRMAILING_ADDR_SCORE`,`DOB`,`MATCH_PHONE_CUST_ID`,`LANDLINE_MATCHED_COUNT`,`PRODUCT_CODE`,`MOB_STAFF_CUST_ID`,`PAN_CUST_ID`,`DOI`,`ENTITY_TYPE`,`MOB_NUMBER`,`PASSPORT_CUST_ID`,`MAILING_ADDR3`,`OFFICEPHONE_MATCHED_COUNT`,`DOI_CUST_ID`,`MAILING_ADDR_SCORE`,`HOME_LANDLINE_NUMBER`,`COMPLETE_PER_ADDR_COUNT`,`INTERMEDIATEHANDPHONE_COUNT`,`INTERMEDIATEOFFICELAND_COUNT`,`MOB_COUNT`,`BRANCHADDR_MATCHEDLIST`,`COMPLETE_MAILING_ADDR_COUNT`,`PHONE_COUNT`,`IS_ACCOUNT_CLOSED`,`CUST_ID`,`DOI_MATCHED_COUNT`,`EMAIL_CUST_ID`,`OFFICE_PHONE_CUST_ID`,`BRCOMPLETE_PER_ADDR_COUNT`,`MAILINGADDR_MATCHEDLIST`,`MAILING_ADDR2`,`MAILING_ADDR1`,`UCIC_ID`" +
              " FROM EVENT_NFT_DEDUP";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_DEDUP (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"LANDLINE_NUMBER\",\"ACCOUNT_COUNT\",\"NAME_MATCH_SCORE\",\"BRPERMANANT_ADDR_SCORE\",\"MOB_NON_STAFF_MATCHED_COUNT\",\"PERMANANT_ADDR_SCORE\",\"HAND_PHONE\",\"PAN_MATCHED_COUNT\",\"COMPLETE_PER_ADDR_CHANGED\",\"NAME_MATCHEDLIST\",\"EMAIL_MATCHED_COUNT\",\"PASSPORT_NUMBER\",\"SCORE_PERCENTAGE\",\"NAME_CHANGED\",\"DOB_CUST_ID\",\"SCORE\",\"PERMANENT_ADDR1\",\"PERMANENT_ADDR3\",\"PERMANENT_ADDR2\",\"INTERMEDIATE_LANDLINE_COUNT\",\"MOB_NONSTAFF_CUST_ID\",\"HOST_ID\",\"PASSPORT_MATCHED_COUNT\",\"PAN\",\"COMPLETE_MAILING_ADDR\",\"PERMANENTADDR_MATCHEDLIST\",\"LANDLINE_CUST_ID\",\"ACCOUNT_ID\",\"CUST_NAME\",\"COMPLETE_PERMANENT_ADDR\",\"MOB_MATCHED_COUNT\",\"DOB_MATCHED_COUNT\",\"SYS_TIME\",\"COMPLETE_MAILING_ADDR_CHANGED\",\"BRCOMPLETE_MAILING_ADDR_COUNT\",\"EMAIL_ID\",\"MOB_CUST_ID\",\"OFFICE_LANDLINE_NUMBER\",\"BRMAILING_ADDR_SCORE\",\"DOB\",\"MATCH_PHONE_CUST_ID\",\"LANDLINE_MATCHED_COUNT\",\"PRODUCT_CODE\",\"MOB_STAFF_CUST_ID\",\"PAN_CUST_ID\",\"DOI\",\"ENTITY_TYPE\",\"MOB_NUMBER\",\"PASSPORT_CUST_ID\",\"MAILING_ADDR3\",\"OFFICEPHONE_MATCHED_COUNT\",\"DOI_CUST_ID\",\"MAILING_ADDR_SCORE\",\"HOME_LANDLINE_NUMBER\",\"COMPLETE_PER_ADDR_COUNT\",\"INTERMEDIATEHANDPHONE_COUNT\",\"INTERMEDIATEOFFICELAND_COUNT\",\"MOB_COUNT\",\"BRANCHADDR_MATCHEDLIST\",\"COMPLETE_MAILING_ADDR_COUNT\",\"PHONE_COUNT\",\"IS_ACCOUNT_CLOSED\",\"CUST_ID\",\"DOI_MATCHED_COUNT\",\"EMAIL_CUST_ID\",\"OFFICE_PHONE_CUST_ID\",\"BRCOMPLETE_PER_ADDR_COUNT\",\"MAILINGADDR_MATCHEDLIST\",\"MAILING_ADDR2\",\"MAILING_ADDR1\",\"UCIC_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[LANDLINE_NUMBER],[ACCOUNT_COUNT],[NAME_MATCH_SCORE],[BRPERMANANT_ADDR_SCORE],[MOB_NON_STAFF_MATCHED_COUNT],[PERMANANT_ADDR_SCORE],[HAND_PHONE],[PAN_MATCHED_COUNT],[COMPLETE_PER_ADDR_CHANGED],[NAME_MATCHEDLIST],[EMAIL_MATCHED_COUNT],[PASSPORT_NUMBER],[SCORE_PERCENTAGE],[NAME_CHANGED],[DOB_CUST_ID],[SCORE],[PERMANENT_ADDR1],[PERMANENT_ADDR3],[PERMANENT_ADDR2],[INTERMEDIATE_LANDLINE_COUNT],[MOB_NONSTAFF_CUST_ID],[HOST_ID],[PASSPORT_MATCHED_COUNT],[PAN],[COMPLETE_MAILING_ADDR],[PERMANENTADDR_MATCHEDLIST],[LANDLINE_CUST_ID],[ACCOUNT_ID],[CUST_NAME],[COMPLETE_PERMANENT_ADDR],[MOB_MATCHED_COUNT],[DOB_MATCHED_COUNT],[SYS_TIME],[COMPLETE_MAILING_ADDR_CHANGED],[BRCOMPLETE_MAILING_ADDR_COUNT],[EMAIL_ID],[MOB_CUST_ID],[OFFICE_LANDLINE_NUMBER],[BRMAILING_ADDR_SCORE],[DOB],[MATCH_PHONE_CUST_ID],[LANDLINE_MATCHED_COUNT],[PRODUCT_CODE],[MOB_STAFF_CUST_ID],[PAN_CUST_ID],[DOI],[ENTITY_TYPE],[MOB_NUMBER],[PASSPORT_CUST_ID],[MAILING_ADDR3],[OFFICEPHONE_MATCHED_COUNT],[DOI_CUST_ID],[MAILING_ADDR_SCORE],[HOME_LANDLINE_NUMBER],[COMPLETE_PER_ADDR_COUNT],[INTERMEDIATEHANDPHONE_COUNT],[INTERMEDIATEOFFICELAND_COUNT],[MOB_COUNT],[BRANCHADDR_MATCHEDLIST],[COMPLETE_MAILING_ADDR_COUNT],[PHONE_COUNT],[IS_ACCOUNT_CLOSED],[CUST_ID],[DOI_MATCHED_COUNT],[EMAIL_CUST_ID],[OFFICE_PHONE_CUST_ID],[BRCOMPLETE_PER_ADDR_COUNT],[MAILINGADDR_MATCHEDLIST],[MAILING_ADDR2],[MAILING_ADDR1],[UCIC_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`LANDLINE_NUMBER`,`ACCOUNT_COUNT`,`NAME_MATCH_SCORE`,`BRPERMANANT_ADDR_SCORE`,`MOB_NON_STAFF_MATCHED_COUNT`,`PERMANANT_ADDR_SCORE`,`HAND_PHONE`,`PAN_MATCHED_COUNT`,`COMPLETE_PER_ADDR_CHANGED`,`NAME_MATCHEDLIST`,`EMAIL_MATCHED_COUNT`,`PASSPORT_NUMBER`,`SCORE_PERCENTAGE`,`NAME_CHANGED`,`DOB_CUST_ID`,`SCORE`,`PERMANENT_ADDR1`,`PERMANENT_ADDR3`,`PERMANENT_ADDR2`,`INTERMEDIATE_LANDLINE_COUNT`,`MOB_NONSTAFF_CUST_ID`,`HOST_ID`,`PASSPORT_MATCHED_COUNT`,`PAN`,`COMPLETE_MAILING_ADDR`,`PERMANENTADDR_MATCHEDLIST`,`LANDLINE_CUST_ID`,`ACCOUNT_ID`,`CUST_NAME`,`COMPLETE_PERMANENT_ADDR`,`MOB_MATCHED_COUNT`,`DOB_MATCHED_COUNT`,`SYS_TIME`,`COMPLETE_MAILING_ADDR_CHANGED`,`BRCOMPLETE_MAILING_ADDR_COUNT`,`EMAIL_ID`,`MOB_CUST_ID`,`OFFICE_LANDLINE_NUMBER`,`BRMAILING_ADDR_SCORE`,`DOB`,`MATCH_PHONE_CUST_ID`,`LANDLINE_MATCHED_COUNT`,`PRODUCT_CODE`,`MOB_STAFF_CUST_ID`,`PAN_CUST_ID`,`DOI`,`ENTITY_TYPE`,`MOB_NUMBER`,`PASSPORT_CUST_ID`,`MAILING_ADDR3`,`OFFICEPHONE_MATCHED_COUNT`,`DOI_CUST_ID`,`MAILING_ADDR_SCORE`,`HOME_LANDLINE_NUMBER`,`COMPLETE_PER_ADDR_COUNT`,`INTERMEDIATEHANDPHONE_COUNT`,`INTERMEDIATEOFFICELAND_COUNT`,`MOB_COUNT`,`BRANCHADDR_MATCHEDLIST`,`COMPLETE_MAILING_ADDR_COUNT`,`PHONE_COUNT`,`IS_ACCOUNT_CLOSED`,`CUST_ID`,`DOI_MATCHED_COUNT`,`EMAIL_CUST_ID`,`OFFICE_PHONE_CUST_ID`,`BRCOMPLETE_PER_ADDR_COUNT`,`MAILINGADDR_MATCHEDLIST`,`MAILING_ADDR2`,`MAILING_ADDR1`,`UCIC_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

