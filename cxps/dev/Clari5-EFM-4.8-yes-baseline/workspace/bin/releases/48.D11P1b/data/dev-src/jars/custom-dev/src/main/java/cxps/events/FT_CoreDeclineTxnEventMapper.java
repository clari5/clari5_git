// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_CoreDeclineTxnEventMapper extends EventMapper<FT_CoreDeclineTxnEvent> {

public FT_CoreDeclineTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_CoreDeclineTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_CoreDeclineTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_CoreDeclineTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_CoreDeclineTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_CoreDeclineTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_CoreDeclineTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setDouble(i++, obj.getTodGrantAmount());
            preparedStatement.setTimestamp(i++, obj.getValueDate());
            preparedStatement.setString(i++, obj.getBenCity());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setDouble(i++, obj.getTranAmount());
            preparedStatement.setString(i++, obj.getBranch());
            preparedStatement.setString(i++, obj.getBenCntryCode());
            preparedStatement.setString(i++, obj.getRefTranCrncy());
            preparedStatement.setString(i++, obj.getPurposeDesc());
            preparedStatement.setString(i++, obj.getAccountType());
            preparedStatement.setString(i++, obj.getRemName());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getBenBic());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getProductDesc());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setTimestamp(i++, obj.getTdMaturityDate());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getBenAcctNo());
            preparedStatement.setString(i++, obj.getBenCustId());
            preparedStatement.setTimestamp(i++, obj.getPstdDate());
            preparedStatement.setString(i++, obj.getSystem());
            preparedStatement.setString(i++, obj.getAtmId());
            preparedStatement.setString(i++, obj.getProductCode());
            preparedStatement.setString(i++, obj.getDrCr());
            preparedStatement.setString(i++, obj.getClientAccNo());
            preparedStatement.setString(i++, obj.getRemBic());
            preparedStatement.setString(i++, obj.getPstdFlg());
            preparedStatement.setString(i++, obj.getStatus());
            preparedStatement.setString(i++, obj.getHostType());
            preparedStatement.setString(i++, obj.getRemAdd1());
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setString(i++, obj.getNumericCode());
            preparedStatement.setString(i++, obj.getRemType());
            preparedStatement.setString(i++, obj.getRemAdd3());
            preparedStatement.setDouble(i++, obj.getInrAmount());
            preparedStatement.setString(i++, obj.getRemAdd2());
            preparedStatement.setTimestamp(i++, obj.getTrnDate());
            preparedStatement.setString(i++, obj.getPurposeCode());
            preparedStatement.setString(i++, obj.getChannelId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getBenAdd3());
            preparedStatement.setString(i++, obj.getCaSchemeCode());
            preparedStatement.setString(i++, obj.getRate());
            preparedStatement.setString(i++, obj.getBenAdd2());
            preparedStatement.setString(i++, obj.getRemAcctNo());
            preparedStatement.setString(i++, obj.getBenAdd1());
            preparedStatement.setString(i++, obj.getRemCustId());
            preparedStatement.setString(i++, obj.getInstrumentNumber());
            preparedStatement.setString(i++, obj.getTranNumonicCode());
            preparedStatement.setString(i++, obj.getBenName());
            preparedStatement.setString(i++, obj.getIsinNumber());
            preparedStatement.setString(i++, obj.getPartTranSrlNum());
            preparedStatement.setString(i++, obj.getTranParticular());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getBankCode());
            preparedStatement.setDouble(i++, obj.getUsdEqvAmt());
            preparedStatement.setString(i++, obj.getTdLiquidationType());
            preparedStatement.setString(i++, obj.getRemCntryCode());
            preparedStatement.setString(i++, obj.getCptyAcNo());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getTranCurr());
            preparedStatement.setString(i++, obj.getInstrumentType());
            preparedStatement.setDouble(i++, obj.getRefTranAmt());
            preparedStatement.setString(i++, obj.getRemCity());
            preparedStatement.setDouble(i++, obj.getEffAvailableBalance());
            preparedStatement.setString(i++, obj.getUcicId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_COREDECLINE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_CoreDeclineTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_CoreDeclineTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_COREDECLINE"));
        putList = new ArrayList<>();

        for (FT_CoreDeclineTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TOD_GRANT_AMOUNT", String.valueOf(obj.getTodGrantAmount()));
            p = this.insert(p, "VALUE_DATE", String.valueOf(obj.getValueDate()));
            p = this.insert(p, "BEN_CITY",  obj.getBenCity());
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TRAN_AMOUNT", String.valueOf(obj.getTranAmount()));
            p = this.insert(p, "BRANCH",  obj.getBranch());
            p = this.insert(p, "BEN_CNTRY_CODE",  obj.getBenCntryCode());
            p = this.insert(p, "REF_TRAN_CRNCY",  obj.getRefTranCrncy());
            p = this.insert(p, "PURPOSE_DESC",  obj.getPurposeDesc());
            p = this.insert(p, "ACCOUNT_TYPE",  obj.getAccountType());
            p = this.insert(p, "REM_NAME",  obj.getRemName());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "BEN_BIC",  obj.getBenBic());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "PRODUCT_DESC",  obj.getProductDesc());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TD_MATURITY_DATE", String.valueOf(obj.getTdMaturityDate()));
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "BEN_ACCT_NO",  obj.getBenAcctNo());
            p = this.insert(p, "BEN_CUST_ID",  obj.getBenCustId());
            p = this.insert(p, "PSTD_DATE", String.valueOf(obj.getPstdDate()));
            p = this.insert(p, "SYSTEM",  obj.getSystem());
            p = this.insert(p, "ATM_ID",  obj.getAtmId());
            p = this.insert(p, "PRODUCT_CODE",  obj.getProductCode());
            p = this.insert(p, "DR_CR",  obj.getDrCr());
            p = this.insert(p, "CLIENT_ACC_NO",  obj.getClientAccNo());
            p = this.insert(p, "REM_BIC",  obj.getRemBic());
            p = this.insert(p, "PSTD_FLG",  obj.getPstdFlg());
            p = this.insert(p, "STATUS",  obj.getStatus());
            p = this.insert(p, "HOST_TYPE",  obj.getHostType());
            p = this.insert(p, "REM_ADD1",  obj.getRemAdd1());
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "NUMERIC_CODE",  obj.getNumericCode());
            p = this.insert(p, "REM_TYPE",  obj.getRemType());
            p = this.insert(p, "REM_ADD3",  obj.getRemAdd3());
            p = this.insert(p, "INR_AMOUNT", String.valueOf(obj.getInrAmount()));
            p = this.insert(p, "REM_ADD2",  obj.getRemAdd2());
            p = this.insert(p, "TRN_DATE", String.valueOf(obj.getTrnDate()));
            p = this.insert(p, "PURPOSE_CODE",  obj.getPurposeCode());
            p = this.insert(p, "CHANNEL_ID",  obj.getChannelId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "BEN_ADD3",  obj.getBenAdd3());
            p = this.insert(p, "CA_SCHEME_CODE",  obj.getCaSchemeCode());
            p = this.insert(p, "RATE",  obj.getRate());
            p = this.insert(p, "BEN_ADD2",  obj.getBenAdd2());
            p = this.insert(p, "REM_ACCT_NO",  obj.getRemAcctNo());
            p = this.insert(p, "BEN_ADD1",  obj.getBenAdd1());
            p = this.insert(p, "REM_CUST_ID",  obj.getRemCustId());
            p = this.insert(p, "INSTRUMENT_NUMBER",  obj.getInstrumentNumber());
            p = this.insert(p, "TRAN_NUMONIC_CODE",  obj.getTranNumonicCode());
            p = this.insert(p, "BEN_NAME",  obj.getBenName());
            p = this.insert(p, "ISIN_NUMBER",  obj.getIsinNumber());
            p = this.insert(p, "PART_TRAN_SRL_NUM",  obj.getPartTranSrlNum());
            p = this.insert(p, "TRAN_PARTICULAR",  obj.getTranParticular());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "BANK_CODE",  obj.getBankCode());
            p = this.insert(p, "USD_EQV_AMT", String.valueOf(obj.getUsdEqvAmt()));
            p = this.insert(p, "TD_LIQUIDATION_TYPE",  obj.getTdLiquidationType());
            p = this.insert(p, "REM_CNTRY_CODE",  obj.getRemCntryCode());
            p = this.insert(p, "CPTY_AC_NO",  obj.getCptyAcNo());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "TRAN_CURR",  obj.getTranCurr());
            p = this.insert(p, "INSTRUMENT_TYPE",  obj.getInstrumentType());
            p = this.insert(p, "REF_TRAN_AMT", String.valueOf(obj.getRefTranAmt()));
            p = this.insert(p, "REM_CITY",  obj.getRemCity());
            p = this.insert(p, "EFF_AVAILABLE_BALANCE", String.valueOf(obj.getEffAvailableBalance()));
            p = this.insert(p, "UCIC_ID",  obj.getUcicId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_COREDECLINE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_COREDECLINE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_COREDECLINE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_CoreDeclineTxnEvent obj = new FT_CoreDeclineTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTodGrantAmount(rs.getDouble("TOD_GRANT_AMOUNT"));
    obj.setValueDate(rs.getTimestamp("VALUE_DATE"));
    obj.setBenCity(rs.getString("BEN_CITY"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTranAmount(rs.getDouble("TRAN_AMOUNT"));
    obj.setBranch(rs.getString("BRANCH"));
    obj.setBenCntryCode(rs.getString("BEN_CNTRY_CODE"));
    obj.setRefTranCrncy(rs.getString("REF_TRAN_CRNCY"));
    obj.setPurposeDesc(rs.getString("PURPOSE_DESC"));
    obj.setAccountType(rs.getString("ACCOUNT_TYPE"));
    obj.setRemName(rs.getString("REM_NAME"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setBenBic(rs.getString("BEN_BIC"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setProductDesc(rs.getString("PRODUCT_DESC"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTdMaturityDate(rs.getTimestamp("TD_MATURITY_DATE"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setBenAcctNo(rs.getString("BEN_ACCT_NO"));
    obj.setBenCustId(rs.getString("BEN_CUST_ID"));
    obj.setPstdDate(rs.getTimestamp("PSTD_DATE"));
    obj.setSystem(rs.getString("SYSTEM"));
    obj.setAtmId(rs.getString("ATM_ID"));
    obj.setProductCode(rs.getString("PRODUCT_CODE"));
    obj.setDrCr(rs.getString("DR_CR"));
    obj.setClientAccNo(rs.getString("CLIENT_ACC_NO"));
    obj.setRemBic(rs.getString("REM_BIC"));
    obj.setPstdFlg(rs.getString("PSTD_FLG"));
    obj.setStatus(rs.getString("STATUS"));
    obj.setHostType(rs.getString("HOST_TYPE"));
    obj.setRemAdd1(rs.getString("REM_ADD1"));
    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setNumericCode(rs.getString("NUMERIC_CODE"));
    obj.setRemType(rs.getString("REM_TYPE"));
    obj.setRemAdd3(rs.getString("REM_ADD3"));
    obj.setInrAmount(rs.getDouble("INR_AMOUNT"));
    obj.setRemAdd2(rs.getString("REM_ADD2"));
    obj.setTrnDate(rs.getTimestamp("TRN_DATE"));
    obj.setPurposeCode(rs.getString("PURPOSE_CODE"));
    obj.setChannelId(rs.getString("CHANNEL_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setBenAdd3(rs.getString("BEN_ADD3"));
    obj.setCaSchemeCode(rs.getString("CA_SCHEME_CODE"));
    obj.setRate(rs.getString("RATE"));
    obj.setBenAdd2(rs.getString("BEN_ADD2"));
    obj.setRemAcctNo(rs.getString("REM_ACCT_NO"));
    obj.setBenAdd1(rs.getString("BEN_ADD1"));
    obj.setRemCustId(rs.getString("REM_CUST_ID"));
    obj.setInstrumentNumber(rs.getString("INSTRUMENT_NUMBER"));
    obj.setTranNumonicCode(rs.getString("TRAN_NUMONIC_CODE"));
    obj.setBenName(rs.getString("BEN_NAME"));
    obj.setIsinNumber(rs.getString("ISIN_NUMBER"));
    obj.setPartTranSrlNum(rs.getString("PART_TRAN_SRL_NUM"));
    obj.setTranParticular(rs.getString("TRAN_PARTICULAR"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setBankCode(rs.getString("BANK_CODE"));
    obj.setUsdEqvAmt(rs.getDouble("USD_EQV_AMT"));
    obj.setTdLiquidationType(rs.getString("TD_LIQUIDATION_TYPE"));
    obj.setRemCntryCode(rs.getString("REM_CNTRY_CODE"));
    obj.setCptyAcNo(rs.getString("CPTY_AC_NO"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setTranCurr(rs.getString("TRAN_CURR"));
    obj.setInstrumentType(rs.getString("INSTRUMENT_TYPE"));
    obj.setRefTranAmt(rs.getDouble("REF_TRAN_AMT"));
    obj.setRemCity(rs.getString("REM_CITY"));
    obj.setEffAvailableBalance(rs.getDouble("EFF_AVAILABLE_BALANCE"));
    obj.setUcicId(rs.getString("UCIC_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_COREDECLINE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_CoreDeclineTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_CoreDeclineTxnEvent> events;
 FT_CoreDeclineTxnEvent obj = new FT_CoreDeclineTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_COREDECLINE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_CoreDeclineTxnEvent obj = new FT_CoreDeclineTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTodGrantAmount(EventHelper.toDouble(getColumnValue(rs, "TOD_GRANT_AMOUNT")));
            obj.setValueDate(EventHelper.toTimestamp(getColumnValue(rs, "VALUE_DATE")));
            obj.setBenCity(getColumnValue(rs, "BEN_CITY"));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTranAmount(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMOUNT")));
            obj.setBranch(getColumnValue(rs, "BRANCH"));
            obj.setBenCntryCode(getColumnValue(rs, "BEN_CNTRY_CODE"));
            obj.setRefTranCrncy(getColumnValue(rs, "REF_TRAN_CRNCY"));
            obj.setPurposeDesc(getColumnValue(rs, "PURPOSE_DESC"));
            obj.setAccountType(getColumnValue(rs, "ACCOUNT_TYPE"));
            obj.setRemName(getColumnValue(rs, "REM_NAME"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setBenBic(getColumnValue(rs, "BEN_BIC"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setProductDesc(getColumnValue(rs, "PRODUCT_DESC"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTdMaturityDate(EventHelper.toTimestamp(getColumnValue(rs, "TD_MATURITY_DATE")));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setBenAcctNo(getColumnValue(rs, "BEN_ACCT_NO"));
            obj.setBenCustId(getColumnValue(rs, "BEN_CUST_ID"));
            obj.setPstdDate(EventHelper.toTimestamp(getColumnValue(rs, "PSTD_DATE")));
            obj.setSystem(getColumnValue(rs, "SYSTEM"));
            obj.setAtmId(getColumnValue(rs, "ATM_ID"));
            obj.setProductCode(getColumnValue(rs, "PRODUCT_CODE"));
            obj.setDrCr(getColumnValue(rs, "DR_CR"));
            obj.setClientAccNo(getColumnValue(rs, "CLIENT_ACC_NO"));
            obj.setRemBic(getColumnValue(rs, "REM_BIC"));
            obj.setPstdFlg(getColumnValue(rs, "PSTD_FLG"));
            obj.setStatus(getColumnValue(rs, "STATUS"));
            obj.setHostType(getColumnValue(rs, "HOST_TYPE"));
            obj.setRemAdd1(getColumnValue(rs, "REM_ADD1"));
            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setNumericCode(getColumnValue(rs, "NUMERIC_CODE"));
            obj.setRemType(getColumnValue(rs, "REM_TYPE"));
            obj.setRemAdd3(getColumnValue(rs, "REM_ADD3"));
            obj.setInrAmount(EventHelper.toDouble(getColumnValue(rs, "INR_AMOUNT")));
            obj.setRemAdd2(getColumnValue(rs, "REM_ADD2"));
            obj.setTrnDate(EventHelper.toTimestamp(getColumnValue(rs, "TRN_DATE")));
            obj.setPurposeCode(getColumnValue(rs, "PURPOSE_CODE"));
            obj.setChannelId(getColumnValue(rs, "CHANNEL_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setBenAdd3(getColumnValue(rs, "BEN_ADD3"));
            obj.setCaSchemeCode(getColumnValue(rs, "CA_SCHEME_CODE"));
            obj.setRate(getColumnValue(rs, "RATE"));
            obj.setBenAdd2(getColumnValue(rs, "BEN_ADD2"));
            obj.setRemAcctNo(getColumnValue(rs, "REM_ACCT_NO"));
            obj.setBenAdd1(getColumnValue(rs, "BEN_ADD1"));
            obj.setRemCustId(getColumnValue(rs, "REM_CUST_ID"));
            obj.setInstrumentNumber(getColumnValue(rs, "INSTRUMENT_NUMBER"));
            obj.setTranNumonicCode(getColumnValue(rs, "TRAN_NUMONIC_CODE"));
            obj.setBenName(getColumnValue(rs, "BEN_NAME"));
            obj.setIsinNumber(getColumnValue(rs, "ISIN_NUMBER"));
            obj.setPartTranSrlNum(getColumnValue(rs, "PART_TRAN_SRL_NUM"));
            obj.setTranParticular(getColumnValue(rs, "TRAN_PARTICULAR"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setBankCode(getColumnValue(rs, "BANK_CODE"));
            obj.setUsdEqvAmt(EventHelper.toDouble(getColumnValue(rs, "USD_EQV_AMT")));
            obj.setTdLiquidationType(getColumnValue(rs, "TD_LIQUIDATION_TYPE"));
            obj.setRemCntryCode(getColumnValue(rs, "REM_CNTRY_CODE"));
            obj.setCptyAcNo(getColumnValue(rs, "CPTY_AC_NO"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setTranCurr(getColumnValue(rs, "TRAN_CURR"));
            obj.setInstrumentType(getColumnValue(rs, "INSTRUMENT_TYPE"));
            obj.setRefTranAmt(EventHelper.toDouble(getColumnValue(rs, "REF_TRAN_AMT")));
            obj.setRemCity(getColumnValue(rs, "REM_CITY"));
            obj.setEffAvailableBalance(EventHelper.toDouble(getColumnValue(rs, "EFF_AVAILABLE_BALANCE")));
            obj.setUcicId(getColumnValue(rs, "UCIC_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_COREDECLINE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_COREDECLINE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TOD_GRANT_AMOUNT\",\"VALUE_DATE\",\"BEN_CITY\",\"ONLINE_BATCH\",\"TRAN_AMOUNT\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"REF_TRAN_CRNCY\",\"PURPOSE_DESC\",\"ACCOUNT_TYPE\",\"REM_NAME\",\"TRAN_ID\",\"BEN_BIC\",\"HOST_ID\",\"TRAN_AMT\",\"PRODUCT_DESC\",\"BRANCH_ID\",\"SYS_TIME\",\"TRAN_CRNCY_CODE\",\"TD_MATURITY_DATE\",\"ACCT_NAME\",\"BEN_ACCT_NO\",\"BEN_CUST_ID\",\"PSTD_DATE\",\"SYSTEM\",\"ATM_ID\",\"PRODUCT_CODE\",\"DR_CR\",\"CLIENT_ACC_NO\",\"REM_BIC\",\"PSTD_FLG\",\"STATUS\",\"HOST_TYPE\",\"REM_ADD1\",\"ACCT_ID\",\"NUMERIC_CODE\",\"REM_TYPE\",\"REM_ADD3\",\"INR_AMOUNT\",\"REM_ADD2\",\"TRN_DATE\",\"PURPOSE_CODE\",\"CHANNEL_ID\",\"USER_ID\",\"BEN_ADD3\",\"CA_SCHEME_CODE\",\"RATE\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"BEN_ADD1\",\"REM_CUST_ID\",\"INSTRUMENT_NUMBER\",\"TRAN_NUMONIC_CODE\",\"BEN_NAME\",\"ISIN_NUMBER\",\"PART_TRAN_SRL_NUM\",\"TRAN_PARTICULAR\",\"CUST_ID\",\"EVENTTS\",\"BANK_CODE\",\"USD_EQV_AMT\",\"TD_LIQUIDATION_TYPE\",\"REM_CNTRY_CODE\",\"CPTY_AC_NO\",\"TRAN_DATE\",\"TRAN_CURR\",\"INSTRUMENT_TYPE\",\"REF_TRAN_AMT\",\"REM_CITY\",\"EFF_AVAILABLE_BALANCE\",\"UCIC_ID\"" +
              " FROM EVENT_FT_COREDECLINE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TOD_GRANT_AMOUNT],[VALUE_DATE],[BEN_CITY],[ONLINE_BATCH],[TRAN_AMOUNT],[BRANCH],[BEN_CNTRY_CODE],[REF_TRAN_CRNCY],[PURPOSE_DESC],[ACCOUNT_TYPE],[REM_NAME],[TRAN_ID],[BEN_BIC],[HOST_ID],[TRAN_AMT],[PRODUCT_DESC],[BRANCH_ID],[SYS_TIME],[TRAN_CRNCY_CODE],[TD_MATURITY_DATE],[ACCT_NAME],[BEN_ACCT_NO],[BEN_CUST_ID],[PSTD_DATE],[SYSTEM],[ATM_ID],[PRODUCT_CODE],[DR_CR],[CLIENT_ACC_NO],[REM_BIC],[PSTD_FLG],[STATUS],[HOST_TYPE],[REM_ADD1],[ACCT_ID],[NUMERIC_CODE],[REM_TYPE],[REM_ADD3],[INR_AMOUNT],[REM_ADD2],[TRN_DATE],[PURPOSE_CODE],[CHANNEL_ID],[USER_ID],[BEN_ADD3],[CA_SCHEME_CODE],[RATE],[BEN_ADD2],[REM_ACCT_NO],[BEN_ADD1],[REM_CUST_ID],[INSTRUMENT_NUMBER],[TRAN_NUMONIC_CODE],[BEN_NAME],[ISIN_NUMBER],[PART_TRAN_SRL_NUM],[TRAN_PARTICULAR],[CUST_ID],[EVENTTS],[BANK_CODE],[USD_EQV_AMT],[TD_LIQUIDATION_TYPE],[REM_CNTRY_CODE],[CPTY_AC_NO],[TRAN_DATE],[TRAN_CURR],[INSTRUMENT_TYPE],[REF_TRAN_AMT],[REM_CITY],[EFF_AVAILABLE_BALANCE],[UCIC_ID]" +
              " FROM EVENT_FT_COREDECLINE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TOD_GRANT_AMOUNT`,`VALUE_DATE`,`BEN_CITY`,`ONLINE_BATCH`,`TRAN_AMOUNT`,`BRANCH`,`BEN_CNTRY_CODE`,`REF_TRAN_CRNCY`,`PURPOSE_DESC`,`ACCOUNT_TYPE`,`REM_NAME`,`TRAN_ID`,`BEN_BIC`,`HOST_ID`,`TRAN_AMT`,`PRODUCT_DESC`,`BRANCH_ID`,`SYS_TIME`,`TRAN_CRNCY_CODE`,`TD_MATURITY_DATE`,`ACCT_NAME`,`BEN_ACCT_NO`,`BEN_CUST_ID`,`PSTD_DATE`,`SYSTEM`,`ATM_ID`,`PRODUCT_CODE`,`DR_CR`,`CLIENT_ACC_NO`,`REM_BIC`,`PSTD_FLG`,`STATUS`,`HOST_TYPE`,`REM_ADD1`,`ACCT_ID`,`NUMERIC_CODE`,`REM_TYPE`,`REM_ADD3`,`INR_AMOUNT`,`REM_ADD2`,`TRN_DATE`,`PURPOSE_CODE`,`CHANNEL_ID`,`USER_ID`,`BEN_ADD3`,`CA_SCHEME_CODE`,`RATE`,`BEN_ADD2`,`REM_ACCT_NO`,`BEN_ADD1`,`REM_CUST_ID`,`INSTRUMENT_NUMBER`,`TRAN_NUMONIC_CODE`,`BEN_NAME`,`ISIN_NUMBER`,`PART_TRAN_SRL_NUM`,`TRAN_PARTICULAR`,`CUST_ID`,`EVENTTS`,`BANK_CODE`,`USD_EQV_AMT`,`TD_LIQUIDATION_TYPE`,`REM_CNTRY_CODE`,`CPTY_AC_NO`,`TRAN_DATE`,`TRAN_CURR`,`INSTRUMENT_TYPE`,`REF_TRAN_AMT`,`REM_CITY`,`EFF_AVAILABLE_BALANCE`,`UCIC_ID`" +
              " FROM EVENT_FT_COREDECLINE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_COREDECLINE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TOD_GRANT_AMOUNT\",\"VALUE_DATE\",\"BEN_CITY\",\"ONLINE_BATCH\",\"TRAN_AMOUNT\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"REF_TRAN_CRNCY\",\"PURPOSE_DESC\",\"ACCOUNT_TYPE\",\"REM_NAME\",\"TRAN_ID\",\"BEN_BIC\",\"HOST_ID\",\"TRAN_AMT\",\"PRODUCT_DESC\",\"BRANCH_ID\",\"SYS_TIME\",\"TRAN_CRNCY_CODE\",\"TD_MATURITY_DATE\",\"ACCT_NAME\",\"BEN_ACCT_NO\",\"BEN_CUST_ID\",\"PSTD_DATE\",\"SYSTEM\",\"ATM_ID\",\"PRODUCT_CODE\",\"DR_CR\",\"CLIENT_ACC_NO\",\"REM_BIC\",\"PSTD_FLG\",\"STATUS\",\"HOST_TYPE\",\"REM_ADD1\",\"ACCT_ID\",\"NUMERIC_CODE\",\"REM_TYPE\",\"REM_ADD3\",\"INR_AMOUNT\",\"REM_ADD2\",\"TRN_DATE\",\"PURPOSE_CODE\",\"CHANNEL_ID\",\"USER_ID\",\"BEN_ADD3\",\"CA_SCHEME_CODE\",\"RATE\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"BEN_ADD1\",\"REM_CUST_ID\",\"INSTRUMENT_NUMBER\",\"TRAN_NUMONIC_CODE\",\"BEN_NAME\",\"ISIN_NUMBER\",\"PART_TRAN_SRL_NUM\",\"TRAN_PARTICULAR\",\"CUST_ID\",\"EVENTTS\",\"BANK_CODE\",\"USD_EQV_AMT\",\"TD_LIQUIDATION_TYPE\",\"REM_CNTRY_CODE\",\"CPTY_AC_NO\",\"TRAN_DATE\",\"TRAN_CURR\",\"INSTRUMENT_TYPE\",\"REF_TRAN_AMT\",\"REM_CITY\",\"EFF_AVAILABLE_BALANCE\",\"UCIC_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TOD_GRANT_AMOUNT],[VALUE_DATE],[BEN_CITY],[ONLINE_BATCH],[TRAN_AMOUNT],[BRANCH],[BEN_CNTRY_CODE],[REF_TRAN_CRNCY],[PURPOSE_DESC],[ACCOUNT_TYPE],[REM_NAME],[TRAN_ID],[BEN_BIC],[HOST_ID],[TRAN_AMT],[PRODUCT_DESC],[BRANCH_ID],[SYS_TIME],[TRAN_CRNCY_CODE],[TD_MATURITY_DATE],[ACCT_NAME],[BEN_ACCT_NO],[BEN_CUST_ID],[PSTD_DATE],[SYSTEM],[ATM_ID],[PRODUCT_CODE],[DR_CR],[CLIENT_ACC_NO],[REM_BIC],[PSTD_FLG],[STATUS],[HOST_TYPE],[REM_ADD1],[ACCT_ID],[NUMERIC_CODE],[REM_TYPE],[REM_ADD3],[INR_AMOUNT],[REM_ADD2],[TRN_DATE],[PURPOSE_CODE],[CHANNEL_ID],[USER_ID],[BEN_ADD3],[CA_SCHEME_CODE],[RATE],[BEN_ADD2],[REM_ACCT_NO],[BEN_ADD1],[REM_CUST_ID],[INSTRUMENT_NUMBER],[TRAN_NUMONIC_CODE],[BEN_NAME],[ISIN_NUMBER],[PART_TRAN_SRL_NUM],[TRAN_PARTICULAR],[CUST_ID],[EVENTTS],[BANK_CODE],[USD_EQV_AMT],[TD_LIQUIDATION_TYPE],[REM_CNTRY_CODE],[CPTY_AC_NO],[TRAN_DATE],[TRAN_CURR],[INSTRUMENT_TYPE],[REF_TRAN_AMT],[REM_CITY],[EFF_AVAILABLE_BALANCE],[UCIC_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TOD_GRANT_AMOUNT`,`VALUE_DATE`,`BEN_CITY`,`ONLINE_BATCH`,`TRAN_AMOUNT`,`BRANCH`,`BEN_CNTRY_CODE`,`REF_TRAN_CRNCY`,`PURPOSE_DESC`,`ACCOUNT_TYPE`,`REM_NAME`,`TRAN_ID`,`BEN_BIC`,`HOST_ID`,`TRAN_AMT`,`PRODUCT_DESC`,`BRANCH_ID`,`SYS_TIME`,`TRAN_CRNCY_CODE`,`TD_MATURITY_DATE`,`ACCT_NAME`,`BEN_ACCT_NO`,`BEN_CUST_ID`,`PSTD_DATE`,`SYSTEM`,`ATM_ID`,`PRODUCT_CODE`,`DR_CR`,`CLIENT_ACC_NO`,`REM_BIC`,`PSTD_FLG`,`STATUS`,`HOST_TYPE`,`REM_ADD1`,`ACCT_ID`,`NUMERIC_CODE`,`REM_TYPE`,`REM_ADD3`,`INR_AMOUNT`,`REM_ADD2`,`TRN_DATE`,`PURPOSE_CODE`,`CHANNEL_ID`,`USER_ID`,`BEN_ADD3`,`CA_SCHEME_CODE`,`RATE`,`BEN_ADD2`,`REM_ACCT_NO`,`BEN_ADD1`,`REM_CUST_ID`,`INSTRUMENT_NUMBER`,`TRAN_NUMONIC_CODE`,`BEN_NAME`,`ISIN_NUMBER`,`PART_TRAN_SRL_NUM`,`TRAN_PARTICULAR`,`CUST_ID`,`EVENTTS`,`BANK_CODE`,`USD_EQV_AMT`,`TD_LIQUIDATION_TYPE`,`REM_CNTRY_CODE`,`CPTY_AC_NO`,`TRAN_DATE`,`TRAN_CURR`,`INSTRUMENT_TYPE`,`REF_TRAN_AMT`,`REM_CITY`,`EFF_AVAILABLE_BALANCE`,`UCIC_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

