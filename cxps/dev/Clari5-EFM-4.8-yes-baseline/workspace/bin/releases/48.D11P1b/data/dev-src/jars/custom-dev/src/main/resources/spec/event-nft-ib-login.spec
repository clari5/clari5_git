cxps.events.event.nft-ib-login{
  table-name : EVENT_NFT_IBLOGIN
  event-mnemonic: IBL
  workspaces : {
    CUSTOMER: cust_id
    NONCUSTOMER: addr_network
    TERMINAL: device_id
  }
  event-attributes : {
        

   	system-user-id: {db : true ,raw_name : system_user_id ,type : "string:200"}
	system-password: {db : true ,raw_name : system_password ,type : "string:200"}
	user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        source: {db : true ,raw_name : source ,type : "string:200"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        keys: {db : true ,raw_name : keys ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db : true ,raw_name : addr_network ,type : "string:200"}
        addr-net-local: {db : true ,raw_name :  addr_net_local ,type : "string:200"}
        country-code: {db : true ,raw_name : country_code ,type : "string:200", derivation:"""clari5.custom.eventpreprocessors.LoginPreprocessor.getCountryCode(this)"""}
        i-p-country: {db : true ,raw_name : ip_country ,type : "string:200"}
        i-p-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:200"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
	two-f-a-status: {db : true ,raw_name : 2fa_status ,type : "string:200"}
	two-f-a-mode: {db : true ,raw_name : 2fa_mode ,type : "string:200"}
        device-type: {db : true ,raw_name :  device_type ,type : "string:200"}
        browser-lang: {db : true ,raw_name :  browser_lang ,type : "string:200"}
        browser-time-zone: {db : true ,raw_name : browser_time_zone ,type : "string:200"}
        user-agent: {db : true ,raw_name : user_agent ,type : "string:200"}
        cookie-date-time-stamp: {db : true ,raw_name : cookie_date_time_stamp ,type : timestamp}
        java-status: {db : true ,raw_name :java_status ,type : "string:200"}
  	j-s-status: {db : true ,raw_name :js_status ,type : "string:200"}
	screen-res: {db : true ,raw_name :screen_res ,type : "string:200"}
	mobile-o-s: {db : true ,raw_name :mobile_os ,type : "string:200"}
	mobile-emulator: {db : true ,raw_name :mobile_emulator ,type : "string:200"}
	user-loan-only-flag: {db : true ,raw_name :user_loan_only_flag ,type : "string:200"}
	user-c-c-only-flag: {db : true ,raw_name :user_cc_only_flag ,type : "string:200"}
	login-id-status: {db : true ,raw_name :login_id_status ,type : "string:200"}
	cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
	device_id_count: {db : true ,raw_name :device_id_count ,type : "string:200", derivation:"""clari5.custom.eventpreprocessors.LoginPreprocessor.deviceCount(this)"""}
	device_id_date: {db : true ,raw_name : device_id_date ,type : timestamp}
	dist-bt-curr-last-used-ip: {db : true ,raw_name :dist_bt_curr_last_used_ip ,type : "string:200"}
	first-login: {db : true ,raw_name :first_login ,type : "string:200"}
	time-slot: {db : true ,raw_name : time_slot ,type : "string:200", derivation:"""clari5.custom.eventpreprocessors.LoginPreprocessor.getTimeSlot(this)"""}
	device-binding: {db : true ,raw_name :device-binding ,type : "string:200", derivation:"""clari5.custom.eventpreprocessors.LoginPreprocessor.saveInRdbms(this)"""}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
        risk-band:{db : true ,raw_name :risk_band ,type : "string:200"}
        distance: {db : true ,raw_name :distance ,type:"number:20,3", derivation:"""cxps.events.CustomFieldDerivator.getDistanceBetweenIP(this)"""}
        browser-version: {db : true ,raw_name : browser_version ,type : "string:200"}
        os: {db : true ,raw_name : os ,type : "string:200"}
       cookie-enabled-flag: {db : true ,raw_name : cookie_enabled_flag ,type : "string:200"}
       java-enabled-flag: {db : true ,raw_name : java_enabled_flag ,type : "string:200"}
      time-zone: {db : true ,raw_name : time_zone ,type : "string:200"}
     time-zone-offset: {db : true ,raw_name : time_zone_offset ,type : "string:200"}
      screen-resolution: {db : true ,raw_name : screen_resolution ,type : "string:200"}
      agent: {db : true ,raw_name : agent ,type : "string:200"}
      ip-address: {db : true ,raw_name : ip_address ,type : "string:200"}
     browser-plugin: {db : true ,raw_name : browser_plugin ,type : "string:200"}
     browser-name: {db : true ,raw_name : browser_name ,type : "string:200"}
     
}
}



