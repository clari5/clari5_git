package clari5.custom.inbox.processors;

import clari5.aml.cms.newjira.JiraInstance;
import clari5.custom.db.InboxAutoReqTable;
import clari5.custom.yes.db.FailedClusterAssignment;
import clari5.custom.yes.db.InboxAutoReq;
import clari5.inbox.Cl5IbxItem;
import clari5.inbox.enums.AuditActivity;
import clari5.inbox.daemons.InboxDaemon;
import clari5.inbox.InboxResponseTransformer;
import clari5.inbox.model.Group;
import clari5.inbox.processors.group.IGroupAssignment;
import clari5.inbox.utils.GroupUtils;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import clari5.trace.CxTracer;
import clari5.trace.TracerException;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;
import java.text.SimpleDateFormat;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.sql.*;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


public class CmsProcessor implements clari5.inbox.processors.IInboxProcessor {

    private static CxpsLogger logger = CxpsLogger.getLogger(CmsProcessor.class);

    private static JiraClient jiraClient;
    private static boolean isJiraClientInitialized = false;
    private static Hocon responseKeys = null;


    private int COMMENT_SUCCESS = 201;
    private int TRANSITION_SUCCESS = 204;

    private static void addAttachmentToIssue(String issueKey, String itemId) throws SQLException, IOException, JiraClientException {

        logger.debug(" Sending the following files to JIRA ");

        try (Connection con = Rdbms.getAppConnection()) {
            InputStream inputStream;
            String statement = "select filename,  attachment from CL5_IBX_ATTACHMENT where item_id = ?";
            try (PreparedStatement preparedStatement = con.prepareStatement(statement)) {
                preparedStatement.setString(1, itemId);
                try (ResultSet result = preparedStatement.executeQuery()) {
                    //For multiple attachments
                    while (result.next()) {
                        Blob blob = result.getBlob("attachment");
                        String filename = result.getString("fileName");
                        logger.debug(filename);
                        if (blob == null) inputStream = new ByteArrayInputStream("".getBytes());
                        else inputStream = blob.getBinaryStream();
                        jiraClient.sendAttachment(issueKey, filename, inputStream);
                        inputStream.close();
                        if (blob != null) {
                            blob.free();
                        }
                    }
                }
            }
        }
    }

    public static boolean isAutomated(String factName) {
        try {
            InboxAutoReqTable inboxAutoReqTable = new InboxAutoReqTable();
            InboxAutoReq inboxAutoReq = inboxAutoReqTable.select(factName);

            if (null != inboxAutoReq) {
                logger.info("[isAutomated]" + inboxAutoReq.toString());
                if (inboxAutoReq.getAutoInboxChqFlg().equalsIgnoreCase("Y")) return true;
            }

        } catch (Exception e) {
            logger.error("[isAutomated] Failed to check inbox automation" + e.getMessage());
        }
        return false;
    }

    public void processMessage(String eventName, String event) throws IOException {

        if (!isJiraClientInitialized) {
            String jiraInstanceName = null;
            try {
                JiraInstance instance = new JiraInstance("jira-instance");
                jiraClient = new JiraClient(instance.getInstanceParams());
                isJiraClientInitialized = true;
                responseKeys = ((InboxDaemon) Clari5.getResource("inbox")).getResponseKeys();
                if (responseKeys != null) {
                    Map<String, String> keyLabelMap = new LinkedHashMap<>();
                    for (String bankKey : responseKeys.getKeysAsList()) {
                        keyLabelMap.put(bankKey, responseKeys.getString(bankKey));
                    }
                }
            } catch (JiraClientException.Configuration configuration) {
                configuration.printStackTrace();
            }
        }

        switch (eventName) {
            case "cms-inquiry":
                try {
                    processCmsInquiry(event);
                } catch (IOException e) {
                    logger.error("Exception occurred while processing CMS Inquiry: " + e.getMessage(), e);
                    throw new IOException(e.getMessage());
                }
                break;
            case "cms-response":
                processCmsResponse(event);
                break;
        }
    }

    private void addInboxItem(RDBMSSession session, CxJson issueJson, Group cluster) throws IOException {
        issueJson.put("assignedTo", cluster.getGroupId());
        issueJson.put("updatedBy", "system");
        issueJson.put("createdBy", "system");

        CxJson msg;
        try {
            msg = CxJson.parse(issueJson.getString("msg"));
        } catch (Exception e) {
            e.printStackTrace();
            msg = null;
        }

        if (msg != null) {
            msg.put("query", msg.getString("comment", ""));
            issueJson.put("msg", msg.toJson());
        }

        try {
            Cl5IbxItem ibxItem = new Cl5IbxItem(session);
            logger.debug("Populating Inbox Item object...");
            ibxItem.from(issueJson.toString());
            logger.debug("Inbox Item populated successfully from JSON Object");

            ibxItem.insert(session);
            //tracer.setPassed(session);
            session.commit();
            logger.debug("Inbox Item Added Successfully");

            GroupUtils.addAuditEntry(session, AuditActivity.ASSIGN.getString(), ibxItem.getItemId(),
                    null, ibxItem.getAssignedTo(), ibxItem.getAssignedTo(), ibxItem.getAssignedTo());
        } catch (Exception e) {
            logger.error("Exception occurred while adding inbox item: " + e.getMessage(), e);
            throw new IOException(e.getMessage());
        }
    }

    private void processCmsInquiry(String event) throws IOException {
        CxJson eventJson;
        CxJson issueJson;
        CxJson msgJson;
        String issueKey = "";
        String segment = "";
        String level = "";
        String group = "";
        String factName = "";
        String custId = "";
        String accountId = "";
        String failedOn = "";
        String msg="";
        String branchId = "";

        /* Parse Event JSON obtained from JIRA */
        try {
            eventJson = CxJson.parse(event);
            String issueData = eventJson.getString("issueData");
            issueJson = CxJson.parse(issueData);
            issueKey = issueJson.getString("issueKey");
            segment = issueJson.getString("Segment");
            level = issueJson.getString("Level");
            group = issueJson.getString("Group");
            msg = issueJson.getString("msg");
            branchId=issueJson.getString("branchId");
            msgJson=CxJson.parse(msg);
            factName = msgJson.getString("FactName");
            custId = msgJson.getString("Entity Id");
            accountId = msgJson.getString("Entity Id Secondary");
            failedOn = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            logger.debug("Event JSON Parse successful: " + eventJson.toJson());
        } catch (Exception e) {
            logger.error("Exception occurred while parsing event request obtained from JIRA: " + e.getMessage(), e);
            throw new IOException(e.getMessage());
        }

        RDBMSSession session = Rdbms.getAppSession();
        try {
            /* Fetch inbox user cluster by custom hook */
            IGroupAssignment iClusterAssignment = GroupUtils.getGroupProcessor();
            logger.debug("Cluster Assignment class " + iClusterAssignment.getClass().getSimpleName() + " instantiated successfully");

            logger.debug("Calling custom hook for inbox item cluster assignment...");
            Group cluster = iClusterAssignment.assign(session, eventJson);
            if (cluster == null || cluster.getUsers().isEmpty()) {
                logger.info("Cluster is null");
                FailedClusterAssignment failedClusterAssignment = new FailedClusterAssignment();
                failedClusterAssignment.setId(String.valueOf(System.currentTimeMillis()));
                failedClusterAssignment.setIssueKey(issueKey);
                failedClusterAssignment.setCustId(custId);
                failedClusterAssignment.setAccountId(accountId);
                failedClusterAssignment.setFactName(factName);
                failedClusterAssignment.setAssignedSegment(segment);
                failedClusterAssignment.setAssignedLevel(level);
                failedClusterAssignment.setAssignedGroup(group);
                failedClusterAssignment.setBranchId(branchId);
                failedClusterAssignment.setFailedOn(failedOn);
                if(cluster!=null&& cluster.getUsers().isEmpty())failedClusterAssignment.setRemark("User not available for the cluster");
                else failedClusterAssignment.setRemark("Cluster not available");
                failedClusterAssignment.insert(session);
                logger.info("Details of failedClusterAssignment: " + failedClusterAssignment.toString());
            } else {
		logger.info("Inbox User Cluster fetched using hook");

		/* Add User to Inbox Cluster User table if entry doesn't exist */
		/*try {

		    ClusterUtils.syncUserCluster(session, cluster);

		logger.debug("Inbox Cluster User table synced successfully");
		} catch (Exception e) {
		logger.error("Exception during Cluster User table sync: " + e.getMessage(), e);
		throw new TracerException.Fatal(e.getMessage());
		}*/

		/* Add Inbox Item with all details */
		addInboxItem(session, issueJson, cluster);
		logger.debug("Auto Assignment and Inbox Item creation successful");
            }
            session.commit();
        } catch (RdbmsException e) {
            logger.error("Exception occurred while processing CMs Inquiry: " + e.getMessage(), e);
        } finally {
            if (session != null) {
                session.rollback();
                session.close();
            }
        }
    }

    private void processCmsResponse(String event) throws IOException {
        try {
            JSONObject eventJson = new JSONObject(event);
            JSONObject issueDataJson = (JSONObject) eventJson.get("issueData");
            String issueKey = issueDataJson.getString("issueKey");
            String itemId = issueDataJson.getString("itemId");
            String userId = issueDataJson.getString("userId");
            JSONObject response = issueDataJson.getJSONObject("response");
            String comment;
            String inboxReply = "";
            String suspicious = "";
            String nonSuspicious = "";
            String fact = "";


            if (((InboxDaemon) Clari5.getResource("inbox")).getResponseTransformer() != null) {

                String additionalComment = issueDataJson.getString("addComment");
                if (additionalComment != null && !additionalComment.isEmpty()) {
                    response.put("additionalComment", additionalComment);
                }
                inboxReply = InboxResponseTransformer.transformResponse(response, responseKeys);

                inboxReply = "Reply from " + userId + " :\n" + inboxReply;
            } else {
                if (issueDataJson.has("comment")) {
                    comment = issueDataJson.getString("comment");
                    inboxReply = "Reply from " + userId + " :" + comment;
                }
            }

            //At Jira UI, user needs to be shown along with the comment

            String hasAttachments = issueDataJson.getString("hasAttachments");
            if (hasAttachments.equalsIgnoreCase("Y")) {
                addAttachmentToIssue(issueKey, itemId);
            }

            //Sending to JIRA
            JSONObject jsonObject = new JSONObject();
            // jsonObject.put("body", comment);
            jsonObject.put("body", inboxReply);

            CxJson responseJson = jiraClient.addCommentRequest(jsonObject.toString(), issueKey);
            int responseCode = responseJson.getInt("responseCode", -1);
            if (responseCode == COMMENT_SUCCESS) {
                jsonObject = new JSONObject();
                JSONObject transitionObject = new JSONObject();
                Iterator<String> keyList = response.keys();
                for (; keyList.hasNext(); ) {
                    String s = keyList.next();
                    if (s.startsWith("issus")) {
                        suspicious = response.getString(s);
                    }
                    if (s.startsWith("factName")) {
                        fact = response.getString(s);
                    }
                }
                //suspicious = response.getString("issus");
                Hocon h = new Hocon();
                h.loadFromContext("suspecious-customFields.conf");
                JSONObject updateFields = new JSONObject();
                JSONObject wrapperJson = new JSONObject();
                String suspiciousByBranch = h.getString("jira-field.Suspecious-By-Branch");
                logger.info("Suspecius value " + suspicious);

                if (isAutomated(fact)) {
                    if (suspicious.equalsIgnoreCase("Y")) {
                        String transitionId = jiraClient.getTransitionId("clarification-received-transition");
                        transitionObject.put("id", transitionId);
                        jsonObject.put("transition", transitionObject);
                        updateFields.put(suspiciousByBranch, "Yes");
                        wrapperJson.put("fields", updateFields);

                        logger.info("wrapperJson " + wrapperJson.toString() + " if suspecious [" + suspicious + "]");
                        jiraClient.updateIssueRequest(wrapperJson.toString(), issueKey);

                    } else if (suspicious.equalsIgnoreCase("N")) {
                        String transitionIdClosed = h.getString("transitions.clarificaiton-closed-transition");
                        logger.debug("clarificaiton-closed-transition-id: " + transitionIdClosed);
                        transitionObject.put("id", transitionIdClosed);
                        jsonObject.put("transition", transitionObject);
                        updateFields.put(suspiciousByBranch, "No");
                        wrapperJson.put("fields", updateFields);
                        logger.info("wrapperJson " + wrapperJson.toString() + " if suspecious [" + suspicious + "]");
                        jiraClient.updateIssueRequest(wrapperJson.toString(), issueKey);
                    }
                } else {
                    String transitionId = jiraClient.getTransitionId("clarification-received-transition");
                    transitionObject.put("id", transitionId);
                    jsonObject.put("transition", transitionObject);
                    if (suspicious.equalsIgnoreCase("Y")) {
                        updateFields.put(suspiciousByBranch, "Yes");
                    } else if (suspicious.equalsIgnoreCase("N")) {
                        updateFields.put(suspiciousByBranch, "No");
                    } else {
                        updateFields.put(suspiciousByBranch, "NA");
                    }
                    wrapperJson.put("fields", updateFields);
                    logger.info("[mannual] wrapperJson " + wrapperJson.toString() + " if suspecious [" + suspicious + "]");

                    jiraClient.updateIssueRequest(wrapperJson.toString(), issueKey);
                }
                responseJson = jiraClient.changeTransitionState(jsonObject.toString(), issueKey);
                responseCode = responseJson.getInt("responseCode", -1);
                if (responseCode == TRANSITION_SUCCESS) {
                    try (RDBMSSession session = Rdbms.getAppSession()) {
                        session.commit();
                    }
                } else {
                    if (responseJson.hasKey("responseCode")) {
                        responseJson.delete("responseCode");
                    }
                    Exception exception = new Exception(responseJson.toString());
                    try {
                        throw exception;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (MalformedURLException e) {
            logger.error("Exception while forming url: " + e.getMessage(), e);
            throw new IOException(e);
        } catch (ProtocolException e) {
            logger.error("Protocol Exception: " + e.getMessage(), e);
            throw new IOException(e);
        } catch (IOException e) {
            logger.error("Exception while I/O: " + e.getMessage(), e);
            throw new IOException(e);
        } catch (SQLException e) {
            logger.error("SQL Exception: " + e.getMessage(), e);
            throw new IOException(e);
        } catch (JiraClientException e) {
            logger.error("Exception while posting to Jira: " + e.getMessage(), e);
            throw new IOException(e);
        } catch (Exception e) {
            logger.error("Exception occurred while processing Inbox response to JIRA: " + e.getMessage(), e);
            throw new IOException(e);
        }
    }
}
