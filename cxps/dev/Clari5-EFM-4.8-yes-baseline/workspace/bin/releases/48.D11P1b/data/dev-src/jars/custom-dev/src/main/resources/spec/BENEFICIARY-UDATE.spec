clari5.custom.mapper {
        entity {
                BENEFICIARY_UDATE {
                       generate = true
                        attributes:[
                                { name: CUST_ID ,type ="string:50", key=true},
                                { name: BENF_PAYEE_ID ,type ="string:50", key=true},
                                { name: GROUP_PAYEE_ID ,type = "string:50", key=true},
                                { name: BENEFICIARY_NICKNAME ,type = "string:50"},
                                { name: BENF_TYPE ,type = "string:50"},
                                { name: BENF_UNIQUE_VALUE ,type = "string:50"},
                                { name: BENF_FLAG ,type = "string:50"},
                                { name: PAYEE_ACCTTYPE ,type = "string:50"},
                                { name: CREATED_ON ,type = timestamp},
                                { name: UPDATED_ON ,type = timestamp},
                                { name: RCRE_TIME ,type = timestamp},
                                { name: RCRE_USER ,type = "string:50"},
                                { name: LCHG_TIME ,type = timestamp},
                                { name: LCHG_USER ,type = "string:50"}
                                ]
                        }
        }
}
