/*
var public_key = '-----BEGIN PUBLIC KEY-----';
public_key += 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKaOP3kuN2UqTTLSqTTdD3d2HaF9CEUQ';
public_key += 'mwv+n5iQ+ufRRGItvAJpxN8vUQxvq/+D/6aAPMt3+j7La+Q4AgPeZLMCAwEAAQ==';
public_key += '-----END PUBLIC KEY-----';
*/



var pub='-----BEGIN PUBLIC KEY-----';
pub+='MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA8mLLVFi+R9BgT7MSlpVk';
pub+='AnNzYDRTbEVkPafmU66GPST+sIib9AiDviKildzGFpKVzEwY+Qyp7X6cfdcCjC1r';
pub+='603A1PTnAJ6c0qw8hc9KVRya5iazgfwUDoXLITtwuAg1ZQIPovKBo9q/fDn736B2';
pub+='JLQ2hrUquB2e+X39uE3WceZ0P1MPua02XF4HGLe2mMZCrAgJEFHB0I/CsPXtC+7S';
pub+='DGhLmgYsLoOXD0rm7mbDfRe5KbEYakA8pzPefJ+aeKFIoeMt+W0ouVe9IdoPnxJb';
pub+='ywprObHqGwwo5yCdqXO1LwilZT/JtYZF9ICYkYdX2bGVtH2U0cc9ozQTSZOSG+0b';
pub+='ynsTmTIEwEKtZX0cHpgmqQMc4LpYljHnMjiebhCsv5LkxCk1FMgcrROh8O9tNr41';
pub+='Kazoqge2+5KdyU5W/t2ASLmgMsHzxVDVZSvLBe2jXBrptoBYvoBgC1hhsK8wv5kz';
pub+='sOh1WoMWrm44CE6r8Ip+O4/4iOtWBaDyFgs2lkx6dDnTappGbUmGIATqlSRNEqKp';
pub+='exX3o6uoAOzhY5pejTbtzs6Z5jphj1TPFrOIqzwbhCihe/tigcAkHPQM/Gf8hgC6';
pub+='ilaJrZ+7S6682MbFi4Kp0EmtBLohb+zb0R6DYQ9LEMIPyYfgJP1pgHWcu+1HcNMs';
pub+='hLbYYiCl/DERLHC87lvlEp0CAwEAAQ==';
pub+='-----END PUBLIC KEY-----';

function isJavaEnabled() {
    return navigator.javaEnabled();
}

function isCookieEnabled() {
    return navigator.cookieEnabled;
}

function getTimeZone() {
    return /\((.*)\)/.exec(new Date().toString())[1];
}

function getScreenResolution() {
    return window.screen.availWidth + ' x ' + window.screen.availHeight;
}

function getTimezoneOffset() {
    var dtStr = new Date().toString();
    return dtStr.substring((dtStr.substring(0, dtStr.lastIndexOf('(')).trim()).lastIndexOf(' '), dtStr.lastIndexOf('(') - 1).trim();
}

function getOS() {
    if (isLinux()) return "Linux";
    if (isWindows()) return "Windows";
    if (isMac()) return "Mac";
}

function getOSVersion() {
    if (isLinux()) return getLinuxVersion();
    if (isWindows()) return getWindowsVersion();
    if (isMac()) return getMacVersion();
}

function isLinux() {
    return navigator.platform.indexOf("Linux") >= 0;
}

function isWindows() {
    return navigator.platform.indexOf("Win") >= 0;
}

function isMac() {
    return navigator.platform.indexOf("Mac") >= 0;
}

function getWindowsVersion() {
    var appVersion = navigator.appVersion;
    var tokens = appVersion.substring(appVersion.indexOf("(") + 1, appVersion.indexOf(")")).split(";");
    for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];
                if (token.indexOf("Win") >= 0 || token.indexOf("win") >= 0) return token.trim();
            }
        }

function getLinuxVersion() {
    return navigator.platform;
}

function getMacVersion() {
//var tokens = appVersion.substring(appVersion.indexOf("(") + 1, appVersion.indexOf(")")).split(";");
    //return tokens[1].trim();
  var appVersion = navigator.appVersion;
    var tokens = appVersion.substring(appVersion.indexOf("(") + 1, appVersion.indexOf(")")).split(";");
    for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];
                if (token.indexOf("Mac") >= 0 || token.indexOf("Mac") >= 0)
                console.log("Mac Version" +token.trim);
                return token.trim();
            }
}

function isChrome() {
    var vendor = navigator.vendor;
    return vendor != undefined && vendor.trim().length > 0 && vendor.indexOf("Google") >= 0;
}

function isFirefox() {
    return navigator.appCodeName == 'Mozilla'
        && navigator.userAgent.split(" ")[navigator.userAgent.split(" ").length - 1].indexOf("Firefox") >= 0;
}
//Addition of Safari Code 
function  isSafari() {
    if (navigator.vendor ==  "Apple Computer, Inc." && navigator.userAgent.indexOf('Safari') >=0)
    return true;
 else return false;
}

function isIE() {
var userAgent = getUserAgent();
    var tokens = userAgent.substring(userAgent.indexOf("(") + 1, userAgent.lastIndexOf(")")).split(";");
    console.log("token:====== " + tokens);
            for (var i = 0; i < tokens.length; i++) {
                if (tokens[i].trim().indexOf(".NET4.0E") >= 0) return true;
            }
            return false;
        }

function isFirefox() {
    return navigator.appCodeName == 'Mozilla'
        && navigator.userAgent.split(" ")[navigator.userAgent.split(" ").length - 1].indexOf("Firefox") >= 0;
}

function getBrowser() {
    return isChrome() ? "Chrome" : (isFirefox() ? "Firefox" : (isIE() ? "InternetExplorer" : ( isSafari() ? "Safari":undefined))) ;
}

function getBrowserVersion() {
    return isChrome() ? getChromeVersion() : (isFirefox() ? getFFVersion() : (isIE() ? getIEVersion() : ( isSafari()? getSafariVersion() :undefined)));
}

        function getIEVersion() {
           var x = navigator.userAgent.split(" ")[navigator.userAgent.split(" ").length - 3];
           return x.substring(x.lastIndexOf(":")+1,x.lastIndexOf(")")) >= 0;
        }

function getFFVersion() {
    var verStr = navigator.userAgent.substring(navigator.userAgent.lastIndexOf(')') + 1).split(' ');
    for (var i = 0; i < verStr.length; i++) {
        if (verStr[i].indexOf('Firefox') >= 0) return verStr[i].split('/')[1];
    }
}

function getChromeVersion() {
    var verStr = navigator.userAgent.substring(navigator.userAgent.lastIndexOf(')') + 1).split(' ');
    for (var i = 0; i < verStr.length; i++) {
        if (verStr[i].indexOf('Chrome') >= 0) return verStr[i].split('/')[1];
    }
}
function getSafariVersion() {
    var verStr = navigator.userAgent.substring(navigator.userAgent.lastIndexOf(')') + 1).split(' ');
    for (var i = 0; i < verStr.length; i++) {
        if (verStr[i].indexOf('Safari') >= 0) return verStr[i].split('/')[1];
    }
}

        function text2Char(text) {
            return text.toString().split('').map(function(c){ return c.charCodeAt(0);});
        }

function byteHex(n) {
    return ("0" + Number(n).toString(16)).substr(-2);
}

        function applySalt(code, salt) {
            return text2Char(salt).reduce(function(a, b){ return a ^ b;}, code);
        }

function getPlugins() {
    var result = []
    var plugins = navigator.plugins;
    for (var i = 0; i < plugins.length; i++) {
        result.push(plugins[i].name);
    }
    return result;
}



        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            console.log(myPeerConnection.name);
            var pc = new myPeerConnection({
                    iceServers: []
                }),
                noop = function() {},
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer().then(function(sdp) {
                sdp.sdp.split('\n').forEach(function(line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }).catch(function(reason) {
                // An error occurred, so handle the failure to connect
            });

            //listen for candidate events
            pc.onicecandidate = function(ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        function hashup(salt, text) {
            var charArr = text.toString().split('');
            var result = '';
            for (var i = 0; i < charArr.length; i++) {
                var t2c = text2Char(charArr[i]);
                //console.log(t2c);
                var aSalt = applySalt(t2c, salt);
                //console.log(aSalt);
                var bHex = byteHex(aSalt);
                //console.log(bHex);
                result = result.concat(bHex);
            }
            //console.log(result);
            return result;
        }

        function dehash(salt, enryptedString) {
            var hexArr = enryptedString.match(/.{1,2}/g);
            var result = '';
            for (var i = 0; i < hexArr.length; i++) {
                //console.log(hexArr[i]);
                var h2n = parseInt(hexArr[i], 16);
                //console.log(h2n);
                var aSalt = applySalt(h2n, salt);
                //console.log(aSalt);
                var cStr = String.fromCharCode(aSalt);
                //console.log(cStr);
                result = result.concat(cStr);
            }
            return result;
        }

       function getUserAgent(){
            var usergent = navigator.userAgent;
            usergent = usergent.replace(/;/g, "");
            return usergent;
        }

        function assimilate(cookieName) {
            var data = {};
            var machID = '';
            var septr = "|";
            data.cookiePresent = getCookie('cl5_cookie') ;
	    data.currentDate = currentTime();
            console.log("current date : "+data.currentDate );
	    console.log("cookie present : "+data.cookiePresent);
            data.browser = getBrowser();
            data.browserVersion = getBrowserVersion();
            data.os = getOS();
            data.osVersion = getOSVersion();
            data.javaEnabled = isJavaEnabled();
            data.cookieEnabled = isCookieEnabled();
            data.timezone = getTimeZone();
            data.timezoneOffset = getTimezoneOffset();
            data.screenResolution = getScreenResolution();
            machID = machID.concat(data.browser).concat(septr).concat(data.browserVersion).concat(septr).concat(data.os).concat(septr)
                .concat(data.osVersion)//.concat(septr).concat(data.javaEnabled).concat(septr).concat(data.cookieEnabled).concat(septr)
                .concat(data.timezone).concat(septr);//.concat(data.timezoneOffset).concat(septr).concat(data.screenResolution);
            //data.userId = salt;
            data.plugins = getPlugins();
            data.userAgent = getUserAgent();
            data.language = navigator.language || navigator.userLanguage;
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cook = cookies[i].split('=');
                if (cook[0] == cookieName) {
                   // data.cookiePresent = true;
                    data.machineId = cook[1];
                }
            }
            //data.machineId = data.machineId ? data.machineId : hashup(salt, machID);
            return data;
        }

        function cl5Collect() {
            var data = assimilate("cl5_cookie");
           /* var proArr = []
            var promise = new Promise(function(resolve) {
                getUserIP(function(ip) {
                    resolve(ip);
                });
            });
            proArr.push(promise);
            Promise.all(proArr).then(function(ip) {
                data.ip = ip[0];
                return data.ip;
                //final form submission code goes here; login or transaction submit
               // console.log(JSON.stringify(data));
            });*/
	//console.log(data);
            return JSON.stringify(data);

        }
/*
function encrypted(){
    var enc = new JSEncrypt();
    enc.setPublicKey(public_key);
    var encrypted = enc.encrypt(cl5Collect());
    console.log("Encrypted data : "+encrypted); 
   return encrypted;
}*/

let encrypted= new Promise(function(resolve,reject)
{
   var enc = new JSEncrypt();
   enc.setPublicKey(pub);
   var encrypted = enc.encrypt(cl5Collect());
   console.log('Data inside encrypted()'+encrypted);
   resolve(encrypted)
                    	//return encrypted;
   
 })



function getCookie(name)
  {
	var value=RegExp(name+"=[^;]+").exec(document.cookie);      
        console.log(value);
        if ((value != null) ) {
	var deviceId_value=RegExp("Cl5_DeviceID"+"=[^;]+").exec(document.cookie); 
	if(deviceId_value){
		console.log(deviceId_value);
		var arr = deviceId_value.toString().split("=");	
		if(arr[1].trim()){
			console.log("Device Id : "+arr[1]);
			return true;
		}
	 	else return false	// if deviceId is null	
	}
	else 
	return false	// if Cl5_DeviceID is not present
    } 
    else return false // if cl5_cookie is not present

  }

function cl5SetCookie(deviceId)
{
	
	    var expiryMonths=6;
	    var curDate = new Date();
	    var newDate  = new Date(curDate.setMonth(curDate.getMonth()+expiryMonths));
	    var today=new Date(curDate.getTime() - (curDate.getTimezoneOffset() * 60000)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
	    var milisecond = today +"."+curDate.getMilliseconds();
	    milisecond = milisecond.split(' ');
	    var curDateGMT =milisecond[0].split('-').reverse().join('-') +" "+milisecond[1]; 
	    document.cookie = "cl5_cookie=" + deviceId + "; Expires="+ newDate + ";";
   	    document.cookie = "Cl5_DeviceID=" + deviceId + ";";

}
function currentTime(){
	  //var expiryMonths=6;
          var cureDate = new Date();
//console.log("curDate"+cureDate);
	  //var newDate=new Date(curDate.setMonth(curDate.getMonth()+expiryMonths));
//console.log("newDate"+newDate);
	  var today=new Date(cureDate.getTime() - (cureDate.getTimezoneOffset() * 60000)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
//console.log("today"+today);
	  var milisecond =today +"."+cureDate.getMilliseconds();
//console.log("miliseceond before"+milisecond);
	   milisecond = milisecond.split(' ');
//console.log("milisecond after"+milisecond);
	 var cureDateGMT = milisecond[0].split('-').reverse().join('-') +" "+milisecond[1];
console.log("curDateGMT"+cureDateGMT);
           return cureDateGMT;
}
