cxps.events.event.ft-eipoapplication{
  table-name : EVENT_FT_EIPOAPPLICATION
  event-mnemonic: NEIP
  workspaces : {
    CUSTOMER: cust_id
    ACCOUNT: casa-ac-no 
  }
  event-attributes : {
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        risk-band: {db : true ,raw_name : risk_band ,type : "string:200" ,derivation : """cxps.events.CustomFieldDerivator.checkInstanceofEvent(this)"""}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        applicant-name: {db : true ,raw_name : applicant_name ,type: "string:200" }
        applicant-pan: {db : true ,raw_name : applicant_pan ,type: "string:200" }
        applicant-unique-identifier: {db : true ,raw_name : applicant_unique_identifier ,type: "string:200" }
        applicant-type: {db : true ,raw_name : applicant_type ,type: "string:200" }
        ipo-id: {db : true ,raw_name : ipo_id ,type: "string:200" }
        ipo-type: {db : true ,raw_name : ipo_type ,type: "string:200" }
        bid1-amount: {db : true ,raw_name : bid1_amount ,type: "number:20,3" }
        bid2-amount: {db : true ,raw_name : bid2_amount ,type: "number:20,3" }
	bid3-amount: {db : true ,raw_name : bid3_amount ,type: "number:20,3" }
	bid4-amount: {db : true ,raw_name : bid4_amount ,type: "number:20,3" }
	bid5-amount: {db : true ,raw_name : bid5_amount ,type: "number:20,3" }
	bid6-amount: {db : true ,raw_name : bid6_amount ,type: "number:20,3" }
	bid7-amount: {db : true ,raw_name : bid7_amount ,type: "number:20,3" }
	bid8-amount: {db : true ,raw_name : bid8_amount ,type: "number:20,3" }
	bid9-amount: {db : true ,raw_name : bid9_amount ,type: "number:20,3" }
	bid10-amount: {db : true ,raw_name : bid10_amount ,type: "number:20,3" }
	bid11-amount: {db : true ,raw_name : bid11_amount ,type: "number:20,3" }
	bid12-amount: {db : true ,raw_name : bid12_amount ,type: "number:20,3" }
	bid13-amount: {db : true ,raw_name : bid13_amount ,type: "number:20,3" }
	bid14-amount: {db : true ,raw_name : bid14_amount ,type: "number:20,3" }
	bid15-amount: {db : true ,raw_name : bid15_amount ,type: "number:20,3" }
	bid16-amount: {db : true ,raw_name : bid16_amount ,type: "number:20,3" }
	bid17-amount: {db : true ,raw_name : bid17_amount ,type: "number:20,3" }
	bid18-amount: {db : true ,raw_name : bid18_amount ,type: "number:20,3" }
	total-application-amount: {db : true ,raw_name : total_application_amount ,type: "number:20,3" }
        casa-ac-no: {db : true ,raw_name : casa_ac_no ,type: "string:200" }
        account-balance: {db : true ,raw_name : account_balance ,type: "number:20,3" }
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
	    session-id:{db : true ,raw_name :session_id ,type : "string:200"}
}
}
