clari5.custom.mapper {
        entity {
                DISTANCE_IP {
                       generate = true
                        attributes:[
                                { name: CUST_ID ,type ="string:50", key=true},
                                { name: CURRENT_IP ,type ="string:50"},
                                { name: LAST_IP ,type = "string:50"},
                                { name: RCRE_TIME ,type = timestamp},
                                { name: RCRE_USER ,type = "string:50"},
                                { name: LCHG_TIME ,type = timestamp},
                                { name: LCHG_USER ,type = "string:50"}

                                ]
                        }
        }
}
