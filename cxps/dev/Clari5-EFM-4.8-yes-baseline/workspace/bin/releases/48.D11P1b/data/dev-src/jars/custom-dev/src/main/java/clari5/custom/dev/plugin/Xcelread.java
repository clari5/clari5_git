package clari5.custom.dev.plugin;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import com.sleepycat.je.rep.elections.Protocol;
import org.apache.hadoop.hbase.protobuf.generated.CellProtos;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class Xcelread {


    Connection con = null;
    PreparedStatement ps = null;
    Statement stmt = null;

    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(Xcelread.class);



    public List<String> insertXcl(String FILE_NAME, String tableName) throws IOException {

        List<String> statusList = new ArrayList<>();
        try {

            Hocon h = new Hocon();
            boolean loadpath =  h.loadFromContext("custom-path.conf");
            if(loadpath){
                cxpsLogger.info("Sucessfully loaded the conf file");
            }
            int commitsize = h.getInt("commitsize");


            XSSFWorkbook my_xls_workbook = null;
            FileInputStream input_document = null;
            input_document = new FileInputStream(new File(FILE_NAME));
            my_xls_workbook = new XSSFWorkbook(input_document);
            String fn = my_xls_workbook.getClass().toString();
            String f = my_xls_workbook.getSheetName(0);
            System.out.println("Sheet Name : " + f);
            System.out.println(fn);
            XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
            int rowcount = my_worksheet.getPhysicalNumberOfRows();
            System.out.println("No of rows is : " + rowcount);

            int noOfColumns = my_worksheet.getRow(0).getPhysicalNumberOfCells();
            XSSFRow header_row = my_worksheet.getRow(0);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Insert into " + tableName + "(");
            for (int i = 0; i < noOfColumns; i++) {
                XSSFCell header_cell = header_row.getCell(i);
                String header = header_cell.getStringCellValue();
                System.out.println("header column" + i + "value is " + header);
                stringBuilder.append(header + ", ");
            }
            int lstindexComma = stringBuilder.lastIndexOf(",");
            stringBuilder.deleteCharAt(lstindexComma);
            stringBuilder.append(") values (");

            String insert = stringBuilder.toString();


            con = Rdbms.getConnection();
            stmt = con.createStatement();

            int cmtcnt = 0 ;

            for (int r = 1; r < rowcount; r++) {
                XSSFRow row = my_worksheet.getRow(r);
                StringBuilder stringBuilder1 = new StringBuilder();

                for (int c = 0; c < noOfColumns; c++) {
                    if (row.getCell(c) != null) {
                        XSSFCell cell = row.getCell(c);
                        if (cell != null) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_STRING:
                                    boolean chkdate = isValidDate(cell.getStringCellValue());
                                    if(chkdate) {

                                        String formatedDate = getDate(cell.getStringCellValue());
                                        if(formatedDate != null) {
                                            stringBuilder1.append("TO_TIMESTAMP('" + formatedDate + "','DD-MM-YYYY HH24:MI:SS'), ");
                                            System.out.println("formatedDate : " + formatedDate);
                                        }
                                        else {
                                            System.out.println("Error forming date");
                                        }
                                        break;

                                    }else {
                                        stringBuilder1.append("'" + cell.getStringCellValue() + "', ");
                                    }
                                    break;

                                case Cell.CELL_TYPE_NUMERIC:
                                    if (DateUtil.isCellDateFormatted(cell)) {
                                        String dateStr = String.valueOf(cell.getDateCellValue());
                                        System.out.println("date : "+dateStr);
                                        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                                        Date date = (Date) formatter.parse(dateStr);
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(date);
                                        String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" +  cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                                        System.out.println("formatedDate : " + formatedDate);
                                        if( cal.get(Calendar.HOUR) == 0 && cal.get(Calendar.MINUTE) == 0 && cal.get(Calendar.SECOND) == 0) {
                                            String formatedDate1 = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
                                            stringBuilder1.append("TO_DATE('" + formatedDate1 + "', 'DD/MM/YYYY'), ");
                                            break;
                                        }
                                        else {
                                            stringBuilder1.append("TO_TIMESTAMP('" + formatedDate + "','DD-MM-YYYY HH24:MI:SS'), ");
                                            System.out.println("formatedDate : " + formatedDate);
                                            break;
                                        }


                                    } else {
                                        System.out.print("NUMERIC" + cell.getRawValue() + "\t");

                                        if(String.valueOf(cell.getNumericCellValue()).contains("E")) {
                                            stringBuilder1.append("'" + BigDecimal.valueOf(cell.getNumericCellValue()).toPlainString() + "', ");
                                            break;
                                        }
                                        else {
                                            stringBuilder1.append("'" + cell.getRawValue() + "', ");
                                            break;
                                        }

                                    }

                                case Cell.CELL_TYPE_BOOLEAN:
                                    stringBuilder1.append("'" + cell.getBooleanCellValue() + "', ");
                                    break;

                                case Cell.CELL_TYPE_FORMULA:

                                    System.out.println("Formula is " + cell.getCellFormula());
                                    switch (cell.getCachedFormulaResultType()) {
                                        case Cell.CELL_TYPE_NUMERIC:
                                            System.out.println("Last evaluated as: " + cell.getNumericCellValue());
                                            if(String.valueOf(cell.getNumericCellValue()).contains("E")) {
                                                stringBuilder1.append("'" + BigDecimal.valueOf(cell.getNumericCellValue()).toPlainString() + "', ");
                                                break;
                                            }
                                            else {
                                                stringBuilder1.append("'" + cell.getRawValue() + "', ");
                                                break;
                                            }

                                        case Cell.CELL_TYPE_STRING:
                                            stringBuilder1.append("'" + cell.getStringCellValue() + "', ");
                                            System.out.println("Last evaluated  string as as \"" + cell.getRichStringCellValue() + "\"");
                                            break;
                                    }
                                    break;

                                default:
                                    stringBuilder1.append("' ', ");
                                    break;

                            }
                        } else {
                            System.out.println("Cell Null hence appending blank");
                            stringBuilder1.append("'', ");
                        }

                    } else {
                        System.out.println("Cell Null hence appending blank");
                        stringBuilder1.append("' ', ");
                    }
                }
                int lstindexCommaVal = stringBuilder1.lastIndexOf(",");
                stringBuilder1.deleteCharAt(lstindexCommaVal);
                stringBuilder1.append(")");
                String sql = insert + stringBuilder1.toString();

                stmt.addBatch(sql);
                cmtcnt++;

                System.out.println("final sql is : " + sql);
                System.out.println("The  commitcount is : [ "+cmtcnt+ " ]");

                if( cmtcnt % commitsize == 0 || cmtcnt == rowcount-1 ){
                    System.out.println("The  commitsize is : [ "+commitsize+ " ]");
                    int[] k =  stmt.executeBatch();
                    System.out.println("k--> " + k);
                    con.commit();
                    System.out.println("Batch executed  sucessfully");
                    stmt.clearBatch();
                }

            }
            statusList.add("success");
            statusList.add(String.valueOf(cmtcnt));
            return statusList;
        } catch (Exception e) {
            e.printStackTrace();
            statusList.add(e.toString());
            return statusList;
        } finally {
            if (con != null) {
                try {
                    con.close();
                    if( stmt != null) {
                        stmt.close();
                    }
                    if( ps != null) {
                        ps.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        List<String> status = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        System.out.print("Enter your FileName with path: ");
        String fileName = in.nextLine();
        System.out.print("Enter the  Table Name : ");
        String tableName = in.nextLine();
        Xcelread  insert = new Xcelread();
        try {
            status = insert.insertXcl(fileName,tableName);
            if(status.get(0) == "success") {
                String sql = "Insert into CUSTOM_CL5_UPLOAD_STATS (FILENAME, FILE_UPLOAD_STATS, TABLENAME, CREATED_TIME, UPDATED_TIME) values (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";
                try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
                    ps.setString(1,fileName);
                    ps.setString(2,"C");
                    ps.setString(3,tableName);
                    int i  = ps.executeUpdate();
                    System.out.println("Insert status : [ "+i+ " ]");
                    con.commit();
                    System.out.println("Sucesfully inserted on the table : [" +tableName+ "] with rows : ["+status.get(1)+"]");
                }
                catch (Exception e){
                    cxpsLogger.info("Exception during insertion of file for data insertion");
                    e.printStackTrace();
                }
            }
            else {
                String sql = "Insert into CUSTOM_CL5_UPLOAD_STATS (FILENAME, FILE_UPLOAD_STATS, TABLENAME, EXCEPTION, CREATED_TIME, UPDATED_TIME) values (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";
                try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
                    ps.setString(1,fileName);
                    ps.setString(2,"E");
                    ps.setString(3,tableName);
                    ps.setString(4,status.get(0));
                    int i  = ps.executeUpdate();
                    System.out.println("Insert status : [ "+i+ " ]");
                    con.commit();
                }
                catch (Exception e){
                    cxpsLogger.info("Exception during insertion of file for data insertion");
                    e.printStackTrace();
                }
                System.out.println("Exception While insertion : ["+status.get(0)+"]");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        in.close();
    }



    public static boolean isValidDate(String inDate) {

        boolean val ;

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:ms");
            dateFormat.setLenient(false);
            dateFormat.parse(inDate.trim());
            return true;

        } catch (ParseException pe) {

            try {
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date = (Date) formatter.parse(inDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                System.out.println("formatedDate : " + formatedDate);
                return true;
            }
            catch (ParseException pe2){
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh.mm.ss.ssssss");
                    dateFormat.setLenient(false);
                    dateFormat.parse(inDate.trim());
                    return true;
                }
                catch (ParseException  pe3){
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.sss");
                        dateFormat.setLenient(false);
                        dateFormat.parse(inDate.trim());
                        return true;
                    }
                    catch (ParseException  pe4){
                        try {
                            //Validating the format
                            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy hh.mm.ss.S aa");
                            Date date = df.parse(inDate);


                            //Removing the Milliseconds
                            String[] array = inDate.split(" ");
                            String finalDate = array[0] + " " + array[1].substring(0, array[1].lastIndexOf(".")) + " " + array[2];
                            System.out.println("date after removing : "+finalDate);


                            //Formatting the date time
                            df = new SimpleDateFormat("dd-MMM-yy hh.mm.ss aa");
                            date = df.parse(finalDate);
                            System.out.println(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date));
                            return true;

                        } catch (IllegalArgumentException e) {
                            return false;
                        } catch (NullPointerException e) {
                            return false;
                        } catch (ParseException e) {
                            return false;
                        }
                    }
                }

            }
        }
    }

    public static String getDate(String inDate) {


        try {

            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:ms");
            Date date = (Date) formatter.parse(inDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
            System.out.println("formatedDate : " + formatedDate);
            return formatedDate;

        } catch (ParseException pe) {

            try {
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date = (Date) formatter.parse(inDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                System.out.println("formatedDate : " + formatedDate);
                return formatedDate;
            }
            catch (ParseException pe2){
                try {
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH.mm.ss:ms");
                    Date date = (Date) formatter.parse(inDate);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                    System.out.println("formatedDate : " + formatedDate);
                    return formatedDate;
                }
                catch (ParseException  pe3){
                    try {
                        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.sss");
                        Date date = (Date) formatter.parse(inDate);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                        System.out.println("formatedDate : " + formatedDate);
                        return formatedDate;
                    }
                    catch (ParseException  pe4){
                        try {
                            //Validating the format
                            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy hh.mm.ss.S aa");
                            Date date = df.parse(inDate);


                            //Removing the Milliseconds
                            String[] array = inDate.split(" ");
                            String finalDate = array[0] + " " + array[1].substring(0, array[1].lastIndexOf(".")) + " " + array[2];
                            System.out.println("date after removing : "+finalDate);


                            //Formatting the date time
                            df = new SimpleDateFormat("dd-MMM-yy hh.mm.ss aa");
                            date = df.parse(finalDate);
                            System.out.println(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date));
                            return (new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date));

                        } catch (IllegalArgumentException e) {
                            return null;
                        } catch (NullPointerException e) {
                            return null;
                        } catch (ParseException e) {
                            return null;
                        }
                    }
                }

            }
        }
    }


}
