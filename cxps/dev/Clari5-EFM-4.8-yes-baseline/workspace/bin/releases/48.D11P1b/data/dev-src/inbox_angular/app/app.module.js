"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var nbb_main_component_1 = require("./components/nbb.main.component");
var nbb_data_service_1 = require("./services/nbb.data.service");
var ngx_mydatepicker_1 = require("ngx-mydatepicker");
var nbb_service_1 = require("./services/nbb.service");
var angular_2_dropdown_multiselect_1 = require("angular-2-dropdown-multiselect");
var mention_1 = require("angular-mentions/mention");
var kar_inbox_component_1 = require("./inbox/kar.inbox.component");
var primeng_1 = require("primeng/primeng");
var animations_1 = require("@angular/platform-browser/animations");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            nbb_main_component_1.NbbMainComponent, kar_inbox_component_1.KarInboxComponent
        ],
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            forms_1.ReactiveFormsModule,
            forms_1.FormsModule,
            ngx_mydatepicker_1.NgxMyDatePickerModule.forRoot(),
            angular_2_dropdown_multiselect_1.MultiselectDropdownModule,
            mention_1.MentionModule,
            primeng_1.PanelMenuModule,
            animations_1.BrowserAnimationsModule
        ],
        providers: [
            nbb_data_service_1.NbbDataService, nbb_service_1.NbbService
        ],
        bootstrap: [
            nbb_main_component_1.NbbMainComponent
        ]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map