cxps.events.event.nft-debit-card-request{
  table-name : EVENT_NFT_DEBITCARDREQUEST
  event-mnemonic: NDC
  workspaces : {
    CUSTOMER: customer_id
  }
  event-attributes : {
	account_id: {db : true ,raw_name : Account_Id ,type : "string:200"  ,custom-getter:Account_Id}
	customer_id: {db : true ,raw_name : Customer_ID ,type : "string:200" ,custom-getter:Customer_Id}
	Card_no: {db : true ,raw_name : Card_No ,type : "string:200" ,custom-getter:Card_No}
	sys_time: {db : true ,raw_name : sys_time ,type : timestamp ,custom-getter:Sys_time}
	host_id: {db : true ,raw_name : host_id ,type : "string:200" ,custom-getter:Host_id}
	eventts: {db : true ,raw_name : eventts ,type : timestamp }
	}
}
