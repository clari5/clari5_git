// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_ChequeReturnEventMapper extends EventMapper<NFT_ChequeReturnEvent> {

public NFT_ChequeReturnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_ChequeReturnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_ChequeReturnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_ChequeReturnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_ChequeReturnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_ChequeReturnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_ChequeReturnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTransactionNumonicCode());
            preparedStatement.setString(i++, obj.getChequeNumber());
            preparedStatement.setString(i++, obj.getBranchIdDesc());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getAcctStatus());
            preparedStatement.setString(i++, obj.getRejectionCode());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getRejectionReason());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getCustId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_CHEQUERETURN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_ChequeReturnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_ChequeReturnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CHEQUERETURN"));
        putList = new ArrayList<>();

        for (NFT_ChequeReturnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TRANSACTION_NUMONIC_CODE",  obj.getTransactionNumonicCode());
            p = this.insert(p, "CHEQUE_NUMBER",  obj.getChequeNumber());
            p = this.insert(p, "BRANCH_ID_DESC",  obj.getBranchIdDesc());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            p = this.insert(p, "REJECTION_CODE",  obj.getRejectionCode());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "REJECTION_REASON",  obj.getRejectionReason());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_CHEQUERETURN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_CHEQUERETURN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_CHEQUERETURN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_ChequeReturnEvent obj = new NFT_ChequeReturnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTransactionNumonicCode(rs.getString("TRANSACTION_NUMONIC_CODE"));
    obj.setChequeNumber(rs.getString("CHEQUE_NUMBER"));
    obj.setBranchIdDesc(rs.getString("BRANCH_ID_DESC"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));
    obj.setRejectionCode(rs.getString("REJECTION_CODE"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setRejectionReason(rs.getString("REJECTION_REASON"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setCustId(rs.getString("CUST_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_CHEQUERETURN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_ChequeReturnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_ChequeReturnEvent> events;
 NFT_ChequeReturnEvent obj = new NFT_ChequeReturnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CHEQUERETURN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_ChequeReturnEvent obj = new NFT_ChequeReturnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTransactionNumonicCode(getColumnValue(rs, "TRANSACTION_NUMONIC_CODE"));
            obj.setChequeNumber(getColumnValue(rs, "CHEQUE_NUMBER"));
            obj.setBranchIdDesc(getColumnValue(rs, "BRANCH_ID_DESC"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));
            obj.setRejectionCode(getColumnValue(rs, "REJECTION_CODE"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setRejectionReason(getColumnValue(rs, "REJECTION_REASON"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CHEQUERETURN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CHEQUERETURN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TRANSACTION_NUMONIC_CODE\",\"CHEQUE_NUMBER\",\"BRANCH_ID_DESC\",\"EVENTTS\",\"HOST_ID\",\"ACCT_ID\",\"USER_ID\",\"BRANCH_ID\",\"ACCT_STATUS\",\"REJECTION_CODE\",\"SYS_TIME\",\"REJECTION_REASON\",\"ACCT_NAME\",\"CUST_ID\"" +
              " FROM EVENT_NFT_CHEQUERETURN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TRANSACTION_NUMONIC_CODE],[CHEQUE_NUMBER],[BRANCH_ID_DESC],[EVENTTS],[HOST_ID],[ACCT_ID],[USER_ID],[BRANCH_ID],[ACCT_STATUS],[REJECTION_CODE],[SYS_TIME],[REJECTION_REASON],[ACCT_NAME],[CUST_ID]" +
              " FROM EVENT_NFT_CHEQUERETURN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TRANSACTION_NUMONIC_CODE`,`CHEQUE_NUMBER`,`BRANCH_ID_DESC`,`EVENTTS`,`HOST_ID`,`ACCT_ID`,`USER_ID`,`BRANCH_ID`,`ACCT_STATUS`,`REJECTION_CODE`,`SYS_TIME`,`REJECTION_REASON`,`ACCT_NAME`,`CUST_ID`" +
              " FROM EVENT_NFT_CHEQUERETURN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_CHEQUERETURN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TRANSACTION_NUMONIC_CODE\",\"CHEQUE_NUMBER\",\"BRANCH_ID_DESC\",\"EVENTTS\",\"HOST_ID\",\"ACCT_ID\",\"USER_ID\",\"BRANCH_ID\",\"ACCT_STATUS\",\"REJECTION_CODE\",\"SYS_TIME\",\"REJECTION_REASON\",\"ACCT_NAME\",\"CUST_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TRANSACTION_NUMONIC_CODE],[CHEQUE_NUMBER],[BRANCH_ID_DESC],[EVENTTS],[HOST_ID],[ACCT_ID],[USER_ID],[BRANCH_ID],[ACCT_STATUS],[REJECTION_CODE],[SYS_TIME],[REJECTION_REASON],[ACCT_NAME],[CUST_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TRANSACTION_NUMONIC_CODE`,`CHEQUE_NUMBER`,`BRANCH_ID_DESC`,`EVENTTS`,`HOST_ID`,`ACCT_ID`,`USER_ID`,`BRANCH_ID`,`ACCT_STATUS`,`REJECTION_CODE`,`SYS_TIME`,`REJECTION_REASON`,`ACCT_NAME`,`CUST_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

