// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_CHEQUERETURN", Schema="rice")
public class NFT_ChequeReturnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String transactionNumonicCode;
       @Field(size=200) public String chequeNumber;
       @Field(size=200) public String branchIdDesc;
       @Field public java.sql.Timestamp eventts;
       @Field(size=200) public String hostId;
       @Field(size=200) public String acctId;
       @Field(size=200) public String userId;
       @Field(size=200) public String branchId;
       @Field(size=200) public String acctStatus;
       @Field(size=200) public String rejectionCode;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String rejectionReason;
       @Field(size=200) public String acctName;
       @Field(size=200) public String custId;


    @JsonIgnore
    public ITable<NFT_ChequeReturnEvent> t = AEF.getITable(this);

	public NFT_ChequeReturnEvent(){}

    public NFT_ChequeReturnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("ChequeReturn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTransactionNumonicCode(json.getString("Transaction_numonic_code"));
            setChequeNumber(json.getString("Cheque_Number"));
            setBranchIdDesc(json.getString("branchiddesc"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setHostId(json.getString("host_id"));
            setAcctId(json.getString("acct_id"));
            setUserId(json.getString("user_id"));
            setBranchId(json.getString("branchid"));
            setAcctStatus(json.getString("acct_status"));
            setRejectionCode(json.getString("Rejection_Code"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setRejectionReason(json.getString("Rejection_Reason"));
            setAcctName(json.getString("acct_name"));
            setCustId(json.getString("cust_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NCR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTransactionNumonicCode(){ return transactionNumonicCode; }

    public String getChequeNumber(){ return chequeNumber; }

    public String getBranchIdDesc(){ return branchIdDesc; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getHostId(){ return hostId; }

    public String getAcctId(){ return acctId; }

    public String getUserId(){ return userId; }

    public String getBranchId(){ return branchId; }

    public String getAcctStatus(){ return acctStatus; }

    public String getRejectionCode(){ return rejectionCode; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getRejectionReason(){ return rejectionReason; }

    public String getAcctName(){ return acctName; }

    public String getCustId(){ return custId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTransactionNumonicCode(String val){ this.transactionNumonicCode = val; }
    public void setChequeNumber(String val){ this.chequeNumber = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setRejectionCode(String val){ this.rejectionCode = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setRejectionReason(String val){ this.rejectionReason = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustId(String val){ this.custId = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getTransaction_numonic_code(){ return transactionNumonicCode; }
    @JsonIgnore
    public String getCheque_Number(){ return chequeNumber; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }
    @JsonIgnore
    public String getAcct_id(){ return acctId; }
    @JsonIgnore
    public String getUser_id(){ return userId; }
    @JsonIgnore
    public String getAcct_status(){ return acctStatus; }
    @JsonIgnore
    public String getRejection_Code(){ return rejectionCode; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getRejection_Reason(){ return rejectionReason; }
    @JsonIgnore
    public String getAcct_name(){ return acctName; }
    @JsonIgnore
    public String getCust_id(){ return custId; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_ChequeReturnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_ChequeReturn");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "ChequeReturn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}