package clari5.custom.yes.integration;


import clari5.custom.yes.integration.data.ITableData;
import clari5.custom.yes.integration.queue.DataQueue;
import cxps.apex.utils.CxpsLogger;


import java.util.List;


public class EventLoader{
    protected static CxpsLogger logger = CxpsLogger.getLogger(EventLoader.class);
    public List<ITableData> getFreshEvents(String tableName) throws Exception {

        DataQueue d=new DataQueue();
        List<ITableData>  data= d.load(tableName);
        return data;

        }
}