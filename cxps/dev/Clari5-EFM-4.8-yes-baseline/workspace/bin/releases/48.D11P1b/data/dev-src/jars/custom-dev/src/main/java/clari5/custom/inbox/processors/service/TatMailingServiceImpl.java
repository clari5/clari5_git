package clari5.custom.inbox.processors.service;

import clari5.custom.dedupe.DedupDaemon;
import clari5.custom.inbox.processors.db.CustomIboxData;
import clari5.custom.inbox.processors.tat.Cl5IbxRetry;
import clari5.custom.inbox.processors.tat.CustomCL5IbxItem;
import clari5.custom.services.KTKMailingServices;
import clari5.custom.services.MailFields;
import clari5.custom.services.MailServices;
import clari5.custom.services.SMTPMail;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.util.UUID.randomUUID;

public class TatMailingServiceImpl implements TatMailingService {
    private static final CxpsLogger logger = CxpsLogger.getLogger(TatMailingServiceImpl.class);

    static Hocon smtp;

    static {
        smtp = new Hocon();
        smtp.loadFromContext("smtp.conf");
    }

    @Override
    public synchronized void sendMail(ConcurrentLinkedQueue<CustomCL5IbxItem> items) {
        logger.debug("inside send mail");
        //System.out.println("inside send mail");
        while (!items.isEmpty()) {
            int res = 0;
            CustomCL5IbxItem ibxItem = items.poll();
            if (CustomIboxData.checkItemIsPresent(ibxItem)) {
                //continue;
                break;
            }
            else {

                logger.debug("group id: " + ibxItem.getAssignedTo());
                //System.out.println("group id: " + ibxItem.getAssignedTo());
                List<String> userIDList = getEmailIdBasedGroupId(ibxItem.getAssignedTo());
                logger.debug("List of user id's: " + userIDList);
                //System.out.println("List of user id's: " + userIDList);
                List<String> managerEmailIds = getManagerEmailIds(userIDList, ibxItem.getAssignedTo());
                logger.debug("List of manager email id's: " + managerEmailIds);
                //System.out.println("List of manager email id's: " + managerEmailIds);

                for (String message : managerEmailIds) {
                    MailServices mailServices = new KTKMailingServices(new SMTPMail(), smtp);
                    MailFields mailFields = mailServices.getMailFields();

                    String[] messageList = message.split(":");
                    //mail to field is set
                    mailFields.setTo(messageList[2]);

                    String ibx[] = ibxItem.getParentId().split("\\|");
                    //mail subject line

                    String subject = smtp.getString("inbox-ms.tat-subject1") +
                            ibx[3].trim() + ":" +
                            ibx[0].trim()
                            + smtp.getString("inbox-ms.tat-subject2");
                    mailFields.setSubject(subject);

                    //mail body
                    String mailBody = smtp.getString("inbox-ms.tat-message1") +
                            smtp.getString("inbox-ms.tat-message2") +
                            "User: [" + messageList[0].trim() + "] " +
                            "Name: [" + messageList[1].trim() + "] " +
                            smtp.getString("inbox-ms.tat-message3") +
                            "[" + ibx[0].trim() + "]" +
                            smtp.getString("inbox-ms.tat-message4") +
                            "[" + CustomIboxData.hocon.getString("tat.breachTime") + " min" + "]";
                    mailFields.setMessage(mailBody);

                    //before sending mail verifying all the data
                    logger.debug("before sending mail: " + mailFields);
                    //System.out.println("before sending mail: " + mailFields);

                    //setting the data for each user
                    Cl5IbxRetry retry = new Cl5IbxRetry();
                    retry.setRetryID(UUID.randomUUID().toString());
                    retry.setItemId(ibxItem.getItemId());
                    retry.setEmailId(messageList[2]);
                    retry.setEmailSubject(subject);
                    retry.setEmailBody(mailBody);

                    //sending mail
                    res = mailServices.sendIndvMail(mailFields);


                    if (res == 1) {
                        retry.setEmailFlag("C");
                        retry.setRetryCount(String.valueOf(0));
                        CustomIboxData.insertCl5IbxItemData(retry);
                    } else {
                        retry.setEmailFlag("R");
                        retry.setRetryCount(String.valueOf(0));
                        CustomIboxData.insertCl5IbxItemData(retry);
                    }
                }
            }
        }
    }


    /**
     * get userId based on group id
     *
     * @param groupID
     * @return
     */
    public static List<String> getEmailIdBasedGroupId(String groupID) {
        logger.debug("group Id: " + groupID);
        //System.out.println("group Id: " + groupID);
        List<String> userIDList = new ArrayList<>();
        try (RDBMSSession session = Rdbms.getAppSession();
             CxConnection connection = session.getCxConnection();
             PreparedStatement ps = connection.prepareStatement("select USER_ID from CL5_GRP_USER_MAPPING where GROUP_ID = ?")) {
            ps.setString(1, groupID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    userIDList.add(rs.getString("USER_ID") != null ? rs.getString("USER_ID") : "");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("userId List: " + userIDList);
        //System.out.println("userId List: " + userIDList);
        return userIDList;
    }

    /**
     * get manager email Ids for all listed users
     *
     * @param userIds
     * @return
     */
    public static List<String> getManagerEmailIds(List<String> userIds, String groupId) {
        List<String> emailIds = new ArrayList<>();
        if (!userIds.isEmpty() && userIds.size() > 0) {
            logger.debug("inside getManagerEmailIds");
            logger.debug("userId's list for manager escalate : " + userIds);
            StringBuilder sb = new StringBuilder("");
            userIds.forEach((users) -> sb.append("'").append(users).append("',"));
            logger.debug("data inside string buffer object" + sb.toString());
            sb.replace(sb.lastIndexOf(","), sb.length(), "");
            logger.debug("List of userId's in: " + sb.toString());

            String query = "SELECT EMPLOYEE_ID, EMPLOYEE_NAME, MANAGER_EMAIL FROM EMPLOYEE_MANAGER_MAPPING WHERE EMPLOYEE_ID IN (" + sb.toString() + ")";
            logger.debug("query for manager mapping: " + query);
            //System.out.println("query for manager mapping: " + query);
            try (RDBMSSession session = Rdbms.getAppSession();
                 CxConnection connection = session.getCxConnection();
                 PreparedStatement ps = connection.prepareStatement(query)) {
                try (ResultSet rs = ps.executeQuery();) {
                    while (rs.next()) {
                        String str = rs.getString("EMPLOYEE_ID");
                        str += ":" + rs.getString("EMPLOYEE_NAME");
                        str += ":" + rs.getString("MANAGER_EMAIL");
                        emailIds.add(str);
                    }
                }
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
            logger.debug("Manager email Id's fetched for employee ----> " + emailIds);
            //System.out.println("Manager email Id's fetched for employee ----> " + emailIds);
            return emailIds;
        } else {
            logger.debug("User Id not found for group id: " + groupId);
            //System.out.println("User Id not found for group id: " + groupId);
            return emailIds;
        }
    }

    @Override
    public void finalize() {
        logger.debug("finalize method called upon gc to avoid memory leaks");
    }
}
