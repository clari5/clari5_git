package clari5.custom.yes.integration.builder;

import java.util.HashMap;
import clari5.custom.yes.integration.builder.data.Message;
import clari5.custom.yes.integration.builder.data.VarSource;

public class MsgMetaData extends HashMap<String, Message> {
	private static final long serialVersionUID = 8649533816941672408L;
	public Message getVarSources(String eventName) {
		return this.get(eventName);
	}
	public VarSource getVarSource(String eventName, String fieldName) {
		Message vsl = this.get(eventName);
		VarSource opt = null;
		for(VarSource vs : vsl) {
			if(vs.getName().equals(fieldName)) {
				opt = vs;
				break;
			}
		}
		return opt;
	}
}
