cxps.events.event.nft-eipoapplicant{
  table-name : EVENT_NFT_EIPOAPPLICANT
  event-mnemonic: EIP
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        applicant-name: {db : true ,raw_name : applicant_name ,type: "string:200" }
        applicant-pan: {db : true ,raw_name : applicant_pan ,type: "string:200" }
        applicant-unique-identifier: {db : true ,raw_name : applicant_unique_identifier ,type: "string:200" }
        applicant-type: {db : true ,raw_name : applicant_type ,type: "string:200" }
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
	session-id:{db : true ,raw_name :session_id ,type : "string:200"}
}
}
