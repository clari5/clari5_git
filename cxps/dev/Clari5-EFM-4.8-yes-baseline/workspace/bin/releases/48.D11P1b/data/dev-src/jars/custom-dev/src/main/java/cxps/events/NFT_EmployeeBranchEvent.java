// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_EMPLOYEEBRANCH", Schema="rice")
public class NFT_EmployeeBranchEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp branchJoiningDate;
       @Field(size=200) public String employeeId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String currentBranch;
       @Field(size=200) public String noticePeriod;
       @Field(size=200) public String branch;
       @Field(size=200) public String desigNation;


    @JsonIgnore
    public ITable<NFT_EmployeeBranchEvent> t = AEF.getITable(this);

	public NFT_EmployeeBranchEvent(){}

    public NFT_EmployeeBranchEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("EmployeeBranch");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBranchJoiningDate(EventHelper.toTimestamp(json.getString("branch_Joining_Date")));
            setEmployeeId(json.getString("employee_ID"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCurrentBranch(json.getString("current_Branch"));
            setNoticePeriod(json.getString("notice_period"));
            setBranch(json.getString("branch"));
            setDesigNation(json.getString("designation"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NE"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getBranchJoiningDate(){ return branchJoiningDate; }

    public String getEmployeeId(){ return employeeId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCurrentBranch(){ return currentBranch; }

    public String getNoticePeriod(){ return noticePeriod; }

    public String getBranch(){ return branch; }

    public String getDesigNation(){ return desigNation; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBranchJoiningDate(java.sql.Timestamp val){ this.branchJoiningDate = val; }
    public void setEmployeeId(String val){ this.employeeId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCurrentBranch(String val){ this.currentBranch = val; }
    public void setNoticePeriod(String val){ this.noticePeriod = val; }
    public void setBranch(String val){ this.branch = val; }
    public void setDesigNation(String val){ this.desigNation = val; }

    /* Custom Getters*/
    @JsonIgnore
    public java.sql.Timestamp getBranch_Joining_Date(){ return branchJoiningDate; }
    @JsonIgnore
    public String getEmployee_ID(){ return employeeId; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getCurrent_Branch(){ return currentBranch; }
    @JsonIgnore
    public String getNotice_period(){ return noticePeriod; }
    @JsonIgnore
    public String getDesignation(){ return desigNation; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_EmployeeBranchEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

       // String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.employeeId);
       // wsInfoSet.add(new WorkspaceInfo("User", userKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_EmployeeBranch");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "EmployeeBranch");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
