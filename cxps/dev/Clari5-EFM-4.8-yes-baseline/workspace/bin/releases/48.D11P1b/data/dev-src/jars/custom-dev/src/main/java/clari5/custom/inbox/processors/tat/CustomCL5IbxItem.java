package clari5.custom.inbox.processors.tat;

import java.sql.Timestamp;

public class CustomCL5IbxItem {
    private String message;
    private java.sql.Timestamp updateOn;
    private String hasAttachments;
    private String itemId;
    private String itemType;
    private java.sql.Timestamp tat;
    private String notificationId;
    private String createdBy;
    private String isRead;
    private java.sql.Timestamp createdOn;
    private String parentId;
    private String updatedBy;
    private String messageFormat;
    private String clobMessage;
    private String state;
    private String appId;
    private String assignedTo;
    private String emailFlag;
    private String retryCount;
    private String emailData;

    public CustomCL5IbxItem() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Timestamp updateOn) {
        this.updateOn = updateOn;
    }

    public String getHasAttachments() {
        return hasAttachments;
    }

    public void setHasAttachments(String hasAttachments) {
        this.hasAttachments = hasAttachments;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Timestamp getTat() {
        return tat;
    }

    public void setTat(Timestamp tat) {
        this.tat = tat;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(String messageFormat) {
        this.messageFormat = messageFormat;
    }

    public String getClobMessage() {
        return clobMessage;
    }

    public void setClobMessage(String clobMessage) {
        this.clobMessage = clobMessage;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getEmailFlag() {
        return emailFlag;
    }

    public void setEmailFlag(String emailFlag) {
        this.emailFlag = emailFlag;
    }

    public String getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(String retryCount) {
        this.retryCount = retryCount;
    }

    public String getEmailData() {
        return emailData;
    }

    public void setEmailData(String emailData) {
        this.emailData = emailData;
    }

    @Override
    public String toString() {
        return "CustomCL5IbxItem{" +
                "message='" + message + '\'' +
                ", updateOn=" + updateOn +
                ", hasAttachments='" + hasAttachments + '\'' +
                ", itemId='" + itemId + '\'' +
                ", itemType='" + itemType + '\'' +
                ", tat=" + tat +
                ", notificationId='" + notificationId + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", isRead='" + isRead + '\'' +
                ", createdOn=" + createdOn +
                ", parentId='" + parentId + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", messageFormat='" + messageFormat + '\'' +
                ", clobMessage='" + clobMessage + '\'' +
                ", state='" + state + '\'' +
                ", appId='" + appId + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", emailFlag='" + emailFlag + '\'' +
                ", retryCount='" + retryCount + '\'' +
                ", emailData='" + emailData + '\'' +
                '}';
    }
}