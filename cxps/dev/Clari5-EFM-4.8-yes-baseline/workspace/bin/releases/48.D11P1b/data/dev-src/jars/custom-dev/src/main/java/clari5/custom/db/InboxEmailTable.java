package clari5.custom.db;

import clari5.custom.yes.db.InboxEmail;
import clari5.custom.yes.db.InboxEmailCriteria;
import clari5.custom.yes.db.InboxEmailSummary;
import clari5.custom.yes.db.mappers.InboxEmailMapper;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.utils.CxpsLogger;

import java.util.List;


/**
 * @author shishir
 * @since 11/01/2018
 */

public class InboxEmailTable extends DBConnection implements IDBOperation {
    private static final CxpsLogger logger = CxpsLogger.getLogger(InboxEmailTable.class);

    InboxEmailTable() {
        rdbmsConnection = new RDBMSConnection();

    }

    /**
     * @param branchId
     * @return return email id from inbox_email table based on branchId
     */
    public String getEmailId(String branchId) {
        RDBMSSession rdbmsSession = getSession();
        String emailId = "";
        try {

            InboxEmailMapper mapper = rdbmsSession.getMapper(InboxEmailMapper.class);
            InboxEmailCriteria criteria = new InboxEmailCriteria();
            criteria.setBranchId(branchId);

            List<InboxEmailSummary> inboxEmailSummaryList = mapper.searchInboxEmail(criteria);
            for (InboxEmailSummary inboxEmailSummary : inboxEmailSummaryList) {

                emailId = inboxEmailSummary.getEmailId();
            }

            return emailId;

        } catch (Exception e) {
            logger.error("failed to fetch email id from inbox_email table " + e.getMessage());
        } finally {

            closeRDMSSession();
        }
        return null;
    }

    @Override
    public Object getAllRecords() {
        return null;
    }

    @Override
    public Object select(Object o) {

        RDBMSSession rdbmsSession = getSession();
        String branchId = (String) o;

        try {
            InboxEmail inboxEmail = new InboxEmail();

            inboxEmail.setMapper(rdbmsSession);
            inboxEmail.setBranchId(branchId);

            return inboxEmail.select();

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from INBOX_EMAIL table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public int insert(Object o) {
        return 0;
    }

    @Override
    public int update(Object o) {
        return 0;
    }

    @Override
    public int delete(Object o) {
        return 0;
    }
}
