// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_DCPINCHANGEREQUEST", Schema="rice")
public class NFT_DCPinChangeRequestEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String rqstChnl;
       @Field(size=200) public String eventSubtype;
       @Field(size=200) public String hostId;
       @Field(size=200) public String onlineOffline;
       @Field(size=200) public String eventType;
       @Field(size=200) public String succFailFlg;
       @Field(size=200) public String cardNo;
       @Field(size=200) public String errorDesc;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String eventName;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String custId;
       @Field(size=200) public String timeSlot;


    @JsonIgnore
    public ITable<NFT_DCPinChangeRequestEvent> t = AEF.getITable(this);

	public NFT_DCPinChangeRequestEvent(){}

    public NFT_DCPinChangeRequestEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("DCPinChangeRequest");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setRqstChnl(json.getString("rqst_chnl"));
            setEventSubtype(json.getString("eventsubtype"));
            setHostId(json.getString("host_id"));
            setOnlineOffline(json.getString("Online_offline"));
            setEventType(json.getString("eventtype"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setCardNo(json.getString("card_no"));
            setErrorDesc(json.getString("error_desc"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setEventName(json.getString("event_name"));
            setErrorCode(json.getString("error_code"));
            setCustId(json.getString("cust_id"));
            setTimeSlot(json.getString("time_slot"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NDP"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getRqstChnl(){ return rqstChnl; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getHostId(){ return hostId; }

    public String getOnlineOffline(){ return onlineOffline; }

    public String getEventType(){ return eventType; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getCardNo(){ return cardNo; }

    public String getErrorDesc(){ return errorDesc; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getEventName(){ return eventName; }

    public String getErrorCode(){ return errorCode; }

    public String getCustId(){ return custId; }

    public String getTimeSlot(){ return timeSlot; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setRqstChnl(String val){ this.rqstChnl = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setOnlineOffline(String val){ this.onlineOffline = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setCardNo(String val){ this.cardNo = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setTimeSlot(String val){ this.timeSlot = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getRqst_chnl(){ return rqstChnl; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }
    @JsonIgnore
    public String getOnline_offline(){ return onlineOffline; }
    @JsonIgnore
    public String getSucc_fail_flg(){ return succFailFlg; }
    @JsonIgnore
    public String getCard_no(){ return cardNo; }
    @JsonIgnore
    public String getError_desc(){ return errorDesc; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getEvent_name(){ return eventName; }
    @JsonIgnore
    public String getError_code(){ return errorCode; }
    @JsonIgnore
    public String getCust_id(){ return custId; }
    @JsonIgnore
    public String getTime_slot(){ return timeSlot; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_DCPinChangeRequestEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_DCPinChangeRequest");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "DCPinChangeRequest");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}