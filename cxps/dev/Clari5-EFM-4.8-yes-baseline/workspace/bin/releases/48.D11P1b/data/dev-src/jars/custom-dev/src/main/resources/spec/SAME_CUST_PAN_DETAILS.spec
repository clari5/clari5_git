clari5.custom.mapper {
        entity {
                SAME_CUST_PAN_DETAILS {
                       generate = true
                        attributes:[
                                { name: CREATED_ON, type=timestamp },
                                { name: AUTO_SEQUENCE_NO ,type ="string:20" , key=true }
                                { name: MATCH_TYPE ,type ="string:100" }
				{ name: ID, type="string:30" }
                                { name: PAN ,type ="string:50" }
                                { name: CUST_ID ,type ="string:25" }
                                ]
			criteria-query {
                          name:sameCustPanDetails
                          summary =[auto-sequence-no,id,cust-id,match-type,pan,created_on]
                          where {
                               cust-id: equal-clause
                          }
                          order = ["created_on"]
                     }
                        indexes {
                                SAME_CUST_PAN_DETAILS_IDX : [ CUST_ID ]
                                 }
                        }
        }
}
