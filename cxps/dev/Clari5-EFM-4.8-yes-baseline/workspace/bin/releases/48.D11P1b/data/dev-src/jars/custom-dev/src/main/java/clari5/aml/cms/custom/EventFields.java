package clari5.aml.cms.newjira;


import cxps.apex.utils.CxpsLogger;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EventFields {
    protected static CxpsLogger logger = CxpsLogger.getLogger(EventFields.class);
    private String wsKey;
    private String wsName;
    private String eventId;
    private String factName;
    private String sysTime;

    public String getWsKey() {
        return wsKey;
    }

    public void setWsKey(String wsKey) {
        if(wsKey.contains("C_F_")) {
            this.wsKey = wsKey.substring(4);
        }else
            this.wsKey = wsKey;
    }

    public String getWsName() {
        return wsName;
    }

    public void setWsName(String wsName) {
        this.wsName = wsName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getFactName() {
        return factName;
    }

    public void setFactName(String factName) {
        this.factName = factName;
    }

    public String getSysTime() {
        System.out.println(sysTime);
        return sysTime;
    }


    public void setSysTime(Date sysTime) {
        System.out.println(sysTime);
        if (sysTime == null || "".equals(sysTime)) {
            logger.info("Value is required: sysTime");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                this.sysTime = dateFormat1.format(sysTime);
            } catch (Exception ex) {
                throw new IllegalArgumentException("Provide sysTime in the format:dd-MM-yyyy HH:mm:ss.SSS .Invalid Value>field:sysTime>value:" + sysTime);
            }
        }
    }


    @Override
    public String toString() {
        return "{\"event-type\":\"nft\",\"eventsubtype\":\"scnclosure\",\"event-name\":\"nft_scnclosure\",\"msgBody\":" +
                "\"{\'host-id\':\'F\',\'sys_time\':\'" + getSysTime() + "\',\'event-id\':\'" + getEventId() + "\',\'ws-key\':\'" + getWsKey()
                + "\',\'fact-name\':\'" + getFactName() + "\',\'ws-name\':\'" + getWsName() + "\'}\"}";
    }
}
