// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_DCPinChangeRequestEventMapper extends EventMapper<NFT_DCPinChangeRequestEvent> {

public NFT_DCPinChangeRequestEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_DCPinChangeRequestEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_DCPinChangeRequestEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_DCPinChangeRequestEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_DCPinChangeRequestEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_DCPinChangeRequestEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_DCPinChangeRequestEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getRqstChnl());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getOnlineOffline());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getCardNo());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getTimeSlot());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_DCPINCHANGEREQUEST]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_DCPinChangeRequestEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_DCPinChangeRequestEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DCPINCHANGEREQUEST"));
        putList = new ArrayList<>();

        for (NFT_DCPinChangeRequestEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "RQST_CHNL",  obj.getRqstChnl());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "ONLINE_OFFLINE",  obj.getOnlineOffline());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "CARD_NO",  obj.getCardNo());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "TIME_SLOT",  obj.getTimeSlot());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_DCPINCHANGEREQUEST"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_DCPINCHANGEREQUEST]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_DCPINCHANGEREQUEST]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_DCPinChangeRequestEvent obj = new NFT_DCPinChangeRequestEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setRqstChnl(rs.getString("RQST_CHNL"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setOnlineOffline(rs.getString("ONLINE_OFFLINE"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setCardNo(rs.getString("CARD_NO"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setTimeSlot(rs.getString("TIME_SLOT"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_DCPINCHANGEREQUEST]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_DCPinChangeRequestEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_DCPinChangeRequestEvent> events;
 NFT_DCPinChangeRequestEvent obj = new NFT_DCPinChangeRequestEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DCPINCHANGEREQUEST"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_DCPinChangeRequestEvent obj = new NFT_DCPinChangeRequestEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setRqstChnl(getColumnValue(rs, "RQST_CHNL"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setOnlineOffline(getColumnValue(rs, "ONLINE_OFFLINE"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setCardNo(getColumnValue(rs, "CARD_NO"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setTimeSlot(getColumnValue(rs, "TIME_SLOT"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DCPINCHANGEREQUEST]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DCPINCHANGEREQUEST]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"RQST_CHNL\",\"EVENT_SUBTYPE\",\"HOST_ID\",\"ONLINE_OFFLINE\",\"EVENT_TYPE\",\"SUCC_FAIL_FLG\",\"CARD_NO\",\"ERROR_DESC\",\"SYS_TIME\",\"EVENT_NAME\",\"ERROR_CODE\",\"CUST_ID\",\"TIME_SLOT\"" +
              " FROM EVENT_NFT_DCPINCHANGEREQUEST";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [RQST_CHNL],[EVENT_SUBTYPE],[HOST_ID],[ONLINE_OFFLINE],[EVENT_TYPE],[SUCC_FAIL_FLG],[CARD_NO],[ERROR_DESC],[SYS_TIME],[EVENT_NAME],[ERROR_CODE],[CUST_ID],[TIME_SLOT]" +
              " FROM EVENT_NFT_DCPINCHANGEREQUEST";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`RQST_CHNL`,`EVENT_SUBTYPE`,`HOST_ID`,`ONLINE_OFFLINE`,`EVENT_TYPE`,`SUCC_FAIL_FLG`,`CARD_NO`,`ERROR_DESC`,`SYS_TIME`,`EVENT_NAME`,`ERROR_CODE`,`CUST_ID`,`TIME_SLOT`" +
              " FROM EVENT_NFT_DCPINCHANGEREQUEST";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_DCPINCHANGEREQUEST (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"RQST_CHNL\",\"EVENT_SUBTYPE\",\"HOST_ID\",\"ONLINE_OFFLINE\",\"EVENT_TYPE\",\"SUCC_FAIL_FLG\",\"CARD_NO\",\"ERROR_DESC\",\"SYS_TIME\",\"EVENT_NAME\",\"ERROR_CODE\",\"CUST_ID\",\"TIME_SLOT\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[RQST_CHNL],[EVENT_SUBTYPE],[HOST_ID],[ONLINE_OFFLINE],[EVENT_TYPE],[SUCC_FAIL_FLG],[CARD_NO],[ERROR_DESC],[SYS_TIME],[EVENT_NAME],[ERROR_CODE],[CUST_ID],[TIME_SLOT]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`RQST_CHNL`,`EVENT_SUBTYPE`,`HOST_ID`,`ONLINE_OFFLINE`,`EVENT_TYPE`,`SUCC_FAIL_FLG`,`CARD_NO`,`ERROR_DESC`,`SYS_TIME`,`EVENT_NAME`,`ERROR_CODE`,`CUST_ID`,`TIME_SLOT`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

