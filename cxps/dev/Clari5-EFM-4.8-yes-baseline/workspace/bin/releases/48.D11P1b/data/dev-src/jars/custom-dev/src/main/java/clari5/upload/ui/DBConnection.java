package clari5.upload.ui;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import clari5.rdbms.Rdbms;
import clari5.platform.applayer.Clari5;
//import cxps.apex.ext.datalayer.utils.DbConnectionPooler;

//import java.io.IOException;
//import java.sql.Connection;

/**
 * Created by lisa
 */
public class DBConnection {


	public static Connection getDBConnection   () {
		Connection con = null;

		try {

			//Clari5.batchBootstrap("Upload","upload-clari5.conf");
			con = Rdbms.getAppConnection();
            System.out.println("Obtained Db Connection");
			return  con;
                }catch(Exception e){
                      throw new RuntimeException("Unable to get DB Connection"+e.getMessage() +" Cause "+e.getCause());
		}

	}
}





