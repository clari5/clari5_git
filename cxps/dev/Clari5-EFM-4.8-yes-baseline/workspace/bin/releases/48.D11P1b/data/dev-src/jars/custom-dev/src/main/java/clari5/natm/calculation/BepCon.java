package clari5.natm.calculation;

import clari5.custom.yes.integration.audit.LogLevel;

import clari5.custom.yes.integration.exceptions.ConfigurationException;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

public class BepCon extends Properties {
    private static CxpsLogger logger= CxpsLogger.getLogger(BepCon.class);
    private static final long serialVersionUID = -8967877852117047914L;
    private static BepCon config;
    public static int SERVER_ID = 0;
    public static int NO_OF_NODES = 0;

    private BepCon() throws Exception {
        try {
            load(Thread.currentThread().getContextClassLoader().getResourceAsStream("BatchEventProcessorConfig.properties"));
            this.NO_OF_NODES=Integer.parseInt(this.getProperty("NO_OF_NODES"));
            if (SERVER_ID == 0)
                SERVER_ID = BepCon.generateServerIdInController();
            System.out.println("SERVER_ID : SERVER_ID value = " + SERVER_ID);
        } catch (Exception e) {
            throw new ConfigurationException("Exception caught --> " + e.getMessage());
        }
    }
    private static int generateServerIdInController() throws SQLException {
        int serverId =0;
        Connection con = null;
        PreparedStatement pst = null;
        boolean bool = true;
        int i =1;
        try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
        CxConnection cxConnection=rdbmsSession.getCxConnection();) {
            logger.info("SERVER_ID : database info : "+cxConnection.getMetaData().getDatabaseProductName());
            cxConnection.setAutoCommit(true);
            while(bool) {
                try {
                    String sql = "INSERT INTO custom_controller (server_id,last_ping_time) VALUES (?,current_timestamp )";
                    logger.info("[Bepcon: NATM]SERVER_ID : Going to create server id : "+sql);
                    pst = cxConnection.prepareStatement(sql);
                    pst.setInt(1, i);
                    int updatedValue = pst.executeUpdate();
                    if(updatedValue>0) {
                        logger.info("[Bepcon: NATM]SERVER_ID : server id created successfully : "+i);
                        serverId = i;
                        bool= false;
                        break;
                    } else {
                        i++;
                        if( i > BepCon.NO_OF_NODES )
                            i = 1;
                    }
                } catch (Exception e) {
                    i++;
                    e.printStackTrace();
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
        logger.info("SERVER_ID : server id is : "+serverId);
        return serverId;
    }

    public static void updateServerIDInfoInController() {
        Connection con = null;
        PreparedStatement pst = null;
        try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
            CxConnection cxConnection=rdbmsSession.getCxConnection();) {
            String sql = "UPDATE custom_controller SET last_ping_time=current_timestamp where server_id=?";
            pst = cxConnection.prepareStatement(sql);
            pst.setInt(1, BepCon.SERVER_ID);
            int updatedValue = pst.executeUpdate();
            if (updatedValue > 0) {
                logger.info("[Bepcon: NATM]Controller Update successfully for serverID = " +BepCon.SERVER_ID + ".");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized clari5.natm.calculation.BepCon getConfig() throws Exception {
        if (config == null) {
            config = new clari5.natm.calculation.BepCon();
        }
        return config;
    }
}




