package clari5.custom.inbox.processors.cluster;

import clari5.custom.services.KTKMailingServices;
import clari5.custom.services.MailFields;
import clari5.custom.services.MailServices;
import clari5.custom.services.SMTPMail;
import clari5.inbox.Cl5IbxItem;
import clari5.inbox.model.Group;
import clari5.inbox.processors.group.BalancedGroupAssignment;
import clari5.inbox.processors.group.IGroupAssignment;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.util.Hocon;
import clari5.uac.constants.MaintenanceActions;
import clari5.uac.groups.usergroup.tables.UserGroup;
import clari5.uac.groups.usergroup.tables.UserMapping;
import cxps.apex.utils.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CustomClusterAssigner extends BalancedGroupAssignment implements IGroupAssignment {
    static Hocon smtp;

    static {
        smtp = new Hocon();
        smtp.loadFromContext("smtp.conf");
    }

    private static final CxpsLogger logger = CxpsLogger.getLogger(CustomClusterAssigner.class);

    @Override
    public Group assign(RDBMSSession rdbmsSession, CxJson cxJson) {
        Group cluster = null;
        try {
            cluster = super.assign(rdbmsSession, cxJson);
            Set<String> userIds = cluster.getUsers();
            logger.debug("Inbox users set on level one ticket assignment: --> " + userIds);
            List<String> userEmail = getEmailIds(userIds, rdbmsSession);
            logger.debug("Inbox users email to be notified level one ticket assignment: --> " + userEmail);
            MailServices mailServices = new KTKMailingServices(new SMTPMail(), smtp);
            MailFields mailFields = mailServices.getMailFields();

            mailFields.setTo(userEmail.toString().replaceAll("\\[", "")
                    .replaceAll("\\]", "").replaceAll(" ", ""));
            CxJson issueData = CxJson.parse(cxJson.get("issueData").toJson());
            String parentId = issueData.getString("parentId");
            CxJson msg = CxJson.parse(issueData.get("msg").toJson());
            String factName = msg.getString("FactName");

            mailFields.setSubject(smtp.getString("inbox-ms.smtp-subject1") + factName
                    + smtp.getString("inbox-ms.smtp-subject2"));
            mailFields.setMessage(smtp.getString("inbox-ms.smtp-message1") + smtp.getString("inbox-ms.smtp-message2")
                    + parentId + ".\t\n\n" + smtp.getString("inbox-ms.smtp-message3"));

            mailServices.sendMailToMulUser(mailFields);
            logger.debug(mailServices.getMailFields().getSmtpHost());
            logger.debug(mailServices.getMailFields().getSmtpPort());
            logger.debug(mailServices.getMailFields().getSender());
            logger.debug(mailServices.getMailFields().getSenderMailPass());
            logger.debug(mailServices.getMailFields().getTo());
            logger.debug(mailServices.getMailFields().getIoTimeout());
            logger.debug(mailServices.getMailFields().getConTimeOut());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cluster;
    }

    /*@Override
    public Group escalate(RDBMSSession rdbmsSession, Cl5IbxItem cl5IbxItem, Integer escalationLevel) throws IOException {
        return super.escalate(rdbmsSession, cl5IbxItem, escalationLevel);
    }*/

    /*@Override
    public String addUsersForMail(RDBMSSession session, Cl5IbxItem item, Set<String> mailIds) {
        logger.debug("Inside addUsersForMail");
        String groupId = item.getAssignedTo();
        logger.debug("groupId from Cl5IbxItem ---> "+groupId);
        UserGroup userGroup = new UserGroup(groupId);
        CxConnection connection = session.getCxConnection();
        List<String> groupUsers = new ArrayList<>();

        *//**
         * Fetching active users to whom mail's will be escalated
         *//*

        try {
            if (userGroup.t.load(connection)) {
                ///if (userGroup.level == 2) {
                    logger.debug("Inside level 2 usergroup...");
                    UserMapping um = new UserMapping(groupId);
                    Map<String, String> map = um.loadUsers4Group(connection);
                    logger.debug("Group Map loaded... "+ map);
                    map.forEach((k, v) -> {
                        if (v.trim().equalsIgnoreCase(MaintenanceActions.NONE.toString()))
                            groupUsers.add(k);
                    });
                //}
            }

            *//**
             * Clearing the mailid's added from product.
             *//*

            mailIds.clear();

            *//***
             * Adding Manager MailId's for TAT Based Escalation is added in set for level 2.
             *//*

            mailIds.addAll(getManagerEmailIds(groupUsers, session));

            logger.debug("Manager Mail Id's added in the set: " + mailIds);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    /**
     * Level 1 users mail id is fetched once inbox ticket is  assigned to a cluster
     *
     * @param userIds
     * @param rdbmsSession
     * @return emailIds
     */
    public static List<String> getEmailIds(Set<String> userIds, RDBMSSession rdbmsSession) {

        StringBuilder sb = new StringBuilder("");
        userIds.forEach((users) -> sb.append("'").append(users).append("',"));
        sb.replace(sb.lastIndexOf(","), sb.length(), "");

        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> emailIds = new ArrayList<>();
        Connection connection = rdbmsSession.getCxConnection();
        try {
            String query = "SELECT EMAIL_ID FROM CL5_USER_TBL WHERE USER_ID IN (" + sb.toString() + ")";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                emailIds.add(rs.getString("EMAIL_ID"));
            }
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return emailIds;
    }

    /**
     * Level 2 (Manager's) emailId's are fetched for escalation
     *
     * @param userIds
     * @param rdbmsSession
     * @return
     */
    /*public static List<String> getManagerEmailIds(List<String> userIds, RDBMSSession rdbmsSession) {

        logger.debug("inside getManagerEmailIds");
        logger.debug("userId's list for manager escalate : "+userIds);
        StringBuilder sb = new StringBuilder("");
        userIds.forEach((users) -> sb.append("'").append(users).append("',"));
        logger.debug("data inside string buffer object"+sb.toString());
        sb.replace(sb.lastIndexOf(","), sb.length(), "");
        logger.debug("List of userId's in: "+sb.toString());

        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> emailIds = new ArrayList<>();
        Connection connection = rdbmsSession.getCxConnection();
        try {
            String query = "SELECT MANAGER_EMAIL FROM EMPLOYEE_MANAGER_MAPPING WHERE EMPLOYEE_ID IN (" + sb.toString() + ")";
            logger.debug("query for manager mapping: "+query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                emailIds.add(rs.getString("MANAGER_EMAIL"));
            }
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        logger.debug("Manager email Id's fetched for employee ----> " + emailIds);
        return emailIds;
    }*/
}