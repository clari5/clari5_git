package clari5.custom.yes.integration.data;

public class FT_CoreAcctTxn extends ITableData{

    private String tableName = "FT_CORE";
    private String event_type = "FT_CoreAcctTxn";
    private String  CUST_ID;
    private String EVENT_ID;
    private String  UCIC_ID;
    private String  PRODUCT_CODE;
    private String  FCC_PRODUCT_CODE;
    private String  DRCR;
    private String  TRAN_AMOUNT;
    private String  BRANCH_ID;
    private String  TD_MATURITY_DATE;
    private String  EFF_AVAILABLE_BALANCE;
    private String  CHANNEL_ID ;
    private String  TRAN_NUMONIC_CODE;
    private String  INST_STATUS;
    private String  HOST_TYPE ;
    private String  INSTRUMENT_TYPE  ;
    private String  INSTRUMENT_NUMBER;
    private String  ATM_ID;
    private String  ISIN_NUMBER;
    private String  TRAN_DATE;
    private String  PSTD_DATE;
    private String  TRAN_ID;
    private String  REFERENCE_SRL_NUM;
    private String   VALUE_DATE;
    private String   TRAN_CRNCY_CODE;
    private String   REF_TRAN_AMT;
    private String   REF_TRAN_CRNCY;
    private String   TRAN_PARTICULAR;
    private String SYS_TIME;
    private String BANK_CODE;
    private String PSTD_FLG;
    private String ONLINE_BATCH;
    private String USER_ID;
    private String STATUS;
    private String HOST_ID;
    private String TD_LIQUIDATION_TYPE;
    private String TOD_GRANT_AMOUNT;
    private String CA_SCHEME_CODE;
    private String SYSTEM;
    private String REM_TYPE;
    private String BRANCH;
    private String TRAN_CURR;
    private String TRAN_AMT;
    private String USD_EQV_AMT;
    private String INR_AMOUNT;
    private String PURPOSE_CODE;
    private String PURPOSE_DESC;
    private String REM_CUST_ID;
    private String REM_NAME;
    private String REM_ADD1;
    private String REM_ADD2;
    private String REM_ADD3;
    private String REM_CITY;
    private String REM_CNTRY_CODE;
    private String BEN_CUST_ID;
    private String BEN_NAME;
    private String BEN_ADD1;
    private String BEN_ADD2;
    private String BEN_ADD3;
    private String BEN_CITY;
    private String BEN_CNTRY_CODE;
    private String CLIENT_ACC_NO;
    private String CPTY_AC_NO;
    private String BEN_ACCT_NO;
    private String BEN_BIC;
    private String REM_ACCT_NO;
    private String REM_BIC;
    private String TRN_DATE;
    private String PRODUCT_DESC;
    private String ACCT_ID;
    private String COD_MSG_TYPE;

    public String getACCT_ID() {
        return ACCT_ID;
    }

    public void setACCT_ID(String ACCT_ID) {
        this.ACCT_ID = ACCT_ID;
    }
   public String getCOD_MSG_TYPE() {
        return COD_MSG_TYPE;
    }

    public void setCOD_MSG_TYPE(String COD_MSG_TYPE) {
        this.COD_MSG_TYPE = COD_MSG_TYPE;
    }


    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getUCIC_ID() {
        return UCIC_ID;
    }

    public void setUCIC_ID(String UCIC_ID) {
        this.UCIC_ID = UCIC_ID;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public void setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
    }

    public String getFCC_PRODUCT_CODE() {
        return FCC_PRODUCT_CODE;
    }

    public void setFCC_PRODUCT_CODE(String FCC_PRODUCT_CODE) {
        this.FCC_PRODUCT_CODE = FCC_PRODUCT_CODE;
    }

    public String getDRCR() {
        return DRCR;
    }

    public void setDRCR(String DRCR) {
        this.DRCR = DRCR;
    }

    public String getTRAN_AMOUNT() {
        return TRAN_AMOUNT;
    }

    public void setTRAN_AMOUNT(String TRAN_AMOUNT) {
        this.TRAN_AMOUNT = TRAN_AMOUNT;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getTD_MATURITY_DATE() {
        return TD_MATURITY_DATE;
    }

    public void setTD_MATURITY_DATE(String TD_MATURITY_DATE) {
        this.TD_MATURITY_DATE = TD_MATURITY_DATE;
    }

    public String getEFF_AVAILABLE_BALANCE() {
        return EFF_AVAILABLE_BALANCE;
    }

    public void setEFF_AVAILABLE_BALANCE(String EFF_AVAILABLE_BALANCE) {
        this.EFF_AVAILABLE_BALANCE = EFF_AVAILABLE_BALANCE;
    }

    public String getCHANNEL_ID() {
        return CHANNEL_ID;
    }

    public void setCHANNEL_ID(String CHANNEL_ID) {
        this.CHANNEL_ID = CHANNEL_ID;
    }

    public String getTRAN_NUMONIC_CODE() {
        return TRAN_NUMONIC_CODE;
    }

    public void setTRAN_NUMONIC_CODE(String TRAN_NUMONIC_CODE) {
        this.TRAN_NUMONIC_CODE = TRAN_NUMONIC_CODE;
    }

    public String getINST_STATUS() {
        return INST_STATUS;
    }

    public void setINST_STATUS(String INST_STATUS) {
        this.INST_STATUS = INST_STATUS;
    }

    public String getHOST_TYPE() {
        return HOST_TYPE;
    }

    public void setHOST_TYPE(String HOST_TYPE) {
        this.HOST_TYPE = HOST_TYPE;
    }

    public String getINSTRUMENT_TYPE() {
        return INSTRUMENT_TYPE;
    }

    public void setINSTRUMENT_TYPE(String INSTRUMENT_TYPE) {
        this.INSTRUMENT_TYPE = INSTRUMENT_TYPE;
    }

    public String getINSTRUMENT_NUMBER() {
        return INSTRUMENT_NUMBER;
    }

    public void setINSTRUMENT_NUMBER(String INSTRUMENT_NUMBER) {
        this.INSTRUMENT_NUMBER = INSTRUMENT_NUMBER;
    }

    public String getATM_ID() {
        return ATM_ID;
    }

    public void setATM_ID(String ATM_ID) {
        this.ATM_ID = ATM_ID;
    }

    public String getISIN_NUMBER() {
        return ISIN_NUMBER;
    }

    public void setISIN_NUMBER(String ISIN_NUMBER) {
        this.ISIN_NUMBER = ISIN_NUMBER;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getPSTD_DATE() {
        return PSTD_DATE;
    }

    public void setPSTD_DATE(String PSTD_DATE) {
        this.PSTD_DATE = PSTD_DATE;
    }

    public String getTRAN_ID() {
        return TRAN_ID;
    }

    public void setTRAN_ID(String TRAN_ID) {
        this.TRAN_ID = TRAN_ID;
    }

    public String getREFERENCE_SRL_NUM() {
        return REFERENCE_SRL_NUM;
    }

    public void setREFERENCE_SRL_NUM(String REFERENCE_SRL_NUM) {
        this.REFERENCE_SRL_NUM = REFERENCE_SRL_NUM;
    }

    public String getVALUE_DATE() {
        return VALUE_DATE;
    }

    public void setVALUE_DATE(String VALUE_DATE) {
        this.VALUE_DATE = VALUE_DATE;
    }

    public String getTRAN_CRNCY_CODE() {
        return TRAN_CRNCY_CODE;
    }

    public void setTRAN_CRNCY_CODE(String TRAN_CRNCY_CODE) {
        this.TRAN_CRNCY_CODE = TRAN_CRNCY_CODE;
    }

    public String getREF_TRAN_AMT() {
        return REF_TRAN_AMT;
    }

    public void setREF_TRAN_AMT(String REF_TRAN_AMT) {
        this.REF_TRAN_AMT = REF_TRAN_AMT;
    }

    public String getREF_TRAN_CRNCY() {
        return REF_TRAN_CRNCY;
    }

    public void setREF_TRAN_CRNCY(String REF_TRAN_CRNCY) {
        this.REF_TRAN_CRNCY = REF_TRAN_CRNCY;
    }

    public String getTRAN_PARTICULAR() {
        return TRAN_PARTICULAR;
    }

    public void setTRAN_PARTICULAR(String TRAN_PARTICULAR) {
        this.TRAN_PARTICULAR = TRAN_PARTICULAR;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getBANK_CODE() {
        return BANK_CODE;
    }

    public void setBANK_CODE(String BANK_CODE) {
        this.BANK_CODE = BANK_CODE;
    }

    public String getPSTD_FLG() {
        return PSTD_FLG;
    }

    public void setPSTD_FLG(String PSTD_FLG) {
        this.PSTD_FLG = PSTD_FLG;
    }

    public String getONLINE_BATCH() {
        return ONLINE_BATCH;
    }

    public void setONLINE_BATCH(String ONLINE_BATCH) {
        this.ONLINE_BATCH = ONLINE_BATCH;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getTD_LIQUIDATION_TYPE() {
        return TD_LIQUIDATION_TYPE;
    }

    public void setTD_LIQUIDATION_TYPE(String TD_LIQUIDATION_TYPE) {
        this.TD_LIQUIDATION_TYPE = TD_LIQUIDATION_TYPE;
    }

    public String getTOD_GRANT_AMOUNT() {
        return TOD_GRANT_AMOUNT;
    }

    public void setTOD_GRANT_AMOUNT(String TOD_GRANT_AMOUNT) {
        this.TOD_GRANT_AMOUNT = TOD_GRANT_AMOUNT;
    }

    public String getCA_SCHEME_CODE() {
        return CA_SCHEME_CODE;
    }

    public void setCA_SCHEME_CODE(String CA_SCHEME_CODE) {
        this.CA_SCHEME_CODE = CA_SCHEME_CODE;
    }

    public String getSYSTEM() {
        return SYSTEM;
    }

    public void setSYSTEM(String SYSTEM) {
        this.SYSTEM = SYSTEM;
    }

    public String getREM_TYPE() {
        return REM_TYPE;
    }

    public void setREM_TYPE(String REM_TYPE) {
        this.REM_TYPE = REM_TYPE;
    }

    public String getBRANCH() {
        return BRANCH;
    }

    public void setBRANCH(String BRANCH) {
        this.BRANCH = BRANCH;
    }

    public String getTRAN_CURR() {
        return TRAN_CURR;
    }

    public void setTRAN_CURR(String TRAN_CURR) {
        this.TRAN_CURR = TRAN_CURR;
    }

    public String getTRAN_AMT() {
        return TRAN_AMT;
    }

    public void setTRAN_AMT(String TRAN_AMT) {
        this.TRAN_AMT = TRAN_AMT;
    }

    public String getUSD_EQV_AMT() {
        return USD_EQV_AMT;
    }

    public void setUSD_EQV_AMT(String USD_EQV_AMT) {
        this.USD_EQV_AMT = USD_EQV_AMT;
    }

    public String getINR_AMOUNT() {
        return INR_AMOUNT;
    }

    public void setINR_AMOUNT(String INR_AMOUNT) {
        this.INR_AMOUNT = INR_AMOUNT;
    }

    public String getPURPOSE_CODE() {
        return PURPOSE_CODE;
    }

    public void setPURPOSE_CODE(String PURPOSE_CODE) {
        this.PURPOSE_CODE = PURPOSE_CODE;
    }

    public String getPURPOSE_DESC() {
        return PURPOSE_DESC;
    }

    public void setPURPOSE_DESC(String PURPOSE_DESC) {
        this.PURPOSE_DESC = PURPOSE_DESC;
    }

    public String getREM_CUST_ID() {
        return REM_CUST_ID;
    }

    public void setREM_CUST_ID(String REM_CUST_ID) {
        this.REM_CUST_ID = REM_CUST_ID;
    }

    public String getREM_NAME() {
        return REM_NAME;
    }

    public void setREM_NAME(String REM_NAME) {
        this.REM_NAME = REM_NAME;
    }

    public String getREM_ADD1() {
        return REM_ADD1;
    }

    public void setREM_ADD1(String REM_ADD1) {
        this.REM_ADD1 = REM_ADD1;
    }

    public String getREM_ADD2() {
        return REM_ADD2;
    }

    public void setREM_ADD2(String REM_ADD2) {
        this.REM_ADD2 = REM_ADD2;
    }

    public String getREM_ADD3() {
        return REM_ADD3;
    }

    public void setREM_ADD3(String REM_ADD3) {
        this.REM_ADD3 = REM_ADD3;
    }

    public String getREM_CITY() {
        return REM_CITY;
    }

    public void setREM_CITY(String REM_CITY) {
        this.REM_CITY = REM_CITY;
    }

    public String getREM_CNTRY_CODE() {
        return REM_CNTRY_CODE;
    }

    public void setREM_CNTRY_CODE(String REM_CNTRY_CODE) {
        this.REM_CNTRY_CODE = REM_CNTRY_CODE;
    }

    public String getBEN_CUST_ID() {
        return BEN_CUST_ID;
    }

    public void setBEN_CUST_ID(String BEN_CUST_ID) {
        this.BEN_CUST_ID = BEN_CUST_ID;
    }

    public String getBEN_NAME() {
        return BEN_NAME;
    }

    public void setBEN_NAME(String BEN_NAME) {
        this.BEN_NAME = BEN_NAME;
    }

    public String getBEN_ADD1() {
        return BEN_ADD1;
    }

    public void setBEN_ADD1(String BEN_ADD1) {
        this.BEN_ADD1 = BEN_ADD1;
    }

    public String getBEN_ADD2() {
        return BEN_ADD2;
    }

    public void setBEN_ADD2(String BEN_ADD2) {
        this.BEN_ADD2 = BEN_ADD2;
    }

    public String getBEN_ADD3() {
        return BEN_ADD3;
    }

    public void setBEN_ADD3(String BEN_ADD3) {
        this.BEN_ADD3 = BEN_ADD3;
    }

    public String getBEN_CITY() {
        return BEN_CITY;
    }

    public void setBEN_CITY(String BEN_CITY) {
        this.BEN_CITY = BEN_CITY;
    }

    public String getBEN_CNTRY_CODE() {
        return BEN_CNTRY_CODE;
    }

    public void setBEN_CNTRY_CODE(String BEN_CNTRY_CODE) {
        this.BEN_CNTRY_CODE = BEN_CNTRY_CODE;
    }

    public String getCLIENT_ACC_NO() {
        return CLIENT_ACC_NO;
    }

    public void setCLIENT_ACC_NO(String CLIENT_ACC_NO) {
        this.CLIENT_ACC_NO = CLIENT_ACC_NO;
    }

    public String getCPTY_AC_NO() {
        return CPTY_AC_NO;
    }

    public void setCPTY_AC_NO(String CPTY_AC_NO) {
        this.CPTY_AC_NO = CPTY_AC_NO;
    }

    public String getBEN_ACCT_NO() {
        return BEN_ACCT_NO;
    }

    public void setBEN_ACCT_NO(String BEN_ACCT_NO) {
        this.BEN_ACCT_NO = BEN_ACCT_NO;
    }

    public String getBEN_BIC() {
        return BEN_BIC;
    }

    public void setBEN_BIC(String BEN_BIC) {
        this.BEN_BIC = BEN_BIC;
    }

    public String getREM_ACCT_NO() {
        return REM_ACCT_NO;
    }

    public void setREM_ACCT_NO(String REM_ACCT_NO) {
        this.REM_ACCT_NO = REM_ACCT_NO;
    }

    public String getREM_BIC() {
        return REM_BIC;
    }

    public void setREM_BIC(String REM_BIC) {
        this.REM_BIC = REM_BIC;
    }

    public String getTRN_DATE() {
        return TRN_DATE;
    }

    public void setTRN_DATE(String TRN_DATE) {
        this.TRN_DATE = TRN_DATE;
    }

    public String getPRODUCT_DESC() {
        return PRODUCT_DESC;
    }

    public void setPRODUCT_DESC(String PRODUCT_DESC) {
        this.PRODUCT_DESC = PRODUCT_DESC;
    }
}
