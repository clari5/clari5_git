package clari5.custom.inbox.processors.service;

import clari5.custom.inbox.processors.tat.CustomCL5IbxItem;

import java.util.concurrent.ConcurrentLinkedQueue;

public interface TatMailingService {
    public void sendMail(ConcurrentLinkedQueue<CustomCL5IbxItem> items);
}
