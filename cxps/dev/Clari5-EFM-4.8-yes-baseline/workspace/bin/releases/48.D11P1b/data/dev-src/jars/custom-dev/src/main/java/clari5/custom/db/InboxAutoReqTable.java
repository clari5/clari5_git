package clari5.custom.db;

import clari5.custom.yes.db.InboxAutoReq;
import clari5.custom.yes.db.mappers.InboxAutoReqMapper;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import cxps.apex.utils.CxpsLogger;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author shishir
 * @since 24/01/2018
 */

public class InboxAutoReqTable extends DBConnection implements IDBOperation {
    private static final CxpsLogger logger = CxpsLogger.getLogger(InboxAutoReqTable.class);

     public  InboxAutoReqTable() {
        rdbmsConnection = new RDBMSConnection();
    }

    @Override
    public Object getAllRecords() {
        RDBMSSession rdbmsSession = getSession();

        try {

            InboxAutoReqMapper queue = rdbmsSession.getMapper(InboxAutoReqMapper.class);
            List<InboxAutoReq> inboxAutoReqs = queue.getAllRecords();

            logger.info("INBOX_AUTO_REQ table record " + inboxAutoReqs.toString());
            return inboxAutoReqs;

        } catch (Exception e) {
            logger.error("Failed to fetch data from INBOX_AUTO_REQ table" + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public int insert(Object o) {
        RDBMSSession rdbmsSession = getSession();
        int count = 0;
        InboxAutoReq inboxAutoReq = (InboxAutoReq) o;
        try {

            count = inboxAutoReq.insert(rdbmsSession);
            rdbmsSession.commit();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to insert into INBOX_AUTO_REQ table " + rdbms.getMessage() + "Cause " + rdbms.getCause());
        } finally {
            closeRDMSSession();
        }

        return count;
    }

    @Override
    public int update(Object o) {

        int count = 0;

        try {

            InboxAutoReq inboxAutoReq = (InboxAutoReq) o;
            RDBMSSession rdbmsSession = getSession();

            count = inboxAutoReq.update(rdbmsSession);

            rdbmsSession.commit();

            logger.info(count + " Row of  INBOX_AUTO_REQ table has updated successfully ");
            return count;
        } catch (RdbmsException.RecordAlreadyExists ex) {
            logger.error(" Fields are already present in INBOX_AUTO_REQ" + ex.getMessage());

        } catch (RdbmsException e) {
            logger.error("Failed to update INBOX_AUTO_REQ table " + e.getMessage());

        } catch (Exception e) {
            logger.info("Exception in updateEvent: " + e.getMessage() + " Cause: " + e.getCause());

        } finally {
            closeRDMSSession();
        }
        return count;
    }


    @Override
    public int delete(Object o) {
        RDBMSSession session = getSession();
        int count = 0;

        try {

            InboxAutoReq inboxAutoReq = new InboxAutoReq();
            String scnName = (String) o;

            inboxAutoReq.setMapper(session);
            inboxAutoReq.setScnName(scnName);

            count = inboxAutoReq.delete();

            session.commit();

            logger.info(count + " Row of  INBOX_AUTO_REQ table has deleted successfully ");
            return count;
        } catch (RdbmsException e)

        {
            logger.error("Failed to delete record  INBOX_AUTO_REQ table " + e.getMessage());
        } catch (Exception e) {
            logger.info("Exception in delete event: " + e.getMessage() + " Cause: " + e.getCause());
        } finally {
            closeRDMSSession();
        }
        return count;

    }

    /**
     * @return fetch record from INBOX_AUTO_REQ based on scenarios name
     */
    @Override
    public InboxAutoReq select(Object o) {

        RDBMSSession rdbmsSession = getSession();
        String scnName = (String) o;

        try {
            InboxAutoReq inboxAutoReq = new InboxAutoReq();

            inboxAutoReq.setMapper(rdbmsSession);
            inboxAutoReq.setScnName(scnName);

            return inboxAutoReq.select();

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from INBOX_AUTO_REQ table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }


    protected Set<String> getScnList() {
        List<InboxAutoReq> tscnList = (List<InboxAutoReq>) getAllRecords();

        Set<String> scnList = new LinkedHashSet<>();

        for (InboxAutoReq inboxAutoReq : tscnList) {
            scnList.add(inboxAutoReq.getKey());
        }
        return scnList;
    }
}
