package clari5.custom.core.applayer;


import clari5.custom.core.utils.InboxRequest;
import clari5.custom.core.utils.ScenariosContent;

import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;


/**
 * @author shishir
 * @since 23/01/2018
 */


public class TemplateRequests extends HttpServlet {
    private static CxpsLogger logger = CxpsLogger.getLogger(TemplateRequests.class);
    HttpServletResponse response = null;
    String defaultResp = "{\n" +
            "\t\"status\": \"failed\",\n" +
            "\t\"exception\": \"payload method value not exist\"\n" +
            "}";


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doEither(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doEither(request, response);
    }

    public void doEither(HttpServletRequest request, HttpServletResponse response) {

        this.response = response;
        String payload = request.getParameter("payload");
        String method = request.getParameter("method");

        process(method, payload);
    }


    public void process(String method, String payload) {


        logger.info("Payload DATA [" + payload + "] and Method is [" + method + "]");
        try {

            //JSONObject jsonObject = new JSONObject(payload);
            // String cmd = jsonObject.getString("method");
            //String data = jsonObject.getString("payload");


            switch (method) {

                case "init":
                    sendResponse(InboxRequest.init(), true);
                    break;

                case "scenarioList":
                    sendResponse(ScenariosContent.getScnEventList(), true);
                    break;
                case "saveTemplate":
                    sendResponse(InboxRequest.insert(payload), true);
                    break;

                case "deleteTempate":
                    sendResponse(InboxRequest.delete(payload), true);
                    break;

                case "editTemplate":
                    sendResponse(InboxRequest.edit(payload), true);
                    break;

                default:
                    sendResponse(new JSONObject(defaultResp), false);
                    break;
            }

        } catch (JSONException e) {
            logger.error("Failed to transaform payload " + payload);
        }
    }

    private void sendResponse(JSONObject response, boolean flag) {

        this.response.setContentType("application/json");
        if (flag) this.response.setStatus(this.response.SC_OK);
        else if (!flag) this.response.setStatus(this.response.SC_BAD_REQUEST);

        PrintWriter out = null;
        try {

            out = this.response.getWriter();
            out.write(response.toString());
            out.flush();

        } catch (Exception io) {
            io.printStackTrace();
            logger.error("Failed to set response " + io.getLocalizedMessage() + "Cause" + io.getCause());
            this.response.setStatus(this.response.SC_ACCEPTED);
        }finally {
            out.close();
        }

    }
}
