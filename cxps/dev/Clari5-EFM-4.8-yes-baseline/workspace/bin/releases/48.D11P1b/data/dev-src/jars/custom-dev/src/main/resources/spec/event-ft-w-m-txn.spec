cxps.events.event.ft-w-m-txn{
  table-name : EVENT_FT_WMTXN
  event-mnemonic: FW
  workspaces : {
    ACCOUNT : acct-id,
    CUSTOMER: cust-id
  }
  event-attributes : {
	wms-ref-no: {db : true ,raw_name : WMS_Ref_No ,type : "string:200", custom-getter: WMS_Ref_No }
	txn-ref-no: {db : true ,raw_name : TXN_Ref_No ,type : "string:200", custom-getter:TXN_Ref_No }
	cust-id: {db : true ,raw_name : Cust_Id ,type : "string:200", custom-getter:Cust_Id }
	acct-id: {db : true ,raw_name : AcctId ,type : "string:200"}
	trade-date: {db : true ,raw_name : Trade_Date ,type : "string:200", custom-getter:Trade_Date }
	amount: {db : true ,raw_name : Amount ,type : "number:11,2"}
	scheme-name: {db : true ,raw_name : Scheme_Name ,type : "string:200", custom-getter:Scheme_Name}
	scheme-type: {db : true ,raw_name : Scheme_Type ,type : "string:200", custom-getter:Scheme_Type }
	tran-type: {db : true ,raw_name : Tran_Type ,type : "string:200", custom-getter:Tran_Type }
	sub-tran-type: {db : true ,raw_name : Sub_Tran_Type ,type : "string:200", custom-getter:Sub_Tran_Type }
	rm-code: {db : true ,raw_name : RM_Code ,type : "string:200", custom-getter:RM_Code }
	tran-code: {db : true ,raw_name : Tran_Code ,type : "string:200", custom-getter:Tran_Code }
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time }
	host-id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id }
	eventts: {db : true ,raw_name : eventts ,type : timestamp}
	}
}
