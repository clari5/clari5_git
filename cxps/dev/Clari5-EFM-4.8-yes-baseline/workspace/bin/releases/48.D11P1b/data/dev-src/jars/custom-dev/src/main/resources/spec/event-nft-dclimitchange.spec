cxps.events.event.nft-dclimitchange{
  table-name : EVENT_NFT_DCLIMITCHANGE
  event-mnemonic: DC
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
         host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        current-combined-limit: {db : true ,raw_name : current_combined_limit ,type: "number:20,3" }
        proposed-combined-limit: {db : true ,raw_name : proposed_combined_limit ,type: "number:20,3" }
        current-individual-limit1: {db : true ,raw_name : current_individual_limit1 ,type: "number:20,3" }
        proposed-combined-limit1: {db : true ,raw_name : proposed_combined_limit1 ,type: "number:20,3" }
        current-individual-limit2: {db : true ,raw_name : current_individual_limit2 ,type: "number:20,3" }
        proposed-combined-limit2: {db : true ,raw_name : proposed_combined_limit2 ,type: "number:20,3" }
        current-individual-limit3: {db : true ,raw_name : current_individual_limit3 ,type: "number:20,3" }
        proposed-combined-limit3: {db : true ,raw_name : proposed_combined_limit3 ,type: "number:20,3" }
        current-individual-limit4: {db : true ,raw_name : current_individual_limit4 ,type: "number:20,3" }
        proposed-combined-limit4: {db : true ,raw_name : proposed_combined_limit4 ,type: "number:20,3" }
        current-individual-limit5: {db : true ,raw_name : current_individual_limit5 ,type: "number:20,3" }
        proposed-combined-limit5: {db : true ,raw_name : proposed_combined_limit5 ,type: "number:20,3" }
        current-individual-limit6: {db : true ,raw_name : current_individual_limit6 ,type: "number:20,3" }
        proposed-combined-limit6: {db : true ,raw_name : proposed_combined_limit6 ,type: "number:20,3" }
        current-individual-limit7: {db : true ,raw_name : current_individual_limit7 ,type: "number:20,3" }
        proposed-combined-limit7: {db : true ,raw_name : proposed_combined_limit7 ,type: "number:20,3" }
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}        
}
}

