package clari5.custom.yes.integration.data;

public class NFT_AccountStat extends ITableData {

	private String tableName = "NFT_ACCTSTAT";
	private String event_type = "NFT_AccountStat";
    private String event_id;
	private String host_id;
	private String account_id;
	private String avl_bal;
	private String init_acct_status;
	private String user_id;
	private String branch_id;
	private String final_acct_status;
	private String acct_open_dt;
	private String branch_id_desc;
	private String cust_id;
	private String eventts;
	private String sys_time;

	@Override
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEvent_type() {
		return event_type;
	}

	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public String getAvl_bal() {
		return avl_bal;
	}

	public void setAvl_bal(String avl_bal) {
		this.avl_bal = avl_bal;
	}

	public String getInit_acct_status() {
		return init_acct_status;
	}

	public void setInit_acct_status(String init_acct_status) {
		this.init_acct_status = init_acct_status;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public String getFinal_acct_status() {
		return final_acct_status;
	}

	public void setFinal_acct_status(String final_acct_status) {
		this.final_acct_status = final_acct_status;
	}

	public String getAcct_open_dt() {
		return acct_open_dt;
	}

	public void setAcct_open_dt(String acct_open_dt) {
		this.acct_open_dt = acct_open_dt;
	}

	public String getBranch_id_desc() {
		return branch_id_desc;
	}

	public void setBranch_id_desc(String branch_id_desc) {
		this.branch_id_desc = branch_id_desc;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getEventts() {
		return eventts;
	}

	public void setEventts(String eventts) {
		this.eventts = eventts;
	}

	public String getSys_time() {
		return sys_time;
	}

	public void setSys_time(String sys_time) {
		this.sys_time = sys_time;
	}
}

