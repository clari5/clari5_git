package clari5.custom.jasper;

import clari5.custom.yes.db.NewAcctDetails;
import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import cxps.apex.utils.CxpsLogger;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author shishir
 * @since 22/05/2018
 */
public class WlRequestPayload {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlRequestPayload.class);
    NewAcctDetails newAcctDetails;

    Map<String, Double> maxScore = new HashMap<>();
    static Matches matches = new Matches();


    //final static List<String> rulesList = Arrays.asList(new String[]{"NAME_MATCH_FCU", "DOB_MATCH_FCU","RESIPHONE_MATCH_FCU","MOBILE_MATCH_FCU","ADDRESS_MATCH_FCU","PAN_MATCH_FCU","OFFICE_MATCH_FCU","DOI_MATCH_FCU"});
    final static List<String> rulesList = Arrays.asList(new String[]{"NAME_MATCH_FCU", "DOB_MATCH_FCU", "MOBILE_MATCH_FCU", "ADDRESS_MATCH_FCU", "PAN_MATCH_FCU", "DOI_MATCH_FCU"});

    public Queue<CxJson> setWlReqValues(NewAcctDetails newAcctDetails) {
        this.newAcctDetails = newAcctDetails;

        Queue<CxJson> reqList = new ConcurrentLinkedQueue<>();


        for (String ruleName : rulesList) {
            reqList.add(createWlRes(ruleName));
        }

        logger.info("WL Request Record " + reqList.toString());
        return reqList;
    }


    private List<String> getRulesFieldsList(String ruleName) {
        List<String> fields;
        String[] field = new String[0];
        switch (ruleName) {

            case "NAME_MATCH_FCU":
                field = new String[]{"name"};
                break;
            case "DOB_MATCH_FCU":
                field = new String[]{"dob"};
                break;
             /*case "RESIPHONE_MATCH_FCU":
                field = new String[]{"resi_phone"};
                break;*/
            case "MOBILE_MATCH_FCU":
                field = new String[]{"mobile_no"};
                break;
            case "ADDRESS_MATCH_FCU":
                field = new String[]{"customer_address"};
                break;
            case "PAN_MATCH_FCU":
                field = new String[]{"pan"};
                break;
             /*case "OFFICE_MATCH_FCU":
                field = new String[]{"office_number"};
                break;*/
            case "DOI_MATCH_FCU":
                field = new String[]{"doi"};
                break;
        }

        return Arrays.asList(field);
    }

    private CxJson createWlRes(String ruleName) {
        List<String> field = getRulesFieldsList(ruleName);

        StringBuilder buildReq = new StringBuilder("{\"ruleName\":\"" + ruleName + "\",");
        buildReq.append("\"fields\":[");
        for (int i = 0; i < field.size(); i++) {
            String fd = field.get(i);
            buildReq.append("{");
            switch (fd) {
                case "name":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getCustName() + "\"");
                    break;
                case "dob":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getDob() + "\"");
                    break;
                case "resi_phone":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getResiNo() + "\"");
                    break;
                case "mobile_no":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getMobileNo() + "\"");
                    break;
                case "customer_address":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getAddress() + "\"");
                    break;
                case "pan":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getPan() + "\"");
                    break;
                case "office_number":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getOfficeNo() + "\"");
                    break;
                case "doi":
                    buildReq.append("\"name\":\"" + fd + "\",\"value\" :\"" + newAcctDetails.getDateOfIncorporation() + "\"");
                    break;
            }
            if (i == field.size() - 1) buildReq.append("}");
            else buildReq.append("},");

        }
        buildReq.append("]}");
        CxJson cxJson = null;
        try {
            cxJson = CxJson.parse(buildReq.toString());
        } catch (IOException io) {
            logger.error(" [WlRequestPayload.setWlReqValues ] Failed to parse json " + buildReq.toString() + "\n Message " + io.getMessage());
        }
        return cxJson;
    }


    public Matches sendBatchWlReq(Queue queue) {


        CalMatchScore calMatchScore = new CalMatchScore();
        calMatchScore.setCustType(newAcctDetails.getCustType());
        List<String> responseList = new ArrayList<>();
        while (!queue.isEmpty()) {
            sendWlRequest((CxJson) queue.poll(), responseList);
        }

        matches.setMatchResult(responseList);
        matches.setMatchType(calMatchScore.getMatchType(maxScore));

        if (matches.getMatchType().isEmpty()) logger.warn("No match records found");

        return matches;
    }

    public void sendWlRequest(CxJson obj, List<String> responseList) {

        logger.info("Request URL "+System.getenv("LOCAL_DN") + "/efm/wlsearch?q="+obj.toString());
        String response = "";

        try {
            String url = System.getenv("LOCAL_DN") + "/efm/wlsearch?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
            String instanceId = System.getenv("INSTANCEID");
            String appSecret = System.getenv("APPSECRET");
            HttpResponse resp;
            resp = CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").basicAuth(instanceId, appSecret).asString();
            response = (String) resp.getBody();

            logger.info("WL Respose data " + response);


            if (response != null && !response.isEmpty()) {
                maxScore.put(obj.getString("ruleName"), getMaxScore(CxJson.parse(response)));
            }

            responseList.add(response);

        } catch (UnirestException e) {
            e.printStackTrace();

        } catch (Exception e) {
            logger.error("Error While getting the matched data from WL" + e.getMessage() + "\n Cause " + e.getCause());
        }


    }


    public Double getMaxScore(CxJson cxJson) {
        return Double.parseDouble(cxJson.get("matchedResults").getString("maxScore"));
    }


}
