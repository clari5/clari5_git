package clari5.custom.yes.integration.query;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import clari5.custom.yes.integration.config.BepCon;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.*;


public class QueryBuilder {

    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    ResultSet rs1 = null;
    String eventname = null;
    java.sql.Timestamp eventts = null;
    String tableName1 = null;
    public  static CxpsLogger cxpsLogger = CxpsLogger.getLogger(QueryBuilder.class);


    public boolean select(String tableName, String columnName) {
        /*cxpsLogger.info("[QueryBuilder] Select Operation Based on Batch Event App Table");
        int wait = 0;
        try {
            wait = BepCon.getConfig().getWaittime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String query = "select * from BATCH_EVENT_APP where TABLE_NAME = '" + columnName + "' and CREATED_ON < SYSTIMESTAMP - INTERVAL '" + wait + "' MINUTE";
        cxpsLogger.info("[QueryBuilder] Executing the following query"+query);
        con = Rdbms.getAppConnection();
        try {
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                eventname = rs.getString("table_name");
                eventts = rs.getTimestamp("created_on");
            }
            if (eventname != null && eventts != null) {
                cxpsLogger.info("[QueryBuilder] Event Name and EventTs is Not Null ");
                String delete = "delete from " + tableName + " where table_name = '" + eventname + "'";
                cxpsLogger.info("[QueryBuilder] Deleting Entry From"+tableName+ " for"+eventname);
                ps = con.prepareStatement(delete);
                int i = ps.executeUpdate();
                if (i > 0) {
                    con.commit();
                }
                String insert = "Insert into " + tableName + "(TABLE_NAME,CREATED_ON) values ('" + eventname + "',CURRENT_TIMESTAMP)";
                cxpsLogger.info("[QueryBuilder] Inserting Entry Inside"+tableName+ " for"+eventname+" Query"+insert);
                ps = con.prepareStatement(insert);
                int j = ps.executeUpdate();
                if (j > 0) {
                    con.commit();
                    return true;
                }

            } else {
                String query1 = "select * from BATCH_EVENT_APP where TABLE_NAME = '" + columnName + "'";
                ps = con.prepareStatement(query1);
                rs1 = ps.executeQuery();
                while (rs1.next()) {
                    tableName1 = rs1.getString("table_name");
                }
                if (tableName1 != null) {
                    return false;
                } else {
                    // FT_CORE
                    String insert = "Insert into " + tableName + "(TABLE_NAME,CREATED_ON) values ('" + columnName + "',CURRENT_TIMESTAMP)";
                    ps = con.prepareStatement(insert);
                    int j = ps.executeUpdate();
                    if (j > 0) {
                        con.commit();
                        return true;
                    }
                }

            }

        } catch (SQLException e) {
            cxpsLogger.info("unique constraint");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;*/
        int wait = 0;
        try {
            wait = BepCon.getConfig().getWaittime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String query = "select * from BATCH_EVENT_APP where TABLE_NAME = '" + columnName + "'" ;
        //+ "and CREATED_ON < SYSTIMESTAMP - INTERVAL '" + wait + "' MINUTE";
        con = Rdbms.getAppConnection();
        cxpsLogger.info("Connection OPENED");
        try {
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                eventname = rs.getString("table_name");
                eventts = rs.getTimestamp("created_on");
            }
            if (eventname != null && eventts != null) {

                cxpsLogger.info("Table name [ " +columnName+ " ] : Already exists and  hence lock can't be acquired wait till it releases");
                return false;
            } else {
                String insert = "Insert into " + tableName + "(TABLE_NAME,CREATED_ON) values ('" + columnName + "',CURRENT_TIMESTAMP)";
                ps = con.prepareStatement(insert);
                int j = ps.executeUpdate();
                if (j > 0) {
                    con.commit();
                    cxpsLogger.info("Table name [ " +columnName+ " ]  : Inserted Successfully and acquired lock");
                    return true;
                }
            }
        } catch (SQLException e) {
            cxpsLogger.info("unique constraint");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (con != null) {
                    cxpsLogger.info("Connection CLOSED");
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean delete(String tablentry) {
        /*cxpsLogger.info("inside deleting lock once donce");
        con = Rdbms.getAppConnection();
        String delete = "delete from BATCH_EVENT_APP where table_name = '" + tablentry + "'";
        try {
            ps = con.prepareStatement(delete);
            int i = 0;
            try {
                i = ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (i > 0) {
                con.commit();
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }*/
        cxpsLogger.info("Removing  lock for table [ " +tablentry+ " ] ");
        con = Rdbms.getAppConnection();
        cxpsLogger.info("Connection OPENED");
        String delete = "delete from BATCH_EVENT_APP where table_name = '" + tablentry + "'";
        try {
            ps = con.prepareStatement(delete);
            int i = 0;
            i = ps.executeUpdate();
            if (i > 0) {
                con.commit();
                cxpsLogger.info("Sucessfully removed lock  for [ " +tablentry+ " ] " );
                return true;
            }
            else {
                cxpsLogger.info("UNABLE TO REMOVE LOCK since delete is failing  for [ " +tablentry+ " ] ");
                return false;
            }

        }
        catch (SQLException e) {
            cxpsLogger.info("UNABLE TO REMOVE LOCK for [ " +tablentry+ " ] . Check Exception ");
            e.printStackTrace();
            return  false;
        }
        finally {
            try {
                if (con != null) {
                    con.close();
                    cxpsLogger.info("Connection CLOSED");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}