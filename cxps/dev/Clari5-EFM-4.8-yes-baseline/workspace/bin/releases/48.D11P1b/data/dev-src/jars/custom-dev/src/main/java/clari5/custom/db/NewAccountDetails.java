package clari5.custom.db;


import clari5.custom.yes.db.*;
import clari5.custom.yes.db.mappers.NewAcctDetailsMapper;
import clari5.platform.applayer.Clari5;
import clari5.platform.rdbms.ObjectKeys;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import cxps.apex.utils.CxpsLogger;
import jdk.nashorn.internal.runtime.linker.Bootstrap;
import org.apache.ibatis.session.RowBounds;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shishir
 * @since 18/05/2018
 */
public class NewAccountDetails extends DBConnection implements IDBOperation {

    private static final CxpsLogger logger = CxpsLogger.getLogger(NewAccountDetails.class);

    public NewAccountDetails() {
        rdbmsConnection = new RDBMSConnection();
    }
    int offset=0;
    int limit=2;
    @Override
    public Object getAllRecords() {
        RDBMSSession rdbmsSession = getSession();


        try {
            NewAcctDetailsMapper queue = rdbmsSession.getMapper(NewAcctDetailsMapper.class);
            RowBounds rowBounds = new RowBounds(0, 10);
            List<NewAcctDetails> newAccountDetails = queue.getAllRecords(rowBounds);

            logger.info("NEW_ACCT_DETAILS table record " + newAccountDetails.toString());
            return newAccountDetails;

        } catch (Exception e) {
            logger.error("Failed to fetch data from NEW_ACCT_DETAILS table" + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;

    }

    @Override
    public Object select(Object o) {
        NewAcctDetails newAcctDetails = new NewAcctDetails(getSession());

        String acct_id = (String) o;
        try {

            newAcctDetails.setAcctId(acct_id);
            return newAcctDetails.select();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to fetch NEW_ACCT_DETAILS table data of [" + acct_id + "] \n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;

    }

    @Override
    public int insert(Object o) {
        RDBMSSession session = getSession();
        int count = 0;
        NewAcctDetails newAcctDetails = (NewAcctDetails) o;

        try {
            newAcctDetails.setLastModifiedTime(Timestamp.from(Instant.now()));

            count = newAcctDetails.insert(session);
            session.commit();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to insert into NEW_ACCT_DETAILS " + newAcctDetails.toString() + "\n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }


        return count;
    }

    @Override
    public int update(Object o) {
        RDBMSSession session = getSession();
        int count = 0;

        NewAcctDetails newAcctDetails = (NewAcctDetails) o;
        try {

            newAcctDetails.setLastModifiedTime(Timestamp.from(Instant.now()));
            count = newAcctDetails.update(session);
            session.commit();

        }

        catch (RdbmsException rdbms) {
            logger.error("Failed to update NEW_ACCT_DETAILS" + newAcctDetails.toString());
            logger.error("Message " + rdbms.getMessage() + " Cause " + rdbms.getCause());

        } finally {
            closeRDMSSession();
        }
        return count;
    }

    /**
     * @return return fresh  record from new account Details table
     */
    public List<NewAcctDetailsSummary> getFreshRecord() {
        RDBMSSession rdbmsSession = getSession();

        try {

            NewAcctDetailsMapper mapper = rdbmsSession.getMapper(NewAcctDetailsMapper.class);
            NewAcctDetailsCriteria criteria = new NewAcctDetailsCriteria();
            criteria.setStatus("F");

            return mapper.searchNewAcctDetails(criteria);

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from new account Details table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override
    public int delete(Object o) {
        return 0;
    }

    /**
     * @return return fresh  record from new account Details table
     */

    public List<NewAcctDetails> getFreshKeys(int offset,int limit) {
        try {
        RDBMSSession rdbmsSession = getSession();

            NewAcctDetailsMapper queue = rdbmsSession.getMapper(NewAcctDetailsMapper.class);
            RowBounds rowBounds = new RowBounds(offset, limit);


            List<NewAcctDetails> records = queue.selectRecords(new ObjectKeys(getFreshKeys(rdbmsSession)),rowBounds);

            return records;
        }
        catch (Exception e) {
            logger.error("failed to fetch pending  record from new account Details table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }


    private List<String> getFreshKeys(RDBMSSession session){

        List<String> keys = new ArrayList<>();

        try {

            NewAcctDetailsMapper mapper = session.getMapper(NewAcctDetailsMapper.class);

            NewAcctDetailsCriteria criteria = new NewAcctDetailsCriteria();
            criteria.setStatus("F");
            List<NewAcctDetailsSummary> newAcctDetailsSummaries = mapper.searchNewAcctDetails(criteria);

            if (newAcctDetailsSummaries.size()<=0){ return null;}

            for (NewAcctDetailsSummary newacct :newAcctDetailsSummaries) {
                keys.add(newacct.getAcctId());
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  keys;

    }

}
