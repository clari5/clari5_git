clari5.custom.mapper {
        entity {
                NFT_DEBITCARDREQ {
                       generate = true
                        attributes:[
                                { name: EVENT_ID, type="string:50" ,key=true },
                                { name: ACCOUNT_ID ,type ="string:50" }
                                { name: CUSTOMER_ID ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
                                { name: CARD_NO ,type ="string:50" }
				{ name: EVENTTS ,type =timestamp }
                                { name: HOST_ID ,type ="string:50" }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
                                { name: SERVER_ID ,type ="number" }
                               ]
                        }
        }
}
