package clari5.custom.yes.integration.data;

import clari5.custom.yes.integration.builder.EventBuilder;
import clari5.custom.yes.integration.builder.MsgMetaData;
import clari5.custom.yes.integration.data.bootstrap.TableMap;

public abstract class ITableData {

	public boolean process() throws Exception {
		MsgMetaData m = TableMap.getTableMap().get(this.getTableName());
		EventBuilder eb = new EventBuilder();
		eb.process(this);
		return true;
	}
	public abstract String getTableName();
}
