clari5.custom.mapper.entity.ACCOUNT_TRANSACTION {
                        attributes=[
                                { name: ACCOUNT_ID , type="string:20", key=true },
                                { name: CUSTOMER_ID ,type ="string:20" }
                                { name: CUSTOMER_SEGMENT ,type ="string:20" }
				{ name: PAN ,type ="string:20" }
				{ name: PROFESSION_CODE ,type ="string:20" }
				{ name: CUSTOMER_TYPE ,type ="string:200" }
				{ name: BRANCH_CODE ,type ="string:20" }
				{ name: ACCOUNT_OPEN_DATE ,type ="date" }
				{ name: CUSTOMER_RISK ,type ="string:20" }
				{ name: OD_LIMIT ,type ="string:8" }
				{ name: AGE ,type ="number:20" }
				{ name: LIEN_FREE_FD ,type ="string:8" }
				{ name: CREATED_ON ,type ="date" }
				{ name: UPDATED_ON ,type ="date" }
				{ name: FLAG_TYPE_CLASS ,type ="string:1" }
				{ name: THRESHOLD_AMT ,type ="number:20,2" }	
				{ name: TRAN_TYPE ,type ="string:20" }
				{ name: EDD_LOC ,type ="string:20" }
				{ name: UPDATED_THRESHOLD ,type ="number:20,2" }
				{ name: MULTIPLE_OF ,type ="number"  } 
				{ name: SCORE ,type ="number:10,2" }
				{ name: BRANCH_ID ,type ="string:20" }
				{ name: LEVEL_ID ,type ="string:20" }
				{ name: SEGMENT_ID ,type ="string:20" }		
				{ name: GROUP_ID ,type ="string:20" }
				{ name: CUM_TXN_AMT ,type ="number:38,2" }
				{ name: TS_COUNT ,type ="number:10" }
				{ name: RCRE_TIME ,type =timestamp }
				{ name: RCRE_USER ,type ="string:50" }
				{ name: LCHG_TIME ,type =timestamp }
				{ name: LCHG_USER ,type ="string:50" }
				
                                ]
				indexes {
              					
						  TSCOUNT : [ TS_COUNT ]
       					 }
                        }
        


