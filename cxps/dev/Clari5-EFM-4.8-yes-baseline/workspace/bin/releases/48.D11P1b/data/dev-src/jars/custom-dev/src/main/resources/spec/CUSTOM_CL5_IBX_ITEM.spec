clari5.custom.mapper {
        entity {
                CUSTOM_CL5_IBX_ITEM {
                       generate = true
                        attributes:[
                                { name: EMAIL_ID, type="string:50"},
                                { name: EMAIL_BODY ,type ="string:300" }
                                { name: ITEM_ID ,type ="string:300" }
				{ name: RETRY_COUNT ,type ="string:20" }
				{ name: EMAIL_FLAG ,type ="string:20" }
				{ name: EMAIL_SUBJECT ,type ="string:200" }
				{ name: RETRY_ID ,type ="string:50", key=true }
                                ]
                        }
        }
}

