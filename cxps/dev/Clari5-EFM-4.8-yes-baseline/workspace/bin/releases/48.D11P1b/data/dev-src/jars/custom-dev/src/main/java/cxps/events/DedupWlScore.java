package cxps.events;

import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.util.*;

public class DedupWlScore {
    private static final CxpsLogger logger = CxpsLogger.getLogger(DedupWlScore.class);

    NFT_DedupEvent nft_dedupEvent;
    static Hocon hocon;
    static int count = 0;


    static {
        hocon = new Hocon();
        hocon.loadFromContext("custom-wl-rule.conf");
    }

    public Object getWlScore(Object object) {
        if (object instanceof NFT_DedupEvent) {
            nft_dedupEvent = (NFT_DedupEvent) object;
        } else return null;
        setScores();
        return nft_dedupEvent;
    }


    public void setScores() {
        Connection con = null;
        try {
            con = Rdbms.getAppConnection();
            logger.info("Obtained Connection [" + con + "]");
            try {

                Hocon factHocon = hocon.get("clari5.custom.wl-rule");
                List<String> rulesList = factHocon.getStringList("rules");
                WlRules wlRules = new WlRules(hocon);
                JSONObject matchedArray = new JSONObject();

                Map<String, List<String>> fieldList = new HashMap<>();
                fieldList.put("name", new ArrayList<>());
                fieldList.put("mailing_address", new ArrayList<>());
                fieldList.put("permanent_address", new ArrayList<>());
                fieldList.put("complete_branchaddr", new ArrayList<>());
                for (String rule : rulesList) {

                    //TODO See if setDedupData is can be replaced by pasisng "this"
                    JSONObject data = wlRules.createRequestJson(rule, nft_dedupEvent.setDedupData());
                    String response = wlRules.sendWlRequest(data);
                    if (!StringUtils.isNullOrEmpty(response)) {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getJSONObject("status").get("status").toString();
                        if ("success".equals(status.trim())) {
                            if (jsonObject.has("matchedResults")) {

                                String maxScore = jsonObject.getJSONObject("matchedResults").getString("maxScore");
                                int totalResult = jsonObject.getJSONObject("matchedResults").getInt("total");
                                totalResult = nft_dedupEvent.getEntityType().equalsIgnoreCase("N") ? totalResult : totalResult - 1;
                                nft_dedupEvent.score = Double.parseDouble(maxScore) * 100;
                                if (rule.equalsIgnoreCase("NAME_MATCH")) {
                                    System.out.println("Inside the name match");
                                    nft_dedupEvent.setNameMatchScore(nft_dedupEvent.score);
                                    logger.info("NameMatchScore [" + nft_dedupEvent.score + "] and Event_Id is [" + nft_dedupEvent.eventId + "]");
                                } else if (rule.equalsIgnoreCase("MAILING_ADDRESS_PARTIAL_MATCH")) {
                                    nft_dedupEvent.setCompleteMailingAddrCount(totalResult);
                                    nft_dedupEvent.setMailingAddrScore(nft_dedupEvent.score);

                                    logger.info("MailingAddrScore [" + nft_dedupEvent.score + "]and Event_Id is [" + nft_dedupEvent.eventId + "]");
                                } else if (rule.equalsIgnoreCase("PERMANENT_ADDRESS_PARTIAL_MATCH")) {
                                    nft_dedupEvent.setPermanantAddrScore(nft_dedupEvent.score);
                                    nft_dedupEvent.setCompletePerAddrCount(totalResult);

                                    logger.info("PermanantAddrScore [" + nft_dedupEvent.score + "] and Event_Id is [" + nft_dedupEvent.eventId + "]");
                                }
                                // done the changes for branch permanent address  and branch mailing address
                                else if (rule.equalsIgnoreCase("BRANCH_PERMANENT_ADDRESS_PARTIAL_MATCH")) { // added for branch_permanent_address
                                    nft_dedupEvent.setBrpermanantAddrScore(nft_dedupEvent.score);
                                    // nft_dedupEvent.setBrcompletePerAddrCount(totalResult);

                                    logger.info("BranchPermanantAddrScore [" + nft_dedupEvent.score + "] and Event_Id is [" + nft_dedupEvent.eventId + "]");
                                } else if (rule.equalsIgnoreCase("BRANCH_MAILING_ADDRESS_PARTIAL_MATCH")) { // added for branch_mailing_address
                                    // nft_dedupEvent.setBrcompleteMailingAddrCount(nft_dedupEvent.score);
                                    nft_dedupEvent.setBrmailingAddrScore(nft_dedupEvent.score);
                                    //nft_dedupEvent.setBrmailingAddrScore(30.00);

                                    logger.info("BranchMailingAddrScore [" + nft_dedupEvent.score + "]and Event_Id is [" + nft_dedupEvent.eventId + "]");
                                }
                                // ArrayList<String> nameList = new ArrayList<String>();


                                // int count = 0;

                                JSONArray jsonArray = jsonObject.getJSONObject("matchedResults").getJSONArray("matchedRecords");


                                for (int i = 0; i < 5; i++) {
                                    if (jsonArray.length() > i) {
                                        JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                        String id =jsonObject1.getString("id");
                                        JSONObject fieldName = (JSONObject) jsonObject1.getJSONArray("fields").get(0);

                                        String name = fieldName.getString("name");
                                        if (name.equalsIgnoreCase("name")) {
                                            String value = fieldName.getString("value");
                                            fieldList.get("name").add(value+" - " +id);
                                        } else if (name.equalsIgnoreCase("mailing_address")) {

                                            String value = fieldName.getString("value");
                                            fieldList.get("mailing_address").add(value+" - " +id);


                                        } else if (name.equalsIgnoreCase("permanent_address")) {

                                            String value = fieldName.getString("value");
                                            fieldList.get("permanent_address").add(value+" - " +id);


                                        } else if (name.equalsIgnoreCase("complete_branchaddr")) {
                                            String value = fieldName.getString("value");
                                            fieldList.get("complete_branchaddr").add(value+" - " +id);


                                        }


                                    }
                                }

                                List<String> fields = fieldList.get("name");
                                String field = (fields.isEmpty()) ? "NA" : fields.toString();
                                nft_dedupEvent.setNameMatchedlist(field);


                                fields = fieldList.get("mailing_address");
                                field = (fields.isEmpty()) ? "NA" : fields.toString();
                                nft_dedupEvent.setMailingaddrMatchedlist(field);

                                fields = fieldList.get("permanent_address");
                                field = (fields.isEmpty()) ? "NA" : fields.toString();
                                nft_dedupEvent.setPermanentaddrMatchedlist(field);

                                fields = fieldList.get("complete_branchaddr");
                                field = (fields.isEmpty()) ? "NA" : fields.toString();

                                nft_dedupEvent.setBranchaddrMatchedlist(field);
                            }

                        } else {
                            logger.debug("rule name [" + rule + "] not mapped in custom-wl-rule.conf ");
                        }
                    }
                }


            } catch (Exception e) {
                logger.info("Exception in Watchlist " + e.getMessage());
            }

            String sdob = "";
            String sdoi = "";
            Date dob = nft_dedupEvent.getDob();
            Date doi = nft_dedupEvent.getDoi();
            if (dob != null) {
                sdob = dob.toString();
            }
            if (doi != null) {
                sdoi = doi.toString();
            }
            String[] data = {nft_dedupEvent.getMobNumber(), nft_dedupEvent.getHomeLandlineNumber(), nft_dedupEvent.getOfficeLandlineNumber(),
                    nft_dedupEvent.getEmailId(), nft_dedupEvent.getPan(), nft_dedupEvent.getPassportNumber(), sdob, sdoi, nft_dedupEvent.getHandPhone()};
            int dataIndex = 0;
            DbQueries dbQueries = new DbQueries();
            String ucicId = nft_dedupEvent.getUcicId();
            List<String>custphone=new ArrayList<>();
            List<String> getcustmatchphone1=new ArrayList<>();
            List<String> getCusthandphone=new ArrayList<>();

            List<String> custPhonedetails = new ArrayList<>();
            List<String> custPhonedata1 = new ArrayList<>();

            for (String value : data) {

                if (!StringUtils.isNullOrEmpty(value)) {
                    try {
                        switch (dataIndex) {

                            case 0:
                                int[] mobiledata = dbQueries.getMobiledata(con, value, ucicId);
                                nft_dedupEvent.setMobMatchedCount(mobiledata[0]);
                                nft_dedupEvent.setMobNonStaffMatchedCount(mobiledata[1]);
                                nft_dedupEvent.setMobCount(mobiledata[2]);
                                logger.info("Mob Matched Count [" + mobiledata[0] + "]Mob Non Staff Matched Count [" + mobiledata[1] + "] Mob Count [" + mobiledata[2] + "] and Event Id is [" + nft_dedupEvent.eventId
                                        + "]");
                                List<String> custmobStaff = dbQueries.getMobileStaffcust(con,value,ucicId);
                                List<String> custmobNonStaff = dbQueries.getMobileNonStaffdata(con,value,ucicId);
                                List<String> MobCount = dbQueries.getCustMobData(con,value,ucicId);


                                String custmobStaffdata = (custmobStaff.isEmpty()) ? "NA" : custmobStaff.toString();
                                nft_dedupEvent.setMobStaffCustId(custmobStaffdata);

                                String custmobNonStaffdata = (custmobNonStaff.isEmpty()) ? "NA" : custmobNonStaff.toString();
                                nft_dedupEvent.setMobNonstaffCustId(custmobNonStaffdata);
                                String MobCountfdata = (MobCount.isEmpty()) ? "NA" : MobCount.toString();
                                nft_dedupEvent.setMobCustId(MobCountfdata);
                                break;
                            case 1:
                                int[] homelandline = dbQueries.gethomeLandline(con, value, ucicId);
                                nft_dedupEvent.setLandlineMatchedCount(homelandline[0]);
                                nft_dedupEvent.setIntermediateLandlineCount(homelandline[1]);
                                logger.info("LandlineMatchedCount [" + homelandline[0] + "]IntermediateLandlineCount [" + homelandline[1] + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String>custLandline = dbQueries.getcustLandline(con,value,ucicId);
                                custphone = dbQueries.getcustmatchPhone(con,value,ucicId);

                                String custLandlinedata = (custLandline.isEmpty()) ? "NA" : custLandline.toString();
                                nft_dedupEvent.setLandlineCustId(custLandlinedata);
                                break;
                            case 2:
                                int[] officelandline = dbQueries.getofficeLandline(con, value, ucicId);
                                nft_dedupEvent.setOfficephoneMatchedCount(officelandline[0]);
                                nft_dedupEvent.setIntermediateofficelandCount(officelandline[1]);
                                logger.info("OfficephoneMatchedCount [" + officelandline[0] + "]IntermediateofficelandCount [" + officelandline[1] + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String> getcustLandline1 = dbQueries.getcustLandline1(con,value,ucicId);
                                getcustmatchphone1 = dbQueries.getcustmatchphone1(con,value,ucicId);

                                String getcustLandline1data = (getcustLandline1.isEmpty()) ? "NA" : getcustLandline1.toString();
                                nft_dedupEvent.setOfficePhoneCustId(getcustLandline1data);

                                break;
                            case 3:
                                int email = dbQueries.getemail(con, value, ucicId);
                                nft_dedupEvent.setEmailMatchedCount(email);
                                logger.info("EmailMatchedCount [" + email + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String> getcustEmail = dbQueries.getcustemail(con,value,ucicId);
                                String getcustEmaildata = (getcustEmail.isEmpty()) ? "NA" : getcustEmail.toString();
                                nft_dedupEvent.setEmailCustId(getcustEmaildata);
                                break;
                            case 4:
                                int pan = dbQueries.getpan(con, value, ucicId);
                                nft_dedupEvent.setPanMatchedCount(pan);
                                logger.info("PanMatchedCount [" + pan + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String> getCustpan = dbQueries.getCustpan(con,value,ucicId);
                                String getCustpandata = (getCustpan.isEmpty()) ? "NA" : getCustpan.toString();
                                nft_dedupEvent.setPanCustId(getCustpandata);
                                break;
                            case 5:
                                int passport = dbQueries.getpassport(con, value, ucicId);
                                nft_dedupEvent.setPassportMatchedCount(passport);
                                logger.info("PassportMatchedCount [" + passport + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String> getCustpassport = dbQueries.getCustpassport(con,value,ucicId);
                                String getCustpassportdata = (getCustpassport.isEmpty()) ? "NA" : getCustpassport.toString();
                                nft_dedupEvent.setPassportCustId(getCustpassportdata);
                                break;
                            case 6:
                                int dateOfBirth = dbQueries.getdob(con, dob, ucicId);
                                nft_dedupEvent.setDobMatchedCount(dateOfBirth);
                                logger.info("DobMatchedCount [" + dateOfBirth + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String> getCustdob= dbQueries.getCustdob(con,dob,ucicId);
                                String getCustdobdata = (getCustdob.isEmpty()) ? "NA" : getCustdob.toString();
                                nft_dedupEvent.setDobCustId(getCustdobdata);
                                break;
                            case 7:
                                int dateOfIncorporation = dbQueries.getdoi(con, doi, ucicId);
                                nft_dedupEvent.setDoiMatchedCount(dateOfIncorporation);
                                logger.info("DoiMatchedCount [" + dateOfIncorporation + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                List<String> getCustdoi= dbQueries.getCustdoi(con,doi,ucicId);
                                String getCustdoidata = (getCustdoi.isEmpty()) ? "NA" : getCustdoi.toString();
                                nft_dedupEvent.setDoiCustId(getCustdoidata);
                                break;
                            case 8:
                                int handphone = dbQueries.gethandphone(con, value, ucicId);
                                nft_dedupEvent.setIntermediatehandphoneCount(handphone);
                                logger.info("IntermediatehandphoneCount [" + handphone + "]  and Event Id is [" + nft_dedupEvent.eventId + "]");
                                 getCusthandphone = dbQueries.getCusthandphone(con,value,ucicId);
                                break;
                        }
                    } catch (Exception e) {
                        System.out.println("index [" + dataIndex + "] \n Message " + e.getMessage());
                    }

                }
                dataIndex++;
            }
            //The logic for phonecount need to be revisited
            int phonecount = nft_dedupEvent.getIntermediatehandphoneCount() + nft_dedupEvent.getIntermediateLandlineCount() + nft_dedupEvent.getIntermediateofficelandCount();
            nft_dedupEvent.setPhoneCount(phonecount);

            custPhonedetails.addAll(custphone);
            custPhonedetails.addAll(getcustmatchphone1);
            custPhonedetails.addAll(getCusthandphone);
            for (int i = 0; i < 6; i++) {
                custPhonedata1.add(custPhonedetails.get(i));
            }
            String custPhonedata = (custPhonedata1.isEmpty()) ? "NA" : custPhonedata1.toString();
            nft_dedupEvent.setMatchPhoneCustId(custPhonedata);
            custphone =null;
            getcustmatchphone1=null;
            getCusthandphone = null;
            custPhonedetails=null;
            custPhonedata1=null;
            if (this != null) logger.info("NFT_DedupEvent fields " + this.toString());
        } catch (Exception e) {
            logger.info("Exception in DbQueries");
        } finally {
            try {
                if (con != null) con.close();
            } catch (Exception e) {
            }
        }

    }



}
