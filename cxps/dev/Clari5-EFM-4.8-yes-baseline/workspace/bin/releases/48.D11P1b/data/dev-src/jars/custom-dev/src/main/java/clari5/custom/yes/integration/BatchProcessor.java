package clari5.custom.yes.integration;

import clari5.custom.yes.integration.builder.EventBuilder;
import clari5.custom.yes.integration.config.BepCon;
import clari5.custom.yes.integration.data.ITableData;
import clari5.custom.yes.integration.db.DBTask;
import clari5.custom.yes.integration.query.QueryBuilder;
import clari5.platform.applayer.CxpsDaemon;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import cxps.apex.utils.CxpsLogger;
import org.apache.commons.lang.ArrayUtils;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BatchProcessor extends CxpsDaemon implements ICxResource {

    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(BatchProcessor.class);
    static ConcurrentLinkedQueue<String> tables = new ConcurrentLinkedQueue<String>();
    private static ConcurrentLinkedQueue<ITableData> dataque = new ConcurrentLinkedQueue<ITableData>();
    Map<ITableData, String> statusmap = new HashMap<ITableData, String>();

    private final static Object lock = new Object();
    boolean staus;
    Long startTime;
    Long endTime;

    @Override
    public Object getData() throws RuntimeFatalException {
        startTime=System.currentTimeMillis();
        cxpsLogger.info("[BatchProcessor] Get Data Called");
        synchronized (lock) {
            try {
                if (tables.isEmpty()) {
                    Long time1=System.currentTimeMillis();
                    String tableName[] = BepCon.getConfig().getOrderOfTableProc();
                    Long time2=System.currentTimeMillis();
                    cxpsLogger.info("[BepCon] Fetched Table Processing Order in"+(time2-time1));
                    for (String table : tableName) {
                        tables.add(table);
                    }
                }
            } catch (Exception e) {
                cxpsLogger.info("Not able to fetch the tablename");
            }
            cxpsLogger.info("TAble Added  " + Thread.currentThread().getName());
        }
        synchronized (lock) {
            ITableData row = null;
            if (dataque.isEmpty()) {
                String tableName = tables.poll();
                cxpsLogger.info("[BatchProcessor] Table In Operation"+tableName);
                if (tableName != null && !"".equals(tableName)) {
                    QueryBuilder qb = new QueryBuilder();
                    try {
                        Long t1 = System.currentTimeMillis();
                        staus = qb.select(BepCon.getConfig().getEventApptable(), tableName);
                        Long t2 = System.currentTimeMillis();
                        cxpsLogger.info("[BatchProcessor] Status Received From QueryBuilder Select Operation " + staus + "in" + (t2 - t1) + "ms");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // compeleted all
                    if (staus) {
                        List<ITableData> data = fetchData(tableName);
                        while (data != null && data.size() > 0) {
                            dataque.addAll(data);
                            cxpsLogger.info("[BatchProcessor]Added all the data hence clearing the list [ "+data.size() + " ] ");
                            data.clear();
                        }
                        staus = qb.delete(tableName);
                        if (staus) {
                            cxpsLogger.info("sucessfully removed lock");
                        } else {
                            cxpsLogger.info("error in removing lock");
                        }

                    }
                }
            }
        }

        if (dataque.isEmpty()) {
            return null;
        } else {
            return new Object();
        }

    }

    public List<ITableData> fetchData(String tableName) {

        cxpsLogger.info("[BatchProcessor] Fetch Data from " + tableName);
        List<ITableData> list = new ArrayList<ITableData>();

        try {
            EventLoader loader = new EventLoader();
            list = loader.getFreshEvents(tableName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        cxpsLogger.info("Fetch Data completed for  " + tableName + " with size " + list.size());
        return list;

    }

    @Override
    public void processData(Object o) throws RuntimeFatalException {

        cxpsLogger.info("Process Data");
        /**
         * Newly added
         */
        ITableData row=null;
        Map<ITableData,String > statusmap=new HashMap<>();

        while ((row = dataque.poll()) != null){
            cxpsLogger.info("[BatchProcessor] Dataque size" + dataque.size() + " by thread" + Thread.currentThread().getName());
            String status = "";
            try {
                //row.process();
                EventBuilder eb = new EventBuilder();
                Long t1=System.currentTimeMillis();
                if (eb.process(row))
                    status = "SUCCESS";
                else
                    status = "FAILED";
                Long t2=System.currentTimeMillis();
                cxpsLogger.info("[BatchProcessor] Time Taken To Process 1 record to Enqueue "+(t2-t1)+ "ms");
            } catch (Exception e) {
                System.out.println("ERROR while processing event.");
                e.printStackTrace();
                cxpsLogger.error("Exception caught while processing data for the row, ", row);
                status = getStackTrc(e);
                if (status.length() > 4000) {
                    status = status.substring(0, 4000);
                }
            }
            statusmap.put(row, status);

        }
        try {
            cxpsLogger.info("[BatchProcessor] Insertion Completed and Updating Status!! ");
            DBTask.updateStatus(statusmap);
        } catch (Exception e) {
            cxpsLogger.error("Error while updating timestamp");
        }
        endTime=System.currentTimeMillis();
        cxpsLogger.info("[BatchProcessor] Time Taken To Process Entire Record "+(endTime-startTime) + "ms");
    }

    public static String getStackTrc(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    @Override
    public void configure(Hocon h) {
        cxpsLogger.info("configuring daemon" + h.toString());
    }

    @Override
    public void release() {

    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void refresh() {

    }

}
