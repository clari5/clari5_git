// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_COREDECLINE", Schema="rice")
public class FT_CoreDeclineTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=11) public Double todGrantAmount;
       @Field public java.sql.Timestamp valueDate;
       @Field(size=200) public String benCity;
       @Field(size=200) public String onlineBatch;
       @Field(size=11) public Double tranAmount;
       @Field(size=200) public String branch;
       @Field(size=200) public String benCntryCode;
       @Field(size=200) public String refTranCrncy;
       @Field(size=200) public String purposeDesc;
       @Field(size=200) public String accountType;
       @Field(size=200) public String remName;
       @Field(size=200) public String tranId;
       @Field(size=200) public String benBic;
       @Field(size=200) public String hostId;
       @Field(size=11) public Double tranAmt;
       @Field(size=200) public String productDesc;
       @Field(size=200) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String tranCrncyCode;
       @Field public java.sql.Timestamp tdMaturityDate;
       @Field(size=200) public String acctName;
       @Field(size=200) public String benAcctNo;
       @Field(size=200) public String benCustId;
       @Field public java.sql.Timestamp pstdDate;
       @Field(size=200) public String system;
       @Field(size=200) public String atmId;
       @Field(size=200) public String productCode;
       @Field(size=200) public String drCr;
       @Field(size=200) public String clientAccNo;
       @Field(size=200) public String remBic;
       @Field(size=200) public String pstdFlg;
       @Field(size=200) public String status;
       @Field(size=200) public String hostType;
       @Field(size=200) public String remAdd1;
       @Field(size=200) public String acctId;
       @Field(size=200) public String numericCode;
       @Field(size=200) public String remType;
       @Field(size=200) public String remAdd3;
       @Field(size=11) public Double inrAmount;
       @Field(size=200) public String remAdd2;
       @Field public java.sql.Timestamp trnDate;
       @Field(size=200) public String purposeCode;
       @Field(size=200) public String channelId;
       @Field(size=200) public String userId;
       @Field(size=200) public String benAdd3;
       @Field(size=200) public String caSchemeCode;
       @Field(size=200) public String rate;
       @Field(size=200) public String benAdd2;
       @Field(size=200) public String remAcctNo;
       @Field(size=200) public String benAdd1;
       @Field(size=200) public String remCustId;
       @Field(size=200) public String instrumentNumber;
       @Field(size=200) public String tranNumonicCode;
       @Field(size=200) public String benName;
       @Field(size=200) public String isinNumber;
       @Field(size=200) public String partTranSrlNum;
       @Field(size=200) public String tranParticular;
       @Field(size=200) public String custId;
       @Field public java.sql.Timestamp eventts;
       @Field(size=200) public String bankCode;
       @Field(size=11) public Double usdEqvAmt;
       @Field(size=200) public String tdLiquidationType;
       @Field(size=200) public String remCntryCode;
       @Field(size=200) public String cptyAcNo;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=200) public String tranCurr;
       @Field(size=200) public String instrumentType;
       @Field(size=11) public Double refTranAmt;
       @Field(size=200) public String remCity;
       @Field(size=11) public Double effAvailableBalance;
       @Field(size=200) public String ucicId;


    @JsonIgnore
    public ITable<FT_CoreDeclineTxnEvent> t = AEF.getITable(this);

	public FT_CoreDeclineTxnEvent(){}

    public FT_CoreDeclineTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("CoreDeclineTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTodGrantAmount(EventHelper.toDouble(json.getString("TOD_Grant_Amount")));
            setValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setBenCity(json.getString("BEN_CITY"));
            setOnlineBatch(json.getString("online_batch"));
            setTranAmount(EventHelper.toDouble(json.getString("Tran_amount")));
            setBranch(json.getString("BRANCH"));
            setBenCntryCode(json.getString("BEN_CNTRY_CODE"));
            setRefTranCrncy(json.getString("ref_tran_crncy"));
            setPurposeDesc(json.getString("PURPOSE_DESC"));
            setAccountType(json.getString("Account_Type"));
            setRemName(json.getString("REM_NAME"));
            setTranId(json.getString("tran_id"));
            setBenBic(json.getString("BEN_BIC"));
            setHostId(json.getString("host_id"));
            setTranAmt(EventHelper.toDouble(json.getString("TRAN_AMT")));
            setProductDesc(json.getString("Product_desc"));
            setBranchId(json.getString("Branch_Id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTdMaturityDate(EventHelper.toTimestamp(json.getString("TD_Maturity_Date")));
            setAcctName(json.getString("acct_name"));
            setBenAcctNo(json.getString("BEN_ACCT_NO"));
            setBenCustId(json.getString("BEN_CUST_ID"));
            setPstdDate(EventHelper.toTimestamp(json.getString("pstd_date")));
            setSystem(json.getString("SYSTEM"));
            setAtmId(json.getString("ATM_ID"));
            setProductCode(json.getString("product_code"));
            setDrCr(json.getString("DRCR"));
            setClientAccNo(json.getString("CLIENT_ACC_NO"));
            setRemBic(json.getString("REM_BIC"));
            setPstdFlg(json.getString("pstd_flg"));
            setStatus(json.getString("status"));
            setHostType(json.getString("Host_Type"));
            setRemAdd1(json.getString("REM_ADD1"));
            setAcctId(json.getString("acct_id"));
            setNumericCode(json.getString("Numeric_Code"));
            setRemType(json.getString("REM_TYPE"));
            setRemAdd3(json.getString("REM_ADD3"));
            setInrAmount(EventHelper.toDouble(json.getString("INR_AMOUNT")));
            setRemAdd2(json.getString("REM_ADD2"));
            setTrnDate(EventHelper.toTimestamp(json.getString("TRN_DATE")));
            setPurposeCode(json.getString("PURPOSE_CODE"));
            setChannelId(json.getString("Channel_Id"));
            setUserId(json.getString("User_id"));
            setBenAdd3(json.getString("BEN_ADD3"));
            setCaSchemeCode(json.getString("CA_Scheme_Code"));
            setRate(json.getString("rate"));
            setBenAdd2(json.getString("BEN_ADD2"));
            setRemAcctNo(json.getString("REM_ACCT_NO"));
            setBenAdd1(json.getString("BEN_ADD1"));
            setRemCustId(json.getString("REM_CUST_ID"));
            setInstrumentNumber(json.getString("Instrument_Number"));
            setTranNumonicCode(json.getString("Tran_numonic_code"));
            setBenName(json.getString("BEN_NAME"));
            setIsinNumber(json.getString("ISIN_Number"));
            setPartTranSrlNum(json.getString("part_tran_srl_num"));
            setTranParticular(json.getString("tran_particular"));
            setCustId(json.getString("cust_id"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setBankCode(json.getString("bank_code"));
            setUsdEqvAmt(EventHelper.toDouble(json.getString("USD_EQV_AMT")));
            setTdLiquidationType(json.getString("TD_Liquidation_Type"));
            setRemCntryCode(json.getString("REM_CNTRY_CODE"));
            setCptyAcNo(json.getString("CPTY_AC_NO"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setTranCurr(json.getString("TRAN_CURR"));
            setInstrumentType(json.getString("Instrument_Type"));
            setRefTranAmt(EventHelper.toDouble(json.getString("ref_tran_amt")));
            setRemCity(json.getString("REM_CITY"));
            setEffAvailableBalance(EventHelper.toDouble(json.getString("Eff_Available_Balance")));
            setUcicId(json.getString("ucic_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FD"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public Double getTodGrantAmount(){ return todGrantAmount; }

    public java.sql.Timestamp getValueDate(){ return valueDate; }

    public String getBenCity(){ return benCity; }

    public String getOnlineBatch(){ return onlineBatch; }

    public Double getTranAmount(){ return tranAmount; }

    public String getBranch(){ return branch; }

    public String getBenCntryCode(){ return benCntryCode; }

    public String getRefTranCrncy(){ return refTranCrncy; }

    public String getPurposeDesc(){ return purposeDesc; }

    public String getAccountType(){ return accountType; }

    public String getRemName(){ return remName; }

    public String getTranId(){ return tranId; }

    public String getBenBic(){ return benBic; }

    public String getHostId(){ return hostId; }

    public Double getTranAmt(){ return tranAmt; }

    public String getProductDesc(){ return productDesc; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public java.sql.Timestamp getTdMaturityDate(){ return tdMaturityDate; }

    public String getAcctName(){ return acctName; }

    public String getBenAcctNo(){ return benAcctNo; }

    public String getBenCustId(){ return benCustId; }

    public java.sql.Timestamp getPstdDate(){ return pstdDate; }

    public String getSystem(){ return system; }

    public String getAtmId(){ return atmId; }

    public String getProductCode(){ return productCode; }

    public String getDrCr(){ return drCr; }

    public String getClientAccNo(){ return clientAccNo; }

    public String getRemBic(){ return remBic; }

    public String getPstdFlg(){ return pstdFlg; }

    public String getStatus(){ return status; }

    public String getHostType(){ return hostType; }

    public String getRemAdd1(){ return remAdd1; }

    public String getAcctId(){ return acctId; }

    public String getNumericCode(){ return numericCode; }

    public String getRemType(){ return remType; }

    public String getRemAdd3(){ return remAdd3; }

    public Double getInrAmount(){ return inrAmount; }

    public String getRemAdd2(){ return remAdd2; }

    public java.sql.Timestamp getTrnDate(){ return trnDate; }

    public String getPurposeCode(){ return purposeCode; }

    public String getChannelId(){ return channelId; }

    public String getUserId(){ return userId; }

    public String getBenAdd3(){ return benAdd3; }

    public String getCaSchemeCode(){ return caSchemeCode; }

    public String getRate(){ return rate; }

    public String getBenAdd2(){ return benAdd2; }

    public String getRemAcctNo(){ return remAcctNo; }

    public String getBenAdd1(){ return benAdd1; }

    public String getRemCustId(){ return remCustId; }

    public String getInstrumentNumber(){ return instrumentNumber; }

    public String getTranNumonicCode(){ return tranNumonicCode; }

    public String getBenName(){ return benName; }

    public String getIsinNumber(){ return isinNumber; }

    public String getPartTranSrlNum(){ return partTranSrlNum; }

    public String getTranParticular(){ return tranParticular; }

    public String getCustId(){ return custId; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getBankCode(){ return bankCode; }

    public Double getUsdEqvAmt(){ return usdEqvAmt; }

    public String getTdLiquidationType(){ return tdLiquidationType; }

    public String getRemCntryCode(){ return remCntryCode; }

    public String getCptyAcNo(){ return cptyAcNo; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getTranCurr(){ return tranCurr; }

    public String getInstrumentType(){ return instrumentType; }

    public Double getRefTranAmt(){ return refTranAmt; }

    public String getRemCity(){ return remCity; }

    public Double getEffAvailableBalance(){ return effAvailableBalance; }

    public String getUcicId(){ return ucicId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTodGrantAmount(Double val){ this.todGrantAmount = val; }
    public void setValueDate(java.sql.Timestamp val){ this.valueDate = val; }
    public void setBenCity(String val){ this.benCity = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranAmount(Double val){ this.tranAmount = val; }
    public void setBranch(String val){ this.branch = val; }
    public void setBenCntryCode(String val){ this.benCntryCode = val; }
    public void setRefTranCrncy(String val){ this.refTranCrncy = val; }
    public void setPurposeDesc(String val){ this.purposeDesc = val; }
    public void setAccountType(String val){ this.accountType = val; }
    public void setRemName(String val){ this.remName = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setBenBic(String val){ this.benBic = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setProductDesc(String val){ this.productDesc = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTdMaturityDate(java.sql.Timestamp val){ this.tdMaturityDate = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setBenAcctNo(String val){ this.benAcctNo = val; }
    public void setBenCustId(String val){ this.benCustId = val; }
    public void setPstdDate(java.sql.Timestamp val){ this.pstdDate = val; }
    public void setSystem(String val){ this.system = val; }
    public void setAtmId(String val){ this.atmId = val; }
    public void setProductCode(String val){ this.productCode = val; }
    public void setDrCr(String val){ this.drCr = val; }
    public void setClientAccNo(String val){ this.clientAccNo = val; }
    public void setRemBic(String val){ this.remBic = val; }
    public void setPstdFlg(String val){ this.pstdFlg = val; }
    public void setStatus(String val){ this.status = val; }
    public void setHostType(String val){ this.hostType = val; }
    public void setRemAdd1(String val){ this.remAdd1 = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setNumericCode(String val){ this.numericCode = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setRemAdd3(String val){ this.remAdd3 = val; }
    public void setInrAmount(Double val){ this.inrAmount = val; }
    public void setRemAdd2(String val){ this.remAdd2 = val; }
    public void setTrnDate(java.sql.Timestamp val){ this.trnDate = val; }
    public void setPurposeCode(String val){ this.purposeCode = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setBenAdd3(String val){ this.benAdd3 = val; }
    public void setCaSchemeCode(String val){ this.caSchemeCode = val; }
    public void setRate(String val){ this.rate = val; }
    public void setBenAdd2(String val){ this.benAdd2 = val; }
    public void setRemAcctNo(String val){ this.remAcctNo = val; }
    public void setBenAdd1(String val){ this.benAdd1 = val; }
    public void setRemCustId(String val){ this.remCustId = val; }
    public void setInstrumentNumber(String val){ this.instrumentNumber = val; }
    public void setTranNumonicCode(String val){ this.tranNumonicCode = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setIsinNumber(String val){ this.isinNumber = val; }
    public void setPartTranSrlNum(String val){ this.partTranSrlNum = val; }
    public void setTranParticular(String val){ this.tranParticular = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setBankCode(String val){ this.bankCode = val; }
    public void setUsdEqvAmt(Double val){ this.usdEqvAmt = val; }
    public void setTdLiquidationType(String val){ this.tdLiquidationType = val; }
    public void setRemCntryCode(String val){ this.remCntryCode = val; }
    public void setCptyAcNo(String val){ this.cptyAcNo = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setTranCurr(String val){ this.tranCurr = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setRefTranAmt(Double val){ this.refTranAmt = val; }
    public void setRemCity(String val){ this.remCity = val; }
    public void setEffAvailableBalance(Double val){ this.effAvailableBalance = val; }
    public void setUcicId(String val){ this.ucicId = val; }

    /* Custom Getters*/
    @JsonIgnore
    public Double getTOD_Grant_Amount(){ return todGrantAmount; }
    @JsonIgnore
    public java.sql.Timestamp getValue_date(){ return valueDate; }
    @JsonIgnore
    public String getBEN_CITY(){ return benCity; }
    @JsonIgnore
    public String getOnline_batch(){ return onlineBatch; }
    @JsonIgnore
    public Double getTran_amount(){ return tranAmount; }
    @JsonIgnore
    public String getBEN_CNTRY_CODE(){ return benCntryCode; }
    @JsonIgnore
    public String getRef_tran_crncy(){ return refTranCrncy; }
    @JsonIgnore
    public String getPURPOSE_DESC(){ return purposeDesc; }
    @JsonIgnore
    public String getAccount_Type(){ return accountType; }
    @JsonIgnore
    public String getREM_NAME(){ return remName; }
    @JsonIgnore
    public String getTran_id(){ return tranId; }
    @JsonIgnore
    public String getBEN_BIC(){ return benBic; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }
    @JsonIgnore
    public Double getTRAN_AMT(){ return tranAmt; }
    @JsonIgnore
    public String getProduct_desc(){ return productDesc; }
    @JsonIgnore
    public String getBranch_Id(){ return branchId; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getTran_crncy_code(){ return tranCrncyCode; }
    @JsonIgnore
    public java.sql.Timestamp getTD_Maturity_Date(){ return tdMaturityDate; }
    @JsonIgnore
    public String getAcct_name(){ return acctName; }
    @JsonIgnore
    public String getBEN_ACCT_NO(){ return benAcctNo; }
    @JsonIgnore
    public String getBEN_CUST_ID(){ return benCustId; }
    @JsonIgnore
    public java.sql.Timestamp getPstd_date(){ return pstdDate; }
    @JsonIgnore
    public String getATM_ID(){ return atmId; }
    @JsonIgnore
    public String getProduct_code(){ return productCode; }
    @JsonIgnore
    public String getCLIENT_ACC_NO(){ return clientAccNo; }
    @JsonIgnore
    public String getREM_BIC(){ return remBic; }
    @JsonIgnore
    public String getPstd_flg(){ return pstdFlg; }
    @JsonIgnore
    public String getHost_Type(){ return hostType; }
    @JsonIgnore
    public String getREM_ADD1(){ return remAdd1; }
    @JsonIgnore
    public String getAcct_id(){ return acctId; }
    @JsonIgnore
    public String getNumeric_Code(){ return numericCode; }
    @JsonIgnore
    public String getREM_TYPE(){ return remType; }
    @JsonIgnore
    public String getREM_ADD3(){ return remAdd3; }
    @JsonIgnore
    public Double getINR_AMOUNT(){ return inrAmount; }
    @JsonIgnore
    public String getREM_ADD2(){ return remAdd2; }
    @JsonIgnore
    public java.sql.Timestamp getTRN_DATE(){ return trnDate; }
    @JsonIgnore
    public String getPURPOSE_CODE(){ return purposeCode; }
    @JsonIgnore
    public String getChannel_Id(){ return channelId; }
    @JsonIgnore
    public String getUser_id(){ return userId; }
    @JsonIgnore
    public String getBEN_ADD3(){ return benAdd3; }
    @JsonIgnore
    public String getCA_Scheme_Code(){ return caSchemeCode; }
    @JsonIgnore
    public String getBEN_ADD2(){ return benAdd2; }
    @JsonIgnore
    public String getREM_ACCT_NO(){ return remAcctNo; }
    @JsonIgnore
    public String getBEN_ADD1(){ return benAdd1; }
    @JsonIgnore
    public String getREM_CUST_ID(){ return remCustId; }
    @JsonIgnore
    public String getInstrument_Number(){ return instrumentNumber; }
    @JsonIgnore
    public String getTran_numonic_code(){ return tranNumonicCode; }
    @JsonIgnore
    public String getBEN_NAME(){ return benName; }
    @JsonIgnore
    public String getISIN_Number(){ return isinNumber; }
    @JsonIgnore
    public String getPart_tran_srl_num(){ return partTranSrlNum; }
    @JsonIgnore
    public String getTran_particular(){ return tranParticular; }
    @JsonIgnore
    public String getCust_id(){ return custId; }
    @JsonIgnore
    public String getBank_code(){ return bankCode; }
    @JsonIgnore
    public Double getUSD_EQV_AMT(){ return usdEqvAmt; }
    @JsonIgnore
    public String getTD_Liquidation_Type(){ return tdLiquidationType; }
    @JsonIgnore
    public String getREM_CNTRY_CODE(){ return remCntryCode; }
    @JsonIgnore
    public String getCPTY_AC_NO(){ return cptyAcNo; }
    @JsonIgnore
    public java.sql.Timestamp getTran_date(){ return tranDate; }
    @JsonIgnore
    public String getTRAN_CURR(){ return tranCurr; }
    @JsonIgnore
    public String getInstrument_Type(){ return instrumentType; }
    @JsonIgnore
    public Double getRef_tran_amt(){ return refTranAmt; }
    @JsonIgnore
    public String getREM_CITY(){ return remCity; }
    @JsonIgnore
    public Double getEff_Available_Balance(){ return effAvailableBalance; }
    @JsonIgnore
    public String getUcic_id(){ return ucicId; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_CoreDeclineTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
       //String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        //wsInfoSet.add(new WorkspaceInfo("User", userKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_CoreDeclineTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "CoreDeclineTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
