clari5.custom.yes.db  {
       entity {
               inbox-email  {                      
                       attributes:[
 				{ name :branch-id,             type : "string:50"}
                                { name :email-id,              type : "string:100",  key=false}
                                { name :email-escalation_1,    type : "string:100",  key=false}
                                { name :email-escalation_2,    type : "string:100",  key=false}
                                { name :last-modified-time,    type : timestamp}
                                { name :last-modified-by,      type : "string:25",   key=false}                
                                { name: user_id , type ="string:50" , key=true }				
                                { name: email_escalation,type ="string:500" }
			
                               ]
criteria-query {
                          name: InboxEmail
                          summary = [branch-id,email-id,email-escalation_1,email-escalation_2,last-modified-time,last-modified-by]
                          where {
                               branch-id: equal-clause
                          }
                          order = ["last-modified-time desc"]
                     }
                       }
       }
}




