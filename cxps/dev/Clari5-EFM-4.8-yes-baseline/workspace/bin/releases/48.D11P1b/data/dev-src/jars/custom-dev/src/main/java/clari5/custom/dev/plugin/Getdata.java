package clari5.custom.dev.plugin;


import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import com.granule.json.JSON;
import com.sun.webkit.graphics.Ref;
import org.apache.commons.fileupload.FileItem;
import org.apache.poi.util.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.List;
import java.util.Map;

import static jdk.nashorn.internal.objects.NativeError.getFileName;
import static jdk.nashorn.internal.runtime.ScriptObject.setGlobalObjectProto;

@Path("xlupload")
public class Getdata {
    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(Getdata.class);
    static String currentuser = null;

    @POST
    @Path("/getTables")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getTables() {
        System.out.println("Testing");
	JSONArray tablejson = new JSONArray();
        String tablename = null;
	
        try {
            String sql = "SELECT TABLENAME FROM CL5_UPLOAD_TBL";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery();) {
                cxpsLogger.info("Getting table name to show on drop down");
                while (rs.next()) {
                    tablename = rs.getString("TABLENAME");
                    tablejson.put(tablename);
                    cxpsLogger.info("Table Name: [" + tablename + "]");
                }
            }
            return tablejson;
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- Not getting table Name");
        }
        return tablejson;
    }


    @POST
    @Path("/validate")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String validateXls(MultipartFormDataInput input) {
        cxpsLogger.info("XL file Validation");
        System.out.println("input is : " + input);

        //String UPLOADED_FILE_PATH = "/home/jabert/Downloads/";
        String fileName = null;
        String tableName = null;
        String UPLOADED_FILE_PATH = null;
        Hocon h = new Hocon();
        boolean loadpath = h.loadFromContext("custom-path.conf");
        if (loadpath) {
            cxpsLogger.info("Sucessfully loaded the upload path");
        }
        Hocon hoconurl = h.get("path");
        UPLOADED_FILE_PATH = hoconurl.getString("UPLOADING_FILE_PATH");
        cxpsLogger.info("UPLOADING FILE PATH IS : [ " + UPLOADED_FILE_PATH + " ]");

        try {

            Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

            List<InputPart> inputParts = uploadForm.get("file");
            tableName = uploadForm.get("tableName").get(0).getBodyAsString();
            String[] values = tableName.split(":");
            tableName = values[0];
            String userName = values[1];
            System.out.println("Current  UserName is : [ " + userName + " ]");
            System.out.println("Table Name is : [ " + tableName + " ]");
            for (InputPart inputPart : inputParts) {
                try {
                    MultivaluedMap<String, String> header = inputPart.getHeaders();
                    fileName = getFileName(header);
                    System.out.println("File Name : [ " + fileName + " ]");
                    //convert the uploaded file to inputstream
                    InputStream inputStream = inputPart.getBody(InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    //constructs upload file path
                    fileName = UPLOADED_FILE_PATH + fileName;
                    writeFile(bytes, fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            System.out.println("File Written Sucessfully");
            if (tableName != null && fileName != null) {
                cxpsLogger.info("tableName  & fileName not Null hence validating");
                boolean valid = XcelValidation.validate(tableName, fileName);
                if (valid) {
                    XcelValidation.auditInsert(userName, tableName, fileName,"Validation");
                    return "validatesuccess";
                } else {
                    return "failed";
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @POST
    @Path("/submit")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.TEXT_PLAIN)
    public String submitXls(MultipartFormDataInput input) {
        cxpsLogger.info("XL file Validation");
        System.out.println("input is : " + input);
        String fileName = null;
        String tableName = null;
        String UPLOADED_FILE_PATH = null;
        Hocon h = new Hocon();
        boolean loadpath = h.loadFromContext("custom-path.conf");
        if (loadpath) {
            cxpsLogger.info("Sucessfully loaded the upload path");
        }
        Hocon hoconurl = h.get("path");
        UPLOADED_FILE_PATH = hoconurl.getString("UPLOADING_FILE_PATH");
        cxpsLogger.info("UPLOADING FILE PATH IS : [ " + UPLOADED_FILE_PATH + " ]");


        try {

            Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
            List<InputPart> inputParts = uploadForm.get("file");
            tableName = uploadForm.get("tableName").get(0).getBodyAsString();
            String[] values = tableName.split(":");
            tableName = values[0];
            String userName = values[1];
            System.out.println("Current  UserName is : [ " + userName + " ]");
            System.out.println("Table Name is : [ " + tableName + " ]");

            for (InputPart inputPart : inputParts) {
                try {
                    MultivaluedMap<String, String> header = inputPart.getHeaders();
                    fileName = getFileName(header);
                    System.out.println("File Name : [ " + fileName + " ]");
                    //convert the uploaded file to inputstream
                    InputStream inputStream = inputPart.getBody(InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    String unqfilname = String.valueOf(System.nanoTime());
                    fileName = UPLOADED_FILE_PATH + unqfilname + fileName;
                    writeFile(bytes, fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            System.out.println("File Written Sucessfully");
            if (tableName != null && fileName != null) {
                cxpsLogger.info("tableName  & fileName not Null hence validating");
                boolean valid = XcelValidation.validateSubmit(tableName, fileName);
                if (valid) {
                    XcelValidation.auditInsert(userName, tableName, fileName,"Truncated");
                    XcelValidation.truncateTable(tableName);
                    return "submitsuccess";
                } else {
                    return "failed";
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    private String getFileName(MultivaluedMap<String, String> header) {

        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {
                String[] name = filename.split("=");
                String finalFileName = name[1].trim().replaceAll("\"", "");
                return finalFileName;
            }
        }
        return "unknown";
    }


    private void writeFile(byte[] content, String filename) throws IOException {
        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fop = new FileOutputStream(file);
        fop.write(content);
        cxpsLogger.info("File Written sucessfully : [ " + filename + " ]");
        fop.flush();
        fop.close();
    }




}
