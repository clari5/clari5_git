import { Component } from "@angular/core";
import { NbbDataService } from '../services/nbb.data.service';
import { CommonFunctions } from '../services/common.functions';
import { NbbService } from '../services/nbb.service';
import { PanelMenuModule, MenuItem } from 'primeng/primeng';
import { concat } from "rxjs/observable/concat";

@Component({
    selector: 'kotak',
    template: `

 <div class="container-fluid">
    <div class="row content">
	<!--left side of the ui-->
        <div class="col-sm-3 ">
            
            <div class="panel-group">
                <div class="panel panel-default" >
                    <div class="panel-heading" style="color: #fff; background-color: #2486b5;">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#tm" (click)="showhome()">Inbox Manager</a>
                        </h4>
                    </div>
                    <div id="tm" class="panel-collapse collapse" >
                        <div class="btn-group-vertical" style="height:400px;overflow:auto; width:290px">
                            <button type="button" class="btn btn-default" style="border: none;text-align:left" (click)="showScreen()">Create Template</button>
                            <!-- to load the existing template-->
                            <button type="button" class="btn btn-default" style="border: none;text-align:left" *ngFor="let scen of this.nbbDataService?.scenarios" (click)="openScreen(scen)">{{scen}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--center part of the ui where form is loaded-->
        <div class="col-sm-6">
            <inbox></inbox>
        </div>
<!--rightside of the ui where eventlist and avilable tags will be loaded-->
        <!--<div class="col-sm-3 ">
            <h4 style="text-align: left; color: #2486b5; font-weight: bold;">Avilable Tags</h4>
            <div class="panel-group">
		 <!--to load the eventlist-->
                <!--<div class="panel panel-default" *ngFor="let event of this.nbbDataService?.eventlist">
                    <div class="panel-heading" style="color: #fff; background-color: #2486b5;">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#s{{event}}" (click)="onEvent(this.nbbDataService.scenario,event)">{{event}}</a>
                        </h4>
                    </div>
			 <!--to load the avilable tags-->
                   <!-- <div id="s{{event}}" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li style="list-style-type:none;margin-left: 20px;" *ngFor="let tag of this.nbbDataService?.tags">{{tag}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>
                `,
})
export class NbbMainComponent extends CommonFunctions {

    nbbDataService: NbbDataService;
    nbbService: NbbService;
    private postData: string;
    private postDataa: string;
    private method: string;

    constructor(nbbDataService: NbbDataService, nbbService: NbbService) {
        super();
        this.nbbDataService = nbbDataService;
        this.nbbService = nbbService;
        //to load the existing templates list on load of the ui
        this.postData = JSON.stringify(
            {

            });
        //this.method = "init";

        console.log(this.postData);
      //  console.log(this.method);
        this.nbbService.getLoadData("scenario", this.nbbDataService.url, "init", this.postData);
        this.postDataa = JSON.stringify(
            {

            });
      //  this.method = "scenarioList";

        console.log(this.postData);
        console.log(this.method);
       // this.nbbService.getLoadData("tempscenario", this.nbbDataService.url,"scenarioList", this.postDataa);
       setTimeout(() => {  
        this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postData);
      }, 2000);
    }
    //to replace the newline character
    escapeRegExp(string: any) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }

    /* Define functin to find and replace specified term with replacement string */
    replaceAll(str: any, term: any, replacement: any) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    }

    //on click of the existing template
    public openScreen(val: any) {
        this.nbbDataService.showscreen = true;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.searchscenario = true;
        this.nbbDataService.showavilscreen = false;
        this.nbbDataService.scenario = val;
        this.nbbDataService.sname = val;
        console.log("scenario", this.nbbDataService.scenario);
        console.log("sname", this.nbbDataService.sname);
        
        //based on scenario parse and load the existing template data
        //this.nbbDataService.temp = this.nbbDataService.allscenarios[this.nbbDataService.scenario];
        this.nbbDataService.temp = JSON.parse(this.replaceAll(this.nbbDataService.allscenarios[this.nbbDataService.scenario], '\n', ' <br>'));
        console.log("temp data", this.nbbDataService.temp);

        //message
       /* this.nbbDataService.message = this.replaceAll(this.nbbDataService.temp.message, '<br>', '\n');
        //subject
        this.nbbDataService.subject = this.nbbDataService.temp.msgSubject;*/
        //checkbox
        this.nbbDataService.icheckbox = this.nbbDataService.temp.autoInboxChqFlg;
       // this.nbbDataService.echeckbox = this.nbbDataService.temp.autoEmailChqFlg;

        if (this.nbbDataService.icheckbox === "Y"){ //&& this.nbbDataService.echeckbox === "Y") {
            this.nbbDataService.officeLIST[0].checked = true;
            //this.nbbDataService.officeLIST[1].checked = true;
        } /*else if (this.nbbDataService.icheckbox === "Y" || this.nbbDataService.echeckbox === "N") {
            this.nbbDataService.officeLIST[0].checked = true;
            this.nbbDataService.officeLIST[1].checked = false;
        } else if (this.nbbDataService.icheckbox === "N" && this.nbbDataService.echeckbox === "Y") {
            this.nbbDataService.officeLIST[0].checked = false;
            this.nbbDataService.officeLIST[1].checked = true;
        } */else {
            this.nbbDataService.officeLIST[0].checked = false;
            //this.nbbDataService.officeLIST[1].checked = false;
        }
        //to load the eventlist
       /* if (typeof this.nbbDataService.test[this.nbbDataService.scenario] == 'undefined') {
            this.nbbDataService.eventlist = "undefined";
            console.log(this.nbbDataService.eventlist);
        } else {
            this.nbbDataService.eventlist = this.nbbDataService.test[this.nbbDataService.scenario]["eventList"];
            console.log("eventlist", this.nbbDataService.eventlist);
        }*/

    }
    //on click of the create template
    public showScreen() {

        this.nbbDataService.showscreen = false;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.searchscenario = true;
        this.nbbDataService.showavilscreen = true;
        this.nbbDataService.eventlist = [];
        this.nbbDataService.officeLIST[0].checked = false;
        this.nbbDataService.officeLIST[1].checked = false;
        this.nbbDataService.sname = "";
    }
    //on cleck of template manager
    public showhome() {
        this.nbbDataService.scenarios=[];
        this.nbbDataService.eventlist=[];
        this.nbbDataService.showscreen = true;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.showavilscreen = true;
        this.nbbDataService.searchscenario = false;
        //to load the existing scenario list
        this.postData = JSON.stringify(
            {

            });
       // this.method = "init";

        console.log(this.postData);
        console.log(this.method);
        this.nbbService.getLoadData("scenario", this.nbbDataService.url, "init", this.postData);
        this.postDataa = JSON.stringify(
            {

            });
       // this.method = "scenarioList";

        console.log(this.postData);
        console.log(this.method);
       // this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postDataa);
       setTimeout(() => {  
        this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postData);
      }, 2000);
        this.nbbDataService.clear();

    }

    //on click of the event to load the avilable tags
   /* onEvent(s: any, ev: any) {
        console.log("s", s);
        console.log("ev", ev);
        if (typeof this.nbbDataService.test[s][ev] == 'undefined') {
            this.nbbDataService.tags = "undefined";
            console.log(this.nbbDataService.tags);
        } else {
            this.nbbDataService.tags = this.nbbDataService.test[s][ev];
            console.log("eventlist", this.nbbDataService.tags);
        }
        // let x:any;
        for (var i = 0; i < this.nbbDataService.tags.length; i++) {
            this.nbbDataService.tag[i] = this.nbbDataService.tags[i] + "$";


            // this.nbbDataService.avaltag=this.nbbDataService.tags[i].concat(this.nbbDataService.tags[i]+"$";
        }
        this.nbbDataService.avltag = this.nbbDataService.tag;
        console.log("tag", this.nbbDataService.avltag);
    }*/
}
