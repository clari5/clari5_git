cxps.events.event.ft-coreDecline-txn{
  table-name : EVENT_FT_COREDECLINE
  event-mnemonic: FD
  workspaces : {
    ACCOUNT : acct_id,
   //USER : User_id
  }
  event-attributes : {
	acct-id: {db : true ,raw_name : acct_id ,type : "string:200", custom-getter:Acct_id}
	cust-id: {db : true ,raw_name : cust_id ,type : "string:200", custom-getter:Cust_id}
	ucic-id: {db : true ,raw_name : ucic_id ,type : "string:200", custom-getter:Ucic_id}
	product-code: {db : true ,raw_name : product_code ,type : "string:200", custom-getter:Product_code}
	dr-cr: {db : true ,raw_name : DRCR ,type : "string:200"}
	tran-amount: {db : true ,raw_name : Tran_amount ,type : "number:11,2", custom-getter:Tran_amount}
	branch-id: {db : true ,raw_name : Branch_Id ,type : "string:200", custom-getter:Branch_Id}
	td-maturity-date: {db : true ,raw_name : TD_Maturity_Date ,type : timestamp, custom-getter:TD_Maturity_Date}
	eff-available-balance: {db : true ,raw_name : Eff_Available_Balance ,type : "number:11,2", custom-getter:Eff_Available_Balance}
	channel-id: {db : true ,raw_name : Channel_Id ,type : "string:200", custom-getter:Channel_Id}
	tran-numonic-code: {db : true ,raw_name : Tran_numonic_code ,type : "string:200", custom-getter:Tran_numonic_code}
	host-type: {db : true ,raw_name : Host_Type ,type : "string:200", custom-getter:Host_Type}
	numeric-code: {db : true ,raw_name : Numeric_Code ,type : "string:200", custom-getter:Numeric_Code}
	instrument-type: {db : true ,raw_name : Instrument_Type ,type : "string:200", custom-getter:Instrument_Type}
	instrument-number: {db : true ,raw_name : Instrument_Number ,type : "string:200", custom-getter:Instrument_Number}
	atm-id: {db : true ,raw_name : ATM_ID ,type : "string:200", custom-getter:ATM_ID}
	isin-number: {db : true ,raw_name : ISIN_Number ,type : "string:200", custom-getter:ISIN_Number}
	tran-date: {db : true ,raw_name : tran_date ,type : timestamp, custom-getter:Tran_date}
	pstd-date: {db : true ,raw_name : pstd_date ,type : timestamp, custom-getter:Pstd_date}
	tran-id: {db : true ,raw_name : tran_id ,type : "string:200", custom-getter:Tran_id}
	part-tran-srl-num: {db : true ,raw_name : part_tran_srl_num ,type : "string:200", custom-getter:Part_tran_srl_num}
	value-date: {db : true ,raw_name : value_date ,type : timestamp, custom-getter:Value_date}
	tran-crncy-code: {db : true ,raw_name : tran_crncy_code ,type : "string:200", custom-getter:Tran_crncy_code}
	ref-tran-amt: {db : true ,raw_name : ref_tran_amt ,type : "number:11,2", custom-getter:Ref_tran_amt}
	ref-tran-crncy: {db : true ,raw_name : ref_tran_crncy ,type : "string:200", custom-getter:Ref_tran_crncy}
	rate: {db : true ,raw_name : rate ,type : "string:200"}
	tran-particular: {db : true ,raw_name : tran_particular ,type : "string:200", custom-getter:Tran_particular}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	bank-code: {db : true ,raw_name : bank_code ,type : "string:200", custom-getter:Bank_code}
	pstd-flg: {db : true ,raw_name : pstd_flg ,type : "string:200", custom-getter:Pstd_flg}
	online-batch: {db : true ,raw_name : online_batch ,type : "string:200", custom-getter:Online_batch}
	User-id: {db : true ,raw_name : User_id ,type : "string:200", custom-getter:User_id}
	status: {db : true ,raw_name : status ,type : "string:200"}
	host-id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id}
	acct-name: {db : true ,raw_name : acct_name ,type : "string:200", custom-getter:Acct_name}
	td-liquidation-type: {db : true ,raw_name : TD_Liquidation_Type ,type : "string:200", custom-getter:TD_Liquidation_Type}
	account-type: {db : true ,raw_name : Account_Type ,type : "string:200", custom-getter:Account_Type}
	tod-grant-amount: {db : true ,raw_name : TOD_Grant_Amount ,type : "number:11,2", custom-getter:TOD_Grant_Amount}
	ca-scheme-code: {db : true ,raw_name : CA_Scheme_Code ,type : "string:200", custom-getter:CA_Scheme_Code}
	system: {db : true ,raw_name : SYSTEM ,type : "string:200"}
	rem-type: {db : true ,raw_name : REM_TYPE ,type : "string:200", custom-getter:REM_TYPE}
	branch: {db : true ,raw_name : BRANCH ,type : "string:200"}
	tran-curr: {db : true ,raw_name : TRAN_CURR ,type : "string:200", custom-getter:TRAN_CURR}
	tran-amt: {db : true ,raw_name : TRAN_AMT ,type : "number:11,2", custom-getter:TRAN_AMT}
	usd-eqv-amt: {db : true ,raw_name : USD_EQV_AMT ,type : "number:11,2", custom-getter:USD_EQV_AMT}
	inr-amount: {db : true ,raw_name : INR_AMOUNT ,type : "number:11,2", custom-getter:INR_AMOUNT}
	purpose-code: {db : true ,raw_name : PURPOSE_CODE ,type : "string:200", custom-getter:PURPOSE_CODE}
	purpose-desc: {db : true ,raw_name : PURPOSE_DESC ,type : "string:200", custom-getter:PURPOSE_DESC}
	rem-cust-id: {db : true ,raw_name : REM_CUST_ID ,type : "string:200", custom-getter:REM_CUST_ID}
	rem-name: {db : true ,raw_name : REM_NAME ,type : "string:200", custom-getter:REM_NAME}
	rem-add1: {db : true ,raw_name : REM_ADD1 ,type : "string:200", custom-getter:REM_ADD1}
	rem-add2: {db : true ,raw_name : REM_ADD2 ,type : "string:200", custom-getter:REM_ADD2}
	rem-add3: {db : true ,raw_name : REM_ADD3 ,type : "string:200", custom-getter:REM_ADD3}
	rem-city: {db : true ,raw_name : REM_CITY ,type : "string:200", custom-getter:REM_CITY}
	rem-cntry-code: {db : true ,raw_name : REM_CNTRY_CODE ,type : "string:200", custom-getter:REM_CNTRY_CODE}
	ben-cust-id: {db : true ,raw_name : BEN_CUST_ID ,type : "string:200", custom-getter:BEN_CUST_ID}
	ben-name: {db : true ,raw_name : BEN_NAME ,type : "string:200", custom-getter:BEN_NAME}
	ben-add1: {db : true ,raw_name : BEN_ADD1 ,type : "string:200", custom-getter:BEN_ADD1}
	ben-add2: {db : true ,raw_name : BEN_ADD2 ,type : "string:200", custom-getter:BEN_ADD2}
	ben-add3: {db : true ,raw_name : BEN_ADD3 ,type : "string:200", custom-getter:BEN_ADD3}
	ben-city: {db : true ,raw_name : BEN_CITY ,type : "string:200", custom-getter:BEN_CITY}
	ben-cntry-code: {db : true ,raw_name : BEN_CNTRY_CODE ,type : "string:200", custom-getter:BEN_CNTRY_CODE}
	client-acc-no: {db : true ,raw_name : CLIENT_ACC_NO ,type : "string:200", custom-getter:CLIENT_ACC_NO}
	cpty-ac-no: {db : true ,raw_name : CPTY_AC_NO ,type : "string:200", custom-getter:CPTY_AC_NO}
	ben-acct-no: {db : true ,raw_name : BEN_ACCT_NO ,type : "string:200", custom-getter:BEN_ACCT_NO}
	ben-bic: {db : true ,raw_name : BEN_BIC ,type : "string:200", custom-getter:BEN_BIC}
	rem-acct-no: {db : true ,raw_name : REM_ACCT_NO ,type : "string:200", custom-getter:REM_ACCT_NO}
	rem-bic: {db : true ,raw_name : REM_BIC ,type : "string:200", custom-getter:REM_BIC}
	trn-date: {db : true ,raw_name : TRN_DATE ,type : timestamp, custom-getter:TRN_DATE}
	product-desc: {db : true ,raw_name : Product_desc ,type : "string:200", custom-getter:Product_desc}
	eventts: {db : true ,raw_name : eventts ,type : timestamp}
	}
}
