// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_FundtransferEventMapper extends EventMapper<FT_FundtransferEvent> {

public FT_FundtransferEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_FundtransferEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_FundtransferEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_FundtransferEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_FundtransferEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_FundtransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_FundtransferEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTwoFaMode());
            preparedStatement.setString(i++, obj.getBeneficiaryName());
            preparedStatement.setString(i++, obj.getCustomerSegment());
            preparedStatement.setString(i++, obj.getIfscCode());
            preparedStatement.setString(i++, obj.getPayeeAccounttype());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setDouble(i++, obj.getBalanceAfterTransaction());
            preparedStatement.setString(i++, obj.getTranRecurrance());
            preparedStatement.setTimestamp(i++, obj.getStartDate());
            preparedStatement.setString(i++, obj.getAccountNumber());
            preparedStatement.setString(i++, obj.getCustSegment());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getGroupPayeeId());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getFrequency());
            preparedStatement.setString(i++, obj.getExchangeRate());
            preparedStatement.setString(i++, obj.getBenfPayeeId());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getPaymentMode());
            preparedStatement.setDouble(i++, obj.getTransferAmount());
            preparedStatement.setDouble(i++, obj.getCurrency());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getSettlementAmount());
            preparedStatement.setString(i++, obj.getBeneficaryUniqueIdentifier1());
            preparedStatement.setString(i++, obj.getTransactionType());
            preparedStatement.setString(i++, obj.getTwoFaStatus());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getObdxTransactionName());
            preparedStatement.setString(i++, obj.getBeneficiaryType1());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setDouble(i++, obj.getDebitAccountNumber());
            preparedStatement.setDouble(i++, obj.getBalanceBeforeTransaction());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getObdxModuleName());
            preparedStatement.setTimestamp(i++, obj.getBenfAddDate());
            preparedStatement.setTimestamp(i++, obj.getEndDate());
            preparedStatement.setString(i++, obj.getRiskBand());
            preparedStatement.setString(i++, obj.getPayeeComb());
            preparedStatement.setString(i++, obj.getSessionId());
            preparedStatement.setString(i++, obj.getWhitelistFlag());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_FUNDTRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_FundtransferEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_FundtransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FUNDTRANSFER"));
        putList = new ArrayList<>();

        for (FT_FundtransferEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TWO_FA_MODE",  obj.getTwoFaMode());
            p = this.insert(p, "BENEFICIARY_NAME",  obj.getBeneficiaryName());
            p = this.insert(p, "CUSTOMER_SEGMENT",  obj.getCustomerSegment());
            p = this.insert(p, "IFSC_CODE",  obj.getIfscCode());
            p = this.insert(p, "PAYEE_ACCOUNTTYPE",  obj.getPayeeAccounttype());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "BALANCE_AFTER_TRANSACTION", String.valueOf(obj.getBalanceAfterTransaction()));
            p = this.insert(p, "TRAN_RECURRANCE",  obj.getTranRecurrance());
            p = this.insert(p, "START_DATE", String.valueOf(obj.getStartDate()));
            p = this.insert(p, "ACCOUNT_NUMBER",  obj.getAccountNumber());
            p = this.insert(p, "CUST_SEGMENT",  obj.getCustSegment());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "GROUP_PAYEE_ID",  obj.getGroupPayeeId());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "FREQUENCY",  obj.getFrequency());
            p = this.insert(p, "EXCHANGE_RATE",  obj.getExchangeRate());
            p = this.insert(p, "BENF_PAYEE_ID",  obj.getBenfPayeeId());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "PAYMENT_MODE",  obj.getPaymentMode());
            p = this.insert(p, "TRANSFER_AMOUNT", String.valueOf(obj.getTransferAmount()));
            p = this.insert(p, "CURRENCY", String.valueOf(obj.getCurrency()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "SETTLEMENT_AMOUNT", String.valueOf(obj.getSettlementAmount()));
            p = this.insert(p, "BENEFICARY_UNIQUE_IDENTIFIER_1",  obj.getBeneficaryUniqueIdentifier1());
            p = this.insert(p, "TRANSACTION_TYPE",  obj.getTransactionType());
            p = this.insert(p, "TWO_FA_STATUS",  obj.getTwoFaStatus());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "OBDX_TRANSACTION_NAME",  obj.getObdxTransactionName());
            p = this.insert(p, "BENEFICIARY_TYPE1",  obj.getBeneficiaryType1());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "DEBIT_ACCOUNT_NUMBER", String.valueOf(obj.getDebitAccountNumber()));
            p = this.insert(p, "BALANCE_BEFORE_TRANSACTION", String.valueOf(obj.getBalanceBeforeTransaction()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "OBDX_MODULE_NAME",  obj.getObdxModuleName());
            p = this.insert(p, "BENF_ADD_DATE", String.valueOf(obj.getBenfAddDate()));
            p = this.insert(p, "END_DATE", String.valueOf(obj.getEndDate()));
            p = this.insert(p, "RISK_BAND",  obj.getRiskBand());
            p = this.insert(p, "PAYEE_COMB",  obj.getPayeeComb());
            p = this.insert(p, "SESSION_ID",  obj.getSessionId());
            p = this.insert(p, "WHITELIST_FLAG",  obj.getWhitelistFlag());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_FUNDTRANSFER"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_FUNDTRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_FUNDTRANSFER]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_FundtransferEvent obj = new FT_FundtransferEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTwoFaMode(rs.getString("TWO_FA_MODE"));
    obj.setBeneficiaryName(rs.getString("BENEFICIARY_NAME"));
    obj.setCustomerSegment(rs.getString("CUSTOMER_SEGMENT"));
    obj.setIfscCode(rs.getString("IFSC_CODE"));
    obj.setPayeeAccounttype(rs.getString("PAYEE_ACCOUNTTYPE"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setBalanceAfterTransaction(rs.getDouble("BALANCE_AFTER_TRANSACTION"));
    obj.setTranRecurrance(rs.getString("TRAN_RECURRANCE"));
    obj.setStartDate(rs.getTimestamp("START_DATE"));
    obj.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
    obj.setCustSegment(rs.getString("CUST_SEGMENT"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setGroupPayeeId(rs.getString("GROUP_PAYEE_ID"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setFrequency(rs.getString("FREQUENCY"));
    obj.setExchangeRate(rs.getString("EXCHANGE_RATE"));
    obj.setBenfPayeeId(rs.getString("BENF_PAYEE_ID"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setPaymentMode(rs.getString("PAYMENT_MODE"));
    obj.setTransferAmount(rs.getDouble("TRANSFER_AMOUNT"));
    obj.setCurrency(rs.getDouble("CURRENCY"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setSettlementAmount(rs.getDouble("SETTLEMENT_AMOUNT"));
    obj.setBeneficaryUniqueIdentifier1(rs.getString("BENEFICARY_UNIQUE_IDENTIFIER_1"));
    obj.setTransactionType(rs.getString("TRANSACTION_TYPE"));
    obj.setTwoFaStatus(rs.getString("TWO_FA_STATUS"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setObdxTransactionName(rs.getString("OBDX_TRANSACTION_NAME"));
    obj.setBeneficiaryType1(rs.getString("BENEFICIARY_TYPE1"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setDebitAccountNumber(rs.getDouble("DEBIT_ACCOUNT_NUMBER"));
    obj.setBalanceBeforeTransaction(rs.getDouble("BALANCE_BEFORE_TRANSACTION"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setObdxModuleName(rs.getString("OBDX_MODULE_NAME"));
    obj.setBenfAddDate(rs.getTimestamp("BENF_ADD_DATE"));
    obj.setEndDate(rs.getTimestamp("END_DATE"));
    obj.setRiskBand(rs.getString("RISK_BAND"));
    obj.setPayeeComb(rs.getString("PAYEE_COMB"));
    obj.setSessionId(rs.getString("SESSION_ID"));
    obj.setWhitelistFlag(rs.getString("WHITELIST_FLAG"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_FUNDTRANSFER]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_FundtransferEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_FundtransferEvent> events;
 FT_FundtransferEvent obj = new FT_FundtransferEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FUNDTRANSFER"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_FundtransferEvent obj = new FT_FundtransferEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTwoFaMode(getColumnValue(rs, "TWO_FA_MODE"));
            obj.setBeneficiaryName(getColumnValue(rs, "BENEFICIARY_NAME"));
            obj.setCustomerSegment(getColumnValue(rs, "CUSTOMER_SEGMENT"));
            obj.setIfscCode(getColumnValue(rs, "IFSC_CODE"));
            obj.setPayeeAccounttype(getColumnValue(rs, "PAYEE_ACCOUNTTYPE"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setBalanceAfterTransaction(EventHelper.toDouble(getColumnValue(rs, "BALANCE_AFTER_TRANSACTION")));
            obj.setTranRecurrance(getColumnValue(rs, "TRAN_RECURRANCE"));
            obj.setStartDate(EventHelper.toTimestamp(getColumnValue(rs, "START_DATE")));
            obj.setAccountNumber(getColumnValue(rs, "ACCOUNT_NUMBER"));
            obj.setCustSegment(getColumnValue(rs, "CUST_SEGMENT"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setGroupPayeeId(getColumnValue(rs, "GROUP_PAYEE_ID"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setFrequency(getColumnValue(rs, "FREQUENCY"));
            obj.setExchangeRate(getColumnValue(rs, "EXCHANGE_RATE"));
            obj.setBenfPayeeId(getColumnValue(rs, "BENF_PAYEE_ID"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setPaymentMode(getColumnValue(rs, "PAYMENT_MODE"));
            obj.setTransferAmount(EventHelper.toDouble(getColumnValue(rs, "TRANSFER_AMOUNT")));
            obj.setCurrency(EventHelper.toDouble(getColumnValue(rs, "CURRENCY")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setSettlementAmount(EventHelper.toDouble(getColumnValue(rs, "SETTLEMENT_AMOUNT")));
            obj.setBeneficaryUniqueIdentifier1(getColumnValue(rs, "BENEFICARY_UNIQUE_IDENTIFIER_1"));
            obj.setTransactionType(getColumnValue(rs, "TRANSACTION_TYPE"));
            obj.setTwoFaStatus(getColumnValue(rs, "TWO_FA_STATUS"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setObdxTransactionName(getColumnValue(rs, "OBDX_TRANSACTION_NAME"));
            obj.setBeneficiaryType1(getColumnValue(rs, "BENEFICIARY_TYPE1"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setDebitAccountNumber(EventHelper.toDouble(getColumnValue(rs, "DEBIT_ACCOUNT_NUMBER")));
            obj.setBalanceBeforeTransaction(EventHelper.toDouble(getColumnValue(rs, "BALANCE_BEFORE_TRANSACTION")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setObdxModuleName(getColumnValue(rs, "OBDX_MODULE_NAME"));
            obj.setBenfAddDate(EventHelper.toTimestamp(getColumnValue(rs, "BENF_ADD_DATE")));
            obj.setEndDate(EventHelper.toTimestamp(getColumnValue(rs, "END_DATE")));
            obj.setRiskBand(getColumnValue(rs, "RISK_BAND"));
            obj.setPayeeComb(getColumnValue(rs, "PAYEE_COMB"));
            obj.setSessionId(getColumnValue(rs, "SESSION_ID"));
            obj.setWhitelistFlag(getColumnValue(rs, "WHITELIST_FLAG"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FUNDTRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FUNDTRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TWO_FA_MODE\",\"BENEFICIARY_NAME\",\"CUSTOMER_SEGMENT\",\"IFSC_CODE\",\"PAYEE_ACCOUNTTYPE\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"BALANCE_AFTER_TRANSACTION\",\"TRAN_RECURRANCE\",\"START_DATE\",\"ACCOUNT_NUMBER\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"GROUP_PAYEE_ID\",\"DEVICE_ID\",\"FREQUENCY\",\"EXCHANGE_RATE\",\"BENF_PAYEE_ID\",\"ADDR_NETWORK\",\"PAYMENT_MODE\",\"TRANSFER_AMOUNT\",\"CURRENCY\",\"HOST_ID\",\"SETTLEMENT_AMOUNT\",\"BENEFICARY_UNIQUE_IDENTIFIER_1\",\"TRANSACTION_TYPE\",\"TWO_FA_STATUS\",\"IP_CITY\",\"OBDX_TRANSACTION_NAME\",\"BENEFICIARY_TYPE1\",\"USER_ID\",\"DEBIT_ACCOUNT_NUMBER\",\"BALANCE_BEFORE_TRANSACTION\",\"SYS_TIME\",\"CUST_ID\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"BENF_ADD_DATE\",\"END_DATE\",\"RISK_BAND\",\"PAYEE_COMB\",\"SESSION_ID\",\"WHITELIST_FLAG\"" +
              " FROM EVENT_FT_FUNDTRANSFER";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TWO_FA_MODE],[BENEFICIARY_NAME],[CUSTOMER_SEGMENT],[IFSC_CODE],[PAYEE_ACCOUNTTYPE],[ERROR_DESC],[SUCC_FAIL_FLG],[BALANCE_AFTER_TRANSACTION],[TRAN_RECURRANCE],[START_DATE],[ACCOUNT_NUMBER],[CUST_SEGMENT],[IP_COUNTRY],[GROUP_PAYEE_ID],[DEVICE_ID],[FREQUENCY],[EXCHANGE_RATE],[BENF_PAYEE_ID],[ADDR_NETWORK],[PAYMENT_MODE],[TRANSFER_AMOUNT],[CURRENCY],[HOST_ID],[SETTLEMENT_AMOUNT],[BENEFICARY_UNIQUE_IDENTIFIER_1],[TRANSACTION_TYPE],[TWO_FA_STATUS],[IP_CITY],[OBDX_TRANSACTION_NAME],[BENEFICIARY_TYPE1],[USER_ID],[DEBIT_ACCOUNT_NUMBER],[BALANCE_BEFORE_TRANSACTION],[SYS_TIME],[CUST_ID],[ERROR_CODE],[OBDX_MODULE_NAME],[BENF_ADD_DATE],[END_DATE],[RISK_BAND],[PAYEE_COMB],[SESSION_ID],[WHITELIST_FLAG]" +
              " FROM EVENT_FT_FUNDTRANSFER";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TWO_FA_MODE`,`BENEFICIARY_NAME`,`CUSTOMER_SEGMENT`,`IFSC_CODE`,`PAYEE_ACCOUNTTYPE`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`BALANCE_AFTER_TRANSACTION`,`TRAN_RECURRANCE`,`START_DATE`,`ACCOUNT_NUMBER`,`CUST_SEGMENT`,`IP_COUNTRY`,`GROUP_PAYEE_ID`,`DEVICE_ID`,`FREQUENCY`,`EXCHANGE_RATE`,`BENF_PAYEE_ID`,`ADDR_NETWORK`,`PAYMENT_MODE`,`TRANSFER_AMOUNT`,`CURRENCY`,`HOST_ID`,`SETTLEMENT_AMOUNT`,`BENEFICARY_UNIQUE_IDENTIFIER_1`,`TRANSACTION_TYPE`,`TWO_FA_STATUS`,`IP_CITY`,`OBDX_TRANSACTION_NAME`,`BENEFICIARY_TYPE1`,`USER_ID`,`DEBIT_ACCOUNT_NUMBER`,`BALANCE_BEFORE_TRANSACTION`,`SYS_TIME`,`CUST_ID`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`BENF_ADD_DATE`,`END_DATE`,`RISK_BAND`,`PAYEE_COMB`,`SESSION_ID`,`WHITELIST_FLAG`" +
              " FROM EVENT_FT_FUNDTRANSFER";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_FUNDTRANSFER (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TWO_FA_MODE\",\"BENEFICIARY_NAME\",\"CUSTOMER_SEGMENT\",\"IFSC_CODE\",\"PAYEE_ACCOUNTTYPE\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"BALANCE_AFTER_TRANSACTION\",\"TRAN_RECURRANCE\",\"START_DATE\",\"ACCOUNT_NUMBER\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"GROUP_PAYEE_ID\",\"DEVICE_ID\",\"FREQUENCY\",\"EXCHANGE_RATE\",\"BENF_PAYEE_ID\",\"ADDR_NETWORK\",\"PAYMENT_MODE\",\"TRANSFER_AMOUNT\",\"CURRENCY\",\"HOST_ID\",\"SETTLEMENT_AMOUNT\",\"BENEFICARY_UNIQUE_IDENTIFIER_1\",\"TRANSACTION_TYPE\",\"TWO_FA_STATUS\",\"IP_CITY\",\"OBDX_TRANSACTION_NAME\",\"BENEFICIARY_TYPE1\",\"USER_ID\",\"DEBIT_ACCOUNT_NUMBER\",\"BALANCE_BEFORE_TRANSACTION\",\"SYS_TIME\",\"CUST_ID\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"BENF_ADD_DATE\",\"END_DATE\",\"RISK_BAND\",\"PAYEE_COMB\",\"SESSION_ID\",\"WHITELIST_FLAG\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TWO_FA_MODE],[BENEFICIARY_NAME],[CUSTOMER_SEGMENT],[IFSC_CODE],[PAYEE_ACCOUNTTYPE],[ERROR_DESC],[SUCC_FAIL_FLG],[BALANCE_AFTER_TRANSACTION],[TRAN_RECURRANCE],[START_DATE],[ACCOUNT_NUMBER],[CUST_SEGMENT],[IP_COUNTRY],[GROUP_PAYEE_ID],[DEVICE_ID],[FREQUENCY],[EXCHANGE_RATE],[BENF_PAYEE_ID],[ADDR_NETWORK],[PAYMENT_MODE],[TRANSFER_AMOUNT],[CURRENCY],[HOST_ID],[SETTLEMENT_AMOUNT],[BENEFICARY_UNIQUE_IDENTIFIER_1],[TRANSACTION_TYPE],[TWO_FA_STATUS],[IP_CITY],[OBDX_TRANSACTION_NAME],[BENEFICIARY_TYPE1],[USER_ID],[DEBIT_ACCOUNT_NUMBER],[BALANCE_BEFORE_TRANSACTION],[SYS_TIME],[CUST_ID],[ERROR_CODE],[OBDX_MODULE_NAME],[BENF_ADD_DATE],[END_DATE],[RISK_BAND],[PAYEE_COMB],[SESSION_ID],[WHITELIST_FLAG]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TWO_FA_MODE`,`BENEFICIARY_NAME`,`CUSTOMER_SEGMENT`,`IFSC_CODE`,`PAYEE_ACCOUNTTYPE`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`BALANCE_AFTER_TRANSACTION`,`TRAN_RECURRANCE`,`START_DATE`,`ACCOUNT_NUMBER`,`CUST_SEGMENT`,`IP_COUNTRY`,`GROUP_PAYEE_ID`,`DEVICE_ID`,`FREQUENCY`,`EXCHANGE_RATE`,`BENF_PAYEE_ID`,`ADDR_NETWORK`,`PAYMENT_MODE`,`TRANSFER_AMOUNT`,`CURRENCY`,`HOST_ID`,`SETTLEMENT_AMOUNT`,`BENEFICARY_UNIQUE_IDENTIFIER_1`,`TRANSACTION_TYPE`,`TWO_FA_STATUS`,`IP_CITY`,`OBDX_TRANSACTION_NAME`,`BENEFICIARY_TYPE1`,`USER_ID`,`DEBIT_ACCOUNT_NUMBER`,`BALANCE_BEFORE_TRANSACTION`,`SYS_TIME`,`CUST_ID`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`BENF_ADD_DATE`,`END_DATE`,`RISK_BAND`,`PAYEE_COMB`,`SESSION_ID`,`WHITELIST_FLAG`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

