package clari5.custom.inbox.processors.tat;

public class Cl5IbxRetry {
    private String retryID;
    private String itemId;
    private String retryCount;
    private String emailFlag;
    private String emailId;
    private String emailSubject;
    private String emailBody;

    public Cl5IbxRetry() {
    }

    public String getRetryID() {
        return retryID;
    }

    public void setRetryID(String retryID) {
        this.retryID = retryID;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(String retryCount) {
        this.retryCount = retryCount;
    }

    public String getEmailFlag() {
        return emailFlag;
    }

    public void setEmailFlag(String emailFlag) {
        this.emailFlag = emailFlag;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    @Override
    public String toString() {
        return "Cl5IbxRetry{" +
                "retryID='" + retryID + '\'' +
                ", itemId='" + itemId + '\'' +
                ", retryCount='" + retryCount + '\'' +
                ", emailFlag='" + emailFlag + '\'' +
                ", emailId='" + emailId + '\'' +
                ", emailSubject='" + emailSubject + '\'' +
                ", emailBody='" + emailBody + '\'' +
                '}';
    }
}