// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_NPCICHEQUEDETAILS", Schema="rice")
public class NFT_NPCIChequeDetailsEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=11) public Double amount;
       @Field(size=200) public String newlyOpenedAccount;
       @Field(size=200) public String tcT;
       @Field(size=200) public String accountId;
       @Field(size=200) public String micrNoT;
       @Field(size=200) public String chequeNum;
       @Field(size=200) public String sanT;
       @Field public java.sql.Timestamp sysTime;
       @Field public java.sql.Timestamp eventts;
       @Field(size=200) public String inOutFlag;
       @Field(size=200) public String hostId;
       @Field(size=200) public String chequeNumberT;


    @JsonIgnore
    public ITable<NFT_NPCIChequeDetailsEvent> t = AEF.getITable(this);

	public NFT_NPCIChequeDetailsEvent(){}

    public NFT_NPCIChequeDetailsEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("NPCIChequeDetails");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAmount(EventHelper.toDouble(json.getString("Amount")));
            setNewlyOpenedAccount(json.getString("Newly_Opened_Account"));
            setTcT(json.getString("TC_t"));
            setAccountId(json.getString("Account_Id"));
            setMicrNoT(json.getString("micr_no_t"));
            setChequeNum(json.getString("Cheque_Num"));
            setSanT(json.getString("SAN_t"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setInOutFlag(json.getString("in_out_flag"));
            setHostId(json.getString("host_id"));
            setChequeNumberT(json.getString("Cheque_Number_t"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NP"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public Double getAmount(){ return amount; }

    public String getNewlyOpenedAccount(){ return newlyOpenedAccount; }

    public String getTcT(){ return tcT; }

    public String getAccountId(){ return accountId; }

    public String getMicrNoT(){ return micrNoT; }

    public String getChequeNum(){ return chequeNum; }

    public String getSanT(){ return sanT; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getInOutFlag(){ return inOutFlag; }

    public String getHostId(){ return hostId; }

    public String getChequeNumberT(){ return chequeNumberT; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAmount(Double val){ this.amount = val; }
    public void setNewlyOpenedAccount(String val){ this.newlyOpenedAccount = val; }
    public void setTcT(String val){ this.tcT = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setMicrNoT(String val){ this.micrNoT = val; }
    public void setChequeNum(String val){ this.chequeNum = val; }
    public void setSanT(String val){ this.sanT = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setInOutFlag(String val){ this.inOutFlag = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setChequeNumberT(String val){ this.chequeNumberT = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getNewly_Opened_Account(){ return newlyOpenedAccount; }
    @JsonIgnore
    public String getTC_t(){ return tcT; }
    @JsonIgnore
    public String getAccount_Id(){ return accountId; }
    @JsonIgnore
    public String getMicr_no_t(){ return micrNoT; }
    @JsonIgnore
    public String getCheque_Num(){ return chequeNum; }
    @JsonIgnore
    public String getSAN_t(){ return sanT; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getIn_out_flag(){ return inOutFlag; }
    @JsonIgnore
    public String getHost_id(){ return hostId; }
    @JsonIgnore
    public String getCheque_Number_t(){ return chequeNumberT; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_NPCIChequeDetailsEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_NPCIChequeDetails");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "NPCIChequeDetails");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}