package clari5.custom.jasper;


import java.util.ArrayList;
import java.util.List;

public class Matches {
    List<String> matchResult = new ArrayList<>();
    List<String> matchType = new ArrayList<>();

    public List<String> getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(List<String> matchResult) {
        this.matchResult = matchResult;
    }

    public List<String> getMatchType() {
        return matchType;
    }

    public void setMatchType(List<String> matchType) {
        this.matchType = matchType;
    }

    @Override
    public String toString() {
        return "Matches{" +
                "matchResult=" + matchResult.toString() +
                ", matchType=" + matchType.toString() +
                '}';
    }

}
