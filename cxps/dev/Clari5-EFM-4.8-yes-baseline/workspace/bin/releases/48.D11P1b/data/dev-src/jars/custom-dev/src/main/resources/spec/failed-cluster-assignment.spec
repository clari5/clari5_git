clari5.custom.yes.db {
        entity {
                failed-cluster-assignment {
                     attributes = [
                                { name :id,           type : "string:100", key=true}
                                { name :issue-key,    type : "string:100" ,key=false}
                                { name :cust-id,      type : "string:100" ,key=false}
                                { name :account-id,   type : "string:100" ,key=false}
                                { name :fact-name,    type : "string:100" ,key=false}
                                { name :assigned-segment,      type : "string:100" ,key=false}
                                { name :assigned-level,        type : "string:100" ,key=false}
                                { name :assigned-group,        type : "string:100" ,key=false}
                                { name :branch-id,        type : "string:100" ,key=false}
                                { name :remark,        type : "string:100" ,key=false}
                                { name :failed-on,    type : date}
                      ]
                     

                }
        }

 }
