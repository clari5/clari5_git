package clari5.custom.db;

import clari5.custom.yes.db.CustomerMaster;
import clari5.custom.yes.db.mappers.CustomerMasterMapper;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import cxps.apex.utils.CxpsLogger;
import org.apache.ibatis.session.RowBounds;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

public class CustMaster extends DBConnection implements IDBOperation
{
    private static final CxpsLogger logger = CxpsLogger.getLogger(CustMaster.class);

    public CustMaster() {
        rdbmsConnection = new RDBMSConnection();
    }

    @Override
    public Object getAllRecords() {
        RDBMSSession rdbmsSession = getSession();


        try {
            CustomerMasterMapper queue = rdbmsSession.getMapper(CustomerMasterMapper.class);
            RowBounds rowBounds = new RowBounds(0, 10);
            List<CustomerMaster> customerMaster = queue.getAllRecords(rowBounds);

            logger.info("CUSTOMER MASTER table record " + customerMaster.toString());
            return customerMaster;

        } catch (Exception e) {
            logger.error("Failed to fetch data from CUSTOMER MASTERtable" + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;

    }

    @Override
    public CustomerMaster select(Object o) {
        CustomerMaster customerMaster =new CustomerMaster(getSession());

        String cust_id = (String) o;
        try {

            customerMaster.setCustomerId(cust_id);
            return customerMaster.select();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to fetch CUSTOMER_MASTER table data of [" + cust_id + "] \n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;

    }

    @Override
    public int insert(Object o) {
        RDBMSSession session = getSession();
        int count = 0;
        CustomerMaster customerMaster = (CustomerMaster) o;

        try {
            customerMaster.setLastModifiedTime(Timestamp.from(Instant.now()));

            count = customerMaster.insert(session);
            session.commit();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to insert into CUSTOMER MASTER " + customerMaster.toString() + "\n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }


        return count;
    }

    @Override
    public int update(Object o) {
        RDBMSSession session = getSession();
        int count =0;

        CustomerMaster customerMaster = (CustomerMaster)o;
        try {

            customerMaster.setLastModifiedTime(Timestamp.from(Instant.now()));
            count = customerMaster.update(session);
            session.commit();

        }catch (RdbmsException rdbms){
            logger.error("Failed to update CUSTOMER MASTER"+customerMaster.toString());
            logger.error("Message "+rdbms.getMessage() +" Cause "+rdbms.getCause());
        }finally {
            closeRDMSSession();
        }
        return count;
    }
    @Override
    public int delete(Object o) {
        return 0;
    }


}
