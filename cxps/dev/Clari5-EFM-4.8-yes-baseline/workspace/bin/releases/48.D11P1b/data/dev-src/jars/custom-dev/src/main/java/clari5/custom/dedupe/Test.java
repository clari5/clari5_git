package clari5.custom.dedupe;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import clari5.tools.util.Hocon;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Test {
    static Hocon newModifiedCustomer;

    static {
        newModifiedCustomer = new Hocon();
        newModifiedCustomer.loadFromContext("new_modified_customers.conf");
    }

    public static void main(String[] args) throws Exception {
        /*String date1 = "2018-02-02 00:00:00.0";
        String start_dt = "2011-01-01";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");
        Date date = (Date)formatter.parse(date1);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
        String finalString = newFormat.format(date);
        System.out.println(finalString);*/
        /*newModifiedCustomer.getStringList("query.NAME_AND_MAILING_ADDR").forEach(i -> System.out.println(i));*/
        /*String mobile = "9999938748";
        System.out.println(mobile.substring(2));
        LinkedHashMap<String, LinkedList<String>> map = new LinkedHashMap<>();
        LinkedList<String> list = new LinkedList<>();
        LinkedList<String> list1 = new LinkedList<>();

        list.add("abc");
        list.add("cdv");
        list.add("frt");
        map.put("cxde3234", list);
        list1.add("mju");
        list1.add("nhj");
        list1.add("poi");
        map.put("cxde3236", list1);
        System.out.println(map);
        if (map.containsKey("cxde3234")) {
            String keySet = map.keySet().toString();
             Object value = map.values().toArray()[0];
             List<String> ll = Arrays.asList(value.toString().replaceAll("\\[", "")
                     .replaceAll("\\]", "")
                     .replaceAll(" ", "")
                     .split(","));
            System.out.println(ll.get(0) + " ----> " + ll.get(1) + " ---> "+ll.get(2));
        }
         */

        /*

        String[] arr = {"12711851", "12711867", "12712547", "12711906"};
//        String[] arr = {"12711851"};

        StringBuffer sb = new StringBuffer("('");

        for (int i = 0; i < arr.length; i++) {
            sb = sb.append(arr[i]).append("','");
        }
        sb.delete(sb.length() - 3, sb.length() - 1).append(")");
        System.out.println(sb.toString());
        updateModifiedCstomer(sb.toString());
        *
         */

    }


    private static void updateModifiedCstomer(String custId) throws Exception {

        PreparedStatement ps = null;
        String query = "update NEW_MODIFIED_CUSTOMERS set STATUS = ? where CUST_ID in ?";
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@192.168.5.70:1521:db12c", "cxpsadm_lakshmisai_48yes", "C_xps123");

        System.out.println("database connected");
        try {
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(query);
            ps.setString(1, "F");
            ps.setString(2, custId);
            ps.executeLargeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    private static Connection getConnection() throws Exception {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@192.168.5.70:1521:db12c", "cxpsadm_lakshmisai_48yes", "C_xps123");
        return connection;
    }

}