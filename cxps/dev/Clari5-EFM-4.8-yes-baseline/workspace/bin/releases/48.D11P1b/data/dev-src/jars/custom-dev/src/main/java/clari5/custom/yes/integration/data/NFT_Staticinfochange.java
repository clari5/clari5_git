package clari5.custom.yes.integration.data;

public class NFT_Staticinfochange extends ITableData {
    private String tableName = "NFT_STATICINFO";
    private String event_type = "NFT_Staticinfochange";
    private String EVENT_ID;
    private String initsubentityval;
    private String tran_date;
    private String finalsubentityval;
    private String acct_id;
    private String branch_id;
    private String entitytype;

    public String getEntitytype() {
        return entitytype;
    }

    public void setEntitytype(String entitytype) {
        this.entitytype = entitytype;
    }

    private String channel;
    private String sys_time;
    private String cust_id;
    private String host_id;
    private String eventts;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getInitsubentityval() {
        return initsubentityval;
    }

    public void setInitsubentityval(String initsubentityval) {
        this.initsubentityval = initsubentityval;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getFinalsubentityval() {
        return finalsubentityval;
    }

    public void setFinalsubentityval(String finalsubentityval) {
        this.finalsubentityval = finalsubentityval;
    }

    public String getAcct_id() {
        return acct_id;
    }

    public void setAcct_id(String acct_id) {
        this.acct_id = acct_id;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

 //   public String getEntitytype() {
 //       return Entitytype;
 //   }

  //  public void setEntitytype(String entitytype) {
  //      Entitytype = Entitytype;
  //  }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }
}
