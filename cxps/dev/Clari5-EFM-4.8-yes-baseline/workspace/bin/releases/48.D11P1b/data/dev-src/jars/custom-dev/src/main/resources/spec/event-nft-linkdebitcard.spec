cxps.events.event.nft-linkdebitcard{
  table-name : EVENT_NFT_LINKDEBITCARD
  event-mnemonic: LDC
  workspaces : {
    CUSTOMER: cust_id
  }
  event-attributes : {
        host-id: {db:true ,raw_name :host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        user-id: {db : true ,raw_name : user_id ,type : "string:200"}
        device-id: {db : true ,raw_name : device_id ,type : "string:200"}
        addr-network: {db :true ,raw_name : addr_network ,type : "string:200"}
        ip-country: {db :true ,raw_name : ip_country ,type : "string:200"}
        ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
        succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:10"}
        error-code: {db : true ,raw_name : error_code ,type : "string:200"}
        error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
        two-fa-status: {db : true ,raw_name : 2fa_status ,type: "string:20" }
        two-fa-mode: {db : true ,raw_name : 2fa_mode ,type: "string:20" }
        obdx-module-name: {db : true ,raw_name : obdx_module_name ,type: "string:200" }
        obdx-transaction-name: {db : true ,raw_name : obdx_transaction_name ,type: "string:200" }
        card-number: {db : true ,raw_name : card_number ,type: "string:20"}
        primary-account-number: {db : true ,raw_name : primary_account_number ,type: "string:50"}
        secondary-account-numbers: {db : true ,raw_name : secondary_account_numbers ,type: "string:50"}
        cust-segment: {db : true ,raw_name :cust_segment ,type : "string:200"}
        session-id:{db : true ,raw_name :session_id ,type : "string:200"}
}
}
