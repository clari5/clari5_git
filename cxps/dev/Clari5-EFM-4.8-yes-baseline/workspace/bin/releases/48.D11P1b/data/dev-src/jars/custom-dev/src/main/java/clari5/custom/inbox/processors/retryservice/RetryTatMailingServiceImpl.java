package clari5.custom.inbox.processors.retryservice;

import clari5.custom.inbox.processors.db.CustomIboxData;
import clari5.custom.inbox.processors.tat.Cl5IbxRetry;
import clari5.custom.services.KTKMailingServices;
import clari5.custom.services.MailFields;
import clari5.custom.services.MailServices;
import clari5.custom.services.SMTPMail;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RetryTatMailingServiceImpl implements RetryTatMailingService {
    private static final CxpsLogger logger = CxpsLogger.getLogger(RetryTatMailingServiceImpl.class);
    static Hocon smtp;

    static {
        smtp = new Hocon();
        smtp.loadFromContext("smtp.conf");
    }

    @Override
    public synchronized void sendMail(ConcurrentLinkedQueue<Cl5IbxRetry> items) {
       // System.out.println("inside sendmail of retry daemon");
        int res = 0;
        while (!items.isEmpty()) {
            Cl5IbxRetry retry = items.poll();
            MailServices mailServices = new KTKMailingServices(new SMTPMail(), smtp);
            MailFields mailFields = mailServices.getMailFields();
            mailFields.setTo(retry.getEmailId());
            mailFields.setSubject(retry.getEmailSubject());
            mailFields.setMessage(retry.getEmailBody());

            //before sending mretry ail
            logger.debug("before sending mail: " + mailFields);
            //System.out.println("before sending mail rety: " + mailFields);

            //sending mail
            res = mailServices.sendIndvMail(mailFields);
            if (res == 1) {
                retry.setEmailFlag("C");
                int count = Integer.parseInt(retry.getRetryCount());
				count++;
                retry.setRetryCount(String.valueOf(count));
                CustomIboxData.updateCl5IbxItemData(retry);
            } else {
                int count = Integer.parseInt(retry.getRetryCount());
				count++;
                retry.setRetryCount(String.valueOf(count));
                CustomIboxData.updateCl5IbxItemData(retry);
            }
        }
    }

    @Override
    public void finalize() {
        logger.debug("finalize method called upon gc to avoid memory leaks");
    }
}
