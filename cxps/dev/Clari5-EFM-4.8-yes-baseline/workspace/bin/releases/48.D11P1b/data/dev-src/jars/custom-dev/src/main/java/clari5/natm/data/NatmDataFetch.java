package clari5.natm.data;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import org.apache.ibatis.mapping.ResultSetType;

import java.sql.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import java.util.concurrent.ConcurrentLinkedQueue;

/*****
 *    @author : Suryakant
 *    @since : 05/02/2020
 */

public class NatmDataFetch {

    private static final CxpsLogger logger = CxpsLogger.getLogger(NatmDataFetch.class);
    static Hocon natmFetch;
    static String natmDataFetchQuery;
    static String product_code;
    static String tran_numonic_code;
    static String accntOpenDate;
    static String whiteList;
    static int numOfDays;
    static String acctTransaction;
    static String productType;
    static String updateSyncStatus;
    static String batchTrackerDataFetch;
    static String updateCumAmt;
    static String updateBatchDate;
    static String updateBatchId;
    static String updateEventList;
    static String natmInProgressDataFetch;
    static String countNewRecords;
    static String countIRecords;



    static {
        natmFetch = new Hocon();
        natmFetch.loadFromContext("natmQueryFilter.conf");
        product_code= natmFetch.getString("product_code");
        tran_numonic_code = natmFetch.getString("tran_mnemonic");
        natmDataFetchQuery = natmFetch.getString("natmDataFetch");
        natmInProgressDataFetch=natmFetch.getString("natmInProgressDataFetch"); // query for in progress data
        accntOpenDate = natmFetch.getString("accntOpenDate");
        whiteList = natmFetch.getString("whiteListFlag");
        numOfDays = natmFetch.getInt("numOfDays");
        acctTransaction = natmFetch.getString("accountTransaction");
        productType = natmFetch.getString("productType");
        updateSyncStatus = natmFetch.getString("updateNatmSync");
        batchTrackerDataFetch = natmFetch.getString("batchTrackerFetch");
        updateCumAmt = natmFetch.getString("updateCumAmount");
        updateBatchDate = natmFetch.getString("updateBatchDate");
        updateBatchId = natmFetch.getString("updateBatchId");
        updateEventList = natmFetch.getString("updateAccountEventList");
        countNewRecords=natmFetch.getString("countNewRecords");
        countIRecords=natmFetch.getString("countIRecords");

    }

    public static ConcurrentLinkedQueue<HashMap> getNatmData() {

        logger.info("[NatmDataFetch: getNatmData(): ]Inside the method where taking data from Batch processor Table");
        ConcurrentLinkedQueue<HashMap> queue = new ConcurrentLinkedQueue<>();
        String flag = "N";

        try(RDBMSSession session=Rdbms.getAppSession();
            Connection connection=session.getCxConnection();
            PreparedStatement statement=connection.prepareStatement(natmDataFetchQuery);
            PreparedStatement batchDetailFetch = connection.prepareStatement(batchTrackerDataFetch);
            PreparedStatement inProgressStatement=connection.prepareStatement(natmInProgressDataFetch);
            PreparedStatement updateStatement=connection.prepareStatement(updateSyncStatus);
            PreparedStatement updateId = connection.prepareStatement(updateBatchId);
            PreparedStatement newRecords=connection.prepareStatement(countNewRecords);
            PreparedStatement iRecords=connection.prepareStatement(countIRecords);) {
            /**
             * 1. SELECT BATCHTIME, BATCHID FROM BATCH_TRACKER"
             */
            ResultSet batchdetail=batchDetailFetch.executeQuery();
            java.sql.Date sysDate = null;
            int batchId=0;
            int count=0;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            while (batchdetail.next()){
                batchId = batchdetail.getInt("BATCHID");
                sysDate = batchdetail.getDate("BATCHTIME");
            }
            logger.info("[NatmDataFetch] getNatmData() Fetched batch Id"+batchId);
            statement.setInt(1,batchId);
            ResultSet countOfRecords=iRecords.executeQuery();
            while(countOfRecords.next()){
                count=countOfRecords.getInt(1);
            }
            if(count!=0){
                logger.info("[NatmDataFetch] Total Records in I"+count);
            }
            //inProgressStatement.setInt(1,batchId);
            // check for I first
            ResultSet resultSet=inProgressStatement.executeQuery();
            while(resultSet.next()){
                HashMap map=formJsonObject(resultSet,connection,sdf,sysDate,cal1,cal2,batchId,flag);
                queue.add(map);
            }
            resultSet=statement.executeQuery();
            String eventId=null;
            countOfRecords=newRecords.executeQuery();
            while(countOfRecords.next()){
                count=countOfRecords.getInt(1);
            }
            if(count!=0){
                logger.info("[NatmDataFetch] Total Records in NEW"+count);
            }
            while(resultSet.next()) {
                try {
                    HashMap jsonObject = formJsonObject(resultSet, connection, sdf, sysDate, cal1, cal2, batchId, flag);
                    eventId=jsonObject.get("EVENT_ID").toString();
                    if (jsonObject.size() > 0 || jsonObject != null) {
                        queue.add(jsonObject);
                        updateStatement.setString(1, "I");
                        updateStatement.setString(2, jsonObject.get("EVENT_ID").toString());
                        updateStatement.executeUpdate();
                        connection.commit();
                        flag = "Y";
                    }
                }catch (Exception e){
                    logger.error("[NatmDataFetch:getNatmData()] Natm Criteria didn't matched! Hence returning null"+eventId);
                }

            }
            if(flag=="Y"){
                updateId.setLong(1,batchId+1);
                updateId.executeUpdate();
                connection.commit();
                logger.info("[NatmDataFetch] Completed Processing Batch! New batch id"+batchId);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return queue;
    }

    private static HashMap formJsonObject(ResultSet resultSet, Connection connection, SimpleDateFormat sdf, Date sysDate, Calendar cal1, Calendar cal2, int batchId, String flag) {

        HashMap map = null;
        try(PreparedStatement updateId = connection.prepareStatement(updateBatchId);
            PreparedStatement cumAmtReset = connection.prepareStatement(updateCumAmt);
            PreparedStatement updateDate = connection.prepareStatement(updateBatchDate);
            PreparedStatement updateEvent = connection.prepareStatement(updateEventList);
            PreparedStatement updateStatement=connection.prepareStatement(updateSyncStatus);) {
            //while(resultSet.next()) {
                flag="Y";
                Timestamp sysTime = resultSet.getTimestamp("SYS_TIME");
                Date checkDate = new Date(sysTime.getTime());
                try {
                    String syncStatus=resultSet.getString("NATM_SYNC_STATUS");
                    if(syncStatus.equals("NEW")){
                        updateStatement.setString(1,"I");
                        String eventId=resultSet.getString("EVENT_ID");
                        updateStatement.setString(2,eventId);
                        updateStatement.executeUpdate();
                        connection.commit();
                    }
                    java.util.Date batchDate = sdf.parse(sysDate.toString());
                    java.util.Date systemDate = sdf.parse(checkDate.toString());

                    logger.info("[NatmDataFetch:getNatmData() ]Batch Date is: " + sdf.format(batchDate) + "sys Date is" + sdf.format(systemDate));
                    cal1.setTime(batchDate);
                    cal2.setTime(systemDate);
                }
                catch (ParseException e) {
                    logger.error("[NatmDataFetch:getNatmData() ]Unable to Convert Date BATCH_TIME" + sysDate);
                    e.printStackTrace();
                }
                    /**
                     * 3. if the batch date and sys date is different
                     */
                if (!cal1.equals(cal2)) {
                        /**
                         *update the ACCOUNT_TRANSACTION to 0
                         * and commit
                         */
                        cumAmtReset.executeUpdate();
                        connection.commit();
                        /**
                         * 4. UPDATE ACCOUNT_EVENTLIST SET EVENT_LIST=?,CUM_TRAN_AMT=? WHERE ACCT_ID=?
                         * and perform the update operation with commit
                         */
                        updateEvent.setString(1, "EMPTY");
                        updateEvent.setDouble(2, 0);
                        updateEvent.setString(3, resultSet.getString("ACCT_ID"));
                        updateEvent.executeUpdate();
                        connection.commit();
                        /**
                         * 5. UPDATE BATCH_TRACKER SET BATCHTIME=? WHERE BATCHID=?"
                         * perform update operation with commit
                         */
                        updateDate.setDate(1, checkDate);
                        updateDate.setInt(2, batchId);
                        updateDate.executeUpdate();
                        connection.commit();
                }
                String accountId = resultSet.getString("ACCT_ID");
                String drcr = resultSet.getString("DRCR");
                String tranNumonicCode = resultSet.getString("TRAN_NUMONIC_CODE");
                String productCode = resultSet.getString("PRODUCT_CODE");
                String eventId = resultSet.getString("EVENT_ID");

                    //checking exclusion criteria and drcr
                if (natmExclusionCriteria(drcr, productCode, tranNumonicCode).equals("Y")) {

                        if (checkAccountQueryFilter(accountId, sysTime).equals("Y")) {
                            if (checkProductType(productCode).equals("Y")) {
                                map=new HashMap();
                                //fetching Data from Batch processor table and adding in JSONObject
                                map.put("SYSTEM", resultSet.getString("SYSTEM"));
                                map.put("CUST_ID", resultSet.getString("CUST_ID"));
                                map.put("CA_SCHEME_CODE", resultSet.getString("CA_SCHEME_CODE"));
                                map.put("BEN_CUST_ID", resultSet.getString("BEN_CUST_ID"));
                                map.put("EFF_AVAILABLE_BALANCE", resultSet.getDouble("EFF_AVAILABLE_BALANCE"));
                                map.put("USER_ID", resultSet.getString("USER_ID"));
                                map.put("TRAN_CRNCY_CODE", resultSet.getString("TRAN_CRNCY_CODE"));
                                map.put("PRODUCT_CODE", resultSet.getString("PRODUCT_CODE"));
                                map.put("SYS_TIME", resultSet.getTimestamp("SYS_TIME"));
                                map.put("TOD_GRANT_AMOUNT", resultSet.getDouble("TOD_GRANT_AMOUNT"));
                                map.put("TRAN_AMT", resultSet.getDouble("TRAN_AMT"));
                                map.put("USD_EQV_AMT", resultSet.getDouble("USD_EQV_AMT"));
                                map.put("STATUS", resultSet.getString("STATUS"));
                                map.put("PURPOSE_CODE", resultSet.getString("PURPOSE_CODE"));
                                map.put("REM_ADD1", resultSet.getString("REM_ADD1"));
                                map.put("BEN_BIC", resultSet.getString("BEN_BIC"));
                                map.put("REM_ADD2", resultSet.getString("REM_ADD2"));
                                map.put("FCC_PRODUCT_CODE", resultSet.getString("FCC_PRODUCT_CODE"));
                                map.put("REM_ADD3", resultSet.getString("REM_ADD3"));
                                map.put("TRAN_ID", resultSet.getString("TRAN_ID"));
                                map.put("INSTRUMENT_TYPE", resultSet.getString("INSTRUMENT_TYPE"));
                                map.put("INR_AMOUNT", resultSet.getString("INR_AMOUNT"));
                                map.put("CLIENT_ACC_NO", resultSet.getString("CLIENT_ACC_NO"));
                                map.put("CHANNEL_ID", resultSet.getString("CHANNEL_ID"));
                                map.put("TRAN_AMOUNT", resultSet.getDouble("TRAN_AMOUNT"));
                                map.put("PURPOSE_DESC", resultSet.getString("PURPOSE_DESC"));
                                map.put("BEN_ADD3", resultSet.getString("BEN_ADD3"));
                                map.put("REM_TYPE", resultSet.getString("REM_TYPE"));
                                map.put("BEN_ADD1", resultSet.getString("BEN_ADD1"));
                                map.put("BEN_ADD2", resultSet.getString("BEN_ADD2"));
                                map.put("VALUE_DATE", resultSet.getTimestamp("VALUE_DATE"));
                                map.put("ATM_ID", resultSet.getString("ATM_ID"));
                                map.put("BEN_CITY", resultSet.getString("BEN_CITY"));
                                map.put("TRAN_NUMONIC_CODE", resultSet.getString("TRAN_NUMONIC_CODE"));
                                map.put("ISIN_NUMBER", resultSet.getString("ISIN_NUMBER"));
                                map.put("TRAN_CURR", resultSet.getString("TRAN_CURR"));
                                map.put("EVENT_ID", resultSet.getString("EVENT_ID"));
                                map.put("REF_TRAN_CRNCY", resultSet.getDouble("REF_TRAN_CRNCY"));
                                map.put("BEN_CNTRY_CODE", resultSet.getString("BEN_CNTRY_CODE"));
                                map.put("TRAN_DATE", resultSet.getTimestamp("TRAN_DATE"));
                                map.put("COD_MSG_TYPE", resultSet.getDouble("COD_MSG_TYPE"));
                                map.put("REM_NAME", resultSet.getString("REM_NAME"));
                                map.put("SERVER_ID", resultSet.getDouble("SERVER_ID"));
                                map.put("TD_MATURITY_DATE", resultSet.getTimestamp("TD_MATURITY_DATE"));
                                map.put("BEN_NAME", resultSet.getString("BEN_NAME"));
                                map.put("INSTRUMENT_NUMBER", resultSet.getString("INSTRUMENT_NUMBER"));
                                map.put("REFERENCE_SRL_NUM", resultSet.getString("REFERENCE_SRL_NUM"));
                                map.put("SYNC_STATUS", resultSet.getString("SYNC_STATUS"));
                                map.put("PRODUCT_DESC", resultSet.getString("PRODUCT_DESC"));
                                map.put("BEN_ACCT_NO", resultSet.getString("BEN_ACCT_NO"));
                                map.put("HOST_ID", resultSet.getString("HOST_ID"));
                                map.put("TRAN_PARTICULAR", resultSet.getString("TRAN_PARTICULAR"));
                                map.put("BANK_CODE", resultSet.getString("BANK_CODE"));
                                map.put("DRCR", resultSet.getString("DRCR"));
                                map.put("INSERTION_TIME", resultSet.getTimestamp("INSERTION_TIME"));
                                map.put("BRANCH", resultSet.getString("BRANCH"));
                                map.put("ACCT_ID", resultSet.getString("ACCT_ID"));
                                map.put("PSTD_DATE", resultSet.getTimestamp("PSTD_DATE"));
                                map.put("REF_TRAN_AMT", resultSet.getDouble("REF_TRAN_AMT"));
                                map.put("CPTY_AC_NO", resultSet.getString("CPTY_AC_NO"));
                                map.put("HOST_TYPE", resultSet.getString("HOST_TYPE"));
                                map.put("TRN_DATE", resultSet.getTimestamp("TRN_DATE"));
                                map.put("INST_STATUS", resultSet.getString("INST_STATUS"));
                                map.put("PSTD_FLG", resultSet.getString("PSTD_FLG"));
                                map.put("TD_LIQUIDATION_TYPE", resultSet.getString("TD_LIQUIDATION_TYPE"));
                                map.put("REM_BIC", resultSet.getString("REM_BIC"));
                                map.put("ONLINE_BATCH", resultSet.getString("ONLINE_BATCH"));
                                map.put("REM_CNTRY_CODE", resultSet.getString("REM_CNTRY_CODE"));
                                map.put("UCIC_ID", resultSet.getString("UCIC_ID"));
                                map.put("REM_CUST_ID", resultSet.getString("REM_CUST_ID"));
                                map.put("REM_ACCT_NO", resultSet.getString("REM_ACCT_NO"));
                                map.put("REM_CITY", resultSet.getString("REM_CITY"));
                                map.put("BRANCH_ID", resultSet.getString("BRANCH_ID"));

                                //return  jsonObject;
                                //return jsonObject;

                            }// end of inner most if
                            else{
                                updateStatement.setString(1, "NOT NATM");
                                updateStatement.setString(2, eventId);
                                updateStatement.executeUpdate();
                                connection.commit();
                            }
                        }
                        else{
                            updateStatement.setString(1, "NOT NATM");
                            updateStatement.setString(2, eventId);
                            updateStatement.executeUpdate();
                            connection.commit();

                        }


                    }// end of outermost if
                    else{
                        updateStatement.setString(1, "NOT NATM");
                        updateStatement.setString(2, eventId);
                        updateStatement.executeUpdate();
                        connection.commit();

                    }

            //}// end of while

        }catch (SQLException e) {
            logger.error(e.getCause()+"Error At ResultSet Execution: Staus is New");
            e.printStackTrace();
        }
        return map;
    }

    public static String natmExclusionCriteria(String drcr, String productCode, String tranNumonicCode) {

        logger.info("Checking the exclusion criteria for specific account");

        String[] productCodeArray = product_code.split(",");
        String[] tranNumonicArray = tran_numonic_code.split(",");

        List productList = Arrays.asList(productCodeArray);
        List tranNumonicList = Arrays.asList(tranNumonicArray);

        if ((drcr.equalsIgnoreCase("C") && !productList.contains(productCode) && !tranNumonicList.contains(tranNumonicCode))) {
            return "Y";
        } else {
            return "N";
        }
    }

    public static String checkAccountQueryFilter(String acctId, Timestamp sysTime) {

        Date acccountOpenDate = new Date(1800 - 01 - 01);
        Calendar cal = Calendar.getInstance();
        Date curr_date = new Date(sysTime.getTime());
        cal.setTime(curr_date);
        cal.add(Calendar.DATE, -numOfDays);
        java.util.Date limitDate = cal.getTime();

        try (Connection connection = Rdbms.getAppConnection();
             PreparedStatement acctOpenDateStatement = connection.prepareStatement(accntOpenDate);
             //SELECT ACCOUNT_ID FROM WHITELIST_ACCT_DET where ACCOUNT_ID=?
             PreparedStatement whiteListStatement = connection.prepareStatement(whiteList);
             PreparedStatement acctTranStatement = connection.prepareStatement(acctTransaction);) {

            whiteListStatement.setString(1, acctId);
            ResultSet resultWhiteList = whiteListStatement.executeQuery();

            if (resultWhiteList.next() && resultWhiteList.getString("ACCOUNT_ID") != null) {
                logger.info("Account is whiteListed");
                return "N";

            } else {

                acctTranStatement.setString(1, acctId);
                ResultSet resultAcctTransaction = acctTranStatement.executeQuery();

                if (resultAcctTransaction.next() && resultAcctTransaction.getString("ACCOUNT_ID") != null) {
                    logger.info("Account is available in Account_transaction Table");
                    acctOpenDateStatement.setString(1, acctId);
                    ResultSet resultAcctOpenDate = acctOpenDateStatement.executeQuery();

                    if (resultAcctOpenDate.next() && resultAcctOpenDate.getDate("ACCCOUNT_OPEN_DATE") != null) {
                        logger.info("Account is available in Account Master table");
                        acccountOpenDate = resultAcctOpenDate.getDate("ACCCOUNT_OPEN_DATE");

                        if ((acccountOpenDate.after(limitDate) && acccountOpenDate.before(curr_date)) || (acccountOpenDate.equals(limitDate)) || (acccountOpenDate.equals(curr_date))) {
                            logger.info("accountOpenDate is valid ");
                            return "Y";

                        } else {
                            logger.info("Account open Date is not Valid");
                            return "N";
                        }

                    } else {
                        logger.info("Account is not available in Account master table ");
                        return "N";
                    }


                } else {
                    logger.info("Account is not available in Account transaction Table");
                    return "N";
                }
            }


        } catch (Exception e) {
            e.printStackTrace();

        }
        return "N";
    }

    public static String checkProductType(String productCode) {

        try (Connection connection = Rdbms.getAppConnection();
             PreparedStatement statement = connection.prepareStatement(productType);) {

            statement.setString(1, productCode);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next() && resultSet.getString("PRODUCT_TYPE") != null) {
                logger.info("Product Type is available in Product_Fcr table");
                String product_Type = resultSet.getString("PRODUCT_TYPE");

                if (!product_Type.equalsIgnoreCase("SAVING") && !product_Type.equalsIgnoreCase("CURRENT")) {
                    logger.info("Product_type is other than saving and current");
                    return "N";
                } else {

                    if (product_Type.equalsIgnoreCase("SAVING") || product_Type.equalsIgnoreCase("CURRENT")) {
                        logger.info("Product type is valid");
                        return "Y";
                    }
                }

            } else {
                logger.info("product Type is not available in product_fcr Table");
                return "N";
            }
            connection.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "N";
    }

}