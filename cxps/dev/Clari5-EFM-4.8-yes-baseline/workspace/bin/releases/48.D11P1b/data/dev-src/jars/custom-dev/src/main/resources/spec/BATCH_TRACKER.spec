clari5.custom.mapper {
        entity {
                BATCH_TRACKER {
                       generate = true
                        attributes:[
                                { name: BATCHTIME, type=timestamp },
                                { name: BATCHID ,type ="number:30" , key=true }
                                ]
                        }
        }
}
