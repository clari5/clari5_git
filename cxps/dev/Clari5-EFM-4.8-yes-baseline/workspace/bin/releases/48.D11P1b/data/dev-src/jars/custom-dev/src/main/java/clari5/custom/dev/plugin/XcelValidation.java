package clari5.custom.dev.plugin;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import org.apache.hadoop.hbase.protobuf.generated.CellProtos;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class XcelValidation {


    public static Map<Integer,String> columnTyp = new HashMap<>();
    static {
        columnTyp.put(-7,"BIT");
        columnTyp.put(-6,"TINYINT");
        columnTyp.put(-5,"BIGINT");
        columnTyp.put(-4,"LONGVARBINARY");
        columnTyp.put(-3,"VARBINARY");
        columnTyp.put(-2,"BINARY");
        columnTyp.put(-1,"LONGVARCHAR");
        columnTyp.put(0,"NULL");
        columnTyp.put(1,"CHAR");
        columnTyp.put(2,"NUMERIC");
        columnTyp.put(3,"DECIMAL");
        columnTyp.put(4,"INTEGER");
        columnTyp.put(5,"SMALLINT");
        columnTyp.put(6,"FLOAT");
        columnTyp.put(7,"REAL");
        columnTyp.put(8,"DOUBLE");
        columnTyp.put(12,"VARCHAR");
        columnTyp.put(91,"DATE");
        columnTyp.put(92,"TIME");
        columnTyp.put(93,"TIMESTAMP");
        columnTyp.put(1111,"OTHER");



    }
    //private static final String FILE_NAME = "/home/jabert/Music/Canara/field47mappingofeventsCanarannew.xlsx";
    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(XcelValidation.class);


    public boolean checkHeaderColumn() {
        return false;
    }

    public static boolean validate(String tablename, String FILE_NAME) throws SQLException {

        boolean checkColumnstatus = false;
        XSSFWorkbook my_xls_workbook = null;
        FileInputStream input_document = null;
        Set<String> xcelheaderlist = new TreeSet<>();
        Set<String> xceldatatyplist = new TreeSet<>();
        Map<String,String> map = null;


        try {

            map = getColumnasList(tablename);
            Set<String> list = new TreeSet<>(map.keySet());
            Set<String> datatypColmlist = new TreeSet<>(map.values());
           //only for debugging remove it once finalized
            Iterator iterator = datatypColmlist.iterator();
            System.out.println("\nUsing ListIterator:\n");
            while (iterator.hasNext()) {
                System.out.println("Value is : " + iterator.next());
            }

            input_document = new FileInputStream(new File(FILE_NAME));
            my_xls_workbook = new XSSFWorkbook(input_document);
            String fn = my_xls_workbook.getClass().toString();
            String f = my_xls_workbook.getSheetName(0);
            System.out.println("Sheet Name : " + f);
            System.out.println(fn);
            XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
            XSSFRow header_row = my_worksheet.getRow(0);
            XSSFRow first_row = my_worksheet.getRow(1);
            int noOfColumns = my_worksheet.getRow(0).getPhysicalNumberOfCells();
            System.out.println("the no of columns " + noOfColumns);
            System.out.println("List Size is : [ " + list.size() + " ]");
            if (noOfColumns == list.size()) {
                System.out.println("Input column and table column is Matched hence checking the Matching of columns");
                for (int i = 0; i < noOfColumns; i++) {
                    XSSFCell header_cell = header_row.getCell(i);
                    String header = header_cell.getStringCellValue();
                    System.out.println("header column" + i + "value is " + header);
                    xcelheaderlist.add(header);
                }
                for (int i = 0; i < noOfColumns; i++) {
                    XSSFCell firstrow_cell = first_row.getCell(i);
                    int cellType = firstrow_cell.getCellType();
                    System.out.println("Cell Type is for cell" + i +" is  : [ " +cellType+ " ]");
                    //xcelheaderlist.add(header);

                }
                Iterator<Cell> cellIterator = first_row.iterator();
                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            System.out.println("cell Type");
                            System.out.print("VARCHAR"+cell.getStringCellValue() + "\t");

                            boolean chkdate = Xcelread.isValidDate(cell.getStringCellValue());
                            if(chkdate) {
                                xceldatatyplist.add("TIMESTAMP");
                                break;

                            }
                            else {
                                xceldatatyplist.add("VARCHAR");
                                break;
                            }

                        case Cell.CELL_TYPE_NUMERIC:

                            if (DateUtil.isCellDateFormatted(cell)) {
                                String dateStr = String.valueOf(cell.getDateCellValue());
                                System.out.println("date : "+dateStr);
                                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                                Date date = (Date) formatter.parse(dateStr);
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(date);
                                String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" +  cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                                System.out.println("formatedDate : " + formatedDate);
                                if( cal.get(Calendar.HOUR) == 0 && cal.get(Calendar.MINUTE) == 0 && cal.get(Calendar.SECOND) == 0) {
                                    xceldatatyplist.add("DATE");
                                    break;
                                }
                                else {
                                    System.out.println("formatedDate : " + formatedDate);
                                    xceldatatyplist.add("TIMESTAMP");
                                    break;
                                }
                            }
                            else {
                                System.out.print("NUMERIC"+cell.getNumericCellValue() + "\t");
                                xceldatatyplist.add("NUMERIC");
                                break;
                            }

                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.print(cell.getBooleanCellValue() + "\t");
                            break;
                        default:


                    }
                    System.out.println();

                }
                Iterator iterator2 = xceldatatyplist.iterator();
                System.out.println("\nUsing xcel  ListIterator:\n");
                while (iterator2.hasNext()) {
                    System.out.println("Value xcel list is : " + iterator2.next());
                }
                if(list.equals(xcelheaderlist) && xceldatatyplist.equals(datatypColmlist)){
                    checkColumnstatus = true;
                }
                if (checkColumnstatus) {
                    //do DB insert
                    System.out.println("Coulmn Matched with xcel" + checkColumnstatus);
                    return true;
                } else {
                    System.out.println("Columns Matching Failed Kindly add  appropriate columns");
                    return false;
                }
            } else {
                System.out.println("Columns Matching Failed Kindly add  appropriate columns");
                return false;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static Map<String,String> getColumnasList(String tableName) {
        try {
            List<String> list = new ArrayList<String>();
            Map<String,String> colmnametyp = new HashMap<>();
            //int j= 0;
            String sql = "SELECT * FROM " + tableName;
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
                cxpsLogger.info("Getting table name to show on drop down");
                ResultSet rs = ps.executeQuery();
                ResultSetMetaData resultSetMetaData = rs.getMetaData();
                for (int i = 1 ; i <= resultSetMetaData.getColumnCount(); i++) {
                    colmnametyp.put(resultSetMetaData.getColumnName(i),columnTyp.get(resultSetMetaData.getColumnType(i)));
                    //list.add(resultSetMetaData.getColumnName(i));
                    //j++;
                }
                return colmnametyp;
            }
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- Not getting table Name");
        }
        return null;
    }


    public static boolean validateSubmit(String tablename, String FILE_NAME) throws SQLException {

        boolean checkColumnstatus = false;
        XSSFWorkbook my_xls_workbook = null;
        FileInputStream input_document = null;
        Set<String> xcelheaderlist = new TreeSet<>();
        Set<String> xceldatatyplist = new TreeSet<>();
        Map<String,String> map = null;


        try {

            map = getColumnasList(tablename);
            Set<String> list = new TreeSet<>(map.keySet());
            Set<String> datatypColmlist = new TreeSet<>(map.values());
            //only for debugging remove it once finalized
            Iterator iterator = datatypColmlist.iterator();
            System.out.println("\nUsing ListIterator:\n");
            while (iterator.hasNext()) {
                System.out.println("Value is : " + iterator.next());
            }
            input_document = new FileInputStream(new File(FILE_NAME));
            my_xls_workbook = new XSSFWorkbook(input_document);
            String fn = my_xls_workbook.getClass().toString();
            String f = my_xls_workbook.getSheetName(0);
            System.out.println("Sheet Name : " + f);
            System.out.println(fn);
            XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
            XSSFRow header_row = my_worksheet.getRow(0);
            XSSFRow first_row = my_worksheet.getRow(1);
            int noOfColumns = my_worksheet.getRow(0).getPhysicalNumberOfCells();
            System.out.println("the no of columns " + noOfColumns);
            System.out.println("List Size is : [ " + list.size() + " ]");
            if (noOfColumns == list.size()) {
                System.out.println("Input column and table column is Matched hence checking the Matching of columns");
                for (int i = 0; i < noOfColumns; i++) {
                    XSSFCell header_cell = header_row.getCell(i);
                    String header = header_cell.getStringCellValue();
                    System.out.println("header column" + i + "value is " + header);
                    xcelheaderlist.add(header);
                }
                for (int i = 0; i < noOfColumns; i++) {
                    XSSFCell firstrow_cell = first_row.getCell(i);
                    int cellType = firstrow_cell.getCellType();
                    System.out.println("Cell Type is for cell" + i +" is  : [ " +cellType+ " ]");
                    //xcelheaderlist.add(header);

                }
                Iterator<Cell> cellIterator = first_row.iterator();
                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            System.out.println("cell Type");
                            System.out.print("VARCHAR"+cell.getStringCellValue() + "\t");
                            boolean chkdate = Xcelread.isValidDate(cell.getStringCellValue());
                            if(chkdate) {
                                xceldatatyplist.add("TIMESTAMP");
                                break;

                            }
                            else {
                                xceldatatyplist.add("VARCHAR");
                                break;
                            }
                        case Cell.CELL_TYPE_NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)) {
                                String dateStr = String.valueOf(cell.getDateCellValue());
                                System.out.println("date : "+dateStr);
                                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                                Date date = (Date) formatter.parse(dateStr);
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(date);
                                String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR) + ":" +  cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                                System.out.println("formatedDate : " + formatedDate);
                                if( cal.get(Calendar.HOUR) == 0 && cal.get(Calendar.MINUTE) == 0 && cal.get(Calendar.SECOND) == 0) {
                                    xceldatatyplist.add("DATE");
                                    break;
                                }
                                else {
                                    System.out.println("formatedDate : " + formatedDate);
                                    xceldatatyplist.add("TIMESTAMP");
                                    break;
                                }
                            }
                            else {
                                System.out.print("NUMERIC"+cell.getNumericCellValue() + "\t");
                                xceldatatyplist.add("NUMERIC");
                                break;
                            }
                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.print(cell.getBooleanCellValue() + "\t");
                            break;
                        default:


                    }
                    System.out.println();
                }
                Iterator iterator2 = xceldatatyplist.iterator();
                System.out.println("\nUsing xcel  ListIterator:\n");
                while (iterator2.hasNext()) {
                    System.out.println("Value xcel list is : " + iterator2.next());
                }
                if(list.equals(xcelheaderlist) && xceldatatyplist.equals(datatypColmlist)){
                    checkColumnstatus = true;
                }
                if (checkColumnstatus) {
                    //do DB insert
                    System.out.println("Coulmn Matched with xcel" + checkColumnstatus);
                    int ins = tableIns(FILE_NAME,"U",tablename);
                    return  ins == 1 ? true : false;
                } else {
                    System.out.println("Columns Matching Failed Kindly add  appropriate columns");
                    return false;
                }
            } else {
                System.out.println("Columns Matching Failed Kindly add  appropriate columns");
                return false;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static int tableIns(String filename,String status,String tableName){

        cxpsLogger.info("file Insertion started for db insert");
        String sql = "Insert into CUSTOM_CL5_UPLOAD_STATS (FILENAME, FILE_UPLOAD_STATS, TABLENAME, CREATED_TIME, UPDATED_TIME) values (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setString(1,filename);
            ps.setString(2,status);
            ps.setString(3,tableName);
            int i  = ps.executeUpdate();
            System.out.println("Insert status : [ "+i+ " ]");
            con.commit();
            return i;
        }
        catch (Exception e){
            cxpsLogger.info("Exception during insertion of file for data insertion");
            e.printStackTrace();
        }
        return 0;

    }

    public static int auditInsert(String loginUser, String tableName, String fileName,String activity) {
        System.out.println("Getting current user" + loginUser);
        String sql = "Insert into CUSTOM_CL5_UPLOAD_AUDIT (CREATED_TIME, UPDATED_TIME,USERNAME,TABLENAME,FILENAME,ACTIVITY) values (CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?,?,?,?)";
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, loginUser);
            ps.setString(2, tableName);
            ps.setString(3, fileName);
            ps.setString(4, activity);
            int i=ps.executeUpdate();
	    cxpsLogger.info("Inserting in to CUSTOM_CL5_UPLOAD_AUDIT table  : [ "+i+ " ]");
            System.out.println("Inserting  in to CUSTOM_CL5_UPLOAD_AUDIT table : [ "+i+ " ]");
            con.commit();
            return i;

        } catch (Exception e) {
            cxpsLogger.info("Exception during insertion of file for data insertion");
  	    System.out.println("Exception during insertion of file for data insertion");
            e.printStackTrace();
        }
        return 0;

    }
  public static String truncateTable(String tableName){
        String sql = "TRUNCATE TABLE "+tableName;
      try (Connection con = Rdbms.getConnection();
          PreparedStatement ps = con.prepareStatement(sql)) {
          int i = 0;
          i=ps.executeUpdate();
	  cxpsLogger.info("Truncated_table : [ "+i+ " ]"+ tableName);
          System.out.println("Truncated_table : [ "+i+ " ]"+ tableName);
          con.commit();
          return "true";
      }catch (Exception e){
          e.printStackTrace();
      }
        return "false";
  }


}
