package clari5.custom.core.utils;

import clari5.custom.db.InboxTemplate;
import clari5.custom.yes.db.InboxAutoReq;

import cxps.apex.utils.CxpsLogger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

/**
 * @author shishir
 * @since 24/01/2018
 */


public class InboxRequest {
    private static CxpsLogger logger = CxpsLogger.getLogger(InboxRequest.class);

    /**
     * @param payload request to create new template
     * @return success :  template has created . failed : failed to create template
     */
    public static JSONObject insert(String payload) {

        InboxAutoReq inboxAutoReq = new InboxAutoReq();
        String modPaylaod = transformPayload(payload);

        try {
            inboxAutoReq.from(modPaylaod);
            InboxTemplate inboxTemplate = new InboxTemplate();

            logger.info("Payload Request " + payload + "\n Modification in request json " + inboxAutoReq.toString());

            int count = 0;

            if (isExist(inboxAutoReq.getScnName(), inboxTemplate))
                return response("alreadyExist");
            else
                count = inboxTemplate.insert(inboxAutoReq);

            if (count > 0) {
                return response("success");
            }
        } catch (Exception e) {
            logger.error("failed to create template " + e.getMessage() + "Cause " + e.getCause());
            return response("failed");
        }
        return response("failed");
    }

    /**
     * @param scnName       Scenario name
     * @param inboxTemplate
     * @return check weather scenarios is exist in inbox_req_table or not.
     */
    private static boolean isExist(String scnName, InboxTemplate inboxTemplate) {

        return (null != inboxTemplate.select(scnName)) ? true : false;

    }

    /**
     * @param payload request json
     * @return Edit inbox template
     */
    public static JSONObject edit(String payload) {
        InboxAutoReq inboxAutoReq = new InboxAutoReq();

        String modPaylaod = transformPayload(payload);

        try {

            inboxAutoReq.from(modPaylaod);
            InboxTemplate inboxTemplate = new InboxTemplate();

            int count = inboxTemplate.update(inboxAutoReq);
            if (count > 0) return response("success");

        } catch (Exception e) {
            logger.error("failed to update the  template " + modPaylaod + "\n Exception " + e.getMessage() + " Cause " + e.getCause());
        }
        return response("failed");
    }

    /**
     * method will call either user has login into inbox template or page refresh
     *
     * @return scenarios list of created templates
     */
    public static JSONObject init() {

        JSONObject js = new JSONObject();
        InboxTemplate inboxTemplate = new InboxTemplate();

        try {
            Set<String>scnList = inboxTemplate.getScnList();

            for (String scnName :scnList){
                js.put(scnName,inboxTemplate.select(scnName).toJson());
            }
            js.put("scenariosList", inboxTemplate.getScnList());


        } catch (JSONException jse) {
            logger.error("Failed to fetch list of template scenarios " + jse.getMessage() + "Cause " + jse.getCause());
        }
        return js;
    }


    public static JSONObject delete(String payload) {
        InboxAutoReq inboxAutoReq = new InboxAutoReq();

        String modPaylaod = transformPayload(payload);
        try {

            inboxAutoReq.from(modPaylaod);
            InboxTemplate inboxTemplate = new InboxTemplate();

            int count = inboxTemplate.delete(inboxAutoReq.getScnName());
            if (count > 0) return response("success");

        } catch (Exception e) {
            logger.error("Failed to delete inbox template " + modPaylaod + "\n Exception " + e.getMessage() + " Cause " + e.getCause());
        }
        return response("failed");
    }

    private static String transformPayload(String payload) {
        StringBuilder sb = new StringBuilder("{");
        sb.append("\"InboxAutoReq\"").append(payload).append("}");
        return sb.toString();

    }

    private static JSONObject response(String result) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("resp", result);
        } catch (JSONException jse) {
            logger.error("failed to set response");
        }
        return jsonObject;
    }
}
