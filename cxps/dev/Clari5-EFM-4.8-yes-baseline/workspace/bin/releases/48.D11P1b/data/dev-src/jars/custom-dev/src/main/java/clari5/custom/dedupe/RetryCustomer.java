package clari5.custom.dedupe;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonArray;
import io.swagger.util.Json;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RetryCustomer {

    private static final CxpsLogger logger = CxpsLogger.getLogger(RetryCustomer.class);

    public static ConcurrentLinkedQueue<JSONObject> getRetryData() {

       // LinkedHashMap<Number,String> failedCustomer = new LinkedHashMap<>();
        ConcurrentLinkedQueue<JSONObject> queue = new ConcurrentLinkedQueue<>();
        logger.info("Inside the getRetryData method");

        try(RDBMSSession session = Rdbms.getAppSession()){
            try(CxConnection connection = session.getCxConnection()){
                try(PreparedStatement statement = connection.prepareStatement("SELECT CUST_ID, STATUS, REQUEST_JSON, SERIAL_NO, RETRY_COUNT, RULE_NAME FROM DEDUPE_RETRY where STATUS='F'")){
                    try(ResultSet rs = statement.executeQuery()){

                        while(rs.next()){
                            //failedCustomer = new LinkedHashMap<>();
                            //failedCustomer.put(rs.getInt("SERIAL_NO"),rs.getString("REQUEST_JSON"));
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("Serial_no",rs.getInt("SERIAL_NO"));
                            jsonObject.put("Request_Json",rs.getString("REQUEST_JSON"));
                            jsonObject.put("CUST_ID",rs.getString("CUST_ID"));
                            jsonObject.put("Rule_name",rs.getString("RULE_NAME"));

                            queue.add(jsonObject);
                        }
                        connection.close();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return queue;
    }

    public static void getStatusChange(int serial,String status,int count){

        logger.debug("Inside the statuschange loop");
        try(RDBMSSession session = Rdbms.getAppSession()){
            try(CxConnection connection = session.getCxConnection()){
            try(PreparedStatement statement = connection.prepareStatement("UPDATE DEDUPE_RETRY SET STATUS=?, RETRY_COUNT=? WHERE SERIAL_NO=?")){
                statement.setString(1,status);
                statement.setInt(2,count);
                statement.setInt(3,serial);
                statement.executeUpdate();
                connection.commit();
                connection.close();
            }

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}