import { Component, OnInit } from '@angular/core';
import { NbbDataService } from '../services/nbb.data.service';
import { CommonFunctions } from '../services/common.functions';
import { NbbService } from '../services/nbb.service';

@Component({
    selector: 'inbox',
    template: require('../inbox/kar.inbox.template.html')
})
export class KarInboxComponent extends CommonFunctions {
    nbbDataService: NbbDataService;
    nbbService: NbbService;
    private postData: string;
    private postDataa: string;
    private method: string;
    item: any;

    checkbox: any;

    constructor(nbbDataService: NbbDataService, nbbService: NbbService) {
        super();
        this.nbbDataService = nbbDataService;
        this.nbbService = nbbService;
    }

    //on click of create new template submit button
    onClick(senario: any) {

        let flag = false;
        let echeckflag = "N";
        let icheckflag = "N";
        //checkbox validation
        this.nbbDataService.officeLIST.map(function (obj) {
            if (obj.checked === true) {
                flag = true; //set the flag again
                //this.checkbox=obj.officename;
                if (obj.officeID === 1) {

                    icheckflag = "Y";
                    obj.checked === false;
                } else if (obj.officeID === 2) {
                    echeckflag = "Y";
                    obj.checked === false;

                } else {
                    echeckflag = "Y";
                    icheckflag = "Y";
                    obj.checked === false;
                }
                console.log("checkbox", obj);

                obj.checked === false;
            }

        })
        if (flag === false) {
            // alert('atleast one checkbox should be checked');
            this.nbbDataService.alert = "false";
            flag = true; //reset the flag again
        }
        if (echeckflag == 'N' && icheckflag == 'N') {
            this.nbbDataService.alert = "false";
        } else {
            this.postData = JSON.stringify(
                {
                    "tempName": senario,
                    "scnName": senario,
                    //"autoEmailChqFlg": echeckflag,
                    "autoInboxChqFlg": icheckflag,
                   // "msgSubject": this.nbbDataService.ssubject,
                    //"message": this.nbbDataService.sinbox,
                    "createdBy": this.nbbDataService.user,
                    "updatedBy": this.nbbDataService.user

                });

            this.method = "saveTemplate";

            console.log(this.postData);
            this.nbbService.getLoadData("", this.nbbDataService.url, this.method, this.postData);
        }
    }
    //on close of alert block
    onClose() {
        this.nbbDataService.res = "";
        this.nbbDataService.showscreen = true;
        this.nbbDataService.screenshow = true;
        this.nbbDataService.showavilscreen = true;
        this.nbbDataService.searchscenario = false;
        this.nbbDataService.scenarios=[];
        this.nbbDataService.eventlist=[];
        this.postData = JSON.stringify(
            {

            });
      //  this.method = "init";

        console.log(this.postData);
        console.log(this.method);
        this.nbbService.getLoadData("scenario", this.nbbDataService.url, "init", this.postData);
        this.postDataa = JSON.stringify(
            {

            });
        //this.method = "scenarioList";

        console.log(this.postData);
        console.log(this.method);
       // this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postDataa);
       setTimeout(() => {  
        this.nbbService.getLoadData("tempscenario", this.nbbDataService.url, "scenarioList", this.postData);
      }, 2000);
        this.nbbDataService.clear();
    }
    //on change event of check box
    onCheck(event: any) {
        console.log("checkbox", event);

        // this.checkbox = item;
    }
    //on select of the scenario to load the event list
  /*  onSelect(event: any) {
        this.nbbDataService.scenario = event;
        console.log("scenario", this.nbbDataService.scenario);
        if (typeof this.nbbDataService.test[this.nbbDataService.scenario] == 'undefined') {
            this.nbbDataService.eventlist = "undefined";
            console.log(this.nbbDataService.eventlist);
        } else {
            this.nbbDataService.eventlist = this.nbbDataService.test[this.nbbDataService.scenario]["eventList"];
            console.log("eventlist", this.nbbDataService.eventlist);
        }
        this.nbbDataService.officeLIST[0].checked = false;
        this.nbbDataService.officeLIST[1].checked = false;
        this.nbbDataService.ssubject = "";
        this.nbbDataService.sinbox = "";

    }*/
    //on select of the existing template. event list, and the existing form data will be loaded 
    public onTempSelect(proj: any) {

        this.nbbDataService.scenario = proj;
        this.nbbDataService.sname = proj;
        console.log("scenario", this.nbbDataService.scenario);
        console.log("sname", this.nbbDataService.sname);
        var isAvailable = false;
        for (var x in this.nbbDataService.allscenarios["scenariosList"]) {
            console.log("X : ", this.nbbDataService.allscenarios["scenariosList"][x]);
        };
        var m = false;
        if (this.nbbDataService.allscenarios["scenariosList"].indexOf(this.nbbDataService.scenario) > -1) {
            m = true;
        }
        console.log("m", m);
        //to load the event list
        //typeof this.nbbDataService.test[this.nbbDataService.scenario] == 'undefined'
        if (!m) {
            this.nbbDataService.noTemp = "undefined";
            console.log(this.nbbDataService.eventlist);
            this.nbbDataService.showscreen = true;
            this.nbbDataService.screenshow = true;
            this.nbbDataService.searchscenario = false;
            this.nbbDataService.showavilscreen = true;
        } else {
            
            this.nbbDataService.showscreen = true;
            this.nbbDataService.screenshow = true;
            this.nbbDataService.searchscenario = true;
            this.nbbDataService.showavilscreen = false;
            //based on scenario parse and load the existing template data
            //this.nbbDataService.temp = this.nbbDataService.allscenarios[this.nbbDataService.scenario];
            this.nbbDataService.temp = JSON.parse(this.replaceAll(this.nbbDataService.allscenarios[this.nbbDataService.scenario], '\n', ' <br>'));
            console.log("temp data", this.nbbDataService.temp);
            //message
            /*this.nbbDataService.message = this.replaceAll(this.nbbDataService.temp.message, '<br>', '\n');
            console.log("message data", this.nbbDataService.temp.message);
            //subject
            this.nbbDataService.subject = this.nbbDataService.temp.msgSubject;*/
            //checkbox
            this.nbbDataService.icheckbox = this.nbbDataService.temp.autoInboxChqFlg;
            console.log("this.nbbDataService.icheckbox", this.nbbDataService.icheckbox);
            //this.nbbDataService.echeckbox = this.nbbDataService.temp.autoEmailChqFlg;
            //console.log("this.nbbDataService.echeckbox", this.nbbDataService.echeckbox);

            if (this.nbbDataService.icheckbox === "Y"){ //&& this.nbbDataService.echeckbox === "Y") {
                this.nbbDataService.officeLIST[0].checked = true;
               // this.nbbDataService.officeLIST[1].checked = true;
            } /*else if (this.nbbDataService.icheckbox === "Y" || this.nbbDataService.echeckbox === "N") {
                this.nbbDataService.officeLIST[0].checked = true;
               // this.nbbDataService.officeLIST[1].checked = false;
            } else if (this.nbbDataService.icheckbox === "N" && this.nbbDataService.echeckbox === "Y") {
                this.nbbDataService.officeLIST[0].checked = false;
                //this.nbbDataService.officeLIST[1].checked = true;
            } */else {
                this.nbbDataService.officeLIST[0].checked = false;
                //this.nbbDataService.officeLIST[1].checked = false;
            }
            this.nbbDataService.eventlist = this.nbbDataService.test[this.nbbDataService.scenario]["eventList"];
            console.log("eventlist", this.nbbDataService.eventlist);
        }
    }
    //to replace the newline character
    escapeRegExp(string: any) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }

    /* Define functin to find and replace specified term with replacement string */
    replaceAll(str: any, term: any, replacement: any) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    }
    //on delete of the existing scenario
    onDelete(senario: any) {
        this.postData = JSON.stringify(
            {
                "tempName": senario,
                "scnName": senario,
                "updatedBy": this.nbbDataService.user


            });
        this.method = "deleteTempate";

        console.log(this.postData);
        console.log(this.method);
        this.nbbService.getLoadData("", this.nbbDataService.url, this.method, this.postData);
    }
    //on submit of the existing template.
    onEdit(senario: any) {
        let flag = false;
        let echeckflag = "N";
        let icheckflag = "N";
        //checkbox validation
        this.nbbDataService.officeLIST.map(function (obj) {
            if (obj.checked === true) {
                flag = true; //set the flag again
                //this.checkbox=obj.officename;
                if (obj.officeID === 1) {

                    icheckflag = "Y";
                } else if (obj.officeID === 2) {
                    echeckflag = "Y";

                } else {
                    echeckflag = "Y";
                    icheckflag = "Y";
                }
                console.log("checkbox", obj);
            }

        })
        if (flag === false) {
            // alert('atleast one checkbox should be checked');
            this.nbbDataService.alert1 = "false";   
            flag = true; //reset the flag again
        }
        if (echeckflag == 'N' && icheckflag == 'N') {
            this.nbbDataService.alert1 = "false";
        } else {
            this.postData = JSON.stringify(
                {
                    "tempName": senario,
                    "scnName": senario,
                    //"autoEmailChqFlg": echeckflag,
                    "autoInboxChqFlg": icheckflag,
                   // "msgSubject": sub,
                    ///"message": inbox,
                    "updatedBy": this.nbbDataService.user

                });

            this.method = "editTemplate";

            console.log(this.postData);
            console.log(this.method);
            this.nbbService.getLoadData("", this.nbbDataService.url, this.method, this.postData);
        }

    }
    //on close of the checkbox validation alert message
    onClosealert() {
        this.nbbDataService.alert = "";
        this.nbbDataService.alert1 = "";
    }

    //on close of search template alert
    onClosenoTemp() {
        this.nbbDataService.clear();
    }

}

