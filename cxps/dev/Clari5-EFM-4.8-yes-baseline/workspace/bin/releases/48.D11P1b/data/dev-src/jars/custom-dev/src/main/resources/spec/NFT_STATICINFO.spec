clari5.custom.mapper.entity.NFT_STATICINFO {
                       attributes=[
                                { name: EVENT_ID , type="string:50" ,key=true },
                                { name: INITSUBENTITYVAL ,type ="string:200" }
                                { name: TRAN_DATE ,type =timestamp }
				{ name: FINALSUBENTITYVAL ,type ="string:200" }
				{ name: ACCT_ID ,type ="string:50" }
				{ name: BRANCH_ID ,type ="string:50" }
				{ name: ENTITYTYPE ,type ="string:50" }
				{ name: CHANNEL ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: CUST_ID ,type ="string:50" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: EVENTTS ,type =timestamp }
				{ name: SYNC_STATUS ,type ="string:4000" ,default="NEW" }
				{ name: SERVER_ID ,type ="number" }
                                ]
				indexes {
              				 IDX_COMP_NFTSTATINFO : [ SYNC_STATUS , SERVER_ID ]
	        			}
                        }
        


