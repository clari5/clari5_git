cxps.events.event.nft-scnclosure{
  table-name : EVENT_NFT_SCNCLOSURE
  event-mnemonic: SC
  workspaces : {
    ACCOUNT: ws-key
  }
  event-attributes : {
	ws-key: {db : true ,raw_name : ws-key ,type : "string:200"}
	ws-name: {db : true ,raw_name : ws-name ,type : "string:200"}
	fact-name: {db : true ,raw_name : fact-name,type : "string:200"}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
	}
}
