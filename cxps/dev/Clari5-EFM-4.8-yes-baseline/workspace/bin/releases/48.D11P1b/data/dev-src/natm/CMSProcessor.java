package clari5.aml.cms.newjira;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.jira.ClosedIncidHandler;
import clari5.aml.commons.EntityFactManager;
import clari5.aml.risk.RiskEvaluator;
import clari5.aml.risk.RiskManager;
import clari5.hfdb.DbLock;
import clari5.hfdb.RiskLevel;
import clari5.platform.applayer.Clari5;
import clari5.platform.cmq.CxqDaemon;
import clari5.platform.fileq.Clari5Payload;
import clari5.platform.jira.JiraClientException;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.platform.util.CxJson;
import clari5.rdbms.Rdbms;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.CaseEvents;
import cxps.eoi.Evidence;
import cxps.eoi.EvidenceData;
import cxps.eoi.IncidentEvent;
import cxps.noesis.utils.AmlCaseEvent;
import cxps.noesis.utils.EFMCaseEvent;
import cxps.noesis.utils.RDECaseEvent;
import cxps.noesis.utils.STAFFCaseEvent;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class CMSProcessor extends CxqDaemon {

    protected static CxpsLogger logger = CxpsLogger.getLogger(CMSProcessor.class);

    public CMSProcessor() {
        super();
    }

    @Override
    public boolean isBulkProcessor() {
        return true;
    }

    @Override
    public boolean processBulk(List<Clari5Payload> list) {

        for(Clari5Payload cp : list) {
            if(!process(cp))
                return false;
        }
        return true;
    }


    @Override
    public boolean dedup(Clari5Payload clari5Payload) {
        return true;
    }
    @Timed
    @ExceptionMetered
    @Metered
    @Override
    public boolean process(Clari5Payload clari5Payload) {

        logger.debug("[Processing Payload ] " + clari5Payload.toString()); // {"processor":"NEWCMS","event-id":"fact-delta"}
        ICXLog icxLog = CXLog.fenter("CMSProcessor.process");
        icxLog.setProcessorId(qName);
        DbLock l = new DbLock();
        CxJson j = null;
        try {
            j = CxJson.parse(clari5Payload.message);
        } catch (IOException e) {
            return false;
        }
        String eventType = j.getString("event-name", "");    // extract event type from event
        if (eventType.equals("incident-closed")) {
            icxLog.ckpt("Incident-close-event");
            String incidentId = j.getString("key", "");
            String projectId = incidentId.split("-")[0];
            try {
                String lockid = getCaseEntityId(incidentId, projectId);
                icxLog.ckpt("lock:" + lockid);
                if (lockid != null) {
                    clari5Payload.entityid = lockid;
                }
            } catch (CmsException.Configuration configuration) {
                configuration.printStackTrace();
                icxLog.ferrexit("ConfigErr", configuration);
                return false;
            } catch (SQLException e) {
                icxLog.ferrexit(e.getMessage(), e);
                return false;
            }
        }
        try {
            if (eventType.equals("incident-closed")) {
                try {
                    if(!l.tryLock(clari5Payload.entityid))
                        return false;
                    new CaseClosureHandler().caseClosureHandler(clari5Payload.message);
                } catch (CmsException.Configuration | RdbmsException | SQLException configuration) {
                    configuration.printStackTrace();
                    icxLog.ferrexit("ConfigErr", configuration);
                    return false;
                } catch (CmsException.Data data) {
                    error(clari5Payload, data.getMessage());
                    icxLog.ferrexit("DataErr", data);
                }
            } else {
                icxLog.ckpt("Case and Incident-Creation-event");
                String triggerId = j.getString("trigger_id", "");
                String moduleId = j.getString("module", "");
                IncidentEvent event = null;
                switch (CaseEvents.valueOf(moduleId)) {
                    case AML:
                        event = new AmlCaseEvent();
                        break;
                    case EFM:
                        event = new EFMCaseEvent();
                        break;
                    case STAFF:
                        event = new STAFFCaseEvent();
                        break;
                    case RDE:
                        event = new RDECaseEvent();
                        break;
                    case UNKNOWN:
                        break;
                    default:
                }
                if (event != null) {
                    try {
                        event.setTrigger_Id(triggerId);
                        try {
                            icxLog.ckpt("DBLoad");
                            event.load();
                            if (event.getModuleId() == null) {
                                logger.error("Event is not available in incident tbl: " + event.getTrigger_Id());
                                return false;
                            }
                            String parentEntity = CMSUtility.getInstance(event.getModuleId()).formatter.getParentEntityId(event);
                            clari5Payload.entityid = event.getModuleId() + parentEntity;
                            if (!l.tryLock(clari5Payload.entityid))
                                return false;

                            List<? extends IncidentEvent> incidentEvents = event.loadEventIdBasis();

                            for (IncidentEvent incidentEvent : incidentEvents) {
                                incidentEvent.setCaseEntityId(CMSUtility.getInstance(incidentEvent.getModuleId()).formatter.getParentEntityId(incidentEvent));
                                if (incidentEvent.getCaseEntityId().equals(parentEntity)) {
                                    if (incidentEvent.getFlag().equals(INCIDENT_FLAG_RETRY) || incidentEvent.getFlag().equals(INCIDENT_FLAG_FRESH) || incidentEvent.getFlag().equals(INCIDENT_STATUS_JIRA_ERROR))
                                        processEvent(incidentEvent);
                                    else {
                                        icxLog.ckpt("Event already processed with PROCESSED_EVENTID [" + incidentEvent.getProcessedEventId() + "]");
                                        icxLog.ckpt(event.convertToJson(incidentEvent));
                                    }
                                }
                            }
                            icxLog.ckpt("EventId:"+event.getEventId());
                            icxLog.setRequest(event.getTrigger_Id());
                            icxLog.setParentId(event.getEventId());
                            icxLog.setEntityId(event.getEntityId());
                            logger.info("Printing event after load" + event.convertToJson(event));
                        } catch (SQLException e) {
                            icxLog.ferrexit(e.getMessage(), e);
                            return false;
                        }
                    } catch (CmsException.Retry e) {
                        icxLog.ferrexit("CMSException.retry", e);
                        RDBMSSession session = null;
                        try {
                            session = Rdbms.getAppSession();
                            event.setFlag(INCIDENT_FLAG_RETRY);
                            event.setDate_time(System.currentTimeMillis());
                            event.setProcessedTime(System.currentTimeMillis());
                            event.setJira_exception(e.getLocalizedMessage());
                            event.update(session);
                            session.commit();
                        } catch (Exception e1) {
                            if (session != null) session.rollback();
                            e1.printStackTrace();
                        }
                        finally {
                            if (session != null) session.close();
                        }
                        return false;
                    } catch (Exception ex) {
                        System.out.println("Exception occurred " + ex.getMessage() + ":" + event.getTrigger_Id());
                        ex.printStackTrace();
                        RDBMSSession session = null;
                        icxLog.error("Setting Incident table status and flag as E", ex);
                        event.setFlag(INCIDENT_STATUS_JIRA_ERROR);
                        event.setStatus(INCIDENT_STATUS_JIRA_ERROR);
                        event.setJira_exception(ex.getLocalizedMessage());
                        event.setProcessedTime(System.currentTimeMillis());
                        try {
                            session = Rdbms.getAppSession();
                            event.update(session);
                            session.commit();
                            error(clari5Payload, ex.getLocalizedMessage());
                        } catch (SQLException | RdbmsException e1) {
                            if (session != null) session.rollback();
                            e1.printStackTrace();
                        } finally {
                            if (session != null) session.close();
                        }
                        return false;
                    }
                } else {
                    icxLog.ferrexit("InvalidIncidentType" + moduleId);
                }
            }
            /**
             * Release Lock
             */
        } catch (IOException e) {
            e.printStackTrace();
            icxLog.ferrexit("io.excp", e);
        } catch (Throwable t) {
            t.printStackTrace();
            icxLog.ferrexit("unk.excp", t);
            throw t;
        } finally {
            l.releaseLock(clari5Payload.entityid);
            icxLog.ckpt("releaseLock");
        }
        icxLog.requestEnd("C");
        return true;
    }

    public final static String INCIDENT_FLAG_FRESH = "F";
    public final static String INCIDENT_FLAG_IN_PROGRESS = "I";
    public final static String INCIDENT_FLAG_PROCESSED = "P";
    public final static String INCIDENT_STATUS_ADDED = "A";
    public final static String INCIDENT_STATUS_CLOSED = "C";
    public final static String INCIDENT_STATUS_JIRA_ERROR = "E";
    public final static String INCIDENT_FLAG_RETRY = "R";
    public static final String INCIDENT_STATUS_SUPPRESSED = "S";
    public static final String INCIDENT_STATUS_NOTMATCHED = "X";

    /* This method is used to process the incidents picked from the Incident_Table
    * based on the EntityId and FactName which are picked from Index_Table
    * New Incidents with F flag are processed.
    * If the previous incident is processed and available then the new incident is updated.
    * If the previous incident is processed and closed then the new incident is created.
    * If the previous incident is not present then a new case is created.
    * Finally the EntityId and FactName are deleted from Index_Table
    */
    @Timed
    @ExceptionMetered
    @Metered
    private void processEvent(IncidentEvent event) throws Exception {
        String parentId = null;
        String incidentId;
        IncidentEvent incident;
        IncidentEvent iEvent = null;
        ICXLog icxLog = CXLog.fenter("CMSProcessor.processEvent");
        try {
            icxLog.ckpt("Loading all non processed events of " + event.getCaseEntityId() + "entity");
            event.setProject(CMSUtility.getInstance(event.getModuleId()).formatter.getProjectId(event));
            event.setEntityId(CMSUtility.getInstance(event.getModuleId()).formatter.getChildEntityId(event));
            event.setCaseEntityId(CMSUtility.getInstance(event.getModuleId()).formatter.getParentEntityId(event));
            setFlag(event.getModuleId(), event); // always retry
            if (!decide(event))  // always retry
                return;
            if ((iEvent = event.getParentCase(INCIDENT_FLAG_PROCESSED, INCIDENT_STATUS_ADDED)) != null) {   //always retry
                icxLog.ckpt("Case is alreday open for [" + iEvent.getCaseEntityId() + "] + with CASE ID [" + parentId + "]");
                parentId = iEvent.getJira_parentId();
                logger.debug("Case is alreday open for [" + iEvent.getCaseEntityId() + "] + with CASE ID [" + parentId + "]");
            }
        } catch (Exception e) {
            throw new CmsException.Retry(e.getMessage());
        }
        if (parentId == null) {
            icxLog.ckpt("Creating case for [" + event.getCaseEntityId() + "]");
            parentId = createCase(event);
            event.setJira_parentId(parentId);
            RDBMSSession session = null;
            try {
                session = Rdbms.getAppSession();
                event.setProcessedTime(System.currentTimeMillis());
                event.update(session);
                session.commit();
            } catch (Exception e) {
                if (session != null) session.rollback();
                e.printStackTrace();
            } finally {
                if (session != null) session.close();
            }
        }
        event.setJira_parentId(parentId);
        try {
            logger.debug("Checking if incident is already open ");
            icxLog.ckpt("Checking if incident is already open for " + event.getEntityId());
            incident = event.getParentIncident(INCIDENT_STATUS_ADDED, INCIDENT_FLAG_PROCESSED);  //always retry
        } catch (SQLException e) {
            throw new CmsException.Retry(e.getMessage());
        }
        if (incident != null) {
            if (parentId.equals(incident.getJira_parentId())) {
                icxLog.ckpt("Incident is already available for [" + incident.getEntityId() + "] + with INCIDENT ID [" + incident.getJira_incidentId() + "] and TriggerId: " + incident.getTrigger_Id() + "]");
                incidentId = incident.getJira_incidentId();
            } else incidentId = null;
        } else {
            incidentId = null;
        }
        try {
            String caseInciId = null;
            String caseIncidentId = null;
            if (incidentId != null) {
                //update Incident
                String id = incidentId;
                icxLog.ckpt("Incident is already available to be updated. So calling UpdateIncident");
                logger.debug("Incident available to be updated. " +
                        "Incident to be updated: " + incidentId + " with TriggerId: " + event.getTrigger_Id());
                if (event.getSource().equalsIgnoreCase("sam")) {
                    Evidence evidence = event.getEvidence();
                    Evidence oldEvidence = incident.getEvidence();
                    List<EvidenceData> evidenceDataList = evidence.getEvidenceData();
                    List<EvidenceData> oldEvidenceDataList = oldEvidence.getEvidenceData();
                    Set<String> oldEventIds = new HashSet<>();
                    Set<String> eventIds = new HashSet<>();
                    for (EvidenceData oldEvidenceData : oldEvidenceDataList) {
                        oldEventIds.addAll(oldEvidenceData.getEventIds());
                    }
                    for (EvidenceData evidenceData : evidenceDataList) {
                        eventIds.addAll(evidenceData.getEventIds());
                    }
                    eventIds.removeAll(oldEventIds);
                    if (eventIds.size() > 0) {
                        caseIncidentId = updateIncident(event, id);
                    } else {
                        event.setFlag(INCIDENT_FLAG_PROCESSED);
                        event.setStatus("D");
                        event.setJira_incidentId(id);
                        event.setProcessedEventId(event.getEventId());
                        event.setProcessedTime(System.currentTimeMillis());
                    }
                } else {
                    caseIncidentId = updateIncident(event, id);
                }
                if (caseIncidentId != null && caseIncidentId.contains("_")) {
                    event.setJira_parentId(caseIncidentId.substring(0, caseIncidentId.indexOf("_")));
                    caseInciId = caseIncidentId.substring(caseIncidentId.indexOf("_") + 1);
                } else {
                    caseInciId = caseIncidentId;
                }
            } else {
                //Create incident with parentid
                icxLog.ckpt("The incident is closed or not available for [" + event.getEntityId() + "] with TriggerId: " + event.getTrigger_Id() + "]. So Calling CreateIncident");
                logger.debug("The incident is closed or not available .So cannot be updated. New incident to be created");
                caseInciId = createIncident(parentId, event);
            }
            if (caseInciId != null) {
                event.setFlag(INCIDENT_FLAG_PROCESSED);
                event.setStatus(INCIDENT_STATUS_ADDED);
                event.setJira_incidentId(caseInciId);
                event.setProcessedEventId(event.getEventId());
                event.setProcessedTime(System.currentTimeMillis());
            }
        } catch (CmsException.Data e) {
            icxLog.error("Setting Incident table status and flag as E", e);
            event.setFlag(INCIDENT_STATUS_JIRA_ERROR);
            event.setStatus(INCIDENT_STATUS_JIRA_ERROR);
        }
        RDBMSSession session = null;
        try {
            session = Rdbms.getAppSession(); // ignore all exceptions here
            EntityFactManager efm = new EntityFactManager(event);
            icxLog.ckpt("Calling Incident table update");
            event.update(session);
            icxLog.ckpt("Calling EntityFact insert");
            efm.save(session);
            session.commit();
            //newly added.
            System.out.println("calling postAssignmentAction");
            CMSUtility.getInstance(event.getModuleId()).formatter.postAssignmentAction(event);
            icxLog.ckpt("IncidentProcessed");
            icxLog.fexit();
        } catch (Exception e) {
            if (session != null) session.rollback();
            icxLog.error("Exception Saving in incident/EntityFact table ", e);
            throw e;
        } finally {
            if (session != null)
                session.rollback();
            session.close();
        }

    }
    @Timed
    @ExceptionMetered
    @Metered
    private boolean decide(IncidentEvent incidentEvent) throws CmsException{
        //Checking if suppDur is true or false
        boolean decision = false;
        ICXLog icxLog = CXLog.fenter("CMSProcessor.decide");
        icxLog.ckpt("Checking Suppressed or not");
        boolean isSuppDur = CMSUtility.getInstance(incidentEvent.getModuleId()).getSuppressionDur(incidentEvent.getFactname(), incidentEvent.getEntityId());
        if (isSuppDur) {
            RDBMSSession session = null;
            try {
                session = Rdbms.getAppSession();
                incidentEvent.setFlag(INCIDENT_FLAG_PROCESSED);
                incidentEvent.setStatus(INCIDENT_STATUS_SUPPRESSED);
                incidentEvent.setJira_exception("Incident Suppressed");
                incidentEvent.setProcessedTime(System.currentTimeMillis());
                incidentEvent.update(session);
                session.commit();
                icxLog.ckpt("Suppressed incident found");
            } catch (Exception e) {
                if (session != null) session.rollback();
                icxLog.error("Exception in Saving Suppressed incident ", e);
            }
            finally {
                if (session != null) session.close();
            }
            return false;
        }
        icxLog.ckpt("Checking Risk level match");
        RiskManager riskManager = (RiskManager) Clari5.getResource("risk");
        RiskEvaluator risk = riskManager.getRiskEvaluator();
        boolean riskcal = risk.decide(incidentEvent.getFactname(), RiskLevel.valueOf(incidentEvent.getRiskLevel()), incidentEvent.getEntityId());
        if (!riskcal) {
            RDBMSSession session = null;
            try {
                session = Rdbms.getAppSession();
                incidentEvent.setFlag(INCIDENT_FLAG_PROCESSED);
                incidentEvent.setStatus(INCIDENT_STATUS_NOTMATCHED);
                incidentEvent.setJira_exception("Risk Level not matched !");
                incidentEvent.setProcessedTime(System.currentTimeMillis());
                incidentEvent.update(session);
                session.commit();
                icxLog.ckpt("Risk Level not matched !");
            } catch (Exception e) {
                if (session != null) session.rollback();
                icxLog.error("Exception in Saving Risk Level not matched incident ", e);
                e.printStackTrace();
            }
            finally {
                if (session != null) session.close();
            }
            return false;
        }
        decision = true;
        icxLog.fexit();
        return decision;
    }

    //Case Creation on Jira.
    @Timed
    @ExceptionMetered
    @Metered
    public String createCase(IncidentEvent event) throws CmsException {
        CxJson caseResponse;
        ICXLog icxLog = CXLog.fenter("CMSProcessor.createCase");
        try {
            icxLog.ckpt("Populating Casejson for Createcase");
            CxJson case_fieldsJson = CMSUtility.getInstance(event.getModuleId()).populateCaseJson(event);
            if (case_fieldsJson != null) {
                caseResponse = CMSUtility.getInstance(event.getModuleId()).jiraClient.sendCreateIssueRequest(case_fieldsJson.toString());
                icxLog.ckpt("Case is created in JIRA" + caseResponse);
                icxLog.fexit();
            } else return null;
        } catch (JiraClientException.Retry e) {
            throw new CmsException.Retry(e.getMessage());
        } catch (JiraClientException e) {
            icxLog.error("Exception in Create case", e);
            throw new CmsException(e);
        }
        return caseResponse.getString("key", "");
    }

    @Timed
    @ExceptionMetered
    @Metered
    public String createIncident(String jiraParentId, IncidentEvent event) throws CmsException {
        CxJson incident_response;
        ICXLog icxLog = CXLog.fenter("CMSProcessor.createIncident");
        try {
            icxLog.ckpt("Populating Incidentjson for CreateIncident");
            CxJson incident_fieldsJson = CMSUtility.getInstance(event.getModuleId()).formatter.populateIncidentJson(event, jiraParentId, false);
            if (incident_fieldsJson != null) {
                incident_response = CMSUtility.getInstance(event.getModuleId()).jiraClient.sendCreateIssueRequest(incident_fieldsJson.toString());
                icxLog.ckpt("Incident is created in JIRA" + incident_response);
            } else return null;
        }catch (JiraClientException.IssueDoesNotExist e) {
            throw new CmsException.IssueDoesNotExist(e.getMessage());
        }catch (JiraClientException.IncidentStateException e) {
            throw new CmsException.IncidentStateException(e.getMessage());
        }catch (JiraClientException.Retry e){
            throw new CmsException.Retry(e.getLocalizedMessage());
        }catch (JiraClientException | IOException e) {
            icxLog.error("Exception in Create incident", e);
            throw new CmsException(e.getMessage());
        }
        String incidentId = incident_response.getString("key", "");
        icxLog.fexit();
        return incidentId;
    }
    @Timed
    @ExceptionMetered
    @Metered
    public String updateIncident(IncidentEvent event, String id) throws CmsException {

        String incidentId = id;//CMSUtility.getInstance(event.getModuleId()).getIncidentId(id);
        ICXLog icxLog = CXLog.fenter("CMSProcessor.updateIncident");
        String caseId = event.getJira_parentId();
        try {
            icxLog.ckpt("Populating Incidentjson for UpdateIncident");
            CxJson updateJson = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, null, true);
            CxJson wrapperJson = new CxJson().put("fields", updateJson);
            CMSUtility.getInstance(event.getModuleId()).jiraClient.updateIssueRequest(wrapperJson.toString(), incidentId);
            icxLog.ckpt("Incdident is updated in JIRA");
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        } catch (JiraClientException.Data e) {
            throw new CmsException.Data(e.getMessage());
        } catch (JiraClientException.IncidentStateException | JiraClientException.IssueDoesNotExist jiraExp) {
            System.out.println("Incident is already closed in jira");
            jiraExp.printStackTrace();
            ClosedIncidHandler clHandler = new ClosedIncidHandler(CMSUtility.getInstance(event.getModuleId()).jiraClient);
            try {
                String closedEvent = clHandler.getClosedIncident(incidentId);
                new CaseClosureHandler().caseClosureHandler(closedEvent);
            } catch (Exception e) {
                icxLog.info("Attempt to Update a non-existent incident. Creating a new incident");
            }
            icxLog.ckpt("Attempt to Update a non-existent incident. Creating a new incident");
            String newIncidentId = null;
            try {
                newIncidentId = createIncident(caseId, event);
                return newIncidentId;
            }catch (CmsException.IncidentStateException | CmsException.IssueDoesNotExist jiraEx){
                String newjiraParentId = createCase(event);
                newIncidentId = createIncident(newjiraParentId, event);
                return newjiraParentId +"_" +newIncidentId;
            }
        }catch (JiraClientException.Retry e){
            throw new CmsException.Retry(e.getLocalizedMessage());
        }
        catch (JiraClientException | IOException e) {
            throw new CmsException(e);
        }
        icxLog.fexit();
        return id;
    }

    /*This method is used to check if any incident in INCIDENT_TABLE is left with I flag.
     * The incident is checked on Jira. If it exists on Jira then JiraId(s) are returned
     * and updated on INCIDENT_TABLE with P flag. If not then the incident is marked with flag F.
     */
    @Timed
    @ExceptionMetered
    @Metered
    public void setFlag(String moduleId, IncidentEvent event) throws SQLException, CmsException.Configuration, RdbmsException {
        ICXLog icxLog = CXLog.fenter("CMSProcessor.setFlag");
        if (event.getFlag().equals(INCIDENT_FLAG_IN_PROGRESS)) {
            icxLog.ckpt("Incident is found with flag as IN_PROFRESS");
            String trig_id = event.getTrigger_Id();
            CxJson exists = null;
            try {
                icxLog.ckpt("Checking incident is exist on jira for IN_PROGRESS event");
                CmsModule module = CMSUtility.getInstance(event.getModuleId()).cmsMod;
                String entityidCustomField = module.customFields.get("CmsIncident-entityId").getJiraId();
                String factnameCustomField = module.customFields.get("CmsIncident-factName").getJiraId();
                String triggerCustomField = module.customFields.get("trigger_Id").getJiraId();
                String entityJiraId = entityidCustomField.split("_")[1];
                String factnameJiraId = factnameCustomField.split("_")[1];
                String triggerJiraId = triggerCustomField.split("_")[1];
                CxJson jsonObject = new CxJson();
                String data = "cf[" + entityJiraId + "]~" + "'" + event.getEntityId() + "'"
                        + " and "
                        + "cf[" + factnameJiraId + "]~" + "'" + event.getFactname() + "'"
                        + " and "
                        + "cf[" + triggerJiraId + "]~" + "'" + event.getTrigger_Id() + "'";
                jsonObject.put("jql", data);
                exists = CMSUtility.getInstance(moduleId).getJiraInstance().checkOnJira(jsonObject);
            } catch (JiraClientException e) {
                e.printStackTrace(System.err);
            }
            if (exists != null && exists.getInt("total", -1) != 0) {
                icxLog.ckpt("Incident is exist on jira for IN_PROGRESS event");
                logger.debug("Incident with I flag found on Jira. Returning JiraID");
                CxJson ja = exists.get("issues");
                CxJson jo = ja.get(ja.keys().get(0));
                String incidentId = jo.getString("key","");
                CxJson fieldsJson = jo.get("fields");
                CxJson parentjson = fieldsJson.get("parent");
                String parentId = parentjson.getString("key","");
                event.setJira_parentId(parentId);
                event.setFlag(INCIDENT_FLAG_PROCESSED);
                event.setJira_incidentId(incidentId);
                try (RDBMSSession session = Rdbms.getAppSession()) {
                    event.update(session);
                    session.commit();
                }
            } else {
                logger.debug("Incident with I flag not found on jira. Changing flag to F");
                icxLog.ckpt("Incident is not exist on jira for IN_PROGRESS event");
                event.setFlag(INCIDENT_FLAG_FRESH);
                try (RDBMSSession session = Rdbms.getAppSession()) {
                    event.update(session);
                    session.commit();
                }
            }
        }
        icxLog.fexit();
    }

    //get the case_entity_id for locking based on incident_id
    @Timed
    @ExceptionMetered
    @Metered
    private String getCaseEntityId(String incidentId, String projectId) throws CmsException.Configuration, SQLException {
        IncidentEvent incidentEvent = null;
        ICXLog icxLog = CXLog.fenter("CMSProcessor.getCaseEntityId");
        String moduleId = null;
        try {
            CmsJira cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
            moduleId = cmsJira.getModule(projectId);
            if (moduleId == null) {
                throw new CmsException.Configuration("Invalid Departments configuration");
            }
        } catch (CmsException.Configuration e) {
            throw new CmsException.Configuration("Invalid Departments configuration");
        }
        switch (CaseEvents.valueOf(moduleId)) {
            case AML:
                incidentEvent = new AmlCaseEvent();
                break;
            case EFM:
                incidentEvent = new EFMCaseEvent();
                break;
            case STAFF:
                incidentEvent = new STAFFCaseEvent();
                break;
            case RDE:
                incidentEvent = new RDECaseEvent();
                break;
            case UNKNOWN:
                break;
            default:
        }
        try {
            if (incidentEvent != null) {
                icxLog.ckpt("Fetch caseentity_id for locking during incident-closed ");
                incidentEvent.setJira_incidentId(incidentId);
                String caseEntityId = incidentEvent.fetchEntityId();
                icxLog.fexit();
                return moduleId + caseEntityId;
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return null;
    }
}
