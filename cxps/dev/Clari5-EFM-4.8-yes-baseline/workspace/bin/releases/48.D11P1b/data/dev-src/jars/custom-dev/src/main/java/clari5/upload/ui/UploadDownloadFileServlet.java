package clari5.upload.ui;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.CxConnectionPool;
import org.apache.commons.net.ntp.TimeStamp;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import clari5.rdbms.Rdbms;

import java.sql.Date;
import java.util.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.text.SimpleDateFormat;

@WebServlet("/UploadDownloadFileServlet")
public class UploadDownloadFileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String status;
    private String timeStamp = null;
    private int numberOfColumnsDB = 0;
    private String insertSql = null;
    private int rowNum = 0;
    private int columnNum = 0;
    static int count = 0;
    private String requestFileName = null;
    private String fileBackupPath = null;
    private String requestFileNameSplit = null;
    Map<Integer, String> hmap = new HashMap<Integer, String>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    /*public static String getUser(HttpServletRequest request) {
        String sessionKey = "";
        List<Cookie> cookieList = Arrays.asList(request.getCookies());
        for (Cookie cookie : cookieList) {
            if (cookie.getName().equalsIgnoreCase("session_id")) {
                sessionKey = cookie.getValue();
                break;
            }
        }

        String sessionUser = "";
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try (Connection connection = Rdbms.getAppConnection()) {
            ps = connection.prepareStatement("SELECT USER_ID FROM CL5_LIVE_SESSION_TBL WHERE SESSION_KEY = ?");
            ps.setString(1, sessionKey);
            resultSet = ps.executeQuery();
            while (resultSet.next()) {
                sessionUser = resultSet.getString("USER_ID");
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sessionUser;
    }*/

    public static LinkedHashMap<String, String> getTableColumnType(String tableName) {
        LinkedHashMap<String, String> columnType = new LinkedHashMap<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection connection = Rdbms.getAppConnection()) {
            ps = connection.prepareStatement("select * from " + tableName);
            rs = ps.executeQuery();

            ResultSetMetaData metaData = rs.getMetaData();

            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                columnType.put(metaData.getColumnName(i), metaData.getColumnTypeName(i));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columnType;
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String requestTableName;
        HttpSession session;
        ResultSetMetaData rsData;
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());


        session = request.getSession();
        //String userId = getUser(request);
        //session.getAttribute("FileName");
        //requestTableName = (String) session.getAttribute("myTable");
        String userId = (String) session.getAttribute("userId");
        requestTableName = (String) session.getAttribute("myTable");
        fileBackupPath = (String) session.getAttribute("FilePath");
        requestFileName = (String) session.getAttribute("FileName");

        try {
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement("Delete from " + requestTableName);
            ps.executeQuery();
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }


        //getting the count of columns of table
        try {
            // con =DBConnection.getDBConnection();
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement("Select * from " + requestTableName);
            rs = ps.executeQuery();
            rsData = rs.getMetaData();
            numberOfColumnsDB = rsData.getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (rs != null) rs.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
     
/*        if (session == null || session.getAttribute("USERID")== null){
    		try {
    			request.getRequestDispatcher("/expiry.jsp").forward(request,
    				    response);
    		} catch (ServletException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}*/

        // preparing insert query as per column size
        switch (numberOfColumnsDB) {
            case 5:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 6:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 7:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 8:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 9:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 10:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 11:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 12:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 13:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 14:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 15:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 16:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 17:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 18:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 19:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;
            case 20:
                insertSql = "INSERT  INTO " + requestTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,systimestamp,'" + userId + "',systimestamp,'" + userId + "')";
                break;

            default:
                System.out.println("insertion failed  into " + requestTableName + " \n Query is [" + insertSql + "]");
                break;
        }
        System.out.println("Column count [" + numberOfColumnsDB + "] sql Query [" + insertSql + "]");
        //reading the type of file (xls,xlsx)
        System.out.println("UploadDownloadServlet[" + fileBackupPath + "]");
        int s = fileBackupPath.lastIndexOf('.');
        System.out.println("index" + s);
        if (s >= 0)
            requestFileNameSplit = fileBackupPath.substring(s + 1);


        //calling the respective insert function based on file type
        if (requestFileNameSplit.equalsIgnoreCase("xlsx")) {
            count = 0;
            xlsxFileInsert(requestTableName, response);
        } else if (requestFileNameSplit.equalsIgnoreCase("xls")) {
            count = 0;
            xlsFileInsert(requestTableName, response);
        }

        System.out.println("File Type[" + requestFileNameSplit + "] and the table is [" + requestTableName + "]");
        //update the audit table 
        UploadUiAudit.updateAudit(requestTableName, userId, status, "", requestFileName, "Insert");
        request.setAttribute("TOTAL", count);
        request.getRequestDispatcher("/DisplayInsert").include(request, response);
    }

    public void xlsFileInsert(String requestTableName, HttpServletResponse response) {

        LinkedHashMap<String, String> columnNameWithType = getTableColumnType(requestTableName != null ? requestTableName : "");
        LinkedList<String> headerList = new LinkedList<>();
        HSSFWorkbook wb;
        HSSFSheet ws;
        BigInteger temp = null;
        int error = 0;
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            //  con = DBConnection.getDBConnection();
            con = Rdbms.getAppConnection();
	    //System.out.println("Connection acquird.. Printing connection object ---> "+con);
	    //System.out.println("insert sql data: --> " +insertSql);
	    
            ps = con.prepareStatement(insertSql);
            wb = new HSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum();
            columnNum = ws.getRow(0).getLastCellNum();
            Iterator<Row> rowIterator = ws.iterator();
            while (rowIterator.hasNext()) {
                int cellPos = 0;
                Row row1 = rowIterator.next();
                for (int i = 0; i < ws.getRow(0).getLastCellNum(); i++) {
                    Cell cell1 = row1.getCell(i, Row.RETURN_BLANK_AS_NULL);
                    cellPos++;
                    if (row1.getRowNum() == 0) {
                        headerList.add(cell1.getStringCellValue().trim());
                    } else {
                        if (cell1 == null) {
                            if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("VARCHAR2")) {
                                ps.setString(cellPos, "");
                            } else if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("NUMBER")) {
                                ps.setInt(cellPos, 0);
                            } else if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("DATE")) {
                                ps.setDate(cellPos, Date.valueOf(""));
                            } else if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("TIMSTAMP")) {
                                ps.setTimestamp(cellPos, Timestamp.valueOf(""));
                            }
                        } else {
                            if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING)  // insertion of String data
                            {
                                ps.setString(cellPos, cell1.getStringCellValue().trim());
                            } else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {

                                if (HSSFDateUtil.isCellDateFormatted(cell1) &&
                                        columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("TIMESTAMP")) {      // Insertion of Date cell
                                    timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                    ps.setTimestamp(cellPos, Timestamp.valueOf(timeStamp));
                                } else if (HSSFDateUtil.isCellDateFormatted(cell1) &&
                                        columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("DATE")) {      // Insertion of Date cell
                                    timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                    ps.setDate(cellPos, Date.valueOf(timeStamp));
                                } else {
                                    temp = BigDecimal.valueOf(cell1.getNumericCellValue()).toBigInteger();//Insertion of number field
                                    ps.setLong(cellPos, temp.longValue());
                                }
                            }
                        }
                    }
                }
                if (row1.getRowNum() > 0) {
                    try {
                        ps.executeUpdate();
                        count++;
                    } catch (SQLException e) {
                        error++;
                        status = "Failure";
                        e.printStackTrace();
                    }
                }
            }
            if (error > 0) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Error in Inserting " + error + " records  Kindly check the logs for more information \" " + ");");
                out.println("</script>");

            }

            con.commit();
            con.close();
            if (error == 0) {
                status = "Success";
            }
        } catch (SQLException | IOException e) {

            status = "Failure";
            e.printStackTrace();
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void xlsxFileInsert(String requestTableName, HttpServletResponse response) {
        LinkedHashMap<String, String> columnNameWithType = getTableColumnType(requestTableName != null ? requestTableName : "");
        LinkedList<String> headerList = new LinkedList<>();

        XSSFWorkbook wb;
        XSSFSheet ws;
        int error = 0;


        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            //  con = DBConnection.getDBConnection();
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement(insertSql);
            wb = new XSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum() + 1;
            columnNum = ws.getRow(0).getLastCellNum();
            Iterator<Row> rowIterator = ws.iterator();
            while (rowIterator.hasNext()) {
                int cellPos = 0;
                Row row1 = rowIterator.next();
                for (int i = 0; i < ws.getRow(0).getLastCellNum(); i++) {
                    Cell cell1 = row1.getCell(i, Row.RETURN_BLANK_AS_NULL);
                    cellPos++;
                    if (row1.getRowNum() == 0) {
                        headerList.add(cell1.getStringCellValue().trim());
                    } else {
                        if (cell1 == null) {
                            if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("VARCHAR2")) {
                                ps.setString(cellPos, "");
                            } else if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("NUMBER")) {
                                ps.setInt(cellPos, 0);
                            } else if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("DATE")) {
                                ps.setDate(cellPos, Date.valueOf(""));
                            } else if (columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("TIMSTAMP")) {
                                ps.setTimestamp(cellPos, Timestamp.valueOf(""));
                            }
                        } else {
                            if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING)  // insertion of String data
                            {
                                ps.setString(cellPos, cell1.getStringCellValue().trim());
                            } else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {

                                if (HSSFDateUtil.isCellDateFormatted(cell1) &&
                                        columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("TIMESTAMP")) {      // Insertion of Date cell
                                    timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                    ps.setTimestamp(cellPos, Timestamp.valueOf(timeStamp));
                                } else if (HSSFDateUtil.isCellDateFormatted(cell1) &&
                                        columnNameWithType.get(headerList.get(i)).equalsIgnoreCase("DATE")) {      // Insertion of Date cell
                                    timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                    ps.setDate(cellPos, Date.valueOf(timeStamp));
                                } else {
                                    BigInteger temp = BigDecimal.valueOf(cell1.getNumericCellValue()).toBigInteger();//Insertion of number field
                                    ps.setLong(cellPos, temp.longValue());
                                }
                            }
                        }
                    }
                }
                if (row1.getRowNum() > 0) {
                    try {
                        ps.executeUpdate();
                        count++;
                    } catch (SQLException e) {
                        error++;
                        status = "Failure";
                        e.printStackTrace();
                    }
                }
            }
            /*while (rowIterator.hasNext()) {
                int cellPos = 0;
                Row row1 = rowIterator.next();
                Iterator<Cell> cellIterator = row1.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell1 = cellIterator.next();
                    cellPos++;

                    if (cell1.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
                        if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                            ps.setString(cellPos, "NA");
                        } else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                            ps.setString(cellPos, "NA");
                        }
                    } else {
                        if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING)  // insertion of String data
                        {
                            ps.setString(cellPos, cell1.getStringCellValue().trim());
                        } else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                            if (HSSFDateUtil.isCellDateFormatted(cell1)) {      // Insertion of Date cell
                                timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
                                ps.setString(cellPos, timeStamp.trim());
                            } else {
                                BigInteger temp = BigDecimal.valueOf(cell1.getNumericCellValue()).toBigInteger();//Insertion of number field
                                ps.setString(cellPos, String.valueOf(temp).trim());
                            }
                        }
                    }
                }
                try {
                    ps.executeUpdate();
                    count++;
                } catch (SQLException e) {
                    error++;
                    status = "Failure";
                    e.getMessage();
                }
            }*/
            if (error > 0) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Error in Inserting " + error + " records  Kindly check the logs for more information \");");
                out.println("</script>");

            }

            con.commit();
            con.close();
            if (error == 0) status = "Success";

        } catch (SQLException | IOException e) {

            status = "Failure";
            e.printStackTrace();
            hmap.clear();
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

    }
}
