clari5.custom.yes.db.entity.NATM_FT_CORE {
                       attributes=[
                                { name: SYSTEM , type="string:50" },
                                { name: CUST_ID ,type ="string:50" }
                                { name: CA_SCHEME_CODE ,type ="string:50" }
				{ name: BEN_CUST_ID ,type ="string:50" }
				{ name: EFF_AVAILABLE_BALANCE ,type ="number:30,2" }
				{ name: USER_ID ,type ="string:50" }
				{ name: TRAN_CRNCY_CODE ,type ="string:50" }
				{ name: PRODUCT_CODE ,type ="string:50" }
				{ name: SYS_TIME ,type =timestamp }
				{ name: TOD_GRANT_AMOUNT ,type ="number:30,2" }
				{ name: TRAN_AMT ,type ="number:30,2" }
				{ name: USD_EQV_AMT ,type ="number:30,2" }
				{ name: STATUS ,type ="string:50" }
				{ name: PURPOSE_CODE ,type ="string:50"  }
				{ name: REM_ADD1 ,type ="string:50" }
				{ name: BEN_BIC ,type ="string:50" }	
				{ name: REM_ADD2 ,type ="string:50" }
				{ name: FCC_PRODUCT_CODE ,type ="string:50" }
				{ name: REM_ADD3 ,type ="string:50" }
				{ name: TRAN_ID ,type ="string:50" }
				{ name: INSTRUMENT_TYPE ,type ="string:50" }
				{ name: INR_AMOUNT ,type ="string:50" }
				{ name: CLIENT_ACC_NO ,type ="string:50" }
				{ name: CHANNEL_ID ,type ="string:50" }		
				{ name: TRAN_AMOUNT ,type ="number:30,2" }
				{ name: PURPOSE_DESC ,type ="string:50" }
				{ name: BEN_ADD3 ,type ="string:50" }
				{ name: REM_TYPE ,type ="string:50" }
				{ name: BEN_ADD1 ,type ="string:50" }
				{ name: BEN_ADD2 ,type ="string:50" }
				{ name: VALUE_DATE ,type =timestamp }
				{ name: ATM_ID ,type ="string:50" }
				{ name: BEN_CITY ,type ="string:50" }
				{ name: TRAN_NUMONIC_CODE ,type ="string:50" }
				{ name: ISIN_NUMBER ,type ="string:50" }
				{ name: TRAN_CURR ,type ="string:50" }
				{ name: EVENT_ID ,type ="string:4000", key=true }
				{ name: REF_TRAN_CRNCY ,type ="number:10" }
				{ name: BEN_CNTRY_CODE ,type ="string:50" }
				{ name: TRAN_DATE ,type =timestamp }
				{ name: COD_MSG_TYPE ,type ="number:11" }
				{ name: REM_NAME ,type ="string:50" }
				{ name: SERVER_ID ,type ="number:10" }
				{ name: TD_MATURITY_DATE ,type =timestamp }
				{ name: BEN_NAME ,type ="string:50" }
				{ name: INSTRUMENT_NUMBER ,type ="string:50" }
				{ name: REFERENCE_SRL_NUM ,type ="string:50"}
				{ name: SYNC_STATUS ,type ="string:50" }
				{ name: PRODUCT_DESC ,type ="string:50" }
				{ name: BEN_ACCT_NO ,type ="string:50" }
				{ name: HOST_ID ,type ="string:50" }
				{ name: TRAN_PARTICULAR ,type ="string:500" }
				{ name: BANK_CODE ,type ="string:50" }
				{ name: DRCR ,type ="string:50"}
				{ name: INSERTION_TIME ,type =timestamp }
				{ name: BRANCH ,type ="string:50" }
				{ name: ACCT_ID ,type ="string:50" }
				{ name: PSTD_DATE ,type =timestamp}
				{ name: REF_TRAN_AMT ,type ="number:30,2" }
				{ name: CPTY_AC_NO ,type ="string:50" }
				{ name: HOST_TYPE ,type ="string:50" }
				{ name: TRN_DATE ,type =timestamp }
				{ name: INST_STATUS ,type ="string:50" }
				{ name: PSTD_FLG ,type ="string:50" }
				{ name: TD_LIQUIDATION_TYPE ,type ="string:50"}
				{ name: REM_BIC ,type ="string:50" }
				{ name: ONLINE_BATCH ,type ="string:50" }
				{ name: REM_CNTRY_CODE ,type ="string:50" }
				{ name: UCIC_ID ,type ="string:50"}
				{ name: REM_CUST_ID ,type ="string:50" }
				{ name: REM_ACCT_NO ,type ="string:50" }
				{ name: REM_CITY ,type ="string:50" }
				{ name: BRANCH_ID ,type ="string:50" }
                                ]
				indexes {
              					 SYNCSERVERID : [ SYNC_STATUS, SERVER_ID ]
       					 }
                        }
        


