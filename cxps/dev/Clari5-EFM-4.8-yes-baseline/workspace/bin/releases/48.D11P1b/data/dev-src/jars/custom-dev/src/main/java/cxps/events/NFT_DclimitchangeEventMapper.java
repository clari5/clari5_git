// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_DclimitchangeEventMapper extends EventMapper<NFT_DclimitchangeEvent> {

public NFT_DclimitchangeEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_DclimitchangeEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_DclimitchangeEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_DclimitchangeEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_DclimitchangeEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_DclimitchangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_DclimitchangeEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTwoFaMode());
            preparedStatement.setDouble(i++, obj.getCurrentCombinedLimit());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getCustSegment());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit7());
            preparedStatement.setString(i++, obj.getTwoFaStatus());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit4());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit3());
            preparedStatement.setString(i++, obj.getObdxTransactionName());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit6());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit5());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit2());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setDouble(i++, obj.getCurrentIndividualLimit1());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getObdxModuleName());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit6());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit7());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit4());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit5());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit2());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit3());
            preparedStatement.setDouble(i++, obj.getProposedCombinedLimit1());
            preparedStatement.setString(i++, obj.getSessionId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_DCLIMITCHANGE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_DclimitchangeEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_DclimitchangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DCLIMITCHANGE"));
        putList = new ArrayList<>();

        for (NFT_DclimitchangeEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TWO_FA_MODE",  obj.getTwoFaMode());
            p = this.insert(p, "CURRENT_COMBINED_LIMIT", String.valueOf(obj.getCurrentCombinedLimit()));
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "CUST_SEGMENT",  obj.getCustSegment());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT7", String.valueOf(obj.getCurrentIndividualLimit7()));
            p = this.insert(p, "TWO_FA_STATUS",  obj.getTwoFaStatus());
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT", String.valueOf(obj.getProposedCombinedLimit()));
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT4", String.valueOf(obj.getCurrentIndividualLimit4()));
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT3", String.valueOf(obj.getCurrentIndividualLimit3()));
            p = this.insert(p, "OBDX_TRANSACTION_NAME",  obj.getObdxTransactionName());
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT6", String.valueOf(obj.getCurrentIndividualLimit6()));
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT5", String.valueOf(obj.getCurrentIndividualLimit5()));
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT2", String.valueOf(obj.getCurrentIndividualLimit2()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CURRENT_INDIVIDUAL_LIMIT1", String.valueOf(obj.getCurrentIndividualLimit1()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "OBDX_MODULE_NAME",  obj.getObdxModuleName());
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT6", String.valueOf(obj.getProposedCombinedLimit6()));
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT7", String.valueOf(obj.getProposedCombinedLimit7()));
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT4", String.valueOf(obj.getProposedCombinedLimit4()));
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT5", String.valueOf(obj.getProposedCombinedLimit5()));
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT2", String.valueOf(obj.getProposedCombinedLimit2()));
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT3", String.valueOf(obj.getProposedCombinedLimit3()));
            p = this.insert(p, "PROPOSED_COMBINED_LIMIT1", String.valueOf(obj.getProposedCombinedLimit1()));
            p = this.insert(p, "SESSION_ID",  obj.getSessionId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_DCLIMITCHANGE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_DCLIMITCHANGE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_DCLIMITCHANGE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_DclimitchangeEvent obj = new NFT_DclimitchangeEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTwoFaMode(rs.getString("TWO_FA_MODE"));
    obj.setCurrentCombinedLimit(rs.getDouble("CURRENT_COMBINED_LIMIT"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setCustSegment(rs.getString("CUST_SEGMENT"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCurrentIndividualLimit7(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT7"));
    obj.setTwoFaStatus(rs.getString("TWO_FA_STATUS"));
    obj.setProposedCombinedLimit(rs.getDouble("PROPOSED_COMBINED_LIMIT"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setCurrentIndividualLimit4(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT4"));
    obj.setCurrentIndividualLimit3(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT3"));
    obj.setObdxTransactionName(rs.getString("OBDX_TRANSACTION_NAME"));
    obj.setCurrentIndividualLimit6(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT6"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setCurrentIndividualLimit5(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT5"));
    obj.setCurrentIndividualLimit2(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT2"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCurrentIndividualLimit1(rs.getDouble("CURRENT_INDIVIDUAL_LIMIT1"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setObdxModuleName(rs.getString("OBDX_MODULE_NAME"));
    obj.setProposedCombinedLimit6(rs.getDouble("PROPOSED_COMBINED_LIMIT6"));
    obj.setProposedCombinedLimit7(rs.getDouble("PROPOSED_COMBINED_LIMIT7"));
    obj.setProposedCombinedLimit4(rs.getDouble("PROPOSED_COMBINED_LIMIT4"));
    obj.setProposedCombinedLimit5(rs.getDouble("PROPOSED_COMBINED_LIMIT5"));
    obj.setProposedCombinedLimit2(rs.getDouble("PROPOSED_COMBINED_LIMIT2"));
    obj.setProposedCombinedLimit3(rs.getDouble("PROPOSED_COMBINED_LIMIT3"));
    obj.setProposedCombinedLimit1(rs.getDouble("PROPOSED_COMBINED_LIMIT1"));
    obj.setSessionId(rs.getString("SESSION_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_DCLIMITCHANGE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_DclimitchangeEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_DclimitchangeEvent> events;
 NFT_DclimitchangeEvent obj = new NFT_DclimitchangeEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DCLIMITCHANGE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_DclimitchangeEvent obj = new NFT_DclimitchangeEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTwoFaMode(getColumnValue(rs, "TWO_FA_MODE"));
            obj.setCurrentCombinedLimit(EventHelper.toDouble(getColumnValue(rs, "CURRENT_COMBINED_LIMIT")));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setCustSegment(getColumnValue(rs, "CUST_SEGMENT"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCurrentIndividualLimit7(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT7")));
            obj.setTwoFaStatus(getColumnValue(rs, "TWO_FA_STATUS"));
            obj.setProposedCombinedLimit(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT")));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setCurrentIndividualLimit4(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT4")));
            obj.setCurrentIndividualLimit3(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT3")));
            obj.setObdxTransactionName(getColumnValue(rs, "OBDX_TRANSACTION_NAME"));
            obj.setCurrentIndividualLimit6(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT6")));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setCurrentIndividualLimit5(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT5")));
            obj.setCurrentIndividualLimit2(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT2")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCurrentIndividualLimit1(EventHelper.toDouble(getColumnValue(rs, "CURRENT_INDIVIDUAL_LIMIT1")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setObdxModuleName(getColumnValue(rs, "OBDX_MODULE_NAME"));
            obj.setProposedCombinedLimit6(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT6")));
            obj.setProposedCombinedLimit7(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT7")));
            obj.setProposedCombinedLimit4(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT4")));
            obj.setProposedCombinedLimit5(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT5")));
            obj.setProposedCombinedLimit2(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT2")));
            obj.setProposedCombinedLimit3(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT3")));
            obj.setProposedCombinedLimit1(EventHelper.toDouble(getColumnValue(rs, "PROPOSED_COMBINED_LIMIT1")));
            obj.setSessionId(getColumnValue(rs, "SESSION_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DCLIMITCHANGE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DCLIMITCHANGE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TWO_FA_MODE\",\"CURRENT_COMBINED_LIMIT\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"DEVICE_ID\",\"ADDR_NETWORK\",\"HOST_ID\",\"CURRENT_INDIVIDUAL_LIMIT7\",\"TWO_FA_STATUS\",\"PROPOSED_COMBINED_LIMIT\",\"IP_CITY\",\"CURRENT_INDIVIDUAL_LIMIT4\",\"CURRENT_INDIVIDUAL_LIMIT3\",\"OBDX_TRANSACTION_NAME\",\"CURRENT_INDIVIDUAL_LIMIT6\",\"USER_ID\",\"CURRENT_INDIVIDUAL_LIMIT5\",\"CURRENT_INDIVIDUAL_LIMIT2\",\"SYS_TIME\",\"CURRENT_INDIVIDUAL_LIMIT1\",\"CUST_ID\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"PROPOSED_COMBINED_LIMIT6\",\"PROPOSED_COMBINED_LIMIT7\",\"PROPOSED_COMBINED_LIMIT4\",\"PROPOSED_COMBINED_LIMIT5\",\"PROPOSED_COMBINED_LIMIT2\",\"PROPOSED_COMBINED_LIMIT3\",\"PROPOSED_COMBINED_LIMIT1\",\"SESSION_ID\"" +
              " FROM EVENT_NFT_DCLIMITCHANGE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TWO_FA_MODE],[CURRENT_COMBINED_LIMIT],[ERROR_DESC],[SUCC_FAIL_FLG],[CUST_SEGMENT],[IP_COUNTRY],[DEVICE_ID],[ADDR_NETWORK],[HOST_ID],[CURRENT_INDIVIDUAL_LIMIT7],[TWO_FA_STATUS],[PROPOSED_COMBINED_LIMIT],[IP_CITY],[CURRENT_INDIVIDUAL_LIMIT4],[CURRENT_INDIVIDUAL_LIMIT3],[OBDX_TRANSACTION_NAME],[CURRENT_INDIVIDUAL_LIMIT6],[USER_ID],[CURRENT_INDIVIDUAL_LIMIT5],[CURRENT_INDIVIDUAL_LIMIT2],[SYS_TIME],[CURRENT_INDIVIDUAL_LIMIT1],[CUST_ID],[ERROR_CODE],[OBDX_MODULE_NAME],[PROPOSED_COMBINED_LIMIT6],[PROPOSED_COMBINED_LIMIT7],[PROPOSED_COMBINED_LIMIT4],[PROPOSED_COMBINED_LIMIT5],[PROPOSED_COMBINED_LIMIT2],[PROPOSED_COMBINED_LIMIT3],[PROPOSED_COMBINED_LIMIT1],[SESSION_ID]" +
              " FROM EVENT_NFT_DCLIMITCHANGE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TWO_FA_MODE`,`CURRENT_COMBINED_LIMIT`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`CUST_SEGMENT`,`IP_COUNTRY`,`DEVICE_ID`,`ADDR_NETWORK`,`HOST_ID`,`CURRENT_INDIVIDUAL_LIMIT7`,`TWO_FA_STATUS`,`PROPOSED_COMBINED_LIMIT`,`IP_CITY`,`CURRENT_INDIVIDUAL_LIMIT4`,`CURRENT_INDIVIDUAL_LIMIT3`,`OBDX_TRANSACTION_NAME`,`CURRENT_INDIVIDUAL_LIMIT6`,`USER_ID`,`CURRENT_INDIVIDUAL_LIMIT5`,`CURRENT_INDIVIDUAL_LIMIT2`,`SYS_TIME`,`CURRENT_INDIVIDUAL_LIMIT1`,`CUST_ID`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`PROPOSED_COMBINED_LIMIT6`,`PROPOSED_COMBINED_LIMIT7`,`PROPOSED_COMBINED_LIMIT4`,`PROPOSED_COMBINED_LIMIT5`,`PROPOSED_COMBINED_LIMIT2`,`PROPOSED_COMBINED_LIMIT3`,`PROPOSED_COMBINED_LIMIT1`,`SESSION_ID`" +
              " FROM EVENT_NFT_DCLIMITCHANGE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_DCLIMITCHANGE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TWO_FA_MODE\",\"CURRENT_COMBINED_LIMIT\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"CUST_SEGMENT\",\"IP_COUNTRY\",\"DEVICE_ID\",\"ADDR_NETWORK\",\"HOST_ID\",\"CURRENT_INDIVIDUAL_LIMIT7\",\"TWO_FA_STATUS\",\"PROPOSED_COMBINED_LIMIT\",\"IP_CITY\",\"CURRENT_INDIVIDUAL_LIMIT4\",\"CURRENT_INDIVIDUAL_LIMIT3\",\"OBDX_TRANSACTION_NAME\",\"CURRENT_INDIVIDUAL_LIMIT6\",\"USER_ID\",\"CURRENT_INDIVIDUAL_LIMIT5\",\"CURRENT_INDIVIDUAL_LIMIT2\",\"SYS_TIME\",\"CURRENT_INDIVIDUAL_LIMIT1\",\"CUST_ID\",\"ERROR_CODE\",\"OBDX_MODULE_NAME\",\"PROPOSED_COMBINED_LIMIT6\",\"PROPOSED_COMBINED_LIMIT7\",\"PROPOSED_COMBINED_LIMIT4\",\"PROPOSED_COMBINED_LIMIT5\",\"PROPOSED_COMBINED_LIMIT2\",\"PROPOSED_COMBINED_LIMIT3\",\"PROPOSED_COMBINED_LIMIT1\",\"SESSION_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TWO_FA_MODE],[CURRENT_COMBINED_LIMIT],[ERROR_DESC],[SUCC_FAIL_FLG],[CUST_SEGMENT],[IP_COUNTRY],[DEVICE_ID],[ADDR_NETWORK],[HOST_ID],[CURRENT_INDIVIDUAL_LIMIT7],[TWO_FA_STATUS],[PROPOSED_COMBINED_LIMIT],[IP_CITY],[CURRENT_INDIVIDUAL_LIMIT4],[CURRENT_INDIVIDUAL_LIMIT3],[OBDX_TRANSACTION_NAME],[CURRENT_INDIVIDUAL_LIMIT6],[USER_ID],[CURRENT_INDIVIDUAL_LIMIT5],[CURRENT_INDIVIDUAL_LIMIT2],[SYS_TIME],[CURRENT_INDIVIDUAL_LIMIT1],[CUST_ID],[ERROR_CODE],[OBDX_MODULE_NAME],[PROPOSED_COMBINED_LIMIT6],[PROPOSED_COMBINED_LIMIT7],[PROPOSED_COMBINED_LIMIT4],[PROPOSED_COMBINED_LIMIT5],[PROPOSED_COMBINED_LIMIT2],[PROPOSED_COMBINED_LIMIT3],[PROPOSED_COMBINED_LIMIT1],[SESSION_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TWO_FA_MODE`,`CURRENT_COMBINED_LIMIT`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`CUST_SEGMENT`,`IP_COUNTRY`,`DEVICE_ID`,`ADDR_NETWORK`,`HOST_ID`,`CURRENT_INDIVIDUAL_LIMIT7`,`TWO_FA_STATUS`,`PROPOSED_COMBINED_LIMIT`,`IP_CITY`,`CURRENT_INDIVIDUAL_LIMIT4`,`CURRENT_INDIVIDUAL_LIMIT3`,`OBDX_TRANSACTION_NAME`,`CURRENT_INDIVIDUAL_LIMIT6`,`USER_ID`,`CURRENT_INDIVIDUAL_LIMIT5`,`CURRENT_INDIVIDUAL_LIMIT2`,`SYS_TIME`,`CURRENT_INDIVIDUAL_LIMIT1`,`CUST_ID`,`ERROR_CODE`,`OBDX_MODULE_NAME`,`PROPOSED_COMBINED_LIMIT6`,`PROPOSED_COMBINED_LIMIT7`,`PROPOSED_COMBINED_LIMIT4`,`PROPOSED_COMBINED_LIMIT5`,`PROPOSED_COMBINED_LIMIT2`,`PROPOSED_COMBINED_LIMIT3`,`PROPOSED_COMBINED_LIMIT1`,`SESSION_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

