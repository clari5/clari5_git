clari5.inbox.entity.custom_cl5_ibx_item {
	attributes = [


		{ name: retry-id, type="string:50", key=true}

		{ name = item-id, type = "string:300"}

		#flag for holding sent email status
		{name = email-flag, type = "string:20"}

		#Retry count for sent email, if status failed
		{name = retry-count, type = "string:20"}

		#Sent mailing id
		{name = email-id, type = "string:50"}

		#Sent mail subject
		{name = email-subject, type = "string:100"}

		#Sent mail body
		{name = email-body, type = "string:300"}
	]

}
