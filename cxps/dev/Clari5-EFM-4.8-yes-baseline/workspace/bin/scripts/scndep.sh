#!/usr/bin/env bash

# Get BASEPATH
BASEPATH=$( cd $CWD && cd `dirname $0` && pwd )

# This'll do all the pre-requisite checks
main() {
    
    export ROOTDIR=${CL5_WS_REP}

    export REPODIR=${ROOTDIR}/rep
    export MAIN_CLASS='com.cxps.sat.deploy.KenDeploy'
    export WORKDIR=${DEP_BASE}/tmp

    mkdir -p ${WORKDIR}

    header "Performing KEN import and publish"

    [ ! -d ${REPODIR}/ilib ] && err "Installer irregular [${REPODIR}/ilib] not found" && return 1
    [ ! -d ${REPODIR}/elib ] && err "Installer irregular [${REPODIR}/elib] not found" && return 1
    [ ! -d ${REPODIR}/conf ] && err "Installer irregular [${REPODIR}/conf] not found" && return 1

    # Executing import
    msg "Executing scndep import"
    ( jexecute "${MAIN_CLASS}" import "${w}/dev-src/KEN" ) || return 1

    # Executing publish
    msg "Executing scndep publish"
    ( jexecute "${MAIN_CLASS}" publish ) || return 1

    # Cleanup installer directory
    rm -fr ${WORKDIR}
}

jexecute () {

    local _cwd=`pwd`
    local _class=$1
    shift 1
    local _args=$*
    local _jar _log
    
    # Add confs in classpath
    local _cp="${REPODIR}/conf"

    # Add internal jars in classpath
    if [ -e ${ROOTDIR}/meta/SAT-ijar.list ]; then
        for _jar in $( cat ${ROOTDIR}/meta/SAT-ijar.list | cut -d '|' -f1 )
        do
            _cp="${_cp}:${REPODIR}/ilib/${_jar}"
        done
    else
        _cp="${_cp}:${REPODIR}/ilib/*"
    fi

    # Add external jars in classpath
    if [ -e ${ROOTDIR}/meta/SAT-ejar.list ]; then
        for _jar in $( cat ${ROOTDIR}/meta/SAT-ejar.list )
        do
            _cp="${_cp}:${REPODIR}/elib/${_jar}"
        done
    else
        _cp="${_cp}:${REPODIR}/elib/*"
    fi

    ( cd ${WORKDIR} && java -cp "${_cp}" ${_class} ${_args} )
}

# Prints a main header
unset header 2>/dev/null
header() {

    local _message="`echo $*`"
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "\033[01;36m
${_tag}
  ${_message}
${_tag}
\033[00m"

}

# Prints success messages
unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

# Prints error messages
unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

# Local message
msg() {
    
    local _message=$( echo $* )
    local _tag=$(echo xx${_message}xx | sed 's/./-/g')

printf "\033[03;32m
${_tag}
| ${_message} |
${_tag}
\033[00m"

}

# -----------
# Main Block
# -----------
[ -e ${MISSING_ASSEMBLY_FILE} ] \
    && err "Error: Build components missing, run fbuild.sh" && exit 1


main $*
[ $? -eq 0 ] || ( err "Error: Operation failed" && exit 1 )
