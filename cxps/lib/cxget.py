#!/usr/bin/env python
# ===========================================================================
# cxget.py
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# This program checks if there is a upgraded version of given file
# in resources. In case yes, it downloads the same in $CL5_MAIN_REP_DIR
# 
# Usage: 
# python cxget.py <module> <version> <compression>
# ===========================================================================

from __future__ import print_function

import getopt
import os
import re
import sys
from datetime import datetime

import requests
from dateutil import parser

try:
    requests.packages.urllib3.disable_warnings()
except:
    pass

def getDevRes(pkgname, pkgver, compression='tgz', baseurl=None):
    status = 1
    if not baseurl:
        baseurl = os.environ['GLB_DEVRES_URL']

    rep = os.environ['CL5_LOCAL_REP']
    try:
        r = requests.get(baseurl + '/', timeout=1, verify=False)
    except requests.exceptions.ConnectionError:
        return None
    except:
        # Bail out on any exception!
        print ('Unable to get file list', file=sys.stderr)
        return None

    if r.status_code != 200:
        print ('requests returned [%d]' % r.status_code, file=sys.stderr)
        return None

    # Create RE pattern to extract all file,date that matches the package name
    pkg = pkgname + '-' + pkgver + '.' + compression

    checklist = {}
    pat = re.compile(pkg)
    for line in r.text.split("\n"):
        m = pat.findall(line)
        if len(m) > 0:
            try:
                # strip initial till 'href='
                s = line[line.find('href='):]
                # Split on '"'
                words = s.split('"')
                # Get file name
                (name, date) = (words[1], words[4][1:])
                # Clean up date
                date = date[0:date.find('  ')]
                checklist[name] = date
            except:
                continue

    rep += '/.downloads/' + pkgname + '/' + pkgver

    # Create directory, including parent
    if not os.access(rep, os.F_OK):
        os.makedirs(rep)

    # Verify and download all packages
    if pkg not in checklist:
        print ("Error: Package [%s] not found in remote repository!" % pkg, file=sys.stderr)
        return 1

    repfile = rep + '/' + pkg
    fetch = True
    if os.path.exists(repfile):
        # Check if the file is latest
        try:
            dt1 = parser.parse(checklist[pkg])
            dt2 = datetime.fromtimestamp(os.path.getmtime(repfile))
        except:
            pass

        if dt1 <= dt2:
            fetch = False

    if not fetch:
        return 0

    # Download the file
    sys.stdout.write('Downloading latest [' + pkg + '] ... ')
    sys.stdout.flush()
    r = requests.get(baseurl + '/' + pkg, timeout=2, verify=False)
    if r.status_code == 200:
        with open(repfile, 'wb') as fw:
            fw.write(r.content)
        print ("done")
        status = 0
        # print >> sys.stdout, 'Downloaded latest [%s]' % pkg
    else:
        print ("error")
        print ('Error! [%d], while fetching [%s]' % (r.status_code, pkg), file=sys.stderr)

    return status

# ------------------------------------------------------
# MAIN ACTION BLOCK
# ------------------------------------------------------
# python cxget.py <name> <ver> [compression]
# default compression is tgz
# ------------------------------------------------------
if __name__ == '__main__':

    (opts, args) = getopt.getopt(sys.argv[1:], "", ["url="])

    if len(args) < 2:
        print ('Usage: python cxget.py [--url=<resource_url>] <pkg-name> <pkg-ver> [compression]', file=sys.stderr)
        sys.exit(1)

    url = None
    for o, a in opts:
        if o == '--url':
            url = a

    c = ''
    try:
        c = args[2]
    except:
        pass

    if c:
        status = getDevRes(args[0], args[1], compression=c, baseurl=url)
    else:
        status = getDevRes(args[0], args[1], baseurl=url)

    sys.exit(status)
