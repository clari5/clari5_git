#!/usr/bin/env bash

# This program sets the deployment folder along with configuration setup for the same.

. cxpsvars.sh	# Assuming path is set

unalias ins_maven 2>/dev/null
ins_maven()
{
	MAVEN_VERSION=`${GREP} maven ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export MAVEN_PROGRAM="maven-${MAVEN_VERSION}"
	if [ ! -d ${CXPS_BIN}/${MAVEN_PROGRAM} ] ; then
		printf "Installing MAVEN ... "
		cxinstall ${MAVEN_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f maven && ln -fs ${CXPS_BIN}/${MAVEN_PROGRAM} maven )
	else
		( cd ${CXPS_BIN} && rm -f maven && ln -fs ${MAVEN_PROGRAM} maven )
	fi
}

unalias ins_oracleXE 2>/dev/null
ins_oracleXE()
{
	if [ "$OS" != "Linux" ] ; then
		echo "Oracle XE is not available for your platform"
		return
	fi
	if uname -a | ${GREP} -q x86_64 >/dev/null
	then
		echo "Oracle-XE is not available for 64-bit"
	else
		# Install Oracle XE
		if ${GREP} -q 192.168.5.56 /etc/apt/sources.list
		then
			echo "/etc/apt/sources.list is already up-to-date"
		else
			sudo sh -c 'echo "deb http://192.168.5.56/debian unstable main non-free" >> /etc/apt/sources.list'
			wget http://oss.oracle.com/el4/RPM-GPG-KEY-oracle -O- | sudo apt-key add -
			sudo apt-get update
		fi

		if [ ! -f /etc/init.d/oracle-xe ] ; then
			sudo apt-get install oracle-xe
			echo "You will be asked for Oracle sys/system password for which provide 'manager' as default. Provide 8080 as default HTTP port."
			echo "Press <ENTER> to start configuration"
			read ans
			sudo /etc/init.d/oracle-xe configure
		else
			echo "Oracle-XE is already installed!"
		fi

	# Do not set ORACLE_HOME because it has to be explicitly set in devvars.sh
	#
	#	if [ -f /etc/init.d/oracle-xe -a -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
	#		( cd ${CXPS_WORKSPACE}/bin && \rm -f oracle && ln -fs /usr/lib/oracle/xe/app/oracle/product/10.2.0/server oracle )
	#	else
	#		( cd ${CXPS_BIN} && \rm -f oracle && ln -fs /usr/lib/oracle/xe/app/oracle/product/10.2.0/server oracle )
	#	fi
	fi
}

unalias ins_oracle 2>/dev/null
ins_oracle()
{
	ORACLE_VERSION=`${GREP} -i 'oracle[ 	]' ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	ORACLE_PROGRAM=""
	if [ -z "`which sqlplus`" ] ; then
		case "$OS" in
			"Linux" )	if uname -a | ${GREP} -q x86_64 >/dev/null
						then
							export ORACLE_PROGRAM=oracle64-${ORACLE_VERSION}
						else
							export ORACLE_PROGRAM=oracle32-${ORACLE_VERSION}
						fi
						;;
			"Darwin")	ORACLE_VERSION=10.2
						export ORACLE_PROGRAM=oracle32-Darwin-${ORACLE_VERSION}
						;;
			"*")		echo "WARNING: No oracle found. Please install and add its path in .profile" >&2
						;;
		esac

		if [ ! -z "$ORACLE_PROGRAM" ] ; then
			if [ ! -d "${CXPS_BIN}/${ORACLE_PROGRAM}" ] ; then
				printf "Installing Oracle Client..."
				cxinstall ${ORACLE_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
				[ $? -eq 0 ] && echo "done!"
			fi

            if [ ! -d ${CXPS_BIN}/${ORACLE_PROGRAM}/network ]; then
                [ -d ${CXPS_BIN}/${ORACLE_PROGRAM}/admin ] \
                    && mkdir ${CXPS_BIN}/${ORACLE_PROGRAM}/network \
                    && mv ${CXPS_BIN}/${ORACLE_PROGRAM}/admin ${CXPS_BIN}/${ORACLE_PROGRAM}/network
            fi

			if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
				( cd ${CXPS_WORKSPACE}/bin && \rm -f oracle && ln -fs ${CXPS_BIN}/${ORACLE_PROGRAM} oracle )
			else
				( cd ${CXPS_BIN} && \rm -f oracle && ln -fs ${ORACLE_PROGRAM} oracle )
			fi
		fi
	fi
}

unalias ins_flex 2>/dev/null
ins_flex()
{
    FLEX_VERSION=`${GREP} flex ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	FLEX=`${GREP} flex ${DEPFILE} | cut -f1 -d':' | ${SED} 's/[ 	]*//g'`
	export FLEX_PROGRAM="${FLEX}-${FLEX_VERSION}"
	if [ ! -d ${CXPS_BIN}/${FLEX_PROGRAM} ] ; then
		printf "Installing flex ... "
		cxinstall ${FLEX_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f ${FLEX} && ln -fs ${CXPS_BIN}/${FLEX_PROGRAM} ${FLEX} )
	else
		( cd ${CXPS_BIN} && \rm -f ${FLEX} && ln -fs ${FLEX_PROGRAM} ${FLEX} )
	fi
}

unalias ins_apr 2>/dev/null
ins_apr()
{
	# Note that this is a dependency of TOMCAT and therefore the deps.cfg should be picked from elsewhere
	APR_VERSION=`${GREP} apr ${CXPS_BIN}/deps.cfg | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	if [ ! -d "${CXPS_LIB}/apr" -a ! -f "${CXPS_LIB}/apr.failed" ] ; then
		echo "Installing Apache Portable Runtime..."
		cxinstall apr-${APR_VERSION}.tgz "--dir=${CXPS_LIB}" ${CXPS_PROGRAMS}
		if [ $? -eq 0 ] ; then
			( 
				cd ${CXPS_LIB}/apr-${APR_VERSION}
				./configure --prefix=${CXPS_LIB}/apr
				make -j 2 && make -j 2 install
			)
			[ ! -d "${CXPS_LIB}/apr" ] && touch ${CXPS_LIB}/apr.failed
		fi
	fi
}

unalias ins_cxpsapache 2>/dev/null
ins_cxpsapache()
{
    CXPSAPACHE_VERSION=`${GREP} cxpsapache ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[   ]*//g'`
    export CXPSAPACHE_PROGRAM=cxpsapache-${CXPSAPACHE_VERSION}
    if [ ! -d ${CXPS_DEPLOYMENT}/${CXPSAPACHE_PROGRAM} ] ; then
        if [ ! -f ${CXPS_PROGRAMS}/${CXPSAPACHE_PROGRAM}.tgz ] ; then
            printf "Downloading cxpsapache......"
            cxinstall ${CXPSAPACHE_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
            [ $? -eq 0 ] && echo "Done!"
        else
            cd ${CXPS_DEPLOYMENT}
            if [ ! -d ${CXPSAPACHE_PROGRAM} ] ; then
                printf "Installing cxpsapache ... "
                ${TAR} xf ${CXPS_PROGRAMS}/${CXPSAPACHE_PROGRAM}.tgz
                echo "Done!"
            fi
        fi
    fi

    cd ${CXPS_DEPLOYMENT}
    rm -f cxpsapache ; ln -fs ${APPAGENT_PROGRAM} cxpsapache 
}
unalias ins_tomcat 2>/dev/null
ins_tomcat()
{
	# Install the dependency
	ins_apr

	TOMCAT_VERSION=`${GREP} tomcat ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export TOMCAT_PROGRAM="tomcat-${TOMCAT_VERSION}"
	# Check if it already exist in ${CXPS_DEPLOYMENT}
	if [ ! -d ${CXPS_DEPLOYMENT}/${TOMCAT_PROGRAM} ] ; then
		if [ ! -d ${CXPS_PROGRAMS}/${TOMCAT_PROGRAM} ] ; then
			echo "Configuring Tomcat with APR ..."
			cxinstall ${TOMCAT_PROGRAM}.tgz "--dir=${CXPS_PROGRAMS}" ${CXPS_PROGRAMS}
			if [ $? -eq 0 ] ; then
				(	cd ${CXPS_PROGRAMS}/${TOMCAT_PROGRAM}/bin ;
					${TAR} -xzf tomcat-native.tar.gz ;
					cd tomcat-native-*-src/jni/native ;
					./configure --with-apr=${CXPS_LIB}/apr --with-java-home="$JAVA_HOME" ;
					make -j 2 ;
					( cd .libs && ${TAR} cvf - * ) | ( cd ${CXPS_LIB} && ${TAR} xvf - )
				)
			fi
		fi
		cd ${CXPS_DEPLOYMENT}
		if [ ! -d ${TOMCAT_PROGRAM} ] ; then
			printf "Installing Tomcat ... "
			( cd ${CXPS_PROGRAMS} && ${TAR} cf - ${TOMCAT_PROGRAM} ) | ${TAR} xf - 
			echo "Done!"
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f tomcat ; ln -fs ${TOMCAT_PROGRAM} tomcat
	( cd tomcat ; mkdir -p apex/lib ; mkdir -p apex/classes )
}

unalias ins_activemq 2>/dev/null
ins_activemq()
{
	ACTIVEMQ_VERSION=`${GREP} activemq ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export ACTIVEMQ_PROGRAM=activemq-${ACTIVEMQ_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${ACTIVEMQ_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${ACTIVEMQ_PROGRAM}.tgz ] ; then
			printf "Downloading ActiveMQ..."
			cxinstall ${ACTIVEMQ_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${ACTIVEMQ_PROGRAM} ] ; then
				printf "Installing ActiveMQ ... "
				${TAR} xf ${CXPS_PROGRAMS}/${ACTIVEMQ_PROGRAM}.tgz 
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f activemq ; ln -fs ${ACTIVEMQ_PROGRAM} activemq
}

unalias ins_openfire 2>/dev/null
ins_openfire()
{
	OPENFIRE_VERSION=`${GREP} openfire ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export OPENFIRE_PROGRAM=openfire-${OPENFIRE_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${OPENFIRE_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${OPENFIRE_PROGRAM}.tgz ] ; then
			printf "Downloading OpenFire..."
			cxinstall ${OPENFIRE_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${OPENFIRE_PROGRAM} ] ; then
				printf "Installing Openfire ... "
				${TAR} xf ${CXPS_PROGRAMS}/${OPENFIRE_PROGRAM}.tgz
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f openfire ; ln -fs ${OPENFIRE_PROGRAM} openfire
}

unalias ins_kestrel 2>/dev/null
ins_kestrel()
{
	KESTREL_VERSION=`${GREP} kestrel ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export KESTREL_PROGRAM=kestrel-${KESTREL_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${KESTREL_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${KESTREL_PROGRAM}.tgz ] ; then
			printf "Downloading Kestrel..."
			cxinstall ${KESTREL_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${KESTREL_PROGRAM} ] ; then
				printf "Installing Kestrel ... "
				${TAR} xf ${CXPS_PROGRAMS}/${KESTREL_PROGRAM}.tgz
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f kestrel ; ln -fs ${KESTREL_PROGRAM} kestrel
}

unalias ins_appagent 2>/dev/null
ins_appagent()
{
    APPAGENT_VERSION=`${GREP} appagent ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[   ]*//g'`
    export APPAGENT_PROGRAM=appagent-${APPAGENT_VERSION}
    if [ ! -d ${CXPS_DEPLOYMENT}/${APPAGENT_PROGRAM} ] ; then
        if [ ! -f ${CXPS_PROGRAMS}/${APPAGENT_PROGRAM}.tgz ] ; then
            printf "Downloading appdynamics appagent"
            cxinstall ${APPAGENT_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
            [ $? -eq 0 ] && echo "Done!"
        else
            cd ${CXPS_DEPLOYMENT}
            if [ ! -d ${APPAGENT_PROGRAM} ] ; then
                printf "Installing appdynamics appagent ... "
                ${TAR} xf ${CXPS_PROGRAMS}/${APPAGENT_PROGRAM}.tgz
                echo "Done!"
            fi
        fi
    fi

    cd ${CXPS_DEPLOYMENT}
    rm -f appdynamics ; ln -fs ${APPAGENT_PROGRAM} appdynamics 
}

unalias ins_CXQ 2>/dev/null
ins_CXQ()
{
	CSB_VERSION=`${GREP} CXQ ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export CSB_PROGRAM=CXQ-${CSB_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${CSB_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${CSB_PROGRAM}.tgz ] ; then
			printf "Downloading CXQ"
			cxinstall ${CSB_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${CSB_PROGRAM} ] ; then
				printf "Installing CXQ ... "
				${TAR} xf ${CXPS_PROGRAMS}/${CSB_PROGRAM}.tgz
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f CXQ ; ln -fs ${CSB_PROGRAM} CXQ
}

unalias ins_antlr 2>/dev/null
ins_antlr()
{
    ANTLR_VERSION=`${GREP} antlr ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export ANTLR_PROGRAM="antlr-${ANTLR_VERSION}"
	if [ ! -d ${CXPS_BIN}/${ANTLR_PROGRAM} ] ; then
		printf "Installing antlr ... "
		cxinstall ${ANTLR_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f antlr && ln -fs ${CXPS_BIN}/${ANTLR_PROGRAM} antlr )
	else
		( cd ${CXPS_BIN} && \rm -f antlr && ln -fs ${ANTLR_PROGRAM} antlr )
	fi
}

unalias ins_gradle 2>/dev/null
ins_gradle()
{
    GRADLE_VERSION=`${GREP} gradle ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export GRADLE_PROGRAM="gradle-${GRADLE_VERSION}"
	local gradle_install=0
	if [ -z "`which gradle`" ] ; then
		gradle_install=1
	else
		version=`gradle --version | ${GREP} ^Gradle | cut -d ' ' -f2`
        if [ $version != ${GRADLE_VERSION} ]; then
            newer=$( echo -e "${version}\n${GRADLE_VERSION}" | sort -V | tail -1 )
            [[ $newer = ${GRADLE_VERSION} ]] && gradle_install=1
        fi
	fi
	[ $gradle_install -eq 0 ] && return
	if [ "$OS" = "Linux" ] ; then
		wget http://192.168.5.56/resources/${GRADLE_PROGRAM}-bin.zip
	    #wget --no-check-certificate ${GLB_RES_URL}/${file}-bin.zip 
		mv ${GRADLE_PROGRAM}-bin.zip /tmp/${GRADLE_PROGRAM}.zip
		if [ $? -eq 0 ] ; then
			( cd /usr/local && sudo unzip /tmp/${GRADLE_PROGRAM}.zip && \rm -f /tmp/${GRADLE_PROGRAM}.zip )
			( cd /usr/local/bin && sudo ln -fs ../${GRADLE_PROGRAM}/bin/gradle . )
		fi
	else
		echo "Please install gradle [$GRADLE_VERSION] using OS specific installer" >&2
	fi
}

unalias ins_codegen 2>/dev/null
ins_codegen()
{
    CODEGEN_VERSION=`${GREP} codegen ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export CODEGEN_PROGRAM="codegen-${CODEGEN_VERSION}"
	if [ ! -d ${CXPS_BIN}/${CODEGEN_PROGRAM} ] ; then
		printf "Installing codegen ... "
		cxinstall ${CODEGEN_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f codegen && ln -fs ${CXPS_BIN}/${CODEGEN_PROGRAM}/bin/gen codegen )
	fi
}

ins_maxmind(){
	curdir=`pwd`
	MAXMIND_PROGRAM=maxmind
	datFile=$CXPS_DEPLOYMENT/GeoLiteCity.dat
	#echo Checking ${MAXMIND_PROGRAM}...
	if [ ! -f $datFile ]; then
		if [ ! -f $CXPS_PROGRAMS/$MAXMIND_PROGRAM.zip ] ; then
			echo "Downloading maxmind.."
			cd ${CXPS_PROGRAMS}
			download $GLB_DEVRES_URL/${MAXMIND_PROGRAM}.zip
			[ $? -eq 0 ] && echo "Done!"
			cd $curdir
		fi

		echo "Deploying maxmind datafile in $CXPS_DEPLOYMENT"
		unzip ${CXPS_PROGRAMS}/${MAXMIND_PROGRAM}.zip -d ${CXPS_DEPLOYMENT} > /dev/null
		if [ -f $datFile ]; then
			echo "Maxmind setup done!"
		else
			echo "Maxmind setup failed!"
		fi
	fi
}

createPath()
{
	# Update the PATH file
	cat > ${CXPS_WORKSPACE}/bin/path.sh <<EOT
#!/bin/sh

[ -d \${CXPS_WORKSPACE}/bin/${FLEX} ] && export FLEX_HOME=\${CXPS_WORKSPACE}/bin/${FLEX}
[ -d \${CXPS_WORKSPACE}/bin/maven ] && export M2_HOME=\${CXPS_WORKSPACE}/bin/maven
[ -d \${CXPS_WORKSPACE}/bin/oracle ] && export ORACLE_HOME=\${CXPS_WORKSPACE}/bin/oracle
[ -d \${CXPS_WORKSPACE}/bin/antlr ] && export ANTLR_HOME=\${CXPS_WORKSPACE}/bin/antlr

[ ! -z "\${my_FLEX_HOME}" ] && export FLEX_HOME=\${my_FLEX_HOME}
if [ -d \${CXPS_WORKSPACE}/bin/oracle -a ! -z "\${my_ORACLE_HOME}" ] ; then
	\rm -f \${CXPS_WORKSPACE}/bin/oracle
	ln -fs \${my_ORACLE_HOME} \${CXPS_WORKSPACE}/bin/oracle
fi

export MAVEN_PROGRAM=${MAVEN_PROGRAM}
export FLEX_PROGRAM=${FLEX_PROGRAM}
export TOMCAT_PROGRAM=${TOMCAT_PROGRAM}
export OPENFIRE_PROGRAM=${OPENFIRE_PROGRAM}
export ACTIVEMQ_PROGRAM=${ACTIVEMQ_PROGRAM}
export ORACLE_PROGRAM=${ORACLE_PROGRAM}

export TOMCAT_HOME=${CXPS_DEPLOYMENT}/${TOMCAT_PROGRAM}
export ACTIVEMQ_HOME=${CXPS_DEPLOYMENT}/${ACTIVEMQ_PROGRAM}
export OPENFIRE_HOME=${CXPS_DEPLOYMENT}/${OPENFIRE_PROGRAM}

[ -z "\$MAVEN_OPTS" ] && export MAVEN_OPTS="-server -Xms100m -Xmx512m -XX:MaxPermSize=256m"

# Set up path for flex, java, python, mysql and maven
[ -z "\$M2_HOME" ] || PATH=\${M2_HOME}/bin:\$PATH
[ -z "\$FLEX_HOME" ] || PATH=\${FLEX_HOME}/bin:\$PATH
if [ ! -z "\$ORACLE_HOME" ] ; then
	PATH=\${ORACLE_HOME}:\${ORACLE_HOME}/bin:\$PATH
	export LD_LIBRARY_PATH=\${ORACLE_HOME}:\${ORACLE_HOME}/lib
	export DYLD_LIBRARY_PATH=\${LD_LIBRARY_PATH}
	export LIBPATH=\${LD_LIBRARY_PATH}
fi

[ ! -z "\${ANTLR_HOME}" ] && export PATH=\${PATH}:\${ANTLR_HOME}/bin

# Put workspace/bin in PATH
export PATH=\${CXPS_WORKSPACE}/bin:\${PATH}
EOT
}

# ------------------
# MAIN ACTION BLOCK
# ------------------
me=`basename $0`

if [ $# -ne 2 ] ; then
	echo "Usage:$me <DEPFILE> <DEPLOYMENT-FOLDER>"
	exit 1
fi

# Load localized installation methods
EXTN=$CXPS_WORKSPACE/bin/depsetupextn.sh
[ -f $EXTN ] && chmod +x $EXTN && . $EXTN

# Change to deployment folder to get debs

DEPFILE=$1
if [ ! -f "${DEPFILE}" ] ; then
	echo "Error:$me: ${DEPFILE} does not exist"
	exit 2
fi
if ! echo ${DEPFILE} | ${GREP} -q '^/' ; then
	DEPFILE=`pwd`/${DEPFILE}
fi

CXPS_DEPLOYMENT=$2
if [ ! -d ${CXPS_DEPLOYMENT} ] ; then
	mkdir -p ${CXPS_DEPLOYMENT} 2>/dev/null
	[ $? -ne 0 ] && echo "Error:$me: Unable to create ${CXPS_DEPLOYMENT}" && exit 2
fi
cd ${CXPS_DEPLOYMENT}

for dep in `cat ${DEPFILE} | ${SED} 's/[ 	]*//g' | tr ':' '-' | tr '\012' ' '`
do
	case "$dep" in
		maven*)		ins_maven ;;
		tomcat*)	ins_tomcat ;;
		cxpsapache*)ins_cxpsapache ;;
		openfire*)	ins_openfire ;;
		activemq*)	ins_activemq ;;
		oracleXE*)	ins_oracleXE ;;
		oracle*)	ins_oracle ;;
		kestrel*)	ins_kestrel ;;
		flex*)		ins_flex ;;
		CXQ*)		ins_CXQ ;;
		appagent*)  ins_appagent ;;
		antlr*)		ins_antlr ;;
		codegen*)	ins_codegen ;;
		maxmind*)	ins_maxmind ;;
		gradle*)	ins_gradle ;;
	esac

	type dep_inst 2>&1 | grep -q function && dep_inst $dep
done

# Create the PATH file
[ -d ${CXPS_WORKSPACE}/bin ] && createPath
