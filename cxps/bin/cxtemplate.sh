#!/bin/bash
# This script generates the file from its template after doing following substitutions.

. port.sh

# This method configures the template for the substitution parameters
# These are specified in an environment variable CX_TEMPLATE_SUBSTITUTIONS in a 
# pipe separated name=value format as follows -
# 	VERSION=baseline|DBVERSION=baseline|CXPS_DEPLOYMENT|..
# In case the '=' is not specified, it substitutes from the current env

__cxtemplate_crecmd(){
	if [ ! -z "$CX_TEMPLATE_SUBSTITUTIONS" ] ; then 
		export CX_TEMPLATE_CMD="${SED} "
		for nvp in `echo $CX_TEMPLATE_SUBSTITUTIONS | tr '|' '\012'`
		do
			if echo $nvp | ${GREP} -q '='
			then
				name=`echo $nvp | cut -f1 -d'='`
				value=`echo $nvp | cut -f2- -d'='`
			else
				name=`echo $nvp`
				value=`eval echo '$'$name`
			fi
			CX_TEMPLATE_CMD="$CX_TEMPLATE_CMD -e s,\\\${$name},$value,"
		done 
	fi
	export CX_TEMPLATE_SKIPLIST="${CX_TEMPLATE_SKIPLIST} -e /target/ -e /.svn/"
	# echo CX_TEMPLATE_SKIPLIST=$CX_TEMPLATE_SKIPLIST
	# echo CX_TEMPLATE_CMD=$CX_TEMPLATE_CMD
}

__cxtemplate_processFile(){
	input=$1
	output=$2
	$CX_TEMPLATE_CMD $input > $output
	if echo $output | ${GREP} -q '\.sh$'
	then
		chmod +x $output
	fi
}

# This method processes all templates within a given folder
__cxtemplate_processFolder(){

	quiet=0
	[ "$1" = '-q' ] && quiet=1 && shift 1
	folder=$1
	[ $quiet -eq 0 ] && echo "Working on all templates in [$folder/] "
	__cwd=`pwd`
	cd $folder
	templates=`find . -name '*.template.*' | ${GREP} -v $CX_TEMPLATE_SKIPLIST`
	for templateFile in $templates
	do
		echo "Processing $templateFile ..."
		file=`echo $templateFile | ${SED} 's/\.template\././'`
		__cxtemplate_processFile $templateFile $file
	done
	cd $__cwd
}

# This method processes a given set of jar files 
__cxtemplate_processJar(){
	cwd=`pwd`
	TMPFOLDER=/tmp/cxtemplate.$$
	[ -d $TMPFOLDER ] || mkdir -p $TMPFOLDER
	[ ! -z "$TMPFOLDER" ] && \rm -rf "${TMPFOLDER}/*"

	for jar in $*
	do
		if echo $jar | ${GREP} -q '.jar$'
		then
			allTemplates=`jar tf $jar | ${GREP} -v $CX_TEMPLATE_SKIPLIST | ${GREP} '\.template\.' | tr '\012' ' '`
			if [ ! -z "$allTemplates" ] ; then
				echo "Processing $jar ..."
				base=""
				if echo $jar | ${GREP} -q '^[^/]'
				then
					base=${cwd}/
				fi
				cd $TMPFOLDER
				# echo "jar xf ${base}$jar `echo $allTemplates`"
				jar xf ${base}$jar `echo $allTemplates`
				__cxtemplate_processFolder $TMPFOLDER >/dev/null
				# echo "jar uf ${base}$jar *"
				jar uf ${base}$jar *
				rm -f $allTemplates `echo $allTemplates | ${SED} 's/\.template\././g'`
				cd $cwd
			else
				echo "Skipping $jar ..."
			fi
		else
			echo "Skipping $jar ..."
		fi
	done

	cd $cwd
	rmdir ${TMPFOLDER}
}

# This method generates a makefile for all templates within a given folder.
genMakefile(){

	folder=$1
	makefile=$2
	[ -z "$makefile" ] && makefile=makefile
	cwd=`pwd`

	cd $folder

	templates=`find . -name '*.template.*' | ${GREP} -v $CX_TEMPLATE_SKIPLIST`
	files=`echo $templates | ${SED} 's/\.template\././g'`

	echo "ALLFILES = \\" > $makefile 
	for file in $files
	do   
		echo "      $file \\"
	done >> $makefile

	cat >> $makefile <<EOT

all: \$(ALLFILES)

clean:
	-rm \$(ALLFILES)

EOT

	for templateFile in $templates
	do
		file=`echo $templateFile | ${SED} 's/\.template\././'`
		echo "$file: $templateFile"
		echo "	@cxtemplate.sh $templateFile $file"
		echo
	done >> $makefile
}

__cxtemplate_usage(){
	echo "Usage: cxtemplate.sh configure [-skip pattern,pattern,...] [<substitution-string>]"
	echo "       cxtemplate.sh make <folder-name> [makefile]"
	echo "       cxtemplate.sh <folder-name>"
	echo "       cxtemplate.sh <jar> ..."
	echo "       cxtemplate.sh <template-file> [output-file]"
}

# ------------------
# MAIN ACTION BLOCK
# ------------------
if [ $# -eq 0 ] ; then
	__cxtemplate_usage

elif [ "$1" = "configure" ] ; then
	if [ "$2" = "-skip" ] ; then
		if [ -z "$3" ] ; then
			export CX_TEMPLATE_SKIPLIST=''
		else
			export CX_TEMPLATE_SKIPLIST="`echo ,$3 | ${SED} -e 's/,,*/,/g' -e 's/,$//' -e 's/,/ -e /g'`"
		fi
		shift 2
	fi
	[ ! -z "$2" ] && CX_TEMPLATE_SUBSTITUTIONS=$2

	printf "Configuring template processor ..."
	__cxtemplate_crecmd
	echo " Done!"

elif [ "$1" = "make" ] ; then
	if [ -d "$2" ] ; then
		printf "Generating template processor makefile ..."
		genMakefile $2 $3
		echo " Done!"
	else
		__cxtemplate_usage
	fi

elif [ ! -z "$CX_TEMPLATE_CMD" ] ; then

	if [ -d "$1" ] ; then
		__cxtemplate_processFolder $1

	elif [ -f "$1" ] ; then
		if echo "$1" | ${GREP} -q '.jar$' ; then
			__cxtemplate_processJar $*
		else
			templateFile=$1
			file=$2
			[ -z "$file" ] && file=`echo $templateFile | ${SED} 's/\.template\././'`
			printf "Processing $templateFile ..."
			__cxtemplate_processFile $templateFile $file
			echo " Done!"
		fi
	else
		echo "Error:$1 is not a folder/file"
	fi
else
	echo "Error: cxtemplate.sh has not been configured yet"
fi
