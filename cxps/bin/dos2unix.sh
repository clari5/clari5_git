#!/bin/sh

[ $# -eq 0 ] && echo "dos2unix.sh <dos-file> ..." && exit 1

for file in $*
do
	TMPFILE=/tmp/tmp.`basename $file`
	cat $file | tr -d '' > $TMPFILE
	diff -q $file $TMPFILE > /dev/null
	if [ $? -eq 1 ] ; then
		echo "Processing $file ..." 
		mv $TMPFILE $file
	else
		echo "Skipping $file ..." 
		rm -f $TMPFILE
	fi
done
